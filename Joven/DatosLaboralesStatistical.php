<?php
include( 'GoogChart.class.php' );

    $lista = $bdd->prepare("SELECT * FROM joven");
	$lista->execute();

                $trabajo = $bdd->prepare("SELECT * from datoslaborales");
                $trabajo->execute();

    $chart = new GoogChart();	
?>
<table style="width: 100%; border-radius: 10px; border-color: skyblue; border-style: groove; background-color: white; ">
<tr>
	<th colspan="2"><h2>Datos Laborales - Estadisticas</h2></th>
</tr>
<tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
<tr style="">
	<td colspan="2" style="text-align: center;"> <h2>Total de Registrados: <?php echo $lista->rowCount();?> Jovenes</h2></td>
</tr>
<tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
<tr><td>
<?php
        $diferencia = $lista->rowCount() - $trabajo->rowCount();
	    $dato1 = ($diferencia /  $lista->rowCount())*100;
        $dato2 = ( $trabajo->rowCount() /  $lista->rowCount())*100;
    
        $data = array(
    			"No han trabajado ". number_format($dato1,2) . '%' => $dato1,
    			'Han Trabajado '. number_format($dato2,2) . '%' => $dato2
    		     );

        
        $color = array(
        			'#99C754',
        			'#54C7C5',
        			'#78bbe6',
        		);

        $chart->setChartAttrs( array(
        	'type' => 'pie',
        	'title' => 'Jovenes que han trabajado',
        	'data' => $data,
        	'size' => array( 550, 300 ),
        	'color' => $color
        	));
    // Print chart
    echo $chart;

    ?>
    </td>
        <td>
            Han trabajado: <br /><?php echo $trabajo->rowCount();?> j�venes<br /><br />
            No han trabajado: <br /><?php echo $diferencia;?> j�venes
        </td>
    </tr>        
</table>