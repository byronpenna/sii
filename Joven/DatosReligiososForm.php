         
<div style="float:left; width: 20%;">
<?php include ('menuJoven.php');?>
</div>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; ">	
    <?php
	
	if(isset($_GET['dr'])){
		$lista = $bdd->prepare("SELECT * FROM  datosreligion WHERE IdReligion =" . $_GET['dr']);
		$lista->execute();
		$datoslista=$lista->fetch();

	$abm="Modificar";

	}else
    {
		$datoslista[0]="";
		$datoslista[1]="";
		$datoslista[2]="";
		$datoslista[3]="";
		$datoslista[4]="";
		$datoslista[5]="";
		$abm="Insertar";
	}


?>
    <script>
        function Religion(form)
        {
            var selec = form.ReligionPracticante.options;
            var pertenece = document.getElementById('grupo');
            var grupo = document.getElementById('GrupoReligioso');
            var lugar = document.getElementById('LugarGrupo');
            var sacramento1 = document.getElementById('sacramento1');
            var sacramento2 = document.getElementById('sacramento2');
            var sacramento3 = document.getElementById('sacramento3');
            var sacramento4 = document.getElementById('sacramento4');
            var sacramento5 = document.getElementById('sacramento5');
            var sacramento6 = document.getElementById('sacramento6');
            var sacramento7 = document.getElementById('sacramento7');                        
            
            if (selec[1].selected == true || selec[2].selected == true)
            {                
                grupo.value = "";
                lugar.value = "";
                pertenece. disabled = true;
                grupo.disabled = true;
                lugar.disabled = true;
                sacramento1.disabled = true;
                sacramento2.disabled = true;
                sacramento3.disabled = true;
                sacramento4.disabled = true;
                sacramento5.disabled = true;
                sacramento6.disabled = true;
                sacramento7.disabled = true;
                
            }
            else
            {               
                grupo.disabled = false;
                lugar.disabled = false;
                pertenece.disabled = false;
                sacramento1.disabled = false;
                sacramento2.disabled = false;
                sacramento3.disabled = false;
                sacramento4.disabled = false;
                sacramento5.disabled = false;
                sacramento6.disabled = false;
                sacramento7.disabled = false;
            }
           
        }
        
        function Grupo(form)
        {
            var selec = form.grupo.options;
            var grupo = document.getElementById('GrupoReligioso');
            var lugar = document.getElementById('LugarGrupo');
                     
            if (selec[0].selected == true)
            {   
                grupo.value = "";
                lugar.value = "";
                
                grupo.disabled = false;
                lugar.disabled = false;
                
            }
            
            if (selec[1].selected == true)
            {                
                grupo.value = "";
                lugar.value = "";
                
                grupo.disabled = true;
                lugar.disabled = true;
            }
            

           
        }        
            
    </script>
	<form action="Joven/DatosReligiososABM.php" method="POST">
		<?php
		if(isset($_GET['dr'])) echo "<input type='hidden' name='IdDr' value='$datoslista[0]' />";
        
        if(isset($_GET['dj']))
        echo "<input type='hidden' name='dj' id='dj' value='".$_GET['dj']."' />"
		?>
		<table  class='Datos' style="padding: 10px; width: 100%;">
            <tr>
                <th colspan="2" style="text-align: center;"><h2>Informaci&oacute;n Religiosos</h2>
                <hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%;" />
                </th>
            </tr> 
			<tr>
				<td style='text-align: right; width:250px'> <label> Religi&oacute;n Practicante: </label></td>
				<td style=' padding-left: 3%; color: red;'>
                <select id="ReligionPracticante" name="ReligionPracticante" onChange="Religion(this.form)">
                        <?php
	                           $array = array(
                                        "Cristiano Catolico" => "Cristiano Catolico", 
                                        "Cristiano Evangelico" => "Cristiano Evangelico",                                                                                                                     
                                        "No Pratico" => "No Pratico");
                               
                               foreach ($array as $i => $value) {
                                    if($i == $datoslista[1])
                                        echo "<option Selected='true' value='$i'>$i</option>";
                                        
                                    else
                                        echo "<option value='$i'>$i</option>";
                                }
                                
                               if($datoslista[1] == "Cristiano Evangelico" || $datoslista[1] == "No Pratico")
                                    $disable = "disabled='true'";
                               else
                                    $disable = "";
                        ?>
                </select>
                </td>
			</tr>
            
			<tr>
				<td style='text-align: right; width:250px'><label>Sacramentos: </label></td>
				<td style=' padding-left: 3%;' >
<?php
	                           
                               
	                           $array = array(
                                        "Bautismo" => "Bautismo", 
                                        "Confesion" => "Confesion",
                                        "Primera Comunion" => "Primera Comunion", 
                                        "Confirmación" => "Confirmación", 
                                        "Matrimonio" => "Matrimonio", 
                                        "Unción de los enfermos" => "Unción de los enfermos", 
                                        "Orden Sacerdotal" => "Orden Sacerdotal",                                                                                                                                                                                                     
                                        );
                               $j = 1;
                               foreach ($array as $i => $value) { 
                               $id = "sacramento" . $j;
                               
                                    if(strpos($datoslista[2], $i))
                                        echo "<input type='checkbox' $disable id='$id' checked='true' name='Sacramentos[]' value='$i'/>$i<br />";
                                        
                                    else
                                        echo "<input type='checkbox' $disable id='$id' name='Sacramentos[]' value='$i'/>$i<br />";
                               $j++;
                               }
                                
                                
?>                   	
                </td>
			</tr>
			<tr>
			    <td style='text-align: right; width:250px'><label>Perteneces a algun Grupo Religioso</label></td>
				<td style=' padding-left: 3%; color: red;'>
                    <select size="1" id="grupo"  onChange="Grupo(this.form)" <?php echo $disable?>>
                    	<option value="Si">Si</option>
                    	<option value="No">No</option>
                    </select>
                </td>
			</tr>            
			<tr>
			    <td style='text-align: right; width:250px'><label> Grupo Relig&iacute;oso: </label></td>
				<td style=' padding-left: 3%; color: red;'><input <?php echo $disable?> type="text" id="GrupoReligioso" name="GrupoReligioso" value="<?php echo $datoslista[3] ?>"/></td>
			</tr>
			<tr>
				<td style='text-align: right; width:250px'><label>Lugar del Grupo: </label></td>
				<td style=' padding-left: 3%; color: red;'><input <?php echo $disable?> type="text" id="LugarGrupo" name="LugarGrupo" value="<?php echo $datoslista[4] ?>"/></td>
			</tr>
				
				<tr>
					<td colspan="2" style="text-align: center;"><input class="boton" type="submit" value="<?php echo $abm ?>" name="enviarReligioso" /></td>
				</tr>
			</table>
</form>
</div>
<div class="clr"></div> 