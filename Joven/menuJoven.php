<style type="text/css">
#menujoven {
	width: 12em;
	padding: 0 0 1em 0;
	margin-bottom: 1em;
	font:normal 12px/1.8em Comic Sans MS;
	color: #333;
    border-radius: 10px;
}

#menunombre {
	width: 12em;
	padding: 0 0 1em 0;
	margin-bottom: 1em;
	font:normal 12px/1.8em Comic Sans MS;
	color: #2851F7;
    border-radius: 10px;
}

#menujoven ul {
	list-style: none;
	margin: 0;
	padding: 0;
	border: none;
    border-radius: 10px;
}
	
#menujoven li {
	border-bottom: 1px solid #90bade;
	margin: 0;
	list-style: none;
	list-style-image: none;
    border-radius: 10px;
}
	
#menujoven li a {
	display: block;
	padding: 5px 5px 5px 0.5em;
	border-left: 10px solid #1958b7;
	border-right: 10px solid #508fc4;
	background-color: #2175bc;
	color: #fff;
	text-decoration: none;
	width: 100%;
    border-radius: 10px;
}
#menujoven li cuadro {
	display: block;
	padding: 5px 5px 5px 0.5em;
	border-left: 10px solid #1958b7;
	border-right: 10px solid #508fc4;
	background-color: #2175bc;
	color: #fff;
	text-decoration: none;
	width: 100%;
    border-radius: 10px;
}
html>body #menujoven li a {
	width: auto;
    border-radius: 10px;
}

#menujoven li a:hover {
	border-left: 10px solid #1c64d1;
	border-right: 10px solid #5ba3e0;
	background-color: #2586d7;
	color: #fff;
    border-radius: 10px;
}
</style>

<?php
    if(isset($_GET['dj']))
    {   
    $idp = $_GET['dj'];
    $url = "&dj=" . $idp;    
    }        
    else
    {
    $idp = ""; 
    $url = "";
    }
    if($_SESSION["TipoUsuario"]== "Administrador" || $_SESSION["TipoUsuario"]=="Buscador" || $_SESSION["TipoUsuario"]=="Coordinador" ) {$IdJoven=$idp;} else{$IdJoven=$_SESSION["IdUsuario"];}//Si un administrador est� usando el sistema, el nombre
    //del joven y su usuario se obtienen a trav�s de su id con $idp, si no, los datos se obtienen a trav�s del id de la sesi�n activa.
    $nombrealumno = $bdd->prepare("SELECT Nombre, Apellido1, Apellido2 from joven where IdUsuario = '$IdJoven'");
    $nombrealumno->execute();
    $nombreusuario = $bdd->prepare("SELECT NombreUsuario from usuario where Id = '$IdJoven'");
    $nombreusuario->execute();
    
?>

<div id="menujoven" style="border-radius: 10px;">
  <ul>
    <li><?php  $datos=$nombrealumno->fetch(); echo "<cuadro>$datos[0]<br>$datos[1] $datos[2]</cuadro>"; ?></li>  
    <li>
        
            <form method='POST' action='Main.php?l=UsuPer' name='UsuaPerfil'>
            <?php 
                $datos=$nombreusuario->fetch();
                if($_SESSION["TipoUsuario"]=="Administrador" || $_SESSION["TipoUsuario"]=="Buscador" || $_SESSION["TipoUsuario"]=="Coordinador"  )
                {
                        echo "<input type='hidden' name='idUsu' id='idUsu' value='$IdJoven' />
                        <a href='#' onclick='javascript:document.UsuaPerfil.submit();'>Datos del Usuario:<br>$datos[0]</a>";//Hacer submit con un enlace para redireccionar hacia el perfil del usuario
                        /*echo " <input type='hidden' name='idUsu' id='idUsu' value='$idp' />                               //correspondiente al joven.
                                <input  type='submit' value='Usuario: $datos[0]'/>";*/
                }
                else
                {
                        echo "<cuadro>Usuario:<br>$datos[0]</cuadro>";
                }
                                                
            ?>
            </form>
        
    </li>
    <li><a href="Main.php?l=joven<?php echo $url?>"   >Datos Personales</a></li>
    <li><a href="Main.php?l=jovenDfl<?php echo $url?>">Datos Familiares</a></li>
    <li><a href="Main.php?l=jovenDll<?php echo $url?>">Datos Laborales</a></li>
    <li><a href="Main.php?l=jovenDsl<?php echo $url?>">Datos de Salud</a></li>
    <li><a href="Main.php?l=jovenDrl<?php echo $url?>">Datos Religiosos</a></li>
  </ul>
</div>