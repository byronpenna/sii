<?php

/**
 * @author Lili 
 * @copyright 2016
 */

$query = "SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo
            FROM CargosAsignacion AS ca
            INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
            INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE ca.IdEmpleado = '".$_SESSION["IdUsuario"]."'  and ca.FechaFin = '0000-00-00'";

$MisCargos = $bddr->prepare($query);
$MisCargos->execute();
$DataC = $MisCargos->fetch();

?>
	<div style="float:right; width: 75%; text-align: left; background-color: white; padding:10px">
    		<h3>CUESTIONARIO DETECCI&Oacute;N DE NECESIDADES (DNC)</h3>
		<p>
			El presente cuestionario tiene como finalidad identificar los requerimientos en materia de capacitaci&oacute;n de cada uno de los miembros de la Fundaci&oacute;n; por lo que agradeceremos conteste con veracidad los datos que se le solicitan, considerado que cada uno de ellos tiene especial importancia permiti&eacute;ndonos conocer su perfil de crecimiento personal y profesional, y que estos se proyecten en el Programa de Capacitaci&oacute;n para cubrir las necesidades del personal y de la fundaci&oacute;n.
		</p>
	<form>
		<!--Seccion 1 del formulario de RRH -->
		<h3>I. Parte. INFORMACI&Oacute;N DE CRECIMIENTO PERSONAL Y PROFESIONAL</h3>
		<hr> 
		<h4>Antig&#252;edad en la instituci&oacute;n:</h4>
		<label for="antigüedad">A&#241;o: </label> <input type="text" placeholder=" a&#241;o" size="5" required /> <label for="meses">Meses: </label> <input type="text" placeholder=" meses" size="8" required /><br/>
		<br/>
		<span>
			Mencione los puestos de trabajo que ha ocupado hasta la actualidad en la fundaci&oacute;n y los a&#241;os calendario de servicio en cada uno de ellos.
		</span><br/>
		<br/>
		<label for="puesto 1">Puesto 1: </label> <input type="text" placeholder=" puesto de trabajo 1" size="40" /> <label for="años">A&#241;os: </label><input type="text" placeholder=" a&#241;os" size="5"/><br/>
		<label for="puesto 2">Puesto 2: </label> <input type="text" placeholder=" puesto de trabajo 2" size="40" /> <label for="años">A&#241;os: </label><input type="text" placeholder=" a&#241;os" size="5" /><br/>
		<label for="puesto 3">Puesto 3: </label> <input type="text" placeholder=" puesto de trabajo 3" size="40" /> <label for="años">A&#241;os: </label><input type="text" placeholder=" a&#241;os" size="5" /><br/><br/>
		<span>&#191;Conoce el perfil y descripci&oacute;n de su puesto laboral?</span>
		<select>
			<option value="si">S&iacute;</option>
			<option value="no">No</option>
		</select><br/>
		<br/>
		<span>
			Indique a continuaci&oacute;n todos los cursos de capacitaci&oacute;n en que Ud. ha participado. Pueden ser cursos organizados por la fundaci&oacute;n o cualquier instituci&oacute;n siempre que tengan relaci&oacute;n con su puesto de trabajo, crecimiento personal y los haya recibido en tiempo laboral. Si no ha participado en un curso, escriba ninguno.
		</span><br/>
		<br/>
		<!--Campo para el desarrollo personal de RRHH-->
		<table border="1">
			<thead><strong>En relaciones Humanas en el trabajo. (Desarrollo personal)</strong></thead>
				<tr>
					<td>Nombre del curso</td>
					<td>Tiempo de Duraci&oacute;n</td>
					<td>Organizado por</td>
					<td>Financiado por</td>
					<td>Especifique</td>
					<!--
					<td id="otros">Descripci&oacute;n</td>
					-->
				</tr>
				
				<tr class="row_to_clone">
					
					<td><input type="text" name="curso1" size="19px" placeholder="nombre curso" /></td>
					<td><input type="text" name="tiempo1" size="15px"  placeholder="tiempo"/></td>
					<td><input type="text" name="organizador1" placeholder="organizador" size="13px"/></td>
					
					<td>
						<select>
							<option value="fusalmo">FUSALMO</option>	
							<option value="gestion" id="gestion">Gesti&oacute;n Fusalmo</option>
							<option value="personal">Personal</option>
							<option value="otras" id="otrasOpc">Otras instituciones</option> 	
							
								
						</select>
					</td>
					<td><input type="text" name="otrasIns" placeholder="instituci&oacute;n" size="10px" /></td>

						<!--Seleccionando otras opciones
					<td id="otrasDesc"><input type="text" name="descripci&oacute;n"></td>
					-->
				</tr>
				
		</table>
		
		<tr>
		<td><input onclick="addRow(); return false;" type="button"  class="boton" name="generar" value="Agregar cursos" style="text-align: center"></td>
		</tr>
		
		<br/>
		<br/>
			<!--Conocimientos técnicos-->
		<table border="1">
			<thead><strong>Conocimientos T&eacute;cnicos (Desarrollo Profesional)</strong></thead>
			<tr>
				<td>Nombre del curso</td>
				<td>A&#241;os</td>
				<td>Organizado por</td>
				<td>Duraci&oacute;n en horas</td>
			</tr>
			
			<tr class="row_to_clone2">
				
				<td><input type="text" name="curso2" size="15" placeholder="nombre curso" /></td>
				<td><input type="text" name="tiempo2" placeholder="tiempo duraci&oacute;n" size="12" /></td>
				<td><input type="text" name="organizador2" placeholder="organizador"/></td>
				<td><input type="text" name="duracion2" placeholder="duraci&oacute;n hrs" /></td>

			</tr>
		</table>

			<tr>
		<td><input onclick="addRow2(); return false;" type="button"  class="boton" name="generar" value="Agregar cursos" style="text-align: center"></td>
		</tr>

			<!--Seccion 2 del formulario de RRH -->
		<h3>II. Parte. LEVANTAMIENTOS DE NECESIDADES DE CAPACITACI&Oacute;N</h3>
		<hr> 
		<span>
		De acuerdo a las funciones que realiza usted y el personal a su cargo indique los temas que considera sean tomados en cuenta para la mejora de su desempe&#241;o de trabajo (en orden de prioridad).
		</span><br/>
		<br/>
		
		<table border="1">
			<thead><strong>En Relaciones Humanas en el trabajo (Desarrollo Personal).</strong></thead>
				<tr >
					
					<td>Nombre del curso</td>
				</tr>
				<tr class="row_to_clone3">
					
					<td><input type="text" name="curso3" placeholder="ingrese el nombre del curso" /></td>
		</table>

		<tr>
		<td><input onclick="addRow3(); return false;" type="button"  class="boton" name="generar" value="Agregar cursos" style="text-align: center"></td>
		</tr>
		<br/>
		<br/>
		
		<table border="1" >
			<thead><strong>Conocimientos T&eacute;cnicos (Desarrollo Personal)</strong></thead>
				<tr>
					
					<td>Nombre del curso</td>
				</tr>
				<tr class="row_to_clone4">
					
					<td><input type="text" name="curso4" placeholder="ingrese el nombre del curso" /></td>
		</table>

		<tr>
		<td><input onclick="addRow4(); return false;" type="button"  class="boton" name="generar" value="Agregar cursos" style="text-align: center"></td>
		</tr>
	</form>
<br/>
<center>
	<input type="submit" class="boton" name="Enviar" value="Enviar" style="text-align: center"></td>	
</center>

</div>



<script type="text/javascript">

function addRow() {
    /* Declare variables */
    var elements, templateRow, rowCount, row, className, newRow, element;
    var i, s, t;
    
    /* Get and count all "tr" elements with class="row".    The last one will
     * be serve as a template. */
    if (!document.getElementsByTagName)
        return false; /* DOM not supported */
    elements = document.getElementsByTagName("tr");
    templateRow = null;
    rowCount = 0;
    for (i = 0; i < elements.length; i++) {
        row = elements.item(i);
        
        /* Get the "class" attribute of the row. */
        className = null;
        if (row.getAttribute)
            className = row.getAttribute('class')
        if (className == null && row.attributes) {    // MSIE 5
            /* getAttribute('class') always returns null on MSIE 5, and
             * row.attributes doesn't work on Firefox 1.0.    Go figure. */
            className = row.attributes['class'];
            if (className && typeof(className) == 'object' && className.value) {
                // MSIE 6
                className = className.value;
            }
        } 
        
        /* This is not one of the rows we're looking for.    Move along. */
        if (className != "row_to_clone")
            continue;
        
        /* This *is* a row we're looking for. */
        templateRow = row;
        rowCount++;
    }
    if (templateRow == null)
        return false; /* Couldn't find a template row. */
    
    /* Make a copy of the template row */
    newRow = templateRow.cloneNode(true);

    /* Change the form variables e.g. price[x] -> price[rowCount] */
    elements = newRow.getElementsByTagName("TEXTAREA");
    for (i = 0; i < elements.length; i++) {
        element = elements.item(i);
        s = null;
        s = element.getAttribute("name");
        if (s == null)
            continue;
        t = s.split("[");
        if (t.length < 2)
            continue;
        s = t[0] + "[" + rowCount.toString() + "]";
        element.setAttribute("name", s);
        element.value = "";
    }
    
    /* Add the newly-created row to the table */
    templateRow.parentNode.appendChild(newRow);
    return true;
}

</script>


<script type="text/javascript">
	
function addRow2() {
    /* Declare variables */
    var elements, templateRow, rowCount, row, className, newRow, element;
    var i, s, t;
    
    /* Get and count all "tr" elements with class="row".    The last one will
     * be serve as a template. */
    if (!document.getElementsByTagName)
        return false; /* DOM not supported */
    elements = document.getElementsByTagName("tr");
    templateRow = null;
    rowCount = 0;
    for (i = 0; i < elements.length; i++) {
        row = elements.item(i);
        
        /* Get the "class" attribute of the row. */
        className = null;
        if (row.getAttribute)
            className = row.getAttribute('class')
        if (className == null && row.attributes) {    // MSIE 5
            /* getAttribute('class') always returns null on MSIE 5, and
             * row.attributes doesn't work on Firefox 1.0.    Go figure. */
            className = row.attributes['class'];
            if (className && typeof(className) == 'object' && className.value) {
                // MSIE 6
                className = className.value;
            }
        } 
        
        /* This is not one of the rows we're looking for.    Move along. */
        if (className != "row_to_clone2")
            continue;
        
        /* This *is* a row we're looking for. */
        templateRow = row;
        rowCount++;
    }
    if (templateRow == null)
        return false; /* Couldn't find a template row. */
    
    /* Make a copy of the template row */
    newRow = templateRow.cloneNode(true);

    /* Change the form variables e.g. price[x] -> price[rowCount] */
    elements = newRow.getElementsByTagName("TEXTAREA");
    for (i = 0; i < elements.length; i++) {
        element = elements.item(i);
        s = null;
        s = element.getAttribute("name");
        if (s == null)
            continue;
        t = s.split("[");
        if (t.length < 2)
            continue;
        s = t[0] + "[" + rowCount.toString() + "]";
        element.setAttribute("name", s);
        element.value = "";
    }
    
    /* Add the newly-created row to the table */
    templateRow.parentNode.appendChild(newRow);
    return true;
}


</script>

<script type="text/javascript">
	
	function addRow3() {
    /* Declare variables */
    var elements, templateRow, rowCount, row, className, newRow, element;
    var i, s, t;
    
    /* Get and count all "tr" elements with class="row".    The last one will
     * be serve as a template. */
    if (!document.getElementsByTagName)
        return false; /* DOM not supported */
    elements = document.getElementsByTagName("tr");
    templateRow = null;
    rowCount = 0;
    for (i = 0; i < elements.length; i++) {
        row = elements.item(i);
        
        /* Get the "class" attribute of the row. */
        className = null;
        if (row.getAttribute)
            className = row.getAttribute('class')
        if (className == null && row.attributes) {    // MSIE 5
            /* getAttribute('class') always returns null on MSIE 5, and
             * row.attributes doesn't work on Firefox 1.0.    Go figure. */
            className = row.attributes['class'];
            if (className && typeof(className) == 'object' && className.value) {
                // MSIE 6
                className = className.value;
            }
        } 
        
        /* This is not one of the rows we're looking for.    Move along. */
        if (className != "row_to_clone3")
            continue;
        
        /* This *is* a row we're looking for. */
        templateRow = row;
        rowCount++;
    }
    if (templateRow == null)
        return false; /* Couldn't find a template row. */
    
    /* Make a copy of the template row */
    newRow = templateRow.cloneNode(true);

    /* Change the form variables e.g. price[x] -> price[rowCount] */
    elements = newRow.getElementsByTagName("TEXTAREA");
    for (i = 0; i < elements.length; i++) {
        element = elements.item(i);
        s = null;
        s = element.getAttribute("name");
        if (s == null)
            continue;
        t = s.split("[");
        if (t.length < 2)
            continue;
        s = t[0] + "[" + rowCount.toString() + "]";
        element.setAttribute("name", s);
        element.value = "";
    }
    
    /* Add the newly-created row to the table */
    templateRow.parentNode.appendChild(newRow);
    return true;
}

</script>


<script type="text/javascript">
	
	function addRow4() {
    /* Declare variables */
    var elements, templateRow, rowCount, row, className, newRow, element;
    var i, s, t;
    
    /* Get and count all "tr" elements with class="row".    The last one will
     * be serve as a template. */
    if (!document.getElementsByTagName)
        return false; /* DOM not supported */
    elements = document.getElementsByTagName("tr");
    templateRow = null;
    rowCount = 0;
    for (i = 0; i < elements.length; i++) {
        row = elements.item(i);
        
        /* Get the "class" attribute of the row. */
        className = null;
        if (row.getAttribute)
            className = row.getAttribute('class')
        if (className == null && row.attributes) {    // MSIE 5
            /* getAttribute('class') always returns null on MSIE 5, and
             * row.attributes doesn't work on Firefox 1.0.    Go figure. */
            className = row.attributes['class'];
            if (className && typeof(className) == 'object' && className.value) {
                // MSIE 6
                className = className.value;
            }
        } 
        
        /* This is not one of the rows we're looking for.    Move along. */
        if (className != "row_to_clone4")
            continue;
        
        /* This *is* a row we're looking for. */
        templateRow = row;
        rowCount++;
    }
    if (templateRow == null)
        return false; /* Couldn't find a template row. */
    
    /* Make a copy of the template row */
    newRow = templateRow.cloneNode(true);

    /* Change the form variables e.g. price[x] -> price[rowCount] */
    elements = newRow.getElementsByTagName("TEXTAREA");
    for (i = 0; i < elements.length; i++) {
        element = elements.item(i);
        s = null;
        s = element.getAttribute("name");
        if (s == null)
            continue;
        t = s.split("[");
        if (t.length < 2)
            continue;
        s = t[0] + "[" + rowCount.toString() + "]";
        element.setAttribute("name", s);
        element.value = "";
    }
    
    /* Add the newly-created row to the table */
    templateRow.parentNode.appendChild(newRow);
    return true;
}

</script>

</body>