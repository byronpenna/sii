<?php
	session_start();
    require '../../net.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Ficha del Empleado</title>
</head>
<script >
function cerrar() { setTimeout(window.close,1500); }
</script>
<body onload="window.print();cerrar();" style="font-family: Calibri; font-size: small;">
<div style="width: 94%; margin-left: 3%;">
<?php
    
//    if(isset($_POST['$IdEmp']))
    

    $DatosEmpleado = $bddr->prepare("SELECT * FROM Empleado 
                                     where IdEmpleado ='$IdEmp' ");
    $DatosEmpleado->execute();
    $Empleado = $DatosEmpleado->fetch();
    
    $RutaImagen = $bddr->prepare("SELECT * FROM Fotografia where IdEmpleado = '$IdEmp' ");
    $RutaImagen->execute();
    
    if($RutaImagen->rowCount() > 0)
    {
        $Foto = $RutaImagen->fetch();
        $Img = $Foto[2];
    }
    else
    {
       if($Empleado[12] == "M")$Img = "h.jpg";
       else $Img = "m.jpg";   
    }
    
    
?>
    <table style="width: 100%; padding: 10px;">
        <!-- Ficha del Empleado --!>
        <tr><td colspan="2"> <h2>Fundaci�n Salvador del Mundo<br />FUSALMO</h2></td>
            <th colspan="2" rowspan="8" style="width: 50%;">
                <img src="../Fotos/<?php echo $Img?>" width="200" height="250" style="border-radius: 10px; margin-top: 10px;" />
            </th>
        <tr><th colspan="2"><h2 style="font-family: Calibri;">Ficha de Empleado</h2><hr color='#69ACD7' /></th></tr>
        <tr><td style="width: auto;">Nombre:</td>
            <td style="padding-left: 20px; width: 50%;"><?php echo $Empleado[4] . " " . $Empleado[5] . ", " . $Empleado[1] . " " . $Empleado[2] . " " .$Empleado[3]?></td>
        </tr>
        
        <?php
	
                $CargosArea = $bddr->prepare("SELECT A.IdAreaDeTrabajo, A.NombreAreaDeTrabajo, C.IdCargos, C.Cargo
                                                 FROM CargosAsignacion AS CA
                                                 INNER JOIN Cargos AS C ON C.IdCargos = CA.IdCargo
                                                 INNER JOIN AreasDeTrabajo AS A ON A.IdAreaDeTrabajo = C.IdArea_Fk 
                                                 where CA.IdEmpleado = '" . $_POST['IdEmp'] ."' and CA.FechaFin = '0000-00-00'");
                $CargosArea->execute();
                  
                
                if($CargosArea->rowCount() > 0)
                {
                    $DatosCargos = $CargosArea->fetch(); 
                    $cargo = $DatosCargos[3];
                    $area = $DatosCargos[1];
                }
                else
                {
                    $cargo = "<em style='color: red;'>No asignado</em>";
                    $area = $cargo;
                }
        ?>
        
        <tr><td>Cargo: </td><td style="padding-left: 20px;"><?php echo $cargo?></td></tr>
        <tr><td>Area:  </td><td style="padding-left: 20px;"><?php echo $area?></td></tr>
        
        <?php
	           if($Empleado[12] == "M")$sexo = "Hombre";
               else                    $sexo = "Mujer";   
               
               
               $edad = $bddr->prepare("SELECT TIMESTAMPDIFF( YEAR,  '$Empleado[11]', NOW( ) ) AS Edad");
               $edad->execute();
               $datoEdad = $edad->fetch();            
        ?>
        <tr><td>Sexo:  </td><td style="padding-left: 20px;"><?php echo $sexo?></td></tr>
        <tr><td>Edad:  </td><td style="padding-left: 20px;"><?php echo $datoEdad[0]?></td></tr>
        
        
        <tr><td colspan="4"></td></tr>        
        <!-- Datos Personales --!>
        <?php
	           if(substr($Empleado[11],5,2) == "01") $mes = "Enero";
               if(substr($Empleado[11],5,2) == "02") $mes = "Febrero";
               if(substr($Empleado[11],5,2) == "03") $mes = "Marzo";
               if(substr($Empleado[11],5,2) == "04") $mes = "Abril";
               if(substr($Empleado[11],5,2) == "05") $mes = "Mayo";
               if(substr($Empleado[11],5,2) == "06") $mes = "Junio";
               if(substr($Empleado[11],5,2) == "07") $mes = "Julio";
               if(substr($Empleado[11],5,2) == "08") $mes = "Agosto";
               if(substr($Empleado[11],5,2) == "09") $mes = "Septiembre";
               if(substr($Empleado[11],5,2) == "10") $mes = "Octubre";
               if(substr($Empleado[11],5,2) == "11") $mes = "Noviembre";
               if(substr($Empleado[11],5,2) == "12") $mes = "Diciembre"; 
               
               $Religion = $bddr->prepare("SELECT * from DatosReligiosos where IdEmpleado_Fk = '" . $_POST['IdE'] ."'");
               $Religion->execute();
               $Resultado = $Religion->fetch();                 
        ?>
        <tr>
            <td colspan="4">
                <table style="width: 100%;">
                    
                    <tr style="padding-top: 20px;"><td colspan="3" style="padding-left: 20px;" ><h2 style="font-family: Calibri;">Datos Personales</h2></td></tr>
                            <tr><td colspan="3"><hr color='#69ACD7' /></td></tr>
                            <tr><td>Fecha de Nacimiento:</td><td colspan="2"><?php echo substr($Empleado[11],8,2) . " de $mes del " . substr($Empleado[11],0,4)?></td></tr>
                            <tr><td>Lugar de Residencia:</td><td colspan="2"><?php echo $Empleado[6] . ", " . $Empleado[7] . ", " . $Empleado[8]?></td></tr>
                            <tr><td>Nacionalidad:  </td><td ><?php echo $Empleado[14]?></td></tr>
                            <tr><td>Telefono:</td><td colspan="2"><?php echo $Empleado[9] . " / " . $Empleado[10]?></td></tr>        
                            <tr><td>Estado Civil:</td><td colspan="2"><?php echo $Empleado[13]?></td></tr>
                            <tr><td>Empresa de AFP:</td><td colspan="2"><?php echo $Empleado[18]?></td></tr>                            
                            <tr><td>NUP:</td><td colspan="2"><?php echo $Empleado[19]?></td></tr>
                            <tr><td>ISSS:</td><td colspan="2"><?php echo $Empleado[20]?></td></tr>
                            <tr><td>NIT:</td><td colspan="2"><?php echo $Empleado[21]?></td></tr>
                            <tr><td>Correo:</td><td colspan="2"><?php echo $Empleado[17]?></td></tr>
                            <tr><td>Religi�n:</td><td colspan="2"><?php echo $Resultado[1]?></td></tr>
                            
                    <?php
                            if($Resultado[1] == "Cristiano Cat�lico")
                            {
                                echo "<tr><td>Sacramentos Realizados:</td><td  colspan='2'><ul style='padding-left: 20px'>";
                                
                                if($Resultado[4] != "") echo "<li>$Resultado[4]</li>";
                                if($Resultado[5] != "") echo "<li>$Resultado[5]</li>";
                                if($Resultado[6] != "") echo "<li>$Resultado[6]</li>";
                                if($Resultado[7] != "") echo "<li>$Resultado[7]</li>";
                                if($Resultado[8] != "") echo "<li>$Resultado[8]</li>";
                                if($Resultado[9] != "") echo "<li>$Resultado[9]</li>";
                                if($Resultado[10] != "") echo "<li>$Resultado[10]</li>";
                                
                                echo "</td></tr>";
                            }
                    ?>   
                    

                    <!-- Datos FUSALMO --!>
                    <tr style="padding-top: 20px;"><td colspan="3" style="padding-left: 20px;" ><h2 style="font-family: Calibri;">Datos en FUSALMO</h2></td></tr>
                    <tr><td colspan="3"><hr color='#69ACD7' /></td></tr>
                    
                    <?php
                            $CargosArea = $bddr->prepare("SELECT CA . * , A.IdAreaDeTrabajo, A.NombreAreaDeTrabajo, C.IdCargos, C.Cargo
                                                          FROM CargosAsignacion AS CA
                                                          INNER JOIN Cargos AS C ON C.IdCargos = CA.IdCargo
                                                          INNER JOIN AreasDeTrabajo AS A ON A.IdAreaDeTrabajo = C.IdArea_Fk
                                                          where CA.IdEmpleado = '" . $_POST['IdE'] ."' 
                                                          ORDER BY CA.FechaFin ASC");
                            $CargosArea->execute();
                              
                            
                            if($CargosArea->rowCount() > 0)
                            {
                                
                                $i = 0;
                                
                                while($DatosCargos = $CargosArea->fetch())
                                {  
                                    if($i == 0)
                                        $color = "blue";
                                    
                                    else
                                        $color = "red";
                                        
                                    $i++;
                                    
                                    
                                    if($DatosCargos[7] == "0000-00-00")
                                        $fecha = "Continua Actualmente en el Cargo";
                                    
                                    else
                                        $fecha = $DatosCargos[7];
                                    
                                    echo "<tr><td>Cargo:</td><td colspan='2' style='color: $color;'> $DatosCargos[11]</td></tr>
                                          <tr><td>Area:</td><td colspan='2' style='color: $color;'> $DatosCargos[9]</td></tr>
                                          <tr><td>Tipo de Contrato:</td><td colspan='2' style='color: $color;'> $DatosCargos[3]</td></tr>
                                          <tr><td>Fuente de Financiamiento:</td><td colspan='2' style='color: $color;'> $DatosCargos[4]</td></tr>
                                          <tr><td>Salario:</td><td colspan='2' style='color: $color;'> $". number_format($DatosCargos[5],2)."</td></tr>
                                          <tr><td>Fecha de Inicio:</td><td colspan='2' style='color: $color;'> $DatosCargos[6]</td></tr>
                                          <tr><td>Fecha de Finalizaci�n:</td><td colspan='2' style='color: $color;'>$fecha</td></tr>        
                                          <tr><td colspan='2'>.</td></tr>";
                                }
                            }
                            else
                            {
                                echo "<tr><td>Cargo:</td><td colspan='2'><em style='color: red;'>No asignado</em></td></tr>";
                            }
                            
                            
                            	
                    ?>   
                                         
            <!-- Datos FUSALMO --!>
                    <tr style="padding-top: 20px;"><td colspan="3" style="padding-left: 20px;" ><br /><br /><br /><h2 style="font-family: Calibri;">Datos Familiares</h2></td></tr>
                    <tr><td colspan="3"><hr color='#69ACD7' /></td></tr>
                    <tr>
                        <td colspan="3">                
                            <?php
                                    
                                    $CargosArea = $bddr->prepare("SELECT * FROM DatosFamiliares
                                                                  where IdEmpleado_Fk = '" . $_POST['IdE'] ."'");
                                    $CargosArea->execute();                
            
                                       $direccion = "left";
                                       echo "<table style='width: 100%;' rules='all'>";
                                       
                                       if($CargosArea->rowCount() > 0)
                                       {
                                                                   
                                           while($DatosFamiliares = $CargosArea->fetch())
                	                       {
                	                           if($direccion == "left")
                	                               echo "<tr>";
                	                           
                	                           echo "<td style='width: 50%'>
                                                     <table style='width: 90%; margin-left: 5%'>
                                                        <tr><td style='width: 40%'>Nombre:</td><td style='width: 60%'>$DatosFamiliares[1] <br />$DatosFamiliares[2]</td></tr>
                                                        <tr><td>Parentesco:</td><td>$DatosFamiliares[3]</td></tr>
                                                        <tr><td>Telefono:</td><td>$DatosFamiliares[4]</td></tr>
                                                        <tr><td>Profesi�n:</td><td>$DatosFamiliares[5]</td></tr>
                                                        <tr><td>Lugar de Trabajo:</td><td>$DatosFamiliares[6]</td></tr>
                                                     </table>
                                                     </td>";
                                                     
                                               if($direccion == "left")
                                                    $direccion = "right";
                                               else
                                               {
                                                    $direccion = "left";
                                                    echo "</tr>";
                                               }
                                                                                                                      
                                           }
                                           }
                                       else
                                            echo "<td colspan='2' style='text-align: center; color: red'>No pose� datos Familiares</td>";
                                            
                                       echo "</table>";
                                ?>                        
                        </td>
                    </tr>
                    
                    
            
                    <!-- Datos Academicos --!>
                    <tr style="padding-top: 20px;"><td colspan="3" style="padding-left: 20px;" ><h2 style="font-family: Calibri;">Datos Acad�micos</h2></td></tr>
                    <tr><td colspan="3"><hr color='#69ACD7' /></td></tr>
                    <tr>
                        <td colspan="3">                
                            <?php
                                    
                                    $DatosAcademicos = $bddr->prepare("SELECT * FROM DatosAcademicos
                                                                  where IdEmpleado_Fk = '" . $_POST['IdE'] ."'");
                                    $DatosAcademicos->execute();                
            
                                       $direccion = "left";
                                       echo "<table style='width: 100%;' rules='all'>";
                                       
                                       if($DatosAcademicos->rowCount() > 0)
                                       {
                                                                   
                                           while($Academicos = $DatosAcademicos->fetch())
                	                       {
                	                           if($direccion == "left")
                	                               echo "<tr>";
                	                           
                	                           echo "<td style='width: 50%'>
                                                     <table style='width: 100%;'>
                                                        <tr><td style='width: 40%'>Institucion:</td><td style='width: 60%'>$Academicos[2]</td></tr>
                                                        <tr><td>Titulo o Certificado:</td><td>$Academicos[3]</td></tr>
                                                        <tr><td>A�o de Egreso:</td><td>$Academicos[8]</td></tr>
                                                     </table>
                                                     </td>";
                                                     
                                               if($direccion == "left")
                                                    $direccion = "right";
                                               else
                                               {
                                                    $direccion = "left";
                                                    echo "</tr>";
                                               }
                                                                                                                      
                                           }
                                           }
                                       else
                                            echo "<td colspan='2' style='text-align: center; color: red'>No pose� datos Academicos</td>";
                                            
                                       echo "</table>";
                                ?>                        
                        </td>
                    </tr>
                    
                    <!-- Datos Laborales --!>
                    <tr style="padding-top: 20px;"><td colspan="3" style="padding-left: 20px;" ><h2 style="font-family: Calibri;">Datos Laborales</h2></td></tr>
                    <tr><td colspan="3"><hr color='#69ACD7' /></td></tr>
                    <tr>
                        <td colspan="3">                
                            <?php
                                    
                                    $DatosLaborales = $bddr->prepare("SELECT * FROM DatosLaborales
                                                                       where IdEmpleado_Fk = '" . $_POST['IdE'] ."'");
                                    $DatosLaborales->execute();                
            
                                       $direccion = "left";
                                       echo "<table style='width: 100%;' rules='all'>";
                                       
                                       if($DatosLaborales->rowCount() > 0)
                                       {
                                                                   
                                           while($Laboral = $DatosLaborales->fetch())
                	                       {
                	                           echo "<tr>";
                	                           echo "<td style='width: 50%'>
                                                     <table style='width: 100%; padding-top: 20px'>
                                                        <tr><td style='width: 40%'>Lugar de Trabajo:</td><td style='width: 60%'>$Laboral[1]</td></tr>
                                                        <tr><td>Cargo:</td><td>$Laboral[2]</td></tr>
                                                        <tr><td>Fecha de Inicio:</td><td>$Laboral[3]</td></tr>
                                                        <tr><td>Fecha de Finalizaci�n:</td><td>$Laboral[4]</td></tr>
                                                        <tr><td>Contacto:</td><td>$Laboral[5]</td></tr>
                                                     </table>
                                                     </td>";
                                               echo "</tr>";                                                                       
                                           }
                                       }
                                       else
                                            echo "<td colspan='2' style='text-align: center; color: red'>No pose� datos Laborales</td>";
                                            
                                       echo "</table>";
                                ?>                        
                        </td>
                    </tr> 
                    <tr><td colspan="3"><br /><br /><hr color='#69ACD7' /><br /><br /></td></tr>
                    <tr><td><?php echo "________________<br />" . $Empleado[1] . " " . $Empleado[2] . " " .$Empleado[4] . " <br /> $cargo"?></td>
                        <td></td>
                        <td style="text-align: right;"><?php echo "________________<br /> Coordinaci�n de RRHH" ?></td></tr>        
                    <tr><td colspan="3"> <?php echo "<br /><br /><br />Fecha:" . date("Y-m-d");?></td></tr>                                
                </table>
            </td>
        </tr>        
    </table>
</div>
</body>
</html>