$(function(){

    $(document).keydown(function(event) { 
      if (event.keyCode == 27) { 
        modalClose();
      }
    });

document.getElementById("boton").onclick = function comparar(){  
    
         var ids= new Array();

       $("input:checkbox[name=idProducto]:checked").each(function(){
        ids.push($(this).val());
        });

        if (ids != "") 
        {
            modalOpen();
            var type =  document.getElementById("tipoRequi").value;
            var idreq =  document.getElementById("idRequi").value;
            document.getElementById("btnModalc").setAttribute("href", "#openModal");
    
            $.post("Empleados/Requisiciones/DataTables/tablaComparativaABM.php", {idProducto: ids, type: type, idreq: idreq}, 
                function(htmlexterno){
            $("#cargaexterna").html(htmlexterno);
                        });
        }else
            {
            alertify.error("<h3>Seleccione un Producto<h3>");
            }
    }        

document.getElementById("boton2").onclick =  function mostrarComparativas(){
    modalOpen();
    var mostrarcomparativas=1;
    var tipo = document.getElementById("tipoRequi").value;
    var idreq =  document.getElementById("idRequi").value;
   $.post("Empleados/Requisiciones/DataTables/tablaComparativaABM.php", {tipo: tipo, mostrarcomparativas: mostrarcomparativas, idreq: idreq}, 
            function(htmlexterno){
   $("#cargaexterna").html(htmlexterno);
        });
   
}

    document.getElementById("btnAtras").onclick =  function mostrarComparativas(){
        modalOpen();
        var mostrarcomparativas=1;
        var tipo = document.getElementById("tipoRequi").value;
        var idreq =  document.getElementById("idRequi").value;
       $.post("Empleados/Requisiciones/DataTables/tablaComparativaABM.php", {tipo: tipo, mostrarcomparativas: mostrarcomparativas, idreq: idreq}, 
                function(htmlexterno){
       $("#cargaexterna").html(htmlexterno);
            });
       var s = document.getElementById('btnAtras').style.display ='none';
    }

});


function guardarComparativa(){
    var guardar = 1;
    var cant = parseInt(document.getElementById("txtNume").value);
    var tipo = document.getElementById("tipopro").value;
    var idSublinea = document.getElementById("idSublinea").value;
    var nombreComparativa =  document.getElementById("nombreComparativa").value;
    var i=1;
    var x;
    var z=1;
    var p = 1;
    var producto = new Array();
    var seleccionproveedor = new Array();
    var bandera=0;
    var bandera2=0;
    var bandera3=0;
    var bandera4=0;
    if(nombreComparativa!=''){
      while(i <= cant){
        var cantidad = 'canti'+(i)+"";
        var can = 'canti'+(i)+"";
        var material = 'idmate'+(i)+"";
        var idmate = document.getElementById(material).value;
        var cantidad = document.getElementById(cantidad).value;
        producto.push(cantidad);
        producto.push(idmate);
        $("input:radio[name=proveedor"+(i)+"]:checked").each(function(){
            seleccionproveedor.push($(this).val());
            bandera3++;
        });

        if((cantidad == '' || cantidad < 1)){
            var s = document.getElementById(can).style.border = '1px solid #FF0000' ;
            bandera4++;
        }
        else{
                var s = document.getElementById(can).style.border = '1px solid green' ;        
            }

            //comparar seleccionproveedor con el proveedor 
        for (x = 0 ; x <= 2; x++){
            var unidad = 'txtUni'+(x+z)+"";
            var tot = 'txtTot'+(x+z)+"";   
            var prove = 'proveedor_'+(p)+"";
            var provee = document.getElementById(prove).value;
            var precio = document.getElementById(unidad).value;
            var total = precio * cantidad;

            if(provee == 'prove1' || provee == 'prove2' || provee == 'prove3'){
               // var s = document.getElementById(provee).style.border = '1px solid #FF0000' ;
                bandera2++;
            }
            else{
                //var s = document.getElementById(provee).style.border = '1px solid green' ;
                if((precio == '' || precio <= 0)){
                   var s = document.getElementById(unidad).style.border = '1px solid #FF0000' ;
                   bandera++;
                }
                else{
                    var s = document.getElementById(unidad).style.border = '1px solid green' ;
                        
                }
                
            }
            producto.push(precio);
            producto.push(total);
            producto.push(provee);
            p++;

            }
        z+=3;
        i++;
        p=1;
        producto.push("|");

      }

        if(bandera4==0){
            if(bandera2==0){
                    if(bandera==0){
                        if(bandera3==cant){
                       $.post("Empleados/Requisiciones/DataTables/tablaComparativaABM.php", {producto: producto, cantidad: cant, tipo: tipo, idSublinea: idSublinea, nombreComparativa: nombreComparativa, seleccion: seleccionproveedor, guardar: guardar}, 
                                function(htmlexterno){
                            if(htmlexterno == 'Comparativa '+nombreComparativa+' Guardada Correctamente'){
                                alertify.success('<h3>'+htmlexterno+'</h3>');
                                $('#boton2').trigger('click');
                            } 
                            else{
                              alertify.error(htmlexterno);
                            }
                            });  
                        }else{
                            alertify.error('<h3>Seleccione el Proveedor</h3>');
                        }
                    }else{
                            alertify.error('<h3>Ingrese Todos Los Precios Por Unidad</h3>');
                        }
                }else{
                    alertify.error('<h3>Seleccione 3 Proveedores</h3>');
                }
        }else{
                alertify.error('<h3>Ingrese La Cantidad de Producto</h3>');
            }
    }else{
        alertify.error('<h3>Ingrese el Nombre de la Comparativa</h3>');
    }
}

function modalOpen(){
    var s = document.getElementById('mdlcontent').setAttribute("class","modal open"); 
    var s = document.getElementById('_background').style.display = "block";
    var s = document.getElementById('_background').style.opacity = "1";
    var s = document.getElementById('cls').style.display = "block";
}

function modalClose(){
    var s = document.getElementById('mdlcontent').setAttribute("class","modal colsed"); 
    var s = document.getElementById('_background').style.display = "none";
    var s = document.getElementById('cerrarProve').setAttribute("class","modal-btn close");
    var s = document.getElementById('btnAtras').style.display ='none';
}

function modalCloseComparativo(){
    var s = document.getElementById('mdlComparative2').style.display ='none';
}

function modalOpenComparativo(){
    var s = document.getElementById('mdlComparative2').setAttribute("class","modal open"); 
    var s = document.getElementById('_background').style.display = "block";
    var s = document.getElementById('_background').style.opacity = "1";
     var s = document.getElementById('mdlComparative2').style.display ='block';
}


function modalOpenProveedor(){
    var s = document.getElementById('mdlProvider').setAttribute("class","modal open");
    var s = document.getElementById('cerrarProve').setAttribute("onclick","modalCloseProveedor()"); 
    var s = document.getElementById('cerrarProve').setAttribute("class","modal-btn hide");  
    var s = document.getElementById('_background').style.display = "block";
    var s = document.getElementById('_background').style.opacity = "1";
    $('#boton').trigger('click');

}
    function modalOpenProveedor2(){
    var s = document.getElementById('mdlProvider').setAttribute("class","modal open");
    var s = document.getElementById('cerrarProve').setAttribute("onclick","modalCloseProveedor()"); 
    var s = document.getElementById('cerrarProve').setAttribute("class","modal-btn hide");  
    var s = document.getElementById('_background').style.display = "block";
    var s = document.getElementById('_background').style.opacity = "1";
    var nombre = document.getElementById("nombreComparativa2").value;
    modificar(nombre);

}

function modalCloseProveedor(){
    var s = document.getElementById('mdlProvider').setAttribute("class","modal closed");
    //var s = document.getElementById('mdlProvider').style.display = "none";

}

function actualizarComparativa(){
    var actualizar = 1;
    var cant = parseInt(document.getElementById("txtNume").value);
    var tipo = document.getElementById("tipopro").value;
    var idSublinea = document.getElementById("idSublinea").value;
    var nombreComparativa =  document.getElementById("nombreComparativa").value;
    var i=1;
    var x;
    var z=1;
    var p = 1;
    var producto = new Array();
    var seleccionproveedor = new Array();
    var bandera=0;
    var bandera2=0;
    var bandera3=0;
      while(i <= cant){
        var cantidad = 'canti'+(i)+"";
        var material = 'idmate'+(i)+"";
        var idmate = document.getElementById(material).value;
        var cantidad = document.getElementById(cantidad).value;
        producto.push(cantidad);
        producto.push(idmate);
        $("input:radio[name=proveedor"+(i)+"]:checked").each(function(){
        seleccionproveedor.push($(this).val());
        bandera3++;
        });
            //comparar seleccionproveedor con el proveedor 
        for (x = 0 ; x <= 2; x++){
            var unidad = 'txtUni'+(x+z)+"";
            var tot = 'txtTot'+(x+z)+"";   
            var prove = 'proveedor_'+(p)+"";
            var provee = document.getElementById(prove).value;
            var precio = document.getElementById(unidad).value;
            var total = precio * cantidad;
            if(provee == 'prove1' || provee == 'prove2' || provee == 'prove3'){
               // var s = document.getElementById(provee).style.border = '1px solid #FF0000' ;
                bandera2++;
            }
            else{
                //var s = document.getElementById(provee).style.border = '1px solid green' ;
                if((precio == '' || precio <= 0)){
                   var s = document.getElementById(unidad).style.border = '1px solid #FF0000' ;
                   bandera++;
                }
                else{
                    var s = document.getElementById(unidad).style.border = '1px solid green' ;
                        
                }
                
            }
            producto.push(precio);
            producto.push(total);
            producto.push(provee);
            p++;

        }
        z+=3;
        i++;
        p=1;
        producto.push("|");

      }
    if(bandera2==0){
        if(bandera==0){
            if(bandera3==cant){
           $.post("Empleados/Requisiciones/DataTables/tablaComparativaABM.php", {producto: producto, cantidad: cant, tipo: tipo, idSublinea: idSublinea, nombreComparativa: nombreComparativa, seleccion: seleccionproveedor, actualizar: actualizar}, 
                    function(htmlexterno){
                if(htmlexterno == 'Comparativa Actualizada correctamente!'){
                    alertify.success('<h3>'+htmlexterno+'</h3>');
                }
                else{
                    alertify.error(htmlexterno);
                }
            });
            }else{
                alertify.error('<h3>Seleccione el Proveedor</h3>');
            }

        }else{
            alertify.error('<h3>Ingrese Todos Los Precios Por Unidad</h3>');
        }
    
    }else{
            alertify.error('<h3>Seleccione 3 Proveedores</h3>');
        }
}


function punto(event) {
  var x = event.which || event.keyCode;
      if(x==46 || x==101 || x ==43 || x==45){
        event.preventDefault();
    }
}

function punto2(event) {
  var x = event.which || event.keyCode;
      if( x==101 || x ==43 || x==45){
        event.preventDefault();
    }
}

function precio() {
  var cant = parseInt(document.getElementById("txtNume").value);
  var i=1;
  var x;
  var z=1;
  while(i <= cant){
    for (x = 0 ; x <= 2; x++){
      var cantidad = 'canti'+(i)+"";
      var can = 'canti'+(i)+"";
      var unidad = 'txtUni'+(x+z)+"";
      var tot = 'txtTot'+(x+z)+"";
      var precio = parseFloat(document.getElementById(unidad).value);
      var cantidad = parseInt(document.getElementById(cantidad).value);


      if(cantidad > 0){
             var total2 = precio * cantidad;
             var total = parseFloat((total2).toFixed(2));
             var s = document.getElementById(can).style.border = '1px solid green' ;
        }else{
            alertify.error('<h3>La Cantidad de Producto debe ser al menos 1</h3>');
             var s = document.getElementById(can).style.border = '1px solid #FF0000' ;
            break;
        }
      document.getElementById(tot).innerHTML = "<input class='ancho_cantidad' name='total_"+(x+z)+"'  style='width='10px;' type='text' value='"+total+"' disabled>";
    }
    z+=3;
    i++;
  }
} 

function modificar(name){
    var modificar = 1;
    var nombre = name;
    var tipo = document.getElementById("tipoRequi").value;
    var idreq =  document.getElementById("idRequi").value;
    var estado =  document.getElementById("estado2").value;
    //alert(estado);
    $.post("Empleados/Requisiciones/DataTables/tablaComparativaABM.php", {nombre: nombre, modificar: modificar, tipo: tipo, idreq: idreq, estado: estado}, 
            function(htmlexterno){
   $("#cargaexterna").html(htmlexterno);
        });
    var s = document.getElementById('btnAtras').style.display ='inline';
    document.getElementById("pdfcompa").value = 1;
    document.getElementById("nombrecompa").value = name;
    document.getElementById("idreque").value = idreq;
    document.getElementById("tiporequisi").value = tipo;


    var s = document.getElementById('btnAtras').setAttribute('onclick', 'mostrarComparativas()');

}





