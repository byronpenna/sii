<?php

/**
 * @author Lili Cordero
 * @copyright 2016
 */
              
$MisCargos = $bddr->prepare("SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo
                            FROM CargosAsignacion AS ca
                            INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                            INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
                            WHERE ca.IdEmpleado = '".$_SESSION["IdUsuario"]."'");      
                                                                  
$MisCargos->execute();

if(isset($_POST['MiCargo']))
    $IdCharge = $_POST['MiCargo'];

else
    $IdCharge = ''; 
    
$Cargos = "";
$Datos = $MisCargos->fetch();
?>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="2">M&oacute;dulo de Requisiciones</td></tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td style="width: 50%; text-align: left;"><a href="?l=proyectos"><input type="submit" value="<- Atras" name="Enviar" class="boton"  /></a></td>
            <td style="width: 50%; text-align: right;"><h2>Proyectos</h2></td>
        </tr>
        <tr>
            <td colspan="2"><hr color='skyblue' /></td>
        </tr>

<?php
        	$Accion = $bddr->prepare("SELECT * FROM Request_Proyecto where IdProyecto = " . $_POST['ip']);                                            
            $Accion->execute();
            $DatosProyecto = $Accion->fetch(); 
?>        
        <tr>
            <td colspan="2">
             <form action="Empleados/Requisiciones/proyectosABM.php" method="post">
                <table style="width: 80%;">
                    <tr><td  style="width: 30%;">Nombre del Proyecto: </td><td><input type="text" name="proyecto" value="<?php echo "$DatosProyecto[1]"?>" /></td></tr>
                    <tr><td >Instituci�n Financiadora: </td><td><input type="text" name="institucion" value="<?php echo "$DatosProyecto[2]"?>" /></td></tr>
                    <tr><td >Descripci�n: </td><td><input type="text" name="descripcion" value="<?php echo "$DatosProyecto[3]"?>" /></td></tr>
                    <input type='hidden' name='idp' value='<?php echo "$DatosProyecto[0]"?>' />
                    <td ><input type="submit" name="Enviar" value="Guardar" class="botonG" /></td></tr>
                </table>
             </form>
            </td>
        </tr>
        <td colspan="2"><hr color='skyblue' /></td>
        <tr>
            <td colspan="2">
                <form action="?l=ProyectArea" method="post">
                    <input type="hidden" name="ip" value="<?php echo "$DatosProyecto[0]"?>" />                
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 80%;"><strong>�reas designadas</strong></td>
                            <td style="width: 20%; text-align: right;"><input type="submit" name="Enviar" value="�reas" class="botonG" /></td>                                                
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?php
                                        $query = "SELECT a.* FROM AreasDeTrabajo as a
                                                                 inner join  Request_ProyectoArea as p on a.IdAreaDeTrabajo = p.IdArea
                                                                 where p.IdProyecto = $DatosProyecto[0]";
                                        $Areas = $bddr->prepare($query);
                                        $Areas->execute();
                                        
                                        echo "<ul style='margin-left: 20px'>";
                                        while($DataA = $Areas->fetch())
                                        {
                                            echo "<li>$DataA[1]</li>";
                                        }
                                        echo "</ul>";
                                ?>
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
        <td colspan="2"><hr color='skyblue' /></td>        
        <tr>
            <td colspan="2">
            <form action="Empleados/Requisiciones/proyectosABM.php" method="post">
            <input type="hidden" value="<?php echo $_POST['ip'];?>" name="ip" />
                <table style="width: 100%;">
                    <tr><td colspan="2"><strong>Formulario para L�nea Presupuestaria</strong></td></tr>
                    <tr><td style="width: 25%;">L�nea Presupuestaria:</td>
                        <td><input type="text" name="Linea" style="width: 60%" /></td></tr>                    
                    <tr><td></td><td><input type="submit" name="Enviar" value="Crear L�nea" class='botonG' /></td></tr>
                </table>
            </form>
            </td>
        </tr>
         <tr>
            <td colspan="2">
            <br /><br />
            <form action="Empleados/Requisiciones/proyectosABM.php" method="post">
            <input type="hidden" value="<?php echo $_POST['ip'];?>" name="ip" />
                <table style="width: 100%;">
                  <tr><td colspan="2"><strong>Formulario para Subl�nea Presupuestaria</strong></td></tr>
                  <tr><td style="width: 25%;">L�nea Presupuestaria:</td><td><select name="lien" id="lien" style="width: 304px;"> 
              <?php
                        $linea = $bddr->prepare("SELECT * FROM Request_LineaPresupuestaria where IdProyecto =" . $_POST['ip']);                                            
                        $linea->execute();
                        
                        while ($Datapro = $linea->fetch())           
                          echo "<option value='$Datapro[0]' >$Datapro[2]</option>";                                    

              ?>
              </select> </td></tr>
                    <tr><td style="width: 25%;">Sub-l�nea Presupuestaria:</td>
                        <td><input type="text" name="subLinea" style="width: 60%" /></td></tr>
                    <tr><td style="width: 25%;">Monto Presupuestario:</td>
                        <td><input type="text" min="1" name="subMonto" style="width: 60%" /></td></tr>                    
                    <tr><td></td><td><input type="submit" name="Enviar" value="Crear SubL�nea" class='botonG' /></td></tr>
                </table>
            </form>
            </td>
        </tr>
        <td colspan="2"><hr color='skyblue' /></td>
        <tr>
            <td colspan="2">
                <table style="width: 100%" rules='all'>
                    <tr>
                        <td colspan="4" style="text-align: right;"><h2>Lineas Presupuesarias</h2></td>
                    </tr>
                    <tr>
                        <th>Lineas Presupuesarias</th>
                        <th>subLineas Presupuesarias</th>
                        <th>Monto Presupuestado</th>
                        <th>Monto Ejecutado</th>
                        <th>Saldo</th>
                    </tr> 
                    <?php
                           $BudgeLine = $bddr->prepare("Select * from Request_LineaPresupuestaria where IdProyecto = " . $_POST['ip']);
                           $BudgeLine->execute();
                           
                           $Monto = 0;
                           $MontoE = 0; 
                               
                           if($BudgeLine->rowCount() > 0)
                           {

                               while($DataL = $BudgeLine->fetch())
                               {
                                   $BudgeSubLine = $bddr->prepare("Select * from Request_SubLineaPresupuestaria where IdLineaPresupuestaria = $DataL[0]");
                                   $BudgeSubLine->execute();
                                   
                                   $Rowspan = $BudgeSubLine->rowCount();
                                   if($Rowspan == 0)
                                    $Rowspan = 1;
                                    
                                   echo "<tr><td rowspan='$Rowspan'>$DataL[2]</td>"; 
                                    

                                   if($BudgeSubLine->rowCount() > 0)
                                   {
                                       while($DataS = $BudgeSubLine->fetch())
                                       {
                                            echo "<th>$DataS[2]</th><th>$" . number_format((double)$DataS[3],2) . "</th>";
                                            $Monto += $DataS[3];
                                            $Saldo = $Monto - $MontoE;
                                            
                                            $Ejecutado = $bddr->prepare("Select SUM(Costo*Cantidad_aprobada) from Request_Seguimiento where IdSubLineaPresupuestaria = $DataS[0]");                                       
                                            $Ejecutado->execute();
                                            $DataE = $Ejecutado->fetch();
                                            


                                            $Ejecuta2 = $bddr->prepare("Select SUM(Costo*Cantidad_aprobada) from Request_SeguimientoAli where IdSubLineaPresupuestaria = $DataS[0]");                                       
                                            $Ejecuta2->execute();
                                            $DataE = $Ejecuta2->fetch();

                                            
                                            $MontoE += $DataE[0];
                                            $SaldoE = $DataS[3] - $DataE[0];
                                            
                                            if($DataE[0] == "")
                                                $MontoE2 = 0;
                                            else
                                                $MontoE2 = $DataE[0];

                                           if($DataE[0] == "")
                                                $SaldoE2 = 0;
                                            else
                                                $SaldoE2 -= $DataE[0];

                                            
                                            echo "<th>$" . number_format($MontoE2,2) . "</th><th>$" . number_format($SaldoE,2) . "</th></tr>";

                                       }
                                       
                                   }
                                   else
                                        echo "<th>-</th><th>0.00</th><th>0.00</th></tr>";
                                                                    
                                    
                               }
                               
                               echo "<tr><td colspan='2'><strong>Total:</strong></td><th>$" . number_format($Monto,2) . "</th><th>$" . number_format($MontoE,2) . "</th><th>$" . number_format($Saldo,2) . "</th></tr>";
                           }
                           else
                                echo "<th colspan='3' style='color: red;'>No hay lineas presupuestarias en este proyecto</th></tr>";
                         
                    ?>

                     <tr>          
                <td colspan='2' style='text-align: center;'><br>
                <form method='post' action='Empleado.php?l=editBug' name='EditarLineasPresupuestarias'>
                      <input type="hidden" value="<?php echo $_POST['ip'];?>" name="ip" />
                      <input type=submit name='Editar' value='Editar' title='Editar datos del proyecto.' class='boton'> 

                </form> 
                </td>   
              </tr>
                </table>
            </td>
        </tr>
    </table>    
</div>