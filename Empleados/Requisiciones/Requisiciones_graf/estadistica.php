
    <?php

    if(!isset($_SESSION["autenticado"])){
        require '../../../net.php';
    } ?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Estadisticas de requisiciones</title>
</head>
<script src="https://code.highcharts.com/highcharts.js"></script>
<body>
    <div style="float:right; width: 75%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="7"><h2 style="color: #197198;">Estadisticas de Requisiciones</h2></td></tr>
        <tr><td colspan="7"><hr color='skyblue' /></td></tr>
        <tr>


     <td style="width: 20%;"><a href="?l=Stats">
                <input type="submit" value="Vista General" name="Enviar" class="botonG" style="width: 105px;" /></a>
               
     </td>
      <td style="width: 20%;"><a href="?l=Statsproy">
                <input type="submit" value="Vista por proyecto" name="Enviar" class="botonG" style="width: 125px;" /></a>
               
     </td>

      <td style="width: 20%;"><a href="?l=Statsarea">
                <input type="submit" value="Vista por area" name="Enviar" class="botonG" style="width: 105px;" /></a>
               
     </td>

</tr>
</table>
<script src="Highcharts-6.1.0/code/highcharts.js"></script>
<script src="Highcharts-6.1.0/code/modules/exporting.js"></script>
<script src="Highcharts-6.1.0/code/modules/export-data.js"></script>

<?php 

echo "<form method=\"POST\">";
echo "<br>filtrar por fecha <br><input type=\"date\" name=\"inicio\"> <input type=\"date\" name=\"fin\">";
echo "<input type=\"submit\" value=\"ver\"name=\"ver\"/>";
echo "</form>";

echo "<div id=\"container\" style=\"min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto\"></div>";
        
    $where = "";
    $date = "";
    if(isset($_POST["ver"])){
        if ($_POST["inicio"] != "" && $_POST["fin"] != ""){
            $where = " AND Fecha_Solicitud BETWEEN '".$_POST["inicio"]."' AND '".$_POST["fin"]."'";
            $date = " (".$_POST['inicio']." / ".$_POST['fin'].")";
        }
    }
        try {
            //Alimentacion
            //realizados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Alimentacion'".$where);
            $sql->execute();
            $A_a = $sql->rowCount();
            //aceptados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Alimentacion' AND Estado = 'Aceptado'".$where);
            $sql->execute();
            $A_b = $sql->rowCount();
            //rechazados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Alimentacion' AND Estado = 'Denegado'".$where);
            $sql->execute();
            $A_c = $sql->rowCount();
            //entregados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Alimentacion' AND Estado = 'Entregado'".$where);
            $sql->execute();
            $A_d = $sql->rowCount();
            //pendientes
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Alimentacion' AND Estado = 'Pendiente'".$where);
            $sql->execute();
            $A_e = $sql->rowCount();


            //Materiales
            //realizadas
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Materiales'".$where);
            $sql->execute();
            $M_a = $sql->rowCount();
            //aceptados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Materiales' AND Estado = 'Aceptado'".$where);
            $sql->execute();
            $M_b = $sql->rowCount();
            //rechazados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Materiales' AND Estado = 'Denegado'".$where);
            $sql->execute();
            $M_c = $sql->rowCount();
            //entregados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Materiales' AND Estado = 'Entregado'".$where);
            $sql->execute();
            $M_d = $sql->rowCount();
            //pendientes
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Materiales' AND Estado = 'Pendiente'".$where);
            $sql->execute();
            $M_e = $sql->rowCount();



            $bddr = null;
            $sql = null;
        } catch (PDOException $e) {
            print "¡Error!: " . $e->getMessage() . "<br/>";
            die();
        }

?>

                    <!--    MUESTRA GRAFICA     -->

</div></body>
<script type="text/javascript">

Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Estadistica de Requisiciones'
    },
    subtitle: {
        text: <?php echo "'FUSALMO".$date."'"; ?>
    },
    xAxis: {
        categories: ['Material', 'Alimentacion'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Cantidad de requisiciones',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Requisiciones'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Realizadas',
        data: [<?php echo "$A_a, $M_a"; ?>]
    }, {
        name: 'Aceptadas',
        data: [<?php echo "$A_b, $M_b"; ?>]
    }, {
        name: 'Rechazadas',
        data: [<?php echo "$A_c, $M_c"; ?>]
    }, {
        name: 'Entregadas',
        data: [<?php echo "$A_d, $M_d"; ?>]
    }, {
        name: 'Pendientes',
        data: [<?php echo "$A_e, $M_e"; ?>]
    }]
});
</script>

</body>
</html>



<?php 
/*

SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk
FROM Request_Requisiciones AS rr
INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
GROUP by Idrequisicion

*/
 ?>