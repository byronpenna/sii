    <?php

    if(!isset($_SESSION["autenticado"])){
        require '../../../net.php';
    } ?>
    

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Estadisticas de requisiciones (AREAS)</title>
</head>
<script src="https://code.highcharts.com/highcharts.js"></script>
<body>

<script src="Highcharts-6.1.0/code/highcharts.js"></script>
<script src="Highcharts-6.1.0/code/modules/exporting.js"></script>
<script src="Highcharts-6.1.0/code/modules/export-data.js"></script>

    <div style="float:right; width: 75%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="7"><h2 style="color: #197198;">Estadisticas de Requisiciones</h2></td></tr>
        <tr><td colspan="7"><hr color='skyblue' /></td></tr>
        <tr>


     <td style="width: 20%;"><a href="?l=Stats">
                <input type="submit" value="Vista General" name="Enviar" class="botonG" style="width: 105px;" /></a>
               
     </td>
      <td style="width: 20%;"><a href="?l=Statsproy">
                <input type="submit" value="Vista por proyecto" name="Enviar" class="botonG" style="width: 125px;" /></a>
               
     </td>

      <td style="width: 20%;"><a href="?l=Statsarea">
                <input type="submit" value="Vista por area" name="Enviar" class="botonG" style="width: 105px;" /></a>
               
     </td>

</tr>
</table>
<form method="POST">

<?php 


        $sql = $bddr->prepare("SELECT IdAreaDeTrabajo, NombreAreaDeTrabajo from AreasDeTrabajo");
        $sql->execute();
        ?>
        Elija un area:
        <br><select name="selectAreas" style="height: 20px">
            <?php 
            foreach ($sql as $key => $value) {
                echo "<option value=\"$value[0]\">$value[1]</option>";
            }
            ?>
        </select>

<?php
echo "<br>filtrar por fecha <br><input type=\"date\" name=\"inicio\"> <input type=\"date\" name=\"fin\">";
echo "<input type=\"submit\" value=\"ver area\"/>";
echo "</form>";


if(isset($_POST['selectAreas'])){
    $area = $_POST['selectAreas'];

    try {
        $where = "";
        $date = "";
        if ($_POST["inicio"] != "" && $_POST["fin"] != ""){
            $where = " AND rr.Fecha_Solicitud BETWEEN '".$_POST["inicio"]."' AND '".$_POST["fin"]."'";
            $date = " (".$_POST['inicio']." / ".$_POST['fin'].")";
        }


        //Alimentacion
        //realizados
        $sql = $bddr->prepare("SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk, rr.Tipo_Requisicion, rr.Estado
            FROM Request_Requisiciones AS rr
            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
            INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE rr.Tipo_Requisicion = 'Alimentacion' AND at.IdAreaDeTrabajo = $area $where
            GROUP by Idrequisicion");
        $sql->execute();
        $A_a = $sql->rowCount();

        //aceptados
        $sql = $bddr->prepare("SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk, rr.Tipo_Requisicion, rr.Estado
            FROM Request_Requisiciones AS rr
            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
            INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE rr.Tipo_Requisicion = 'Alimentacion' AND at.IdAreaDeTrabajo = $area AND rr.Estado = 'Aceptado' $where
            GROUP by Idrequisicion");
        $sql->execute();
        $A_b = $sql->rowCount();

        //Rechazados
        $sql = $bddr->prepare("SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk, rr.Tipo_Requisicion, rr.Estado
            FROM Request_Requisiciones AS rr
            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
            INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE rr.Tipo_Requisicion = 'Alimentacion' AND at.IdAreaDeTrabajo = $area AND rr.Estado = 'Denegado' $where
            GROUP by Idrequisicion");
        $sql->execute();
        $A_c = $sql->rowCount();

        //Entregado
        $sql = $bddr->prepare("SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk, rr.Tipo_Requisicion, rr.Estado
            FROM Request_Requisiciones AS rr
            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
            INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE rr.Tipo_Requisicion = 'Alimentacion' AND at.IdAreaDeTrabajo = $area AND rr.Estado = 'Entregado' $where
            GROUP by Idrequisicion");
        $sql->execute();
        $A_d = $sql->rowCount();

        //Pendientes
        $sql = $bddr->prepare("SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk, rr.Tipo_Requisicion, rr.Estado
            FROM Request_Requisiciones AS rr
            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
            INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE rr.Tipo_Requisicion = 'Alimentacion' AND at.IdAreaDeTrabajo = $area AND rr.Estado = 'Pendiente' $where
            GROUP by Idrequisicion");
        $sql->execute();
        $A_e = $sql->rowCount();

        //Materiales
        //realizados
        $sql = $bddr->prepare("SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk, rr.Tipo_Requisicion, rr.Estado
            FROM Request_Requisiciones AS rr
            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
            INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE rr.Tipo_Requisicion = 'Materiales' AND at.IdAreaDeTrabajo = $area $where
            GROUP by Idrequisicion");
        $sql->execute();
        $M_a = $sql->rowCount();

        //aceptados
        $sql = $bddr->prepare("SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk, rr.Tipo_Requisicion, rr.Estado
            FROM Request_Requisiciones AS rr
            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
            INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE rr.Tipo_Requisicion = 'Materiales' AND at.IdAreaDeTrabajo = $area AND rr.Estado = 'Aceptado' $where
            GROUP by Idrequisicion");
        $sql->execute();
        $M_b = $sql->rowCount();

        //Rechazados
        $sql = $bddr->prepare("SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk, rr.Tipo_Requisicion, rr.Estado
            FROM Request_Requisiciones AS rr
            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
            INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE rr.Tipo_Requisicion = 'Materiales' AND at.IdAreaDeTrabajo = $area AND rr.Estado = 'Denegado' $where
            GROUP by Idrequisicion");
        $sql->execute();
        $M_c = $sql->rowCount();

        //Entregado
        $sql = $bddr->prepare("SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk, rr.Tipo_Requisicion, rr.Estado
            FROM Request_Requisiciones AS rr
            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
            INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE rr.Tipo_Requisicion = 'Materiales' AND at.IdAreaDeTrabajo = $area AND rr.Estado = 'Entregado' $where
            GROUP by Idrequisicion");
        $sql->execute();
        $M_d = $sql->rowCount();

        //Pendientes
        $sql = $bddr->prepare("SELECT rr.Idrequisicion, c.Cargo, at.NombreAreaDeTrabajo, rr.IdEmpleado, c.IdArea_Fk, rr.Tipo_Requisicion, rr.Estado
            FROM Request_Requisiciones AS rr
            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = rr.IdEmpleado
            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
            INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE rr.Tipo_Requisicion = 'Materiales' AND at.IdAreaDeTrabajo = $area AND rr.Estado = 'Pendiente' $where
            GROUP by Idrequisicion");
        $sql->execute();
        $M_e = $sql->rowCount();   
    } catch (Exception $e) {
        echo "Ocurrio un error al consultar la base de datos";
    }

    //nombre del area
    $sql = $bddr->prepare("SELECT NombreAreaDeTrabajo from AreasDeTrabajo WHERE IdAreaDeTrabajo = ". $_POST["selectAreas"]);
    $sql->execute();
    $A = $sql->fetch()[0];

    $bddr = null;
    $sql = null;

}	
?>

<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>


</form>
</div>

<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Estadistica de Requisiciones'
    },
    subtitle: {
        text: <?php echo "'".$A.$date."'";  ?>
    },
    xAxis: {
        categories: ['Material', 'Alimentacion'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Cantidad de requisiciones',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Requisiciones'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Realizadas',
        data: [<?php echo "$A_a, $M_a"; ?>]
    }, {
        name: 'Aceptadas',
        data: [<?php echo "$A_b, $M_b"; ?>]
    }, {
        name: 'Rechazadas',
        data: [<?php echo "$A_c, $M_c"; ?>]
    }, {
        name: 'Entregadas',
        data: [<?php echo "$A_d, $M_d"; ?>]
    }, {
        name: 'Pendientes',
        data: [<?php echo "$A_e, $M_e"; ?>]
    }]
});
</script>
</body>
</html>