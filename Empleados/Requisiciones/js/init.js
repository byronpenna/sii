/*!
  * eWallet App v3.0
  *
  * Authors: [Frank Esquivel, Leo López]
  *
  * Realeased under the MIT License
  * Repo: https://github.com/franklinesquivel/eWallet/
*/
(function(window){
	if (window.eWallet === undefined || typeof window.eWallet !== "object") {
		window.eWallet = {};
	}
})(window);

//Clase para el elemento de la ventana MODAL
class Modal {
	constructor(element) {
		this.element = element;
		this.back = eWallet.find('#_background', 1);
	}

	open(callback = null){
		if (this.back === null) {
			eWallet.genPopOutBackground();
			this.back = eWallet.find('#_background', 1);
		}

		this.back.eWallet.fadeIn(2);
		this.element.classList.remove('closed');
		this.element.classList.add('open');
		typeof callback === "function" ? callback() : "";
	}

	close(callback = null){
		this.back.eWallet.fadeOut(2);
		this.element.classList.remove('open');
		this.element.classList.add('closed');
		typeof callback === "function" ? callback() : "";
	}
}

eWallet.on = function(el, evt, sel, handler) {
	el.addEventListener(evt, function(event) {
		var t = event.target;
		while (t && t !== this) {
			if (t.matches(sel)) {
				handler.call(t, event);
			}
			t = t.parentNode;
		}
	});
}

//Método del OBJETO GENERAL para crear elementos del DOM
eWallet.create = function(tag, content = null, dataset = null){
	let element = document.createElement(tag);

	if (dataset !== null && typeof dataset === "object") {
		try{
			for(attr in dataset){
				element.setAttribute(attr, dataset[attr]);
			}
		}catch(err){
			console.error("eWallet Error: Parámetros inválidos para la creación del elemento");
		}
	}

	if (content !== null) {
		let elementPattern = /<[a-z][\s\S]*>/im;

		if (eWallet.isElement(content)) {
			element.appendChild(content);
		}else{
			element.innerHTML = content;
		}
	}

	return element;
};

//Método del OBJETO GENERAL para verificar sí su parámetro es un elemento del DOM
eWallet.isElement = function(obj){
	try{
		return (obj.constructor.__proto__.prototype.constructor.name) ? true : false;
	}catch(e){
		return false;
	}
};

//Método del OBJETO GENERAL para encontrar y obtener un o varios elementos del DOM
eWallet.find = function(selector, index = false){
	if (selector === undefined || selector === null || typeof selector !== "string" || selector.length <= 0) {
		console.error("eWallet Error: Ingrese un selector válido");
	}else{
		try{
			let elCollection = document.querySelectorAll(selector);
			// return (elCollection.length == 1 ? elCollection[0] : elCollection);
			return index !== false ? elCollection[index - 1] : elCollection;
		}catch(ex){
			console.error("eWallet Error: El elemento no existe");
		}
	}
};

//Método del OBJETO GENERAL para eliminar elementos del DOM
eWallet.destroy = function(selector){
	if (typeof selector !== "string" && !eWallet.isElement(selector)) return;

	if (selector !== null) {
		if (typeof selector === "string") {
			let elements = eWallet.find(selector);

			if (elements.length > 0) {
				for (var i = 0; i < elements.length; i++) {
					elements[i].parentNode.removeChild(elements[i]);
				}
			}
		}else if(selector.parentNode !== null){
			selector.parentNode.removeChild(selector);
		}
	}
};

//Método del OBJETO GENERAL para mostrar un mensaje temporal en una caja personalizada
eWallet.toast = function(msg, time = 2, style = 'grey darken-3'){
	if (typeof msg !== "string") return;
	var toast = eWallet.create('div', msg, {class: "toast"});

	style = style.trim();
	if (style.split(' ').length > 1) {
		let cls = style.split(' ');
		cls.forEach(c => toast.classList.add(c));
	}else if (style.trim() !== ""){
		toast.classList.add(style);
	}

	toast.style.animation = `toast-animation ${time}s ease-in`;
	document.querySelector('body').appendChild(toast);

	setTimeout(function(){
		eWallet.destroy(toast);
	}, ((time * 1000) + 500));
};

eWallet.genPopOutBackground = function(){
	let background = eWallet.create('div', '', {id: '_background'});
	eWallet.find('body', 1).eWallet.append(background);
}

eWallet.modal = function(selector){
	let element = eWallet.find(selector);

	if (element.length > 1){
		console.error('eWallet Error: Se quiere instaciar más de un elemento como modal, favor hacerlo individual');
		return;
	}

	element = element[0];
	const _id = element.getAttribute('id'),
		triggers = _id !== null ? eWallet.find(`.modal-trigger[modal=${_id}]`) : null;

	if (eWallet.find('#_background').length == 0) eWallet.genPopOutBackground();

	if (!element instanceof HTMLDivElement) {
		console.error('eWallet Error: El parámetro no es un elemento válido para instaciar un modal!');
		return;
	}

	!element.classList.contains('modal') ? element.classList.add('modal') : "";

	const ModalObj = new Modal(element);
	if (_id !== null && triggers.length !== 0){
		triggers.forEach(trigger => trigger.addEventListener('click', function(){ModalObj.open()}));
	}

	window.addEventListener('keydown', function(e){
	    if ( e.keyCode == 27 && ModalObj.element.classList.contains('open')) {
	        ModalObj.close();
	    }
	})

	eWallet.find("#_background", 1).addEventListener('click', function(e){
		if (e.target === eWallet.find("#_background", 1) && ModalObj.element.classList.contains('open')) {
	        ModalObj.close();
		}
	})

	return ModalObj;
};

(function(){
	//----------------------------------------------------------------------------------//
	//																					//
	//						**** eWallet DOM Methods ****								//
	//																					//
	//	 					- Version: 1.1												//
	//	 					- author: Franklin Esquivel									//
	//																					//
	//----------------------------------------------------------------------------------//
	const eWallet_Methods = function(element){
		this.element = element;
	};

	eWallet_Methods.prototype.validate = function(dataset, handler = null) {
		if (!this.element instanceof HTMLFormElement) return false;
		if (typeof dataset === "object") {
			let f = 0, options = dataset;
			for(el in options){
				if (this.element[el] !== undefined) {
					let condition, msg;
					if (this.element[el] instanceof HTMLInputElement || this.element[el][this.element[el].length - 1] instanceof HTMLInputElement) {
						for(rule in options[el]){
							msg = options[el][rule].msg;
							let pattern = msg !== undefined ? options[el][rule].value : undefined;
							switch(rule){
								case "required":
									condition = parseInt(this.element[el].value.trim().length) === 0;
									break;
								case "pattern":
									condition = !pattern.test(this.element[el].value);
									break;
								case "condition":
									condition = !options[el][rule].value;
									break;
							}
						}
					}else if(this.element[el] instanceof HTMLSelectElement){
						for(rule in options[el]){
							msg = options[el][rule].msg;
							if (rule === "required") {
								condition = this.element[el].selectedIndex === 0;
							}
						}
					}

					if (this.element[el] instanceof RadioNodeList){
						this.element[el].forEach((rdbEl, i) => {
							rdbEl.getAttribute('disabled') === null && condition ? f++ : "";
							rdbEl.getAttribute('disabled') === null ? rdbEl.eWallet.validateInput(condition, msg) : "";
						});
					}else{
						this.element[el].getAttribute('disabled') === null && condition ? f++ : "";
						this.element[el].eWallet.validateInput(condition, msg);
					}
				}else{
					console.warn(`eValidate Warning: El elemento ${el} no existe en el DOM!`);
				}
			}

			if (handler !== null && typeof handler === "function")  {
				handler(f == 0);
			}
			return f == 0;
		}else{
			console.error(`eValidate Error: Parámetro inválido!`);
		}
	};

	eWallet_Methods.prototype.validateInput = function(toogle, msg = undefined){
		if (!this.element instanceof HTMLInputElement || !this.element instanceof HTMLSelectElement) return false;
		if (this.element.getAttribute('disabled') !== null) {
			this.element.parentNode.classList.remove('invalid');
			this.deleteMsg();
			this.element.classList.remove('invalid');
			return false;
		}
		if (this.element instanceof HTMLInputElement) {
			toogle ? this.element.classList.add('invalid') : this.element.classList.remove('invalid');
		}else if(this.element instanceof HTMLSelectElement){
			toogle ? this.element.parentNode.classList.add('invalid') : this.element.parentNode.classList.remove('invalid');
		}
		// !toogle ? this.element.classList.add('valid') : this.element.classList.remove('valid');
		// msg !== undefined ? (toogle ? (this.insertMsg(msg) : this.deleteMsg())) : (!toogle ? this.deleteMsg() : "");
		msg !== undefined ? (toogle ? this.insertMsg(msg) : this.deleteMsg()) : (!toogle ? this.deleteMsg() : "");
	};

	eWallet_Methods.prototype.deleteMsg = function(){
		if (!this.element instanceof HTMLInputElement || !this.element instanceof HTMLSelectElement) return false;
		let container = this.element.parentNode, msgEl = null;
		if(this.element instanceof HTMLSelectElement){
			container = this.element.parentNode.parentNode;
		}else if(this.element instanceof HTMLInputElement){
			switch(this.element.getAttribute('type')){
				case "radio":
					container = this.element.parentNode.parentNode;
					break;
				default:
					container = this.element.parentNode;
					break;
			}
		}

		let childrens = container.children;
		for (let i = 0; i < childrens.length; i++) {
			if (childrens[i].getAttribute('validate_msg') !== null) {
				msgEl = childrens[i]; break;
			}
		}
		if (msgEl !== null) {
			msgEl.eWallet.fadeOut(1, function(){
				eWallet.destroy(msgEl);
			})
		}
	}

	eWallet_Methods.prototype.insertMsg = function(msg, toogle = 1){
		if (!this.element instanceof HTMLInputElement || !this.element instanceof HTMLSelectElement) return false;
		if(this.element instanceof HTMLSelectElement){
			if (this.element.parentNode.nextElementSibling === null) {
				this.element.parentNode.parentNode.appendChild(eWallet.create('div', msg, {
					class: `msg ${toogle ? 'error' : 'success'}`,
					id: `${this.element.getAttribute('name')}-${toogle ? 'error' : 'success'}`,
					validate_msg: true
				}))
			}
		}else if(this.element instanceof HTMLInputElement){
			let msgFlag = false, container = this.element.parentNode,
				msgEl = eWallet.create('div', msg, {
					class: `msg ${toogle ? 'error' : 'success'}`,
					id: `${this.element.getAttribute('name')}-${toogle ? 'error' : 'success'}`,
					validate_msg: true
				});

			switch(this.element.getAttribute('type')){
				case "radio":
					container = this.element.parentNode.parentNode;
					break;
				default:
					container = this.element.parentNode;
					break;
			}

			let childrens = container.children;

			for (let i = 0; i < childrens.length; i++) {
				if (childrens[i].getAttribute('validate_msg') !== null) {
					msgFlag = true; break;
				}
			}

			if (!msgFlag) {
				switch(this.element.getAttribute('type')){
					case "radio":
						toogle ? container.children[1].classList.add('invalid') : container.children[1].classList.remove('invalid');
						container.eWallet.append(msgEl);
						break;
					default:
						container.eWallet.append(msgEl);
						break;
				}
			}
		}
	};

	eWallet_Methods.prototype.prepend = function(content){
		if (this.element instanceof Element) {
			let elementPattern = /<[a-z][\s\S]*>/im;

			if (this.element.children[0]) {
				if(eWallet.isElement(content)){
					let auxContent = this.element.innerHTML;
					this.element.innerHTML = "";
					this.element.append(content);
					this.element.innerHTML += auxContent;
				}else if(elementPattern.test(content)){
					let auxContent = this.element.innerHTML;
					this.element.innerHTML = content + auxContent;
				}else{
					content = eWallet.isElement(content) ? content : document.createTextNode(content);
					this.element.insertBefore(content, this.element.children[0]);
				}
			}else{
				eWallet.isElement(content) ? this.element.appendChild(content) : this.element.innerHTML = content;
			}
		}
	};

	eWallet_Methods.prototype.append = function(content){
		if (this.element instanceof Element) {
			let elementPattern = /<[a-z][\s\S]*>/im;

			if (!eWallet.isElement(content) || typeof content === "string") {
				content = !elementPattern.test(content) ? document.createTextNode(content) : content;

				elementPattern.test(content) ? this.element.innerHTML += content :  this.element.appendChild(content);
			}else if(eWallet.isElement(content)){
				this.element.appendChild(content);
			}
		}
	};

	eWallet_Methods.prototype.fadeIn = function(time = 1, handler){
		if (this.element instanceof Element) {
			let el = this.element, op = 0.1, timer;

			el.style.display = 'block';
			timer = setInterval(function(){
				if (op >= 1) {
					clearInterval(timer);
				}
				el.style.opacity = op;
				el.style.filter = `alpha(opacity=${op * 100})`;
				op += op * 0.1;
			}, time);
			setTimeout(function(){
				typeof handler === "function" ? handler() : "";
			}, time * 1000);
		}
	};

	eWallet_Methods.prototype.fadeOut = function(time = 1, handler){
		if (this.element instanceof Element) {
			let el = this.element, op = 1, timer;

			timer = setInterval(function(){
				if (op <= 0.1) {
					clearInterval(timer);
					el.style.display = 'none';
				}
				el.style.opacity = op;
				el.style.filter = `alpha(opacity=${op * 100})`;
				op -= op * 0.1;
			}, time);
			setTimeout(function(){
				typeof handler === "function" ? handler() : "";
			}, time * 1000);
		}
	};

	/*
	El objeto eWallet_Methods es añadido a TODOS los elementos existentes y/o que existirán en
	el DOM como una propiedad más. Mediante esta propiedad se podrá acceder a todos los métodos
	declarados anteriormente.
	*/
	Object.defineProperty(Element.prototype, "eWallet", {
		get: function () {
			Object.defineProperty(this, "eWallet", {
				value: new eWallet_Methods(this)
			});

			return this.eWallet;
		},
		configurable: true,
		writeable: false
	});
	//							END eWallet DOM Methods									//
	//----------------------------------------------------------------------------------//
})();
