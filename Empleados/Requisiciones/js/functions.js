(function(){
    var materials = [], providersData = [], orderId = [], prevMaterials = [], orderFlag = true, orderData = [];
    document.addEventListener('DOMContentLoaded', function(){
        var mdlProvider = eWallet.modal('#mdlProvider');
        var mdlOrder = eWallet.modal('#mdlOrder');
        var mdlQuotation = eWallet.modal('#mdlQuotation');
        var mdlComparative = eWallet.modal('#mdlComparative');
        var providers = null; //Guarda los proveedores traidos en el ajax
        var providersSelect = []; //Guarda los proveedores que se van seleccionando

        $.validator.methods.phone = function(value, element) {
            return this.optional(element) || /^(7|6|2)\d{3}\-?\d{4}$/.test(value);
        }

        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        })

        function orderSelect(select){
            $.ajax({
                type: 'POST',
                url: '/Empleados/Requisiciones/php/Controller.php',
                data: {getHeadquarters: true},
                success: function(res){
                    $(select).html("<option disabled selected value=''>Seleccione una sede</option>");
                    res = JSON.parse(res);
                    res.forEach(el => {
                        $(select).append(`<option value='${el.idSede}'>${el.nombre}</option>`);
                    })
                }
            })
        }

        $(document).on("click", "#mdlProvider .modal-btn.close", function(){
            mdlProvider.close();
        })

        $(document).on("click", "#mdlOrder .modal-btn.close", function(){
            mdlOrder.close();
        })

        $(document).on('click', '#mdlQuotation .modal-btn.close', function(){
            mdlQuotation.close();
        });

        $(document).on("click", "#mdlComparative .modal-btn.close", function(){
            mdlComparative.close();
        })

        $("#frmProvider").validate({
            debug: true,
            rules: {
                txtName: 'required',
                txtAddress: 'required',
                txtCity: 'required',
                txtState: 'required',
                txtPhone: {
                    required: true
                    // ,phone: true
                },
                rdbType: 'required',
                txtCP: 'required'
            },
            messages: {
                txtName: 'Ingrese un nombre',
                txtAddress: 'Ingrese una direcci&oacute;n',
                txtCity: 'Ingrese una ciudad',
                txtState: 'Ingrese un estado',
                txtPhone: {
                    required: 'Ingrese un n&uacute;mero telef&oacute;nico'
                    // ,phone: 'Ingrese un número telefónico válido'
                },
                rdbType: 'Seleccione un tipo',
                txtCP: 'Ingrese un c&oacute;digo postal'
            },
            errorElement : 'p',
	        errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form){
                let data = {
                    name: form.txtName.value,
                    address: form.txtAddress.value,
                    city: form.txtCity.value,
                    state: form.txtState.value,
                    phone: form.txtPhone.value,
                    postalCode: form.txtCP.value,
                    type: form.rdbType.value
                }

                $.ajax({
                    type: 'POST',
                    url: '/Empleados/Requisiciones/php/Controller.php',
                    data: {addProvider: true, data: JSON.stringify(data)},
                    success: function(res){
                        if(parseInt(res)){
                            eWallet.toast('El nuevo proveedor se ha registrado &eacute;xitosamente!', 2, 'green darken-1');
                            form.reset();
                            mdlProvider.close();
                        }else{
                            eWallet.toast('Ha ocurrido un error :( Intentalo m&aacute;s tarde...', 2, 'red darken-1');
                        }
                        getProviders($('input[name=Tprovider]:checked').val());
                    }
                })
            },
            invalidHandler: function(){
                eWallet.toast('Ingrese todos lo datos solicitados!', 2, 'red darken-2');
            }
        });

        function getProviders(type){ //Se cargan los proveedore en los selects
            $.ajax({
                type: 'POST',
                url: '/Empleados/Requisiciones/php/Controller.php',
                data: {
                    getProviders: 1,
                    type: type
                },
                success: function(r){
                    providers = JSON.parse(r);
                    let s = 0;
                    providersSelect = []; //Se inicializa los proveedores seleccionados a cero
                    for(var i = 0; i < document.querySelectorAll('select.cmbProvider').length; i++){ //Se recorren los proveedores
                        $('select').eq(i).html("");
                        $('select').eq(i).append($("<option disabled> Proveedor "+ (++s) +"</option>")
                            .attr({selected: true})
                        );
                        for(var x = 0; x < providers.length; x++){ //Se recorre el arreglo traido en la petición
                            $('select').eq(i).append($("<option>"+ providers[x].nombre +"</option>")
                                .attr({value: providers[x].IdProveedor}) //Se agregan los respectivos options
                            );
                        }
                    }
                }
            });
        }

        $(document).on("change", "select.cmbProvider", function() {
            if (providersSelect.length > 0) {
                if (checkProvider($(this))) {
                    //Se chequea la repeticion
                    providersSelect.push({
                        select: $(this).attr("id"),
                        id: $(this).val()
                    });
                }
            } else {
                providersSelect.push({
                    select: $(this).attr("id"),
                    id: $(this).val()
                });
            }

            providersSelect.forEach(el => $(`input[type=radio].${el.select}`).val(el.id));
            providersSelect.forEach(el => $(`input[type=radio].${el.select}`).attr("setprovider", true));
        });

        function checkProvider(select) {
          /* Se verifica la existencia de un proveedor repetido ya en el array de objetos */
            let provider = providersSelect.find(function(provider) {
                return provider.id == $(select).val();
            });

            if (provider == undefined) {
                for (var i in providersSelect) {
                    if (providersSelect[i].select == $(select).attr("id")) {
                        providersSelect[i].id = $(select).val();
                        return false;
                    }
                }
                return true;
            } else {
                /* Se elimina el indice donde se repite */
                let checkSelect = providersSelect.find(function(provider) { return (provider.select == $(select).attr("id")); });

                if(checkSelect != undefined){ providersSelect.splice(providersSelect.indexOf(checkSelect), 1); }

                $("#" + $(select).attr("id") + " option:eq(0)").prop("selected", true);
                eWallet.toast("Ese proveedor ya ha sido seleccionado", 2, "red darken-2");
                return false;
            }
        }

        function initPage(){
            $.get('/Empleados/Requisiciones/php/Controller.php', {l: 'listMaterial', id: formIdRequi.idp.value, type: $("input[name=requiType]").val()}).done(function(r){
                $(".table-material").empty();
                if(r == "0"){
                    $(".table-material").append("<h1 style='text-align: center;'> Requisici&oacute;n procesada </h1><br>");
                    document.querySelector(".buttonProvider").classList.remove('visible');
                }else if(r == "-1"){
                    $(".table-material").append("<h1 style='text-align: center;'> No se ha encontrado materiales en la requisici&oacute;n seleccionada </h1>");
                    document.querySelector(".buttonProvider").classList.remove('visible');
                }else{
                    $(".table-material").append(r);
                    if($("input[name=requiType]").val() == 'Materiales'){
                        $('input[name=Tprovider]').eq(0).prop('checked', true);
                    }else if($("input[name=requiType]").val() == 'Alimentacion'){
                        $('input[name=Tprovider]').eq(1).prop('checked', true);
                    }

                    getProviders($('input[name=Tprovider]:checked').val()); //Se cargan los proveedores
                }

            });
        }

        initPage(); //Se carga la tabla con el material

        $(document).on('blur', 'form[name=frmRegisterData] input.txtUnit, form[name=frmModifyData] input.txtUnit', function(){
            let inputIndex = parseInt($(this).attr('inputindex'));
            let unitData = Number($(this).val().trim());

            if((unitData !== "" && isNaN(unitData)) || (unitData < 0)){
                eWallet.toast('Ingrese un valor v&aacute;lido!', 2, 'red darken-1');
                $(this).focus();
                return;
            }

            let materialRow = $(this).parent().parent();

            if (materialRow.attr('materialrow') !== undefined && materialRow.attr('idMaterial') !== undefined) {
                let materialCant = Number(materialRow.find('td[materialcant]').html()), auxTotal = 0, auxUnit = 0;
                materialRow.find('.txtTotal').eq(inputIndex).val(`$${(unitData * materialCant).toFixed(2)}`);
                $(`tr[materialrow=true] td input.txtTotal[inputindex=${inputIndex}]`).each((i, input) => auxTotal += Number($(input).val().substr(1, $(input).val().length)));
                $(`tr[materialrow=true] td input.txtUnit[inputindex=${inputIndex}]`).each((i, input) => auxUnit += Number($(input).val()));

                $(`#totalData td[dataindex=${inputIndex}].tdTotalTotal`).html(`$${Number(auxTotal).toFixed(2)}`);
                $(`#totalData td[dataindex=${inputIndex}].tdTotalUnit`).html(`$${Number(auxUnit).toFixed(2)}`);
            }else{
                eWallet.toast('Ha ocurrido un error! Notifica a nuestro equipo de soporte...', 2, 'red darken-2');
            }
        })

        $(document).on('click', 'form[name=frmRegisterData] #sendData', function(){
            materials = [];
            let validFlag = false;

            if(providersSelect.length === 1){
                eWallet.toast('Debes seleccionar por lo menos 2 proveedores de materiales!', 2, 'red darken-1');
                return;
            }else if(providersSelect.length === 0){
                eWallet.toast('Debes seleccionar proveedores de materiales!', 2, 'red darken-1');
                $('form[name=frmRegisterData] select.cmbProvider').eq(0).focus();
                return;
            }

            $('form[name=frmRegisterData] tr[materialrow=true]').each((i, row) => {
                let idMaterial = $(row).attr('idmaterial');
                let cant = $(row).find('td[materialCant=true]').html();
                let c = 0;
                let inputIndex = parseInt($(row).find(`input[name=${idMaterial}]:checked`).attr('inputindex'));
                
                $(row).find(`input[name=${idMaterial}]`).each((radioIndex, radio) => {c += !radio.checked ? 1 : 0;});
                if(c === $(row).find(`input[name=${idMaterial}]`).length){
                    eWallet.toast('Debes seleccionar un proveedor para cada material en la tabla!', 2, 'red darken-1');
                    return false;
                }
                
                if($(row).find(`.txtUnit`).eq(inputIndex).val().trim() <= 0){
                    eWallet.toast('Ingresa una cantidad en los campos ', 2, 'red darken-1');
                    return false;
                }
                
                
                providersData = [];
                let rowData = [];
                for (var x = 0; x < 3; x++) {
                    $(row).find(`input[name=${idMaterial}]`).each((radioIndex, radio) => {
                        let price = $(row).find(`.txtUnit`).eq(radioIndex).val(),
                            total = $(row).find(`.txtTotal`).eq(radioIndex).val();

                        if($(radio).attr('setprovider') !== undefined){
                            let idProvider = radio.value;
                            providersData[radioIndex] = {
                                idProvider,
                                price: price === "" || isNaN(Number(price)) ? 0.00 : Number(price).toFixed(2),
                                total: price === "" || isNaN(Number(total.substr(1, total.length)))
                                ? 0.00
                                : Number(total.substr(1, total.length)).toFixed(2),
                                selected: radio.checked
                            }
                        }else {
                            providersData[radioIndex] = {
                                idProvider: 1,
                                price: price === "" || isNaN(Number(price)) ? 0.00 : Number(price).toFixed(2),
                                total: price === "" || isNaN(Number(total.substr(1, total.length)))
                                ? 0.00
                                : Number(total.substr(1, total.length)).toFixed(2),
                                selected: false
                            }
                        }
                    });

                }

                materials.push({
                    idMaterial: idMaterial,
                    idSubLinea: $("[name=idSubLinea]").val(),
                    cant,
                    providers: providersData
                })

                validFlag = $('form[name=frmRegisterData] tr[materialrow=true]').length === (i + 1);
            })

            if (validFlag) {
                $.ajax({
                    type: 'POST',
                    url: '/Empleados/Requisiciones/php/Controller.php',
                    data: {insertComparation: true, data: JSON.stringify(materials), idRequi: formIdRequi.idp.value, type: frmRegisterData.Tprovider.value},
                    success: function(res){
                        if (parseInt(res)) {
                            eWallet.toast('Los datos han sido ingresados &eacute;xitosamente!', 2, 'green darken-1');
                            location.reload();
                        }else{
                            eWallet.toast('Ha ocurrido un error! Intentalo m&aacute;s tarde...', 2, 'red darken-1');
                        }
                    }
                })
            }
        })

        $(document).on('change', 'input[name=Tprovider]', function(){ getProviders($('input[name=Tprovider]:checked').val()); });

        function providersForm(){
            $.ajax({
                type: 'POST',
                url: 'Empleados/Requisiciones/php/Controller.php',
                data: {getProvidersForOrder: true, idRequisition: formIdRequi.idp.value, type: $("[name=requiType]").val()},
                success: function(res){
                    orderId = JSON.parse(res);

                    if(orderId.error === false) orderId = false;

                    if(orderId && orderId.length > 0){
                        $("#mdlOrder .content").html("");
                        orderId.forEach((el, i) => {
                            $("#mdlOrder .content").append(`
                                <form action="Empleados/Requisiciones/php/Print.php" name="frmOrder_${i}" method="GET" target="_blank" class="row frmProviderOrder">
                                    <h2 style="text-align: center">Factura a: ${el.nombre}</h2>
                                    <input type="hidden" name="idRequision" value="${formIdRequi.idp.value}">
                                    <input type="hidden" name="purchaseOrder" value="true">
                                    <input type="hidden" name="type" value="${$("[name=requiType]").val()}">
                                    <input type="hidden" name="idProvider" value="${el.idProveedor}">
                                    <input type="hidden" name="operationsFlag" value="${$("[name=idProject]").val() === "29"}">
                    
                                    <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                                        <label for="code">C&oacute;digo de la orden: </label>
                                        <input type="text" name="code">
                                    </div>
                                    <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                                        <label for="orderNum">N&uacute;mero de la orden: </label>
                                        <input type="text" name="orderNum">
                                    </div>
                                    <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                                        <label for="sendBy">Enviado por: </label>
                                        <input type="text" name="sendBy" value="Walter Hern&aacute;ndez">
                                    </div>
                                    <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                                        <label for="idHeadquarter">Sede FUSALMO: </label>
                                        <select name="idHeadquarter"></select>
                                    </div>
                                    <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                                        <h4 style="text-align: center">Detalle de pago:</h4>
                                        <input type="checkbox" name="checkPayment[]" id="payment_1_${i}" value="Cr&eacute;dito de 30 d&iacute;as">
                                        <label for="payment_1_${i}">Cr&eacute;dito de 30 d&iacute;as</label><br>
                                        <input type="checkbox" name="checkPayment[]" id="payment_2_${i}" value="Cr&eacute;dito de 15 d&iacute;as">
                                        <label for="payment_2_${i}">Cr&eacute;dito de 15 d&iacute;as</label><br>
                                        <input type="checkbox" name="checkPayment[]" id="payment_3_${i}" value="Cr&eacute;dito de 8 d&iacute;as">
                                        <label for="payment_3_${i}">Cr&eacute;dito de 8 d&iacute;as</label><br>
                                        <input type="checkbox" name="checkPayment[]" id="payment_4_${i}" value="Pago con Cheque">
                                        <label for="payment_4_${i}">Pago con Cheque</label><br>
                                        <input type="checkbox" name="checkPayment[]" id="payment_5_${i}" value="Pago a Cuenta">
                                        <label for="payment_5_${i}">Abono a Cuenta</label><br>
                                    </div>
                                    <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                                        <label for="name">Nombre: </label>
                                        <input type="text" name="name">
                                    </div>
                                    <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                                        <label for="cardNumber">N&uacute;m. de Tarjeta: </label>
                                        <input type="text" name="cardNumber">
                                    </div>
                                    <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                                        <label for="bill">Facturaci&oacute;n: </label>
                                        <input type="text" name="bill" value="Factura de Consumidor final a nombre de FUSALMO">
                                    </div>
                                    <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                                        <label for="expires">Caduca: </label>
                                        <input type="date" name="expires">
                                    </div>
                                    <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                                        <label for="comment">Notas o Comentarios</label>
                                        <textarea name="comment" cols="80" rows="20"></textarea>
                                    </div>
                    
                                    <div class="content-field c-button col l4 m6 s8 offset-l4 offset-m3 offset-s2">
                                        <button id="sendOrder" class="button">Orden de Compra</button>  
                                    </div>
                                </form>
                                <hr>
                            `);

                            orderSelect($(`[name=frmOrder_${i}] [name=idHeadquarter]`));

                            $(`[name=frmOrder_${i}]`).validate({
                                debug: true,
                                rules: {
                                    code: 'required',
                                    orderNum: 'required',
                                    sendBy: 'required',
                                    idHeadquarter: 'required',
                                    checkPayment: 'required',
                                    bill: 'required'
                                },
                                messages: {
                                    code: 'Ingrese un c&oacute;digo',
                                    orderNum: 'Ingrese un n&uacute;mero de orden',
                                    sendBy: 'Ingrese un dato',
                                    idHeadquarter: 'Seleccione una sede',
                                    checkPayment: 'Seleccione al menos un detalle de pago',
                                    bill: 'Ingrese un detalle de facturación'
                                },
                                errorElement : 'p',
                                errorPlacement: function(error, element) {
                                    var placement = $(element).data('error');
                                    if (placement) {
                                        $(placement).append(error)
                                    } else {
                                        error.insertAfter(element);
                                    }
                                },
                                submitHandler: function(form){
                                    if(orderId !== false){
                                        let data = $(form).serialize();
                                        let orderTab = window.open(`https://siifusalmo.org/Empleados/Requisiciones/php/Print.php?${data}`, '_blank');
                                        if(!orderTab){
                                            eWallet.toast('Debes activar las "ventanas emergentes" para poder visualizar TODAS las &oacute;rdenes de compra!', 2, 'red darken-2');
                                            return;
                                        }
                                    }else{
                                        eWallet.toast("La requisición no posee proveedores para generar una orden de compra!", 2, 'red darken-2');
                                        $("#sendOrder").attr('disabled', true);
                                    }
                                },
                                invalidHandler: function(){
                                    eWallet.toast('Ingrese todos lo datos solicitados!', 2, 'red darken-2');
                                }
                            });
                        })
                    }
                }
            })
        }

        if($("[name=FlagProcesado]").val() == "1"){
            providersForm();
        }

        $(document).on('click', '.btnModify', function(){
            $.ajax({
                type: 'POST',
                url: 'Empleados/Requisiciones/php/Controller.php',
                data: {modifyRequisition: true, idRequisition: formIdRequi.idp.value, type: $("[name=requiType]").val()},
                success: function(res){
                    providersSelect = JSON.parse(res.split('|')[1]);
                    $(".table-material").append(res.split('|')[0]);
                    
                    document.querySelector(".buttonProvider").classList.add('visible');
                    $("[action-button=true]").each((i, button) => $(button).attr('disabled', true));
                }
            })
        })
        
        $(document).on('click', 'form[name=frmModifyData] #btnModifyRequisition', function(){
            materials = [];
            let validFlag = false;

            if(providersSelect.length === 1){
                eWallet.toast('Debes seleccionar por lo menos 2 proveedores de materiales!', 2, 'red darken-1');
                return;
            }else if(providersSelect.length === 0){
                eWallet.toast('Debes seleccionar proveedores de materiales!', 2, 'red darken-1');
                $('form[name=frmModifyData] select.cmbProvider').eq(0).focus();
                return;
            }

            $('form[name=frmModifyData] tr[materialrow=true]').each((i, row) => {
                let idMaterial = $(row).attr('idmaterial');
                let idComparative = $(row).attr('idcomparative');
                let idTracing = $(row).attr('idtracing');
                let idProvider = $(row).attr('idprovider');
                let cant = $(row).find('td[materialCant=true]').html();
                let c = 0;
                let inputIndex = parseInt($(row).find(`input[name=${idMaterial}]:checked`).attr('inputindex'));
                
                $(row).find(`input[name=${idMaterial}]`).each((radioIndex, radio) => {c += !radio.checked ? 1 : 0;});
                if(c === $(row).find(`input[name=${idMaterial}]`).length){
                    eWallet.toast('Debes seleccionar un proveedor para cada material en la tabla!', 2, 'red darken-1');
                    return false;
                }
                
                if($(row).find(`.txtUnit`).eq(inputIndex).val().trim() <= 0){
                    eWallet.toast('Ingresa una cantidad en los campos ', 2, 'red darken-1');
                    return false;
                }
                
                
                providersData = [];
                let rowData = [];
                for (var x = 0; x < 3; x++) {
                    $(row).find(`input[name=${idMaterial}]`).each((radioIndex, radio) => {
                        let price = $(row).find(`.txtUnit`).eq(radioIndex).val(),
                            total = $(row).find(`.txtTotal`).eq(radioIndex).val();

                        if($(radio).attr('setprovider') !== undefined){
                            let idProvider = radio.value;
                            providersData[radioIndex] = {
                                idProvider,
                                price: price === "" || isNaN(Number(price)) ? 0.00 : Number(price).toFixed(2),
                                total: price === "" || isNaN(Number(total.substr(1, total.length)))
                                ? 0.00
                                : Number(total.substr(1, total.length)).toFixed(2),
                                selected: radio.checked
                            }
                        }else {
                            providersData[radioIndex] = {
                                idProvider: 1,
                                price: price === "" || isNaN(Number(price)) ? 0.00 : Number(price).toFixed(2),
                                total: price === "" || isNaN(Number(total.substr(1, total.length)))
                                ? 0.00
                                : Number(total.substr(1, total.length)).toFixed(2),
                                selected: false
                            }
                        }
                    });

                }

                materials.push({
                    idComparative,
                    idMaterial,
                    idTracing,
                    idSubLinea: $("[name=idSubLinea]").val(),
                    cant,
                    providers: providersData
                })

                validFlag = $('form[name=frmModifyData] tr[materialrow=true]').length === (i + 1);
            })

            if (validFlag) {
                $.ajax({
                    type: 'POST',
                    url: '/Empleados/Requisiciones/php/Controller.php',
                    data: {updateComparative: true, data: JSON.stringify(materials), prevData: JSON.stringify(prevMaterials), idRequi: formIdRequi.idp.value, type: $("[name=requiType]").val()},
                    success: function(res){
                        $("[action-button=true]").each((i, button) => $(button).removeAttr('disabled', true));
                        document.querySelector(".buttonProvider").classList.remove('visible');
                        if (parseInt(res)) {
                            eWallet.toast('Los datos han sido modificados &eacute;xitosamente!', 2, 'green darken-1');
                            initPage();
                            providersForm();
                        }else{
                            eWallet.toast('Ha ocurrido un error! Intentalo m&aacute;s tarde...', 2, 'red darken-1');
                        }
                    }
                })
            }
        })
    })
})();
