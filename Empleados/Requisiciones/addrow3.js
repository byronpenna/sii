function addRow3() {
    /* Declare variables */
    var elements3, templateRow3, rowCount3, row3, className3, newRow3, element3;
    var i, s, t;
    
    /* Get and count all "tr" elements with class="row".    The last one will
     * be serve as a template. */
    if (!document.getElementsByTagName)
        return false; /* DOM not supported */
    elements3 = document.getElementsByTagName("tr");
    templateRow3 = null;
    rowCount3 = 0;
    for (i = 0; i < elements3.length; i++) {
        row = elements3.item(i);
        
        /* Get the "class" attribute of the row. */
        className3 = null;
        if (row.getAttribute)
            className3 = row3.getAttribute('class')
        if (className3 == null && row3.attributes) {    // MSIE 5
            /* getAttribute('class') always returns null on MSIE 5, and
             * row.attributes doesn't work on Firefox 1.0.    Go figure. */
            className3 = row3.attributes['class'];
            if (className3 && typeof(className3) == 'object' && className3.value) {
                // MSIE 6
                className3 = className3.value;
            }
        } 
        
        /* This is not one of the rows we're looking for.    Move along. */
        if (className3 != "row_to_clone3")
            continue;
        
        /* This *is* a row we're looking for. */
        templateRow3 = row3;
        rowCount3++;
    }
    if (templateRow3 == null)
        return false; /* Couldn't find a template row. */
    
    /* Make a copy of the template row */
    newRow3 = templateRow3.cloneNode(true);

    /* Change the form variables e.g. price[x] -> price[rowCount] */
    elements3 = newRow3.getElementsByTagName("TEXTAREA");
    for (i = 0; i < elements.length; i++) {
        element3 = elements3.item(i);
        s = null;
        s = element3.getAttribute("name");
        if (s == null)
            continue;
        t = s.split("[");
        if (t.length < 2)
            continue;
        s = t[0] + "[" + rowCount3.toString() + "]";
        element3.setAttribute("name", s);
        element3.value = "";
    }
    
    /* Add the newly-created row to the table */
    templateRow3.parentNode.appendChild(newRow3);
    return true;
}

/* set ts=8 sw=4 sts=4 expandtab: */