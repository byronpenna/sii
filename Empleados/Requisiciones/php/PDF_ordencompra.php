<?php
	require '../../../net.php';
	include 'mpdf/mpdf.php';

if(isset($_POST['ordencompra'])){
    header("Content-Type: text/html; charset=utf-8");
    $nombreRequi=$_POST['nombrecomparativas'];
	$idProvider=$_POST['idProvider'];
	$idRequisition =$_POST['idRequisition'];
    $code=$_POST['code'];
    $orderNum = $_POST['orderNum'];
    $sendBy = $_POST['sendBy'];
    $type = $_POST['type'];
    $card = $_POST['cardNumber'];
    $expire = $_POST['expires'];
    $comment = $_POST['comment'];
    $payments=$_POST['checkPayment'];
    $bill = $_POST['bill'];
    $name = $_POST['name'];
    $idHeadquarter = $_POST['idHeadquarter'];
    $total=0;

    //Convierte los caracteres raros
    define('CHARSET', 'ISO-8859-1');
    define('REPLACE_FLAGS', ENT_COMPAT | ENT_XHTML);
    function html($string) {
        return htmlentities($string, REPLACE_FLAGS, CHARSET);
    }

	$days = array(
				"Monday" => "Lunes",
				"Tuesday" => "Martes",
				"Wednesday" => "Miércoles",
				"Thursday" => "Jueves",
				"Friday" => "Viernes",
				"Saturday" => "Sábado",
				"Sunday" => "Domingo");

	$months = array(
				"Jan" => "enero",
				"Feb" => "febrero",
				"Mar" => "marzo",
				"Apr" => "abril",
				"May" => "mayo",
				"Jun" => "junio",
				"Jul" => "julio",
				"Aug" => "agosto",
				"Sep" => "septiembre",
				"Oct" => "octubre",
				"Nov" => "noviembre",
				"Dec" => "diciembre"
			);

	$longDate = "";
    $table = $type === "Materiales" ? "Material" : "Alimentacion";
    ini_set("date.timezone", 'America/El_Salvador');

    $longDate .= $days[date("l")] . ", " . date("j") . " de " . $months[date("M")] . " de " . date("Y");
    $content = "";

	$query = $bddr->prepare("SELECT * FROM request_proveedor WHERE idProveedor = $idProvider;");
	$query->execute();
	$providerData = $query->fetchAll();

    $idtipo = $type === 'Materiales' ? 'IdMaterial' : 'IdAlimentacion';

    $headquarterQuery = $bddr->prepare("SELECT * FROM sedes_fusalmo WHERE idSede = $idHeadquarter");
    $headquarterQuery->execute();
    $hData = $headquarterQuery->fetchAll();

        $dataQuery = ("SELECT * FROM Request_Seguimiento2_$table rsm 
                inner join Request_Comparativa_$table rcm 
                on rsm.idComparativa = rcm.idComparativa 
                inner join Request_$table rt 
                on rt.$idtipo = rcm.Id_Material 
                where rsm.idProveedor = $idProvider 
                and rcm.Nombre = '$nombreRequi'");

    $query2 = $bddr->prepare($dataQuery);
    $query2->execute();
	$content .= "
    <div class='logo_cont'>
        <img class='logo' style='margin-left:10px;float:left;' src='../img/fusalmo.png'>
        <h2 style='margin-left:105px;'>Fundación Salvador del Mundo
        <p style='font-size:9px;margin-left:-100px; margin-top:-5px; color:rgb(16, 40, 79);'>Centro Juvenil Don Bosco, Polideportivo
            Soyapango, Plaza España.
        <br>Soyapango, San Salvador.
        <br>Codigo Postal: 1101
        <br>Tel.2259-2000 </p></h2>
        
    </div>
    <div class='code'>COP $code</div>
        <div class='data'>
            <div class='cont Provider'>
                    <div class='fieldTitle'>
                        <span> Proveedor </span>
                    </div>   
                <div class='cont_cont'>
                    <div class='title'>Nombre: </div>
                    <div class='content'>" . html($providerData[0]['nombre']). "</div>
                </div>
                <div class='cont_cont'>
                    <div class='title'>Dirección: </div>
                    <div class='content'>" .html($providerData[0]['direccion']). "</div>
                </div>
                <div class='cont_cont xtra'>
                    <span class='title'>Ciudad: </span>
                    <span class='content'>" .html($providerData[0]['ciudad']). "</span> &nbsp;&nbsp;&nbsp;
                    <span class='title2'>Estado: </span>
                    <span class='content2'>" .html($providerData[0]['estado']). "</span>
                </div>
                <div class='cont_cont xtra'>
                    <span class='title'>Teléfono: </span>
                    <span class='content'>" . $providerData[0]['telefono'] . "</span> &nbsp;&nbsp;&nbsp;
                    <span class='title2'>Código Postal: </span>
                    <span class='content2'>" . $providerData[0]['codigo_postal'] . "</span>
                </div>
            </div>
            <div class='cont Fusalmo'>
                <div class='fieldTitle'>
                    <span> Enviar a </span>
                </div>   
                <div class='cont_cont'>
                    <div class='title'>Nombre: </div>
                    <div class='content'>FUSALMO \"Fundación Salvador del Mundo\"</div>
                </div>
                <div class='cont_cont'>
                    <div class='title'>Dirección: </div>
                    <div class='content'>" .html($hData[0]['direccion']). "</div>
                </div>";
                    
    if($operationsFlag === "true"){
        $content .= "
                <div class='cont_cont xtra'>
                    <span class='title2'>NIT: </span>
                    <span class='content2'>0614-170801-104-0</span> &nbsp;&nbsp;&nbsp;
                    <span class='title'>NRC: </span>
                    <span class='content'>136738-7</span> 
                </div>
        ";
    }
    
    $content .= "
                <div class='cont_cont'>
                    <div class='title'>Ciudad: </div>
                    <div class='content'>" .html($hData[0]['ciudad']). "</div>
                </div>
                <div class='cont_cont xtra'>
                    <span class='title'>Teléfono: </span>
                    <span class='content'>" . $hData[0]['telefono'] . "</span> &nbsp;&nbsp;&nbsp;
                    <span class='title2'>Código Postal: </span>
                    <span class='content2'>" . $hData[0]['codigo_postal'] . "</span>
                </div>
            </div>
        </div><br>
        <table>
            <tr>
                <th>Cantidad</th>
                <th>Tipo de Unidad</th>
                <th>Descripción</th>
                <th>Precio Unitario</th>
                <th>TOTAL</th>
            </tr>";

    while ($row2 = $query2->fetch()) {
        $cantid = $row2[4];
        $costo = $row2['Costo'];
        $tot= number_format($cantid*$costo,2);
        $content .= "
            <tr>
                <td>".$row2[4]."</td>
                <td>".html(($providerData[0]['tipo'] == "M" ? "Material" : "Alimento")). "</td>
                <td>". html($row2[$type === 'Materiales' ? 'Material': 'Tipo'])."</td>
                <td>$ ".number_format($row2['Costo'], 2)."</td>
                <td>$ ".$tot."</td>
            </tr>";
        $total += $row2['Costo'] * $row2[4];
    }

    $content .= "
            <tr class='total-row'>
                <th colspan='4'>TOTAL</th>
                <th>$" . number_format($total, 2) . "</th>
            </tr>
        </table>

        <div class='payment'>
        <div class='fieldTitle'>
            <span>Detalle de Pago</span>
        </div>    
            <ul>";
    if (count($payments) > 0) {
        foreach ($payments as $payment) {
             $content .= "<li>".html($payment)."</li>";
        }
    }
    $content .= "</ul>
            <div class='cont_cont'>
                <div class='title'>Nombre: </div>
                <div class='content'>".html($name)."</div>
            </div>
            <div class='cont_cont'>
                <div class='title'>Núm. de tarjeta: </div>
                <div class='content'>$card</div>
            </div>
            <div class='cont_cont'>
                <div class='title'>Caduca: </div>
                <div class='content'>$expire</div>
            </div>
        </div>

        <div class='xtraData'>
            <div class='xtra date'>
                <div class='fieldTitle' style='margin-top: -20px;'>
                    <span>Fecha de Envío</span>
                </div>
                <span class='content'>$longDate</span>
            </div>
            <div class='xtra bill'>
                <div class='fieldTitle' style='margin-top: -20px;'>
                    <span>Facturación</span>
                </div>
                <span class='content'>$bill</span>
            </div>
        </div>

        <div class='xtraData xtrax2'>
            <div class='xtra a'>
                <div class='fieldTitle'>
                    <span>Aprobado</span>
                </div>
                <br><br><br>
                <p style='text-align: center;'>Lic. José Wilfredo Iglesias Osegueda<br>
                Gerente Administrativo Financiero</p>
            </div>
            <div class='xtra order'>
                <div class='cont_cont'>
                    <div class='title'>Fecha: </div>
                    <div class='content'>" .date("d/m/Y") . "</div>
                </div>
                <div class='cont_cont'>
                    <div class='title'>Pedido núm: </div>
                    <div class='content'>$orderNum</div>
                </div>
                <div class='cont_cont'>
                    <div class='title'>Representante: </div>
                    <div class='content'>José Wilfredo Iglesias</div>
                </div>
                <div class='cont_cont'>
                    <div class='title'>Enviar por: </div>
                    <div class='content'>".html($sendBy)."</div>
                </div>
            </div>
        </div>

        <div class='xtraData xtra3'>
            <div class='fieldTitle' style='margin-top: -11px;'>
                <span style='font-size: 15px;'>Notas o Comentarios</span>
                <p style='font-size: 11px; padding: 0px 15px;'>".html($comment)."</p>
            </div>
        </div>
        
        ";



	$pdf = new mPDF('utf-8', 'A4', 0, '', 5, 5, 10, 0, 0, 0, 'P');
	$pdf->allow_charset_conversion=true;
	$pdf->charset_in='UTF-8';
	$content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');
	$stylesheet= file_get_contents('Classes/purchaseOrder.css');
	$stylesheet2 = file_get_contents('Classes/colors.css');
	$pdf->WriteHTML($stylesheet2,1);
	$pdf->WriteHTML($stylesheet,1);
	$pdf->WriteHTML($content,2);
	$pdf->Output();
	exit;
}



?>