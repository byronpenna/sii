<?php
    require_once 'Connection.php';
	require_once 'Classes/Provider.php';
    require_once 'mpdf60/mpdf.php';
	require_once 'Classes/Requisicion.php';

	class Print_Class
	{
		private $mpdf;
		private $db;
		private $_print;
		private $stylesheet;
        private $provider;
		private $requisicion;
		function __construct()
		{
			$this->db = new Connection();
			$this->db->Connect();
            $this->db = $this->db->connection;

			$this->provider = new Provider();
			$this->requisicion = new Requisicion();
		}

		private function genHeader($title)
		{
			ini_set("date.timezone", 'America/El_Salvador');
			$date = date("d/m/Y");
			$aux .= "
			<div class='PDF_header'>
				<div class='logo_cont'>
					<img class='logo' src='../img/fusalmo.png'>
					<h2>Fundación Salvador del Mundo</h2>
				</div>
				<div class='title'>
					<h3>$title</h3>
				</div>
			</div>";

			$this->mpdf->WriteHTML($aux, 2);
		}

		private function openPDF($title, $name)
		{
			$this->mpdf->WriteHTML($this->stylesheet, 1);
			$this->genHeader($title);
			$this->mpdf->WriteHTML($this->_print, 2);
			$this->mpdf->Output($name.".pdf", "I");
		}

        public function getPurchaseOrder($idP, $idR,  $idH, $code, $orderNum, $sendBy, $type, $name, $card, $expire, $comment, $payments, $bill, $operationFlag)
        {
            $this->stylesheet = file_get_contents('Classes/purchaseOrder.css');
			$auxCSS = file_get_contents('Classes/colors.css');

            $this->_print = $this->provider->purchaseOrder($idP, $idR, $idH, $code, $orderNum, $sendBy, $type, $name, $card, $expire, $comment, $payments, $bill, $operationFlag);
            $this->mpdf = new mPDF('utf-8', 'A4', 0, '', 5, 5, 0, 0, 0, 0, 'P');
            $this->mpdf->WriteHTML($auxCSS, 1);
            $this->openPDF("Orden de Compra", "Orden de Compra - $orderNum");
        }

		public function PrintComparative($idRequisicion, $type, $maker, $payments)
		{
			$this->mpdf = new mPDF('utf-8', 'A4-L', 0, '', 10, 10, 10, 0, 0, 0, 'L');
			$css = file_get_contents('Classes/cuadro.css');
			$this->mpdf->WriteHTML($css, 1);
			$this->_print = $this->requisicion->getComparative($idRequisicion, $type, $maker, $payments);
			$this->OpenPDF('Cuadro Comparativo', 'Cuadro-Comparativo');
			echo $this->_print;
		}
		
		public function PrintQuotation($idRequi, $inicio, $placeDate, $place, $validity, $warranty, $note, $nameResponsable, $type){
			$this->stylesheet = file_get_contents('Classes/Quotation.css');
			$auxCSS = file_get_contents('Classes/colors.css');

			$this->_print = $this->requisicion->getQuotation($idRequi, $inicio, $placeDate, $place, $validity, $warranty, $note, $nameResponsable, $type);
			$this->mpdf = new mPDF('utf-8', 'A4', 0, '', 5, 5, 0, 0, 0, 0, 'P');
			$this->mpdf->WriteHTML($auxCSS, 1);
			$this->openPDF("Solicitud de Cotización", "Solicitud-Cotización");
		}
	}

	$print = new Print_Class();

	if(isset($_REQUEST['purchaseOrder'])){
		$print->getPurchaseOrder(($_REQUEST['idProvider']), ($_REQUEST['idRequision']), ($_REQUEST['idHeadquarter']), ($_REQUEST['code']), ($_REQUEST['orderNum']), ($_REQUEST['sendBy']), ($_REQUEST['type']), ($_REQUEST['name']), ($_REQUEST['cardNumber']), ($_REQUEST['expires']), ($_REQUEST['comment']), ($_REQUEST['checkPayment']), ($_REQUEST['bill']), $_REQUEST['operationsFlag']);
	}

	if(isset($_REQUEST['comparation'])){
		$print->PrintComparative($_REQUEST['idRequision'], $_REQUEST['type'], $_REQUEST['maker'], $_REQUEST['payment']);
	}

	if(isset($_REQUEST['Quotation'])){ //Impresión de la solicitud de cotizacion
		$print->PrintQuotation($_REQUEST['idRequi'], $_REQUEST['inicio'], $_REQUEST['placeDate'], $_REQUEST['place'], $_REQUEST['validity'], $_REQUEST['warranty'], $_REQUEST['note'], $_REQUEST['nameResponsable'], $_REQUEST['type']);
	}
?>
