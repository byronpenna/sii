<?php

    class Requisicion{
        private $db;
        private $provider;
        public function __construct()
        {
            require_once('Connection.php');
            $this->db = new Connection();
            $this->db->connect();
            $this->db = $this->db->connection;

            require_once('Provider.php');
            $this->provider = new Provider();
        }


        public function html($string) {
            return htmlentities($string);
        }


        public function loadMaterial($idRequisicion, $type){
            $table = $type === "Materiales" ? "Material" : "Alimentacion";
            try{
                $this->db->beginTransaction();
                $query = $this->db->prepare("SELECT * FROM Request_$table WHERE IdRequisicion = :id");
                if($query->execute(array(":id" => $idRequisicion))){
                    $this->db->commit();
                    if($this->checkState($idRequisicion)){
                        $form = "0";
                    }else{
                        if($query->rowCount()){ //Se verifica el número de filas
                            $form = "
                                <center>
                                <table class='table' style='width:85%; margin-bottom:3%; margin-top:1%;' >
                                    <thead>
                                        <tr>
                                            <th rowspan='3'>Comparar</th>
                                            <th rowspan='3'>Cantidad</th>
                                            <th rowspan='3'>Descripci&oacute;n</th>
                                            <th rowspan='3'>Especificaci&oacute;n</th>
                                            
                                        </tr>
                                        
                                        
                                    </thead>
                                    <tbody>
                            ";
                            $i = 0;
                            while($row = $query->fetch(PDO::FETCH_ASSOC)){ //Recorrer
                                $form .= "
                                    <tr materialRow='true' idMaterial='" . $row[$type == 'Alimentacion' ? 'IdAlimentacion' : 'IdMaterial'] . "'>

                                        <td ><input type='checkbox' name='idProducto' id='myCheck" .($i + 1) . "' class='micheckbox'  value='".$row[$type == 'Alimentacion' ? 'IdAlimentacion' : 'IdMaterial']."'></td>

                                        <td materialCant='true'>" . $row['Cantidad_aprobada'] . "</td>
                                        <td materialName='true'>" .$this->html($row[$type == 'Alimentacion' ? 'Tipo' : 'Material']). "</td>
                                        <td materialEsp='true'>" .$this->html($row['Espe_Tecnica']). "</td>
                                        
                                    </tr>
                                ";
                                $i++;
                            }
                            $form .= "
                                        </tbody>

                                    </table>
                                    
                                </center>
                            ";
                        }else{
                            $form = "-1";
                        }
                    }
                    return $form;
                }
            }catch(Exception $e){
                echo $e->getMessage();
            }
            return 0;
        }

        public function insertComparation($data, $idRequi, $type)
        {
            try {
                $this->db->beginTransaction();
                $table = $type === "M" ? "Material" : "Alimentacion";
                $selectedFlag = true;
                $idMaterial = $cant = $idProv =  $idProv1 = $idProv2 = $idProv3 = $price = $price1 =  $price2 = $price3 = $total1 = $total2 = $total3 = $selected = $idComparativa = $idSubLinea = null;
                //1ro. Comparativa
                //(ID, Id_Material, Cantidad_aprobada, Proveedor1, Costo1, CostoTotal1, Proveedor2, Costo2, CostoTotal2, Proveedor, Costo3, CostoTotal3, seleccionada, IdSubLineaPresupuestaria)
                $queryComparamiento = $this->db->prepare("INSERT INTO Request_Comparativa_$table VALUES(NULL, :idMaterial, :cant, :idProv1, :price1, :total1, :idProv2, :price2, :total2, :idProv3, :price3, :total3, :idSubLinea);");
                $queryComparamiento->bindParam(":idMaterial", $idMaterial);
                $queryComparamiento->bindParam(":cant", $cant);
                $queryComparamiento->bindParam(":idProv1", $idProv1);
                $queryComparamiento->bindParam(":price1", $price1);
                $queryComparamiento->bindParam(":total1", $total1);
                $queryComparamiento->bindParam(":idProv2", $idProv2);
                $queryComparamiento->bindParam(":price2", $price2);
                $queryComparamiento->bindParam(":total2", $total2);
                $queryComparamiento->bindParam(":idProv3", $idProv3);
                $queryComparamiento->bindParam(":price3", $price3);
                $queryComparamiento->bindParam(":total3", $total3);
                $queryComparamiento->bindParam(":idSubLinea", $idSubLinea);

                //2do. Seguimiento (Sí está seleccionado)
                //(ID, idSeguimiento, Id_Material, Costo, Cantidad_aprobada, idProveedor, IdSubLineaPresupuestaria)
                $querySeguimiento = $this->db->prepare("INSERT INTO Request_Seguimiento2_$table VALUES(NULL, :idComparativa, :idMaterial, :price, :cant, :idProv, :idSubLinea);");
                $querySeguimiento->bindParam(":idComparativa", $idComparativa);
                $querySeguimiento->bindParam(":idMaterial", $idMaterial);
                $querySeguimiento->bindParam(":price", $price);
                $querySeguimiento->bindParam(":cant", $cant);
                $querySeguimiento->bindParam(":idProv", $idProv);
                $querySeguimiento->bindParam(":idSubLinea", $idSubLinea);                

                for ($i=0; $i < count($data); $i++) {
                    $selectedFlag = true;
                    $idMaterial = $data[$i]['idMaterial'];
                    $cant = $data[$i]['cant'];
                    $idSubLinea = $data[$i]['idSubLinea'];
                    $idProv1 = $data[$i]['providers'][0]['idProvider'];
                    $price1 = $data[$i]['providers'][0]['price'];
                    $total1 = $data[$i]['providers'][0]['total'];
                    $idProv2 = $data[$i]['providers'][1]['idProvider'];
                    $price2 = $data[$i]['providers'][1]['price'];
                    $total2 = $data[$i]['providers'][1]['total'];
                    $idProv3 = $data[$i]['providers'][2]['idProvider'];
                    $price3 = $data[$i]['providers'][2]['price'];
                    $total3 = $data[$i]['providers'][2]['total'];
                    $selected = $data[$i]['providers'][2]['total'];

                    $queryComparamiento->execute();

                    for ($z=0; $z < count($data[$i]['providers']); $z++) {
                        if ($data[$i]['providers'][$z]['selected'] && $selectedFlag) {
                            $selectedFlag = false;
                            $price = $data[$i]['providers'][$z]['price'];
                            $idProv = $data[$i]['providers'][$z]['idProvider'];
                            $idComparativa = $this->db->lastInsertId();

                            $querySeguimiento->execute();
                        }
                    }
                }
                if($this->changeState($idRequi)){
                    $this->db->commit();
                    return 1;
                }
            } catch (Exception $e) {
                $this->db->rollback();
                echo $e->getMessage();
                return 0;
            }
        }

        private function changeState($idRequisicion){
            try{
                $query = $this->db->prepare("UPDATE Request_Requisiciones SET procesado = 1 WHERE Idrequisicion = $idRequisicion");
                if($query->execute()){ return 1; }
            }catch(Exception $e){
                echo $e->getMessage();
            }
            return 0;
        }

        public function getComparative($idRequisicion, $type, $maker, $payments){
            $table = $type === "Materiales" ? "Material" : "Alimentacion";
            try{
                $this->db->beginTransaction();
                $query = $this->db->prepare("SELECT c.IdComparativa, c.Id_Material, c.Cantidad_aprobada, c.Costo1, c.CostoTotal1, c.Costo2, c.CostoTotal2, c.Costo3, c.CostoTotal3, m." . ($type == "Materiales" ? "Material" : "Tipo") . ", c.Proveedor1, c.Proveedor2, c.Proveedor3 FROM Request_Comparativa_$table c INNER JOIN Request_$table m ON c.Id_Material = m." . ($type == "Materiales" ? "IdMaterial" : "IdAlimentacion") . " WHERE m.IdRequisicion = :id");
                
                $total = [ //Array que guarda la suma de cada dato - Se imprime en el footer de la tabla
                    'total1' => 0,
                    'totalComprar1' => 0,
                    'total2' => 0,
                    'totalComprar2' => 0,
                    'total3' => 0,
                    'totalComprar3' => 0
                ];
                $providers = $this->GetProviders($idRequisicion, $type); //Guarda el array de proveedores en orden
                $proyect = $this->getDataProyect($idRequisicion); //Guarda los datos del proyecto de la respectiva requiscion
                $tabla = "
                <br>
                <div class='PDF-Cuadro'>
                    <ul class='datos'>
                        <li><a><b>Nombre del Proyecto:</b> ".$proyect['Nombre']."</a></li>
                        <li>
                            <a><b>Fecha:</b> ".$proyect['fechaSolicitud']."</a>
                        </li>
                        <li>
                            <a class='titulo'><b>Empresas que ofertaron:</b> </a>
                            <ul>";
                                for($i = 0; $i < count($providers); $i++){
                                    if($providers[$i]['nombre'] != 'NULO'){
                                        $tabla .= "<li class='providers'>".$providers[$i]['nombre']."</li>";
                                    }
                                }
                $tabla.="  </ul>
                        </li>
                    </ul>
                    <br>
                    <table class='borde'>
                        <thead>
                            <tr>
                                <th style='width: 5%;' rowspan='2'>Cant.</th>
                                <th style='width: 35%;' rowspan='2'>Descripción</th>";
                                for($i = 0; $i < count($providers); $i++){
                                    $providers[$i]['nombre'] = $providers[$i]['nombre'] != 'NULO' ? $providers[$i]['nombre'] : ' - ';
                                    $tabla .= "<th style='width: 20%;' colspan='2'>" . wordwrap($providers[$i]['nombre'], 20, '<br>', true) . "</th>";
                                }
                $tabla .="  </tr>
                            <tr>
                                <th style='width: 10%;'>PU</th>
                                <th style='width: 10%;'>Total</th>
                                <th style='width: 10%;'>PU</th>
                                <th style='width: 10%;'>Total</th>
                                <th style='width: 10%;'>PU</th>
                                <th style='width: 10%;'>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                ";
                if($query->execute(array(':id' => $idRequisicion))){
                    while($row = $query->fetch(PDO::FETCH_ASSOC)){

                        $class = [ //Arreglo que sirve para ver que datos fueron los seleccionados
                            '1' => $this->checkSegumiento($row['IdComparativa'], $row['Proveedor1'], $type) == 1 ? 'select' : '',
                            '2' => $this->checkSegumiento($row['IdComparativa'], $row['Proveedor2'], $type) == 1 ? 'select' : '',
                            '3' => $this->checkSegumiento($row['IdComparativa'], $row['Proveedor3'], $type) == 1 ? 'select' : ''
                        ];

                        //En cada indice del arreglo se va guardando la suma de los datos iterados
                        $total['total1'] += $row['CostoTotal1'];
                        $total['total2'] += $row['CostoTotal2'];
                        $total['total3'] += $row['CostoTotal3'];
                        $total['totalComprar1'] += $class['1'] == 'select' ? $row['CostoTotal1'] : 0;
                        $total['totalComprar2'] += $class['2'] == 'select' ? $row['CostoTotal2'] : 0;
                        $total['totalComprar3'] += $class['3'] == 'select' ? $row['CostoTotal3'] : 0;

                        $tabla  .= "
                            <tr>
                                <td style='width: 5%;'> " .wordwrap($row['Cantidad_aprobada'], 6, '<br>', true)."</td>
                                <td style='width: 35%;'> ".wordwrap($row[$type == "Materiales" ? "Material" : "Tipo"], 39, '<br>', true)."</td>
                                <td style='width: 10%;' class='".$class['1']."'> $".wordwrap($row['Costo1'], 6, '<br>', true)."</td>
                                <td style='width: 10%;' class='".$class['1']."'> $".wordwrap($row['CostoTotal1'], 6, '<br>', true)." </td> 
                                <td style='width: 10%;' class='".$class['2']."'> $".wordwrap($row['Costo2'], 6, '<br>', true)." </td>
                                <td style='width: 10%;' class='".$class['2']."'> $".wordwrap($row['CostoTotal2'], 6, '<br>', true)." </td>
                                <td style='width: 10%;' class='".$class['3']."'> $".wordwrap($row['Costo3'], 6, '<br>', true)." </td>
                                <td style='width: 10%;' class='".$class['3']."'> $".wordwrap($row['CostoTotal3'], 6, '<br>', true)." </td>
                            </tr>
                        ";
                    }
                }
                $tabla .= "
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td>Total</td>
                                <td></td>
                                <td> $".wordwrap(number_format($total['total1'], 2), 6, '<br>', true)."</td>
                                <td></td>
                                <td> $".wordwrap(number_format($total['total2'], 2), 6, '<br>', true)."</td>
                                <td></td>
                                <td> $".wordwrap(number_format($total['total3'], 2), 6, '<br>', true)."</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Total a comprar</td>
                                <td colspan='2'> $".wordwrap(number_format($total['totalComprar1'], 2), 6, '<br>', true)."</td>
                                <td colspan='2'> $".wordwrap(number_format($total['totalComprar2'], 2), 6, '<br>', true)."</td>
                                <td colspan='2'> $".wordwrap(number_format($total['totalComprar3'], 2), 6, '<br>', true)."</td>
                            </tr>
                        </tfoot>
                    </table>
                    <br>
                    <ul>
                        <a class='titulo'><b>Terminos y Condiciones:</b> </a>
                        <ul>";
                if(count($payments) > 0){
                    foreach ($payments as $payment) {
                        $tabla .= "<li>$payment</li>";
                    }
                }
                $tabla .= "</ul>
                    </ul>
                    <br>
                    <table class='staff'>
                        <tbody>
                            <tr>
                                <td class='firm' colspan='3'></td>
                                <td></td>
                                <td class='firm' colspan='3'></td>
                                <td></td>
                                <td class='firm' colspan='3'></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan='3'>$maker</td>
                                <td></td>
                                <td colspan='3'>Lic. José Wilfredo Iglesias</td>
                                <td></td>
                                <td colspan='3'>Lic. Oscar Nelson Cruz</td> 
                            </tr>
                            <tr>
                                <td colspan='3'>Elaborado por</td>
                                <td></td>
                                <td colspan='3'>VoBo</td>
                                <td></td>
                                <td colspan='3'>Autorizado</td> 
                            </tr>
                        <tfoot>
                    </table>
                </div>
                ";
                return $tabla;
            }catch(Exception $e){
                echo $e->getMessage();
            }
            return 0;
        }

        private function GetProviders($idRequisicion, $type){ //Obtiene los proveedores en orden según la requisición
            try{
                $table = $type === "Materiales" ? "Material" : "Alimentacion";
                $query = $this->db->prepare("SELECT c.Proveedor1, c.Proveedor2, c.Proveedor3 FROM Request_Comparativa_$table c INNER JOIN Request_$table m ON c.Id_Material = m." . ($type === "Materiales" ? "IdMaterial" : "IdAlimentacion ") . " WHERE m.IdRequisicion = :id LIMIT 1");
                if($query->execute(array(':id' => $idRequisicion))){
                    $row = $query->fetch(PDO::FETCH_ASSOC); //Fila con los id de los proveedores

                    $providers1 = [$row['Proveedor1'], $row['Proveedor2'], $row['Proveedor3']]; //Array de los proveedores obtenidos

                    //Consulta para obtener dato de los proveedores
                    $query = $this->db->prepare("SELECT nombre, idProveedor FROM request_proveedor WHERE idProveedor = :id1 OR idProveedor = :id2 OR idProveedor = :id3");
                    $query->execute(array(':id1' => $row['Proveedor1'], ':id2' => $row['Proveedor2'], ':id3' => $row['Proveedor3']));
                    $providers = [];
                    $i = 0;
    
                    while($row = $query->fetch(PDO::FETCH_ASSOC)){ //Se agregan los datos de los proveedores
                        $providers[$i] = [
                            "id" => $row['idProveedor'],
                            "nombre" => $row['nombre']
                        ];
                        $i++;
                    }
                    $providers2 = [];

                    for($i = 0; $i < 3; $i++){ //Se guardan los proveedores en orden
                        for($x = 0; $x < 3; $x++){
                            if($providers[$i]['id'] == $providers1[$x]){
                                $providers2[$x]['id'] = $providers[$i]['id'];
                                $providers2[$x]['nombre'] = $providers[$i]['nombre'];
                            }
                        }
                    }

                    return $providers2;
                }
            }catch(Exception $e){
                echo $e->getMessage();
            }
            return 0;
        }

        private function checkSegumiento($idComparativa, $idProvider, $type){
            try{
                $table = $type === "Materiales" ? "Material" : "Alimentacion";
                if($idProvider != -1 ){
                    $query = $this->db->prepare("SELECT * FROM Request_Seguimiento2_$table WHERE idComparativa = :idComparativa AND idProveedor = :idProvider");
                    if($query->execute(array(':idComparativa' => $idComparativa, ':idProvider' => $idProvider))){
                        if($query->rowCount() > 0){ return 1; }
                    }
                }
            }catch(Exception $e){
                echo $e->getMessage();
            }
            return 0;
        }

        private function checkState($id){
            try{
                $query = $this->db->prepare("SELECT * FROM Request_Requisiciones WHERE Idrequisicion = $id AND procesado = 1");
                if($query->execute()){
                    if($query->rowCount() > 0){ return 1; }
                }
            }catch(Exception $e){
                echo $e->getMessage();
            }
            return 0;
        }

        private function getDataProyect($idRequisicion){ //Obtiene los datos de un proyecto según requisición
            try{
                $query = $this->db->prepare('SELECT p.Proyecto , r.Fecha_Solicitud, r.Fecha_entrega FROM Request_Requisiciones r INNER JOIN Request_Proyecto p ON r.Idproyecto = p.IdProyecto WHERE r.Idrequisicion =:id');
                if($query->execute(array(':id' => $idRequisicion))){
                    if($query->rowCount() > 0){
                        $row = $query->fetch(PDO::FETCH_ASSOC);
                        $proyect = [
                            'Nombre' => $row['Proyecto'], 
                            'fechaSolicitud' => $row['Fecha_Solicitud'],
                            'FechaEntrega' => $row['Fecha_entrega'] 
                        ];
                        return $proyect;
                    }
                }
            }catch(Exception $e){
                echo $e->getMessage();
            }
            return 0;
        }

        public function tableUpdate($idRequi, $type){
            $table = $type === "Materiales" ? "Material" : "Alimentacion";
            try{
                $query = $this->db->prepare("SELECT * FROM Request_Comparativa_$table c INNER JOIN Request_$table r ON c.id_Material = r.Id$table WHERE r.IdRequisicion = :id");
                $total = [ //Array que guarda la suma de cada dato - Se imprime en el footer de la tabla
                    'total1' => 0,
                    'totalComprar1' => 0,
                    'total2' => 0,
                    'totalComprar2' => 0,
                    'total3' => 0,
                    'totalComprar3' => 0
                ];
                $providerSelect = $this->GetProviders($idRequi, $type);
                $type = $type === "Materiales" ? "M" : "A";
                $providers = json_decode($this->provider->getProviders($type), 1);
                if($query->execute(array(":id" => $idRequi))){

                    if($query->rowCount()){ //Se verifica el número de filas
                        $form = "
                        <div>
                        <form class='frmModifyData' name='frmModifyData' style='width: 100px;'>
                            <div style='display: none;'>
                                <label> Tipo de proveedor: </label>
                                <input name='Tprovider' type='radio' value='M' checked> <label>Material</label>
                                <input name='Tprovider' type='radio' value='A'> <label>Alimento</label>
                            </div>
                            <table>
                                <thead>
                                    <tr>
                                        <th rowspan='3'>Cantidad</th>
                                        <th rowspan='3'>Descripci&oacute;n</th>
                                        <th rowspan='3'>Especificaci&oacute;n</th>
                                        <th colspan='12'>Proveedores</th>
                                    </tr>
                                    <tr>
                                        <th colspan='3'>
                                            <select class='cmbProvider' name='' id='proveedor1'>
                                                <option selected disabled>Proveedor 1</option> ";
                                                for($x = 0; $x < count($providers); $x++){
                                                    if(($providers[$x]['IdProveedor'] == $providerSelect[0]['id']) &&  ($provider[$x]['IdProveedor'] != 1)){
                                                        $form .="<option selected value='".$providers[$x]['IdProveedor']."'>".$providers[$x]['nombre']."</option> ";
                                                    }else{
                                                        $form .="<option value='".$providers[$x]['IdProveedor']."'>".$providers[$x]['nombre']."</option> ";
                                                    }
                                                }
                        $form .="           </select>
                                        </th>
                                        <th colspan='3'>
                                            <select class='cmbProvider' name='' id='proveedor2'>
                                                <option selected disabled>Proveedor 2</option> ";
                                                for($x = 0; $x < count($providers); $x++){
                                                    if(($providers[$x]['IdProveedor'] == $providerSelect[1]['id']) && ($provider[$x]['IdProveedor'] != 1)){
                                                        $form .="<option selected value='".$providers[$x]['IdProveedor']."'>".$providers[$x]['nombre']."</option> ";
                                                    }else{
                                                        $form .="<option value='".$providers[$x]['IdProveedor']."'>".$providers[$x]['nombre']."</option> ";
                                                    }
                                                }
                        $form .="           </select>
                                        </th>
                                        <th colspan='3'>
                                            <select class='cmbProvider' name='' id='proveedor3'>
                                                <option selected disabled>Proveedor 3</option> ";
                                                for($x = 0; $x < count($providers); $x++){
                                                    if(($providers[$x]['IdProveedor'] == $providerSelect[2]['id']) && ($provider[$x]['IdProveedor'] != 1)){
                                                        $form .="<option selected value='".$providers[$x]['IdProveedor']."'>".$providers[$x]['nombre']."</option> ";
                                                    }else{
                                                        $form .="<option value='".$providers[$x]['IdProveedor']."'>".$providers[$x]['nombre']."</option> ";
                                                    }
                                                }
                        $form .="           </select>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Selecci&oacute;n</th>
                                        <th>Prec.Unit.</th>
                                        <th>Total</th>
                                        <th>Selecci&oacute;n</th>
                                        <th>Prec.Unit.</th>
                                        <th>Total</th>
                                        <th>Selecci&oacute;n</th>
                                        <th>Prec.Unit.</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                        ";
                        $i = 0;
                        $type = $type === "M" ? "Materiales" : "Alimentacion";
                        while($row = $query->fetch(PDO::FETCH_ASSOC)){ //Recorrer

                            $tracingQuery = $this->db->prepare("SELECT * FROM Request_Seguimiento2_$table rs INNER JOIN Request_Comparativa_$table rc ON rs.idComparativa = rc.idComparativa WHERE rc.idComparativa = " . $row['idComparativa'] . ";");
                            if(!$tracingQuery->execute()){return 0;};
                            $tracingFlag = $tracingQuery->rowCount() > 0;

                            $tracingData = $tracingFlag ? $tracingQuery->fetch() : false;

                            $class = [ //Arreglo que sirve para ver que datos fueron los seleccionados
                                '1' => $this->checkSegumiento($row['idComparativa'], $row['Proveedor1'], $type) == 1 ? true : false,
                                '2' => $this->checkSegumiento($row['idComparativa'], $row['Proveedor2'], $type) == 1 ? true : false,
                                '3' => $this->checkSegumiento($row['idComparativa'], $row['Proveedor3'], $type) == 1 ? true : false
                            ];

                            //En cada indice del arreglo se va guardando la suma de los datos iterados
                            $total['total1'] += $row['Costo1'];
                            $total['total2'] += $row['Costo2'];
                            $total['total3'] += $row['Costo3'];
                            $total['totalComprar1'] += $row['CostoTotal1'];
                            $total['totalComprar2'] += $row['CostoTotal2'];
                            $total['totalComprar3'] += $row['CostoTotal3'];

                            $form .= "
                                <tr materialRow='true' idMaterial='" . $row[$type == 'Alimentacion' ? 'IdAlimentacion' : 'IdMaterial'] . "' " . ($tracingFlag ? "idComparative='" . $tracingData['idComparativa']  . "' idTracing='" . $tracingData['IdSeguimiento'] . "' idprovider='" . $tracingData['idProveedor'] . "'" : "") . ">
                                    <td materialCant='true'>" . $row['Cantidad_aprobada'] . "</td>
                                    <td materialName='true'>" . $row[$type == 'Alimentacion' ? 'Tipo' : 'Material'] . "</td>
                                    <td materialEsp='true'>" . $row['Espe_Tecnica'] . "</td>



                                    <td>
                                        <input type='radio' inputindex='0' class='proveedor1' name='".$row[$type == 'Alimentacion' ? 'IdAlimentacion' : 'IdMaterial']."' ".($class['1'] ? "checked" : "") ." value='" . $row['Proveedor1'] . "' setprovider='true'>
                                    </td>
                                    <td>
                                        <input type='number' class='txtUnit' inputindex='0' name='txtUnit_" . ($i + 1) . "' value='".number_format($row['Costo1'], 2)."'>
                                    </td>
                                    <td>
                                        <input type='text' class='txtTotal' inputindex='0' name='txtTotal_" . ($i + 1) . "' disabled value='$".number_format($row['CostoTotal1'], 2)."'>
                                    </td>
                                    <td>
                                        <input type='radio' inputindex='1' class='proveedor2' name='".$row[$type == 'Alimentacion' ? 'IdAlimentacion' : 'IdMaterial']."' ".($class['2'] ? 'checked' : '') ." value='" . $row['Proveedor2'] . "' setprovider='true'>
                                    </td>
                                    <td>
                                        <input type='number' class='txtUnit' inputindex='1' name='txtUnit_" . ($i + 1) . "' value='".number_format($row['Costo2'], 2)."'>
                                    </td>
                                    <td>
                                        <input type='text' class='txtTotal' inputindex='1' name='txtTotal_" . ($i + 1) . "' disabled value='$".number_format($row['CostoTotal2'], 2)."'>
                                    </td>
                                    <td>
                                        <input type='radio' inputindex='2' class='proveedor3' name='".$row[$type == 'Alimentacion' ? 'IdAlimentacion' : 'IdMaterial']."' ".($class['3'] ? 'checked' : '' )." value='" . $row['Proveedor3'] . "' setprovider='true'>
                                    </td>
                                    <td>
                                        <input type='number' class='txtUnit' inputindex='2' name='txtUnit_" . ($i + 1) . "' value='".number_format($row['Costo3'], 2)."'>
                                    </td>
                                    <td>
                                        <input type='text' class='txtTotal' inputindex='2' name='txtTotal_" . ($i + 1) . "' disabled value='$".number_format($row['CostoTotal3'], 2)."'>
                                    </td>
                                </tr>
                            ";
                            $i++;
                        }
                        $form .= "
                                    </tbody>
                                    <tfoot id='totalData'>
                                        <td colspan='3'>Total</td>
                                        <td></td>
                                        <td dataindex='0' class='tdTotalUnit'>$".number_format($total['total1'], 2)."</td>
                                        <td dataindex='0' class='tdTotalTotal'>$".number_format($total['totalComprar1'], 2)."</td>

                                        <td></td>
                                        <td dataindex='1' class='tdTotalUnit'>$".number_format($total['total2'], 2)."</td>
                                        <td dataindex='1' class='tdTotalTotal'>$".number_format($total['totalComprar2'], 2)."</td>

                                        <td></td>
                                        <td dataindex='2' class='tdTotalUnit'>$".number_format($total['total3'], 2)."</td>
                                        <td dataindex='2' class='tdTotalTotal'>$".number_format($total['totalComprar3'], 2)."</td>
                                    </tfoot>
                                </table>
                                <input type='button' value='Enviar' id='btnModifyRequisition' class='button'>
                            </form>
                            </div>
                        ";
                    }

                    $j = 1;
                    for($z = 0; $z < count($providerSelect); $z++){
                        $providerData[$z] = [
                            "select" => "proveedor".$j++."",
                            "id" => $providerSelect[$z]['id']
                        ];
                    }
                    
                    return $form . "|" . json_encode($providerData);
                    
                }
            }catch(Exception $e){
                echo $e->getMessage();
            }
            return 0;
        }

        public function updateComparative($data, $idRequi, $type)
        {
            try {
                $this->db->beginTransaction();
                $table = $type === "Materiales" ? "Material" : "Alimentacion";
                $selectedFlag = true;
                $idMaterial = $cant = $idProv =  $idProv1 = $idProv2 = $idProv3 = $price = $price1 =  $price2 = $price3 = $total1 = $total2 = $total3 = $selected = $idComparativa = $idTracing = $idSubLinea = null;
                //1ro. Comparativa
                //(ID, Id_Material, Cantidad_aprobada, Proveedor1, Costo1, CostoTotal1, Proveedor2, Costo2, CostoTotal2, Proveedor, Costo3, CostoTotal3, seleccionada, IdSubLineaPresupuestaria)
                $queryComparamiento = $this->db->prepare("UPDATE Request_Comparativa_$table SET Id_Material=:idMaterial, Cantidad_aprobada=:cant, Proveedor1=:idProv1, Costo1=:price1, CostoTotal1=:total1, Proveedor2=:idProv2, Costo2=:price2, CostoTotal2=:total2, Proveedor3=:idProv3, Costo3=:price3, CostoTotal3=:total3, IdSubLineaPresupuestaria=:idSubLinea WHERE idComparativa = :idComparative;");
                $queryComparamiento->bindParam(":idMaterial", $idMaterial);
                $queryComparamiento->bindParam(":cant", $cant);
                $queryComparamiento->bindParam(":idProv1", $idProv1);
                $queryComparamiento->bindParam(":price1", $price1);
                $queryComparamiento->bindParam(":total1", $total1);
                $queryComparamiento->bindParam(":idProv2", $idProv2);
                $queryComparamiento->bindParam(":price2", $price2);
                $queryComparamiento->bindParam(":total2", $total2);
                $queryComparamiento->bindParam(":idProv3", $idProv3);
                $queryComparamiento->bindParam(":price3", $price3);
                $queryComparamiento->bindParam(":total3", $total3);
                $queryComparamiento->bindParam(":idSubLinea", $idSubLinea);
                $queryComparamiento->bindParam(":idComparative", $idComparativa);

                //2do. Seguimiento (Sí está seleccionado)
                //(ID, idSeguimiento, Id_Material, Costo, Cantidad_aprobada, idProveedor, IdSubLineaPresupuestaria)
                $querySeguimiento = $this->db->prepare("UPDATE Request_Seguimiento2_$table SET Id_Material=:idMaterial, Costo=:price, Cantidad_aprobada=:cant, idProveedor=:idProv, IdSubLineaPresupuestaria=:idSubLinea WHERE IdSeguimiento = :idTracing;");
                $querySeguimiento->bindParam(":idMaterial", $idMaterial);
                $querySeguimiento->bindParam(":price", $price);
                $querySeguimiento->bindParam(":cant", $cant);
                $querySeguimiento->bindParam(":idProv", $idProv);
                $querySeguimiento->bindParam(":idSubLinea", $idSubLinea);                
                $querySeguimiento->bindParam(":idTracing", $idTracing);           

                for ($i=0; $i < count($data); $i++) {
                    $selectedFlag = true;
                    $idComparativa = $data[$i]['idComparative'];
                    $idMaterial = $data[$i]['idMaterial'];
                    $cant = $data[$i]['cant'];
                    $idSubLinea = $data[$i]['idSubLinea'];
                    $idProv1 = $data[$i]['providers'][0]['idProvider'];
                    $price1 = $data[$i]['providers'][0]['price'];
                    $total1 = $data[$i]['providers'][0]['total'];
                    $idProv2 = $data[$i]['providers'][1]['idProvider'];
                    $price2 = $data[$i]['providers'][1]['price'];
                    $total2 = $data[$i]['providers'][1]['total'];
                    $idProv3 = $data[$i]['providers'][2]['idProvider'];
                    $price3 = $data[$i]['providers'][2]['price'];
                    $total3 = $data[$i]['providers'][2]['total'];
                    $selected = $data[$i]['providers'][2]['total'];

                    $queryComparamiento->execute();

                    for ($z=0; $z < count($data[$i]['providers']); $z++) {
                        if ($data[$i]['providers'][$z]['selected'] && $selectedFlag) {
                            $selectedFlag = false;
                            $idTracing = $data[$i]['idTracing'];
                            $price = $data[$i]['providers'][$z]['price'];
                            $idProv = $data[$i]['providers'][$z]['idProvider'];

                            $querySeguimiento->execute();
                        }
                    }
                }
                $this->db->commit();
                return 1;
            } catch (Exception $e) {
                $this->db->rollback();
                echo $e->getMessage();
                return 0;
            }
        }

        public function getQuotation($idRequi, $inicio,  $placeDate, $place, $validity, $warranty, $note, $nameResponsable, $type){ //PDF de la solicitud de cotizacion en las requisiciones de operaciones
            try{
                $table = $type === "Materiales" ? "Material" : "Alimentacion";//Tipo de la tabla a realizar la consulta
                $requiData = $this->getDataProyect($idRequi);
                $query = $this->db->prepare("SELECT * FROM Request_$table WHERE IdRequisicion = $idRequi");
                if($query->execute()){
                    $form = " 
                        <ul>
                            <li> ".utf8_encode($inicio)."</li>
                            <li> <b>1. Lugar y fecha:</b> ".utf8_encode($placeDate)."</li>
                            <li> <b>2. Lugar de entrega:</b> ".utf8_encode($place)."</li>
                            <li> <b>3. Vigencia de la cotización:</b> ".utf8_encode($validity)."</li>
                            <li> <b>4. Tiempo de garantía de los bienes (de ser necesario 30 días):</b> ".utf8_encode($warranty)."</li>
                        </ul>
                        <br>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Cantidad</th>
                                    <th>Unidad de Medida</th>
                                    <th>Descripción</th>
                                    <th>Especificación</th>
                                </tr>
                            </thead>
                            <tbody>
                    ";
                    $i = 0;
                    while($row = $query->fetch(PDO::FETCH_ASSOC)){
                        $form .= "
                            <tr>
                                <td>".++$i."</td>
                                <td>".$row['Cantidad_aprobada']."</td>
                                <td>Unidad</td>
                                <td>".$row['Material']."</td>
                                <td>".$row['Espe_Tecnica']."</td>
                            </tr>
                        ";
                    }
                    $form .= "
                            </tbody>
                        </table>
                        <br>
                        <div class='Quotation-Note'>
                            <p class='title'>Nota</p>
                            <p class='result'>".utf8_encode($note)."</p>
                        </div>
                        <br>
                        <br>
                        <div class='signature'>
                            <table class='responsable'>
                                <tr>
                                    <td><p><span style='font-size: 25px; margin-right: 5px;'>F.</span>".utf8_encode($nameResponsable)."</p></td>
                                </tr>
                                <tr >
                                    <td>NOMBRE Y FIRMA ENCARGADO DE COMPRAS</td>
                                </tr>
                            </table>
                        </div>
                    ";
                    return $form;
                }
            }catch(Exception $e){
                echo $e->getMessage();
            }
            return 0;
        }
    }
?>
