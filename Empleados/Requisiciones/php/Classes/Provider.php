<?php

    class Provider
    {
        private $db;

        public function __construct()
        {
            require_once('Connection.php');
            $this->db = new Connection();
            $this->db->connect();
            $this->db = $this->db->connection;

            $this->days = array(
				"Monday" => "Lunes",
				"Tuesday" => "Martes",
				"Wednesday" => "Miércoles",
				"Thursday" => "Jueves",
				"Friday" => "Viernes",
				"Saturday" => "Sábado",
				"Sunday" => "Domingo"
			);

			$this->months = array(
				"Jan" => "enero",
				"Feb" => "febrero",
				"Mar" => "marzo",
				"Apr" => "abril",
				"May" => "mayo",
				"Jun" => "junio",
				"Jul" => "julio",
				"Aug" => "agosto",
				"Sep" => "septiembre",
				"Oct" => "octubre",
				"Nov" => "noviembre",
				"Dec" => "diciembre"
			);
        }

        public function addProvider($data)
        {
            try{
                $this->db->beginTransaction();
                $query = $this->db->prepare("INSERT INTO request_proveedor VALUES(NULL, :name, :address, :city, :state, :phone, :cp, :type);");
                $query->bindParam(':name', $data['name']);
                $query->bindParam(':address', $data['address']);
                $query->bindParam(':city', $data['city']);
                $query->bindParam(':state', $data['state']);
                $query->bindParam(':phone', $data['phone']);
                $query->bindParam(':cp', $data['postalCode']);
                $query->bindParam(':type', $data['type']);

                if($query->execute()){$this->db->commit(); return 1;}
                return 0;
            }catch(Exception $e){
                echo $e->getMessage();
                return 0;
            }
        }

        public function getProviders($type)
        { /* Devuelve un JSON con todo los proveedores */
            try{
                $this->db->beginTransaction();
                $query = $this->db->prepare("SELECT * FROM request_proveedor WHERE tipo = :type AND idProveedor <> 1");
                if($query->execute(array(':type' => $type))){
                    $this->db->commit();

                    if($query->rowCount()){
                        $providers = array();
                        $i = 0;

                        while($row = $query->fetch(PDO::FETCH_ASSOC)){
                            $providers[$i++] = [
                                'IdProveedor' => $row['idProveedor'],
                                'nombre' => $row['nombre'],
                                'direccion' => $row['direccion'],
                                'ciudad' => $row['ciudad'],
                                'estado' => $row['estado'],
                                'telefono' => $row['telefono']
                            ];
                        }

                        return json_encode($providers);
                    }
                }
            }catch(Exception $e){
                echo $e->getMessage();
            }
            return 0;
        }

        public function purchaseOrder($idProvider, $idRequisition, $idHeadquarter, $code, $orderNum, $sendBy = "Walter Hernández", $type, $name, $card, $expire, $comment, $payments, $bill, $operationsFlag)
        {
            $longDate = "";
            $table = $type === "Materiales" ? "Material" : "Alimentacion";
            ini_set("date.timezone", 'America/El_Salvador');

            $longDate .= $this->days[date("l")] . ", " . date("j") . " de " . $this->months[date("M")] . " de " . date("Y");
            $content = "";
            $total = 0;
            
            $headquarterQuery = $this->db->prepare("SELECT * FROM sedes_fusalmo WHERE idSede = $idHeadquarter;");
            $headquarterQuery->execute();
            $hData = $headquarterQuery->fetch(PDO::FETCH_ASSOC);

            $query = $this->db->prepare("SELECT * FROM request_proveedor WHERE idProveedor = $idProvider;");
            $query->execute();
            $providerData = $query->fetch(PDO::FETCH_ASSOC);

            $dataQuery = "
            SELECT * FROM Request_Requisiciones rr
            INNER JOIN Request_$table rm ON rr.Idrequisicion = rm.IdRequisicion
            INNER JOIN Request_Comparativa_$table rc ON rc.Id_Material = rm." . ($type === "Materiales" ? "IdMaterial" : "IdAlimentacion") . "
            INNER JOIN Request_Seguimiento2_$table rs ON rc.idComparativa = rs.idComparativa
            WHERE rr.Idrequisicion = $idRequisition AND rs.idProveedor = " . $providerData['idProveedor'] . ";";

            $query = $this->db->prepare($dataQuery);
            $query->execute();

            $content .= "
            <div class='code'>$code</div>
                <div class='data'>
                    <div class='cont Fusalmo'>
                        <h4>Enviar a</h4>
                        <div class='cont_cont'>
                            <div class='title'>Nombre: </div>
                            <div class='content'>FUSALMO \"Fundación Salvador del Mundo\"</div>
                        </div>
                        <div class='cont_cont'>
                            <div class='title'>Dirección: </div>
                            <div class='content'>" . $hData['direccion'] . "</div>
                        </div>";
                            
            if($operationsFlag === "true"){
                $content .= "
                        <div class='cont_cont xtra'>
                            <span class='title2'>NIT: </span>
                            <span class='content2'>0614-170801-104-0</span> &nbsp;&nbsp;&nbsp;
                            <span class='title'>NRC: </span>
                            <span class='content'>136738-7</span> 
                        </div>
                ";
            }
            
            $content .= "
                        <div class='cont_cont'>
                            <div class='title'>Ciudad: </div>
                            <div class='content'>" . $hData['ciudad'] . "</div>
                        </div>
                        <div class='cont_cont xtra'>
                            <span class='title'>Teléfono: </span>
                            <span class='content'>" . $hData['telefono'] . "</span> &nbsp;&nbsp;&nbsp;
                            <span class='title2'>Código Postal: </span>
                            <span class='content2'>" . $hData['codigo_postal'] . "</span>
                        </div>
                    </div>
                    <div class='cont Provider'>
                        <h4>Proveedor</h4>
                        <div class='cont_cont'>
                            <div class='title'>Nombre: </div>
                            <div class='content'>" . $providerData['nombre'] . "</div>
                        </div>
                        <div class='cont_cont'>
                            <div class='title'>Dirección: </div>
                            <div class='content'>" . $providerData['direccion'] . "</div>
                        </div>
                        <div class='cont_cont xtra'>
                            <span class='title'>Ciudad: </span>
                            <span class='content'>" . $providerData['ciudad'] . "</span> &nbsp;&nbsp;&nbsp;
                            <span class='title2'>Estado: </span>
                            <span class='content2'>" . $providerData['estado'] . "</span>
                        </div>
                        <div class='cont_cont xtra'>
                            <span class='title'>Teléfono: </span>
                            <span class='content'>" . $providerData['telefono'] . "</span> &nbsp;&nbsp;&nbsp;
                            <span class='title2'>Código Postal: </span>
                            <span class='content2'>" . $providerData['codigo_postal'] . "</span>
                        </div>
                    </div>
                </div><br>
                <table>
                    <tr>
                        <th>Cantidad</th>
                        <th>Tipo de Unidad</th>
                        <th>Descripción</th>
                        <th>Precio Unitario</th>
                        <th>TOTAL</th>
                    </tr>";

            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $content .= "
                    <tr>
                        <td>" . $row['Cantidad'] . "</td>
                        <td>" . ($providerData['tipo'] == "M" ? "Unidad" : "Alimento") . "</td>
                        <td>" . $row['Material'] . "</td>
                        <td>$" . number_format($row['Costo'], 4) . "</td>
                        <td>$" . number_format($row['Costo'] * $row['Cantidad_aprobada'], 2) . "</td>
                    </tr>";
                $total += $row['Costo'] * $row['Cantidad_aprobada'];
            }

            $content .= "
                    <tr class='total-row'>
                        <th colspan='4'>TOTAL</th>
                        <th>$" . number_format($total, 2) . "</th>
                    </tr>
                </table>

                <div class='payment'>
                <div class='fieldTitle'>
                    <span>Detalle de Pago</span>
                </div>    
                    <ul>";
            if (count($payments) > 0) {
                foreach ($payments as $payment) {
                     $content .= "<li>$payment</li>";
                }
            }
            $content .= "</ul>
                    <div class='cont_cont'>
                        <div class='title'>Nombre: </div>
                        <div class='content'>$name</div>
                    </div>
                    <div class='cont_cont'>
                        <div class='title'>Núm. de tarjeta: </div>
                        <div class='content'>$card</div>
                    </div>
                    <div class='cont_cont'>
                        <div class='title'>Caduca: </div>
                        <div class='content'>$expire</div>
                    </div>
                </div>

                <div class='xtraData'>
                    <div class='xtra date'>
                        <div class='fieldTitle' style='margin-top: -20px;'>
                            <span>Fecha de Envío</span>
                        </div>
                        <span class='content'>$longDate</span>
                    </div>
                    <div class='xtra bill'>
                        <div class='fieldTitle' style='margin-top: -20px;'>
                            <span>Facturación</span>
                        </div>
                        <span class='content'>$bill</span>
                    </div>
                </div>

                <div class='xtraData xtrax2'>
                    <div class='xtra a'>
                        <div class='fieldTitle'>
                            <span>Aprobado</span>
                        </div>
                        <br><br><br>
                        <p style='text-align: center;'>Lic. José Wilfredo Iglesias Osegueda<br>
                        Gerente Administrativo Financiero</p>
                    </div>
                    <div class='xtra order'>
                        <div class='cont_cont'>
                            <div class='title'>Fecha: </div>
                            <div class='content'>" .date("d/m/Y") . "</div>
                        </div>
                        <div class='cont_cont'>
                            <div class='title'>Pedido núm: </div>
                            <div class='content'>$orderNum</div>
                        </div>
                        <div class='cont_cont'>
                            <div class='title'>Representante: </div>
                            <div class='content'>José Wilfredo Iglesias</div>
                        </div>
                        <div class='cont_cont'>
                            <div class='title'>Enviar por: </div>
                            <div class='content'>$sendBy</div>
                        </div>
                    </div>
                </div>

                <div class='xtraData xtra3'>
                    <div class='fieldTitle' style='margin-top: -11px;'>
                        <span style='font-size: 15px;'>Notas o Comentarios</span>
                        <p style='font-size: 11px; padding: 0px 15px;'>$comment</p>
                    </div>
                </div>
                
                ";

            return $content;
        }

        public function providersForOrder($idR, $type)
        {
            $data = [];
            $table = $type === "Materiales" ? "Material" : "Alimentacion";
            $query = $this->db->prepare("
			SELECT DISTINCT rs.idProveedor, rp.nombre FROM Request_Requisiciones rr
            INNER JOIN Request_$table rm ON rr.Idrequisicion = rm.IdRequisicion
            INNER JOIN Request_Comparativa_$table rc ON rc.Id_Material = rm." . ($type === "Materiales" ? "IdMaterial" : "IdAlimentacion") . " 
            INNER JOIN Request_Seguimiento2_$table rs ON rc.idComparativa =  rs.idComparativa
            INNER JOIN request_proveedor rp ON rp.idProveedor = rs.idProveedor
            WHERE rr.Idrequisicion = $idR;");

            if(!$query->execute()) return Array("res" => false);
            if($query->rowCount() == 0) return Array("res" => false);

            while($row = $query->fetch(PDO::FETCH_ASSOC)){
                array_push($data, $row);
            }

            return $data;
        }
    }
?>
