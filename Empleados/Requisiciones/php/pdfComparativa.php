<?php
	require '../../../net.php';
	include 'mpdf/mpdf.php';

if(isset($_POST['pdfComparativa'])){
	header("Content-Type: text/html; charset=utf-8");
	$nombre = $_POST['nombre'];
	$idRequi = $_POST['idreq'];
	$type = $_POST['tipo'];
	$total1=0;
	$total2=0;
	$total3=0;
	$total=0;
	$tabla = $type === "Materiales" ? "Material" : "Alimentacion";
	$idtipo = $type === 'Materiales' ? 'IdMaterial' : 'IdAlimentacion';
	$tipoPago = $_POST['payment2'];
	$usuario = $_POST['usuario'];

	//Convierte los caracteres raros
	define('CHARSET', 'ISO-8859-1');
	define('REPLACE_FLAGS', ENT_COMPAT | ENT_XHTML);
	function html($string) {
	    return htmlentities($string, REPLACE_FLAGS, CHARSET);
	}

	
	$query4 = ("SELECT * FROM Request_Seguimiento2_$tabla rsm 
			inner join Request_Comparativa_$tabla rcm
			on rsm.idComparativa = rcm.idComparativa
			inner join Request_$tabla rt
			on rt.$idtipo = rcm.Id_Material
			inner join Request_Requisiciones rr
			on rt.IdRequisicion = rr.IdRequisicion
			inner join Request_Proyecto rpr
			on rr.Idproyecto = rpr.IdProyecto
			where rcm.Nombre like '$nombre'
			order by rsm.idProveedor");

	$data4 = $bddr->prepare($query4);
	$data4->execute();
	$cuent= $data4->rowCount();
	$prove = $data4->fetchAll();

	ini_set("date.timezone", 'America/El_Salvador');
	$date = date("d/m/Y");
	$proyecto = $prove[0]['Proyecto'];
	
	$html ="
			<div class='header'>
				<div class='logo_cont'>
					<img class='logo' src='../img/fusalmo.png'>
					<h2>Fundación Salvador del Mundo</h2>
					<div ='tablaName'><h3>".html($_POST['nombre'])."</h3></div>
				</div>
			<di class='head2S'>
				<p><b>Nombre del Proyecto: </b>".html($proyecto)."
					<br><b>Fecha: </b>$date
					<br><b>Empresas que ofertaron: </b><ul>";
			$proveedor = $type === "Materiales" ? "M" : "A";

			$query3 = ("SELECT * from request_proveedor where tipo = '$proveedor'");

            $data3 = $bddr->prepare($query3);

            $r=0;
            for($x=1; $x <= 3; $x++){
            $data3->execute();
                while ($datas3= $data3->fetch()) {

                	if($cuent > 0){
                    	if($prove[$r]["Proveedor$x"] == $datas3['idProveedor']){
                    		$html.= "<li type='circle'>".html($datas3['nombre'])."</li>";
                    	}
                    }
                }
            }

			$html.="</ul></div>
		</div>
		
		<table class='table' id='tablaComparativa' style='font-size:1.5em;'>
		
	  	    <tr>
	  	    	<th rowspan='3'>Cantidad</th>
	            <th rowspan='3'>producto</th>
	            <th rowspan='3'>Especificaci&oacute;n</th>
	            <th colspan='6'>Proveedores</th>
            </tr>
            <tr>
		  	";
            $r=0;
            for($x=1; $x <= 3; $x++){
            $data3->execute();
                while ($datas3= $data3->fetch()) {

                	if($cuent > 0){
                    	if($prove[$r]["Proveedor$x"] == $datas3['idProveedor']){
                    		$html.= "<th colspan='2'>".html($datas3['nombre'])."</th>";
                    	}else{
                    		
                    	}	

                    }
                    else{
                    	
                    }
                }
            }
		    $html.="</tr>
		    <tr>";
		     for($o=1; $o <= 3; $o++){
  	$html.=  "
          	<th  class=''>Prec.Unit.</th>
             <th  class=''>Total($)</th>";
			}
		        
		   $html.=" </tr>
		   </tbody>";

		  $total1=0;
		  $total2=0;
		  $total3=0;
		  $total=0;
	 	for($x = 0 ; $x < $cuent; $x++){
		$html.="<tr>
				<td>".$prove[$x][4]."</td>
			    <td>".html($prove[$x][$type=='Materiales' ? 'Material': 'Tipo'])."</td>
			    <td>".html($prove[$x][$type=='Materiales' ? 'Espe_Tecnica': 'Tipo'])."</td>";
			    for($z = 1; $z <=3; $z++){

					if($prove[$x]["idProveedor"] == $prove[$x]["Proveedor$z"]){

				   	$html.="<td class='selected'>$ ".number_format((float)$prove[$x]["Costo$z"], 2, '.', '')."</td>
					    <td class='selected'>$ ".number_format((float)$prove[$x]["CostoTotal$z"], 2, '.', '')."</td>
					    ";

					    if($z==1){
					    	$total1 += $prove[$x]["CostoTotal$z"];
					    }
					    if($z==2){
					    	$total2 += $prove[$x]["CostoTotal$z"];
					    }
					    if($z==3){
					    	$total3 += $prove[$x]["CostoTotal$z"];
					    }

					    $total = $total1+$total2+$total3;	
					}
					else{
						$html.="<td >$ ".number_format((float)$prove[$x]["Costo$z"], 2, '.', '')."</td>
					    <td>$ ".number_format((float)$prove[$x]["CostoTotal$z"], 2, '.', '')."</td>
					    ";
					}
				}

		$html.="  </tr>";
		}
		$html.="</tbody>
				<tfooter>
				<tr class='total'>
					<th colspan='3' class='total'>TOTAL($ USD)<ht>
					<td></td>
					<td class='total'>$ ".number_format((float)$total1, 2, '.', '')."</td>
					<td></td>
					<td class='total'>$ ".number_format((float)$total2, 2, '.', '')."</td>
					<td></td>
					<td class='total'>$ ".number_format((float)$total3, 2, '.', '')."</td>
				</tr>
				<tr class='selected'>
					<th colspan='3' class=''>Total a Comprar ($ USD)</th>
					<th class='selecteh'></th>
					<th class='selecteh'></th>
					<th class='selecteh'></th>
					<th class='selecteh'></th>
					<th class='selecteh'></th>
					<th class=''>$ ".number_format((float)$total, 2, '.', '')."</th>
				</tr>
				</tfooter>
		</table>
		<p><b>Terminos y condiciones:<b>
		<ul>";
			foreach($tipoPago as $selected){
			 $html.="
				<li type='circle'>".htmlentities($selected)."</li>
				";	
		}
		$html.="</ul><p><table class='staff'>
                        <tbody>
                            <tr>
                                <td class='firm' colspan='3'></td>
                                <td class='otro'></td>
                                <td class='firm' colspan='3'></td>
                                <td class='otro'></td>
                                <td class='firm' colspan='3'></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan='3'>".html($usuario)."</td>
                                <td class='otro'></td>
                                <td colspan='3'>Lic. José Wilfredo Iglesias</td>
                                <td class='otro'></td>
                                <td colspan='3'>Lic. Oscar Nelson Cruz</td> 
                            </tr>
                            <tr>
                                <td colspan='3'>Elaborado por</td>
                                <td class='otro'></td>
                                <td colspan='3'>VoBo</td>
                                <td class='otro'></td>
                                <td colspan='3'>Autorizado</td> 
                            </tr>
                        <tfoot>
                    </table>";
		
		$pdf = new mPDF('UTF-8', 'A4-L', 0, '', 10, 10, 10, 0, 0, 0, 'L');
		$pdf->allow_charset_conversion=true;
		$pdf->charset_in='UTF-8';
		$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
		$stylesheet= file_get_contents('Classes/purchaseOrder.css');
		$stylesheet2 = file_get_contents('pdfComparativa.css');
		$pdf->WriteHTML($stylesheet2,1);
		$pdf->WriteHTML($stylesheet,1);
		$pdf->WriteHTML($html,2);
		$pdf->Output();
		exit;
}

?>