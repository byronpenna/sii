<?php

/**
 * @author Manuel
 * @copyright 2013
 */

    require '../../net.php';	
    
    if(isset($_POST['Enviar']))
    {
        if($_POST['Enviar'] == "Crear Resumen")
        {
            $tematicas = $_POST["tematica"];
            $count = count($tematicas);
            $tematica = ".";
            
            for ($i = 0; $i < $count; $i++)             
                $tematica .= $tematicas[$i]."."; 
            
            if($_POST['nuevatematica'] != "")
            {
                $tematica .= $_POST['nuevatematica'].".";
                
                $InsertTematic = $bdd->prepare("Insert into Project_Tematica value(null, '".$_POST['nuevatematica']."' )");
                $InsertTematic->execute();    
            }
            
            $lineas = $_POST["linea"];
            $count = count($lineas);
            $linea = ".";
            
            for ($i = 0; $i < $count; $i++)             
                $linea .= $lineas[$i].".";               
            
            $Insert = $bdd->prepare("Insert into Project_Summary values(null, :iduser, :np, :financiador, :monto, :contra, :coordinador, :fdesde, :fhasta, :objetivo, :tematica, :descripcion, :date, :line)");
            $Insert->bindParam(':iduser' , $_SESSION["IdUsuario"]);
            $Insert->bindParam(':np' , $_POST["nombre"]);
            $Insert->bindParam(':financiador' , $_POST["financiador"]);
            $Insert->bindParam(':monto' , $_POST["monto"]);
            $Insert->bindParam(':contra' , $_POST["contrapartida"]);
            $Insert->bindParam(':coordinador' , $_POST["coordinador"]);
            $Insert->bindParam(':fdesde' , $_POST["FechaI"]);
            $Insert->bindParam(':fhasta' , $_POST["FechaH"]);
            $Insert->bindParam(':objetivo' , $_POST["objetivo"]);
            $Insert->bindParam(':tematica' , $tematica);
            $Insert->bindParam(':descripcion' , $_POST["descripcion"]);
            $Insert->bindParam(':date' , date("Y-m-d H:i:s"));
            $Insert->bindParam(':line' , $linea);
            $Insert->execute();
            
            Redireccion("../../Empleado.php?l=Summary"); 
        }
        if($_POST['Enviar'] == "Actualizar")
        {
            $tematicas = $_POST["tematica"];
            $count = count($tematicas);
            $tematica = ".";
            
            for ($i = 0; $i < $count; $i++)             
                $tematica .= $tematicas[$i].".";   
            
            if($_POST['nuevatematica'] != "")
            {
                $tematica .= $_POST['nuevatematica'].".";
                
                $InsertTematic = $bdd->prepare("Insert into Project_Tematica value(null, '".$_POST['nuevatematica']."' )");
                $InsertTematic->execute();    
            }
                                         
            $lineas = $_POST["linea"];
            $count = count($lineas);
            $linea = ".";
            
            for ($i = 0; $i < $count; $i++)             
                $linea .= $lineas[$i].".";             
            
            
            $Insert = $bdd->prepare("update Project_Summary set 
                                     NombreProyecto = :np,
                                     Financiador = :financiador,
                                     Monton = :monto,
                                     Contrapartida = :contra,
                                     Coordinador = :coordinador,
                                     FechaDesde = :fdesde,
                                     FechaHasta = :fhasta,
                                     Objetivo = :objetivo,
                                     Tematica = :tematica,
                                     ResumenProyecto = :descripcion,
                                     Actualizado = :date,
                                     LineaEstrategica = :line
                                     where IdSummary = :irp");
                                     
            $Insert->bindParam(':np' , $_POST["nombre"]);
            $Insert->bindParam(':financiador' , $_POST["financiador"]);
            $Insert->bindParam(':monto' , $_POST["monto"]);
            $Insert->bindParam(':contra' , $_POST["contrapartida"]);
            $Insert->bindParam(':coordinador' , $_POST["coordinador"]);
            $Insert->bindParam(':fdesde' , $_POST["FechaI"]);
            $Insert->bindParam(':fhasta' , $_POST["FechaH"]);
            $Insert->bindParam(':objetivo' , $_POST["objetivo"]);
            $Insert->bindParam(':tematica' , $tematica);
            $Insert->bindParam(':descripcion' , $_POST["descripcion"]);
            $Insert->bindParam(':irp' , $_SESSION["irp"]);
            $Insert->bindParam(':date' , date("Y-m-d H:i:s"));
            $Insert->bindParam(':line' , $linea);
            $Insert->execute();
            
            Redireccion("../../Empleado.php?l=Summary"); 
        }        
        else if($_POST['Enviar'] == "Guardar Participantes")
        {
            $Insert = $bdd->prepare("Insert into Project_Participants values(null, :participante, :sexo, :edadi, :cantidad, :idS)");            
            $Insert->bindParam(':participante' , $_POST["participante"]);
            $Insert->bindParam(':sexo' , $_POST["sexo"]);
            $Insert->bindParam(':edadi' , $_POST["Edades"]);
            $Insert->bindParam(':cantidad' , $_POST["cantidad"]);
            $Insert->bindParam(':idS' , $_SESSION["irp"]);
            $Insert->execute();        
            
            $Update = $bdd->prepare("Update Project_Summary set 
                                     Actualizado = :date 
                                     where IdSummary = :irp");

            $Update->bindParam(':date' , date("Y-m-d H:i:s"));
            $Update->bindParam(':irp' , $_SESSION["irp"]);
            $Update->execute();            
            
            Redireccion("../../Empleado.php?l=Summary");             
        }
        else if($_POST['Enviar'] == "Guardar Lugar")
        {
            if($_POST["Departamentos"] == "1") $Departamento = "Ahuachap�n";
            if($_POST["Departamentos"] == "2") $Departamento = "Caba�as";
            if($_POST["Departamentos"] == "3") $Departamento = "Chalatenango";
            if($_POST["Departamentos"] == "4") $Departamento = "Cuscatl�n";
            if($_POST["Departamentos"] == "5") $Departamento = "La Libertad";
            if($_POST["Departamentos"] == "6") $Departamento = "La Paz";
            if($_POST["Departamentos"] == "7") $Departamento = "La Uni�n";
            if($_POST["Departamentos"] == "8") $Departamento = "Moraz�n";
            if($_POST["Departamentos"] == "9") $Departamento = "San Miguel";
            if($_POST["Departamentos"] == "10") $Departamento = "San Salvador";
            if($_POST["Departamentos"] == "11") $Departamento = "San Vicente";
            if($_POST["Departamentos"] == "12") $Departamento = "Santa Ana";
            if($_POST["Departamentos"] == "13") $Departamento = "Sonsonate";
            if($_POST["Departamentos"] == "14") $Departamento = "Usulut�n";
            
            if(strlen( $_POST["Departamentos"]) > 2)
                $Departamento = $_POST["Departamentos"];
            
            if($_POST["LugarNuevo"] != "")
                $Lugar = $_POST["LugarNuevo"];
                
            else
                $Lugar = $_POST["Lugar"];
            
            $Insert = $bdd->prepare("Insert into Project_Comunity values(null, :pais, :depar, :municipio, :lugar, :idS)");            
            $Insert->bindParam(':pais' , $_POST["Pais"]);
            $Insert->bindParam(':depar' , $Departamento);
            $Insert->bindParam(':municipio' , $_POST["Municipio"]);
            $Insert->bindParam(':lugar' , $Lugar);
            $Insert->bindParam(':idS' , $_SESSION["irp"]);
            $Insert->execute();
            
            
            $Update = $bdd->prepare("Update Project_Summary set 
                                     Actualizado = :date 
                                     where IdSummary = :irp");

            $Update->bindParam(':date' , date("Y-m-d H:i:s"));
            $Update->bindParam(':irp' , $_SESSION["irp"]);
            $Update->execute();
            
            Redireccion("../../Empleado.php?l=Summary");             
        } 
        else if($_POST['Enviar'] == "Eliminar")
        {
            $Delete = $bdd->prepare("Delete from Project_Summary where IdSummary = :ip");            
            $Delete->bindParam(':ip' , $_SESSION["irp"]);
            $Delete->execute();
            
            $Update = $bdd->prepare("Update Project_Summary set 
                                     Actualizado = :date 
                                     where IdSummary = :irp");

            $Update->bindParam(':date' , date("Y-m-d H:i:s"));
            $Update->bindParam(':irp' , $_SESSION["irp"]);
            $Update->execute();    
            
            Redireccion("../../Empleado.php?l=Summary");             
        }          
        
              
        else if($_POST['Enviar'] == "x")
        {
            $Update = $bdd->prepare("Update Project_Summary set 
                                     Actualizado = :date 
                                     where IdSummary = :irp");

            $Update->bindParam(':date' , date("Y-m-d H:i:s"));
            $Update->bindParam(':irp' , $_SESSION["irp"]);
            $Update->execute();
                        
            if(isset($_POST['ip']))
            {
                $Delete = $bdd->prepare("Delete from Project_Participants where IdParticipantes = :ip");            
                $Delete->bindParam(':ip' , $_POST["ip"]);
                $Delete->execute();
                
                Redireccion("../../Empleado.php?l=Summary");      
            }
            else if(isset($_POST['ic']))
            {
                $Delete = $bdd->prepare("Delete from Project_Comunity where IdComunidad = :ic");            
                $Delete->bindParam(':ic' , $_POST["ic"]);
                $Delete->execute();
                
                Redireccion("../../Empleado.php?l=Summary");      
            }             
            else
                Redireccion("../../Empleado.php?l=Summary");         
        }        
    }
?>