<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">    
<table style="width: 100%;">
    <tr>
        <td><h2 style="color: darkblue;">Definici�n de Participantes</h2></td>
        <td style="text-align: right;">
        <a href="?l=Summary"><input type="button" class="boton" value="<- Atras" /></a>
        </td>
    </tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td>
            <form action="Empleados/ProjectSummary/ResumenABM.php" method="post">
            <table style="margin-left: 15%; width: 75%;">
                <tr>
                    <td class='tdleft' style='width: 50%; padding: 10px;'>Tipo de participantes:</td>
                    <td style='width: 40%; padding: 10px;'>
                        <select style='width: 120px' name="participante">
                            <option value="Ni�os">Ni�os</option>                                                
                            <option value="Adolecentes">Adolecentes</option>
                            <option value="J�venes">J�venes</option>
                            <option value="Adulto">Adulto</option>
                            <option value="Madres Lideresas">Madres Lideresas</option>
                            <option value="Lideres Comunales">Lideres Comunales</option>
                            <option value="Profesores">Profesores</option>
                            <option value="Directores">Directores</option>
                            <option value="Estudiantes">Estudiantes</option>
                            <option value="Universitarios">Universitarios</option>
                            <option value="T�cnicos">T�cnicos</option>
                            <option value="Otros">Otros</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class='tdleft' style='width: 50%; padding: 10px;'>Sexo:</td>
                    <td  style='width: 40%; padding: 10px;'>
                        <select name="sexo">
                            <option value="Masculino">Masculino</option>
                            <option value="Femenino">Femenino</option>
                        </select>
                    </td>
                </tr>                  
                <tr>
                    <td class='tdleft'  style='width: 50%; padding: 10px;'>Rango de Edades:</td>
                    <td  style='width: 40%; padding: 10px;'>
                        <select style='width: 120px' name="Edades">
                            <option value="6 - 10 a�os">6 - 10 a�os</option>                                                
                            <option value="11 - 17 a�os">11 - 17 a�os</option>
                            <option value="18 - 25 a�os">18 - 25 a�os</option>
                            <option value="21 - 25 a�os">21 - 25 a�os</option>
                            <option value="26 - 35 a�os">26 - 35 a�os</option>
                            <option value="36 o mas a�os">36 o mas a�os</option>
                        </select>                    
                    </td>
                </tr>
                <tr>
                    <td class='tdleft'  style='width: 50%; padding: 10px;'>Cantidad de participantes:</td>
                    <td  style='width: 40%; padding: 10px;'><input type="number" name="cantidad" value="" style='width: 120px'  min="0" /></td>
                </tr>              
                <tr><td colspan="2" style="text-align: center; padding-top: 10px; padding-bottom: 10px;"><input type="submit" name="Enviar" value="Guardar Participantes" class="boton" style="width: 150px;" /></td></tr>                                
            </table>
            </form>
        </td>
    </tr>
</table>
</div>
