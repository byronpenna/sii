<?php
    if(isset($_POST['irp']))
	   $_SESSION['irp'] = $_POST['irp'];
    
?>
<style>
.mytable td
{
    vertical-align: top;
} 
</style>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">    
<table style="width: 100%;">
    <tr>
        <td><h2 style="color: blue;">Resumen de Proyecto</h2></td>
        <td style="text-align: right;">
        <a href="?l=ProjectSummary"><input type="button" class="boton" value="<- Atras" /></a>
        </td>
    </tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="2">
            <table style="width: 100%;" class="mytable" style="">
            <?php
                   $Proyectos = $bdd->prepare("Select * from Project_Summary where IdSummary = ". $_SESSION["irp"]);
                   $Proyectos->execute();
                   
                   if($Proyectos->rowCount() > 0)
                   {
                        $DataP = $Proyectos->fetch();
                        $UltimaUpdate = $DataP[12];
                        echo "<tr><th class='tdleft'style='width: 30%'>Nombre del proyecto:</th><td>$DataP[2]<br /></td></tr>
                              <tr><th class='tdleft'>Entidad financiadora:</th><td>$DataP[3]<br /></td></tr>
                              <tr><th class='tdleft'>Monto financiado:</th><td>$". number_format($DataP[4],2)."<br /></td></tr>
                              <tr><th class='tdleft'>Monto contrapartioda:</th><td>$". number_format($DataP[5],2)."<br /></td></tr>
                              <tr><th colspan='2'><br /></th></tr>
                              <tr><th class='tdleft'>Coordinador del proyecto:</th><td>$DataP[6]<br /></td></tr>
                              <tr><th class='tdleft' style='vertical-align: top;'>L�neas Estrategicas:</th><td>";
                        
                        $ArrayLinea = explode ("." ,$DataP[13]);
                        
                        $i = 0;
                        foreach($ArrayLinea as $value)
                        {
                            if($i > 0)
                                echo "$value<br />";
                            
                            $i++;
                        }
                        
                        echo "    </td>
                              </tr>
                              <tr><th class='tdleft'>Fecha de inicio:</th><td>$DataP[7]<br /></td></tr>
                              <tr><th class='tdleft'>Fecha de finalizaci�n:</th><td>$DataP[8]<br /></td></tr>
                              <tr><th class='tdleft' style='vertical-align: top;'>Objetivo general:</th><td>$DataP[9]<br /></td></tr>
                              <tr><th class='tdleft' style='vertical-align: top;'>Descripci�n general:</th><td>$DataP[11]<br /></td></tr>
                              <tr><th class='tdleft' style='vertical-align: top;'>Tem�tica del proyecto:</th><td>";
                        
                        $ArrayTematica = explode ("." ,$DataP[10]);
                        
                        $i = 0;
                        foreach($ArrayTematica as $value)
                        {
                            if($i > 0)
                                echo "$value<br />";
                            
                            $i++;
                        }
                        
                        echo "    </td>
                              </tr>
                              
                              <tr><th colspan='2'>
                                   <table style='width: 80%; margin-left:10%'>
                                        <tr>
                                            <td style='text-align: center;'>
                                            <form action='?l=ProjectSummaryForm' method='post'>
                                                <input type='submit' value='Editar' name='Enviar' class='boton'  />
                                            </form>
                                            </td>
                                            <td style='text-align: center;'>
                                            <form action='Empleados/ProjectSummary/ResumenABM.php' method='post'>                                             
                                                <input type='submit' value='Eliminar' name='Enviar' class='botonE' onclick='return confirm (\"Realmente deseas borrar este resumen?\")' />
                                            </form>
                                            </td>
                                        </tr>
                                   </table>
                                   </th>
                              </tr>                              ";
                   }
                   else
                        Redireccion("?l=ProjectSummary"); 
            ?>
            </table>
        </td>
    </tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="2">
            <table style="width: 100%;">
                <tr>
                    <td><h2 style="color: blue;">Sujetos de Derecho</h2></td>
                    <td style="text-align: right;"><a href='?l=Participants' style='text-decoration: none'><input type="button" class="boton" value="Agregar" /></a></td>
                </tr>
            </table>
        <?php
           $Participantes = $bdd->prepare("Select * from Project_Participants where IdSummary = ".$_SESSION["irp"]);
           $Participantes->execute();
           
           if($Participantes->rowCount() > 0)
           {
               $i = 0;
               echo "<table style='width: 100%; text-align: center;'>
                     <tr>
                         <th>N�</th>
                         <th>Participantes</th>
                         <th>Rango de Edad</th>
                         <th>Cantidad</th>
                         <th>Sexo</th>
                         <th></th>
                     </tr>"; 
               while($DataPa = $Participantes->fetch())
               {    
                    $i++;
                    echo "<tr>
                              <td>$i</td>
                              <td>$DataPa[1]</td>
                              <td>$DataPa[3]</td>
                              <td>$DataPa[4]</td>
                              <td>$DataPa[2]</td>
                              <td>
                                <form action='Empleados/ProjectSummary/ResumenABM.php' method='post'>
                                    <input type='hidden' value='$DataPa[0]' name='ip'  />
                                    <input type='submit' value='x' name='Enviar' class='botonE' style='width: 35px' onclick='return confirm(\"Deseas borrar este dato\")'  />
                                </form>
                              </td>    
                          </tr>";
               }
               echo "</table>";
           }
           else
           {
                echo "<center>
                        <h2 style='color: green'>Agrega tus participantes en el proyecto </h2>
                      </center>";
           }
        ?>
        </td>
    </tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="2">
            <table style="width: 100%;">
                <tr>
                    <td><h2 style="color: blue;">Comunidades de Impacto</h2></td>
                    <td style="text-align: right;"><a href='?l=Comunity' style='text-decoration: none'><input type="button" class="boton" value="Agregar" /></a></td>
                </tr>
            </table>
        <?php
           $Participantes = $bdd->prepare("Select * from Project_Comunity where IdSummary = ".$_SESSION["irp"]);
           $Participantes->execute();
           
           if($Participantes->rowCount() > 0)
           {
               $i = 0;
               echo "<table style='width: 100%; text-align: center;'>
                     <tr>
                         <th>N�</th>
                         <th>Pais</th>
                         <th>Departamento</th>
                         <th>Municipio</th>
                         <th>Lugar</th>
                         <th></th>
                     </tr>"; 
               while($DataC = $Participantes->fetch())
               {    
                    $i++;
                    echo "<tr>
                              <td>$i</td>
                              <td>$DataC[1]</td>
                              <td>$DataC[2]</td>
                              <td>$DataC[3]</td>
                              <td>$DataC[4]</td>
                              <td>
                                <form action='Empleados/ProjectSummary/ResumenABM.php' method='post'>
                                    <input type='hidden' value='$DataC[0]' name='ic'  />
                                    <input type='submit' value='x' name='Enviar' class='botonE' style='width: 35px' onclick='return confirm(\"Deseas borrar este dato\")'  />
                                </form>
                              </td>    
                          </tr>";
               }
               echo "</table>";
           }
           else
           {
                echo "<center>
                        <h2 style='color: green'>Agrega tus comunidades en el proyecto </h2>                
                      </center>";
           }
        ?>
        </td>
    </tr>    
    <tr><td colspan="2"><hr color='skyblue' />Ultima Actualizaci�n: <?php echo $UltimaUpdate?></td></tr>
</table>
</div>