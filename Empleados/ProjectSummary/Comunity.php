<script>

function CargarListaMunicipios()
{
    var departamento = document.getElementById('Departamentos').value;    
    document.getElementById('Municipio').options.length = 0;
    
    var aux = true;
    
    $.get("USAID/SelectMunicipios.php", { departamento:departamento, aux:aux },
  		function(resultado)
  		{           
            document.getElementById('Municipio').options.length = 0;
                
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#Municipio').append(resultado);	            			
  		}
   	);        
}

function CargarListaComunidades()
{
    var municipios = document.getElementById('Municipio').value;     
    $.get("Empleados/ProjectSummary/OptionsComunidades.php", { municipios : municipios },
  		function(resultado)
  		{
  		    document.getElementById('comunidades').innerHTML = "";
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#comunidades').append(resultado);	            			
  		}
   	);        
}

function CargarListaDepartamentos()
{
    var pais = document.getElementById('Pais').value;    
    
    if(pais == "El Salvador")
    {
        var aux = "<select name='Departamentos' id='Departamentos' style='width: 150px; margin-left: 5px;' onchange='CargarListaMunicipios()'> ";
        aux += "<option value='...'>...</option>";
        aux += "<option value='1'>Ahuachap�n</option>";
        aux += "<option value='2'>Caba�as</option>";
        aux += "<option value='3'>Chalatenango</option>";
        aux += "<option value='4'>Cuscatl�n</option>";
        aux += "<option value='5'>La Libertad</option>";
        aux += "<option value='6'>La Paz</option>";
        aux += "<option value='7'>La Uni�n</option>";
        aux += "<option value='8'>Moraz�n</option>";
        aux += "<option value='9'>San Miguel</option>";
        aux += "<option value='10'>San Salvador</option>";
        aux += "<option value='11'>San Vicente</option>";
        aux += "<option value='12'>Santa Ana</option>";
        aux += "<option value='13'>Sonsonate</option>";
        aux += "<option value='14'>Usulut�n</option>";
        aux += "</select>";
                                                                                                                                                                        
        document.getElementById('departamentos').innerHTML = "";
        document.getElementById('departamentos').innerHTML = aux;
                
        var aux = "<select name='Municipio' id='Municipio' style='width: 150px; margin-left: 5px;' onchange='CargarListaComunidades()'>";
        aux += "<option value='...' selected='true'>...</option>";
        aux += "</select>";        
        
        document.getElementById('municipios').innerHTML = "";
        document.getElementById('municipios').innerHTML = aux;                
                
        document.getElementById('comunidades').innerHTML = "";
        document.getElementById('comunidades').innerHTML = "<em style='color: green;'>Esperando...</em>";
                 
    }
    else
    {
        document.getElementById('departamentos').innerHTML = "";
        document.getElementById('departamentos').innerHTML = "<input type='text' name='Departamentos' value='' style='width: 150px; margin-left: 5px;' />";
        
        document.getElementById('municipios').innerHTML = "";
        document.getElementById('municipios').innerHTML = "<input type='text' name='Municipio' value='' style='width: 150px; margin-left: 5px;' />";
        
        document.getElementById('comunidades').innerHTML = "";
        document.getElementById('comunidades').innerHTML = "<input type='text' name='Lugar' value='' style='width: 150px; margin-left: 5px;' />";                
    }      
}

</script>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">    
<table style="width: 100%;">
    <tr>
        <td><h2 style="color: darkblue;">Definici�n de Comunidades de Impacto</h2></td>
        <td style="text-align: right;">
        <a href="?l=Summary"><input type="button" class="boton" value="<- Atras" /></a>
        </td>
    </tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="2">
            <form action="Empleados/ProjectSummary/ResumenABM.php" method="post">
            <table style="width: 100%;">
                <tr>
                    <td class='tdleft' style='width: 40%; padding: 10px;'>Pais:</td>
                    <td style='width: 70%;'>                        
                        <select style="width: 150px; margin-left: 5px;" name="Pais" id="Pais" onchange="CargarListaDepartamentos()">
                            <option value="...">...</option>
                            <option value="El Salvador">El Salvador</option>
                            <option value="Guatemala">Guatemala</option>                            
                            <option value="Honduras">Honduras</option>
                            <option value="Nicaragua">Nicaragua</option>
                            <option value="Costa Rica">Costa Rica</option>
                            <option value="Estados Unidos">Estados Unidos</option>
                            <option value="Otro">Otro</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class='tdleft' style='width: 40%; padding: 10px;'>Departamento:</td>
                    <td>
                        <div id="departamentos"><em style="color: green;">Esperando...</em></div>
                    </td>
                </tr>
                <tr>
                    <td class='tdleft' style='width: 40%; padding: 10px;'>Municipio:</td>
                    <td>
                        <div id="municipios"><em style="color: green;">Esperando...</em></div>
                    </td>
                </tr>
                <tr>
                    <td class='tdleft' style='width: 40%; padding: 10px; vertical-align: top;'>Comunidad/Colonia:</td>
                    <td>
                        <div id="comunidades"><em style="color: green;">Esperando...</em></div>
                    </td>
                </tr>
                <tr><td colspan="2" style="text-align: center; padding-top: 10px; padding-bottom: 10px;"><input type="submit" name="Enviar" value="Guardar Lugar" class="boton" style="width: 150px;" /></td></tr>                                
            </table>
            </form>
        </td>
    </tr>
</table>
</div>