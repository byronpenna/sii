        <?php

/**
 * @author Manuel Calderón
 * @copyright 2014
 */

?>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px; "> 

<h2>Módulo de Recursos Humanos</h2>
<hr color='#0099FF'/>
    <?php
	       $DatosInstitucionales = $bddr->prepare("Select * from DatosInstitucionales where IdEmpleado = $IdEmp");  
           $DatosInstitucionales->execute();
           
           if($DatosInstitucionales->rowCount() > 0)
           {
                $Datos = $DatosInstitucionales->fetch();
                $correo = $Datos[2];
                $ext = $Datos[3] ;
                $color = "blue";
                $rowspan = "";
           }
           else
           {
                $color = "red";
                $rowspan = "rowspan='2'";
                $correo = "<a href='Empleado.php?l=Info' style='color:$color; text-decoration: none;'>Ingresa tus datos dando clic <ins>aqui</ins></a>";
                $ext    = "";
           }
    ?>    
<table style="width: 80%;">
<tr><td><h2>Datos Institucionales</h2></td></tr>
<tr><td>Mi Correo Institucional: </td><td style="padding-left: 20px; color: <?php echo$color?>" <?php echo $rowspan?> ><?php echo $correo;?></td></tr>
<tr><td>Mi Extensión de Contacto:</td><td style="padding-left: 20px; color: <?php echo$color?>"><?php echo $ext;?></td></tr>
</table>
<hr color='#0099FF'/>
<?php
	
    $Query = "SELECT e.* FROM Empleado as e
              inner join CargosAsignacion as ca on e.IdEmpleado = ca.IdEmpleado 
              inner join Cargos as c on ca.IdCargo = c.IdCargos             
              WHERE MONTH(FechaNacimiento) = " . date("m") . " AND FechaFin =  '0000-00-00'
              ORDER BY DAY( FechaNacimiento ) ASC ";
    $Cumpleañeros = $bddr->prepare($Query);
    $Cumpleañeros->execute();
    
    if(date("m") == "01")$mes = "Enero";
    if(date("m") == "02")$mes = "Febrero";
    if(date("m") == "03")$mes = "Marzo";
    if(date("m") == "04")$mes = "Abril";
    if(date("m") == "05")$mes = "Mayo";
    if(date("m") == "06")$mes = "Junio";
    if(date("m") == "07")$mes = "Julio";
    if(date("m") == "08")$mes = "Agosto";
    if(date("m") == "09")$mes = "Septiembre";
    if(date("m") == "10")$mes = "Octubre";
    if(date("m") == "11")$mes = "Noviembre";
    if(date("m") == "12")$mes = "Diciembre";
    
    
    echo "<br />Cumpleañeros del Mes de $mes:<br /><br />";
    if($Cumpleañeros->rowCount() > 0)
    {   
        echo "<table style='width: 80%; margin-left: 10%; margin-bottom: 40px;'>
              <tr><th style='width: 45%'>Empleado</th><th style='width: 20%'>Dia de $mes</th><th style='width: 35%'>Area</th><tr>";
        while($Empleados = $Cumpleañeros->fetch())        
        {
            if(substr($Empleados[11],8,2) == date("d"))
                $color = "Green";
            
            else
                $color = "blue";

            echo "<tr><td style='color:$color'>$Empleados[1] $Empleados[2] $Empleados[4]</td>
                      <th style='color:$color'>" . substr($Empleados[11],8,2). "</th>
                      <th style='color:$color'>$Empleados[30]</th></tr>";
        }
        echo "</table>";
    }
?>
</div>