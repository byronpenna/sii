<?php session_start();?>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<?php

/**
 * @author Manuel Calder�n
 * @copyright 2015
 */
 
date_default_timezone_set('Etc/GMT+6');

if(isset($_POST['Enviar']))
{
    
    if(utf8_decode($_POST['Enviar'])== "�Iniciar Donaci�n!")
    {
        if(isset($_POST['nombre'], $_POST['apellido'],$_POST['correo'],$_POST['pais']))
        {
           /**  Establecer conexi�n a la Base de Datos  **/
           $bdd = new PDO('mysql:host=localhost;dbname=poli09_payment', 'poli09_SiipUser', 'fusalmo2013');
           
           /**  Detalles de Valores  **/
           if($_POST['monto'] == "Otro")
           {
                $Monto = number_format($_POST['montootro'],2);
                $AuxMonto = $_POST['montootro'];
           }
           else
           {
                $Monto = number_format( $_POST['monto'],2);
                $AuxMonto = $_POST['monto'];
           }
                          
           
           $Data = $bdd->prepare("Select * from InformacionCliente ");
           $Data->execute();                                 
           $prefijo = substr(md5(uniqid(rand())),0,6);
           $Id= $prefijo . "-" . ( $Data->rowCount() + 1);
           
           $estado = "En Proceso de Pago";
           $correo = utf8_decode($_POST['correo']);
                
           /**  Insersi�n a la Base de Datos  **/
           $Data = $bdd->prepare("Insert into InformacionCliente values(null, :n1, :n2, :n3, :n4, :n5, :n6, :n7, :n8, :n9, :n10, :n11) ");
           $Data->bindParam(':n1', $Id);
           $Data->bindParam(':n2', utf8_decode($_POST['nombre']));
           $Data->bindParam(':n3', utf8_decode($_POST['apellido']));
           $Data->bindParam(':n4', $correo);
           $Data->bindParam(':n5', utf8_decode($_POST['pais']));
           $Data->bindParam(':n6', $Monto);
           $Data->bindParam(':n7', utf8_decode($_POST['Option']));
           $Data->bindParam(':n8', date('Y-m-d H:i:s'));
           $Data->bindParam(':n9', $estado);
           $Data->bindParam(':n10', utf8_decode($_POST['NIT']));
           $Data->bindParam(':n11', utf8_decode($_POST['programa']));           
           
           if(!$Data->execute())
            echo "<script>location.href ='http://www.fusalmo.org/index.php?option=com_content&view=article&id=157&Itemid=404&n=5';</script>";
           
           /**  Envio de Correo  **/
           $to = "$correo";
           $subject = "FUSALMO - Acceso para aportar Donativos";
           
           if(utf8_decode($_POST['Option']) == "glasswing")
            $AddText = "Llegar� un sobre para que puedas hacer tu donativo a la siguiente direcci�n: ". utf8_decode($_POST['address']) . " <br />";

           if(utf8_decode($_POST['Option']) == "salesianmissions")
            $AddText = "Llegar� un sobre para que puedas hacer tu donativo a la siguiente direcci�n: ". utf8_decode($_POST['address']) . " <br />";

           
           $message = "
                <div style='width: 90%; margin-left: 5%; text-align: justify;'>
                <hr />
                <h2>Saludos Amigo/a FUSALMO</h2>
                <hr />
                Gracias por tu apoyo! estimado/a ".utf8_decode($_POST['nombre'])." ". utf8_decode($_POST['apellido'])."<br />
                Ha accedido para hacer un donativo con ".$_POST['Option'].", tu n&uacute;mero de referencia FUSALMO es: $Id, si tienes alguna duda 
                sobre este proceso puedes contactarnos al fusalmo@fusalmo.org
                <br />$AddText<br />
                <center><img src='http://siipdb.fusalmo.org/images/LogoFUSALMO2015-mod-rec.png' style='width:50%; height: 50%;' /><br /></center>
                
                Su apoyo es muy importante, esto ayudar&aacute; para seguir mejorando la vida de a muchos j&oacute;venes que viven en situaciones 
                de riesgo en nuestro pais.
                
                <hr /> 
                    <div class='paragraph' style='text-align:center;'>
                        Intersecci&oacute;n Carretera a San Miguel&nbsp;y Calle Antigua a Tonacatepeque&nbsp;<br />
                        Frente a Vidr&iacute; Soyapango.&nbsp;El Salvador
                    </div>
                </div>";
            
            // More headers
           $headers .= "From: <noreply-donaciones@fusalmo.org>" . "\r\n";
           $headers .= "Bcc: fusalmo@fusalmo.org, liliaiv@fusalmo.org, desarrollo@fusalmo.org\r\n";
           //$headers .= "Bcc: m.calderon@fusalmo.org\r\n";
           $headers .= "MIME-Version: 1.0\r\n";
           $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
           mail($to,$subject,$message,$headers);
           
           
           /** Validaci�n de pago **/
           if(utf8_decode($_POST['Option']) == "pagadito")
           {
                try       
                {                  
                    require_once('config.php');
                    $soap = new SoapClient('https://comercios.pagadito.com/wspg/charges.php?wsdl');
                    
                    $params = array(
                        "uid"           => UID,
                        "wsk"           => WSK,
                        "format_return" => "php"
                    );  
                                      
                    $result = $soap->__call('connect', $params) ;
                    
                    $status = substr($result, 34, 6); 
                    $token = substr($result, 104, 32);   
                    
                    $cantidad = "1";

                    $details = array();                    
                    $details[] = array(
                                     "quantity"      => $cantidad,
                                     "description"   => $_POST['descripcion'],
                                     "price"         => $AuxMonto,
                                     "url_product"   => $_POST['url']
                                 );
                                 
                    $params = array(
                        "token"                     => "$token",
                        "ern"                       => $Id,
                        "amount"                    => $AuxMonto * 1,
                        "details"                   => json_encode($details),
                        "format_return"             => "php",
                        "currency"                  => "USD",
                        "custom_params"             => "",
                        "allow_pending_payments"    => "true"                                            
                    );
                    
                    $response = $soap->__call('exec_trans', $params);                    
                    $status = substr($response, 34, 6);   
                    
                    if($status == "PG1002")
                        echo "<script>location.href = 'https://comercios.pagadito.com/index.php?mod=login&token=$token';</script>";
                        /** Aqu�! abr� Cambio :D!! **/ 
                        
                    else
                        echo "<script>location.href = 'http://www.fusalmo.org/index.php?option=com_content&view=article&id=164';</script>";
                    
                }
                catch(Exception $e) 
                {     
                    echo "Bad Net";
                    //echo "<script>location.href = 'http://www5.fusalmo.org/index.php?option=com_content&view=article&id=164';</script>";  
                }                                    
           }
           
           else if(utf8_decode($_POST['Option']) == "paypal")
           { 
                echo "<form action='https://www.paypal.com/cgi-bin/webscr' method='post' name='paypal'>
                        <input type='hidden' name='cmd' value='_s-xclick'>
                        <input type='hidden' name='hosted_button_id' value='FEPHR7J6LUBHS'>
                        <img alt='' border='0' src='https://www.paypalobjects.com/en_US/i/scr/pixel.gif' width='1' height='1' onload='javascript:document.paypal.submit();'>
                      </form>";
           }
           
           else if(utf8_decode($_POST['Option']) == "glasswing")
           { 
                echo "<form action='http://glasswing.org/es/donate/' method='post' name='paypal'>                                                
                        <img alt='' border='0' src='https://www.paypalobjects.com/en_US/i/scr/pixel.gif' width='1' height='1' onload='javascript:document.paypal.submit();'>
                      </form>";
           }
           
           else if(utf8_decode($_POST['Option']) == "salesianmissions")
           { 
                echo "<form action='http://fusalmo.org/' method='post' name='paypal'>
                        <img alt='' border='0' src='https://www.paypalobjects.com/en_US/i/scr/pixel.gif' width='1' height='1' onload='javascript:document.paypal.submit();'>
                      </form>";
           }                      
           
           else
              echo "<script>location.href ='http://www.fusalmo.org/index.php?option=com_content&view=article&id=157&Itemid=404&n=6';</script>";
        }
        else
            echo "<script>location.href ='http://www.fusalmo.org/index.php?option=com_content&view=article&id=157&Itemid=404&n=4';</script>";
    }
    else
        echo "<script>location.href ='http://www.fusalmo.org/index.php?option=com_content&view=article&id=157&Itemid=404&n=3';</script>";
}

if(isset($_GET['n1'],$_GET['n2']))
{
       unset($_SESSION['statustrans'],$_SESSION['ref'],$_SESSION['datetime'],$_SESSION['status'],$_SESSION['data'],$_SESSION['ern']);
       
       require_once('config.php');
       $soap = new SoapClient('https://comercios.pagadito.com/wspg/charges.php?wsdl');
       $bdd = new PDO('mysql:host=localhost;dbname=poli09_payment', 'poli09_SiipUser', 'fusalmo2013');
       $ern = $_GET['n2']; 
       $params = array(
           "uid"           => UID,
           "wsk"           => WSK,
           "format_return" => "php"
       );                    
       $result = $soap->__call('connect', $params) ;
       $StatusConnection = substr($result, 34, 6);
       
       
       
       
       if("PG1001" == $StatusConnection)
       { 
           $token_trans = substr($result, 104, 32);                                                   
           $token = $_GET['n1'];
           
           $params = array(
                "token"         => $token_trans,
                "token_trans"   => $token,
                "format_return" => "php"
           );
            
           $result = $soap->__call('get_status', $params) ;                            
           $codtrans = substr($result, 34, 6);

            //081FAAEC
            //-66";s:9
           
           if($codtrans == "PG1003")       
           {
               $statustrans = substr($result, 118, 9);
               if($statustrans == "COMPLETED" || $statustrans == "VERIFYING")
               {
                  $_SESSION['statustrans'] = $statustrans; 
                  $_SESSION['ref'] =      substr($result, 145, 8);  
                  $_SESSION['datetime'] = substr($result, 184, 19);
               }
               else
               {
                   $statustrans = substr($result, 119, 10);
                   if($statustrans == "REGISTERED")
                   {
                      $_SESSION['statustrans'] = $statustrans;   
                      $_SESSION['datetime'] = substr($result, 212, 19);
                   }
                   else if($statustrans == "REVOKED")
                   {
                      $statustrans = substr($result, 118, 7);
                      $_SESSION['statustrans'] = $statustrans;                        
                      $_SESSION['datetime'] = substr($result, 182, 19);                
                   }
               }   
                             
               $_SESSION['status'] = "Process"; 
               $_SESSION['data'] = "Petici�n llego al servidor";
               $_SESSION['ern']  = $ern;
               
               $Update = $bdd->prepare("Update InformacionCliente set Estado = 'Petici�n procesada' where IdInformacion = '$ern'");
               $Update->execute();               
                
           }
           else
           {
                $Update = $bdd->prepare("Update InformacionCliente set Estado = 'Petici�n no procesada' where IdInformacion = '$ern'");
                $Update->execute();
            
                $_SESSION['status'] = "Bad Process"; 
                $_SESSION['data'] = "No se ha sido procesada correctamente la petici�n de estado de transacci�n.";
           }
       }
       else
       {
            
            $Update = $bdd->prepare("Update InformacionCliente set Estado = 'Cancelado' where IdInformacion = '$ern'");
            $Update->execute();
            
            $_SESSION['status'] = "Cancel";
            $_SESSION['data'] = "No fue posible la conexi&oacute;n o su transacci&oacute;n fue cancelada";
            echo "<script language='javascript'>window.location='../?option=com_content&view=article&id=164'</script>";
       }

       if($statustrans == "COMPLETED")
       {  
        
           $Data = $bdd->prepare("Select * from InformacionCliente where IdInformacion = :n1 ");
           $Data->bindParam(':n1', $_GET['n2']);
           $Data->execute();
            
           $DataC = $Data->fetch();
           /**  Envio de Correo  **/
           $to = "$DataC[4]";
           $subject = "FUSALMO - Detalle de Transacci�n";
            
           $message = "
                <div style='width: 90%; margin-left: 5%; text-align: justify;'>
                <hr />
                <h2>Amigo $DataC[2] $DataC[3]</h2>
                <hr />
                Ha donado $$DataC[6], le mostramos los detalles de la transferencia:<br /><br />
                <table style='width: 50%; margin-left: 25%'>
                    <tr><td>Estado de la transferencia:</td><td>$statustrans </td></tr>
                    <tr><td>Referencia FUSALMO: </td><td>". $_GET['n2'] ." </td></tr>
                    <tr><td>Referencia Pagadito:</td><td>". $_SESSION['ref'] ." </td></tr>
                </table>
                <br /><br />
                                
                Puede ingresar a <a href='http://www.fusalmo.org/'>http://www.fusalmo.org/</a>
                para conocer m�s acerca de nosotros y nuestros proyectos por la Juventud.   
                <br /><br />                
                <center>
                    <img src='http://siipdb.fusalmo.org/images/LogoFUSALMO2015-mod-rec.png' style='width:50%; height: 50%;' /><br />                        
                </center>
                <hr /> 
                <div class='paragraph' style='text-align:center;'>
                    Intersecci&oacute;n Carretera a San Miguel&nbsp;y Calle Antigua a Tonacatepeque&nbsp;<br />
                    Frente a Vidr&iacute; Soyapango.&nbsp;El Salvador
                </div>
                </div>";

           $headers .= "From: <noreply-donaciones@fusalmo.org>" . "\r\n";
           $headers .= "Bcc: fusalmo@fusalmo.org, liliaiv@fusalmo.org, desarrollo@fusalmo.org\r\n";
           //$headers .= "Bcc: m.calderon@fusalmo.org\r\n";
           $headers .= "MIME-Version: 1.0\r\n";
           $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
           mail($to,$subject,$message,$headers);
        }
        
        echo "<script language='javascript'>window.location='../?option=com_content&view=article&id=164'</script>";            
}
echo "<script language='javascript'>window.location='../?option=com_content&view=article&id=164'</script>";
?>