<meta http-equiv="content-type" content="text/html" charset="ISO-8859-1" />

<?php
/**
 * Mi Tienda Pagadito 1.2 (API PHP)
 *
 * Es un ejemplo de plataforma de e-commerce, que realiza venta de productos
 * electr&oacute;nicos, y efect�a los cobros utilizando Pagadito, a trav�s del WSPG.
 *
 * index.php
 *
 * Es el script de la p�gina inicial de Mi Tienda Pagadito. Aqu� se muestran
 * los productos que se encuentran a la venta.
 *
 * LICENCIA: �ste c&oacute;digo fuente es de uso libre. Su comercializaci&oacute;n no est�
 * permitida. Toda publicaci&oacute;n o menci&oacute;n del mismo, debe ser referenciada a
 * su autor original Pagadito.com.
 *
 * @author      Pagadito.com <soporte@pagadito.com>
 * @copyright   Copyright (c) 2013, Pagadito.com
 * @version     1.2
 * @link        https://dev.pagadito.com/index.php?mod=docs&hac=wspg
 */
 $paises = array(
		"Afghanistan",
		"Albania",
		"Algeria",
		"Andorra",
		"Angola",
		"Antigua and Barbuda",
		"Argentina",
		"Armenia",
		"Australia",
		"Austria",
		"Azerbaijan",
		"Bahamas",
		"Bahrain",
		"Bangladesh",
		"Barbados",
		"Belarus",
		"Belgium",
		"Belize",
		"Benin",
		"Bhutan",
		"Bolivia",
		"Bosnia and Herzegovina",
		"Botswana",
		"Brazil",
		"Brunei",
		"Bulgaria",
		"Burkina Faso",
		"Burundi",
		"Cambodia",
		"Cameroon",
		"Canada",
		"Cape Verde",
		"Central African Republic",
		"Chad",
		"Chile",
		"China",
		"Colombia",
		"Comoros",
		"Congo (Brazzaville)",
		"Congo",
		"Costa Rica",
		"Cote d'Ivoire",
		"Croatia",
		"Cuba",
		"Cyprus",
		"Czech Republic",
		"Denmark",
		"Djibouti",
		"Dominica",
		"Dominican Republic",
		"East Timor (Timor Timur)",
		"Ecuador",
		"Egypt",
		"El Salvador",
		"Equatorial Guinea",
		"Eritrea",
		"Estonia",
		"Ethiopia",
		"Fiji",
		"Finland",
		"France",
		"Gabon",
		"Gambia, The",
		"Georgia",
		"Germany",
		"Ghana",
		"Greece",
		"Grenada",
		"Guatemala",
		"Guinea",
		"Guinea-Bissau",
		"Guyana",
		"Haiti",
		"Honduras",
		"Hungary",
		"Iceland",
		"India",
		"Indonesia",
		"Iran",
		"Iraq",
		"Ireland",
		"Israel",
		"Italy",
		"Jamaica",
		"Japan",
		"Jordan",
		"Kazakhstan",
		"Kenya",
		"Kiribati",
		"Korea, North",
		"Korea, South",
		"Kuwait",
		"Kyrgyzstan",
		"Laos",
		"Latvia",
		"Lebanon",
		"Lesotho",
		"Liberia",
		"Libya",
		"Liechtenstein",
		"Lithuania",
		"Luxembourg",
		"Macedonia",
		"Madagascar",
		"Malawi",
		"Malaysia",
		"Maldives",
		"Mali",
		"Malta",
		"Marshall Islands",
		"Mauritania",
		"Mauritius",
		"Mexico",
		"Micronesia",
		"Moldova",
		"Monaco",
		"Mongolia",
		"Morocco",
		"Mozambique",
		"Myanmar",
		"Namibia",
		"Nauru",
		"Nepa",
		"Netherlands",
		"New Zealand",
		"Nicaragua",
		"Niger",
		"Nigeria",
		"Norway",
		"Oman",
		"Pakistan",
		"Palau",
		"Panama",
		"Papua New Guinea",
		"Paraguay",
		"Peru",
		"Philippines",
		"Poland",
		"Portugal",
		"Qatar",
		"Romania",
		"Russia",
		"Rwanda",
		"Saint Kitts and Nevis",
		"Saint Lucia",
		"Saint Vincent",
		"Samoa",
		"San Marino",
		"Sao Tome and Principe",
		"Saudi Arabia",
		"Senegal",
		"Serbia and Montenegro",
		"Seychelles",
		"Sierra Leone",
		"Singapore",
		"Slovakia",
		"Slovenia",
		"Solomon Islands",
		"Somalia",
		"South Africa",
		"Spain",
		"Sri Lanka",
		"Sudan",
		"Suriname",
		"Swaziland",
		"Sweden",
		"Switzerland",
		"Syria",
		"Taiwan",
		"Tajikistan",
		"Tanzania",
		"Thailand",
		"Togo",
		"Tonga",
		"Trinidad and Tobago",
		"Tunisia",
		"Turkey",
		"Turkmenistan",
		"Tuvalu",
		"Uganda",
		"Ukraine",
		"United Arab Emirates",
		"United Kingdom",
		"United States",
		"Uruguay",
		"Uzbekistan",
		"Vanuatu",
		"Vatican City",
		"Venezuela",
		"Vietnam",
		"Yemen",
		"Zambia",
		"Zimbabwe"
	);

  $program = array(
		"Programa Integral Juvenil",
		"Escuelas Deportivas",
		"Escuelas de Arte",
		"Oratorio",
		"Red de Jovenes",
		"Tecnolog&iacute;a",
		"Emprendimientos/Formaci&oacute;n laboral",
		"Atenci�n Psicopedag&oacute;gica",
		"II Cena de la Alegr�a - Los Angeles"
	);
?>

<script>
function OnOtro()
{
    if(document.getElementById("otro").checked)
    {
        document.getElementById("montootro").disabled=false;
        document.getElementById("montootro").value= 10;
        document.getElementById("montootro").focus();          
    }
    else
    {
        document.getElementById("montootro").disabled = true;
        document.getElementById("montootro").value= null;  
    }
}

function Recibo()
{
    var pais = document.getElementById("pais").value;
        
    if(pais == "United States" || pais == "El Salvador")
    {
        document.getElementById("Recibo").style.visibility = "visible";
        document.getElementById("trNIT").style.visibility = "hidden";
    }
    else
    {
        document.getElementById("Recibo").style.visibility = "hidden";
        document.getElementById("trNIT").style.visibility = "hidden";
        document.getElementById('NIT').value = "";
        document.getElementById("r2").checked = true;
    }
}

function verificacionNIT()
{
    if(document.getElementById("r1").checked)
    {
        document.getElementById("trNIT").style.visibility = "visible";
        document.getElementById('NIT').value = "";
    }    
    if(document.getElementById("r2").checked)
    {
        document.getElementById("trNIT").style.visibility = "hidden";
        document.getElementById('NIT').value = "";        
    }    
}
</script>

<form method="post" action="pay/engine.php">
<input type="hidden" value="Donaciones Amigos FUSALMO" name="descripcion" />
<input type="hidden" value="http://www.fusalmo.org/" name="url" />

<table style="width: 100%;">
<tr>
    <td style="width: 50%;  vertical-align: top;">
        <table style=" width: 100%; border-color: skyblue; border-radius: 10px; border-style: 1px;">
        <tr><td colspan="2"><h3>1 - Informaci&oacute;n de Facturaci�n</h3></td></tr>
         <tr>
            <td style="width: 30%;">Programas / Programs:</td>
            <td style="text-align: left;">
                <select name="programa" id="programa" style="width: 80%;" onchange="Recibo()" >                
                    <?php
            	           foreach($program as $value) 
                           {
                                if($value  == "II Cena de la Alegr�a - Los Angeles")
                                    echo "<option value='$value' selected='true'>$value</option>";
                                    
                                else
                                    echo "<option value='$value'>$value</option>";
                           }                         
                                
                    ?>
                </select>
            </td>    
        </tr>
        <tr>
            <td style="width: 30%;">Nombres / First Name:</td>
            <td style="text-align: left;"><input type="text" value="" name="nombre"  maxlength="50"  style="width: 80%;" required="true" /></td>    
        </tr>
        <tr>
            <td style="width: 30%;">Apellidos / Last Name:</td>
            <td style="text-align: left;"><input type="text" value="" name="apellido"  maxlength="50"  style="width: 80%;" required="true" /></td>    
        </tr>
        <tr>
            <td style="width: 30%;">Correo / Email:</td>
            <td style="text-align: left;"><input type="email" value="" name="correo"  maxlength="50" style="width: 80%;" required="true" /></td>    
        </tr>        
        <tr>
            <td style="width: 30%;">Pa&iacute;s / Country:</td>
            <td style="text-align: left;">
                <select name="pais" id="pais" style="width: 80%;" onchange="Recibo()" >                
                    <?php
            	           foreach($paises as $value) 
                           {
                                if($value  == "El Salvador")
                                    echo "<option value='$value' selected='true'>$value</option>";
                                    
                                else
                                    echo "<option value='$value'>$value</option>";
                           }                         
                                
                    ?>
                </select>
            </td>    
        </tr>
        <tr>
            <td style="width: 30%;">NIT :</td>
            <td style="text-align: left;"><input type="text" value="" name="NIT"  maxlength="15" style="width: 80%;" /></td>    
        </tr>    
        <tr id="Recibo" style="visibility: visible;">
            <td colspan="2">�Quieres un recibo de donaci�n? / Do you want a donation receipt?:
                <table style="width: 80%; margin-left: 10%;">
                    <tr>
                        <td><input type="radio" id="r1" value="Y" name='recibo' onchange="verificacionNIT()" />Si / Yes</td>
                        <td><input type="radio" id="r2" value="N" name='recibo' onchange="verificacionNIT()" checked="true" />No / Not</td>
                    </tr>
                </table>
            </td>
        </tr> 
        <tr id="trNIT" style="visibility: hidden;">
            <td style="width: 30%;">NIT (con Guiones):</td>
            <td style="text-align: left;"><input type="text" id="NIT" value="" name="NIT" style="width: 80%;" />
                <br />Nos comunicaremos con usted por correo electr�nico para mas detalles.
            </td>    
        </tr>                
        </table>
    </td>
    <td style="width: 50%; vertical-align: top;">
    
    <table style="width: 80%;">        
        <tr><td colspan="2"><h3>2 - Con cu�nto apoyar</h3></td></tr>
        <tr>
            <td colspan="2" >
                <table style="width: 100%; margin-left: 15%; vertical-align: top;">
                    <tr>
                        <td style="width: 50%;"><input type="radio" name="monto" value="5" onchange="OnOtro()" />$5.00</td>
                        <td style="width: 50%;"><input type="radio" name="monto" value="10" onchange="OnOtro()" />$10.00</td>                
                    </tr>
                    <tr>
                        <td style="width: 50%;"><input type="radio" name="monto" value="20" onchange="OnOtro()" />$20.00</td>
                        <td style="width: 50%;"><input type="radio" name="monto" checked="true" value="65" onchange="OnOtro()" />$65.00</td>
                    </tr>            
                    <tr>
                        <td style="width: 50%;"><input type="radio" name="monto" value="100" onchange="OnOtro()" />$100.00</td>
                        <td style="width: 50%;"><input type="radio" name="monto" value="200" onchange="OnOtro()" />$200.00</td>
                    </tr>
                    <tr>
                        <td><input type="radio" id="otro" name="monto" value="Otro" onchange="OnOtro()"  />Otra Cantidad</td>
                        <td><input type="text" id="montootro" name="montootro" min="1" value="" style="width: 40%;" disabled="true" pattern="^[0-9]+(\.[0-9]{1,2})?$" /></td>
                    </tr>
                </table>

            </td>
        </tr>        
       </table>
          
    </td>
</tr>
<tr>
    <td>
        <table style="width: 100%; margin-top: -17em;">
            <tr><td colspan="2"><h3>3 - Opciones de donaci&oacute;n</h3></td></tr>
            <tr style="height: 80px;">
                <td style="vertical-align: central; text-align: center; width: 25%;">
                    <input type="radio" name="Option" value="paypal" checked="true" />
                    <img src="pay/img/pg-logo-paypal.png" style="max-width: 200px;" /> </td>                   
                    <br /><br /><br />
                   <td style="vertical-align: central; text-align: center; width: 25%;"> 
                    <input type="radio" name="Option" value="pagadito" /> <!--<em style="color: red;">Proximanente tambi�n con:</em>--!>
                    <img src="pay/img/pg-logo-pagadito.jpg" style="max-width: 200px;" /></td>
                    <br /><br /><br />       
                    <td style="vertical-align: central; text-align: center; width: 25%;">
                    <input type="radio" name="Option" value="glasswing" /> 
                    <img src="pay/img/logoglasswing.png" style="max-width: 200px; padding-left: 20px;" />         

                </td>
                </tr>
                <tr>
                <td style="vertical-align: central; text-align: left; width: 25%;">                    

                    <input type="radio" name="Option" value="salesianmissions"  style="margin-top: -6em;"/> 
                    <img src="pay/img/pg-logo-salesian.jpg" style="max-width: 100px; padding-left: 20px; margin-top: -6em;" /> </td>
                   <td colspan="2">         
                    <b>Salesian Missions</b> </br>
                    Puedes recibir un sobre de donativo / receives a packet for your donative<br />                                          
                    <strong>Address / Direcci�n</strong>
                    <textarea style="width: 100%" name="address">
                    </textarea>
                </td>
                <!--
                <td rowspan="3" style="width: 50%; visibility: hidden;">
                    <table style="width: 70%; margin-left: 15%;">
                        <tr>
                            <td style="text-align: center;"><a href="https://www.salesianmissions.org/ways-to-help/donate" target="_blank"><img src="pay/img/pg-logo-salesian.jpg" /><br />Puedes apoyarnos tambien desde <br />Salesian Missions</a></td>
                            <td></td>
                        </tr> 
                    </table>
                </td>
                --!>
            </tr>
        </table>    
    </td>
    <td>
        <table style="text-align: center; width: 100%; margin-top: -15em;">
            <tr>
                <td style="vertical-align: central; text-align: center; width: 25%;">                    
                <style>
                .Donar
                {
                    width: 250px;
                    background-color: #05568C;
                    height: 50px;
                    color: white;
                }
                </style>
                <br />
                <input type="submit" name="Enviar" value="&iexcl;Iniciar Donaci&oacute;n!" class="Donar" />
                <br />Clic una vez / Please click only once 
                </td>
            </tr>
            <tr>
                <th><br /><br />Estamos para apoyar a los jovenes<br /> <a href="pay/Plantilla_de_Politicas_Pagadito.pdf" target="_blank" >si tienes alguna duda puedes descargar nuestras politicas de uso</a>.</th></tr>
        </table>
    </td>
</tr>
</table>
</form>