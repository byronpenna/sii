<?php

    session_start();
    date_default_timezone_set('Etc/GMT+6');
    $bdd = new PDO('mysql:host=localhost;dbname=poli09_payment', 'poli09_SiipUser', 'fusalmo2013');
        
    /**  Busqueda de Datos en la Base de Datos  **/
    $Select = $bdd->prepare("Select * from InformacionCliente as c 
                             inner join DetalleTransaccion as d on d.ERN = c.IdInformacion
                             ORDER BY c.Id DESC LIMIT 0,1");
    $Select->execute();               
   
    $Data = $Select->fetch();
    
    $auxInicio = strtotime(date('Y-m-d H:i:s'));
    $auxFin = strtotime($Data[8]);
    $auxdiff = $auxInicio - $auxFin;
    

    
    if(!($_SESSION['status']  == "Cancel" || $_SESSION['status'] == ""))
    {
        if(!($_SESSION['status']  == "Bad Process"))
        {
            switch($_SESSION['statustrans'])
            {
                case "COMPLETED":

                    $msgPrincipal = "Atenci&oacute;n, Su donativo fue exitoso";
                    break;
                    
                case "REGISTERED":
                    /*
                     * Tratamiento para una transacci&oacute;n a�n en
                     * proceso.
                     */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                    $msgPrincipal = "Atenci&oacute;n, La transacci&oacute;n fue cancelada.<br /><br />";
                    break;
                case "FAILED":
                    /*
                     * Tratamiento para una transacci&oacute;n fallida.
                     */
                     $msgPrincipal = "Atenci&oacute;n, La transacci&oacute;n fue cancelada.<br /><br />";
                default:
                    /*
                     * Por ser un ejemplo, se muestra un mensaje
                     * de error fijo.
                     */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                    $msgPrincipal = "Atenci&oacute;n, La transacci&oacute;n no fue realizada.<br /><br />";
                    break;
            }
    ?>
    <div style="visibility: hidden;"><?php echo $_SESSION['data'];?></div>
    <table style="width: 80%; font-size: large; margin-left: 10%;">
        <tr><td colspan="2"><h2>Resultado de Operaci&oacute;n</h2></td></tr>
        
        <?php if($_SESSION['ref'] != ""){?>
        <tr><td style="width: 30%;">Referencia Pagadito: </td><td style="width: 70%;"></td></tr>
        <?php }?>        
        <tr><td style="vertical-align: top;">Estado de operaci&oacute;n: </td>
            <td style="width: 70%;"><?php echo $_SESSION['statustrans'];?>
            <br /><?php echo "$msgPrincipal";?></strong></td></tr>
        <tr><td colspan="2"><br /></td></tr>
        <tr><td>Fecha de transacci&oacute;n: </td><td><?php echo $_SESSION['datetime'] . "  "?>(GMT-4)</td></tr>
        <tr><td>Hora El Salvador: </td><td><?php echo date('Y-m-d H:i:s') . "  "?>(GMT-6)</td></tr>
        <tr><td>C&oacute;digo FUSALMO: </td><td><?php echo $_SESSION['ern'];?></td></tr>
        <tr><td colspan="2"></td></tr>
    </table>
    <?php
        }
        else
        {
    ?>
        <table style="width: 60%; font-size: large; margin-left: 20%;">
            <tr><td colspan="2"><h2>Operaci&oacute;n no procesada.</h2></td></tr>            
            <tr>
                <td colspan="2">
                    Esto puede ser por cualquiera de las siguientes razones:
                    <ul>
                        <li>La transacci&oacute;n solicitada no ha sido registrada.</li>
                        <li>Conexi&oacute;n con Pagadito se encuentra deshabilitada.</li>
                        <li>La transacci&oacute;n ha sido denegada debido a que excede el monto m&aacute;ximo por transacci&oacute;n.</li>
                        <li>La transacci&oacute;n ha sido denegada debido a que el monto es menor al m&iacute;nimo permitido.</li>
                    </ul>
                </td>
            </tr>
            <tr><td><br /><br /></td></tr>
            <tr>
                <td>
                    <span class="mymarg">
                    <a class="yjanchor " href="/index.php?option=com_content&amp;view=article&amp;id=157&amp;Itemid=404">
                    <span class="yjm_has_none"><span class="yjm_title"><i class="fa fa-credit-card"></i>Tu donaci&oacute;n es importante, intenta nuevamente</span></span></a></span>
                </td>
            </tr>
        </table>
    <?php 
        }
	}
    else
    {
    ?>
        <table style="width: 60%; font-size: large; margin-left: 20%;">
            <tr><td colspan="2"><h2>Operaci&oacute;n Cancelada.</h2></td></tr>            
            <tr>
                <td colspan="2">
                    Esto puede ser por cualquiera de las siguientes razones:
                    <ul>
                        <li>Has cancelado la transacci&oacute;n</li>
                        <li>El tiempo de espera estimado a finalizado</li>
                        <li>No fue posible conectarse con el servidor de Pagadito</li>
                    </ul>
                </td>
            </tr>
            <tr><td><br /><br /></td></tr>
            <tr>
                <td>
                    <span class="mymarg">
                    <a class="yjanchor " href="/index.php?option=com_content&amp;view=article&amp;id=157&amp;Itemid=404">
                    <span class="yjm_has_none"><span class="yjm_title"><i class="fa fa-credit-card"></i>Tu donaci&oacute;n es importante, intenta nuevamente</span></span></a></span>
                </td>
            </tr>
        </table>
<?php 
      }
      
      unset($_SESSION['statustrans'],$_SESSION['ref'],$_SESSION['datetime'],$_SESSION['status'],$_SESSION['data'],$_SESSION['ern']);

?>
<input type="hidden" value="<?php echo $_SESSION['response'];?>" name="response" />