<head>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Información Institucional FUSALMO</title>
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="Empleados/POA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">


</head>

<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="10"><h2 style="color: #197198;">Plan Estrategico 2018 - 2022</h2></td></tr>
        <tr><td colspan="10"><hr color='skyblue' /></td></tr>
        <tr style='display:  inline-block;'>
   </tr>
 </tr>
</table>
<table  style="width: 100%;  border-collapse: collapse;">
     <tr>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Objetivos Estrategicos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Indicadores</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Productos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Avances</th>
     </tr>
     </tr>
     <tr>
      <td  style="border: 1px solid #dddddd; padding: 8px;"><b>Resultado Directo /Global.</b><br>
Desarrollar integralmente las capacidades de la ni&ntilde;ez, adolescencia y juventud, fomentando principios  y valores con carisma salesiano para mejorar la calidad de vida en su entorno familiar y comunitario.</td>
      <td  style="border: 1px solid #dddddd; padding: 8px;">Hasta diciembre 2022, ni&ntilde;ez, adolescentes y j&oacute;venes atendidos por FUSALMO han mejorado su desarrollo integral de capacidades, fomentando principios y valores con carisma salesiano para mejorar la calidad de vida en su entorno familiar y comunitario.<br><br>
<i>LB: <br>
&#8226; Nivel de desarrollo integral de niñez, adolescentes y j&oacute;venes en el 2018 al ingresar a programas de FUSALMO. (LB de la plataforma SIIF)<br>
&#8226; Nivel de calidad de vida de niñez, adolescente y j&oacute;venes en su entorno familiar y comunitario al ingresar a programas de FUSALMO en el 2018. (LB de la plataforma SIIF)<br><br>

FV:  adecuarlo<br>
&#8226;(LB de la plataforma SIIF)</i></td>
      <td  style="border: 1px solid #dddddd; padding: 8px;"></td>
      <td  style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%; background-color: #b01212;">
    <span style="background-color: #b01212;">6% completado</span>
  </div>
</div></td>
    

    </tr>
     </table>
     <br>
     <br>
     <br>
     <table  style="width: 100%">
            
              <td ><a href="?l=Linea1">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Educacion Integral Liberadora" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              
              <td ><a href="?l=Linea2">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Gestion Socio Laboral" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea3">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Protagonismo, Participacion e Incidencia. " name="Enviar" class="botonG"  style="width: 236px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea4">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Tecnologia Educativa" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea5">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Fortalecimiento Organizacional" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

             </tr>
               
             </tr>
        <tr>
            <td colspan="10"><hr color='skyblue' /></td>
        </tr>
    </table>
</div>

<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px">
  <table style="width: 100%;">
            <tr>
             <td style="margin-left: 10px;">
              
        </tr>
    </table>
</div>