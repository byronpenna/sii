<head>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Información Institucional FUSALMO</title>
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="Empleados/POA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">


</head>

<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="10"><h2 style="color: #197198;">Plan Estrategico 2018 - 2022</h2></td></tr>
        <tr><td colspan="10"><h4 style="color: #197198;">Linea 2 - Socio Laboral</h4></td></tr>
        <tr><td colspan="10"><hr color='skyblue' /></td></tr>
        <tr style='display:  inline-block;'>

<table  style="width: 100%;  border-collapse: collapse;">
     <tr>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Objetivos Estrategicos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Indicadores</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Productos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Avances</th>
     </tr>
     </tr>
     <tr>
      <td rowspan="5"  style="border: 1px solid #dddddd; padding: 8px;"><b>OE2:</b><br> J&oacute;venes provenientes de los programas atendidos por FUSALMO y comunidades aleda&ntilde;as cuentan con mejores herramientas, para el desarrollo de habilidades personales, sociales, educativas y laborales, aportando productivamente a la sociedad.
</td>
      <td rowspan="5" style="border: 1px solid #dddddd; padding: 8px;">A octubre del 2022 al menos 1500 j&oacute;venes cuentan con herramientas para mejorar las condiciones de vida personal y familiar; de las cuales el 50% de los beneficiarios son mujeres, aportando a la economía familiar.<br><br>

<i>LB: <br>
N&uacute;mero de j&oacute;venes atendidos por año hasta el 2022 con mejores herramientas
 Levantamiento de informaci&oacute;n sobre sus condiciones de vida y sus aportes a la economía familiar.<br><br>
 
FV:<br> An&aacute;lisis de resultados de l&iacute;nea de base en el SIIF y de la Plataforma de Intermediaci&oacute;n Laboral</i>
</td>
      <td  style="border: 1px solid #dddddd; padding: 8px;">1. Intermediaci&oacute;n, inserci&oacute;n y seguimiento laboral</td>
      <td  style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>

      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">2.  Formaci&oacute;n t&eacute;cnica-vocacional </td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>
      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">3. Curr&iacute;culas estandarizadas y complementarias  de orientaci&oacute;n para la vida y el trabajo </td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>
      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">4. Educaci&oacute;n a trav&eacute;s de modalidades flexibles</td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>
    
    <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">5.  Gesti&oacute;n emprendedora</td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>

    </tr>
     </table>
     <br>
     <br>
     <br>
     <table  style="width: 100%">
            
              <td ><a href="?l=Linea1">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Educacion Integral Liberadora" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              
              <td ><a href="?l=Linea2">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Gestion Socio Laboral" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea3">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Protagonismo, Participacion e Incidencia. " name="Enviar" class="botonG"  style="width: 236px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea4">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Tecnologia Educativa" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea5">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Fortalecimiento Organizacional" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              <td ><a href="?l=Planestrategico">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="General" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
             </tr>
               
             </tr>
        <tr>
            <td colspan="10"><hr color='skyblue' /></td>
        </tr>
    </table>
</div>

<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px">
  <table style="width: 100%;">
            <tr>
             <td style="margin-left: 10px;">
              
        </tr>
    </table>
</div>