
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Sistema de Plan Operativo Anual</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-light" id="page-top">


    

  </style>
  <!-- Navigation-->
      <!-- Breadcrumbs-->
      
      <div class="row">
        
<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
       <tr><td colspan="7"><h2 style="color: #197198;">Plan Operativo Anual</h2></td></tr>
        <tr><td colspan="7"><hr color='skyblue' /></td></tr>
        <tr style='display:  inline-block;'>

        <?php                

                $MisCargos = $bddr->prepare("SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo
                                            FROM CargosAsignacion AS ca
                                            INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                                            INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
                                            WHERE ca.IdEmpleado = '".$_SESSION["IdUsuario"]."' and ca.FechaFin = '0000-00-00'");                                            
                                                                                   
                $MisCargos->execute();
                
                
                if(isset($_POST['MiCargo']))
                    $IdCharge = $_POST['MiCargo'];
                
                else
                    $IdCharge = ''; 
                    
                $Cargos = "";
        ?> 
        <tr><form method="post" action="Empleado.php?l=Request">
            <td colspan="4">                
                Cargo:
                <select name="MiCargo" id="MiCargo">                         
                    <?php                                                    
                         while($Datos = $MisCargos->fetch())
                           {
                                if($IdCharge == $Datos[1])                                    
                                    echo "<option value='$Datos[1]' selected='true' >$Datos[3]</option>";                                    
                                else
                                    echo "<option value='$Datos[1]' >$Datos[3]</option>";
                                
                                $Cargos = $Datos[1];
                                $IdCargosAux = $Datos[2];                                                                               
                           }
                           if($MisCargos->rowCount() == 0)
                                echo "<option value='...' >...</option>";
                        ?>
                </select>
                <br /><br />
            </td>

        </tr>
     </form>
    <tr></tr>  <tr></tr>  <tr></tr>  <tr></tr>  <tr></tr>
     <tr style='display:  inline-block;'>
      <td><a href="?l=AgregarPOA">
             <input type="hidden" name="charge" value="<?php echo $Cargos?>" />
                <input type="submit" value="Agregar POA" name="Enviar" class="botonG" style="width: 105px;" /></a>
             </td>
              <td><a href="?l=VerPOA">
              <form action="Empleado.php?l=VerPOA" method="post">
 <input style="width: 0px ; height: 0px; " type="text"  hidden="true" value="2019" name="b">
             <input type="hidden" name="charge" value="<?php echo $Cargos?>" />
                <input type="submit" value="Ver POAS" name="Enviar" class="botonG" style="width: 105px;" /></a>
                </form>
             </td>
             <td><a href="?l=Planestrategico">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
             <input type="hidden" name="charge" value="<?php echo $Cargos?>" />
                <input type="submit" value="Plan Estrategico" name="Enviar" class="botonG" style="width: 105px;" /></a>
             <?php }?>
               </td>
              <td><a href="?l=stats">
             <input type="hidden" name="charge" value="<?php echo $Cargos?>" />
                <input type="submit" value="Ver Estadisticas" name="Enviar" class="botonG" style="width: 105px;" /></a>
             </td>
              <td><a href="?l=calendario">
             <input type="hidden" name="charge" value="<?php echo $Cargos?>" />
                <input type="submit" value="Ver Calendario" name="Enviar" class="botonG" style="width: 105px;" /></a>
             </td>
</tr></tr>

    </div>     
</div></table>
</div>
        
    </a>
    <!-- Logout Modal-->
    
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <script src="js/pestañas.js"></script>
  </div>
</tr>
</tr>
</table>
</div>
</div>

</body>

</html>
