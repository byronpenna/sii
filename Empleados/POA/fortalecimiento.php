<head>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Información Institucional FUSALMO</title>
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="Empleados/POA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">


</head>
<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="10"><h2 style="color: #197198;">Plan Estrategico 2018 - 2022</h2></td></tr>
        <tr><td colspan="10"><h4 style="color: #197198;">Linea 4 - Tecnologia Educativa </h4></td></tr>
        <tr><td colspan="10"><hr color='skyblue' /></td></tr>
        <tr style='display:  inline-block;'>

<table  style="width: 100%;  border-collapse: collapse;">
     <tr>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Objetivos Estrategicos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Indicadores</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Productos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Avances</th>
     </tr>
     </tr>
     <tr>
      <td rowspan="8"  style="border: 1px solid #dddddd; padding: 8px;"><b>OE 5:</b><br> FUSALMO con un modelo de gesti&oacute;n fortalecido lograr brindar m&aacute;s y mejores servicios a sus usuarios y tienen bases para la auto sostenibilidad.
</td>
      <td rowspan="8" style="border: 1px solid #dddddd; padding: 8px;">A octubre del 2022 FUSALMO ha mejorado su modelo de gesti&oacute;n en base a los 5 FE, y brinda m&aacute;s y mejores servicios a los usuarios.<br><br>
<i><b>LB:</b> <br>
&#8226;N&uacute;mero y calidad de los servicios brindados a usuarios en el 2017<br><br>
  
<b>FV:</b><br> 
&#8226;Entrevistas a usuarios, socios y empleados de FUSALMO.



</td>
      <td  style="border: 1px solid #dddddd; padding: 8px;">1.  Estrategia de Comunicaciones y Marketing</td>
      <td  style="border: 1px solid #dddddd; padding: 8px;">
        <div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div>
</td>

      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">2. Visibilizar y sensibilizar la misi&oacute;n, visi&oacute;n y valores.</td>
        <td style="border: 1px solid #dddddd; padding: 8px;">
          <div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div>
        </td>
      </tr>
      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">3. Normas sobre Manejo de posici&oacute;n apol&iacute;tica  de FUSALMO. </td>
        <td style="border: 1px solid #dddddd; padding: 8px;">
          <div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div>
        </td>
      </tr>

      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">4. Plan de Fortalecimiento de Capacidades</td>
        <td style="border: 1px solid #dddddd; padding: 8px;">
          <div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div>
        </td>
      </tr>
    
    <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">5. Diagn&oacute;stico de las condiciones de trabajo, </td>
        <td style="border: 1px solid #dddddd; padding: 8px;">
          <div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div>
        </td>
      </tr>

      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">6. Programa de voluntariado </td>
        <td style="border: 1px solid #dddddd; padding: 8px;">
          <div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div>
        </td>
      </tr>
      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">7. Plan de renovaci&oacute;n de equipos tecnol&oacute;gicos. </td>
        <td style="border: 1px solid #dddddd; padding: 8px;">
         <div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div>
        </td>
      </tr>
      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">8. Manual de seguridad de FUSALMO Pol&iacute;tica de contrataci&oacute;n de RRHH con banda salarial acorde a las competencias y perfiles.</td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div>
</td>
      </tr>



    </tr>
     </table>
     <br>
     <br>
     <br>
     <table  style="width: 100%">
            
              <td ><a href="?l=Linea1">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Educacion Integral Liberadora" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              
              <td ><a href="?l=Linea2">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Gestion Socio Laboral" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea3">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Protagonismo, Participacion e Incidencia. " name="Enviar" class="botonG"  style="width: 236px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea4">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Tecnologia Educativa" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea5">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Fortalecimiento Organizacional" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              <td ><a href="?l=Planestrategico">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="General" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
             </tr>
               
             </tr>
        <tr>
            <td colspan="10"><hr color='skyblue' /></td>
        </tr>
    </table>
</div>

<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px">
  <table style="width: 100%;">
            <tr>
             <td style="margin-left: 10px;">
              
        </tr>
    </table>
</div>