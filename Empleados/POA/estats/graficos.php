<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <style type="text/css">

    </style>
  </head>
  <body>
<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script src="https://code.highcharts.com/modules/exporting.src.js"></script>

<script src="code/highcharts.js"></script>
<script src="code/highcharts-3d.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="highcharts.js"></script>
<script src="exporting.js"></script>
<script src="export-data.js"></script>


</body>
</html>

<?php 




//mis variables
 $impresion='';
 $procompletados='';
 $pronocompletados='';
 $por=0;
$items = array();
$tt = array();
   $st = $conn->prepare("SELECT GROUP_CONCAT(poa_cargos.Cargo SEPARATOR '/') AS Cargo, 
                                             poa_procesos.IdProceso,
                                             poa_procesos.nombre_proceso,
                                             
                                             poa_procesos.fecha_inicio,
                                             poa_procesos.fecha_final,
                                             poa_procesos.presupuesto,
                                             poa_procesos.indicador_cualitativo,
                                             poa_procesos.indicador_cuantitativo,
                                             poa_procesos.Estado
                                      FROM (((siif_EmpleadoFusalmo.poa_cargosasignacion poa_cargosasignacion
                                              INNER JOIN siif_EmpleadoFusalmo.poa_empleado poa_empleado
                                                 ON (poa_cargosasignacion.IdEmpleado = poa_empleado.IdEmpleado))
                                             INNER JOIN siif_EmpleadoFusalmo.poa_cargos poa_cargos
                                                ON (poa_cargosasignacion.IdCargo = poa_cargos.IdCargos))
                                            INNER JOIN
                                            siif_EmpleadoFusalmo.poa_asignacionprocesos poa_asignacionprocesos
                                               ON (poa_asignacionprocesos.IdEmpleado = poa_empleado.IdEmpleado))
                                           INNER JOIN siif_EmpleadoFusalmo.poa_procesos poa_procesos
                                              ON (poa_asignacionprocesos.IdProceso = poa_procesos.IdProceso)
                                              WHERE poa_procesos.IdPoa = :poa  
                                              GROUP BY  poa_procesos.IdProceso");                
                $st->bindParam(':poa',$idPoa, PDO::PARAM_STR);
                $st->execute();
                $rows = $st->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($rows as $row) {
                        $impresion='';
                        $impresion.="<tr>";
                        $impresion.='<td style="display: none;">'.$row['IdProceso'].'</td>';
                        $impresion.='<td>'.utf8_decode($row['nombre_proceso']).'</td>';
                        $impresion.='<td>'.utf8_decode($row['Cargo']).'</td>';
                        $impresion.='<td>'.$row['fecha_inicio'].'</td>';
                        $impresion.='<td>'.$row['fecha_final'].'</td>';
                         if (!empty($row["indicador_cuantitativo"])){
                           $impresion.='<td>'.$row['indicador_cuantitativo'].'</td>';
                        }else{
                           $impresion.='<td>'.utf8_decode($row['indicador_cualitativo']).'</td>';
                        }
                        
/////////////////////////////////////////////////////////////////
                       
                           $ab = $conn->prepare("SELECT COUNT(*) from poa_actividades Where IdProceso = :id");
                           $ab->bindParam(':id',$row['IdProceso'], PDO::PARAM_STR);
                           $ab->execute();
                           $registros2 = $ab->fetchColumn();
                         

                           $ac = $conn->prepare("SELECT * from poa_actividades Where IdProceso = :id");
                           $ac->bindParam(':id',$row['IdProceso'], PDO::PARAM_STR);
                           $ac->execute();
                           $columnas = $ac->fetchAll(PDO::FETCH_ASSOC);

                                  foreach ($columnas as $listos) {

                                       if ($listos["Estado"] == 0 ){
                                        $nocompletado2 ++;
                                         
                                  }else{
                                      $completado2 ++;

                                  }
                                }

                                
                                 
                                  
                        if ($registros2 != 0){
                              $porcentaje2 = 100/$registros2;
                              $t = $porcentaje2 * $completado2;
 //echo $row['nombre_proceso']. $t;

 $items[] = utf8_decode($row['nombre_proceso']);
 $tt[] = $t;



                              echo "<br>";

                              $avanceTotal2 = round($porcentaje2 * $completado2,0,PHP_ROUND_HALF_UP);
                              $por=$avanceTotal2;
                              if($avanceTotal2 <= 33){
                                $impresion.='<td style="color: #FF0000;">'.$avanceTotal2.'% Completados</td>';
                               }elseif ($avanceTotal2 >= 34 && $avanceTotal2 <= 66) {
                                $impresion.='<td style="color: #e49216;">'.$avanceTotal2.'% Completados</td>';
                               }elseif ($avanceTotal2 >= 67) {
                                $impresion.='<td style="color: green;">'.$avanceTotal2.'% Completados</td>';
                               }
                        }
                        $completado2 = 0;
                        $nocompletado2 = 0;
                       
                       ////////////////////////////////
                        $impresion.='<td><a href="?l=actividades&id='.$row['IdProceso'].'&poa='.$idPoa.'" title="Ver Actividades">
                        ver actividades</a>';
                        $impresion.='</tr>';
                        if($por==100){

                            $procompletados.=$impresion;
                        }else{
                            $pronocompletados.=$impresion;
                        }
                   

            

$row['nombre_proceso'];
$t1=$row['nombre_proceso'];

$i = 'name'; 

 } 
 ?>


    <script type="text/javascript">

Highcharts.chart('pastel', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Estadisticas'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Porcentaje de Avance',
        colorByPoint: true,
        data: [




<?php 
for ($i = 0; $i < count($items); $i++) {
    echo "{";
    print "name:" . "'$items[$i]', ";
    print "y:" . "$tt[$i] ,";
    echo " },";
}


 ?>



]
    }]
});
    </script>




