<head>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Información Institucional FUSALMO</title>
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="Empleados/POA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">


</head>
<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="10"><h2 style="color: #197198;">Plan Estrategico 2018 - 2022 -</h2><h4 style="color: #197198;">Linea 1 - Educacion Integral Liberadora</h4></td></tr>
       
        <tr><td colspan="10"><hr color='skyblue' /></td></tr>
        <tr style='display:  inline-block;'>

<table  style="width: 100%;  border-collapse: collapse;">
     <tr>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Objetivos Estrategicos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Indicadores</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Productos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Avances</th>
     </tr>
     </tr>
     <tr>
      <td rowspan="4"  style="border: 1px solid #dddddd; padding: 8px;"><b>OE1.1</b><br> 
Fortalecer capacidades y habilidades en la ni&ntilde;ez, adolescencia y juventud, basados en el carisma salesiano, para que construyan un proyecto de vida solidario, que se vea reflejado y apoyado por su familia, centro escolar y  otras instancias de la comunidad.
</td>
      <td rowspan="4" style="border: 1px solid #dddddd; padding: 8px;">Hasta diciembre del 2022;  20,000 ni&ntilde;os, ni&ntilde;as, adolescentes y  J&oacute;venes de los Centros Escolares son atendidos por  FUSALMO,  mejoran su nivel de  competencias educativas curriculares (en estudios sociales, educaci&oacute;n f&iacute;sica, lenguaje, ciencias salud y medio ambiente y tecnolog&iacute;a.), así mismo sus  habilidades y actitudes de acuerdo a las dimensiones Educativo cultural, Experiencia en Asociacionismo, Experiencia vocacional y Educaci&oacute;n en la fe.<br><br>

<i>LB: Nivel de ingreso de los j&oacute;venes al inicio del programa.<br><br>
FV: Valoraci&oacute;n del nivel de los j&oacute;venes cada a&ntilde;o.<br><br>
FV: Generar procesos de seguimiento para j&oacute;venes de noveno grado y bachillerato del PIJDB posterior a finalizar su proceso formativo. </i></td>
      <td  style="border: 1px solid #dddddd; padding: 8px;">1. Curr&iacute;cula de Educaci&oacute;n para la Paz institucionalizada.</td>
      <td  style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>

      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">2. Elaborada Curricula de Orientaci&oacute;n Educativa para parvulario, primer ciclo, segundo ciclo, tercer ciclo y bachillerato.</td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>
      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">3. Revisi&oacute;n y evaluaci&oacute;n del proceso evangelizaci&oacute;n con el &aacute;rea pastoral. </td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>
      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">4. Plan de convocatoria, formaci&oacute;n y acompa&ntilde;amiento de fraternidad.</td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>
    

    </tr>
     </table>
     <br>
     <br>
     <br>
     <table  style="width: 100%">
            
              <td ><a href="?l=Linea1">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Educacion Integral Liberadora" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              
              <td ><a href="?l=Linea2">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Gestion Socio Laboral" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea3">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Protagonismo, Participacion e Incidencia. " name="Enviar" class="botonG"  style="width: 236px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea4">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Tecnologia Educativa" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea5">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Fortalecimiento Organizacional" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              <td ><a href="?l=Planestrategico">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="General" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
             </tr>
               
             </tr>
        <tr>
            <td colspan="10"><hr color='skyblue' /></td>
        </tr>
    </table>
</div>

<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px">
  <table style="width: 100%;">
            <tr>
             <td style="margin-left: 10px;">
              
        </tr>
    </table>
</div>