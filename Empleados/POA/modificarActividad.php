<?php
//conexion a base de datos
  $db_host="localhost";
  $db_name="siif_EmpleadoFusalmo";
  $db_user="siif_padre";
  $db_pass="Fusalmo2016";

 
 
     try {
     $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
     $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }
  //Captura de error si no se puede conectar
  catch(PDOException $e)
  {
       echo "<script>alert('Error conectando a la base de datos');</script>";
      header('Location: index.php.php?r=0');
      
  }

  $idActividad = $_GET['id'];
  $idPoa = $_GET['poa'];
  $prc = $_GET['prc'];

   $ac = $conn->prepare("SELECT * from poa_actividades Where IdActividad = :id");
                         $ac->bindParam(':id',$idActividad, PDO::PARAM_STR);
                         $ac->execute();
                         $columnas = $ac->fetchAll(PDO::FETCH_ASSOC);

foreach ($columnas as $listos) {

  ?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Modificar Actividad</title>
<!-- Bootstrap core CSS-->
<link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Custom styles for this template-->
<link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">
    <script src="Empleados/POA/vendor/jquery/jquery.min.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


  <style type="text/css">
  #cabezera {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  padding: .75rem 1rem;
  margin-bottom: 1rem;
  list-style: none;
  background-color: #e9ecef;
  border-radius: .25rem;
  height: 20%;
}

#botoncete {
  background-color: #6aadd8;
}

</style>


<script type="text/javascript">
$(document).ready(function() {
$('#selectRecursos').select2();
}); 
/*
$(document).ready(function() {
$('#selectEmpleados').select2();
}); 
*/

$(document).ready(function() {
$('#selectEmpleados').select2();
}); 


  function myFunction() {
  document.getElementById("finicio").defaultValue = "<?php echo $listos['fecha_inicio'];   ?>";
   document.getElementById("ffinal").defaultValue = "<?php echo $listos['fecha_final'];   ?>";
   
}

window.onload = myFunction;

</script>
</head>

<body class="fixed-nav sticky-footer bg-light" id="page-top">
<!-- Navigation-->

<div class="content-wrapper">
   

  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.html">Actividad</a>
      </li>
      <li class="breadcrumb-item active">Registro de Actividad</li>
    </ol>


    <div class="row">
<div class="container">
<?php
echo '<a class="btn btn-primary btn-primary" id="botoncete" href="javascript:history.back(-1);">Volver</a>';
echo "<br>";
?>
  <div class="card card-register mx-auto mt-5">
    <div class="card-header">Registrar una actividad</div>
    <div class="card-body">
      <form enctype="multipart/form-data" action="?l=updateActividades" method="post">
         <?php
                echo '<input style="display: none;" name="idActividad" value="'.$idActividad.'"/>';
                echo '<input style="display: none;" name="idPoa" value="'.$idPoa.'"/>';
                 echo '<input style="display: none;" name="prc" value="'.$prc.'"/>';
              ?>
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
              <label for="exampleInputName">Nombre de Actividad</label>
             
              <input class="form-control" name="inputNombre" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Ingrese nombre de Actividad" value="<?php echo utf8_decode($listos['nombre_actividad']); ?>">
            </div>
            <div class="col-md-6">
              <label for="exampleInputLastName">Beneficiarios</label>
              <input class="form-control" name="inputBeneficiarios" id="exampleInputLastName" type="number" min="1" aria-describedby="nameHelp" placeholder="Beneficiarios"
              value="<?php echo $listos['beneficiarios']; ?>">
            </div>
          </div>
        </div>


          <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">

                  <label for="exampleConfirmPassword">Fecha Inicio</label>
                  <div class="input-group date">
                   <!-- <input class="form-control" name="fechaFinal" type="date">-->
                   <input type='date' name="fechaInicio" id="finicio" class="form-control" autocomplete="off" required />
                   <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"/>
                   </div>
                 </div>
                 <div class="col-md-6">

                  <label for="exampleConfirmPassword">Fecha Final</label>
                  <div class="input-group date">
                   <!-- <input class="form-control" name="fechaFinal" type="date">-->
                   <input type='date' name="fechaFinal" id="ffinal" class="form-control" autocomplete="off" required />
                   <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"/>
                   </div>
                 </div>
               </div>
             </div>
<?php

 }
?>
      
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">


            <label for="exampleInputEmail1">Presupuesto</label>
            <div class="input-group"> 
      <span class="input-group-addon">$</span>
      <input type="number" name="inputPresupuesto" value="1000" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="c2" value="<?php echo utf8_decode($listos['presupuesto']); ?>"/>
  </div>
            </div>

            <div class="col-md-6">
              <label for="exampleInputPassword1">Recursos</label>
              <input class="form-control" name="inputRecursos" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Ingrese los recursos a utilizar" value="<?php echo utf8_decode($listos['Recursos']); ?>">

            </div>

          </div>
        </div> <!-- aqui -->

         <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
              <label for="exampleInputPassword1">Producto</label>
              <input class="form-control"  name="inputProducto" type="text" value="<?php echo utf8_decode($listos['producto']); ?>">
            </div>
            <div class="col-md-6">
              <label for="exampleConfirmPassword">Resultado</label>
              <input class="form-control" name="inputResultado" type="text" value="<?php echo utf8_decode($listos['resultado']); ?>">
            </div>
          </div>
        </div>


         <div class="form-group">
          <div class="form-row">
              <div class="col-md-12">
                       <label for="exampleInputPassword1">Asignacion de Empleados</label>
                <select class="form-control" id="selectEmpleados" name="empleados[]" multiple="multiple">
               
            <?php 
                $stm = $conn->prepare("SELECT  IdEmpleado,Nombre1,Apellido1 FROM poa_empleado");
                $stm->execute();
                $rows = $stm->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($rows as $row) {
                      ?>
                       <option value="<?php echo $row['IdEmpleado']; ?>"><?php echo utf8_decode($row['Nombre1']). ' ' . utf8_decode($row['Apellido1']); ?></option>  
                       <?php
                        }

                      ?>

             </select> 
             </div>
             </div>
        </div>

         <script type="text/javascript">
          $(function () {
            $('#fechaInicio').datetimepicker({
              language: 'es',
              minDate: new Date()
            });
            $('#fechaFinal').datetimepicker({
              language: 'es',
              minDate: new Date()
            });

          });
        </script>
     
        <!-- hasta aqui -->
         <input type="submit" class="btn btn-primary btn-block" />
      </form>
     
    </div>
  </div>
</div>
    </div>
  </div>
  <!-- /.container-fluid-->
  <!-- /.content-wrapper-->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
  </a>
  <!-- Logout Modal-->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="Empleados/POA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="Empleados/POA/vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="Empleados/POA/js/sb-admin.min.js"></script>
   <script src="Empleados/POA/js/pestañas.js"></script>
</div>
</body>

</html>
