<?php
//conexion a base de datos
  $db_host="localhost";
  $db_name="siif_EmpleadoFusalmo";
  $db_user="siif_padre";
  $db_pass="Fusalmo2016";

   
   
       try {
       $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
       $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    //Captura de error si no se puede conectar
    catch(PDOException $e)
    {
         echo "<script>alert('Error conectando a la base de datos');</script>";
        header('Location: index.php.php?r=0');
        
    }

    $idProceso = $_GET['id'];
    $idPoa = $_GET['poa'];


    ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Agregar Actividad</title>
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">
      <script src="Empleados/POA/vendor/jquery/jquery.min.js"></script>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <style type="text/css">
    #cabezera {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    padding: .75rem 1rem;
    margin-bottom: 1rem;
    list-style: none;
    background-color: #e9ecef;
    border-radius: .25rem;
    height: 20%;
}

  #botoncete {
    background-color: #6aadd8;
  }

  </style>


<script type="text/javascript">
$(document).ready(function() {
$('#selectRecursos').select2();
}); 

$(document).ready(function() {
$('#selectEmpleados').select2();
}); 


</script>
</head>

<body class="fixed-nav sticky-footer bg-light" id="page-top">
  <!-- Navigation-->

  <div class="content-wrapper">
     

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Actividad</a>
        </li>
        <li class="breadcrumb-item active">Registro de Actividad</li>
      </ol>


       <div id="cabezera">
                
        
       <?php
          
          $st = $conn->prepare("SELECT       poa_procesos.IdProceso,
                                             poa_procesos.nombre_proceso,
                                             poa_cargos.Cargo,
                                             poa_procesos.fecha_inicio,
                                             poa_procesos.fecha_final,
                                             poa_procesos.presupuesto,
                                             poa_procesos.indicador_cualitativo,
                                             poa_procesos.indicador_cuantitativo
                                      FROM (((siif_EmpleadoFusalmo.poa_cargosasignacion poa_cargosasignacion
                                              INNER JOIN siif_EmpleadoFusalmo.poa_empleado poa_empleado
                                                 ON (poa_cargosasignacion.IdEmpleado = poa_empleado.IdEmpleado))
                                             INNER JOIN siif_EmpleadoFusalmo.poa_cargos poa_cargos
                                                ON (poa_cargosasignacion.IdCargo = poa_cargos.IdCargos))
                                            INNER JOIN
                                            siif_EmpleadoFusalmo.poa_asignacionprocesos poa_asignacionprocesos
                                               ON (poa_asignacionprocesos.IdEmpleado = poa_empleado.IdEmpleado))
                                           INNER JOIN siif_EmpleadoFusalmo.poa_procesos poa_procesos
                                              ON (poa_asignacionprocesos.IdProceso = poa_procesos.IdProceso)
                                              Where poa_procesos.IdProceso = :id");
          $st->bindParam(':id',$idProceso, PDO::PARAM_STR);
          $st->execute();
          ?>

          <ul>

          <?php

          $result = $st -> fetch();

              echo "<li><b>Nombre de Proceso: </b>".utf8_decode($result['nombre_proceso'])."</li>";
              echo "<li><b>Asignado: </b>".utf8_decode($result['Cargo'])."</li>";
              echo "<li><b>Fecha Inicio: </b>".$result['fecha_inicio']."</li>";
              echo "<li><b>Fecha de Finzalizacion: </b>".$result['fecha_final']."</li>";

             
          
          
        ?>
       </ul>
      </div>


      <div class="row">
<div class="container">
 <?php
echo '<a class="btn btn-primary btn-primary" id="botoncete" href="javascript:history.back(-1);">Volver</a>';
echo "<br>";
?>
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Registrar una actividad</div>
      <div class="card-body">
        <form enctype="multipart/form-data" action="?l=registroActividad" method="post">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">Nombre de Actividad</label>
                <?php
                  echo '<input style="display: none;" name="idProceso" value="'.$idProceso.'"/>';
                  echo '<input style="display: none;" name="idPoa" value="'.$idPoa.'"/>';
                ?>
                <input class="form-control" name="inputNombre" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Ingrese nombre de Actividad">
              </div>
              <div class="col-md-6">
                <label for="exampleInputLastName">Beneficiarios</label>
                <input class="form-control" name="inputBeneficiarios" id="exampleInputLastName" type="number" min="1" aria-describedby="nameHelp" placeholder="Beneficiarios">
              </div>
            </div>
          </div>

           

           <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPassword1">Fecha Inicio</label>
                <input class="form-control" name="fechaInicio" type="date" required>
              </div>
              <div class="col-md-6">
                <label for="exampleConfirmPassword">Fecha Final</label>
                <input class="form-control" name="fechaFinal" type="date" required>
              </div>
            </div>
          </div>

        
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">


              <label for="exampleInputEmail1">Presupuesto</label>
              <div class="input-group"> 
        <span class="input-group-addon">$</span>
        <input type="number" name="inputPresupuesto" value="1000" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="c2" />
    </div>
              </div>

              <div class="col-md-6">
                <label for="exampleInputPassword1">Recursos</label>
                <input class="form-control" name="inputRecursos" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Ingrese los recursos a utilizar">

              </div>


             
            </div>
          </div> <!-- aqui -->



           <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPassword1">Producto</label>
                <input class="form-control"  name="inputProducto" type="text">
              </div>
              <div class="col-md-6">
                <label for="exampleConfirmPassword">Resultado</label>
                <input class="form-control" name="inputResultado" type="text">
              </div>
            </div>
          </div>


           <div class="form-group">
           <div class="form-row">
              <div class="col-md-12">
                       <label for="exampleInputPassword1">Asignacion de Empleados</label>
                <select class="form-control" id="selectEmpleados" name="empleados[]" multiple="multiple">
               
<?php 
                $stm = $conn->prepare("SELECT  IdEmpleado,Nombre1,Apellido1 FROM poa_empleado");
                $stm->execute();
                $rows = $stm->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($rows as $row) {
                      ?>
                       <option value="<?php echo $row['IdEmpleado']; ?>"><?php echo utf8_decode($row['Nombre1']). ' ' . utf8_decode($row['Apellido1']); ?></option>  
                       <?php
                        }

                      ?>

             </select> 
             </div>
             </div>
          </div>


          

          
         

       
          <!-- hasta aqui -->
           <input type="submit" class="btn btn-primary btn-block" />
        </form>
       
      </div>
    </div>
  </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
  
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="Empleados/POA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="Empleados/POA/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="Empleados/POA/js/sb-admin.min.js"></script>
     <script src="Empleados/POA/js/pestañas.js"></script>
  </div>
</body>

</html>
