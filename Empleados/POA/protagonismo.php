<head>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Información Institucional FUSALMO</title>
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="Empleados/POA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">


</head>

<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="10"><h2 style="color: #197198;">Plan Estrategico 2018 - 2022</h2></td></tr>
        <tr><td colspan="10"><h4 style="color: #197198;">Linea 3 - Protagonismo, Participacion e Incidencia</h4></td></tr>
        <tr><td colspan="10"><hr color='skyblue' /></td></tr>
        <tr style='display:  inline-block;'>

<table  style="width: 100%;  border-collapse: collapse;">
     <tr>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Objetivos Estrategicos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Indicadores</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Productos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Avances</th>
     </tr>
     </tr>
     <tr>
      <td rowspan="7"  style="border: 1px solid #dddddd; padding: 8px;"><b>OE3:</b><br> J&oacute;venes organizados con capacidades desarrolladas inciden en sus comunidades de vida y educativas
</td>
      <td rowspan="7" style="border: 1px solid #dddddd; padding: 8px;">Hasta diciembre 2022, 10,000 ni&ntilde;ez, adolescencia y juventud atendida (2,000 por a&ntilde;o de lo cual 60%  Soyapango, 30% Santa Ana, 10% San Miguel), se han organizado y desarrollado sus capacidades de liderazgo, voluntariado, art&iacute;sticas, deportivas y en valores cristianos y humanos. <br><br>
<i>LB:<br> 
&#8226;N&uacute;mero de j&oacute;venes que han desarrollado sus capacidades de liderazgo, voluntariado, art&iacute;sticas, deportivas y en valores.<br>
&#8226;N&uacute;mero de iniciativas juveniles que inciden en sus comunidades de vida y educativas.<br><br>
 
FV:<br> L&iacute;nea base en SIIF e informes de monitoreo.</i>

</td>
      <td  style="border: 1px solid #dddddd; padding: 8px;">1. Red juvenil nacional de FUSALMO.</td>
      <td  style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>

      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">2. Propuestas juveniles que inciden en la implementaci&oacute;n de pol&iacute;ticas y programas p&uacute;blicos. </td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>
      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">3. Formaci&oacute;n en valores humanos, cristianos, deportivos  y formaci&oacute;n sacramental. </td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>

      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">4. Proceso formativo para el discernimiento y opci&oacute;n vocacional.</td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>
    
    <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">5. Escuela de artes certificada con enfoque de paz y estilo salesiano </td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>

<tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">6. Escuelas deportivas con enfoque de juego limpio e inclusi&oacute;n </td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>

      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">7. Promoci&oacute;n de talento deportivo</td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>

    </tr>
     </table>
     <br>
     <br>
     <br>
     <table  style="width: 100%">
            
              <td ><a href="?l=Linea1">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Educacion Integral Liberadora" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              
              <td ><a href="?l=Linea2">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Gestion Socio Laboral" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea3">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Protagonismo, Participacion e Incidencia. " name="Enviar" class="botonG"  style="width: 236px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea4">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Tecnologia Educativa" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea5">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Fortalecimiento Organizacional" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              <td ><a href="?l=Planestrategico">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="General" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
             </tr>
               
             </tr>
        <tr>
            <td colspan="10"><hr color='skyblue' /></td>
        </tr>
    </table>
</div>

<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px">
  <table style="width: 100%;">
            <tr>
             <td style="margin-left: 10px;">
              
        </tr>
    </table>
</div>