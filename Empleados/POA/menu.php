<style type="text/css">
  .mainNav{
    background: white;
  }
.icono {
  width: 100px;
  height: 100px;
}

.btn-awesome {
    color: #fff;
    background-color: #6aadd8;
    border-color: #6aadd8;
    border-radius: 0.0;
}

.btn-awesome:hover {
    color: black;
    background-color: #b4c7d2;
    border-color: #17a2b8;
}

.btn-EvenMoreAwesome {
    color: #fff;
    background-color: #00859a;
    border-color: #00859a;
}

.btn-EvenMoreAwesome:hover {
    color: black;
    background-color: #b4c7d2;
    border-color: #17a2b8;
}



</style>

<nav style="width: 15%; height: 20%;  " class="navbar navbar-expand-lg navbar-dark bg-dark2 fixed-top" id="mainNav">
<div>
<img src="Imagenes/logo_new.png" style="width: 70%; height: 90%; border-radius: 5px; margin-left: 10px;" >
<div>

    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">


        <div class="dropdown">
    <button class="btn btn-awesome dropdown-toggle" id="menu1" style="margin-left: 5%; width: 95%;" type="button" data-toggle="dropdown">POA
    <span class="caret" style="margin-left: 80px;"></span></button>
    <ul class="dropdown-menu" style="background-color: #00859a; padding-left: 5%;" role="menu" aria-labelledby="menu1">
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Estadisticas</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Calendario</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="index.php">Procesos</a></li>
      
    </ul>
  </div>

    <div class="dropdown">
    <button class="btn btn-awesome dropdown-toggle" style="margin-left: 5%; width: 95%;" id="menu1" type="button" data-toggle="dropdown">Mi Area
    <span class="caret" style="margin-left: 55px;"></span></button>
    <ul class="dropdown-menu" style="background-color: #00859a;" role="menu" aria-labelledby="menu1">
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Mis Compañeros</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Requisiciones</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Acciones Personales</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Notificaciones / Permisos</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Resumen de Proyecto</a></li>
      
    </ul>
  </div>

   <div class="dropdown">
    <button class="btn btn-awesome dropdown-toggle" style="margin-left: 5%; width: 95%;" id="menu1" type="button" data-toggle="dropdown">Mis datos
    <span class="caret" style="margin-left: 40px;"></span></button>
    <ul class="dropdown-menu" style="background-color: #00859a;" role="menu" aria-labelledby="menu1">
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Datos Personales</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Datos Familiares</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Datos Academicos</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Datos Laborales</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Datos de Salud</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Datos Religiosos</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Perfil de Empleado</a></li>
      
    </ul>
  </div>



<div class="dropdown">
    <button class="btn btn-awesome dropdown-toggle" style="margin-left: 5%; width: 95%;" id="menu1" type="button" data-toggle="dropdown">Evaluaciones
    <span class="caret" style="margin-left: 20px;"></span></button>
    <ul class="dropdown-menu" style="background-color: #00859a;" role="menu" aria-labelledby="menu1">
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Mis Evaluaciones</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Solicitudes de Evaluacion</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Resumen Personal</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Resultados de Areas</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Cuestionario</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Resultados DNC</a></li>
      
    </ul>
  </div>


  <div class="dropdown" >
    <button class="btn btn-awesome dropdown-toggle" style="margin-left: 5%; width: 95%;" id="menu1" type="button" data-toggle="dropdown">Administracion
    <span class="caret" style="margin-left: 5px;"></span></button>
    <ul class="dropdown-menu" style="background-color: #00859a;" role="menu" aria-labelledby="menu1">
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Áreas</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Gestion De Evaluaciones</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Acciones</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Permisos</a></li>
      <li role="presentation"><a class="btn btn-EvenMoreAwesome" style="color: white;" role="menuitem" tabindex="-1" href="#">Buscar Empleado</a></li>
     
      
    </ul>
  </div>

  <div class="dropdown">
<button class="btn btn-awesome" style="width: 95%; margin-left: 5%;" id="menu1" type="button" data-toggle="dropdown">Mis Documentos
</button>
</div>
<div class="dropdown">
<button class="btn btn-awesome " style="width: 95%; margin-left: 5%;" id="menu1" type="button" data-toggle="dropdown">Mis Actividades</span></button>
</div>



       
       
       
      </ul>
      
    </div>
  </nav>