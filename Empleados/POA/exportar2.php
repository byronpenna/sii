<?php

 $salida = '';
 $salida_NombreActividad = '';
 $salidaProductos = '';
 $salidaResultado = '';
 $salidaRecursos = '';
 $salidaMetodos = '';
 $titulo = '';

 $completado = 0;
 $nocompletado = 0;
 $completado2 = 0;
 $nocompletado2 = 0;
 $completado3 = 0;
 $nocompletado3 = 0;
 $idPoa = $_GET['poa'];
 $comovamo = $_GET['avance'];

  $db_host="localhost";
  $db_name="siif_EmpleadoFusalmo";
  $db_user="siif_padre";
  $db_pass="Fusalmo2016";

   
   
       try {
       $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
       $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    //Captura de error si no se puede conectar
    catch(PDOException $e)
    {
         echo "<script>alert('Error conectando a la base de datos');</script>";
        header('Location: index.php.php?r=0');
        
    }                
           

if (PHP_SAPI == 'cli')
	die('sólo se puede ejecutar desde un navegador Web');

/** Incluye PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
require_once dirname(__FILE__) . '/Classes/PHPExcel/Cell/AdvancedValueBinder.php'; PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Fusalmo")
							 ->setLastModifiedBy("Fusalmo")
							 ->setTitle("Office 2010 XLSX Fusalmo")
							 ->setSubject("Office 2010 XLSX Fusalmo")
							 ->setDescription("Documento de Procesos.")
							 ->setKeywords("office 2010 php")
							 ->setCategory("Archivo con resultado de Procesos");



// Combino las celdas desde A1 hasta E1
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');

  $pl = $conn->prepare("SELECT Nombre FROM poa_poa WHERE Id = :id");
                 $pl->bindParam(':id',$idPoa, PDO::PARAM_STR);
                 $pl->execute();
                 $titulo = $pl->fetchColumn();

                 

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Plan Operativo linea de acción '.$titulo)
            ->setCellValue('A2', 'TIPO DE PROCESO')
            ->setCellValue('B2', 'RESULTADO')
            ->setCellValue('C2', 'INDICADOR')
			->setCellValue('D2', 'PRODUCTO')
			->setCellValue('E2', 'ACTIVIDADES')
			->setCellValue('F2', 'MEDIO DE VERIFICACION')
			->setCellValue('G2', 'TIEMPO DE CUMPLIMIENTO')
			->setCellValue('H2', 'RESPONSABLE')
			->setCellValue('I2', '% AVANCE');
			
// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

$objPHPExcel->getActiveSheet()->getStyle('A1:I2')->applyFromArray($boldArray);		

	
			
//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);	
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);	
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);	
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);	
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);	
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
	

/*Extraer datos de MYSQL*/
	# conectare la base de datos
    $con=@mysqli_connect('localhost', 'siif_padre', 'Fusalmo2016', 'siif_EmpleadoFusalmo');
    if(!$con){
        die("imposible conectarse: ".mysqli_error($con));
    }
    if (@mysqli_connect_errno()) {
        die("Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
    }

    mysqli_set_charset($con,"utf8");

	$sql='SELECT IdProceso,nombre_proceso,indicador_cuantitativo,indicador_cualitativo,fecha_inicio,fecha_final FROM poa_procesos WHERE IdPoa='.$idPoa;
	$query=mysqli_query($con,$sql);
	$cel=3;//Numero de fila donde empezara a crear  el reporte



	while ($row=mysqli_fetch_array($query)){

		$procesoID=$row['IdProceso'];
		$nombreProceso=$row['nombre_proceso'];

		if(!empty($row['indicador_cuantitativo'])){
			$indicador=$row['indicador_cuantitativo'];
		}else{
			$indicador=$row['indicador_cualitativo'];
		}



		
		$periodo=$row['fecha_inicio'] .' - ' . $row['fecha_final'];

		 $cn = $conn->prepare("SELECT COUNT(*) from poa_actividades Where IdProceso = :id");
                 $cn->bindParam(':id',$procesoID, PDO::PARAM_STR);
                 $cn->execute();
                 $registros = $cn->fetchColumn();
                 
                


                 $br = $conn->prepare("SELECT * from poa_actividades Where IdProceso = :id");
                 $br->bindParam(':id',$procesoID, PDO::PARAM_STR);
                 $br->execute();
                 $rows = $br->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($rows as $rowx) {

                             if ($rowx["Estado"] == 0 ){
                              $nocompletado +=1;
                        }else{
                            $completado +=1;
                            
                               
                            
                        }
                      
                      }

                      
                    
                        if ($registros != 0){
                              $porcentaje = 100/$registros;
                              $avanceTotal = round($porcentaje * $completado,0,PHP_ROUND_HALF_UP);
                        }else {
                          $avanceTotal = 0;
                        }



           $sql2='SELECT poa_procesos.IdProceso, poa_cargos.IdCargos, poa_cargos.Cargo
					FROM (((siif_EmpleadoFusalmo.poa_asignacionprocesos poa_asignacionprocesos
					        INNER JOIN siif_EmpleadoFusalmo.poa_empleado poa_empleado
					           ON (poa_asignacionprocesos.IdEmpleado = poa_empleado.IdEmpleado))
					       INNER JOIN siif_EmpleadoFusalmo.poa_procesos poa_procesos
					          ON (poa_asignacionprocesos.IdProceso = poa_procesos.IdProceso))
					      INNER JOIN
					      siif_EmpleadoFusalmo.poa_cargosasignacion poa_cargosasignacion
					         ON (poa_cargosasignacion.IdEmpleado = poa_empleado.IdEmpleado))
					     INNER JOIN siif_EmpleadoFusalmo.poa_cargos poa_cargos
					        ON (poa_cargosasignacion.IdCargo = poa_cargos.IdCargos)
					       WHERE poa_procesos.IdProceso = '.$procesoID.';';
	$query2=mysqli_query($con,$sql2);
	
	while ($row2=mysqli_fetch_array($query2)){
		$salida .= $row2['Cargo'].' / ';
	}

	$sql3='SELECT IdActividad,producto,resultado,nombre_actividad,recursos 
					FROM poa_actividades
					WHERE IdProceso = '.$procesoID.';';
	$query3=mysqli_query($con,$sql3);
	
	while ($row3=mysqli_fetch_array($query3)){

		$sql4='SELECT nombre_metodo 
					FROM poa_metodos
					WHERE IdActividad = '.$row3['IdActividad'].';';
	    $query4=mysqli_query($con,$sql4);

	    while ($row5=mysqli_fetch_array($query4)){
            $salidaMetodos .= $row5['nombre_metodo']."\n";
	    }


		$salidaProductos .= $row3['producto']."\n";
		$salidaResultado .=  $row3['resultado']."\n";
	    $salida_NombreActividad .= $row3['nombre_actividad']."\n";
			
	}



		
		
			$a="A".$cel;
			$b="B".$cel;
			$c="C".$cel;
			$d="D".$cel;
			$e="E".$cel;			
			$f="F".$cel;
			$g="G".$cel;
			$h="H".$cel;
			$i="I".$cel;
			
			// Agregar datos
			$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($a, $nombreProceso)  //ya esta
            ->setCellValue($b, $salidaResultado)  //ya esta        
            ->setCellValue($c, $indicador) //ya esta
            ->setCellValue($d, $salidaProductos) //ya esta
            ->setCellValue($e, $salida_NombreActividad) //ya esta
            ->setCellValue($f, $salidaMetodos)
            ->setCellValue($g, $periodo)
            ->setCellValue($h, $salida) //ya esta
            ->setCellValue($i, $avanceTotal.'% Completado'); //ya esta
			
	$cel+=1;
	$salidaMetodos= '';
	$salidaResultado= '';
	$salidaProductos='';
	$salida_NombreActividad='';
	$avanceTotal = 0;
	 $salida= '';
	 $porcentaje = 0;
	 $completado = 0;
	 $nocompletado = 0;
	}

/*Fin extracion de datos MYSQL*/
$rango="A2:$i";
$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
);
$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
// Cambiar el nombre de hoja de cálculo
$objPHPExcel->getActiveSheet()->setTitle('Nombre de proceso');
$objPHPExcel->getActiveSheet()->getStyle('A3:K999')->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('A3:K999')->getAlignment()->setHorizontal(true);
$objPHPExcel->getActiveSheet()->getStyle('A3:K999')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:K999')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);




// Establecer índice de hoja activa a la primera hoja , por lo que Excel abre esto como la primera hoja
$objPHPExcel->setActiveSheetIndex(0);


// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="reporte.xls"');
header('Cache-Control: max-age=0');
// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;