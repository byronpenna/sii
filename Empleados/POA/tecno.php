<head>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Información Institucional FUSALMO</title>
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="Empleados/POA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">


</head>
<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="10"><h2 style="color: #197198;">Plan Estrategico 2018 - 2022</h2></td></tr>
        <tr><td colspan="10"><h4 style="color: #197198;">Linea 4 - Tecnologia Educativa </h4></td></tr>
        <tr><td colspan="10"><hr color='skyblue' /></td></tr>
        <tr style='display:  inline-block;'>

<table  style="width: 100%;  border-collapse: collapse;">
     <tr>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Objetivos Estrategicos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Indicadores</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Productos</th>
       <th colspan="1" style="border: 1px solid #dddddd; padding: 8px; width: 25%;">Avances</th>
     </tr>
     </tr>
     <tr>
      <td rowspan="5"  style="border: 1px solid #dddddd; padding: 8px;"><b>OE4:</b><br> Desarrollar  capacidades creativas de  l&oacute;gica e inventiva en la ni&ntilde;ez, adolescencia y juventud as&iacute; como facilitar la  mejora de sus competencias a trav&eacute;s de la formaci&oacute;n tecnol&oacute;gica influyendo en su opci&oacute;n vocacional.
</td>
      <td rowspan="5" style="border: 1px solid #dddddd; padding: 8px;"><b>IRD</b> 1.1 Hasta diciembre 2022,  8,000 la ni&ntilde;ez, adolescencia y juventud mejoran sus competencias en el uso de tecnolog&iacute;a para construir una opci&oacute;n vocacional tecnol&oacute;gica.<br><br>
<i><b>LB:</b><br> Capacidades y habilidades del j&oacute;ven al ingresar al programa.<br>
<b>FV:</b><br> Valoraci&oacute;n del nivel alcanzado por los j&oacute;venes cada a&ntilde;o seg&uacute;n la herramienta de LB.</i>


</td>
      <td  style="border: 1px solid #dddddd; padding: 8px;">1.  Curricula de tecnolog&iacute;a educativa integrada y  con los enfoques institucionales.</td>
      <td  style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>

      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">2. Plataformas tecnol&oacute;gicas educativas y administrativas.</td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #b01212;">
    <span style="background-color: #b01212;">0% completado</span>
  </div>
</div></td>
      </tr>
      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">3. Clubes tecnol&oacute;gicos </td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 86%; background-color: #8ec50b;">
    <span style="background-color: #8ec50b;">86% completado</span>
  </div>
</div>
<br><a href="https://siifusalmo.org/Empleado.php?l=actividades&id=121&poa=32" target="_blank">Visualizar</a></td>
      </tr>

      <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">4. Aulas Innovadoras.</td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 43%; background-color: #c7c360;">
    <span style="background-color: #c7c360;">40% completado</span>
  </div>
</div><br><a href="https://siifusalmo.org/Empleado.php?l=actividades&id=124&poa=32" target="_blank">Visualizar</a></td></td>
      </tr>
    
    <tr>
        <td style="border: 1px solid #dddddd; padding: 8px;">5. Emprendimientos tecnol&oacute;gico con  equidad g&eacute;nero. </td>
        <td style="border: 1px solid #dddddd; padding: 8px;"><div class="progress progress-striped active" >
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 35%; background-color: #b01212;">
    <span style="background-color: #b01212;">33% completado</span>
  </div>
</div><br><a href="https://siifusalmo.org/Empleado.php?l=actividades&id=122&poa=32" target="_blank">Visualizar</a></td>
      </tr>



    </tr>
     </table>
     <br>
     <br>
     <br>
     <table  style="width: 100%">
            
              <td ><a href="?l=Linea1">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Educacion Integral Liberadora" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              
              <td ><a href="?l=Linea2">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Gestion Socio Laboral" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea3">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Protagonismo, Participacion e Incidencia. " name="Enviar" class="botonG"  style="width: 236px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea4">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Tecnologia Educativa" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>

               <td ><a href="?l=Linea5">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="Fortalecimiento Organizacional" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
              <td ><a href="?l=Planestrategico">
              <?php if( $_SESSION["TipoUsuario"] == "Administrador"){?>
               <input type="submit" value="General" name="Enviar" class="botonG"  style="width: 206px;" /></a></td>
               <?php }?>
             </tr>
               
             </tr>
        <tr>
            <td colspan="10"><hr color='skyblue' /></td>
        </tr>
    </table>
</div>

<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px">
  <table style="width: 100%;">
            <tr>
             <td style="margin-left: 10px;">
              
        </tr>
    </table>
</div>