<?php

/**
 * @author José Manuel Calderón
 * @copyright 2014
 */
 
$charge = $_POST['charge'];
      
$Accion = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado
                          FROM Empleado AS e
                          INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = e.IdEmpleado
                          INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
                          INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
                          where ac.IdAsignacion = $charge");                                            
$Accion->execute();
$Datos = $Accion->fetch();    

if($_SESSION["Localidad"] == "Multigimnasio")   $localidad = "Multigimnasio Don Bosco";
if($_SESSION["Localidad"] == "San Miguel")      $localidad = "Polideportivo Don Bosco San Miguel";
if($_SESSION["Localidad"] == "Santa Ana")       $localidad = "Polideportivo Don Bosco Santa Ana";
if($_SESSION["Localidad"] == "Soyapango")       $localidad = "Polideportivo Don Bosco Soyapango";
 

?>


<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px;">  
<form action="Empleados/ProcesosArea/PermisoABM.php" method="post">
<input type="hidden" name="ide" value="<?php echo $charge?>" />
<table style="width: 100%;">
    <tr><td><h2 style="color: #1D7399;">Penalización a Empleado</h2></td></tr>
    <tr><td colspan="3"><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td>
            <table style="width: 80%;">
                <input type="hidden" name="NombreEmpleado" value="<?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?>" />
                <tr><td  style="width: 30%;">Nombre del Empleado: </td><td><?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?></td></tr>
                <tr><td >Cargo Asignado:               </td><td><?php echo "$Datos[5]"?></td></tr>
                <tr><td >Area de trabajo:                </td><td><?php echo "$Datos[6]"?></td></tr>
                <tr><td></td><td><?php echo $localidad?></td></tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="3"><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td>        
            <table style="width: 100%;">
                <tr><td colspan="3"><strong>Detalle del Penalización</strong></td></tr>
                <tr><td>Fecha de Inicio:</td>
                    <td>
                        <table>
                        <tr>
                            <td>
                                <select id="AnioI" name='AnioI'>
                                <?php 
                                    for($i = date('Y')+1; $i >= 2001; $i--)
                                    {
                                        if( $i == date('Y'))
                                            echo "          <option selected='true' value='$i'>$i</option>";
                                        else
                                            echo "          <option value='$i'>$i</option>";                                                
                                    }
                                ?>
                                </select>
                            </td>
                            <td>
                                <select id="MesI" name='MesI' >   
                                    <option <?php if(date('m')=="01")echo "selected='true'";?> value='01'>Enero</option>                             
                                    <option <?php if(date('m')=="02")echo "selected='true'";?> value='02'>Febrero</option>
                                    <option <?php if(date('m')=="03")echo "selected='true'";?> value='03'>Marzo</option>
                                    <option <?php if(date('m')=="04")echo "selected='true'";?> value='04'>Abril</option>
                                    <option <?php if(date('m')=="05")echo "selected='true'";?> value='05'>Mayo</option>
                                    <option <?php if(date('m')=="06")echo "selected='true'";?> value='06'>Junio</option>
                                    <option <?php if(date('m')=="07")echo "selected='true'";?> value='07'>Julio</option>
                                    <option <?php if(date('m')=="08")echo "selected='true'";?> value='08'>Agosto</option>
                                    <option <?php if(date('m')=="09")echo "selected='true'";?> value='09'>Septiembre</option>
                                    <option <?php if(date('m')=="10")echo "selected='true'";?> value='10'>Octubre</option>
                                    <option <?php if(date('m')=="11")echo "selected='true'";?> value='11'>Noviembre</option>
                                    <option <?php if(date('m')=="12")echo "selected='true'";?> value='12'>Diciembre</option>
                                </select>
                            </td> 
                            <td>
                                <select id="DiaI" name='DiaI' > 
                                    <?php
                                        for($i = 1; $i <= 31; $i++)
                                        {
                                            if($i==date('d'))
                                                echo "<option selected='true' value='$i'>$i</option>"; 
                                            else
                                                echo "<option value='$i'>$i</option>";                                               
                                        }
                                    ?>                                  
                                </select>
                            </td> 
                            </tr>
                        </table>
                    </td>
                    <td >Hora Desde:</td>
                    <td><input style="width: 40px;" type="number" id="HI" name="HI" value="00" min="00" max="12" required="true" title="Hora de Inicial" />:
                        <input style="width: 40px;" type="number" id="MI" name="MI" value="00" min="00" max="59" required="true" title="Minutos de Inicial"/>
                        <select name="TI" id="TI">
                                <option value="AM">AM</option>
                                <option value="PM">PM</option>
                        </select>
                    </td>   
                    </tr>
                    <tr>
                    <td>Fecha de Finalización:</td>
                    <td>
                        <table>
                        <tr>
                            <td>
                                <select id="AnioF" name='AnioF'>
                                <?php 
                                    for($i = date('Y')+ 1; $i >= 2001; $i--)
                                    {
                                        if( $i == date('Y'))
                                            echo "          <option selected='true' value='$i'>$i</option>";
                                        else
                                            echo "          <option value='$i'>$i</option>";                                                
                                    }
                                ?>
                                </select>
                            </td>
                            <td>
                                <select id="MesF" name='MesF' >   
                                    <option <?php if(date('m')=="01")echo "selected='true'";?> value='01'>Enero</option>                             
                                    <option <?php if(date('m')=="02")echo "selected='true'";?> value='02'>Febrero</option>
                                    <option <?php if(date('m')=="03")echo "selected='true'";?> value='03'>Marzo</option>
                                    <option <?php if(date('m')=="04")echo "selected='true'";?> value='04'>Abril</option>
                                    <option <?php if(date('m')=="05")echo "selected='true'";?> value='05'>Mayo</option>
                                    <option <?php if(date('m')=="06")echo "selected='true'";?> value='06'>Junio</option>
                                    <option <?php if(date('m')=="07")echo "selected='true'";?> value='07'>Julio</option>
                                    <option <?php if(date('m')=="08")echo "selected='true'";?> value='08'>Agosto</option>
                                    <option <?php if(date('m')=="09")echo "selected='true'";?> value='09'>Septiembre</option>
                                    <option <?php if(date('m')=="10")echo "selected='true'";?> value='10'>Octubre</option>
                                    <option <?php if(date('m')=="11")echo "selected='true'";?> value='11'>Noviembre</option>
                                    <option <?php if(date('m')=="12")echo "selected='true'";?> value='12'>Diciembre</option>
                                </select>
                            </td> 
                            <td>
                                <select id="DiaF" name='DiaF' > 
                                    <?php
                                        for($i = 1; $i <= 31; $i++)
                                        {
                                            if($i==date('d'))
                                                echo "<option selected='true' value='$i'>$i</option>"; 
                                            else
                                                echo "<option value='$i'>$i</option>";                                               
                                        }
                                    ?>                                  
                                </select>
                            </td> 
                            </tr>
                        </table>
                    </td>                                                                             
                    <td >Hora Hasta:</td>
                    <td><input style="width: 40px;" type="number" id="HF" name="HF" value="00" min="00" max="12" required="true" title="Hora de Finalización" />:
                        <input style="width: 40px;" type="number" id="MF" name="MF" value="00" min="00" max="59" required="true" title="Minutos de Finalización"/>
                        <select name="TF" id="TF">
                                <option value="AM">AM</option>
                                <option value="PM">PM</option>
                        </select>
                    </td>                    
                </tr>
            </table>
        </td>
    </tr>
    <tr><td><strong>Motivo del Permiso:</strong></td></tr>
    <tr><td><textarea rows='4' cols='85' name='Motivo' id='Motivo' required="true"></textarea></td></tr>
    <tr><td><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td style="text-align: center;"><input type="submit" name="Enviar" value="Penalizar" class="boton" /></td>
    </tr>    
</table>
</form>
</div>