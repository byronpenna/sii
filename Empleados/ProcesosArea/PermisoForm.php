<?php

/**
 * @author Jos� Manuel Calder�n \\ Lilian Cordero
 * @copyright 2014 \\ 2016,17,18,19
 */
 
$charge = $_POST['charge'];
      
$Accion = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado
                          FROM Empleado AS e
                          INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = e.IdEmpleado
                          INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
                          INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
                          where ac.IdAsignacion = $charge");                                            
$Accion->execute();
$Datos = $Accion->fetch();    

if($_SESSION["Localidad"] == "Multigimnasio")   $localidad = "Multigimnasio Don Bosco";
if($_SESSION["Localidad"] == "San Miguel")      $localidad = "Polideportivo Don Bosco San Miguel";
if($_SESSION["Localidad"] == "Santa Ana")       $localidad = "Polideportivo Don Bosco Santa Ana";
if($_SESSION["Localidad"] == "Soyapango")       $localidad = "Polideportivo Don Bosco Soyapango";

$query = "SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado, ca.IdAsignacion 
                             FROM CargosAsignacion AS ca
                             INNER JOIN Jerarquia AS j ON ca.IdCargo = j.IdCargoSuperior
                             INNER JOIN Cargos AS c ON c.IdCargos = j.IdCargoSuperior
                             INNER JOIN Empleado AS e ON ca.IdEmpleado = e.IdEmpleado
                             INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
                             WHERE j.IdCargos_Fk = $Datos[4] and ca.FechaFin = '0000-00-00'";

//echo "$query";
$DatosJefe = $bddr->prepare($query);                                            
$DatosJefe->execute();
$Jefe = $DatosJefe->fetch(); 

?>


<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px;">  
<form action="Empleados/ProcesosArea/PermisoABM.php" method="post">
<table style="width: 100%;">
    <tr><td>
            <table style="width: 100%;">
                <tr>
                    <td><h2 style="color: #1D7399;">Formulario de Solicitudes</h2></td>
                    <td style="text-align: right;"><a href="?l=Permissions"><input type="button" value="<- Atras" class="boton" /></a></td>
                </tr>    
            </table></td></tr>
    <tr><td colspan="3"><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td>
            <table style="width: 80%;">
                <input type="hidden" name="NombreEmpleado" value="<?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?>" />
                <tr><td  style="width: 30%;">Nombre del Empleado: </td><td><?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?></td></tr>
                <tr><td >Cargo Asignado:               </td><td><?php echo "$Datos[5]"?></td></tr>
                <tr><td >Area de trabajo:                </td><td><?php echo "$Datos[6]"?></td></tr>
                <tr><td></td><td><?php echo $localidad?></td></tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="3"><hr color='#69ACD7'  /></td></tr>
    <input type="hidden" name="idje"  value="<?php echo $Jefe[7]?>"/>
    <input type="hidden" name="idj"  value="<?php echo $Jefe[8]?>"/>
    <input type="hidden" name="ide"  value="<?php echo $charge?>" />
    <tr>
        <td>
            <table style="width: 80%;">
                <tr><td  style="width: 30%;">Jefe Inmediato: </td><td><?php echo "$Jefe[0] $Jefe[1] $Jefe[2] $Jefe[3]"?></td></tr>
                <tr><td >Cargo Asignado:                     </td><td><?php echo "$Jefe[5]"?></td></tr>
                <tr><td >Area de trabajo:                    </td><td><?php echo "$Jefe[6]"?></td></tr>
            </table>
        </td>
    </tr>    
    <tr><td><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td>
        <?php
            
           function HoraAMinutos($hora)
           {
                
                $HoraS =  substr($hora,0,5);                   
                $AuxFecha = explode( ":", $HoraS);
                
                if(strpos($hora, "AM"))
                {
                    if($AuxFecha[0] == 12)
                        $hora = $AuxFecha[0];
                        
                    else
                        $hora = ($AuxFecha[0] * 60) + $AuxFecha[1];
                }                  
                else
                {
                    if($AuxFecha[0] == 12)
                        $hora = ($AuxFecha[0] * 60) + $AuxFecha[1];
                        
                    else
                        $hora = (($AuxFecha[0] + 12) * 60) + $AuxFecha[1];
                }        
                return $hora;
           } 
	       
           $query = "SELECT * FROM Permisos 
                     WHERE IdAsignacionEmpleado = $charge and 
                     (TipoPermiso = 'Diligencias Personales' OR TipoPermiso = 'Penalizaci�n')  
                     AND (Estado = 'Aceptada' OR Estado = 'Penalizaci�n' ) and   YEAR(Fecha) = " . date("Y");
           //echo $query . "<br />";           
           $PermisoPersonales = $bddr->prepare($query);                                            
                                                
           $PermisoPersonales->execute();
           
           
           $HorasTotales = 0;
           $MinutosTotales = 0;
           while($DataP = $PermisoPersonales->fetch())
           {    
                $MinutosI = HoraAMinutos($DataP[4]);
                $MinutosF = HoraAMinutos($DataP[6]);
               // echo " <br /> $MinutosI - $MinutosF";   
                
                
                if($DataP[3] == $DataP[5])//Mismo D�a :D
                {                        
                    if($MinutosI < 720 && $MinutosF > 780)       
                        $MinutosT = $MinutosF - $MinutosI - 60;
                    
                    else
                        $MinutosT = $MinutosF - $MinutosI;                        
                }
                else //Fechas Diferentes :D
                {
                    $datetime1 = new DateTime($DataP[3]);
                    $datetime2 = new DateTime($DataP[5]);
                    
                    $interval = $datetime1->diff($datetime2);
                    $dias = $interval->format('%a');
                    
                    if($MinutosI < 720 )       
                        $MinutosT1 = 1020 - $MinutosI - 60;
                 
                    else
                        $MinutosT1 = 1020 - $MinutosI;
                        
                        
                    if($MinutosF < 720 )       
                        $MinutosT2 = $MinutosF - 480 - 60;
                
                    else
                        $MinutosT2 = $MinutosF - 480;  
                        
                    $MinutosT = $MinutosT2 + $MinutosT1; 
                    
                    if($dias > 1)
                    {
                        $MinutosT = $MinutosT + (($dias - 1)*(8*60));            
                    }
                }
              //  echo "<br /> " . $MinutosT . " <br />";
                                     
                $MinutosTotales += $MinutosT;

                
           }

          // echo " <br /> " . $MinutosTotales;

           /** Politica de horas por tiempo de mes**/
           $mes = date("m");
           
           switch($mes)
           {
                case "01":
                case "02":
                case "03":
                    $PoliticaHoras = 480; /** 08 horas **/         
                break;
                
                case "04":
                case "05":
                case "06":
                    $PoliticaHoras = 960; /** 16 horas **/
                break;  
                  
                case "07":
                case "08":
                case "09":
                    $PoliticaHoras = 1440; /** 24 horas **/
                break;
                
                case "10":
                case "11":
                case "12":
                    $PoliticaHoras = 2400; /** 40 horas **/
                break;                                                            
           }

           $HorasRestantes = (480 - $MinutosTotales);
        ?> 
               
<script>
// Funci�n para calcular los d�as transcurridos entre dos fechas
    function restaFechas(f1,f2)
    {
         var aFecha1 = f1.split('/'); 
         var aFecha2 = f2.split('/'); 
         var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
         var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
         var dif = fFecha2 - fFecha1;
         var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
         return dias;
    }
 
    function Info()
    {
        var x = document.getElementById('auxiliar');
        var y = document.getElementById('TipoP');
        document.getElementById('Motivo').textContent = "";
        
        if(y.value == "Por Compensaci�n")
            x.innerHTML = "La compensasi�n se da por trabajar horas extras, pero en el motivo se tiene que justificar que actividades realizo las fechas y el rango de hora.";

        else if (y.value == "Consulta al ISSS")
            x.innerHTML = "Se ha ido a una cita al seguro, o por que ha tenido una dolencia o efecto de enfermedad que impide su labor o lo situa en un riesgo para su salud.";

        else if (y.value == "Incapacidad del ISSS")
            x.innerHTML = "Se tiene que demostrar el comprabante de la enfermedad personal, por maternidad o por incapacidad establesida por el ISSS.";

       else if (y.value == "Consulta privada (Con comprobante)")
            x.innerHTML = "Se ha ido a una cita que no cubre el ISSS, por tratamiento o efecto de enfermedad que impide su labor o lo situa en un riesgo para su salud.";

        else if (y.value == "Permiso por Graduaci�n Universitaria")
            x.innerHTML = "Por motivos de Graduaci�n se dara el d�a de graduaci�n universitaria como libre y con goze de sueldo.";  

        else if (y.value == "Duelo o Enfermedades")
            x.innerHTML = "Para Cumplir con las obligaciones familiares que racionalmente reclamen su presencia, tal sea el caso de muerte o enfermedad grave del conyuge, de sus descendientes y ascendientes";

        else if (y.value == "Obligaciones de Caracter P�blico")
            x.innerHTML = "Para Cumplir con las obligaciones de caracter publicas establecidas por la ley u orden por autoridades competentes.";

        else if (y.value == "Permiso por Matrimonio")
            x.innerHTML = "Este permiso se consede para que el empleado pueda preparar y gozar tres d�as por celebraci�n de su matrimonio";

        else if (y.value == "Permisos por Paternidad")
            x.innerHTML = "El Empleado puede solicitar tres d�as consecutivos para el cuido de su hijo en caso de ser recien nacido";            

                                    
        else if (y.value == "Por Capacitaci�n o Talleres")
        {
            x.innerHTML = "Por motivos de una capacitaci�n o de taller que implica un gasto o no a la fundaci�n, que mejorar� la formaci�n y los conocimientos profesionales de para su cargo.";
            document.getElementById('Motivo').textContent = "<br />-Nombre de Capacitaci�n: \n<br />-Lugar de Capacitaci�n: \n<br />-Costo de la Capacitaci�n: \n<br />-Empresa Capacitadora: \n<br />-�rea de la Capacitaci�n: ";
        }   

        <?php
	        $numero = $HorasRestantes / 60;
            $separa = explode(".",$numero); 
                        
            $numero = $HorasRestantes % 60;
        ?> 

        else if (y.value == "Diligencias Personales")        
            x.innerHTML = "\n\nTotal de horas restantes: <?php echo "$separa[0] horas con $numero minutos"?> ";


        else if (y.value == "Por Reuni�n Laboral")        
            x.innerHTML = "\n\nToda reuni�n que genere una no marcaci�n debe ser registrada";

        else if (y.value == "Por Evento")        
            x.innerHTML = "\n\nDebe ser ingresada una solicitud cuando un evento genere una no marcaci�n";
            
        else if (y.value == "Otros")        
            x.innerHTML = "\n\nDetalle en el Motivo de la solicitud para que sea guardado. Ser� enviado a su Jefe para notificaci�n";            
                                                            
    }
    
    function Validar()
    {
        var y = document.getElementById('TipoP');
        
        if (y.value == "Diligencias Personales")
        {
        
            var hi = document.getElementById('HI').value;
            var mi = document.getElementById('MI').value;
            var ti = document.getElementById('TI').value;
            
            var hf = document.getElementById('HF').value;
            var mf = document.getElementById('MF').value;
            var tf = document.getElementById('TF').value; 
            
            var MinutosI = 0;
            var MinutosF = 0;
    
            if(ti == "AM")
                MinutosI = (hi * 60) + (mi * 1);
                 
            else
                MinutosI = ((hi * 1 + 12) * 60) + (mi * 1);
            
            
            if(tf == "AM")
                MinutosF = (hf * 60) + (mf * 1);
                 
            else
                MinutosF = ((hf * 1 + 12) * 60) + (mf * 1);
            
            
            var Ani = document.getElementById('AnioI').value;
            var Mei = document.getElementById('MesI').value;
            var Dii = document.getElementById('DiaI').value;
            
            var Anf = document.getElementById('AnioF').value;
            var Mef = document.getElementById('MesF').value;
            var Dif = document.getElementById('DiaF').value;          
            
            
            if(MinutosI > MinutosF)
            {
                alert('Error, la Hora Inicial es menor que la Hora Final')
                document.getElementById('HI').focus();
                return false;
            }
            else
            {
                var f1 = Dii + "/" + Mei + "/" + Ani;
                var f2 = Dif + "/" + Mef + "/" + Anf;
                var totalDias = restaFechas(f1,f2);               
                
                if(totalDias == 0)
                {
               
                    if(MinutosI < 720 && MinutosF > 780)
                        TotalMinutos = MinutosF - MinutosI - 60;
                    
                    else
                        TotalMinutos = MinutosF - MinutosI;
 
                }
                else if(totalDias >= 1)
                {
                    var TotalMinutos1 = 0;
                    var TotalMinutos2 = 0;
                    
                    if(MinutosI < 720)
                        TotalMinutos1 = 1020 - MinutosI - 60;
                    
                    else
                        TotalMinutos1 = 1020 - MinutosI;  
                        
                    if(MinutosI < 720)
                        TotalMinutos2 = MinutosF - 480 - 60;
                    
                    else
                        TotalMinutos2 = MinutosF - 480;   
                        
                    
                    TotalMinutos =  TotalMinutos2 + TotalMinutos1;
                    
                    if(totalDias > 1)
                    {
                        TotalMinutos = TotalMinutos + ((totalDias - 1)*(8*60));            
                    }                                            
                }
                
                TotalMinutos = TotalMinutos / 60;                
                
                if(TotalMinutos <= <?php echo $HorasRestantes;?>)
                {                
                    return true;
                }                        
                else
                {
                    alert('El total de horas solicitadas en su permiso, superan las horas restantes que posee');
                    document.getElementById('HI').focus();
                    return false;
                }                          
            }            
        }
        else
            return true;
    }
    
    function CargarPermisos()
    {
        
                
        var Options = document.getElementById('TipoP');
        var OptionPermiso = document.getElementById('TipoSolicitudP');
        var OptionLaboral = document.getElementById('TipoSolicitudL');
        
        Options.options.length = 0;
        
        if(OptionPermiso.checked)
        {
            Options.options.add(new Option("Por Compensaci�n","Por Compensaci�n"));
            Options.options.add(new Option("Consulta al ISSS","Consulta al ISSS")); 
            Options.options.add(new Option("Incapacidad del ISSS","Incapacidad del ISSS")); 
            Options.options.add(new Option("Consulta privada (Con comprobante)")); 
            Options.options.add(new Option("Permiso por Graduaci�n Universitaria","Permiso por Graduaci�n Universitaria"));
            Options.options.add(new Option("Por Capacitaci�n o Talleres","Por Capacitaci�n o Talleres"));
            Options.options.add(new Option("Duelo o Enfermedades","Duelo o Enfermedades"));
            Options.options.add(new Option("Obligaciones de Caracter P�blico","Obligaciones de Caracter P�blico"));
            Options.options.add(new Option("Permiso por Matrim�nio","Permiso por Matrimonio"));
            Options.options.add(new Option("Permisos por Paternidad","Permisos por Paternidad"));
            Options.options.add(new Option("Diligencias Personales","Diligencias Personales"));  
            
            var x = document.getElementById('auxiliar');
            x.innerHTML = "La compensasi�n se da por trabajar horas extras, pero en el motivo se tiene que justificar que actividades realizo las fechas y el rango de hora.";    
        }
        else if(OptionLaboral.checked)
        {
            Options.options.add(new Option("Por Reuni�n Laboral","Por Reuni�n Laboral"));
            Options.options.add(new Option("Por Evento","Por Evento"));
            Options.options.add(new Option("Otros","Otros"));   
            var x = document.getElementById('auxiliar');
            x.innerHTML = "\n\nDetalle en el Motivo de la solicitud para que sea guardado. Ser� enviado a su Jefe para notificaci�n";             
        }
               
        
    } 
        
</script>        
            <table style="width: 100%;">
                <tr>
                    <td><strong>Tipo de Solicitud:</strong></td>
                    <td colspan="2">
                        <table style="width: 100%;">
                            <tr>
                                <td><input type="radio" name="TipoSolicitud" value="Permiso" id="TipoSolicitudP" onchange="CargarPermisos()" checked="true" />Permiso</td>
                                <td><input type="radio" name="TipoSolicitud" value="Laboral" id="TipoSolicitudL" onchange="CargarPermisos()" />Diligencias Laborales</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="4"><hr color='#69ACD7'  /></td></tr>
                <tr><td colspan="3"><strong>Detalle del Permiso</strong></td></tr>
                <tr>
                    <td  style="vertical-align: top; width: 25%;">Tipo de Solicitud:</td>
                    <td colspan="3" style=" width: 75%;">
                        <select id='TipoP' name='TipoP' onchange="Info()" style="width: 250px;">
                            <option value="Por Compensaci�n">Por Compensaci�n</option>
                            <option value="Consulta al ISSS">Consulta al ISSS</option>
                            <option value="Incapacidad del ISSS">Incapacidad del ISSS</option>
                            <option value="Consulta privada (Con comprobante)">Consulta privada (Con comprobante)</option>                                 
                            <option value="Permiso por Graduaci�n Universitaria">Permiso por Graduaci�n Universitaria</option>
                            <option value="Por Capacitaci�n o Talleres">Por Capacitaci�n o Talleres</option>
                            <option value="Duelo o Enfermedades">Duelo o Enfermedades</option>
                            <option value="Obligaciones de Caracter P�blico">Obligaciones de Caracter P�blico</option>
                            <option value="Permiso por Matrim�nio">Permiso por Matrimonio</option>
                            <option value="Permisos por Paternidad">Permisos por Paternidad</option>
                            <option value="Diligencias Personales">Diligencias Personales</option>
                        </select><br /><br />
                        <label id="auxiliar" style="width: 100%;">La compensasi�n se da por trabajar horas extras, pero en el motivo se tiene que justificar que actividades realizo las fechas y el rango de hora. </label>
                        <br /><br />
                    </td>
                </tr>
                <tr><td>Fecha de Inicio:</td>
                    <td>
                        <table>
                        <tr>
                            <td>
                                <select id="AnioI" name='AnioI'>
                                <?php 
                                    for($i = date('Y')+1; $i >= 2001; $i--)
                                    {
                                        if( $i == date('Y'))
                                            echo "          <option selected='true' value='$i'>$i</option>";
                                        else
                                            echo "          <option value='$i'>$i</option>";                                                
                                    }
                                ?>
                                </select>
                            </td>
                            <td>
                                <select id="MesI" name='MesI' >   
                                    <option <?php if(date('m')=="01")echo "selected='true'";?> value='01'>Enero</option>                             
                                    <option <?php if(date('m')=="02")echo "selected='true'";?> value='02'>Febrero</option>
                                    <option <?php if(date('m')=="03")echo "selected='true'";?> value='03'>Marzo</option>
                                    <option <?php if(date('m')=="04")echo "selected='true'";?> value='04'>Abril</option>
                                    <option <?php if(date('m')=="05")echo "selected='true'";?> value='05'>Mayo</option>
                                    <option <?php if(date('m')=="06")echo "selected='true'";?> value='06'>Junio</option>
                                    <option <?php if(date('m')=="07")echo "selected='true'";?> value='07'>Julio</option>
                                    <option <?php if(date('m')=="08")echo "selected='true'";?> value='08'>Agosto</option>
                                    <option <?php if(date('m')=="09")echo "selected='true'";?> value='09'>Septiembre</option>
                                    <option <?php if(date('m')=="10")echo "selected='true'";?> value='10'>Octubre</option>
                                    <option <?php if(date('m')=="11")echo "selected='true'";?> value='11'>Noviembre</option>
                                    <option <?php if(date('m')=="12")echo "selected='true'";?> value='12'>Diciembre</option>
                                </select>
                            </td> 
                            <td>
                                <select id="DiaI" name='DiaI' > 
                                    <?php
                                        for($i = 1; $i <= 31; $i++)
                                        {
                                            if($i==date('d'))
                                                echo "<option selected='true' value='$i'>$i</option>"; 
                                            else
                                                echo "<option value='$i'>$i</option>";                                               
                                        }
                                    ?>                                  
                                </select>
                            </td> 
                            </tr>
                        </table>
                    </td>
                    <td >Hora Desde:</td>
                    <td><input style="width: 40px;" type="number" id="HI" name="HI" min="00" max="12" required="true" value="00" title="Hora de Inicial" />:
                        <input style="width: 40px;" type="number" id="MI" name="MI" min="00" max="59" required="true" value="00" title="Minutos de Inicial"/>
                        <select name="TI" id="TI">
                                <option value="AM">AM</option>
                                <option value="PM">PM</option>
                        </select>
                    </td>   
                    </tr>
                    <tr>
                    <td>Fecha de Finalizaci�n:</td>
                    <td>
                        <table>
                        <tr>
                            <td>
                                <select id="AnioF" name='AnioF'>
                                <?php 
                                    for($i = date('Y')+ 1; $i >= 2001; $i--)
                                    {
                                        if( $i == date('Y'))
                                            echo "          <option selected='true' value='$i'>$i</option>";
                                        else
                                            echo "          <option value='$i'>$i</option>";                                                
                                    }
                                ?>
                                </select>
                            </td>
                            <td>
                                <select id="MesF" name='MesF' >   
                                    <option <?php if(date('m')=="01")echo "selected='true'";?> value='01'>Enero</option>                             
                                    <option <?php if(date('m')=="02")echo "selected='true'";?> value='02'>Febrero</option>
                                    <option <?php if(date('m')=="03")echo "selected='true'";?> value='03'>Marzo</option>
                                    <option <?php if(date('m')=="04")echo "selected='true'";?> value='04'>Abril</option>
                                    <option <?php if(date('m')=="05")echo "selected='true'";?> value='05'>Mayo</option>
                                    <option <?php if(date('m')=="06")echo "selected='true'";?> value='06'>Junio</option>
                                    <option <?php if(date('m')=="07")echo "selected='true'";?> value='07'>Julio</option>
                                    <option <?php if(date('m')=="08")echo "selected='true'";?> value='08'>Agosto</option>
                                    <option <?php if(date('m')=="09")echo "selected='true'";?> value='09'>Septiembre</option>
                                    <option <?php if(date('m')=="10")echo "selected='true'";?> value='10'>Octubre</option>
                                    <option <?php if(date('m')=="11")echo "selected='true'";?> value='11'>Noviembre</option>
                                    <option <?php if(date('m')=="12")echo "selected='true'";?> value='12'>Diciembre</option>
                                </select>
                            </td> 
                            <td>
                                <select id="DiaF" name='DiaF' > 
                                    <?php
                                        for($i = 1; $i <= 31; $i++)
                                        {
                                            if($i==date('d'))
                                                echo "<option selected='true' value='$i'>$i</option>"; 
                                            else
                                                echo "<option value='$i'>$i</option>";                                               
                                        }
                                    ?>                                  
                                </select>
                            </td> 
                            </tr>
                        </table>
                    </td>                                                                             
                    <td >Hora Hasta:</td>
                    <td><input style="width: 40px;" type="number" id="HF" name="HF" min="00" max="12" value="00" required="true" title="Hora de Finalizaci�n" />:
                        <input style="width: 40px;" type="number" id="MF" name="MF" min="00" max="59" value="00" required="true" title="Minutos de Finalizaci�n"/>
                        <select name="TF" id="TF">
                                <option value="AM">AM</option>
                                <option value="PM">PM</option>
                        </select>
                    </td>                    
                </tr>
            </table>
        </td>
    </tr>
    <tr><td><strong>Motivo del Permiso:</strong></td></tr>
    <tr><td><textarea rows='8' cols='80' name='Motivo' id='Motivo' required="true"></textarea></td></tr>
    <tr><td><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td style="text-align: center;">
        <?php if($DatosJefe->rowCount() > 0){?>
            <input type="submit" name="Enviar" value="Solicitar" class="boton" onclick="return Validar()" />
        <?php }
              else
              {  
	               echo "<em style='color: red'>No posees Jefe asignado en el Sistema, comunicate con RRHH</em>";
              }
?>
        </td>
    </tr>    
</table>
</form>
</div>
