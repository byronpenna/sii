<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

    require '../../net.php';    
    
    if(isset($_POST['Enviar']))
    {
        if($_POST['Enviar'] == "Crear Horario")
        {
            $Insert = $bddr->prepare("Insert Into Horario values(null, :FechaI, :FechaF, :ide, :idc, :idj, :idcj, 'Pendiente', :Carrera, :Institucion)");
            $Insert->bindParam(':FechaI',$_POST['FechaI']);
            $Insert->bindParam(':FechaF',$_POST['FechaF']);            
            $Insert->bindParam(':ide',$_POST['ide']);
            $Insert->bindParam(':idc',$_POST['idc']);
            $Insert->bindParam(':idj',$_POST['idje']);
            $Insert->bindParam(':idcj',$_POST['idjec']);
            $Insert->bindParam(':Carrera',$_POST['Carrera']);
            $Insert->bindParam(':Institucion',$_POST['Institucion']);            
            $Insert->execute();                       
            
            Redireccion("../../Empleado.php?l=Schedule&n=6");                     
        }
        
        if($_POST['Enviar'] == "Eliminar")
        {

            $Delete = $bddr->prepare("Delete from Horario where IdHorario = :ih");
            $Delete->bindParam(':ih',$_POST['ih']);
            $Delete->execute();                       
            
            Redireccion("../../Empleado.php?l=Schedule&n=6");                     
        } 
        
        if($_POST['Enviar'] == "Agregar")
        {
            $Inicio = $_POST['HoraI'] . ":" .$_POST['MinI'] . " " . $_POST['TiempoI'];
            $Fin    = $_POST['HoraF'] . ":" .$_POST['MinF'] . " " . $_POST['TiempoF'];
            
            $Insert = $bddr->prepare("Insert Into Horario_Bloque values(null, :ih, :Dia, :inicio, :fin)");
            $Insert->bindParam(':ih', $_POST['ih']);
            $Insert->bindParam(':Dia', $_POST['Dia']);            
            $Insert->bindParam(':inicio', $Inicio);
            $Insert->bindParam(':fin', $Fin);
            $Insert->execute();                       
            
            $_SESSION['ih'] = $_POST['ih'];
            
            Redireccion("../../Empleado.php?l=Schedule&n=6");                     
        } 

        if($_POST['Enviar'] == "x")
        {

            $Delete = $bddr->prepare("Delete from Horario_Bloque where IdBloque = :ib");
            $Delete->bindParam(':ib',$_POST['ib']);
            $Delete->execute();                       
            
            Redireccion("../../Empleado.php?l=Schedule&n=6");                     
        }   
        
        if($_POST['Enviar'] == "Solicitar")
        {
            $IdHorario =  $_POST['ih'];
            
            $Query = "Select * from Horario_Bloque where IdHorario = $IdHorario";
            $Bloques = $bddr->prepare($Query);
            $Bloques->execute(); 
            
            $ArrayDay = array("Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo");
            $TablaHorario = "<table style='width: 100%'>
                  <tr>
                      <th style='width: 14%'>Lunes</th>
                      <th style='width: 14%'>Martes</th>
                      <th style='width: 15%'>Mi&eacute;rcoles</th>
                      <th style='width: 14%'>Jueves</th>
                      <th style='width: 14%'>Viernes</th>
                      <th style='width: 15%'>S&aacute;bado</th>
                      <th style='width: 14%'>Domingo</th>
                  </tr>
                  <tr>";
                  
            $TotalHoras = 0;
            foreach($ArrayDay as $Value)
            {
                $Query = "Select * from Horario_Bloque where IdHorario = $IdHorario and Dia = '$Value'";
                $BloqueDia = $bddr->prepare($Query);
                $BloqueDia->execute();
                
                
                $TotalDias = 0;
                
                $TablaHorario .= "<td style='vertical-align: top;'>";
                while($DataBd = $BloqueDia->fetch())
                {
                    $TablaHorario .= utf8_encode("<table border='1' style='width: 100%;'>
                                      <tr><td style='text-align: center'>$DataBd[3]</td></tr>
                                      <tr><td style='text-align: center'>$DataBd[4]</td></tr>
                                      </table>");
                    
                    
                    $HoraI = substr($DataBd[3],0,2);
                    $MinI = substr($DataBd[3],3,2);
                    $TiempoI = substr($DataBd[3],6,2);
                    
                    $HoraF = substr($DataBd[4],0,2);
                    $MinF = substr($DataBd[4],3,2);
                    $TiempoF = substr($DataBd[4],6,2);        
                            
                    
                    if($TiempoI == "AM")
                        $MinutosI = ($HoraI * 60) + $MinI;
                    
                    else
                        $MinutosI = (12 + $HoraI) * 60 + $MinI;
                        
                        
                    if($TiempoF == "AM")
                        $MinutosF = ($HoraF * 60) + $MinF;
                    
                    else
                        $MinutosF = (12 + $HoraF) * 60 + $MinF;                    
                                    
                                    
                    $TotalDias = $TotalDias + ($MinutosF - $MinutosI);
                    
                    
                    if($MinutosI < 720 && $MinutosF > 780)
                        $TotalDias = $TotalDias - 60;
                }
                
                
                $TotalDias = $TotalDias / 60;
                $TotalHoras += $TotalDias;
                $TablaHorario .= "</td>";
            }
            $TablaHorario .= "</tr>
                  <tr><td colspan='7'><hr color='#69ACD7'  /></td></tr>  
                  <tr>
                      <td colspan='6' class='tdleft'><strong>Total de Horas realizadas por semana </strong></td>
                      <td><strong>$TotalHoras Horas</strong></td>
                  </tr>  
                  </table>";
            
            
            $Query = "Select * from Horario where IdHorario = $IdHorario";
            $Horario = $bddr->prepare($Query);
            $Horario->execute();            
            $DataH = $Horario->fetch();
            
            
            $CorreoEmpleado = $bddr->prepare("SELECT Correo FROM DatosInstitucionales where IdEmpleado = " . $DataH[3]);
            $CorreoEmpleado->execute();    
            $DataCE = $CorreoEmpleado->fetch();
            
            $CorreoEmpleado = $bddr->prepare("SELECT Nombre1, Nombre2, Nombre3, Apellido1, Apellido2 FROM Empleado where IdEmpleado = " . $DataH[3]);
            $CorreoEmpleado->execute();    
            $DataE = $CorreoEmpleado->fetch();            
            
            
                
            $CorreoEmpleado = $bddr->prepare("SELECT Correo FROM DatosInstitucionales where IdEmpleado = " . $DataH[5]);
            $CorreoEmpleado->execute();    
            $DataCJ = $CorreoEmpleado->fetch();     
                 
            
            $to = "$DataCJ[0]"; 
                      
            $subject = "Solicitud de Permiso de Horario Educativo";
            
            $message = "
                <h2>Solicitud de Permiso de Horario Educativo</h2><br />
                
                Nombre del Empleado: <br />
                $DataE[0] $DataE[1] $DataE[2] $DataE[3] $DataE[4] <br /><br />
                
                Instituci�n Educativa:<br />
                $DataH[9]<br /><br />
                
                Carrera de Estudio, Dipolmado o Curso:<br />
                $DataH[8]<br /><br />                
                
                Fecha de Inicio: <br />
                $DataH[1]<br /><br />
                
                Fecha de Finalizaci�n: <br />
                $DataH[2]<br /><br />
                
                Horario:<br /> 
                $TablaHorario
                
                Para aceptar o rechazar a la solicitud pulse  
                <a href='http://siifusalmo.org/PermisoView.php?h=$IdHorario'>Aqu�</a>";

            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org, permisos@fusalmo.org, liligr9221@gmail.com" . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            mail($to,$subject,$message,$headers);            
            
            Redireccion("../../Empleado.php?l=Schedule&n=6");                                      
        }     
        if($_POST['Enviar'] == "Guardar")
        {
            if($_POST['Decision'] == "Denegar")
                $estado = "Denegado"; 
                
            if($_POST['Decision'] == "Aceptar")
                $estado = "Aceptado";
            
            $Update = $bddr->prepare("UPDATE Horario SET Estado = '$estado' WHERE IdHorario = :ih");
            $Update->bindParam(':ih',  $_SESSION["h"]);            
            $Update->execute();
            
            $IdHorario =  $_SESSION["h"];
                
            unset($_SESSION["h"]);  
            echo"<script language='javascript'>alert('Proceso exitoso');</script>";
                        
            $Query = "Select * from Horario where IdHorario = $IdHorario";
            $Horario = $bddr->prepare($Query);
            $Horario->execute();            
            $DataH = $Horario->fetch();
            
            
            $CorreoEmpleado = $bddr->prepare("SELECT Correo FROM DatosInstitucionales where IdEmpleado = " . $DataH[3]);
            $CorreoEmpleado->execute();    
            $DataCE = $CorreoEmpleado->fetch();
            
            $CorreoEmpleado = $bddr->prepare("SELECT Nombre1, Nombre2, Nombre3, Apellido1, Apellido2 FROM Empleado where IdEmpleado = " . $DataH[3]);
            $CorreoEmpleado->execute();    
            $DataE = $CorreoEmpleado->fetch();            
            
            $to = "$DataCE[0]";                        
            $subject = "Solicitud de Permiso de Horario Educativo";
            
            $message = "
                <h2>Solicitud de Permiso de Horario Educativo</h2><br />
                
                Nombre del empleado solicitante: <br />
                $DataE[0] $DataE[1] $DataE[2] $DataE[3] $DataE[4] <br /><br />
                
                Estado del Permiso: $estado
                
                Para mayor detalle puedes entrar al SIIF en la opcion 
                <a href='http://siifusalmo.org/Empleado.php?l=Permissions'>MiArea->Notificaciones y Permisos</a>";

            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org, permisos@fusalmo.org" . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            mail($to,$subject,$message,$headers);            
            
            Redireccion("../../Empleado.php?l=Schedule&n=6");                                                                        
        } 
    }
?>