<?php

/**
 * @author Manuel
 * @copyright 2016
 */


$IdEmployee = $_POST['idp'];

if(isset($_SESSION['ide']))
{
    $IdEmployee = $_SESSION['ide'];
    unset($_SESSION['ide']);
}


$Empleados = $bddr->prepare("SELECT e.IdEmpleado, e.Nombre1, e.Nombre2, e.Nombre3, e.Apellido1, e.Apellido2, t.NombreAreaDeTrabajo, c.Cargo, ca.IdAsignacion
                             FROM Empleado as e 
                             INNER JOIN CargosAsignacion as ca on e.IdEmpleado = ca.IdEmpleado
                             INNER JOIN Cargos as c on ca.IdCargo = c.IdCargos 
                             INNER JOIN AreasDeTrabajo as t on c.IdArea_Fk = t.IdAreaDeTrabajo
                             where ca.IdEmpleado = $IdEmployee  and ca.FechaFin = '0000-00-00' 
                             GROUP BY e.IdEmpleado");

$Empleados->execute();            
                
if($Empleados->rowCount() == 0)
Redireccion("../../Empleado.php?l=PermissionsControl&n=1");

$DataE = $Empleados->fetch();


if(!isset($_POST['tipoP']))
Redireccion("?l=PermissionsControl");


if($_POST['tipoP'] == "Pendientes")
    $Estado = "Pendiente";
    
if($_POST['tipoP'] == "Denegados")
    $Estado = "Denegada";
    
if($_POST['tipoP'] == "Aprobados")
    $Estado = "Aceptada";
    
if($_POST['tipoP'] == "Penalizaci�n")
    $Estado = "Penalizaci�n";        



$TotalPermisos = $bddr->prepare("SELECT * FROM Permisos as p
                                 inner join CargosAsignacion as ca on p.IdAsignacionEmpleado = ca.IdAsignacion  
                                 WHERE YEAR(Fecha) = ".$_SESSION['a�o']." and ca.IdAsignacion = $DataE[8]  and Estado = '$Estado'");
$TotalPermisos->execute();


?>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px;">  
<table style="width: 100%;">
    <tr>
        <td colspan="2">
            <table style="width: 100%">
                <tr>
                    <td><h2>Vista de Solicitudes</h2></td> 
                    <td style="text-align: right;">
                    <form action="?l=PermissionsEmployee" method="post">
                        <input type="hidden" name="idp" value="<?php echo $IdEmployee?>" />
                        <input type="button" value="<- Atras" class="boton" onclick="this.form.submit()" />
                    </form>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="2">
        <?php
        echo "<table style='width: 60%;'>
                <tr><td>Empleado:</td><td>$DataE[1] $DataE[2] $DataE[3] $DataE[4]</td></tr>        
                <tr><td>Cargo:</td><td>$DataE[7]</td></tr>
                <tr><td>�rea:</td><td>$DataE[6]</td></tr>
             </table>";
        ?>
        </td>
    </tr>    
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr><td style="width: 30%;">Tipo de Solicitudes:</td><td><?php echo $_POST['tipoP'];?></td></tr>
    <tr><td>Total de Solicitudes:</td><td><?php echo $TotalPermisos->rowCount();?></td></tr>
    <tr><td>A�o de Solicitudes:</td><td><?php echo $_SESSION['a�o'];?></td></tr>
</table>
<br />
<table style="width: 100%;">        
    <tr><th>Fecha Inicio</th><th>Hora de Inicio</th><th>Fecha Fin</th><th>Hora de Fin</th>
        <th>
            <?php if($_POST['tipoP'] == "Penalizaci�n") {?>
            <form action="?l=PermissionsPenalty" method="post">
                <input type="hidden" name="tipoP" value="Penalizaci�n" />                
                <input type="hidden" name="idp" value="<?php echo $IdEmployee;?>" />
                <input type="hidden" name="charge" value="<?php echo $DataE[8] ;?>" />                 
                <input type="submit" class="boton" value="Agregar" style="width: 60px;" />                
            </form>
            <?php } ?> 
        </th>
    </tr>
    <?php
        
        if($TotalPermisos->rowCount() > 0)
        {
            while( $DataP = $TotalPermisos->fetch() )
            {
    
                echo "<tr>
                        <td style='text-align: center;'>$DataP[3]</td>                    
                        <td style='text-align: center;'>$DataP[4]</td>
                        <td style='text-align: center;'>$DataP[5]</td>
                        <td style='text-align: center;'>$DataP[6]</td>
                        <td style='text-align: center;'>
                        <form method='post' action='PermisoView.php' target='_blank'>
                            <input type='hidden' name='idp' value='$DataP[0]' />
                            <input type='hidden' name='aux' value='View'>
                            <input type='submit' name='Permiso' value='Ver' class='boton'  style='width: 40px'/>
                        </form>
                        </td>
                      </tr>";
            }            
        }
        else
            echo "<tr><td colspan='5' style='text-align: center;'><br /><strong style='color: red'>No hay registros de permisos o penalizaciones</strong><br /></td></tr>";
                        
    ?>
</table>
</div>