<?php
	session_start();
    require '../../net.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Ficha del Empleado</title>
</head>
<script >
function cerrar() { setTimeout(window.close,1500); }
</script>
<body onload="window.print();cerrar()" style="font-family: Calibri; font-size: small;">
<div style="width: 94%; margin-left: 3%;">
<table style="width: 100%;">
    <tr><td style="vertical-align: central;"><h1>Listados de Permisos</h1></td><td style="text-align: right;"><img src="../../images/LogoFUSALMO2015-mod-rec.png" style="width:150px;" /></td></tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="2">            
        <?php
        
            $FechaI = $_POST['FechaInicio'];
            $FechaF = $_POST['FechaFin'];
            $Area = $_POST['Area'];
        
            $DatosArea = $bddr->prepare("SELECT a.NombreAreaDeTrabajo, c.Cargo, e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2 
                                         FROM AreasDeTrabajo as a INNER JOIN AreasDeTrabajoJefes as atr on a.IdAreaDeTrabajo = atr.IdArea 
                                         INNER JOIN Cargos as c on atr.IdCargoJefe = c.IdCargos 
                                         INNER JOIN CargosAsignacion as ca on c.IdCargos = ca.IdCargo 
                                         INNER JOIN Empleado as e on e.IdEmpleado = ca.IdEmpleado 
                                         WHERE atr.IdArea = $Area and ca.FechaFin = '0000-00-00'");
            $DatosArea->execute();
            $DataArea = $DatosArea->fetch();                                 
        
            $QueryResumen = "SELECT p.Estado, COUNT(p.IdPermiso) FROM Permisos as p  
                             INNER JOIN CargosAsignacion as ac on p.IdAsignacionEmpleado = ac.IdAsignacion
                             INNER JOIN Cargos as c on ac.IdCargo = c.IdCargos 
                             INNER JOIN Empleado as e on ac.IdEmpleado = e.IdEmpleado 
                             WHERE (p.Fecha BETWEEN '$FechaI' AND '$FechaF') AND c.IdArea_Fk = $Area 
                             GROUP BY p.Estado";


                                                      
            $PermisosResumen = $bddr->prepare($QueryResumen);                                
            $PermisosResumen->execute();

            echo "<table style='width: 50%'>
                  <tr>
                    <th>Area:</th>
                    <td>$DataArea[0]</td>
                  </tr>
                  <tr>
                    <th>Cargo:</th>
                    <td>$DataArea[1]</td>
                  </tr>
                  <tr>
                    <th>Empleado:</th>
                    <td>$DataArea[2] $DataArea[3] $DataArea[4] $DataArea[5]</td>
                  </tr>
                  </table><hr color='skyblue' />";
            
            
            while($DataPr = $PermisosResumen->fetch())
            {
                $Query = "SELECT p.*, e.Nombre1, e.Nombre2, e.Nombre3, e.Apellido1, e.Apellido2 FROM Permisos as p 
                          INNER JOIN CargosAsignacion as ac on p.IdAsignacionEmpleado = ac.IdAsignacion
                          INNER JOIN Cargos as c on ac.IdCargo = c.IdCargos 
                          INNER JOIN Empleado as e on ac.IdEmpleado = e.IdEmpleado 
                          WHERE (p.Fecha BETWEEN '$FechaI' AND '$FechaF') AND c.IdArea_Fk = $Area and p.Estado = '$DataPr[0]'
                          ORDER BY p.Fecha ASC";  
                                              
                $Permisos = $bddr->prepare($Query);                                
                $Permisos->execute();
            
                echo "<br /><h3>Permisos $DataPr[0]: $DataPr[1]</h3>
                      <table style='width: 100%;'>";                   
                while($DataP = $Permisos->fetch())
                {
                    echo "<tr>
                            <th style='width: 8%; vertical-align: top;'>$DataP[3]<br />$DataP[4]</th>
                            <th style='width: 8%; vertical-align: top;'>$DataP[5]<br />$DataP[6]</th>                    
                            <th style='width: 30%; text-align: left; vertical-align: top;'>$DataP[7]</th>                            
                            <th style='width: 8%; vertical-align: top;'>$DataP[9]</th>
                            <th style='width: 8%; vertical-align: top;'>$DataP[10]</th>
                            <th style='width: 8%; vertical-align: top;'>$DataP[11] $DataP[12] $DataP[14]</th>  
                                                    
                          </tr>";                      
                }
                echo "</table>";                     
            }
        ?>
        </td>
    </tr>
</table>
</div>