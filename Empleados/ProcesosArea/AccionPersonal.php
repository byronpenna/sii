<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

?>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">   
    <h2 style="color: blue;">Acciones de Personal</h2><br />
    
    <table style="width: 90%; margin-left: 5%;">
    
        <tr><td style="text-align: center;" >
                <a href="Empleado.php?l=PersonalActionForm"><input type="button" class="boton" value="Crear Acci�n" /></a>
            </td>
        </tr>  
        <tr><td><hr color='#69ACD7'  /></td></tr>
        <tr><td>Listado de acciones </td></tr>              
        <?php                
                $MisCargos = $bddr->prepare("SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo
                                            FROM CargosAsignacion AS ca
                                            INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                                            INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
                                            WHERE ca.IdEmpleado = '$IdEmp'");                                            
                $MisCargos->execute();
                
                if(isset($_POST['MiCargo']))
                    $IdCharge = $_POST['MiCargo'];
                
                else
                    $IdCharge = ''; 
                    
                $Cargos = "";
        ?> 
        <tr>
            <td>
                <form method="post">
                    Cargos:   
                    <select name="MiCargo" id="MiCargo">
                        <option value='...' selected='true' >...</option> 
                        <?php                                                    
        	                   while($Datos = $MisCargos->fetch())
                               {
                                    if($IdCharge == $Datos[1])
                                    {
                                        echo "<option value='$Datos[1]' selected='true' >$Datos[3]</option>";
                                        $Cargos = $Datos[3];
                                    }
                                    else
                                        echo "<option value='$Datos[1]' >$Datos[3]</option>";                                                                                     
                               }
                        ?>
                    </select>
                    <input style="margin-left: 10px; width: 125px;" type="submit" name="Enviar" value="Acciones Hechas"  class="botonG" />                                                
                    <input style="margin-left: 10px; width: 125px;" type="submit" name="Enviar" value="Mis Acciones"  class="botonG" />
                </form>
            </td>            
        </tr>
        
        <tr>
            <td>
                <?php
                
                    if(isset($_POST['Enviar']))
                    {
                        if($_POST['Enviar'] == "Acciones Hechas")
                        {
                            if($IdCharge != '')
                            {
                                $Acciones = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, ap.FechaCreada, c.IdCargos, c.Cargo, ap.IdAccionPersonal
                                                            FROM Empleado AS e
                                                            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = e.IdEmpleado
                                                            INNER JOIN AccionesPersonales AS ap ON ap.IdAsignacionEmpleado = ac.IdAsignacion
                                                            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
                                                            where ap.IdAsignacionJefe = $IdCharge");                                            
                                $Acciones->execute();          
                                
                                echo "<table style='width: 90%; margin-left: 5%'>";
                                
                                if($Acciones->rowCount() > 0)
                                {
                                    while($accion = $Acciones->fetch())
                                    {                                                                           
                                        echo "<form action='Empleado.php?l=Ticket' method='post'>
                                              <tr><td>$accion[0] $accion[1] $accion[2]</td><td>$accion[3]</td><td>$accion[5]</td>
                                                  <td><input type='hidden' name='action' value='$accion[6]' />
                                                      <input style='width: 50px' type='submit' value='Ver' class='boton' /></td>
                                              </tr>
                                              </form>";
                                    }
                                }
                                else       
                                    echo "<tr><td colspan='4' style='text-align: center;' ><br />
                                              <h3 style='color: red'>No hay Accion de Personales como $Cargos</h3>
                                              </td>
                                          </tr>";
                                             
                                echo "</table>";
                            }
                            else
                                echo "<br /><center><em style='color: #000080'>Esperando Datos</em></center>";
                        }
                        if($_POST['Enviar'] == "Mis Acciones")
                        {
                            if($IdCharge != '')
                            {
                                $Acciones = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, ap.FechaCreada, c.IdCargos, c.Cargo, ap.IdAccionPersonal
                                                            FROM Empleado AS e
                                                            INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = e.IdEmpleado
                                                            INNER JOIN AccionesPersonales AS ap ON ap.IdAsignacionEmpleado = ac.IdAsignacion
                                                            INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
                                                            where ap.IdAsignacionEmpleado = $IdCharge");                                            
                                $Acciones->execute();          
                                
                                echo "<table style='width: 90%; margin-left: 5%'>";
                                
                                if($Acciones->rowCount() > 0)
                                {
                                    while($accion = $Acciones->fetch())
                                        echo "<form action='Empleado.php?l=Ticket' method='post'>
                                              <tr><td>$accion[0] $accion[1] $accion[2]</td><td>$accion[3]</td><td>$accion[5]</td>
                                                  <td><input type='hidden' name='action' value='$accion[6]' />
                                                      <input style='width: 50px' type='submit' value='Ver' class='boton' /></td>
                                              </tr>
                                              </form>";
                                }
                                else       
                                    echo "<tr><td colspan='4' style='text-align: center;' ><br />
                                              <h3 style='color: red'>No hay Accion de Personales asignadas a mi</h3>
                                              </td>
                                          </tr>
                                          </form>";
                                             
                                echo "</table>";
                            }
                            else
                                echo "<br /><center><em style='color: #000080'>Esperando Datos</em></center>";                            
                        }
                    }
                    else
                            echo "<br /><center><em style='color: #000080'>Esperando Datos</em></center>";
                ?>
            </td>
        </tr>
    </table>
</div>