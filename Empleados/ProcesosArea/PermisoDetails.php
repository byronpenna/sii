
<?php
	
    $ide = $_POST['ide'];
          
    $Accion = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado
                              FROM Empleado AS e
                              INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = e.IdEmpleado
                              INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
                              INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
                              where e.IdEmpleado = $ide");                                            
    $Accion->execute();
    $Datos = $Accion->fetch();      
    
?>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px;">  

<table style="width: 100%;">
    <tr><td><h2 style="color: #1D7399;">Reportes de Solicitudes</h2></td>
        <td style="text-align: right;">
            <form action="Empleado.php?l=Permissions" method="post">  
                <input type="hidden" name="Enviar" value="Ver Solicitudes" />
                <input type="submit" name="Regresar" value="<- Regresar" class="boton" />
            </form>
        </td>
    </tr>
    <tr><td colspan="4"><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td colspan="2">
            <table style="width: 100%;">
                <input type="hidden" name="NombreEmpleado" value="<?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?>" />
                <tr><td  style="width: 30%;">Nombre del Empleado: </td><td><?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?></td></tr>
                <tr><td>Cargo Asignado:               </td><td><?php echo "$Datos[5]"?></td></tr>
                <tr><td>Area de trabajo:                </td><td><?php echo "$Datos[6]"?></td></tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="4"><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td colspan="4">
            <table style="width: 100%;" rules='cols'>
            <tr><td colspan="5" style="text-align: right;" ><h2 style="color: green;">Solicitudes Aceptados</h2></td></tr>
            <tr><th style="width: 25%;">Tipo de Solicitud</th><th colspan='2' style="width: 23%;">Desde</th><th colspan='2'  style="width: 23%;">Hasta</th></tr>
            <?php
            
                $Permisos = $bddr->prepare("SELECT TipoPermiso, Fecha, HoraDesde, FechaFin, HoraHasta, Motivo FROM Permisos 
                                            where IdAsignacionEmpleado = $ide and Estado = 'Aceptada' 
                                            ORDER BY IdPermiso DESC");                                            
                $Permisos->execute();                
                
                if($Permisos->rowCount() > 0)
                {
                    while($Datos = $Permisos->fetch())
                    {
                        echo "<tr><td colspan='5'><hr /></td></tr>
                              <tr><td>$Datos[0]</td>
                                  <td style='text-align: center;'>$Datos[1]</td><td style='text-align: center;'>$Datos[2]</td>
                                  <td style='text-align: center;'>$Datos[3]</td><td style='text-align: center;'>$Datos[4]</td>
                              </tr>                          
                              <tr><td colspan='5'><strong>Motivo:</strong> $Datos[5]<br /><br /></td></tr>";
                    }                    
                }
                else    
                    echo "<tr><td colspan='5' style='text-align: center; color: red'>No hay solicitudes Aceptadas</td></tr>";
            ?>
            </table>
        </td>
    </tr>      
    <tr>
        <td colspan="4">
            <table style="width: 100%;" rules='cols'>
            <tr><td colspan="5" style="text-align: right;" ><h2 style="color: red;">Solicitudes Denegados</h2></td></tr>
            <tr><th style="width: 25%;">Tipo de Solicitud</th><th colspan='2' style="width: 23%;">Desde</th><th colspan='2'  style="width: 23%;">Hasta</th></tr>
            <?php
            
                $Permisos = $bddr->prepare("SELECT TipoPermiso, Fecha, HoraDesde, FechaFin, HoraHasta, Motivo FROM Permisos 
                                            where IdAsignacionEmpleado = $ide and Estado = 'Denegada' 
                                            ORDER BY IdPermiso DESC");                                            
                $Permisos->execute();                
                
                if($Permisos->rowCount() > 0)
                {
                    while($Datos = $Permisos->fetch())
                    {
                        echo "<tr><td colspan='5'><hr /></td></tr>
                              <tr><td>$Datos[0]</td>
                                  <td style='text-align: center;'>$Datos[1]</td><td style='text-align: center;'>$Datos[2]</td>
                                  <td style='text-align: center;'>$Datos[3]</td><td style='text-align: center;'>$Datos[4]</td>
                              </tr>                          
                              <tr><td colspan='5'><strong>Motivo:</strong> $Datos[5]<br /><br /></td></tr>";
                    }                    
                }
                else    
                    echo "<tr><td colspan='5' style='text-align: center; color: red'>No hay solicitudes denegadas</td></tr>";
        
            ?>
            </table>
        </td>
    </tr>      
    </table>

</div>    