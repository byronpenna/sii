<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 
$charge = $_POST['charge'];

if(isset($_POST['charge']))
    unset($_SESSION['charge']);

if($charge == "")
    $charge = $_SESSION['charge'];

else
    $_SESSION['charge'] = $charge;
   
$Accion = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado
                          FROM Empleado AS e
                          INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = e.IdEmpleado
                          INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
                          INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
                          where ac.IdAsignacion = $charge ");                                            
$Accion->execute();
$Datos = $Accion->fetch();    

if($_SESSION["Localidad"] == "Multigimnasio")   $localidad = "Multigimnasio Don Bosco";
if($_SESSION["Localidad"] == "San Miguel")      $localidad = "Polideportivo Don Bosco San Miguel";
if($_SESSION["Localidad"] == "Santa Ana")       $localidad = "Polideportivo Don Bosco Santa Ana";
if($_SESSION["Localidad"] == "Soyapango")       $localidad = "Polideportivo Don Bosco Soyapango";
 

$DatosJefe = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado, ca.IdAsignacion 
                             FROM CargosAsignacion AS ca
                             INNER JOIN Jerarquia AS j ON ca.IdCargo = j.IdCargoSuperior
                             INNER JOIN Cargos AS c ON c.IdCargos = j.IdCargoSuperior
                             INNER JOIN Empleado AS e ON ca.IdEmpleado = e.IdEmpleado
                             INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
                             WHERE j.IdCargos_Fk = $Datos[4] and ca.FechaFin = '0000-00-00'");                                            
$DatosJefe->execute();
$Jefe = $DatosJefe->fetch(); 




?>

<script>
function CargarForm()
{
	var code = "<?php echo "$charge";?>";
    document.getElementById('Content').innerHTML = "<center><em style='color: green;'>Cargando datos...</em></center>";

    $.get("Empleados/ProcesosArea/HorarioForm.php", {code : code},
  		function(resultado)
  		{
            document.getElementById('Content').innerHTML = "";                        
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#Content').append(resultado);	            			
  		}
   	);        
}
function CargarHorario()
{
	var code = "<?php echo "$Datos[4]";?>";
    var code2 = "<?php echo "$Datos[7]";?>";
    document.getElementById('Content').innerHTML = "<center><em style='color: green;'>Cargando datos...</em></center>";

    $.get("Empleados/ProcesosArea/Horario.php", {code : code , code2 : code2},
  		function(resultado)
  		{
            document.getElementById('Content').innerHTML = "";                        
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#Content').append(resultado);	            			
  		}
   	);        
}
</script>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px;">  

<table style="width: 100%;">
    <tr><td>
            <table style="width: 100%;">
                <tr>
                    <td><h2 style="color: #1D7399;">Solicitud de Permiso por Horario de Educaci�n</h2></td>
                    <td style="text-align: right;"><a href="?l=Permissions"><input type="button" value="<- Atras" class="boton" /></a></td>
                </tr>    
            </table>
        </td>
    </tr>
    <tr><td colspan="3"><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td>
            <table style="width: 80%;">
                <input type="hidden" name="NombreEmpleado" value="<?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?>" />
                <tr><td  style="width: 30%;">Nombre del Empleado: </td><td><?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?></td></tr>
                <tr><td >Cargo Asignado:               </td><td><?php echo "$Datos[5]"?></td></tr>
                <tr><td >Area de trabajo:                </td><td><?php echo "$Datos[6]"?></td></tr>
                <tr><td></td><td><?php echo $localidad?></td></tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="3"><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td>
            <table style="width: 80%;">
                <tr><td  style="width: 30%;">Jefe Inmediato: </td><td><?php echo "$Jefe[0] $Jefe[1] $Jefe[2] $Jefe[3]"?></td></tr>
                <tr><td >Cargo Asignado:                     </td><td><?php echo "$Jefe[5]"?></td></tr>
                <tr><td >Area de trabajo:                    </td><td><?php echo "$Jefe[6]"?></td></tr>
            </table>
        </td>
    </tr>    
    <tr><td><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td>
            <table style="width: 50%;text-align: center;">
                <tr>
                    <td><input type="button" class="boton" value="Crear" onclick="CargarForm()" /></td>
                    <td><input type="button" class="boton" value="Ver Horarios" onclick="CargarHorario()" /></td>
                </tr>
            </table>
        </td>
    </tr>           
    <tr><td><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td style="text-align: center;">                             
            <div id="Content">
                <?php
        	       if(isset($_GET['n']))
                   {
                        if($_GET['n'] == 6 || $_GET['n'] == 7)
                            echo "<script>CargarHorario()</script>";
                   }
                ?>
            </div>            
        </td>
    </tr>
</table>

</div>