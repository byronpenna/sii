<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

?>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">
    <h2 style="color: #197198;">Notificaci�n / Permisos  de Empleados</h2><br />
    
    <table style="width: 100%;">
        <tr><td colspan="4"><hr color='#69ACD7'  /></td></tr>          
        <?php                

                $MisCargos = $bddr->prepare("SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo
                                            FROM CargosAsignacion AS ca
                                            INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                                            INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
                                            WHERE ca.IdEmpleado = '$IdEmp' and ca.FechaFin = '0000-00-00'");                                            
                $MisCargos->execute();
                
                if(isset($_POST['MiCargo']))
                    $IdCharge = $_POST['MiCargo'];
                
                else
                    $IdCharge = ''; 
                    
                $Cargos = "";
        ?> 
        <tr><form method="post" action="Empleado.php?l=Permissions">
            <td colspan="4">                
                Cargo:
                <select name="MiCargo" id="MiCargo">                         
                    <?php                                                    
    	                   while($Datos = $MisCargos->fetch())
                           {
                                if($IdCharge == $Datos[1])                                    
                                    echo "<option value='$Datos[1]' selected='true' >$Datos[3]</option>";                                    
                                else
                                    echo "<option value='$Datos[1]' >$Datos[3]</option>";
                                
                                $Cargos = $Datos[1];
                                $IdCargosAux = $Datos[2];                                                                               
                           }
                           if($MisCargos->rowCount() == 0)
                                echo "<option value='...' >...</option>";
                        ?>
                </select>
                <br /><br />
            </td>
        </tr>
        <tr>
            <td><input style="margin-left: 10px; width: 125px;" type="submit" name="Enviar" value="Mis Solicitudes"  class="botonG" /></td>
            <td><input style="margin-left: 10px; width: 125px;" type="submit" name="Enviar" value="Ver Solicitudes"  class="botonG" /></td>
            </form>
            <td>                
                <form action="Empleado.php?l=PermissionsForm" method="post">
                     <input type="hidden" name="charge" value="<?php echo $Cargos?>" />
                      <input style="margin-left: 10px; width: 125px;" type="submit" name="Enviar" value="Crear Solicitud"  class="botonG" />
                </form>                
            </td>
            <td>                
                <form action="Empleado.php?l=Schedule" method="post">
                     <input type="hidden" name="charge" value="<?php echo $Cargos?>" />
                      <input style="margin-left: 10px; width: 125px;" type="submit" name="Enviar" value="Horario por Estudio"  class="botonG" />
                </form>                
            </td>                        
        </tr>
        <tr><td colspan="4"><hr color='#69ACD7'  /></td></tr>
        <tr>
            <td colspan="4">
            <?php
                if(isset($_POST['Enviar']))
                {
                    if($_POST['Enviar'] == "Mis Solicitudes")
                    {         
                        
                        
                        if(isset($_POST['All']))
                        {
                            if($_POST['All'] == "Ver Todos")
                            {
                                $Query = "SELECT * FROM Permisos 
                                          where IdAsignacionEmpleado = $Cargos 
                                          ORDER BY Fecha DESC";                            
                                
                                $Value = "Ver Mes";
        
                            }
                            else
                            {
                                $Query = "SELECT * FROM Permisos 
                                          where IdAsignacionEmpleado = $Cargos and MONTH(Fecha) = '". date("m")."'
                                          ORDER BY Fecha DESC";                            
                                
                                $Value = "Ver Todos";
                            }
                        }
                        else
                        {
                            $Query = "SELECT * FROM Permisos 
                                      where IdAsignacionEmpleado = $Cargos and MONTH(Fecha) = '". date("m")."'
                                      ORDER BY Fecha DESC";
                                      
                            $Value = "Ver Todos";
                        }
                                                
                        $MisPermisos = $bddr->prepare($Query);          
                        $MisPermisos->execute();
                           
                        
                        
                        
                        echo "<table style='width: 100%; text-align: center;' >
                              <tr>
                                <td colspan='5'>";
                        
                            echo "<table style='width: 100%;'>
                                    <tr>
                                        <td style='width: 50%'><h2 style='color: #197198;'>Todos las solicitudes</h2></td>
                                        <td style='width: 50%; text-align: right;'>
                                            <form method='post' action=''>
                                                <input type='hidden' name='Enviar' value='Mis Solicitudes' />
                                                <input type='submit' name='All' value='$Value' class='boton' />
                                            </form>
                                        </td>
                                    </tr>
                                  </table>
                                  </td>
                              </tr>";
                        
                        echo "<tr>
                                  <th style='width: 20%'>Tipo de Solicitud</th>
                                  <th style='width: 15%'>Fecha de Inicio</th>
                                  <th style='width: 15%'>Fecha de Fin</th>
                                  <th style='width: 40%'>Motivo</th>
                                  <th style='width: 10%'>Estado</th>
                              </tr>";
                        
                        if($MisPermisos->rowCount() > 0)
                        {
                        
                           while($Permiso = $MisPermisos->fetch())
                           {
                                if($Permiso[8] == "Pendiente")                                                      
                                    $Estado = "<em style='color: black'>Pendiente</em>";
                                    
                                if($Permiso[8] == "Aceptada")
                                   $Estado = "<em style='color: blue'>Aceptada</em>";

                                if($Permiso[8] == "Denegada")
                                   $Estado = "<em style='color: red'>Denegada</em>";

                                                          
                                $Solicitud = "<form method='post' action='PermisoView.php' target='_blank'>
                                                 <input type='hidden' name='idp' value='$Permiso[0]' />
                                                 <input type='hidden' name='aux' value='View' />
                                                 <input type='submit' name='Enviar' style='width: 30px' class='botonG' value='Ver' />
                                              </form>";
                                                                                                                       
                                echo "<tr style='height: 50px;'>
                                          <td style='vertical-align: top;'>$Permiso[9]</td>
                                          <td style='vertical-align: top;'>$Permiso[3]</td>
                                          <td style='vertical-align: top;'>$Permiso[5]</td>
                                          <td style='text-align: left; padding-left: 10px; vertical-align: top;'>$Permiso[7]</td>
                                          <td style='vertical-align: top;'>$Estado </td>
                                          <td style='vertical-align: top;'>$Solicitud</td>
                                      </tr>";
                           } 
                       }
                       else
                            echo "<tr><th colspan='5'><p align='center' ><h2 style='color: red'>No hay Solicitudes</h2></p></th></tr>";
                            
                       echo " </td>
                              </tr>
                              </table>"; 
                              
                       /**
                       $Horarios = $bddr->prepare("SELECT * FROM Horario where IdEmpleado = $IdEmp and IdCargo = $IdCargosAux");
                       $Horarios->execute();
                       
                       if($Horarios->rowCount() > 0)
                       {                       
                           echo "<table style='width: 60%; text-align: center; padding-top: 50px'>
                                 <tr><th colspan='2'><strong style='font-size: 15pt'>Horario por Motivos de Estudio</strong></th></tr>
                                 <tr><th>D�a</th><th>Definici�n de Tiempos</th><th>Total de Horas</th></tr>";
                                                   
                           while($DataH = $Horarios->fetch())
                           {
                                $ArrayW = explode("{",$DataH[5]);
                                
                                echo "<tr><td colspan='3'><hr color='skyblue' /></td></tr>";
                                echo "<tr><td colspan='3'>Estado del Permiso: <strong>$DataH[6]</strong></td></tr>";
                                
                                foreach($ArrayW as $valueDay)
                                {
                                    $ArrayD = explode("[",$valueDay);
                                    
                                    for($i = 0; $i < count($ArrayD); $i++)
                                    {
                                        echo "<tr><th>" . $ArrayD[$i] . " </th>" ;                                    
                                        $i++;
                                        
                                        $ArrayT = explode("(",$ArrayD[$i]);
                                        
                                        foreach($ArrayT as $valueTime)                                    
                                            echo "<td>$valueTime</td>";
                                        
                                        echo "</tr>
                                              <tr><td colspan='3'></td></tr>";
                                        
                                    }    
                                }
                                
                           }                       
                           echo "</table>";
                       }
                       **/                                                                     
                    }

                    if($_POST['Enviar'] == "Ver Solicitudes")
                    {
                        $MisSolicitudes = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado, ca.IdAsignacion 
                                                           FROM Empleado as e
                                                           inner join CargosAsignacion as ca on ca.IdEmpleado = e.IdEmpleado 
                                                           inner join Permisos as p on p.IdAsignacionEmpleado = ca.IdAsignacion
                                                           INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                                                           INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk                                                                                                              
                                                           where p.IdAsignacionJefe = $Cargos
                                                           group by e.IdEmpleado");          
                        $MisSolicitudes->execute();
                        
                        if($MisSolicitudes->rowCount() > 0)
                        {
        
                           while($Permiso = $MisSolicitudes->fetch())
                           {
                               $Query = "SELECT COUNT(IdPermiso), Estado FROM Permisos 
                                         where (Estado = 'Aceptada' or Estado = 'Denegada') AND IdAsignacionEmpleado = $Permiso[8] 
                                         GROUP BY Estado
                                         ORDER BY Estado ASC";
                                          
                               $MisPermisosG = $bddr->prepare($Query);          
                               $MisPermisosG->execute();
                               $Aceptadas = $Denegadas = 0;
                               
                               while($DataPg = $MisPermisosG->fetch())
                               {
                                    if($DataPg[1] == "Aceptada")
                                        $Aceptadas = $DataPg[0];
                                        
                                    if($DataPg[1] == "Denegada")
                                        $Denegadas = $DataPg[0];
                               }
                                   
                            
                                echo "<table style='width: 100%; text-align: center;  border-style: solid; border-radius: 10px; border-color: #69ACD7; padding: 10px; margin-top:10px;'>
                                      <tr>
                                          <td style='width: 15%; padding: 3px;' class='tdleft'>Solicitante:</td><td style='text-align: left;'>$Permiso[0] $Permiso[1] $Permiso[2] $Permiso[3]</td>
                                          <td>Solicitudes Aceptados:</td><td style='text-align: left;'>$Aceptadas</td>
                                      </tr>
                                      <tr>
                                          <td class='tdleft' style='padding: 3px;'>Area:</td><td style='text-align: left;'>$Permiso[6]</td>
                                          <td>Solicitudes Denegados:</td><td style='text-align: left;'>$Denegadas</td> 
                                      </tr>
                                      <tr>
                                          <td class='tdleft' style='padding: 3px;'>Cargo:</td><td style='text-align: left;'>$Permiso[5]</td>
                                          <td colspan='2'>
                                            <form action='?l=PermissionsoDetails' method='post'>
                                                <input type='hidden' name='ide' value='$Permiso[8]' />
                                                <input type='submit' class='boton' value='Ver los Solicitudes' name='Enviar' style='width: 120px' />
                                            </form>
                                          </td>
                                      </tr>";

                               $MisPermisos = $bddr->prepare("SELECT * FROM Permisos 
                                                              where IdAsignacionEmpleado = $Permiso[8] and IdAsignacionJefe = $Cargos and Estado = 'Pendiente'
                                                              ORDER BY Fecha DESC ");          
                               $MisPermisos->execute();                                           
                            
                               echo "<tr><td colspan='4'><hr color='#69ACD7'  /></td></tr>
                                     <tr><td colspan='4'>
                                          <table style='width: 100%; text-align: center;'>
                                          <tr><th colspan='5' style='margin-top: 20px;'>Solicitudes Pendientes</th></tr>
                                          <tr><th>Tipo de Permiso</th><th>Fecha de Inicio</th><th>Fecha de Fin</th><th style='width: 40%'>Motivo</th><th>Estado</th></tr>";
                                                                              
                                       if($MisPermisos->rowCount() > 0)
                                       {
                                           while($Permiso = $MisPermisos->fetch())
                                           {
                                                          
                                                $Solicitud = "<form method='post' action='PermisoView.php'>
                                                                 <input type='hidden' name='idp' value='$Permiso[0]' />
                                                                 <input type='submit' name='Enviar' style='width: 30px' class='botonG' value='Ver' />
                                                              </form>";
                                                                         
                                                echo "<tr>
                                                          <td style=' width: 15%'>$Permiso[9]</td>
                                                          <td style=' width: 15%'>$Permiso[3]</td>
                                                          <td style=' width: 15%'>$Permiso[5]</td>
                                                          <td style='text-align: left; width:45%'>$Permiso[7]</td>
                                                          <td style='width: 10%'>$Solicitud </td>
                                                      </tr>";
                                           }
                                       }
                                       else
                                       {
                                            echo "<tr><td colspan='5' style='text-align: center;'>No hay Solicitudes pendientes</td></tr>";
                                       }
                                   echo "  </table>
                                      </td>
                                      </tr>
                                      </table>";        
                                                                      
                           }  
                        }
                        else
                            echo "<tr><th colspan='5'><p align='center' ><h2 style='color: red'>No hay Solicitudes</h2></p></th></tr>";
                                                                                                     
                    }
                }
                ?>
            </td>
        </tr>
    </table>
</div>