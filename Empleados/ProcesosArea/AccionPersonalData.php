<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

$id = $_GET['code'];
$id2 = $_GET['code2'];

require '../../net.php';

echo "<form action='Empleados/ProcesosArea/AccionPersonalABM.php' method='post'>
      <hr color='#69ACD7'  />
      <table>
      <tr>
          <td style='width:50%'>";
          
          
            $query = "SELECT ca.IdEmpleado, ca.Salario, a.IdAreaDeTrabajo, a.NombreAreaDeTrabajo, c.IdCargos, c.Cargo, ca.IdAsignacion
                      FROM CargosAsignacion AS ca               
                      INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo  
                      INNER JOIN AreasDeTrabajo as a ON a.IdAreaDeTrabajo = c.IdArea_Fk
                      Inner JOin Jerarquia as j on j.IdCargos_Fk = ca.IdCargo 
                      WHERE ca.IdEmpleado = '$id' AND ca.FechaFin =  '0000-00-00'";
        
            $CargoActual = $bddr->prepare($query);
                                           
            $CargoActual->execute();            
            $Datos = $CargoActual->fetch();
            
            echo "<table style='width:90%;border: 10px; border-color: skyblue; height: 125px;  margin-left: 5%;' rules='All'>
                    <tr><td colspan='2' style='text-align:center;'>Datos Actuales</td></tr>
                    <tr><td style='padding-left: 20px'>Cargo         </td><td style='padding-left: 20px'>". ( $Datos[3]). "</td></tr>
                    <tr><td style='padding-left: 20px'>&Aacute;rea   </td><td style='padding-left: 20px'>". ( $Datos[5]) . "</td></tr>
                    <tr><td style='padding-left: 20px'>Sueldo        </td><td style='padding-left: 20px'>$". number_format($Datos[1],2) . "</td></tr>                    
                  </table>";
            $pregunta = ('�Seguro de crear esta Acci&oacute;n de Personal?');
    echo "    
          <td style='width:50%'>
            <table style='width:90%;border: 10px; border-color: skyblue; height: 125px; margin-left: 5%;' rules='All'>
                    <tr><td colspan='2' style='text-align:center;'>Datos Propuestos</td></tr>
                    <tr><td style='padding-left: 20px'>&Aacute;rea   </td><td style='padding-left: 20px'><input type='text' id='cargoA' name='cargoA' value='". ( $Datos[3]). "' /></td></tr>
                    <tr><td style='padding-left: 20px'>Cargo         </td><td style='padding-left: 20px'><input type='text' id='cargoP' name='cargoP' value='". ( $Datos[5]) . "' /></td></tr>                    
                    <tr><td style='padding-left: 20px'>Sueldo        </td><td style='padding-left: 20px'><input type='text' id='cargoS' name='cargoS' value='". number_format($Datos[1],2) . "' onblur='Calcular()' /></td></tr>                    
            </table>
          </td>
       </tr>          
       <tr>
          <td colspan='2'>
            <table style='width: 100%; height: 30px; text-align: center; margin-top: 20px'>
                <tr>
                    <td>Salario Actual US$</td><td style='text-align: left'><input type='text' id='SA' style='width: 80px' readonly='true' name='SA' value='". number_format($Datos[1],2). "'  /> </td>
                    <td>Incremento US$    </td><td style='text-align: left'><input type='text' id='IS' style='width: 80px' readonly='true' name='IS' value='0.00'  /> </td>
                    <td>Nuevo Salario US$ </td><td style='text-align: left'><input type='text' id='NS' style='width: 80px' readonly='true' name='NS' value='0.00'  /> </td>
                </tr>
            </table>
          </td>
      </tr>
      <tr><td colspan='2'><hr color='#69ACD7'  /></td><tr>
      <tr><td colspan='2' style='padding-top: 20px; text-align: center'><h3>Acci&oacute;n que se propone</h3></td></tr>
      <tr>
          <td style='width:50%'>
            <table style='width: 90%; margin-left: 5%;'>
                <tr><td>1. Incremento Salarial</td><td><input name='Accion[]' type='checkbox' value='1' /></td></tr>
                <tr><td>2. Nivelaci&oacute;n de Salarial</td><td><input name='Accion[]' type='checkbox' value='2' /></td></tr>
                <tr><td>3. Promoci&oacute;n</td><td><input name='Accion[]' type='checkbox' value='3' /></td></tr>
                <tr><td>4. Felicitaci&oacute;n</td><td><input name='Accion[]' type='checkbox' value='4' /></td></tr>
                <tr><td>5. Traslado</td><td><input name='Accion[]' type='checkbox' value='5' /></td></tr>
                <tr><td>6. Ausencia por Beca</td><td><input name='Accion[]' type='checkbox' value='6' /></td></tr>
                <tr><td>7. Descuento </td><td><input name='Accion[]' type='checkbox' value='7' /></td></tr>
            </table>
          </td>
          <td style='width:50%'>
            <table style='width: 90%; margin-left: 5%;'>
                <tr><td>8. Amonestaci&oacute;n Verbal</td><td><input name='Accion[]' type='checkbox' value='8' /></td></tr>
                <tr><td>9. Amonestaci&oacute;n Escrita </td><td><input name='Accion[]' type='checkbox' value='9' /></td></tr>
                <tr><td>10. Suspensi&oacute;n Temporal</td><td><input name='Accion[]' type='checkbox' value='A' /></td></tr>
                <tr><td>11. Despido con indemnizaci&oacute;n</td><td><input name='Accion[]' type='checkbox' value='B' /></td></tr>
                <tr><td>12. Despido sin indemnizaci&oacute;n</td><td><input name='Accion[]' type='checkbox' value='C' /></td></tr>
                <tr><td>13. Finalizaci&oacute;n de Contrato</td><td><input name='Accion[]' type='checkbox' value='D' /></td></tr>
                <tr><td>14. Otros </td><td><input name='Accion[]' type='checkbox' value='E' /></td></tr>
            </table>
          </td>
      </tr>
      
      <tr>
            <td colspan='2' style='padding-top: 20px; text-align: center'><h3>Observaciones</h3>
                <textarea rows='5' cols='80' name='Observaciones' ></textarea>
            </td>
      </tr>
      
      <tr><td colspan='2' style='text-align: center'>
            <input type='submit' name='Enviar' value='Crear Accion de Personal' onclick=\"return confirm('$pregunta');\" class='botonG' style='width: 150px ' />
          </td>
      </tr>";
echo "<input type='hidden' name='idae' value='$Datos[6]' />
      <input type='hidden' name='idaj' value='$id2' />
      </table>
      </form>";

?>