<?php

/**
 * @author Lilian Cordero
 * @copyright 2018
 */
   
    if(isset($_POST['IdE']) && isset($_POST['IdC']))
    {
        $IDE = $_POST['IdE'];
        $IDC = $_POST['IdC'];
        $_POST['Eva'] = 2;
        if(isset($_POST['Eva']))
        {            
            $Eva = $bddr->prepare("Select * from Evaluacion_cuest where IdEvaluacion = " . $_POST['Eva']);                                                   
            $Eva->execute();
            $DataEva = $Eva->fetch();
            
            $_SESSION['IdEva'] = $_POST['Eva'];
            $_SESSION['eva'] = $DataEva[1];        
        }        
        
        
        
        $_SESSION['IdE'] = $IDE;
        $_SESSION['IdC'] = $IDC;
        $_POST['IdForm'] = 5;

        $FormularioAux = $bddr->prepare("Select * from Evaluacion_Formulario_cuest where IdFormulario = " . $_POST['IdForm']);
        $FormularioAux->execute();
            
        $DataFaux = $FormularioAux->fetch();
                
        $_SESSION['IdForm'] = $_POST['IdForm'];
                
        $DataEmpleados = $bddr->prepare("Select e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.* 
                                         from Empleado as e
                                         inner join CargosAsignacion as ca on ca.IdEmpleado = e.IdEmpleado
                                         inner join Cargos as c on ca.IdCargo = c.IdCargos  
                                         where e.IdEmpleado = $IDE and c.IdCargos = $IDC  and FechaFin = '0000-00-00'");
        $DataEmpleados->execute();                                
        $DatosEmpleados = $DataEmpleados->fetch();
        
        $Area = $bddr->prepare("Select * from AreasDeTrabajo where IdAreaDeTrabajo = $DatosEmpleados[8]");
        $Area->execute();
            
        $DataA = $Area->fetch();
    }
    else
        Redireccion("Empleado.php?l=Evaluation");                                               
  $_POST['IdEvaluador'] = $IDE;                            
   $TemasResultados = $bddr->prepare("SELECT t . * , f.Formulario FROM Evaluacion_Resultados_cuest AS r
                                      INNER JOIN Evaluacion_Preguntas_cuest AS p ON r.IdPregunta = p.IdPregunta
                                      INNER JOIN Evaluacion_Temas_cuest AS t ON p.IdTema = t.IdTema
                                      INNER JOIN Evaluacion_Formulario_cuest AS f ON t.IdFormulario = f.IdFormulario
                                      WHERE r.Evaluacion = '". $_SESSION['eva'] ."' and IdEmpleado = $IDE and IdAsignacionCargo = $IDC 
                                              AND f.IdFormulario = " . $_POST['IdForm'] ." and IdJefe = ".$_POST['IdEvaluador']."
                                      GROUP BY t.IdTema"); 
                                                                          
   $TemasResultados->execute();           
   
   $arrayTemas = array();
   $arrayPromedio = array();    
   $Formulario = "";        
                            
   while($DataT = $TemasResultados->fetch())   
        array_push($arrayTemas, $DataT[0]);

   

?>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">
<table style="width: 100%;">
    <tr><td style='text-align: right;'>
        <div style="float: left; width: 100px;">
            <form action="?l=ManagementEvaluations" method="post">
                <input type="hidden" class="boton" name="IdE" value="<?php echo $_POST['IdE'];?>"/>
                <input type="hidden" class="boton" name="Aux1" value="true"/>
                
                <input type="submit" class="boton" name="Enviar" value="<- Resumen Personal" style="width: 150px;"/>
            </form>
        </div>
        <h2>Evaluaci�n de Desempe�o</h2></td></tr>
    <tr>
        <td>   
        <?php
            echo "<table style='width: 94%; margin-left: 3%; '>
                    
                  <tr><td colspan='2' style='color: blue; '> Empleado evaluado</td><tr>
                  <tr><td class='tdleft' style='width: 30%'> Empleado: </td><td>$DatosEmpleados[1] $DatosEmpleados[2] $DatosEmpleados[3] $DatosEmpleados[4] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Area: </td><td>$DataA[1] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Cargo: </td><td> $DatosEmpleados[6] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Formulario evaluado:</td><td>$DataFaux[1]</td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Tipo de Evaluaci�n:</td><td><strong>" . $_SESSION['eva'] . "</strong></td></tr>
                  </table>"; 
        ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php

                   
            echo "<table style='width: 94%; margin-left: 3%;'>
                      <tr><td colspan='2'><br />
                          <table style='width: 100%;' >

                          <tr><td colspan='14'><hr color='skyblue' /></td></tr>
                          <tr><th style='width: 6%'>N�</th>
                              <th style='width: 30%'>Tema</th>
                              <th colspan='11' style='width:30%'></th>
                              <th style='width: 10%'>Promedio</th>
                          <tr><td colspan='14'><hr color='skyblue' /></td></tr>";
            
            $PromedioTotal = 0;
            $contador = 0;
            
            $ArraInstitucional = array();
            $ArraCTecnicas = array();
            $ArraCPersonal = array();
            
            
            if(count($arrayTemas) > 0)
            {
            
            foreach($arrayTemas as $value)
            {
               /* echo '$value '. $value. "<br>";
                echo '$_SESSION[eva] '.$_SESSION['eva']. "<br>";
                echo '$IDE '. $IDE. "<br>";
                echo '$IDC '.$IDC. "<br>";
                echo '$_POST[IdEvaluador] '.$_POST['IdEvaluador']. "<br>";*/
                 $Resultados  = $bddr->prepare("SELECT r.* , t.Temas  FROM Evaluacion_Resultados_cuest AS r
                                               INNER JOIN Evaluacion_Preguntas_cuest AS p ON r.IdPregunta = p.IdPregunta
                                               INNER JOIN Evaluacion_Temas_cuest AS t ON p.IdTema = t.IdTema
                                               where t.IdTema = $value and r.Evaluacion = '".$_SESSION['eva'] ."' and IdEmpleado = $IDE and IdAsignacionCargo = $IDC and IdJefe = ".$_POST['IdEvaluador']."");
                 $Resultados->execute();
                 $Promedio = 0;              
                 $TemaColocado = false;           
                 $contador++;
                 $tema = "";
                 $temaArray = array();
                 $promarray = array();
                 while($DataR = $Resultados->fetch())
                 {                  
                    if(!$TemaColocado)
                    {
                        echo "<tr style='height: 10px;' ><th>$contador</th><td>$DataR[8]</td>";
                        $TemaColocado = true;
                         

                    }
                    
                    $tema = $DataR[8];
                    echo "<td style='text-align: center; width: 6%;'>" . number_format($DataR[3],2) . "</td>";
                    
                    $Promedio = $Promedio + $DataR[3];
                    array_push($promarray, $Promedio / $Resultados->rowCount());
                 }
                 
                $Promedio = $Promedio / $Resultados->rowCount(); 
                 
                if($tema == "PLANIFICACI�N Y ORGANIZACI�N" || 
                   $tema == "CUMPLIMIENTOS DE METAS Y OBJETIVOS" || 
                   $tema == "COMPROMISO INSTITUCIONAL" || 
                   $tema == "VISI�N SOBRE NUEVOS PROYECTOS")
                        array_push($ArraInstitucional, number_format($Promedio,2));
                
                else if($tema == "CONOCIMIENTO DEL PUESTO" ||
                        $tema == "CALIDAD DE TRABAJO" || 
                        $tema == "CREATIVIDAD E INNOVACI�N" || 
                        $tema == "M�TODOS DE TRABAJO" ||
                        $tema == "PENSAMIENTO ESTRAT�GICO" ||
                        $tema == "CANTIDAD DE TRABAJO")
                        array_push($ArraCTecnicas, number_format($Promedio,2));
                
                else if($tema == "SALESIANIDAD" ||
                        $tema == "RELACIONES INTERPERSONALES" || 
                        $tema == "TRABAJO EN EQUIPO" || 
                        $tema == "RESPONSABILIDAD Y DISCRECI�N" || 
                        $tema == "ESP�RITU DE SERVICIO" || 
                        $tema == "ACTITUD DE SERVICIO AL CLIENTE" || 
                        $tema == "HABILIDAD PARA TOMAR DECISIONES" ||                                                                 
                        $tema == "DIRECCI�N Y DESARROLLO DE LOS COLABORADORES")
                        array_push($ArraCPersonal, number_format($Promedio,2));                 
                 
                 $cols = 11 - $Resultados->rowCount();

                 for ($i=0; $i < $cols; $i++) { 
                      echo "<td style='text-align: center;'>-</td>";
                 }
                 
                 echo "<td style='text-align: center;'><strong>" . number_format($Promedio,2) . "</strong></td></tr>";
                 array_push($arrayPromedio, number_format($Promedio,2));
                 
                 $PromedioTotal = $PromedioTotal + $Promedio;           
            }            
            
            $PromedioTotal = $PromedioTotal / count($arrayTemas);
            
            }
            else
                echo "  <tr><td colspan='6' style='text-align: center'><h2 style='color: red'>No hay Factores de evaluacion examinados</h2></td></tr>";
            
            echo "  <tr><td colspan='14'><hr color='skyblue' /></td></tr>
                    <tr><td colspan='4'></td>
                        <td colspan='9' class='tdleft' ><strong>Promedio Total: </strong> </td><td style='text-align: center;'><strong>" . number_format($PromedioTotal,2) . "</strong></td></tr>
                    </table>
                    </td></tr>
                </table>";
            ?>
        </td>        
    </tr>
    <?php
            
           $verificacion = $bddr->prepare("Select * from Evaluacion_Estado where IdEvaluacion = ".$_SESSION['IdEva']." and IdEmpleado = $IDE and IdCargo = $IDC and IdEvaluador  = ".$_POST['IdEvaluador']);
           $verificacion->execute();

           $DataV = $verificacion->fetch();
            
           $idJefe = $DataV[6];
           $idEvaluado = $DataV[4]; 
           $idEstado =  $DataV[0];
            
           echo "<tr>
                     <td>
                         <table style='width: 80%; margin-left: 10%;'>
                            <tr>
                                <td colspan='2'><strong style='color: blue;'>Comentario del Evaluador:</strong></td>
                            </tr>
                            <tr>
                                <td colspan='2'><em style='color: blue;'>$DataV[8]</em><br /><br /></td>
                            </tr>
                         </table>
                     </td>
                </tr>";
           
           
           $IdEvaluaciton = $_SESSION['IdEva']; // Id de la Evaluaci�n de Desempe�o
           $verificacionComentario = $bddr->prepare("Select * from Evaluacion_Comentarios as c
                                                     inner join Evaluacion_Estado as e
                                                     where c.IdEstado = $idEstado and e.IdEvaluacion = $IdEvaluaciton");
           $verificacionComentario->execute();
           
           $DataComent = $verificacionComentario->fetch();
        
            if($verificacionComentario->rowCount() == 0)
            {
                echo "<tr>
                          <td>
                                <table style='width: 80%; margin-left: 10%;'>
                                    <form action='Empleados/Evaluacion/EvaluationABM.php' method='post'>
                                    <input type='hidden' class='boton' name='IdEstado' value='$idEstado'/>                                       
                                    <tr>
                                        <td colspan='2'>Conformidad de la Evaluaci�n:</td>
                                    </tr>
                                    <tr>
                                        <td colspan='2'>
                                            <textarea style='width: 80%; margin-left: 10%;' name='Comentario'></textarea>
                                    </tr>
                                    <tr>                                            
                                        <td style='text-align: center'><input type='submit' class='boton' name='Enviar' value='Conforme'/></td>
                                        <td style='text-align: center'><input type='submit' class='botonE' name='Enviar' value='Inconforme'/></td>
                                    </tr>
                                    <form>
                                </table>
                          </td>
                      </tr>";
            }
            else
            {
                echo "<tr>
                          <td>
                             <table style='width: 80%; margin-left: 10%;'>
                                <tr>
                                    <td colspan='2'><strong style='color: blue;'>Tu Conformidad con respecto a la Evaluaci�n es: </strong>
                                    <br /><em style='color: blue;'>$DataComent[3]</em><br /><br /></td>                                        
                                </tr>
                                <tr>
                                    <td colspan='2'><strong style='color: blue;'>Comentario del Empleado evaluado: </strong> 
                                    <br /><em style='color: blue;'>$DataComent[2]</em><br /><br /></td>
                                </tr>                                    
                             </table>
                          </td>
                      </tr>";
             }   
             
             foreach ($temaArray as $prom) {
                                      echo $prom.",";
                                    }                                             
    ?>

    <tr>
        <td><br />
            <script src="js/highcharts.js"></script>
            <script src="js/exporting.js"></script>
            
            <div id="container" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
            <script>

            $(function () {
                $('#container').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Promedios de Evaluaci�n de Desempe�o personal'
                    },
                    subtitle: {
                        text: 'Fuente: Sistema de Informaci�n Institucional de Polideportivos Don Bosco'
                    },
                    xAxis: {
                        type: 'category',
                        labels: {   
                            rotation: 0,

                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Nota de Evaluaci�n'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Nota de Evaluaci�n',
                    },
                    credits: {
                        enabled: false
                    },    
                    series: [{
                        name: 'Competencias Institucionales',
                        data: [        
                              <?php
                                   /* $i = 0;
                                    foreach($ArraInstitucional as $value)
                                    {
                                        $i++;
                                        if(count($ArraInstitucional) == $i)
                                            echo "['Criterio A$i', $value]";
                                            
                                        else
                                            echo "['Criterio A$i', $value],";
                                    }
                                    
                                    echo ",[' ', null],";
                                    
                                    $i = 0;
                                    foreach($ArraCTecnicas as $value)
                                    {
                                        $i++;
                                        if(count($ArraCTecnicas) == $i)
                                            echo "['Criterio B$i', $value]";
                                            
                                        else
                                            echo "['Criterio B$i', $value],";
                                    }   
                                    
                                    echo ",[' ', null],";   
                                    
                                    $i = 0;
                                    foreach($ArraCPersonal as $value)
                                    {
                                        $i++;
                                        if(count($ArraCPersonal) == $i)
                                            echo "['Criterio C$i', $value]";
                                            
                                        else
                                            echo "['Criterio C$i', $value],";
                                    }  */
                                    $temaArray = array('COMUNICACI�N',
                                      'CONFLICTO Y COOPERACI�N',
                                      'CONFORT',
                                      'ESTRUCTURA',
                                      'IDENTIDAD',
                                      'FLEXIBILIDAD E INNOVACI�N',
                                      'LIDERAZGO',
                                      'MOTIVACI�N',
                                      'REMUNERACI�N',
                                      'RELACIONES SOCIALES');
                                    $i =0;
                                    foreach ($arrayPromedio as $prom) {
                                      echo "['$temaArray[$i]', $prom],";
                                      $i++;
                                    }  
                                    
                                                           
                              ?>
                        ],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            x: 4,
                            y: 10,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif',
                                textShadow: '0 0 3px black'
                            }
                        }
                    }]
                });
            });
            
            </script>
            <table style="width: 50%; margin-left: 25%;">
            <tr><th colspan="2">Factores de Evaluaci�n</th></tr>
            <tr><td>Criterio A</td> <td> Competencias Institucionales</td></tr>
            <tr><td>Criterio B</td> <td> Competencias T�cnicas</td></tr>
            <tr><td>Criterio C</td> <td> Competencias Personales</td></tr>
            </table>
        </td>
    </tr>
    <tr>
      <td colspan='2'>
      <br />
      <hr color='skyblue' />
      <br />
      <!--      
        <table style='width: 100%; text-align: center;'>
        <form action="Empleados/Evaluacion/EvaluationABM.php" method="Post">
              <input type='hidden' name='Eva' value='<?php echo $_SESSION['eva'];?>' />
              <input type='hidden' name='IdE' value='<?php echo $idEvaluado;?>' />
              <input type='hidden' name='IdC' value='<?php echo $IDC;?>' />
              <input type='hidden' name='IdJ' value='<?php echo $idJefe;?>' />                                          
        <tr><td colspan="2" style="color: blue;"><br />Verificaci�n de Evaluaci�n de Desempe�o<br /><br /></td></tr>
        <tr><td><input type='submit' style='text-align: center; width: 130px;' class='boton' name="Enviar" value='Ver Evaluaci�n' /></td></tr>
        </form>
        </table>        
      </td>
      --!>
  </tr>
</table>
</div>