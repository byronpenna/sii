<?php


   
    if((isset($_POST['IdE']) && isset($_POST['IdC'])))
    {
        $IDE = $_POST['IdE'];
        $IDC = $_POST['IdC'];
        $IdFor = $_POST['IdForm'];
        $Ideva = $_POST['IdEvaluacion'];
        
        
        $_SESSION['IdE'] = $IDE;
        $_SESSION['IdC'] = $IDC;
        $IDF = $_SESSION['IdForm'];
        
        
        $Evaluacion = $bddr->prepare("Select * from Evaluacion_cuest where IdEvaluacion = $Ideva");
        $Evaluacion->execute(); 
        $DataEv = $Evaluacion->fetch();       
        $_SESSION['eva'] = $DataEv[1];
        $_SESSION['ideva'] = $Ideva;
        
 
            
                
        $DataEmpleados = $bddr->prepare("Select e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.* 
                                         from Empleado as e
                                         inner join CargosAsignacion as ca on ca.IdEmpleado = e.IdEmpleado
                                         inner join Cargos as c on ca.IdCargo = c.IdCargos  
                                         where e.IdEmpleado = $IDE and c.IdCargos = $IDC  and FechaFin = '0000-00-00'");
        $DataEmpleados->execute();                                
        $DatosEmpleados = $DataEmpleados->fetch();
        
        $Area = $bddr->prepare("Select * from AreasDeTrabajo where IdAreaDeTrabajo = $DatosEmpleados[8]");
        $Area->execute();
            
        $DataA = $Area->fetch();
    }
    else
        Redireccion("Empleado.php?l=Evaluations");       

?>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px"> 
    <table style="width: 100%; ">
    
    <?php


            echo "<tr><td style='text-align: right;'><h2>Evaluaci&oacute;n Clima Laboral</h2></td></tr>
                  <tr>
                    <td>
                        <table style='width: 94%; margin-left: 3%; '>
                        <tr><td colspan='2' style='color: blue; '> Empleado a Evaluar</td><tr>
                        <tr><td class='tdleft'> Empleado: </td><td>$DatosEmpleados[1] $DatosEmpleados[2] $DatosEmpleados[3] $DatosEmpleados[4] </td></tr>
                        <tr><td class='tdleft'> Area: </td><td>$DataA[1] </td></tr>
                        <tr><td class='tdleft'> Cargo: </td><td> $DatosEmpleados[6] </td></tr>
                        <tr><td class='tdleft'> Evaluaci&oacute;n: </td><td> " . $_SESSION['eva'] ."</td></tr>
                        <tr><td colspan='2'><hr color='skyblue' /></td></tr>";
            
           $Formularios = $bddr->prepare("Select * from Evaluacion_Formulario_cuest where IdFormulario = 5");//$IdFor\");
           $Formularios->execute();
           $DataF = $Formularios->fetch();
                
           echo "       <form action='Empleados/Evaluacion_cuest/EvaluationABM.php' method='Post'>
                        <tr><td colspan='2' style='color: blue;'>Formulario de Evaluaci&oacute;n</td></tr>
                        <tr><td class='tdleft'>Formulario:</td>
                            <input type='hidden' name='IdC' value='$IDC' />
                            <input type='hidden' name='IdE' value='$IDE' />
                            <input type='hidden' name='IdForm' value='$DataF[0]' />                             
                            <td>$DataF[1]</td>
                        </tr>
                        <tr><td colspan='2'><hr color='skyblue' /></td></tr>
                        <tr><td colspan='2' style='color: blue'>Instrucciones</td></tr>
                        <tr>
                            <td colspan='2'>
                                El presente cuestionario tiene como objetivo realizar el estudio del Clima Laboral. Tenga en cuenta que su opini&oacute;n servir&aacute; y permitir&aacute; mejorar la gesti&oacute;n del talento humano de la fundaci&oacute;n. <br /><br />
                                Antes de responder, debe tener en cuenta lo siguiente: <br><br>
                                <ol style='margin-left: 60px;'>
                                    <li>- El cuestionario es an&oacute;nimo y confidencial. </li>
                                    <li>- Es importante responder de manera franca y honesta </li>
                                    <li>- Enfoque su atenci&oacute;n en lo que sucede habitualmente en la fundaci&oacute;n, puede pensar en los &uacute;ltimos tres meses de trabajo.</li>
                                    <li>- Llenar el cuestionario con bol&iacute;grafo</li>
                                    <li>- Tener en cuenta que se tiene una sola opci&oacute;n para llenar por cada una de las preguntas o enunciados.</li>
                                    <li>- Aseg&uacute;rese de responder todas las preguntas o enunciados.</li>
                                    <li>- Responder posicion&aacute;ndose en alguna de las opciones que se presentan, dibujando (encerrando) un circulo en el enunciado que indique lo que usted percibe en su ambiente de trabajo.</li>
                                </ol><br>
                                La informaci&oacute;n ser&aacute; recogida y analizada por el comit&eacute; de evaluaci&oacute;n  del Clima Laboral de su organizaci&oacute;n.
                                <br><br>
                                A continuaci&oacute;n presentamos un ejemplo de llenado:
                                <br><br>
                                \"Mi jefe esta disponible cuando se le necesita\"<br>
<br>
                                <table class=\"ta\">
    <tr class=\"trow\">
        <th class=\"thead\">1</th>
        <th class=\"thead\">2</th>
        <th class=\"thead\">3</th>
        <th class=\"thead\">4</th>
        <th class=\"thead\">5</th>
    </tr>
    <tr class=\"trow\">
        <td class=\"tddd\">Totalmente en desacuerdo <b>(TE)</b></td>
        <td class=\"tddd\">En desacuerdo <b>(ED)</b></td>
        <td class=\"tddd\">Ni en acuerdo Ni en desacuerdo <b>(NDNED)</b></td>
        <td class=\"tddd\">De acuerdo <b>(DA)</b></td>
        <td class=\"tddd\">Totalmente de Acuerdo <b>(TDA)</b></td>
    </tr>
</table>
<br>
Agradecemos anticipadamente su colaboraci&oacute;nn.

<style>
.ta {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.tddd, .thead {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
  width: 20%;
  text-align: center;
}

.thead {
  background-color: #dddddd;
  border: 1px solid silver;
}
</style>
                            </td>

                        </tr>
                        <tr style='margin-top: 20px;'><td colspan='2' style='text-align: center;'><input type='submit' value='Ver Evaluaci&oacute;n' name='Enviar' class='botonG' /></td></tr>
                        </form>
                        </table>
                    </td>
                  </tr>";
    ?>
    </table>
    <br /><br />
 </div>