<?php
/**
 * @author Calder�n
 * @copyright 2014
 */
 
    if(isset($_POST['Evaluacion']))
    {
        $_SESSION['IdEva'] = $_POST['Evaluacion'];
        //$default = '';
    }
     else{$_POST['Evaluacion'] = 2; } 
?>


<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px"> 
    <table style="width: 100%;">
        <tr><td><h2>Gesti�n de Evaluaciones</h2></td></tr>
        <?php
	                      
            $Evaluacion = $bddr->prepare("Select * from Evaluacion_cuest");
            $Evaluacion->execute();                                                  
        ?>
        <tr>
        <form method="post" action="">
            <td style="padding-left: 20px; width: 50%;">Seleccione la evaluaci�n que desea observar:</td>
            <td style=" width: 40%;"><select name="Evaluacion" style="width: 60%;" >
            
                <?php
	                   while($DataE = $Evaluacion->fetch())
                       {
                            if(isset($_POST['Evaluacion']))
                            {
                                if($_POST['Evaluacion'] == $DataE[0])
                                    echo "<option value='$DataE[0]' selected>$DataE[1]</option>";
                                    
                                else
                                    echo "<option value='$DataE[0]'>$DataE[1]</option>";
                            }
                            else
                            {
                                if($_SESSION['IdEva'] != "")
                                {
                                    if($_SESSION['IdEva'] == $DataE[0])
                                        echo "<option value='$DataE[0]' selected>$DataE[1]</option>";
                                    
                                    else
                                        echo "<option value='$DataE[0]'>$DataE[1]</option>";   
                                }
                                else                                
                                    echo "<option value='$DataE[0]'>$DataE[1]</option>";
                            }
                                
                       }
                ?>
                </select>
            </td>
            <td><input type="submit" value="Ver" style="width: 50px;" class="boton" /></td>
        </form>
        </tr>  
        <tr>
        <td colspan="3">
            <hr style="width: 100%;" color='skyblue' />
            <table style="width: 90%; margin-left: 5%;">
            <?php
               
               if(isset($_POST['Evaluacion']) && !isset($_POST['ia']))
               {
                    $Areas = $bddr->prepare("SELECT a.* , COUNT(ca.IdCargo) FROM AreasDeTrabajo AS a
                                             INNER JOIN Cargos AS c ON c.IdArea_Fk = a.IdAreaDeTrabajo
                                             INNER JOIN CargosAsignacion AS ca ON ca.IdCargo = c.IdCargos
                                             WHERE ca.FechaFin =  '0000-00-00'
                                             GROUP BY a.IdAreaDeTrabajo");
                    $Areas->execute();                                           
                                             
                    echo "<tr><th>Abreviatura</th><th>Nombre de Area</th><th>N� de Cargos</th><th></th></tr>";
                    while($DataA = $Areas->fetch())
                    {
                        echo "<tr><td>$DataA[2]</td><td>$DataA[1]</td><td style='text-align: center'>$DataA[3]</td>
                                  <td>
                                    <form method='post' action=''>
                                    <input type='hidden' value='$DataA[0]' name='ia' />
                                    <input type='submit' value='Detalles' name='Enviar' class='boton'>
                                    </form>
                                  </td>
                                  <td>
                                    <form action='https://siifusalmo.org/Empleado.php?l=ResultAreas' method='Post'>  
                                      <input type='hidden' value='$DataA[0]' name='ia' />                                   
                                      <input type='submit' name='Enviar' value='Resultados' class='boton'/>
                                    </form>
                                  </td>
                              </tr>";
                    }
                
               }
               else
               {
                    if(isset($_POST['ia']))
                    {
                        $GetDatos=$bddr->prepare('SELECT NombreAreaDeTrabajo FROM AreasDeTrabajo where IdAreaDeTrabajo = ' . $_POST['ia']);	
                        $GetDatos->execute();
                        
                        $Nombre = $GetDatos->fetch();
                    
                    	$GetDatos=$bddr->prepare('SELECT * FROM Cargos where IdArea_Fk = '. $_POST['ia'] . ' ORDER BY Cargo ASC');	
                        $GetDatos->execute();
                        
                        echo "<h2 style='color: blue'>$Nombre[0]</h2>";
                    	if ($GetDatos->rowCount()>0)
                        {
                            echo "
                                  <table style='padding: 5px; text-align:left ;width:100%'>
                                  <tr><th style='width: 35%;'>Nombre del Cargo</th>
                                      <th style='width: 30%;'>Asignado a</th>
                                      <th style='width: 15%;'>Solicitudes de<br /> Evaluaciones</th>
                                  </tr>
                                  <tr><td colspan='4'></td></tr>";
                            
                            while($Cargos = $GetDatos->fetch())
                            {
                    
                                $GetEmpleado =$bddr->prepare('SELECT ca . * , em.Nombre1, em.Nombre2, em.Nombre3, em.Apellido1, em.Apellido2, em.IdEmpleado
                                                              FROM CargosAsignacion as ca
                                                              INNER JOIN Empleado AS em ON ca.IdEmpleado = em.IdEmpleado 
                                                              where ca.IdCargo = ' .$Cargos[0]. ' and ca.FechaFin = \'0000-00-00\'' );	
                                $GetEmpleado->execute();
                                
                                if($GetEmpleado->rowCount() > 0)
                                {
                                    $nombres = $GetEmpleado->fetch();
                                    $iem = $nombres[13];
                                    $asignado = "<em style='color: blue;'>" . $nombres[8] . " " . $nombres[9] . " " . $nombres[11]. "</em>";
                                }
                                
                                else
                                    $asignado = "<em style='color: red;'>No asignado Actualmente</em>";
                                    
                                    
                                    
                                $EncargadoArea = $bddr->prepare("Select * from AreasDeTrabajoJefes where IdArea = " . $_POST['ia'] . " and IdCargoJefe  = $Cargos[0]");
                                $EncargadoArea->execute();
                                
                                if($EncargadoArea->rowCount() > 0)
                                    $AuxCargo = "<em style='color: green'>$Cargos[1]</em>";
                                
                                else
                                    $AuxCargo = "$Cargos[1]";
                                    
                                
                                echo "<tr><td>$AuxCargo</td><td>$asignado</td>";
                                
                                $Solicitudes = $bddr->prepare("SELECT * FROM Evaluacion_Estado_cuest where IdEmpleado = $iem and IdEvaluacion = " . $_SESSION['IdEva'] );
                                $Solicitudes->execute();    
                                


                                echo "   <th style='text-align: center;'> ". $Solicitudes->rowCount() . "</th>
                                         <td>
                                              <form action='Empleados/Evaluacion_cuest/GestionEvaluacionABM.php' method='Post'>
                                                    <input type='hidden' name='IdPosition' value='$Cargos[0]' />
                                                    <input type='hidden' name='IdE' value='$iem' />
                                                    <input type='hidden' name='ia' value='".$_POST['ia'].  "' />
                                                    <input type='hidden' name='TipoEvaluacion' value='Autoevaluaci�n' />
                                                    <input type='hidden' name='IdFormulario' value='Clima Laboral' />";
                                if ($Solicitudes->rowCount() == 0) {
                                  echo "<input type='submit' name='Enviar' value='Asignar Evaluador' class='boton'/>
                                              </form>
                                          </td>
                                          <td>
                                            <form action='https://siifusalmo.org/Empleado.php?l=Results' method='Post'>                                     
                                              <input type='hidden' name='IdE' value='$iem'>
                                              <input type='hidden' name='IdC' value='$Cargos[0]'>
                                              <input type='submit' name='Enviar' value='Ver Resultados' class='boton'/>
                                            </form>
                                          </td>
                                      </tr>";
                                }else{
                                  echo "<input type='submit' name='Enviar' value='Quitar' class='boton'/>
                                              </form>
                                          </td>
                                          <td>
                                            <form action='https://siifusalmo.org/Empleado.php?l=Results' method='Post'>                                     
                                              <input type='hidden' name='IdE' value='$iem'>
                                              <input type='hidden' name='IdC' value='$Cargos[0]'>
                                              <input type='submit' name='Enviar' value='Ver Resultados' class='boton'/>
                                            </form>
                                          </td>
                                      </tr>";
                                }
                                
                                                                                         


                            }
                            echo "</table>";
                        }
                        else
                            echo "<center><em style='color: red'>No hay cargos registrados en el sistema<br />
                                          <a href='Empleado.php?l=PositionForm' style='text-decoration: none; color: red'>Clic aqui para registrar</a></em>
                                  </center>";
                    }
                    else{
                        echo "<center><em style='color: blue'>Esperando datos...</em></center>";

                    }

               }
                    
            ?>
            </table>
        </td>            
        </tr>  
    </table>
</div>

