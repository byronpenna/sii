<?php

/**
 * @author Lilian Anabel Cordero
 * @copyright 2014
 */

if(isset($_POST['ia'])){
    $IdA = $_POST['ia'];

}
else
    Redireccion("Empleado.php?l=AllEvaluationss");
    

    unset($_SESSION['Numero']);
    unset($_SESSION['IdC']);
    unset($_SESSION['IdE']);
    unset($_SESSION['IdForm']);       
    
?>

<style type="text/css">
    .text-rotation 
    {
      -webkit-transform: rotate(-90deg);
      -moz-transform: rotate(-90deg);
      -ms-transform: rotate(-90deg);
      -o-transform: rotate(-90deg);
      transform: rotate(-90deg);
    
      /* also accepts left, right, top, bottom coordinates; not required, but a good idea for styling */
      -webkit-transform-origin: 50% 50%;
      -moz-transform-origin: 50% 50%;
      -ms-transform-origin: 50% 50%;
      -o-transform-origin: 50% 50%;
      transform-origin: 50% 50%;
      
      -font-size: 40px;
      -background: #e0e0e0;
      -width: 100px;
      -position: relative;
      -top: 50px;
      -width: 200px;
    }
</style>

<div style="background-color: white; border-radius: 10px;">
    <h2 style="text-align: right; padding-right: 20px;">Evaluacion Clima Laboral</h2>
    <table style="width: 80%;">
        <tr>
            <td style="width: 50%; text-align: left;">
                <form action="?l=ManagementEvaluations" method="post">
                    <input type="hidden" name="ia" value="<?php echo $IdA;?>" />
                    <input type="submit" name="Enviar" class="boton" value="<- Regresar" />
                </form>
            </td>
            <td style="width: 50%; text-align: right;">
                <form action="Empleados/Evaluacion_cuest/EvaluationAreaResultPrint.php" method="post" target="_blank">
                    <input type="hidden" name="ia" value="<?php echo $IdA;?>" />
                    <input type="submit" name="Enviar" class="botonG" value="Imprimir" />
                </form>
            </td>            
        </tr>
    </table>
    <br />
    <!-- Array Empleados -->
    <table style="width: 80%; border: 1px;" rules='all'>
        <tr style="height: 160px">
            <td colspan="2" style="text-align: center; border-right-width:3px; width: 70%;"><h2>Empleados</h2></td>
            <td style="text-align: center;" >Nota Evaluación</td>
            
            
        </tr>
        <?php
                
             $EmpleadosA = $bddr->prepare("SELECT e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, c.IdCargos, c.Cargo
                                             FROM Empleado AS e
                                             inner join CargosAsignacion as ca on e.IdEmpleado = ca.IdEmpleado 
                                             inner join Cargos as c on c.IdCargos = ca.IdCargo
                                             inner join AreasDeTrabajo as a on c.IdArea_Fk = a.IdAreaDeTrabajo
                                             where a.IdAreaDeTrabajo = $IdA and FechaFin = '0000-00-00'");
                $EmpleadosA->execute();                
                $ArrayPromedio = array();
                
                $ArrayTotalAutoevaluacion = array();
                $ArrayTotalClienteInterno = array();
                $ArrayTotalColegaTrabajo = array();
                $ArrayTotalColaborador = array();
                $ArrayTotalJefeInmediato = array();
                
                $ArrayTotalArea = array();                 
                
                while($DataE = $EmpleadosA->fetch())
                {
                    $ArrayAutoevaluacion = array();
                    $ArrayClienteInterno = array();
                    $ArrayColegaTrabajo = array();
                    $ArrayColaborador = array();
                    $ArrayJefeInmediato = array();
                    
                    $ArrayAutoevaluacionJ = array();
                    $ArrayClienteInternoJ = array();
                    $ArrayColegaTrabajoJ = array();
                    $ArrayColaboradorJ = array();
                    $ArrayJefeInmediatoJ = array();   
        
                    echo "<tr style='height: 53px;'><td style='padding-left: 10px; width: 150px'>$DataE[1] $DataE[2] $DataE[3]</td><td style='padding-left: 10px; width: 150px; border-right-width: 3px;'>$DataE[5]</td>";
                    
                    
                    /** Proceso de analisis de las notas **/
                    $IDE = $DataE[0];
                    $IDC = $DataE[4];
                    


                    $Evaluadores = $bddr->prepare("Select * from Evaluacion_Estado_cuest WHERE IdEmpleado = $IDE ORDER BY TipoEvaluacion ASC ");
                    $Evaluadores->execute();
                    
                    while($DataEvaluador = $Evaluadores->fetch())  
                    {                                 
                           $TemasResultados = $bddr->prepare("SELECT t . * , f.Formulario FROM Evaluacion_Resultados_cuest AS r
                                                              INNER JOIN Evaluacion_Preguntas_cuest AS p ON r.IdPregunta = p.IdPregunta
                                                              INNER JOIN Evaluacion_Temas_cuest AS t ON p.IdTema = t.IdTema
                                                              INNER JOIN Evaluacion_Formulario_cuest AS f ON t.IdFormulario = f.IdFormulario
                                                              WHERE r.Evaluacion = 'Clima Laboral 2018' and IdEmpleado = $IDE and IdAsignacionCargo = $IDC 
                                                              AND f.IdFormulario = 5 and IdJefe = $DataEvaluador[6] 
                                                              GROUP BY t.IdTema");   
                           $IdForm = $DataEvaluador[3];                                                   
                                                                                                  
                           $TemasResultados->execute();                       
                           
                           unset($arrayTemas);
                           unset($arrayPromedio);
                           
                           $arrayTemas = array();
                           $arrayPromedio = array();            
                                                
                           while($DataT = $TemasResultados->fetch())   
                                array_push($arrayTemas, $DataT[0]);
                            
                                       
                                
                           $PromedioTotal = 0;
                           $contador = 0;
                            
        
                            
                            //echo count($arrayTemas);
                           if(count($arrayTemas) > 0)
                           {
                            
                               foreach($arrayTemas as $value)
                               {
                                     $Resultados  = $bddr->prepare("SELECT r.* , t.Temas  FROM Evaluacion_Resultados_cuest AS r
                                                                   INNER JOIN Evaluacion_Preguntas_cuest AS p ON r.IdPregunta = p.IdPregunta
                                                                   INNER JOIN Evaluacion_Temas_cuest AS t ON p.IdTema = t.IdTema
                                                                   where t.IdTema = $value and r.Evaluacion = 'Clima Laboral 2019' and IdEmpleado = $IDE and IdAsignacionCargo = $IDC and IdJefe = $DataEvaluador[6] ");
                                     $Resultados->execute();
                                     $Promedio = 0;              
                                     $TemaColocado = false;           
                                     $contador++;
                                     $tema = "";
                                     
                                     while($DataR = $Resultados->fetch())
                                     {                   
                                        if(!$TemaColocado)                            
                                            $TemaColocado = true;   
            
                                        $tema = $DataR[8];                            
                                        $Promedio = $Promedio + $DataR[3];
                                     }
                                     
                                     $Promedio = $Promedio / $Resultados->rowCount(); 
                                     array_push($arrayPromedio, number_format($Promedio,2));
                                     
                                     $PromedioTotal = $PromedioTotal + $Promedio;           
                               }            
                            
                                $PromedioTotal = $PromedioTotal / count($arrayTemas);                
                           }
                            
                            if($DataEvaluador[2] == "Autoevaluación")
                            {
                                array_push($ArrayAutoevaluacion, number_format($PromedioTotal,2));
                                array_push($ArrayAutoevaluacionJ, $DataEvaluador[6]);                        
                            }    
                            if($DataEvaluador[2] == "Evaluación - Cliente interno")
                            {
                                array_push($ArrayClienteInterno, number_format($PromedioTotal,2));
                                array_push($ArrayClienteInternoJ, $DataEvaluador[6]);
                            }    
                            if($DataEvaluador[2] == "Evaluación - Colega de trabajo")
                            {
                                array_push($ArrayColegaTrabajo, number_format($PromedioTotal,2));
                                array_push($ArrayColegaTrabajoJ, $DataEvaluador[6]);
                            }    
                            if($DataEvaluador[2] == "Evaluación - Colaborador")
                            {
                                array_push($ArrayColaborador, number_format($PromedioTotal,2));
                                array_push($ArrayColaboradorJ,  $DataEvaluador[6]);
                            }    
                            if($DataEvaluador[2] == "Evaluación - Jefe inmediato")
                            {
                                array_push($ArrayJefeInmediato, number_format($PromedioTotal,2));
                                array_push($ArrayJefeInmediatoJ, $DataEvaluador[6]);
                            }
                                                                                                                                   
                    }
                    
                    $ArrayPromedioTotal = array();
                    $ContadorTotal = 0;
                                    
                    
                    
                    $PromedioAux = 0;
                    $ContadorA = 0;
                    $i = 0;
                    
                    foreach($ArrayAutoevaluacion as $value)
                    {
                        $i++;                                    
                        if($value > 0)
                        {
                            $PromedioAux += $value;
                            $ContadorA++;
                        }    
                    }  
                                      
                    if(count($ArrayAutoevaluacion) == 0)
                    {
    
                        array_push($ArrayPromedioTotal, "-");
                    }
                    else
                    {
                        if($ContadorA == 0)
                        $ContadorA = 1;                                        
                        $PromedioAux = $PromedioAux / $ContadorA;
                              
                        if($PromedioAux != 0)
                        {
                            //$ContadorTotal++;
                            array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                        }
                        
                        else
                            array_push($ArrayPromedioTotal, "-");
                    }
    
                    
                    
                    $PromedioAux = 0;
                    $ContadorA = 0;
                    $i = 0;
    
                    foreach($ArrayClienteInterno as $value)
                    {
                        
                        $i++;
                        if($value > 0)
                        {
                            $PromedioAux += $value;
                            $ContadorA++;
                        }                                           
                    }
                                          
                    if(count($ArrayClienteInterno) == 0)
                    {
                        array_push($ArrayPromedioTotal, "-");
                    }
                    else
                    {
                        if($ContadorA == 0)
                        $ContadorA = 1;
                                            
                        $PromedioAux = $PromedioAux / $ContadorA;
                              
                        if($PromedioAux != 0)
                        {
                            $ContadorTotal++;
                            array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                        }
                        
                        else
                            array_push($ArrayPromedioTotal, "-");
                    }
                                                        
    
                    
                    
                    $PromedioAux = 0;
                    $ContadorA = 0;                
                    $i = 0;
    
                    foreach($ArrayColegaTrabajo as $value)
                    {
                        
                        $i++;
                   
                        if($value > 0)
                        {
                            $PromedioAux += $value;
                            $ContadorA++;
                        }                    
                    }                      
                    if(count($ArrayColegaTrabajo) == 0)
                    {
    
                        array_push($ArrayPromedioTotal, "-");
                    }
                    else
                    {
                        if($ContadorA == 0)
                        $ContadorA = 1;
                                            
                        $PromedioAux = $PromedioAux / $ContadorA;
    
                        if($PromedioAux != 0)
                        {
                            $ContadorTotal++;
                            array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                        }
                        
                        else
                            array_push($ArrayPromedioTotal, "-");
                            
                    }
                                                        
                    
                    
                    $PromedioAux = 0;
                    $ContadorA = 0;                
                    $i = 0;
                    
                    foreach($ArrayColaborador as $value)
                    {
                        
                        $i++;
                    
                        
                        
                        if($value > 0)
                        {
                            $PromedioAux += $value;
                            $ContadorA++;
                        }                                        
                    }                    
                    if(count($ArrayColaborador) == 0)
                    {
                        
                    
                        array_push($ArrayPromedioTotal, "-");
                    }
                    else
                    {
                        if($ContadorA == 0)
                        $ContadorA = 1;
                        
                        $PromedioAux = $PromedioAux / $ContadorA;
    
                        if($PromedioAux != 0)
                        {
                            $ContadorTotal++;
                            array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                        }
                        
                        else
                            array_push($ArrayPromedioTotal, "-");
                    }                    
    
                    
                    
                    $PromedioAux = 0;
                    $ContadorA = 0;                
                    $i = 0;
    
                    foreach($ArrayJefeInmediato as $value)
                    {                    
                        $i++;
          
                        
                        if($value > 0)
                        {
                            $PromedioAux += $value;
                            $ContadorA++;
                        }                                                                
                    } 
                                      
                    if(count($ArrayJefeInmediato) == 0)
                    {
    
                        array_push($ArrayPromedioTotal, "-");
                    }   
                    else
                    {
                        if($ContadorA == 0)
                        $ContadorA = 1;
                                            
                        $PromedioAux = $PromedioAux / $ContadorA;
                        
                        if($PromedioAux != 0)
                        {
                            $ContadorTotal++;
                            array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                        }
                        else
                            array_push($ArrayPromedioTotal, "-");                    
                    }
?>                    

  
  

                
                
                <th><em style="color: blue;"><?php echo $ArrayPromedioTotal[0];?></em></th>
                
                
                <?php
                      if($ArrayPromedioTotal[0] != "-" )
                        array_push($ArrayTotalAutoevaluacion, $ArrayPromedioTotal[0]);
                        
                      if($ArrayPromedioTotal[1] != "-" )
                        array_push($ArrayTotalClienteInterno, $ArrayPromedioTotal[1]);
                        
                      if($ArrayPromedioTotal[2] != "-" )
                        array_push($ArrayTotalColegaTrabajo, $ArrayPromedioTotal[2]);
                        
                      if($ArrayPromedioTotal[3] != "-" )
                        array_push($ArrayTotalColaborador, $ArrayPromedioTotal[3]);
                        
                      if($ArrayPromedioTotal[4] != "-" )
                        array_push($ArrayTotalJefeInmediato, $ArrayPromedioTotal[4]);                                                                                                  
                      
                      $SumaPromedio = 0;
                      $j = 0;
                      
                     foreach($ArrayPromedioTotal as $value)
                       {
                            $j++;
                            if($value != "-" && $j != 1)
                                $SumaPromedio += $value;
                       }    
                       
                       if($ContadorTotal == 0)
                        $ContadorTotal = 1; 
                       
                       $SumaPromedio = $SumaPromedio/$ContadorTotal;
                       
                ?>
                <!--<th style="border-left-width: 3px;"><?php echo number_format($SumaPromedio, 2)?></th>--></tr>
                            
                                                                      
        <?php             
                if($SumaPromedio > 0)
                    array_push($ArrayTotalArea, $SumaPromedio);       
                }                
        ?>
        <tr style="height: 50px; border-style: groove;">
            <th colspan="2" class="tdleft" style="color: blue;">Promedio:</th>
            
<?php


            $ArrayTotalEnfoques = array();

            $aux = 0;
            foreach($ArrayTotalAutoevaluacion as $value)                        
                $aux += $value;
            
            if(count($ArrayTotalAutoevaluacion) == 0)
                $aux = 0;
                
            else
                $aux = $aux / count($ArrayTotalAutoevaluacion);
                
            echo "<th style='border-style: groove;'><em style='color: blue;'><strong>". number_format($aux, 2)."</strong></em></th>";
            array_push($ArrayTotalEnfoques, $aux);

            
            /*
            $aux = 0;
            foreach($ArrayTotalClienteInterno as $value)                        
                $aux += $value;
            
            if(count($ArrayTotalClienteInterno) > 0)
            $aux = $aux / count($ArrayTotalClienteInterno);
            
            else
            $aux = $aux / 1;
            
            echo "<th><em><strong>". number_format($aux, 2)."</strong></em></th>";
            array_push($ArrayTotalEnfoques, $aux);
            
                        
            $aux = 0;
            foreach($ArrayTotalColegaTrabajo as $value)                        
                $aux += $value;
            
            if(count($ArrayTotalColegaTrabajo) == 0)
                $aux = 0;
                
            else
                $aux = $aux / count($ArrayTotalColegaTrabajo);            
            
            echo "<th><em><strong>". number_format($aux, 2)."</strong></em></th>";                        
            array_push($ArrayTotalEnfoques, $aux);
                        
                        
            $aux = 0;
            foreach($ArrayTotalColaborador as $value)                        
                $aux += $value;
                
            if(count($ArrayTotalColaborador) > 0)            
                $aux = $aux / count($ArrayTotalColaborador);
                
            $aux = $aux / 1;
            
            echo "<th><em><strong>". number_format($aux, 2)."</strong></em></th>";                        
            array_push($ArrayTotalEnfoques, $aux);
            
            
                                    
            $aux = 0;
            foreach($ArrayTotalJefeInmediato as $value)                        
                $aux += $value;
                
            if(count($ArrayTotalJefeInmediato) == 0)
                $aux = 0;
                
            else
                $aux = $aux / count($ArrayTotalJefeInmediato);
                             

            echo "<th><em><strong>". number_format($aux, 2)."</strong></em></th>";                        
            array_push($ArrayTotalEnfoques, $aux);
            
            
            $aux = 0;
            foreach($ArrayTotalArea as $value)                        
                $aux += $value;

            if(count($ArrayTotalArea) == 0)
                $aux = 0;
                
            else
                $aux = $aux / count($ArrayTotalArea);
                            
            echo "<th style='border-style: groove;'><em style='color: blue;'><strong>". number_format($aux, 2)."</strong></em></th>";                        
            */
            $Total = $aux;

            

?>
        </tr>
    </table>
<br /><br />

<script src="js/highcharts.js"></script>
<script src="js/highcharts-more.js"></script>
<script src="js/exporting.js"></script>

<div id="container" style="width: 90%; height: 400px; margin: 0 auto"></div>

<?php 
$Resultadosgraf  = $bddr->prepare("select t.Temas, AVG(r.Resultado) as porc FROM Evaluacion_Resultados_cuest as r JOIN Evaluacion_Preguntas_cuest as p on r.IdPregunta = p.IdPregunta JOIN Evaluacion_Temas_cuest as t on p.IdTema = t.IdTema GROUP by p.IdTema
");
?>
<script>
$(function () {
    $('#container').highcharts({
        title: {
            text: 'Evaluación Clima Laboral - Resultados de Área'
        },
        xAxis: {
            categories: [<?php 
              $Resultadosgraf->execute();
              foreach ($Resultadosgraf as $value) { echo "'".$value[0] . "', "; } 
?>]
        },        
        credits: {
              enabled: false
        },
        series: [{
            type: 'column',
            name: 'Promedio - Enfoque de Evaluación',
            data: [ <?php 
                $Resultadosgraf->execute();
                foreach ($Resultadosgraf as $value) { echo number_format($value[1],2) . ","; }
              ?> ]
        }, {
            type: 'spline',
            name: 'Promedio Total',
            data: [ <?php 
                $Resultadosgraf->execute();
                foreach ($Resultadosgraf as $value) { echo number_format($value[1],2) . ","; }
             ?> ],
            marker: {
                lineWidth: 1,
                lineColor: Highcharts.getOptions().colors[2],
                fillColor: 'white'
            }
        }]
    });
});
</script>    
</div>

<?php /*
$Resultadosgraf  = $bddr->prepare("select t.Temas, AVG(r.Resultado) as porc FROM Evaluacion_Resultados_cuest as r JOIN Evaluacion_Preguntas_cuest as p on r.IdPregunta = p.IdPregunta JOIN Evaluacion_Temas_cuest as t on p.IdTema = t.IdTema GROUP by p.IdTema
");
            $Resultadosgraf->execute();

while($Datagraf = $Resultadosgraf->fetch()){
  echo number_format($Datagraf[1], 2).",";
} */
?>