<?php

/**
 * @author Calderón
 * @copyright 2014
 */

$Evaluador = $_POST['IEv'];

$Empleados = $bddr->prepare("Select e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, a.IdAreaDeTrabajo, a.NombreAreaDeTrabajo  
                             from Empleado as e
                             inner join CargosAsignacion as ca on e.IdEmpleado = ca.IdEmpleado
                             inner join Cargos as c on ca.IdCargo = c.IdCargos 
                             inner join AreasDeTrabajo as a on c.IdArea_Fk = a.IdAreaDeTrabajo
                             where e.IdEmpleado = '$Evaluador' ");                                   
$Empleados->execute();   
                             
$DataEm = $Empleados->fetch();
?>
<br /><hr color='skyblue' /><br />
<form method="post" action="Empleados/Evaluacion_cuest/GestionEvaluacionABM.php">
<input type='hidden' name='IdPosition' value='<?php echo $IdCargo; ?>' />
<input type='hidden' name='IdE' value='<?php echo $IdEmpleado?>' />
<input type='hidden' name='IEv' value='<?php echo $Evaluador?>' />

<table style="width: 80%; margin-left: 10%;">
<tr><td>Nombre del evaluador:</td><td><?php echo "$DataEm[1] $DataEm[2] $DataEm[3] $DataEm[4]";?> </td></tr>
<tr><td>Area de trabajo:</td><td><?php echo $DataEm[8]?> </td></tr>
<tr><td>Cargo:</td><td><?php echo $DataEm[6]?> </td></tr>
<tr>
    <td>Enfoque de la evaluación:</td>
    <td>
        <select name="TipoEvaluacion" style="width: 300px;">
            <option value="Autoevaluación">Autoevaluación</option>
        </select>
    </td>
</tr>
<tr>
    <td>Formulario a evaluar:</td>
    <td>
        <select name="IdFormulario" style="width: 300px;">
            <option value="1">Clima laboral</option>
            
        </select>
    </td>
</tr>

<tr>
<td colspan="2" style="text-align: center; padding-top: 10px;">
    <input type="submit" name="Enviar" class="boton" value="Asignar Evaluador" style="width: 150px;" />
</td>
</tr>
</table>
</form>