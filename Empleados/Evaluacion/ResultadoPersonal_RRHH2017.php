<?php

/**
 * @author Lilian Anabel Cordero 
 * @copyright 2017
 */
   
   

    unset($_SESSION['Numero']);
    unset($_SESSION['IdC']);
    unset($_SESSION['IdE']);
    unset($_SESSION['IdForm']);    
            
    if(isset($_POST['IdE']))
    $IDE = $_POST['IdE'];
    
    else
    $IDE = $_SESSION["IdUsuario"];
    
    $MisDatos = $bddr->prepare("SELECT a.*  FROM  Cargos as a
                                inner join CargosAsignacion as ca on ca.IdCargo = a.IdCargos 
                                where ca.IdEmpleado = $IDE and FechaFin = '0000-00-00'" );
    $MisDatos->execute();
    $DataEmpleado = $MisDatos->fetch();
       
    
   

    
    if(isset($_POST['Evaluacion']))
    {
        $_SESSION['eva'] = $_POST['Evaluacion'];        
    }   
    else
    {
        $Evaluacion = $bddr->prepare("Select * from cuestionario_RRHH  ORDER BY IdCuestionario DESC");
        $Evaluacion->execute();   
        
        $DataEva = $Evaluacion->fetch();
        $_SESSION['eva'] = $DataEva[0];
        $_SESSION['IdEva'] = $DataEva[1];
    }                                            

    
        
        $IDC = $DataEmpleado[0];              
                
        $_SESSION['IdE'] = $IDE;
        $_SESSION['IdC'] = $IDC;
                          
                
        $DataEmpleados = $bddr->prepare("Select e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.* 
                                         from Empleado as e
                                         inner join CargosAsignacion as ca on ca.IdEmpleado = e.IdEmpleado
                                         inner join Cargos as c on ca.IdCargo = c.IdCargos  
                                         where e.IdEmpleado = $IDE and c.IdCargos = $IDC  and FechaFin = '0000-00-00'");
        $DataEmpleados->execute();                                
        $DatosEmpleados = $DataEmpleados->fetch();
        
        $Area = $bddr->prepare("Select * from AreasDeTrabajo where IdAreaDeTrabajo = $DatosEmpleados[8]");
        $Area->execute();
            
        $DataA = $Area->fetch();
                                     
        $ArrayAutoevaluacion = array();
        $ArrayClienteInterno = array();
        $ArrayColegaTrabajo = array();
        $ArrayColaborador = array();
        $ArrayJefeInmediato = array();
        
        $ArrayAutoevaluacionJ = array();
        $ArrayClienteInternoJ = array();
        $ArrayColegaTrabajoJ = array();
        $ArrayColaboradorJ = array();
        $ArrayJefeInmediatoJ = array();        

        $IdEvaluacion = $bddr->prepare("SELECT IdCuestionario FROM cuestionario_Resultados  WHERE Fecha BETWEEN '2017/01/01' AND '2017/12/31' AND IdCuestionario ='" . $_SESSION['eva']."'");
        $IdEvaluacion->execute();
        $DataIdEva = $IdEvaluacion->fetch();

?>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">
<table style="width: 100%;">
    <tr><td style='text-align: right;'><h2>Resultado de Evaluaci�n DNC 2017</h2></td></tr>
    <tr>
        <td>
            <table>
            <tr>
                <td>
                    <?php
        	               if(isset($_POST['Aux1']))
                           {
                                echo "<form action='?l=DNC2017' method='post'>
                                        <input type='hidden' name='ia' value='$DataA[0]' />
                                        <input type='submit' name='Enviar' value='<- Regresar' class='boton' />
                                      </form>  ";
                           }
                    ?>
                </td>
              </tr>
            <tr>
                <td colspan="4" style="text-align: right;">
                    <?php
	                       echo "<form action='Empleados/Evaluacion/ResultadoPersonalPrint.php' method='post' target='_blank'>
                                    <input type='hidden' name='IdE' value='$IDE' />
                                    <input type='hidden' name='DataIdEva' value='$DataIdEva[0]' />
                                    <input type='hidden' name='Evaluacion' value='".$_SESSION['eva']."' />
                                    
                                    <input type='submit' class='botonG' value='Imprimir' />                          
                                  </form>";
                    ?>
                </td>
            </tr>
            </table>
        </td>  
    </tr>  
    <tr>
        <td>   
        <?php
            echo "<table style='width: 94%; margin-left: 3%; '>
                    
                  <tr><td colspan='2' style='color: blue; '> Datos del Empleado</td><tr>
                  <tr><td class='tdleft' style='width: 30%'> Empleado: </td><td>$DatosEmpleados[1] $DatosEmpleados[2] $DatosEmpleados[3] $DatosEmpleados[4] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Area: </td><td>$DataA[1] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Cargo: </td><td> $DatosEmpleados[6] </td></tr>
                   </table>";
            $InfoCuestio = $bddr->prepare("Select i.*, j.*, k.*, l.*, m.* from cuestionario_RRHH as i inner join cuestionario_RH as j on i.IdCuestionario = j.idrrhh Inner join cuestionario_Resultados as k on i.IdCuestionario = k.IdCuestionario inner join cuestionario_NRH  as l on i.IdCuestionario = l.idrrhh inner join cuestionario_NCT as m on i.IdCuestionario = m.idrrhh inner join cuestionario_CT as n on i.IdCuestionario = n.idrrhh inner join cuestionario_Jefes as r on i.IdCuestionario = r.idrrhh where k.Fecha BETWEEN '2017/01/01' AND '2017/12/31' AND i.IdEmpleado='" . $_SESSION['IdE']."'");
            $InfoCuestio->execute();                                
            $DatosCuestionario = $InfoCuestio->fetch();

            //En relaciones Humanas en el trabajo

            $relHuman = $bddr->prepare("SELECT p.IdCuestionario, z.*, p.IdEmpleado FROM cuestionario_RRHH as p
                                          INNER JOIN cuestionario_RH as z on  p.IdCuestionario=z.idrrhh
                                          INNER JOIN cuestionario_Resultados as r on p.IdCuestionario=r.IdCuestionario
                                          WHERE r.Fecha BETWEEN '2017/01/01' AND '2017/12/31' AND p.IdEmpleado ='" . $_SESSION['IdE']."'");
            $relHuman->execute();
            $relHumanas = $relHuman->fetchAll();

            //Conocimientos tecnicos

            $desProfes = $bddr->prepare("SELECT p.IdCuestionario, z.*, p.IdEmpleado FROM cuestionario_RRHH as p
                                          INNER JOIN cuestionario_CT as z on  p.IdCuestionario=z.idrrhh
                                          INNER JOIN cuestionario_Resultados as r on p.IdCuestionario=r.IdCuestionario
                                          WHERE r.Fecha BETWEEN '2017/01/01' AND '2017/12/31' AND p.IdEmpleado ='" . $_SESSION['IdE']."'");
            $desProfes->execute();
            $desProf = $desProfes->fetchAll();

            //PARTE II Levantamiento de necesidades

            //En relaciones Humanas en el trabajo (Desarrollo Personal)
            $NombreCurso = $bddr->prepare("SELECT p.IdCuestionario, z.Nombre, p.IdEmpleado FROM cuestionario_RRHH as p
                                          INNER JOIN cuestionario_NRH as z on  p.IdCuestionario=z.idrrhh
                                          INNER JOIN cuestionario_Resultados as r on p.IdCuestionario=r.IdCuestionario
                                          WHERE r.Fecha BETWEEN '2017/01/01' AND '2017/12/31' AND p.IdEmpleado ='" . $_SESSION['IdE']."'");
            $NombreCurso->execute();
            $nomCur = $NombreCurso->fetchAll();

            //Conocimientos Tecnicos (Desarrollo Personal)

            $conTecni = $bddr->prepare("SELECT p.IdCuestionario, z.*, p.IdEmpleado FROM cuestionario_RRHH as p
                                          INNER JOIN cuestionario_NCT as z on  p.IdCuestionario=z.idrrhh
                                          INNER JOIN cuestionario_Resultados as r on p.IdCuestionario=r.IdCuestionario
                                          WHERE r.Fecha BETWEEN '2017/01/01' AND '2017/12/31' AND p.IdEmpleado ='" . $_SESSION['IdE']."'");
            $conTecni->execute();
            $conTec = $conTecni->fetchAll();

            //PARTE III Necesidades de Capacitacion SOLO JEFATURA

            //Conocimientos tecnicos (Desarrollo personal)

            $conTecJ = $bddr->prepare("SELECT p.IdCuestionario, z.*, p.IdEmpleado FROM cuestionario_RRHH as p
                                          INNER JOIN cuestionario_Jefes as z on  p.IdCuestionario=z.idrrhh
                                          INNER JOIN cuestionario_Resultados as r on p.IdCuestionario=r.IdCuestionario
                                          WHERE r.Fecha BETWEEN '2017/01/01' AND '2017/12/31' AND p.IdEmpleado ='" . $_SESSION['IdE']."'");
            $conTecJ->execute();
            $conJefes = $conTecJ->fetchAll();

             $conTecJ2 = $bddr->prepare("SELECT p.IdCuestionario, z.*, p.IdEmpleado FROM cuestionario_RRHH as p
                                          INNER JOIN cuestionario_Jefeshum as z on  p.IdCuestionario=z.idrrhh
                                          INNER JOIN cuestionario_Resultados as r on p.IdCuestionario=r.IdCuestionario
                                          WHERE r.Fecha BETWEEN '2017/01/01' AND '2017/12/31' AND p.IdEmpleado ='" . $_SESSION['IdE']."'");
            $conTecJ2->execute();
            $conJefes2 = $conTecJ2->fetchAll();
        ?>
        </td>
    </tr>

    <tr>
        <td>
        <table style="width: 100%"; >
        <tr><td colspan='6'><hr color='skyblue' /></td></tr>
       
                  
                     <tr><td  style="padding-bottom: 8px;"><h4>Antiguedad en la institucion</h4></td></tr>
                     <table style="width: 70%"; rules="rows" ; >
                     <tr><td><strong>A�os:</strong></td><td><?php echo"$DatosCuestionario[2]"?></td><td><strong>Meses:</strong></td><td><?php echo "$DatosCuestionario[3]"?></td></tr>
                     <tr><td><strong>Puesto 1:</strong></td><td><?php echo  "$DatosCuestionario[4]"?></td><td><strong>A�os:</strong></td><td><?php echo  "$DatosCuestionario[5]"?></td></tr>
                     <tr><td><strong>Puesto 2:</strong></td><td><?php echo  "$DatosCuestionario[6]"?></td><td><strong>A�os:</strong></td><td><?php echo  "$DatosCuestionario[7]"?></td></tr>
                     <tr><td><strong>Puesto 3:</strong></td><td><?php echo  "$DatosCuestionario[8]"?></td><td><strong>A�os:</strong></td><td><?php echo  "$DatosCuestionario[9]"?></td></tr>
                     <tr><td><strong>Perfil laboral:</strong></td><td><?php echo  "$DatosCuestionario[10]"?></td></tr>
                     </table>
        <tr><td colspan='6'><hr color='skyblue' /></td></tr>
        </table>
        <tr border="1"><td  style="padding-bottom: 8px;"><h4>Relaciones Humanas (Desarrollo Personal)</h4></td></tr>
                      <table style="text-align: center; width: 100%;"; border="1"; >
                     
                     <!--Recorriendo arreglo para mostrar datos 1ra tabla-->

                     <tr><td><strong>Nombre del curso</strong></td><td><strong>Tiempo de Duracion</strong></td><td><strong>Organizado por</strong></td><td><strong>Finaciado por</strong></td><td><strong>Especificacion</strong></td></tr>
                     <?php                      
                      foreach ($relHumanas as $row2) {
                        echo "<tr><td>{$row2['Nombre']}</td></th>";
                        echo "<td>{$row2['Duracion']}</td>";
                        echo "<td>{$row2['Organizado']}</td>";
                        echo "<td>{$row2['Financiado']}</td>";
                        echo "<td>{$row2['Especifique']}</td></tr>";
                      }
                     ?>
</table>

        <tr><td colspan='6'><hr color='skyblue' /></td></tr>

                    <!--Segunda tabla-->
                    <tr><td  style="padding-bottom: 8px;"><h4>Conocimientos Tecnicos (Desarrollo Profesional)</h4></td></tr>
                    <table style="text-align: center; width: 100%;"; border="1"; >
                    <tr><td><strong>Nombre del curso</strong></td><td><strong>Tiempo de Duracion</strong></td><td><strong>Organizado por</strong></td><td><strong>Finaciado por</strong></td><td><strong>Especificacion</strong></td></tr>
                    <?php 
                      foreach ($desProf as $row3) {
                        echo "<tr><td>{$row3['Nombre']}</td></th>";
                        echo "<td>{$row3['Duracion']}</td>";
                        echo "<td>{$row3['Organizado']}</td>";
                        echo "<td>{$row3['Financiado']}</td>";
                        echo "<td>{$row3['Especifique']}</td></tr>";
                      }
                    ?>
                    </table>
        <tr><td colspan='6'><hr color='skyblue' /></td></tr>
                    <!--Tercera Tabla-->
                    <tr><td  style="padding-bottom: 8px;"><h4>En relaciones humanas de trabajo (Desarrollo Personal)</h4></td></tr>
                  <table style="width: 100%"; border="1"; >
                    <?php 
                    foreach ($nomCur as $row) {
                        # code...
                      echo "<tr><td>{$row['Nombre']}</td></tr>";
                      }
                    ?>
</table>
        <tr><td colspan='6'><hr color='skyblue' /></td></tr>
                    <!--Cuarta Tabla-->
                    <tr><td  style="padding-bottom: 8px;"><h4>Conocimientos Tecnicos (Desarrollo Profesional)</h4></td></tr>
                    <table style="width: 100%"; border="1"; >
                    <?php 
                    foreach ($conTec as $row4) {
                        # code...
                      echo "<tr><td>{$row4['Nombre']}</td></tr>";
                    }
                    ?>
</table>
 <tr><td colspan='6'><hr color='skyblue' /></td></tr>
                    <!--Quinta Tabla-->
                    <tr><td  style="padding-bottom: 8px;"><h4>En relaciones humanas de trabajo (Desarrollo Personal)</h4></td></tr>
                    <table style="width: 100%"; border="1"; >
                    <?php 
                     foreach ($conJefes2 as $row6) {
                        # code...
                      echo "<tr><td>{$row6['Nombre']}</td></th>";
                      echo "<td>{$row6['Empleado']}</td></tr>";
                    } 
                    ?> 
                      
</table>

        <tr><td colspan='6'><hr color='skyblue' /></td></tr>
                    <!--Quinta Tabla-->
                    <tr><td  style="padding-bottom: 8px;"><h4>Conocimientos Tecnicos (Desarrollo Personal - SOLO JEFATURAS)</h4></td></tr>
                    <table style="width: 100%"; border="1"; >
                    <?php 
                     foreach ($conJefes as $row5) {
                        # code...
                      echo "<tr><td>{$row5['Nombre']}</td></th>";
                      echo "<td>{$row5['Empleado']}</td></tr>";
                    } 
                    ?> 
                      
</table>

        </td>
    </tr>
</table>
</div>