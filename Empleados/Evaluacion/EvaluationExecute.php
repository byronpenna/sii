<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
   
    if( $_SESSION['IdC'] != "" && $_SESSION['IdE'] != "" )
    {
        $IDE = $_SESSION['IdE'];
        $IDC = $_SESSION['IdC'];
        $IDF = $_SESSION['IdForm'];
        
        $DataEmpleados = $bddr->prepare("Select e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.* , ca.IdAsignacion
                                         from Empleado as e
                                         inner join CargosAsignacion as ca on ca.IdEmpleado = e.IdEmpleado
                                         inner join Cargos as c on ca.IdCargo = c.IdCargos  
                                         where e.IdEmpleado = $IDE and c.IdCargos = $IDC  and FechaFin = '0000-00-00'");
        $DataEmpleados->execute();                                
        $DatosEmpleados = $DataEmpleados->fetch();
        
        $Area = $bddr->prepare("Select * from AreasDeTrabajo where IdAreaDeTrabajo = $DatosEmpleados[8]");
        $Area->execute();
            
        $DataA = $Area->fetch();
    }
    else
        Redireccion("Empleado.php?l=Evaluation");       

?>

<div style="width: 100%;background-color: white; border-radius: 10px; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="2"><h2>Evaluaci�n de Desempe�o</h2></td></tr>
        <tr><td colspan='2' style='color: blue; '>Evaluando</td><tr>
        <tr><td class='tdleft'> Empleado: </td><td><?php echo $DatosEmpleados[1] . " " . $DatosEmpleados[2] . " " . $DatosEmpleados[3] . " " . $DatosEmpleados[4] ?></td></tr>
        <tr><td class='tdleft'> Area: </td><td><?php echo $DataA[1]?> </td></tr>
        <tr><td class='tdleft'> Cargo: </td><td><?php echo $DatosEmpleados[6] . $IDF?> </td></tr>
        <tr><td colspan='2'><hr color='skyblue' /></td></tr>
        <?php
               $Formulario = $bddr->prepare("Select * from Evaluacion_Formulario where IdFormulario = $IDF ");
               $Formulario->execute();
               
               //if($Formulario->rowCount() == 0)
                    //Redireccion("Empleado.php?l=Evaluation");
               
               
               $DataF = $Formulario->fetch();
               
               $Query = "Select * from Evaluacion_Temas where IdFormulario = $DataF[0] ";
               $TotalTema = $bddr->prepare($Query);
               $TotalTema->execute();
               
               $Numero = 0;
               
               if($_SESSION['Numero'] != "")
                    $Numero = $_SESSION['Numero'];
                    
               else 
                    $Numero = 0;
               
               $Tema = $bddr->prepare($Query . " Limit $Numero, 1");
               $Tema->execute();
               $DataT = $Tema->fetch();
               
               $IdTema = $DataT[0];
        ?>            
        <tr><td class='tdleft'>Evaluaci�n:</td><td><?php echo $_SESSION['eva'];?></td></tr>    
        <tr><td class='tdleft'>Formulario:</td><td><?php echo $DataF[1];?></td></tr>        
        <tr><td class="tdleft">Factor de Evaluaci�n:</td><td><?php echo $DataT[2] ;?></td></tr>
        <tr><td class="tdleft"></td><td><?php echo $Numero+1 . " de " . $TotalTema->rowCount();?></td></tr>
        <tr><td colspan='2'><hr color='skyblue' /></td></tr>
<script>
function promediar()
{    
<?php
    
    $preguntas = $bddr->prepare("Select * from Evaluacion_Preguntas where IdTema = $IdTema");
    $preguntas->execute();  
                    
	
    $promedio = "";
    
    while($DataPre = $preguntas->fetch())
    {
        $promedio = "document.getElementById('$DataPre[0]').value * 1 + " . $promedio;    
    }
    
    $promedio = "(" . $promedio  . " 0 )/" . $preguntas->rowCount() ;
    
    echo "          var promedio = $promedio ;
          document.getElementById('Prom').value = promedio.toFixed(2)";

?>

}
function Calcular(id,valor)
{     
    document.getElementById(id).value =  valor.toFixed(2);
    promediar();    
}

function validar()
{
    var NoAplica = document.getElementById('NoAplica').checked;
    if(!NoAplica){
        var cont = 0;
        for (i=0;i<document.evaluacion.elements.length;i++)
        { 
          if(document.evaluacion.elements[i].type == "radio")
          {	
             if(document.evaluacion.elements[i].checked)
                cont++;
          } 
        }
        
        if(cont == <?php echo $preguntas->rowCount();?>)
            return true;
        
        else
        {
            alert("Error, Tema Incompleto")
            return false;
        }
    }
    else
        return true;
}
           
function desSeleccionar() 
{
    var NoAplica = document.getElementById('NoAplica').checked;

    if(NoAplica)
    {
        for (i=0;i<document.evaluacion.elements.length;i++)
        {
          if(document.evaluacion.elements[i].type == "radio")
          {	
             document.evaluacion.elements[i].checked = false;
             document.evaluacion.elements[i].disabled = true;
          }
          if(document.evaluacion.elements[i].type == "text")
          {	
             document.evaluacion.elements[i].value = '0.00'
          }         
        }
    } 
    else
    {
        for (i=0;i<document.evaluacion.elements.length;i++)
        {
          if(document.evaluacion.elements[i].type == "radio")
          {	
             document.evaluacion.elements[i].disabled = false;
          }        
        }
    }   
}             
</script>
        <tr>
            <td colspan='2'>
                <form action="Empleados/Evaluacion/EvaluationABM.php" method="Post" name="evaluacion"  id="evaluacion">
                <input type="hidden" name="Numero" value="<?php echo $Numero;?>" />
                <input type="hidden" name="Tema" value="<?php echo $IdTema;?>" />
                <input type="hidden" name="IdE" value="<?php echo $DatosEmpleados[0];?>" />
                <table style="width: 100%; text-align: center;">
                    <tr><th style="width: 60%;">Pregunta</th>                        
                        <th style="width: 5%;">1</th>
                        <th style="width: 5%;">2</th>
                        <th style="width: 5%;">3</th>
                        <th style="width: 5%;">4</th>
                        <th style="width: 5%;">5</th>
                        <th style="width: 15%;">Resultado</th></tr>
                    <?php
                        
                        $promedio = 0;
                        
                        $preguntas = $bddr->prepare("Select * from Evaluacion_Preguntas where IdTema = $IdTema");
                        $preguntas->execute();  
                        $totalPreguntas = $preguntas->rowCount();                      
                    	
                        $Apreguntas[] = null;
                        
                        while($DataPre = $preguntas->fetch())
                        {
                            array_push($Apreguntas,$DataPre[0]);
                            
                            $VerificacionP = $bddr->prepare("Select * from Evaluacion_Resultados 
                                                             where IdPregunta = $DataPre[0] and IdEmpleado = $DatosEmpleados[0] and 
                                                                   Evaluacion = '" . $_SESSION['eva']. "' and IdJefe = " .$_SESSION["IdUsuario"]);
                            $VerificacionP->execute();
                            
                            $Respuesta = $VerificacionP->fetch();
                            
                            
                            
                            if($Respuesta[3] != "")
                                $RespuestaValor = number_format($Respuesta[3],2);
                            
                            else
                                $RespuestaValor = number_format(0,2);
                                
                                
                            $promedio = $promedio + $RespuestaValor;
                    ?>                                    
                    <tr style="height: 60px;">
                        <td style="text-align: left;"><?php echo $DataPre[2]?></td>                        
                        <td><input type="radio" name="<?php echo $DataPre[0]?>" value="1" <?php if($Respuesta[3] == 1) echo "checked";?> onclick="Calcular(<?php echo $DataPre[0]?>,1.00)" /></td>
                        <td><input type="radio" name="<?php echo $DataPre[0]?>" value="2" <?php if($Respuesta[3] == 2) echo "checked";?> onclick="Calcular(<?php echo $DataPre[0]?>,2.00)"/></td>
                        <td><input type="radio" name="<?php echo $DataPre[0]?>" value="3" <?php if($Respuesta[3] == 3) echo "checked";?> onclick="Calcular(<?php echo $DataPre[0]?>,3.00)"/></td>
                        <td><input type="radio" name="<?php echo $DataPre[0]?>" value="4" <?php if($Respuesta[3] == 4) echo "checked";?> onclick="Calcular(<?php echo $DataPre[0]?>,4.00)"/></td>
                        <td><input type="radio" name="<?php echo $DataPre[0]?>" value="5" <?php if($Respuesta[3] == 5) echo "checked";?> onclick="Calcular(<?php echo $DataPre[0]?>,5.00)"/></td>
                        <td><input type="text" style="background-color: transparent; border-color: transparent; text-align: center;" id="<?php echo $DataPre[0]?>" value="<?php echo $RespuestaValor?>" readonly="true" /></td>
                    </tr>
                    <?php
	                     }
                         echo "<input type='hidden' value='";
                         foreach($Apreguntas as $value)
                         {
                            echo "$value-";
                         }
                         echo "' name='Idp' />";
                         
                         
                         $promedio = $promedio / $totalPreguntas;
                    ?>                   
                    </tr> 
                                      
                    <tr><td colspan="6" style="text-align: right; "><strong>Resultado:</strong></td>
                        <td>
                            <input type="text" id="Prom" name="Prom" value="<?php echo number_format($promedio, 2);?>" style="background-color: transparent; border-color: transparent; text-align: center;" readonly="true"/>
                        </td>
                    </tr>
                    <tr><td colspan='7'><hr color='skyblue' /></td></tr>
                    <?php
	                       
                            if(($Numero+1) == $TotalTema->rowCount())
                            {
                                $buscarComentaro = $bddr->prepare("Select * from Evaluacion_Estado 
                                                                    where IdEvaluacion = " . $_SESSION['ideva']. " and IdEvaluador = " .$_SESSION["IdUsuario"] . " and IdEmpleado = $DatosEmpleados[0]");
                                
                                $buscarComentaro->execute();
                                
                                if($buscarComentaro->rowCount() > 0)
                                    $DataComentario = $buscarComentaro->fetch();
                                
                                else
                                    $DataComentario = "";
                                
                                echo "<tr>
                                            <td colspan='7' style='text-align: left;'>
                                                Observaciones:<br />
                                                <textarea name='Observaciones' style='width: 90%; margin-left: 5%; height: 70px; margin-top: 5px;'>$DataComentario[8]</textarea><br /><br />
                                            </td>
                                      </tr>";
                            }
                    ?>
                    <tr>
                        <td style="text-align: left;">
                        <?php
	                           if($Numero != 0)
                                    echo "<input type='submit' name='Enviar' value='Regresar Tema' class='botonG' />";
                                    
                        ?>
                        </td>
                        <td colspan="5"><input type="checkbox" value="No Aplica" id="NoAplica" onclick="desSeleccionar();"  />No aplica</td>
                        <td style="text-align: center;">
                        <?php
	                           if(($Numero+1) == $TotalTema->rowCount())
                                    echo "<input type='submit' name='Enviar' value='Finalizar' class='botonG' onclick='return validar();' />";
                               
                               else 
                                    echo "<input type='submit' name='Enviar' value='Siguiente Tema' class='botonG' onclick='return validar()' />";
                        ?>
                        </td>                        
                    </tr>
                </table>
                </form>
            </td>
        </tr>
    </table>
 </div>
 