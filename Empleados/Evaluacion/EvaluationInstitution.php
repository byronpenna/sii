<?php
	
/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */


    unset($_SESSION['Numero']);
    unset($_SESSION['IdC']);
    unset($_SESSION['IdE']);
    unset($_SESSION['IdForm']);    
            
    if(isset($_POST['Evaluacion']))
    {
        $_SESSION['eva'] = $_POST['Evaluacion'];        
    }   
    else
    {
        $Evaluacion = $bddr->prepare("Select * from Evaluacion ORDER BY IdEvaluacion DESC");
        $Evaluacion->execute();   
        $DataEva = $Evaluacion->fetch();
        $_SESSION['eva'] = $DataEva[1];
        $_SESSION['IdEva'] = $DataEva[0];
    } 
    
    $ArrayInstitucional = array();
    

    $Areas = $bddr->prepare("Select * from AreasDeTrabajo");
    $Areas->execute();   
    
    echo "<div style='float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px'>
          <table style='width: 100%'>
            <tr><td style='text-align: right;'><h2 style='padding: 0px; margin: 0px;'>Evaluaci�nes de Desempe�o</h2></td></tr>
            <tr><td style='text-align: right;'><h3><em>".$_SESSION['eva']."<br /> Resumen Institucional</em></h3></td></tr>";

    
    echo "<tr>
          <td> 
            <table style='width: 90%; margin-left: 5%; text-align: center;'>
            <tr><td colspan='3'><hr color='skyblue' /></td></tr>";
            
                $arrayt01 = array();
                $arrayt02 = array();
                $arrayt03 = array();
                $arrayt04 = array();
                $arrayt05 = array();
                $arrayt06 = array();
                $arrayt07 = array();
                $arrayt08 = array();
                $arrayt09 = array();
                $arrayt10 = array();
                $arrayt11 = array();
                $arrayt12 = array();
                $arrayt13 = array();
                $arrayt14 = array();
                $arrayt15 = array();
                $arrayt16 = array();
                $arrayt17 = array();
                $arrayt18 = array();
                                                    
    while($DataArea = $Areas->fetch())
    {
        /** Verificaci�n de Promedios por �rea **/

        $EmpleadosA = $bddr->prepare("SELECT e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, c.IdCargos, c.Cargo
                                             FROM Empleado AS e
                                             inner join CargosAsignacion as ca on e.IdEmpleado = ca.IdEmpleado 
                                             inner join Cargos as c on c.IdCargos = ca.IdCargo
                                             inner join AreasDeTrabajo as a on c.IdArea_Fk = a.IdAreaDeTrabajo
                                             where a.IdAreaDeTrabajo = $DataArea[0] and FechaFin = '0000-00-00'");
                $EmpleadosA->execute();                
                $ArrayPromedio = array();
                
                $ArrayTotalAutoevaluacion = array();
                $ArrayTotalClienteInterno = array();
                $ArrayTotalColegaTrabajo = array();
                $ArrayTotalColaborador = array();
                $ArrayTotalJefeInmediato = array();
                
                $ArrayTotalArea = array();                    
                

                              
                
                while($DataE = $EmpleadosA->fetch())
                {
                    $ArrayAutoevaluacion = array();
                    $ArrayClienteInterno = array();
                    $ArrayColegaTrabajo = array();
                    $ArrayColaborador = array();
                    $ArrayJefeInmediato = array();
                    
                    $ArrayAutoevaluacionJ = array();
                    $ArrayClienteInternoJ = array();
                    $ArrayColegaTrabajoJ = array();
                    $ArrayColaboradorJ = array();
                    $ArrayJefeInmediatoJ = array();   
        
                    
                    
                    /** Proceso de analisis de las notas **/
                    $IDE = $DataE[0];
                    $IDC = $DataE[4];
                    
                    $Evaluadores = $bddr->prepare("Select * from Evaluacion_Estado WHERE IdEmpleado = $IDE ORDER BY TipoEvaluacion ASC ");
                    $Evaluadores->execute();
                    
                    while($DataEvaluador = $Evaluadores->fetch())  
                    {
                                                      
                           $TemasResultados = $bddr->prepare("SELECT t . * , f.Formulario FROM Evaluacion_Resultados AS r
                                                              INNER JOIN Evaluacion_Preguntas AS p ON r.IdPregunta = p.IdPregunta
                                                              INNER JOIN Evaluacion_Temas AS t ON p.IdTema = t.IdTema
                                                              INNER JOIN Evaluacion_Formulario AS f ON t.IdFormulario = f.IdFormulario
                                                              WHERE r.Evaluacion = '". $_SESSION['eva'] ."' and IdEmpleado = $IDE and IdAsignaci�nCargo = $IDC 
                                                              AND f.IdFormulario = $DataEvaluador[3] and IdJefe = $DataEvaluador[6] 
                                                              GROUP BY t.IdTema");   
                           $IdForm = $DataEvaluador[3];                                                   
                                                                                                  
                           $TemasResultados->execute();                       
                           
                           unset($arrayTemas);
                           unset($arrayPromedio);
                           
                           $arrayTemas = array();
                           $arrayPromedio = array();            
                                                
                           while($DataT = $TemasResultados->fetch())   
                                array_push($arrayTemas, $DataT[0]);
                            
                                       
                                
                           $PromedioTotal = 0;
                           $contador = 0;
                            
        
                            
                            
                           if(count($arrayTemas) > 0)
                           {
                            
                               foreach($arrayTemas as $value)
                               {
                                     $Resultados  = $bddr->prepare("SELECT r.* , t.Temas  FROM Evaluacion_Resultados AS r
                                                                   INNER JOIN Evaluacion_Preguntas AS p ON r.IdPregunta = p.IdPregunta
                                                                   INNER JOIN Evaluacion_Temas AS t ON p.IdTema = t.IdTema
                                                                   where t.IdTema = $value and r.Evaluacion = '".$_SESSION['eva'] ."' and IdEmpleado = $IDE and IdAsignaci�nCargo = $IDC and IdJefe = $DataEvaluador[6] ");
                                     $Resultados->execute();
                                     $Promedio = 0;                     
                                     $contador++;
                                     $tema = "";
                                     
                                     while($DataR = $Resultados->fetch())
                                     {                                
                                        $tema = $DataR[8];                            
                                        $Promedio = $Promedio + $DataR[3];
                                     }
                                     
                                     $Promedio = $Promedio / $Resultados->rowCount();                                      
                                     array_push($arrayPromedio, number_format($Promedio,2));
                                     
                                     

                                                             
                                    /** Institucional **/     
                                    if($tema == "PLANIFICACI�N Y ORGANIZACI�N")                         
                                        array_push($arrayt01, number_format($Promedio,2));
                                        
                                    if($tema == "CUMPLIMIENTOS DE METAS Y OBJETIVOS")                         
                                        array_push($arrayt02, number_format($Promedio,2));
                                        
                                    if($tema == "COMPROMISO INSTITUCIONAL")                         
                                        array_push($arrayt03, number_format($Promedio,2));

                                    if($tema == "VISI�N SOBRE NUEVOS PROYECTOS")                         
                                        array_push($arrayt04, number_format($Promedio,2));
                                        
                                    /** Tecnica **/    
                                    if($tema == "CONOCIMIENTO DEL PUESTO")                         
                                        array_push($arrayt05, number_format($Promedio,2));

                                    if($tema == "CALIDAD DE TRABAJO")                         
                                        array_push($arrayt06, number_format($Promedio,2));

                                    if($tema == "CREATIVIDAD E INNOVACI�N")                         
                                        array_push($arrayt07, number_format($Promedio,2));

                                    if($tema == "M�TODOS DE TRABAJO")                         
                                        array_push($arrayt08, number_format($Promedio,2));

                                    if($tema == "PENSAMIENTO ESTRAT�GICO")                         
                                        array_push($arrayt09, number_format($Promedio,2));

                                    if($tema == "CANTIDAD DE TRABAJO")                                                                         
                                        array_push($arrayt10, number_format($Promedio,2));
                                           
                                        
                                    /** Personal **/
                                    if($tema == "SALESIANIDAD")                         
                                        array_push($arrayt11, number_format($Promedio,2));

                                    if($tema == "RELACIONES INTERPERSONALES")                         
                                        array_push($arrayt12, number_format($Promedio,2));

                                    if($tema == "TRABAJO EN EQUIPO")                         
                                        array_push($arrayt13, number_format($Promedio,2));

                                    if($tema == "RESPONSABILIDAD Y DISCRECI�N")                         
                                        array_push($arrayt14, number_format($Promedio,2));

                                    if($tema == "ESP�RITU DE SERVICIO")                         
                                        array_push($arrayt15, number_format($Promedio,2));

                                    if($tema == "ACTITUD DE SERVICIO AL CLIENTE")                         
                                        array_push($arrayt16, number_format($Promedio,2));
                                        
                                    if($tema == "HABILIDAD PARA TOMAR DECISIONES")                         
                                        array_push($arrayt17, number_format($Promedio,2));

                                    if($tema == "DIRECCI�N Y DESARROLLO DE LOS COLABORADORES")                         
                                        array_push($arrayt18, number_format($Promedio,2));                                           
                                                                                                                                                                                                                                                                                                                         
                                     
                                     $PromedioTotal = $PromedioTotal + $Promedio;           
                               }            
                            
                                $PromedioTotal = $PromedioTotal / count($arrayTemas);                
                           }
                            
                            if($DataEvaluador[2] == "Autoevaluaci�n")
                            {
                                array_push($ArrayAutoevaluacion, number_format($PromedioTotal,2));
                                array_push($ArrayAutoevaluacionJ, $DataEvaluador[6]);                        
                            }    
                            if($DataEvaluador[2] == "Evaluaci�n - Cliente interno")
                            {
                                array_push($ArrayClienteInterno, number_format($PromedioTotal,2));
                                array_push($ArrayClienteInternoJ, $DataEvaluador[6]);
                            }    
                            if($DataEvaluador[2] == "Evaluaci�n - Colega de trabajo")
                            {
                                array_push($ArrayColegaTrabajo, number_format($PromedioTotal,2));
                                array_push($ArrayColegaTrabajoJ, $DataEvaluador[6]);
                            }    
                            if($DataEvaluador[2] == "Evaluaci�n - Colaborador")
                            {
                                array_push($ArrayColaborador, number_format($PromedioTotal,2));
                                array_push($ArrayColaboradorJ,  $DataEvaluador[6]);
                            }    
                            if($DataEvaluador[2] == "Evaluaci�n - Jefe inmediato")
                            {
                                array_push($ArrayJefeInmediato, number_format($PromedioTotal,2));
                                array_push($ArrayJefeInmediatoJ, $DataEvaluador[6]);
                            }
                                                                                                                                   
                    }
                    
                    $ArrayPromedioTotal = array();
                    $ContadorTotal = 0;
                                    
                    
                    
                    $PromedioAux = 0;
                    $ContadorA = 0;
                    $i = 0;
                    
                    foreach($ArrayAutoevaluacion as $value)
                    {
                        $i++;                                    
                        if($value > 0)
                        {
                            $PromedioAux += $value;
                            $ContadorA++;
                        }    
                    }  
                                      
                    if(count($ArrayAutoevaluacion) == 0)
                    {
    
                        array_push($ArrayPromedioTotal, "-");
                    }
                    else
                    {
                        if($ContadorA == 0)
                        $ContadorA = 1;                                        
                        $PromedioAux = $PromedioAux / $ContadorA;
                              
                        if($PromedioAux != 0)
                        {
                            //$ContadorTotal++;
                            array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                        }
                        
                        else
                            array_push($ArrayPromedioTotal, "-");
                    }
    
                    
                    
                    $PromedioAux = 0;
                    $ContadorA = 0;
                    $i = 0;
    
                    foreach($ArrayClienteInterno as $value)
                    {
                        
                        $i++;
                        if($value > 0)
                        {
                            $PromedioAux += $value;
                            $ContadorA++;
                        }                                           
                    }
                                          
                    if(count($ArrayClienteInterno) == 0)
                    {
                        array_push($ArrayPromedioTotal, "-");
                    }
                    else
                    {
                        if($ContadorA == 0)
                        $ContadorA = 1;
                                            
                        $PromedioAux = $PromedioAux / $ContadorA;
                              
                        if($PromedioAux != 0)
                        {
                            $ContadorTotal++;
                            array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                        }
                        
                        else
                            array_push($ArrayPromedioTotal, "-");
                    }
                                                        
    
                    
                    
                    $PromedioAux = 0;
                    $ContadorA = 0;                
                    $i = 0;
    
                    foreach($ArrayColegaTrabajo as $value)
                    {
                        
                        $i++;
                   
                        if($value > 0)
                        {
                            $PromedioAux += $value;
                            $ContadorA++;
                        }                    
                    }                      
                    if(count($ArrayColegaTrabajo) == 0)
                    {
    
                        array_push($ArrayPromedioTotal, "-");
                    }
                    else
                    {
                        if($ContadorA == 0)
                        $ContadorA = 1;
                                            
                        $PromedioAux = $PromedioAux / $ContadorA;
    
                        if($PromedioAux != 0)
                        {
                            $ContadorTotal++;
                            array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                        }
                        
                        else
                            array_push($ArrayPromedioTotal, "-");
                            
                    }
                                                        
                    
                    
                    $PromedioAux = 0;
                    $ContadorA = 0;                
                    $i = 0;
                    
                    foreach($ArrayColaborador as $value)
                    {
                        
                        $i++;
                    
                        
                        
                        if($value > 0)
                        {
                            $PromedioAux += $value;
                            $ContadorA++;
                        }                                        
                    }                    
                    if(count($ArrayColaborador) == 0)
                    {
                        
                    
                        array_push($ArrayPromedioTotal, "-");
                    }
                    else
                    {
                        if($ContadorA == 0)
                        $ContadorA = 1;
                        
                        $PromedioAux = $PromedioAux / $ContadorA;
    
                        if($PromedioAux != 0)
                        {
                            $ContadorTotal++;
                            array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                        }
                        
                        else
                            array_push($ArrayPromedioTotal, "-");
                    }                    
    
                    
                    
                    $PromedioAux = 0;
                    $ContadorA = 0;                
                    $i = 0;
    
                    foreach($ArrayJefeInmediato as $value)
                    {                    
                        $i++;
          
                        
                        if($value > 0)
                        {
                            $PromedioAux += $value;
                            $ContadorA++;
                        }                                                                
                    } 
                                      
                    if(count($ArrayJefeInmediato) == 0)
                    {
    
                        array_push($ArrayPromedioTotal, "-");
                    }   
                    else
                    {
                        if($ContadorA == 0)
                        $ContadorA = 1;
                                            
                        $PromedioAux = $PromedioAux / $ContadorA;
                        
                        if($PromedioAux != 0)
                        {
                            $ContadorTotal++;
                            array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                        }
                        else
                            array_push($ArrayPromedioTotal, "-");                    
                    }

                      if($ArrayPromedioTotal[0] != "-" )
                        array_push($ArrayTotalAutoevaluacion, $ArrayPromedioTotal[0]);
                        
                      if($ArrayPromedioTotal[1] != "-" )
                        array_push($ArrayTotalClienteInterno, $ArrayPromedioTotal[1]);
                        
                      if($ArrayPromedioTotal[2] != "-" )
                        array_push($ArrayTotalColegaTrabajo, $ArrayPromedioTotal[2]);
                        
                      if($ArrayPromedioTotal[3] != "-" )
                        array_push($ArrayTotalColaborador, $ArrayPromedioTotal[3]);
                        
                      if($ArrayPromedioTotal[4] != "-" )
                        array_push($ArrayTotalJefeInmediato, $ArrayPromedioTotal[4]);                                                                                                  
                      
                      $SumaPromedio = 0;
                      $j = 0;
                      
	                   foreach($ArrayPromedioTotal as $value)
                       {
                            $j++;
                            if($value != "-" && $j != 1)
                                $SumaPromedio += $value;
                       }    
                       
                       if($ContadorTotal == 0)
                        $ContadorTotal = 1; 
                       
                       $SumaPromedio = $SumaPromedio/$ContadorTotal;
                       

                if($SumaPromedio > 0)
                    array_push($ArrayTotalArea, $SumaPromedio);       
                }                



            $ArrayTotalEnfoques = array();

            $aux = 0;
            foreach($ArrayTotalAutoevaluacion as $value)                        
                $aux += $value;
            
            if(count($ArrayTotalAutoevaluacion) == 0)
                $aux = 0;
            else
                $aux = $aux / count($ArrayTotalAutoevaluacion);
                
            array_push($ArrayTotalEnfoques, $aux);

            
            
            $aux = 0;
            foreach($ArrayTotalClienteInterno as $value)                        
                $aux += $value;
            
            if(count($ArrayTotalClienteInterno) > 0)
            $aux = $aux / count($ArrayTotalClienteInterno);
            
            else
            $aux = $aux / 1;

            array_push($ArrayTotalEnfoques, $aux);
            
                        
            $aux = 0;
            foreach($ArrayTotalColegaTrabajo as $value)                        
                $aux += $value;

            if(count($ArrayTotalColegaTrabajo) == 0)
                $aux = 0;
            else
                $aux = $aux / count($ArrayTotalColegaTrabajo);
                            
                    
            array_push($ArrayTotalEnfoques, $aux);
                        
                        
            $aux = 0;
            foreach($ArrayTotalColaborador as $value)                        
                $aux += $value;
                
            if(count($ArrayTotalColaborador) > 0)            
                $aux = $aux / count($ArrayTotalColaborador);
                
            $aux = $aux / 1;
            
                      
            array_push($ArrayTotalEnfoques, $aux);
            
            
                                    
            $aux = 0;
            foreach($ArrayTotalJefeInmediato as $value)                        
                $aux += $value;
            
            if(count($ArrayTotalJefeInmediato) == 0)
                $aux = 0;
            else
                $aux = $aux / count($ArrayTotalJefeInmediato);            
                       
            array_push($ArrayTotalEnfoques, $aux);
            
            
            $aux = 0;
            foreach($ArrayTotalArea as $value)                        
                $aux += $value;

            if(count($ArrayTotalArea) == 0)
                $aux = 0;
            else
                $aux = $aux / count($ArrayTotalArea); 
            
        
        if($aux > 0)
        {
            array_push($ArrayInstitucional, $aux);
            $promedio = number_format($aux, 2);
        }
        else
            $promedio = "-";
            
                                                          
        
        echo "<tr><td style='text-align: left;'>$DataArea[1]</td><td>$promedio</td>
                  <td>
                    <form action='?l=ResultArea' method='post'>
                        <input type='hidden' name='ia' value='$DataArea[0]' />
                        <input type='submit' name='Enviar' value='Ver Detalles' class='boton'  />
                    </form>
                  </td>
              </tr>";
        
        
    }
        $aux = 0;
        foreach($ArrayInstitucional as $value)                        
           $aux += $value;
            
        $aux = $aux / count($ArrayInstitucional);
            
    echo "<tr><td colspan='3'><hr color='skyblue' /></td></tr>
          <tr><td style='text-align: right;'><strong>Promedio Institucional</strong></td><td><strong>".number_format($aux, 2)."</strong></td><td></td></tr>
          </table>
          </td>
          </tr>";
       
    
 
    echo "<tr>
              <td>
              <table style='width: 90%; margin-left: 5%;'>";


    echo "<tr><td colspan='2'><h2>Factores de Evaluaci�n</h2></td></tr>";
    
    function Criterio( $array, $total, $criterio)
    {
        $aux = $aux2 = 0 ;
        foreach($array as $value)
            $aux +=  $value;
           
        $aux2 =  $aux / count($array);        
        echo "<tr><td style='width: 70%; color: blue'>$criterio</td><td>". number_format((double)$aux2,2) . "</td>";
        return $aux2;        

    }    
    function ignore_divide_by_zero($errno, $errstring)
{
  return ($errstring == 'Division by zero');
}

set_error_handler('ignore_divide_by_zero', E_WARNING);
    
    $TotalCInstitucinal = 0;    
    echo "<tr><td><strong style='color: blue;'>Competencias Institucionales</strong></td><td></td></tr>";
    $TotalCInstitucinal += Criterio( $arrayt01, $TotalCInstitucinal, "Planificaci�n y Organizaci�n");    
    $TotalCInstitucinal += Criterio( $arrayt02, $TotalCInstitucinal, "Cumplimiento de Metas y Objetivos");    
    $TotalCInstitucinal += Criterio( $arrayt03, $TotalCInstitucinal, "Compromiso Institucional");    
    $TotalCInstitucinal += Criterio( $arrayt04, $TotalCInstitucinal, "Visi�n de Nuevos Proyectos");
    
          
    /** Promedio Institucional **/
    $TotalCInstitucinal = $TotalCInstitucinal / 4;             
    echo "<tr><td><hr /></td><td></td></tr>";


    $TotalCPersonal = 0;
    echo "<tr><td><strong style='color: blue;'>Competencias T�cnicas</strong></td><td></td></tr>";
    $TotalCPersonal += Criterio( $arrayt05, $TotalCPersonal, "Conocimiento del Puesto");
    $TotalCPersonal += Criterio( $arrayt06, $TotalCPersonal, "Calidad de Trabajo");
    $TotalCPersonal += Criterio( $arrayt07, $TotalCPersonal, "Creatividad e Innovaci�n");
    $TotalCPersonal += Criterio( $arrayt08, $TotalCPersonal, "M�todo de Trabajo");
    $TotalCPersonal += Criterio( $arrayt09, $TotalCPersonal, "Pensamiento Estrategico");
    $TotalCPersonal += Criterio( $arrayt10, $TotalCPersonal, "Cantidad de Trabajo");    

    /** Promedio Institucional **/
    $TotalCPersonal = $TotalCPersonal / 6;     
    echo "<tr><td><hr /></td><td></td></tr>";
    

    $TotalCTecnicas = 0;
    echo "<tr><td><strong style='color: blue;'>Competencias Personales</strong></td><td></td></tr>";
    $TotalCTecnicas += Criterio( $arrayt11, $TotalCTecnicas, "Salesianidad");
    $TotalCTecnicas += Criterio( $arrayt12, $TotalCTecnicas, "Relaciones Interpersonales");
    $TotalCTecnicas += Criterio( $arrayt13, $TotalCTecnicas, "Trabajo en Equipo");
    $TotalCTecnicas += Criterio( $arrayt14, $TotalCTecnicas, "Responsabilidad y Discresi�n");
    $TotalCTecnicas += Criterio( $arrayt15, $TotalCTecnicas, "Esp�ritu de Servicio");
    $TotalCTecnicas += Criterio( $arrayt16, $TotalCTecnicas, "Actitud de Servicio al Cliente");    
    $TotalCTecnicas += Criterio( $arrayt17, $TotalCTecnicas, "Habilidad para tomar Desiciones");
    $TotalCTecnicas += Criterio( $arrayt18, $TotalCTecnicas, "Direcci�n y Desarrollo de los Colaboradores");
    
    /** Promedio Institucional **/
    $TotalCTecnicas = $TotalCTecnicas / 8;                       
    echo "<tr><td><hr /></td><td></td></tr>";
    
               


                 
    echo "      </table>
            </td>
          </tr>
          <tr>
              <td>                  
                  <table style='width: 90%; margin-left: 5%;'>
                    <tr><td colspan='2'><hr color='skyblue' /></td></tr>
                    <tr><td colspan='2'><h2>Resumen Institucional por Competencias</h2></td></tr>
                    <tr><td style='width: 70%'>Competencias Institucionales</td><td style='width: 30%'> " . number_format($TotalCInstitucinal, 2) ."</td>
                    <tr><td style='width: 70%'>Competencias Personales</td><td style='width: 30%'> " . number_format($TotalCPersonal, 2) ."</td>
                    <tr><td style='width: 70%'>Competencias Tecnicas</td><td style='width: 30%'> " . number_format($TotalCTecnicas, 2) ."</td>
                  </table>
              </td>
          </tr>
          <tr>
            <td>";
	
?>
            
            <script src="js/highcharts.js"></script>
            <script src="js/exporting.js"></script>
        
        <div id="container" style="min-width: 400px; height: 300px; margin: 0 auto"></div>
        
        <script>
        $(function () {
            $('#container').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'GRAFICA POR COMPETENCIAS'
                },
                subtitle: {
                    text: '<?php echo strtoupper($_SESSION['eva']);?>'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Escala Evaluada'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Promedio por Competencia: <b>{point.y:.1f}</b>'
                },
                credits: {
                        enabled: false
                },
                series: [{
                    name: 'Population',
                    data: [
                        [' ', 0],
                        ['Institucionales', <?php echo number_format($TotalCInstitucinal, 2);?>],
                        [' ', 0],
                        ['Personales',  <?php echo number_format($TotalCPersonal, 2);?>],
                        [' ', 0],
                        ['Tecnicas',  <?php echo number_format($TotalCTecnicas, 2);?>],
                        [' ', 0]                        
                    ],
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: 4,
                        y: 10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px black'
                        }
                    }
                }]
            });
        });        
        </script>  
                  
  <?php
    echo "  </td>
          </tr>
    </table>";
    echo "</div>";

    
?>