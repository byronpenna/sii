<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */


    unset($_SESSION['Numero']);
    unset($_SESSION['IdC']);
    unset($_SESSION['IdE']);
    unset($_SESSION['IdForm']);    
            
    if(isset($_POST['Evaluacion']))
    {
        $_SESSION['eva'] = $_POST['Evaluacion'];        
    }   
    else
    {
        $Evaluacion = $bddr->prepare("Select * from Evaluacion ORDER BY IdEvaluacion DESC");
        $Evaluacion->execute();   
        $DataEva = $Evaluacion->fetch();
        $_SESSION['eva'] = $DataEva[1];
        $_SESSION['IdEva'] = $DataEva[0];
    } 
    
    $MisDatos = $bddr->prepare("SELECT a.*  FROM  Cargos as a
                                inner join CargosAsignacion as ca on ca.IdCargo = a.IdCargos 
                                where ca.IdEmpleado = " . $_SESSION["IdUsuario"] . " and FechaFin = '0000-00-00'" );
    $MisDatos->execute();
?>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px"> 
    <table style="width: 100%;">
    <tr><td colspan="4" style="text-align: right;"><h2>Evaluaci�nes de Desempe�o - Areas de Trabajo</h2></td></tr>
<?php
    if($MisDatos->rowCount() > 0)
    {
        while($DataM = $MisDatos->fetch())
        {
            $Area = $bddr->prepare("Select * from AreasDeTrabajo where IdAreaDeTrabajo = $DataM[3]");
            $Area->execute();
            
            $DataA = $Area->fetch();
            $IdC = $DataM[0];
            
            echo "<tr>
                    <td>
                        <table style='width: 94%; margin-left: 3%; '>
                        <tr><td colspan='3' style='color: blue; '> Tus Datos</td><tr>
                        <tr><td class='tdleft'> Area: </td><td>$DataA[1] </td>
                            <td rowspan='2' style='text-align: center;'>";
            
            if($_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["TipoUsuario"] == "RRHH" || $IdC == 13)                          
                echo "          <form action='?l=Institution2016' method='post'>  
                                    <input type='submit' name='Enviar' value='Resumen Institucional 2016' style='width: 182px;margin: 2px;' class='boton' />
                                </form>
                                
                                <form action='?l=Institution2015' method='post'>  
                                    <input type='submit' name='Enviar' value='Resumen Institucional 2015' style='width: 182px;margin: 2px;' class='boton' />
                                </form>

                                <form action='?l=Institution2014' method='post'>  
                                    <input type='submit' name='Enviar' value='Resumen Institucional 2014' style='width: 182px;margin: 2px;' class='boton' />
                                </form>";
                                
            echo "          </td>
                        </tr>
                        <tr><td class='tdleft'> Cargo: </td><td>$DataM[1]</td></tr>
                        <tr><td colspan='3'><hr color='skyblue' /></td></tr> 
                        </table>
                    </td>
                  </tr>";

        }
    }
    else
    {
        echo "<tr><td><h1>No posees posees un cargo asignado, comunicate con Recursos Humanos...</h1></td></tr>";
    }
?>    
    <tr>
        <td colspan="4">
            <table style="width: 90%; margin-left: 5%;">
            <?php
            
            if(!isset($_POST['ia']))
            {
            
                    if($_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["TipoUsuario"] == "RRHH")
                        $Query = "Select * from AreasDeTrabajo";
                    
                    else
                    {
                        $Query = "SELECT * FROM AreasDeTrabajo AS a
                                  INNER JOIN Cargos AS c ON a.IdAreaDeTrabajo = c.IdArea_Fk
                                  INNER JOIN Jerarquia AS j ON c.IdCargos = j.IdCargos_Fk
                                  WHERE j.IdCargoSuperior = $IdC
                                  Group by a.IdAreaDeTrabajo";
                    }
                                                                                                                                                
                    $Areas = $bddr->prepare($Query);
                    $Areas->execute();
                    
                    
                    $PromedioInstitucional = 0;
                    $PromedioArea = 0;
                    $contadorA = 0;
                    $TotalEmpleadosEvaluados = 0;
                    
                    if($Area->rowCount() > 0)
                    {
                        while($DataA = $Areas->fetch())
                        {
    
                            echo "<tr style='height: 35px;'>
                                    <td colspan='2' style='padding-left: 30px;'>$DataA[1]</td>
                                    <th>
                                        <form action='' method='post'>
                                            <input type='hidden' value='$DataA[0]' name='ia' />
                                            <input type='submit' style='width: 130px' value='Ver Evaluaciones' name='Enviar' class='boton' />
                                        </form>
                                    </th>
                                  </tr>";
                        }
                    }
                    else
                        echo "<tr><td colspan='3'><h3 style='color: red'>No dispones de Empleados a tu cargo...</h3></td></tr>";
                        
            }
            if(isset($_POST['ia']))
            {
                $GetDatos=$bddr->prepare('SELECT NombreAreaDeTrabajo FROM AreasDeTrabajo where IdAreaDeTrabajo = ' . $_POST['ia']);	
                $GetDatos->execute();
                $Nombre = $GetDatos->fetch();
                
                if($_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["TipoUsuario"] == "RRHH" || $IdC == 13)
                    $Query = 'SELECT * FROM Cargos where IdArea_Fk = '. $_POST['ia'] . ' ORDER BY Cargo ASC';
                
                else
                    $Query = "SELECT c . * FROM AreasDeTrabajo AS a
                              INNER JOIN Cargos AS c ON a.IdAreaDeTrabajo = c.IdArea_Fk
                              INNER JOIN Jerarquia AS j ON c.IdCargos = j.IdCargos_Fk
                              WHERE j.IdCargoSuperior = $IdC and a.IdAreaDeTrabajo = " . $_POST['ia'];
                
                
                
            	$GetDatos=$bddr->prepare($Query);	
                $GetDatos->execute();                
            	if ($GetDatos->rowCount()>0)
                {
                    echo "<table style='padding: 5px; text-align:left ;width:100%'>
                          <tr>
                              <th colspan='3'></th>
                              <th>
                                <form action='?l=ResultArea' method='Post'>
                                    <input type='hidden' name='ia' value='".$_POST['ia']."' />
                                    <input type='submit' name='Enviar' value='Detalles de �rea' class='boton'/>
                                </form>
                              </th>
                          </tr>
                          <tr>
                              <th style='width: 35%;'>Nombre del Cargo</th>
                              <th style='width: 30%;'>Asignado a</th>
                              <th style='width: 15%;'>Total de Evaluaciones</th>
                          </tr>
                          <tr><td colspan='4'></td></tr>";
                    
                    while($Cargos = $GetDatos->fetch())
                    {
            
                        $GetEmpleado =$bddr->prepare('SELECT ca . * , em.Nombre1, em.Nombre2, em.Nombre3, em.Apellido1, em.Apellido2, em.IdEmpleado
                                                      FROM CargosAsignacion as ca
                                                      INNER JOIN Empleado AS em ON ca.IdEmpleado = em.IdEmpleado 
                                                      where ca.IdCargo = ' .$Cargos[0]. ' and ca.FechaFin = \'0000-00-00\'' );	
                        $GetEmpleado->execute();
                        
                        if($GetEmpleado->rowCount() > 0)
                        {
                            $nombres = $GetEmpleado->fetch();
                            $iem = $nombres[13];
                            $asignado = "<em style='color: blue;'>" . $nombres[8] . " " . $nombres[9] . " " . $nombres[11]. "</em>";
                        }
                        
                        else
                            $asignado = "<em style='color: red;'>No asignado Actualmente</em>";
                            
                            
                            
                        $EncargadoArea = $bddr->prepare("Select * from AreasDeTrabajoJefes where IdArea = " . $_POST['ia'] . " and IdCargoJefe  = $Cargos[0]");
                        $EncargadoArea->execute();
                        
                        if($EncargadoArea->rowCount() > 0)
                            $AuxCargo = "<em style='color: green'>$Cargos[1]</em>";
                        
                        else
                            $AuxCargo = "$Cargos[1]";
                            
                        
                        echo "<tr><td>$AuxCargo</td><td>$asignado</td>";
                        
                        $Solicitudes = $bddr->prepare("SELECT * FROM Evaluacion_Estado where IdEmpleado = $iem and IdEvaluacion = " . $_SESSION['IdEva'] );
                        $Solicitudes->execute();    
                        
                        echo "   <th style='text-align: center;'> ". $Solicitudes->rowCount() ."</th>
                                 <td>
                                      <form action='?l=ResultStaff' method='Post'>
                                            <input type='hidden' name='IdPosition' value='$Cargos[0]' />
                                            <input type='hidden' name='IdE' value='$iem' />
                                            <input type='hidden' name='Aux1' value='Global' />
                                            <input type='submit' name='Enviar' value='Ver' class='boton'/>
                                      </form>
                                  </td>
                              </tr>";
                    }
                    echo "</table>";
                }
            }                    
            ?>
            </table>
        </td>
    </tr>
    </table>
 </div>