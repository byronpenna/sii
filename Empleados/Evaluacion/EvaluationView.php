<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
   
    if((isset($_POST['IdE']) && isset($_POST['IdC'])))
    {
        $IDE = $_POST['IdE'];
        $IDC = $_POST['IdC'];
        $IdFor = $_POST['IdForm'];
        $Ideva = $_POST['IdEvaluacion'];
        
        
        $_SESSION['IdE'] = $IDE;
        $_SESSION['IdC'] = $IDC;
        $IDF = $_SESSION['IdForm'];
        
        
        $Evaluacion = $bddr->prepare("Select * from Evaluacion where IdEvaluacion = $Ideva");
        $Evaluacion->execute(); 
        $DataEv = $Evaluacion->fetch();       
        $_SESSION['eva'] = $DataEv[1];
        $_SESSION['ideva'] = $Ideva;
        
 
            
                
        $DataEmpleados = $bddr->prepare("Select e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.* 
                                         from Empleado as e
                                         inner join CargosAsignacion as ca on ca.IdEmpleado = e.IdEmpleado
                                         inner join Cargos as c on ca.IdCargo = c.IdCargos  
                                         where e.IdEmpleado = $IDE and c.IdCargos = $IDC  and FechaFin = '0000-00-00'");
        $DataEmpleados->execute();                                
        $DatosEmpleados = $DataEmpleados->fetch();
        
        $Area = $bddr->prepare("Select * from AreasDeTrabajo where IdAreaDeTrabajo = $DatosEmpleados[8]");
        $Area->execute();
            
        $DataA = $Area->fetch();
    }
    else
        Redireccion("Empleado.php?l=Evaluation");       

?>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px"> 
    <table style="width: 100%; ">
    
    <?php


            echo "<tr><td style='text-align: right;'><h2>Evaluaci�n de Desempe�o</h2></td></tr>
                  <tr>
                    <td>
                        <table style='width: 94%; margin-left: 3%; '>
                        <tr><td colspan='2' style='color: blue; '> Empleado a Evaluar</td><tr>
                        <tr><td class='tdleft'> Empleado: </td><td>$DatosEmpleados[1] $DatosEmpleados[2] $DatosEmpleados[3] $DatosEmpleados[4] </td></tr>
                        <tr><td class='tdleft'> Area: </td><td>$DataA[1] </td></tr>
                        <tr><td class='tdleft'> Cargo: </td><td> $DatosEmpleados[6] </td></tr>
                        <tr><td class='tdleft'> Evaluaci�n: </td><td> " . $_SESSION['eva'] ."</td></tr>
                        <tr><td colspan='2'><hr color='skyblue' /></td></tr>";
            
           $Formularios = $bddr->prepare("Select * from Evaluacion_Formulario where IdFormulario = $IdFor");
           $Formularios->execute();
           $DataF = $Formularios->fetch();
                
           echo "       <form action='Empleados/Evaluacion/EvaluationABM.php' method='Post'>
                        <tr><td colspan='2' style='color: blue;'>Formulario de Evaluaci�n</td></tr>
                        <tr><td class='tdleft'>Formulario:</td>
                            <input type='hidden' name='IdC' value='$IDC' />
                            <input type='hidden' name='IdE' value='$IDE' />
                            <input type='hidden' name='IdForm' value='$DataF[0]' />                             
                            <td>$DataF[1]</td>
                        </tr>
                        <tr><td colspan='2'><hr color='skyblue' /></td></tr>
                        <tr><td colspan='2' style='color: blue'>Instrucciones</td></tr>
                        <tr>
                            <td colspan='2'>
                                Se detallan los factores a evaluar con su respectivos criterios, 
                                los cuales todos deben ser calificados de acuerdo a la escala siguiente: <br /><br />
                                <ol style='margin-left: 60px;'>
                                    <li>1 - Deficiente </li>
                                    <li>2 - Regular </li>
                                    <li>3 - Buen Dominio</li>
                                    <li>4 - Muy Buen Dominio</li>
                                    <li>5 - Excelente</li>
                                </ol>
                                <br />Seleccionar la opcion para que se emite la calificaci�n.
                                <br />Procura finalizar la evaluaci�n una vez iniciada, de interrumpirla avisa a Tecnolog�a e Innovaci�n para retomarla. 
                            </td>
                        </tr>									

                        <tr style='margin-top: 20px;'><td colspan='2' style='text-align: center;'><input type='submit' value='Ver Evaluaci�n' name='Enviar' class='botonG' /></td></tr>
                        </form>
                        </table>
                    </td>
                  </tr>";
    ?>
    </table>
    <br /><br />
 </div>