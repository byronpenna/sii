<?php


/**
 * @author Lilian Cordero
 * @copyright 2017
 */


  require '../../net.php';
    
    if(isset($_POST['Enviar']))
    {    
        if($_POST['Enviar'] == "Enviar")
        {
            $Addrequest = $bddr->prepare("Insert into cuestionario_RRHH 
            values (Null, :idEmpleado, :anios, :meses, :puesto1, :anio1, :puesto2, :anio2, :puesto3, :anio3, :perfil)");
           	$Addrequest->bindParam(':idEmpleado', $_SESSION["IdUsuario"]);
            $Addrequest->bindParam(':anios', $_POST['antiguedad']);
            $Addrequest->bindParam(':meses', $_POST['meses']);
            $Addrequest->bindParam(':puesto1',$_POST['puesto1']);
            $Addrequest->bindParam(':anio1', $_POST['anio1']);
            $Addrequest->bindParam(':puesto2', $_POST['puesto2']);
            $Addrequest->bindParam(':anio2', $_POST['anio2']);
            $Addrequest->bindParam(':puesto3', $_POST['puesto3']);
            $Addrequest->bindParam(':anio3', $_POST['anio3']);
            $Addrequest->bindParam(':perfil', $_POST['perfil']);
         
            $Addrequest->execute();

            $ok = $bddr->lastInsertId();

            $curso = isset($_POST['curso1']) ? $_POST['curso1'] : array();
            $temp = isset($_POST['tiempo1']) ? $_POST['tiempo1'] : array();   
            $orga = isset($_POST['organizador1']) ? $_POST['organizador1'] : array(); 
            $finan = isset($_POST['financiado']) ? $_POST['financiado'] : array();
            $inst = isset($_POST['otrasIns']) ? $_POST['otrasIns'] : array();

            
           for ($index = 0 ; $index < count($temp); $index++) 
            {                                
                $AddMaterial = $bddr->prepare("Insert into cuestionario_RH values (Null, :idrrhh, :Nombre, :Duracion, :Organizado, :Financiado, :Especifique)");
                $AddMaterial->bindParam(':idrrhh', $ok);
                $AddMaterial->bindParam(':Nombre',  $curso[$index]);
                $AddMaterial->bindParam(':Duracion', $temp[$index]);
                $AddMaterial->bindParam(':Organizado',  $orga[$index]);
                $AddMaterial->bindParam(':Financiado', $finan[$index]);
                $AddMaterial->bindParam(':Especifique',  $inst[$index]);
                $AddMaterial->execute();
           }

            $curso2 = isset($_POST['curso2']) ? $_POST['curso2'] : array();
            $temp2 = isset($_POST['tiempo2']) ? $_POST['tiempo2'] : array();   
            $orga2 = isset($_POST['organizador2']) ? $_POST['organizador2'] : array(); 
            $finan2 = isset($_POST['financiado2']) ? $_POST['financiado2'] : array();
            $inst2 = isset($_POST['otrasIns2']) ? $_POST['otrasIns2'] : array();

            
           for ($index = 0 ; $index < count($temp); $index++) 
            {                                
                $AddMaterial2 = $bddr->prepare("Insert into cuestionario_CT values (Null, :idrrhh, :Nombre, :Duracion, :Organizado, :Financiado, :Especifique)");
                $AddMaterial2->bindParam(':idrrhh', $ok);
                $AddMaterial2->bindParam(':Nombre',  $curso2[$index]);
                $AddMaterial2->bindParam(':Duracion', $temp2[$index]);
                $AddMaterial2->bindParam(':Organizado',  $orga2[$index]);
                $AddMaterial2->bindParam(':Financiado', $finan2[$index]);
                $AddMaterial2->bindParam(':Especifique',  $inst2[$index]);
                $AddMaterial2->execute();
           }

             $cur = isset($_POST['curso3']) ? $_POST['curso3'] : array();

            
            for ($index = 0 ; $index < count($cur); $index++) 
            {                                
                $AddMaterial3 = $bddr->prepare("Insert into cuestionario_NRH values (Null, :idrrhh, :Nombre)");
                $AddMaterial3->bindParam(':idrrhh',$ok);
                $AddMaterial3->bindParam(':Nombre',  $cur[$index]);
                $AddMaterial3->execute();
            }
            
            $cuer = isset($_POST['curso4']) ? $_POST['curso4'] : array();

            
            for ($index = 0 ; $index < count($cuer); $index++) 
            {                                
                $AddMaterial4 = $bddr->prepare("Insert into cuestionario_NCT values (Null, :idrrhh, :Nombre)");
                $AddMaterial4->bindParam(':idrrhh',$ok);
                $AddMaterial4->bindParam(':Nombre',  $cuer[$index]);
                $AddMaterial4->execute();
            }

            $cursoj = isset($_POST['curso5']) ? $_POST['curso5'] : array();
            $empleadojf = isset($_POST['empleado']) ? $_POST['empleado'] : array();

            for ($index = 0 ; $index < count( $cursoj); $index++) 
            {                                
                $AddMaterial5 = $bddr->prepare("Insert into cuestionario_Jefes values (Null, :idrrhh, :Nombre, :Empleado)");
                $AddMaterial5->bindParam(':idrrhh',$ok);
                $AddMaterial5->bindParam(':Nombre',  $cursoj[$index]);
                $AddMaterial5->bindParam(':Empleado',  $empleadojf[$index]);
                $AddMaterial5->execute();
            }

            $cursoj2 = isset($_POST['curso6']) ? $_POST['curso6'] : array();
            $empleadojf2 = isset($_POST['empleado2']) ? $_POST['empleado2'] : array();

            for ($index = 0 ; $index < count( $cursoj2); $index++) 
            {                                
                $AddMaterial5 = $bddr->prepare("Insert into cuestionario_Jefeshum values (Null, :idrrhh, :Nombre, :Empleado)");
                $AddMaterial5->bindParam(':idrrhh',$ok);
                $AddMaterial5->bindParam(':Nombre',  $cursoj2[$index]);
                $AddMaterial5->bindParam(':Empleado',  $empleadojf2[$index]);
                $AddMaterial5->execute();
            }
            
            $jerarquia = $bddr->prepare("Insert into cuestionario_Resultados values (Null, :IdCuestionario, :Fecha, :IdEmpleado, :IdAsignacionCargo, :IdJefe, :Area)");
            $jerarquia->bindParam(':IdCuestionario', $ok);
            $jerarquia->bindParam(':Fecha', date("Y-m-d"));
            $jerarquia->bindParam(':IdEmpleado',$_POST['usuario']);
            $jerarquia->bindParam(':IdAsignacionCargo',$_POST['cargo']);
            $jerarquia->bindParam(':IdJefe', $_POST['jefe']);
            $jerarquia->bindParam(':Area', $_POST['area']);
        
            $jerarquia->execute();

            

            $MailEmpleado = $bddr->prepare("Select Correo FROM DatosInstitucionales WHERE IdEmpleado = " . $_POST['idj']);
            $MailEmpleado->execute();
            $DataM = $MailEmpleado->fetch();

           // $to = "$DataM[0]";    
           //$to = "liligrc9221@gmail.com";                  
            $subject = "Solicitud de Requisición";
            
            $message = "
                <h2>Solicitud de Requisición</h2><br />
            
                Nombre del Empleado: <br /> ".$_POST['NombreEmpleado']." <br /><br />
                Actividad: <br /> ".$_POST['actividad']." <br /><br />
                Fecha de entrega: <br /> ".$_POST['fecha']." <br /><br />
                Comentarios: <br /> ".$_POST['comentarios']." <br /><br />
                
                Materiales Solicitados <br/>
               <table style='width: 70%'>
               <tr><th>Cantidad</th><th>Material</th><th>Especificación Tecnica</th></tr>";
                
                for ($index = 0 ; $index < count($cantidad); $index++) 
                {    
                    $message .= "
                                <tr>
                                   <th>$cantidad[$index] </th>
                                   <th>$material[$index]</th>
                                   <th>$espe[$index]</th>
                                </tr>";
                }
                $message .= "</table>";                
                $message .= "<br /><br />Acepta o deniega dando <a href='https://siifusalmo.org/RequisicionView.php?idr=$id'> clic aquí </a>";              

            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org, liligrc9221@gmail.com," . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            mail($to,$subject,$message,$headers);  
            Redireccion("../../Empleado.php");
        }
        
        else if($_POST['Enviar'] == "Actualizar")
        {
           
        }    
        
        else if($_POST['Enviar'] == "Eliminar")
        {
          
        }     

      
             
     
 }

  //Redireccion("../../Empleado.php?l=Request&n=2");

?>