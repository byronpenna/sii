<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["add_record"])) {

	$alumno = $_POST['post_alumno'];
	$tipomatricula = $_POST['post_tipomatricula'];
	$grupo = $_POST['post_grupo'];
	$monto = $_POST['post_monto'];
	$convivio = $_POST['post_convivio'];
	$fechainscripcion = $_POST['post_fechainscripcion'];


	$sql = " call insert_inscripcion(:idalumno, :idgrupo, :idtipomatricula, :monto, :convivio, :idsede, :fechainscripcion)";
	$pdo_statement = $pdo_conn->prepare( $sql );
	$result = $pdo_statement->execute( 
			array( 
					':idalumno'=>$alumno, 
					':idgrupo'=>$grupo,
					':idtipomatricula'=>$tipomatricula, 
					':monto'=>$monto,
					':convivio'=>$convivio,
					':idsede'=>$idsede,
					':fechainscripcion'=>$fechainscripcion
			) 
		);
	
	if (!empty($result) ){
	  header('location:add_inscripcion.php');
	}//if
	
}//if(!empty($_POST["add_record"]))
else{
	//PARA LLENAR LOS GRUPOS
	$pdo_statement = $pdo_conn->prepare("SELECT g.idgrupo,g.grupo,a.anio FROM grupo g
		JOIN
		(SELECT idanio,anio, idsede
		FROM anio WHERE borrado = 0 AND estado = 1 and idsede = ".$idsede." ORDER BY anio DESC
		LIMIT 1
		) a
	ON
	a.idanio = g.idanio
	and a.idsede = g.idsede
	WHERE
	g.borrado = 0
	and g.idsede = ".$idsede."
	ORDER BY
		g.grupo");
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();

	//PARA LLENAR LOS ALUMNOS QUE NO HAYAN SIDO INSCRITOS EN EL AÑO ACTIVO  
	$pdo_statement1 = $pdo_conn->prepare("SELECT f.idficha,f.nombrealumno, ins.idalumno, ins.idgrupo, ins.idanio FROM ficha f 
LEFT JOIN
(SELECT i.idalumno, g.idgrupo, a.idanio, i.idsede FROM 
	inscripcion i

 JOIN
	grupo AS g
ON
	i.idgrupo = g.idgrupo
	and i.idsede = g.idsede
	AND g.borrado	 = 0
JOIN
		(
			SELECT  * FROM anio
			WHERE estado = 1
			AND borrado = 0
			and idsede = ".$idsede."
			ORDER BY anio DESC
			LIMIT 1
		)AS a
	ON
		a.idanio = g.idanio
		and a.idsede = g.idsede
WHERE i.borrado		 = 0
	and i.idsede = ".$idsede."
) AS ins
ON
	ins.idalumno = f.idficha
	and ins.idsede = f.idsede
		WHERE f.borrado = 0 and f.idsede = ".$idsede." AND f.activo = 1
		AND ins.idalumno IS NULL
		AND ins.idgrupo IS NULL
		AND ins.idanio IS NULL
	ORDER BY
		f.nombrealumno
		");
	$pdo_statement1->execute();
	$result1 = $pdo_statement1->fetchAll();

	//PARA LLENAR LOS TIPOS DE MATRICULA
	$pdo_statement2 = $pdo_conn->prepare("SELECT t.idtipomatricula,t.tipomatricula FROM tipomatricula t 
		WHERE borrado = 0 and idsede = ".$idsede);
	$pdo_statement2->execute();
	$result2 = $pdo_statement2->fetchAll();

}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Nueva Inscripción</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
		<main role="main" class="container">
		<div class="container">
		<h1>Nueva Inscripción</h1>
		<form name="frmAdd" action="" method="POST">
		  	<div class="form-group">
	  			<label>Alumno: </label><br>
			  	<select name = "post_alumno" class="js-example-basic-single form-control" style="width: 50%" required>
			  	<?php
					if(!empty($result1)) { 
						foreach($result1 as $row1) {
				?>
	  				<option value="<?php echo $row1["idficha"]; ?>"><?php echo $row1["nombrealumno"]; ?></option>	 	  	
		    	<?php
					}//foreach
				}//if
				?>
				</select>
			</div>

			<div class="form-group">
	  			<label>Tipo de Matrícula: </label><br>
			  	<select name = "post_tipomatricula" class="js-example-basic-single form-control" style="width: 50%" required>
			  	<?php
					if(!empty($result2)) { 
						foreach($result2 as $row2) {
				?>
	  				<option value="<?php echo $row2["idtipomatricula"]; ?>"><?php echo $row2["tipomatricula"]; ?></option>	 	  	
		    	<?php
					}//foreach
				}//if
				?>
				</select>
			</div>

		  	<div class="form-group">
	  			<label>Grupo: </label><br>
			  	<select name = "post_grupo" class="js-example-basic-single form-control" style="width: 50%" required>
			  	<?php
					if(!empty($result)) { 
						foreach($result as $row) {
				?>
	  				<option value="<?php echo $row["idgrupo"]; ?>"><?php echo $row["grupo"]; ?> - <?php echo $row["anio"]; ?></option>	 	  	
		    	<?php
					}//foreach
				}//if
				?>
				</select>
			</div>

			<div class="form-group">
			  	<label>Monto: </label><br>
			  	<input type="number" step="0.01" name="post_monto" class="form-control" min="0.00" max="1000.00" value = "0.00"/>
		  	</div>

	  		<div class="form-check">
			  	<input type='hidden' value='0' name='post_convivio'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_convivio' value="1" class="form-check-input">
			    		Cancelación de Convivio
			    	</label>
			</div>	

			<div class="form-group">
	  			<label>Fecha de Inscripción : </label>
			  	<input name="post_fechainscripcion" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required />
			</div>

		  	<div class="form-group">
			  	<input name="add_record" type="submit" value="Añadir" class="btn btn-primary">
			</div>
		</form>
	</div>
</main>
	<?php include ("footer_js.php"); ?>	
	<script type="text/javascript">

		// In your Javascript (external .js resource or <script> tag)
		$(document).ready(function() {
		    $('.js-example-basic-single').select2({
		    		language: "es"
		    	}
	    	);

		});
	</script>
</body>

</html>