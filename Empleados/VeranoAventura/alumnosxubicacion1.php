<?php
require_once("db.php");
$pdo_statement = $pdo_conn->prepare("call alumnosxubicacion");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
$cuentaFilas = $pdo_statement->rowCount();
$pdo_statement->closeCursor();

$pdo_statement1 = $pdo_conn->prepare("SELECT anio,tema
		FROM anio WHERE borrado = 0 AND estado = 1 ORDER BY anio DESC
		LIMIT 1");
$pdo_statement1->execute();
$result1 = $pdo_statement1->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Listado de Alumnos Inscritos por Ubicación Geográfica</title>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<?php
	if(!empty($result1)) { 
		foreach($result1 as $row1) {
		?>
			<div><h2> Listado de Alumnos Inscritos por Ubicación Geográfica<br> 
				Año : <?php echo $row1["anio"]; ?><br>
  				Tema : <?php echo $row1["tema"]; ?></h2><br>
  				<h3>Total de Alumnos - <?php echo $cuentaFilas; ?>  </h3>
			</div>
    <?php
		}
	}
	?>
	<table class="tbl-qa">
	  	<thead>
			<tr>
				
				<th class="table-header" width="10%">Nombre</th>
				<th class="table-header" width="6%">Municipio</th>
			</tr>
	  	</thead>
		<tbody id="table-body">
		<?php

			if(!empty($result)) { 
				$current_grupo = null;
				$current_activo = null;
				foreach($result as $row) {
					if ($row["departamento"] != $current_grupo) {
						$current_activo = null;
    					$current_grupo = $row["departamento"];
    					?>
   						<tr class="table-row">
							<td><b>Departamento <?php echo $row["departamento"]; ?> - Total <?php echo $row["totaldepartamento"]; ?></b></td>
							<td></td>
						</tr>	
    					<?php
  					}
  					?>
  						<tr class="table-row">
							<td><?php echo $row["nombrealumno"]; ?></td>
							
							<td><?php echo $row["municipio"]; ?></td>
						</tr>
				<?php	
				}	
			}
		?>
		</tbody>
	</table>
</body>
</html>