<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["save_record"])) {
	$pdo_statement=$pdo_conn->prepare("update grupo set 
				grupo='" . $_POST[ 'post_nombre_grupo' ] . 
				"' where borrado = 0 and idsede = ".$idsede." and idgrupo=" . $_GET["idgrupo"]);
	$result = $pdo_statement->execute();
	if($result) {
		header('location:grupo.php');
	}//if
}//if
else{
	$pdo_statement = $pdo_conn->prepare("SELECT g.idgrupo,g.grupo,a.anio, g.borrado
FROM grupo AS g
	INNER JOIN
		(
			SELECT  * FROM anio
			WHERE estado = 1
			AND borrado = 0
			and idsede = ".$idsede."
			ORDER BY anio DESC
			LIMIT 1
		)AS a
	ON
		a.idanio = g.idanio
		and a.idsede = g.idsede
	WHERE g.borrado = 0 and g.idsede = ".$idsede." and g.idgrupo=" . $_GET["idgrupo"]);
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();

}//else

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Editar Grupo</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
		<h1>Editar Grupo</h1>
		<form name="frmAdd" action="" method="POST">
			<div class="form-group">
			  	<label>Nombre del Grupo: </label><br>
			  	<textarea name="post_nombre_grupo" class="form-control"  rows="5" maxlenght="255" required ><?php echo $result[0]['grupo']; ?></textarea>
		  	</div>
		  	<div class="form-group">
			  	<label>Año: </label><br>
			  	<input type="number" name="post_anio" class="form-control"  min="2000" max="2050" 
			  	value="<?php echo $result[0]['anio']; ?>"
			  	required readonly/>
		  	</div>

		  	<div class="form-group">
			  	<input name="save_record" type="submit" value="Guardar cambios" class="btn btn-primary"
			  	onclick="return confirm('Está seguro que desea guardar los cambios ?')">
			</div>
		</form>
	</div>
</main>
	<?php include ("footer_js.php"); ?>	
</body>
</html>