<?php
session_start(); 
require_once("db.php");
if(!empty($_POST["submit_sede"])) {
	$sede = $_POST["post_sede"];
	$_SESSION["sede"] = $sede; 
	$pdo_statement1 = $pdo_conn->prepare("SELECT 
	s.nombresede,a.anio
	FROM sede AS s
	INNER JOIN
		(
			SELECT  * FROM anio
			WHERE estado = 1
			AND borrado = 0
			AND idsede = '".$sede."'
			ORDER BY anio DESC
			LIMIT 1
		)AS a
	ON
		a.idsede = s.idsede
	
	WHERE s.borrado = 0 
	AND s.idsede = '".$sede."' ");
	$pdo_statement1->execute();
	$result1 = $pdo_statement1->fetchAll();
	$_SESSION["nombresede"] = $result1[0]['nombresede']; 
	$_SESSION["anioactivo"] = $result1[0]['anio']; 
	header('location:menu.php');
}//if
else{
	//INICIALIZA LAS SEDES
	session_unset(); 
	session_destroy(); 

	//LLENA LAS SEDES EN EL SELECT
	$pdo_statement = $pdo_conn->prepare("SELECT idsede, nombresede FROM sede WHERE borrado = 0 order by idsede");
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();

}//else

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Seleccionar Sede</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<div class="container">
		<h1 class="demo-form-heading">Seleccionar Sede</h1>
		<form name="frmAdd" action="" method="POST">

			<div class="form-group">
	  			<label>Sede: </label><br>
			  	<select name = "post_sede" class="form-control" required>
			  	<?php
					if(!empty($result)) { 
						foreach($result as $row) {
				?>
	  				<option value="<?php echo $row["idsede"]; ?>"><?php echo $row["nombresede"]; ?></option>	 	  	
		    	<?php
					}//foreach
				}//if
				?>
				</select>
			</div>
			<div class="demo-form-row">
			  	<input name="submit_sede" type="submit" value="Seleccionar" class="btn btn-primary">
			</div>
		</form>
	</div> 
	<?php include ("footer_js.php"); ?>
</body>
</html>