<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["save_record"])) {

	$pdo_statement=$pdo_conn->prepare(
		"update ficha set 
			nombrealumno='".$_POST[ 'post_nombre' ]."', 
			fechanacimiento='".$_POST[ 'post_fechanac' ]."', 
			sexo='".$_POST[ 'post_sexo' ]."', 
			direccion='".$_POST[ 'post_direccion' ]."', 
			municipio='".$_POST[ 'post_municipio' ]."', 
			departamento='".$_POST[ 'post_departamento' ]."', 
			responsable='".$_POST[ 'post_responsable' ]."', 
			parentescoresponsable='".$_POST[ 'post_parentescoresponsable' ]."', 
			telefonoresponsable='".$_POST[ 'post_telefonofijo' ]."', 
			celularresponsable='".$_POST[ 'post_celular' ]."', 
			centroestudios='".$_POST[ 'post_centroestudios' ]."', 
			gradoacademico='".$_POST[ 'post_gradoacademico' ]."', 
			alergia='".$_POST[ 'post_alergia' ]."', 
			asma='".$_POST[ 'post_asma' ]."', 
			bronquitis='".$_POST[ 'post_bronquitis' ]."', 
			epilepsias='".$_POST[ 'post_epilepsias' ]."', 
			fobias='".$_POST[ 'post_fobias' ]."', 
			convulsiones='".$_POST[ 'post_convulsiones' ]."', 
			gastritis='".$_POST[ 'post_gastritis' ]."', 
			colitis='".$_POST[ 'post_colitis' ]."', 
			cardiacos='".$_POST[ 'post_cardiacos' ]."', 
			otros='".$_POST[ 'post_otros' ]."',
			activo='".$_POST[ 'post_activo' ]."',
			voluntario ='".$_POST[ 'post_voluntario' ]."'
		where
			borrado = 0 and idsede = ".$idsede." and idficha=" . $_GET["idficha"]);

	$result = $pdo_statement->execute();
	if($result) {
		header('location:ficha.php');
	}//if
}//if
else{
	$pdo_statement = $pdo_conn->prepare(
		"SELECT 
			nombrealumno, 
			fechanacimiento, 
			sexo, 
			direccion, 
			municipio, 
			departamento, 
			responsable, 
			parentescoresponsable, 
			telefonoresponsable, 
			celularresponsable, 
			centroestudios, 
			gradoacademico, 
			alergia, 
			asma, 
			bronquitis, 
			epilepsias, 
			fobias, 
			convulsiones, 
			gastritis, 
			colitis, 
			cardiacos, 
			otros,
			activo,
			voluntario
		FROM ficha 
		where borrado = 0 and idsede = ".$idsede." and idficha=" . $_GET["idficha"]);
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();
}//else
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Editar Ficha de Alumno</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
		<main role="main" class="container">
	<div class="container">
	
		<h1>Editar Ficha de Alumno</h1>

		<form name="frmAdd" action="" method="POST">
			<div class="form-group">
	  			<label>Nombre : </label><br>
			  	<input name="post_nombre" maxlength = "255" type="text" class="form-control" 
					value="<?php echo $result[0]['nombrealumno']; ?>"
			  		required />
			</div>

			<div class="form-group">
	  			<label>Fecha de Nacimiento : </label><br>
			  	<input name="post_fechanac" type="date" class="form-control" 
			  		value="<?php echo $result[0]['fechanacimiento']; ?>"
			  		required />
			</div>

			<fieldset class="form-group">
			    <legend>Sexo</legend>
			    <div class="form-check">
			      <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_sexo" id="id_post_sexo1" value="M" <?php if($result[0]['sexo']=="M") echo 'checked';?>>
			        Masculino
			      </label>
			    </div>
			    <div class="form-check">
			    <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_sexo" id="id_post_sexo2" value="F" <?php if($result[0]['sexo']=="F") echo 'checked';?>>
			        Femenino
			      </label>
			    </div>
			 </fieldset>

		  	<div class="form-group">
			  	<label>Dirección: </label><br>
			  	<textarea name="post_direccion" class="form-control" rows="5" maxlenght="255" ><?php echo $result[0]['direccion']; ?></textarea>
		  	</div>

		  	<div class="form-group">
	  			<label>Municipio: </label><br>
			  	<input name="post_municipio" maxlength = "30" type="text" class="form-control"
			  		value="<?php echo $result[0]['municipio']; ?>" />
			</div>
	  		
	  		<div class="form-group">
	  			<label>Departamento: </label><br>
			  	<input name="post_departamento" maxlength = "30" type="text" class="form-control"
			  		value="<?php echo $result[0]['departamento']; ?>" />
			</div>

			<div class="form-group">
	  			<label>Responsable: </label><br>
			  	<input name="post_responsable" maxlength = "255" type="text" class="form-control"
			  		value="<?php echo $result[0]['responsable']; ?>" />
			</div>

			<div class="form-group">
	  			<label>Parentesco del Responsable: </label><br>
			  	<input name="post_parentescoresponsable" maxlength = "45" type="text" class="form-control"
			  		value="<?php echo $result[0]['parentescoresponsable']; ?>" />
			</div>

		  	<div class="form-group">
			  	<label>Teléfono Fijo: </label><br>
			  	<input type="tel" maxlength = "15" name="post_telefonofijo" class="form-control" 
			  		value="<?php echo $result[0]['telefonoresponsable']; ?>" />
		  	</div>

		  	<div class="form-group">
			  	<label>Celular: </label><br>
			  	<input type="tel" maxlength = "15" name="post_celular" class="form-control" 
			  		value="<?php echo $result[0]['celularresponsable']; ?>" />
		  	</div>

		  	<div class="form-group">
			  	<label>Centro de Estudios: </label><br>
			  	<input type="text" maxlength = "255" name="post_centroestudios" class="form-control" 
			  		value="<?php echo $result[0]['centroestudios']; ?>" />
		  	</div>

		  	<div class="form-group">
			  	<label>Grado Académico: </label><br>
			  	<input type="text" maxlength = "45" name="post_gradoacademico" class="form-control" 
			  		value="<?php echo $result[0]['gradoacademico']; ?>" />
		  	</div>

		  	<fieldset class="form-group">
			    <legend>DATOS MÉDICOS:</legend>
		  		<div class="form-check">
		  			<input type='hidden' value='0' name='post_alergia'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name="post_alergia" value="1" class="form-check-input" <?php if($result[0]['alergia']=="1") echo 'checked';?>>
			    		Alergia
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_asma'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_asma' value="1" class="form-check-input" <?php if($result[0]['asma']=="1") echo 'checked';?>>
			    		Asma
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_bronquitis'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_bronquitis' value="1" class="form-check-input" <?php if($result[0]['bronquitis']=="1") echo 'checked';?>>
			    		Bronquitis
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_epilepsias'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_epilepsias' value="1" class="form-check-input" <?php if($result[0]['epilepsias']=="1") echo 'checked';?>>
			    		Epilepsias
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_fobias'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_fobias' value="1" class="form-check-input" <?php if($result[0]['fobias']=="1") echo 'checked';?>>
			    		Fobias/Miedos
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_convulsiones'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_convulsiones' value="1" class="form-check-input" <?php if($result[0]['convulsiones']=="1") echo 'checked';?>>
			    		Convulsiones
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_gastritis'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_gastritis' value="1" class="form-check-input" <?php if($result[0]['gastritis']=="1") echo 'checked';?>>
			    		Gastritis
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_colitis'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_colitis' value="1" class="form-check-input" <?php if($result[0]['colitis']=="1") echo 'checked';?>>
			    		Colitis
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_cardiacos'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_cardiacos' value="1" class="form-check-input" <?php if($result[0]['cardiacos']=="1") echo 'checked';?>>
			    		Padecimientos Cardíacos
			    	</label>
			 	</div>
			</fieldset>

		  	<div class="form-group">
			  	<label>Otros: </label><br>
			  	<textarea name="post_otros" class="form-control" rows="5" maxlenght="255"><?php echo $result[0]['otros']; ?></textarea>
		  	</div>

		  	<fieldset class="form-group">
			    <legend>Tipo</legend>
			    <div class="form-check">
			      <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_voluntario" id="id_post_voluntario1" value="0" <?php if($result[0]['voluntario']=="0") echo 'checked';?>>
			        Estudiante
			      </label>
			    </div>
			    <div class="form-check">
			    <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_voluntario" id="id_post_voluntario2" value="1" <?php if($result[0]['voluntario']=="1") echo 'checked';?>>
			        Voluntario
			      </label>
			    </div>
			 </fieldset>

	  		<div class="form-check">
			  	<input type='hidden' value='0' name='post_activo'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_activo' value="1" class="form-check-input" <?php if($result[0]['activo']=="1") echo 'checked';?>>
			    		Activo
			    	</label>
			</div>	

		  	<div class="form-group">
			  	<input name="save_record" type="submit" value="Guardar cambios"  class="btn btn-primary"
			  	onclick="return confirm('Está seguro que desea guardar los cambios ?')">
			</div>
		</form>
	</div>
		</main>
	<?php include ("footer_js.php"); ?>
</body>
</html>