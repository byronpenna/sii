<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"];
$anioactivo = $_SESSION["anioactivo"];

$postsede = $_POST['post_sede'];
$postanio = $_POST['post_anio'];
$postgrupo = $_POST['post_grupo'];

require_once("db.php");
$pdo_statement = $pdo_conn->prepare("call desercionesxgrupo ('".$postsede."','".$postanio."','".$postgrupo."')");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
$cuentaFilas = $pdo_statement->rowCount();
$pdo_statement->closeCursor();

$pdo_statement1 = $pdo_conn->prepare("SELECT a.anio,a.tema, s.nombresede, g.grupo
		FROM anio AS a
			JOIN
				sede AS s
			ON
				s.idsede			 = a.idsede			
				AND s.borrado				 = 0
			JOIN
				grupo AS g
			ON
				g.idanio			= a.idanio			
				AND g.borrado				 = 0
				AND g.idsede				= s.idsede				
				
		 WHERE a.borrado = 0 AND a.idsede = '".$postsede."' AND a.idanio = '".$postanio."' AND g.idgrupo = '".$postgrupo."' 
		 ORDER BY anio DESC
		 LIMIT 1");
$pdo_statement1->execute();
$result1 = $pdo_statement1->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Listado de Deserciones por Grupo</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">

	<?php
	if(!empty($result1)) { 
		foreach($result1 as $row1) {
		?>
			<div><h2> Listado de Deserciones por Grupo</h2><br> 
				<h3>Sede : <?php echo $row1["nombresede"]; ?><br>
				Año : <?php echo $row1["anio"]; ?><br>
  				Tema : <?php echo $row1["tema"]; ?><br>
  				Grupo : <?php echo $row1["grupo"]; ?></h3><br>
  				<h3>Total de Alumnos - <?php echo $cuentaFilas; ?>  </h3>
			</div>
    <?php
		}
	}
	?>
	<table class="table table-hover">
	  	<thead>
			<tr>
				
				<th scope="col">Nombre</th>
				<th scope="col">Fecha de Nacimiento</th>
				<th scope="col">Edad</th>
				<th scope="col">Sexo</th>
				<th scope="col">Responsable</th>
				<th scope="col">Teléfonos</th>
                <th scope="col">Fecha Deserción</th>
			</tr>
	  	</thead>
		<tbody id="table-body">
		<?php

			if(!empty($result)) { 
				$current_grupo = null;
				$current_activo = null;
				foreach($result as $row) {
					if ($row["grupo"] != $current_grupo) {
						$current_activo = null;
    					$current_grupo = $row["grupo"];
    					?>
   						<tr class="table-row">
							<td><b><?php echo $row["grupo"]; ?> </b></td>
							
						</tr>
							
    					<?php
  					}
  					?>
  						<tr>
							<td><?php echo $row["nombrealumno"]; ?></td>
							<td><?php echo date_format(date_create($row["fechanacimiento"]), 'd/m/Y'); ?></td>
							<td><?php echo $row["edad"]; ?></td>
							<td><?php if($row["sexo"]=="M")  echo 'Masculino'; else echo 'Femenino' ; ?></td>
							<td><?php echo $row["responsable"]; ?></td>
							<td><?php echo $row["telefonoresponsable"]; ?> / <?php echo $row["celularresponsable"]; ?></td>
                            <td><?php echo date_format(date_create($row["fechadesercion"]), 'd/m/Y'); ?></td>
                        </tr>
				<?php	
				}	
				?>
			
			<?php
			}
		?>
		</tbody>
	</table>
	</div>
</main>
	<?php include ("footer_js.php"); ?>
</body>
</html>