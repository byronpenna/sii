<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["save_record"])) {
	if($_POST[ 'post_desercion' ] == "1")
		$fechadesercion = $_POST[ 'post_fechadesercion' ];
	else
		$fechadesercion = "1900-01-01";
	$pdo_statement=$pdo_conn->prepare(
		"call update_inscripcion (
			'".$_POST[ 'post_tipomatricula' ]."', 
			'".$_POST[ 'post_grupo' ]."', 
			'".$_POST[ 'post_monto' ]."', 
			'".$_POST[ 'post_convivio' ]."', 
			'".$_POST[ 'post_desercion' ]."',
			'".$_POST[ 'post_fechainscripcion' ]."',
			'".$fechadesercion."',
			'".$idsede."',
			'".$_GET["idinscripcion"]."')");
	/*echo("call update_inscripcion (
			'".$_POST[ 'post_tipomatricula' ]."', 
			'".$_POST[ 'post_grupo' ]."', 
			'".$_POST[ 'post_monto' ]."', 
			'".$_POST[ 'post_convivio' ]."', 
			'".$_POST[ 'post_desercion' ]."',
			'".$_POST[ 'post_fechainscripcion' ]."',
			'".$fechadesercion."',
			'".$idsede."',
			'".$_GET["idinscripcion"]."')");
	*/
	$result = $pdo_statement->execute();
	if($result) {
		header('location:inscripcion.php');
	}//if
}//if
else{
	//PARA LLENAR LOS GRUPOS
	$pdo_statement = $pdo_conn->prepare("SELECT g.idgrupo,g.grupo,a.anio FROM grupo g
		JOIN
		(SELECT idanio,anio, idsede
		FROM anio WHERE borrado = 0 and idsede = ".$idsede." AND estado = 1 ORDER BY anio DESC
		LIMIT 1
		) a
	ON
	a.idanio = g.idanio
	and a.idsede = g.idsede
	WHERE
	g.borrado = 0 and g.idsede =".$idsede);
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();

	//PARA LLENAR LOS TIPOS DE MATRICULA
	$pdo_statement2 = $pdo_conn->prepare("SELECT t.idtipomatricula,t.tipomatricula FROM tipomatricula t 
		WHERE borrado = 0 and idsede = ".$idsede);
	$pdo_statement2->execute();
	$result2 = $pdo_statement2->fetchAll();

	//DATOS DE INSCRIPCION
	$pdo_statement1 = $pdo_conn->prepare(
		"SELECT 
			i.idinscripcion, i.idalumno, f.nombrealumno, i.idgrupo, i.idtipomatricula, i.convivio, i.desercion, i.monto, i.fechainscripcion, i.fechadesercion FROM inscripcion i
		LEFT JOIN
			ficha f
		ON
			f.idficha = i.idalumno
			and f.idsede = i.idsede
			AND f.borrado = 0
		WHERE i.borrado = 0 and i.idsede = ".$idsede." and i.idinscripcion = " . $_GET["idinscripcion"]);
	$pdo_statement1->execute();
	$result1 = $pdo_statement1->fetchAll();
}//else
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Editar Inscripción</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
		<main role="main" class="container">
		<div class="container">
		<h1>Editar Inscripción</h1>
		
		<form name="frmAdd" action="" method="POST">
		  	<div class="form-group">
	  			<label>Alumno: </label><br>
			  	<select name = "post_alumno" class="js-example-basic-single form-control" style="width: 50%" required>
			  	<?php
					if(!empty($result1)) { 
						foreach($result1 as $row1) {
				?>
	  				<option value="<?php echo $row1["idalumno"]; ?>"><?php echo $row1["nombrealumno"]; ?></option>	 	  	
		    	<?php
					}//foreach
				}//if
				?>
				</select>
			</div>

			<div class="form-group">
	  			<label>Tipo de Matrícula: </label><br>
			  	<select name = "post_tipomatricula" class="js-example-basic-single form-control" style="width: 50%" required >
			  	<?php
					if(!empty($result2)) { 
						foreach($result2 as $row2) {
				?>
	  				<option value="<?php echo $row2["idtipomatricula"]; ?>" <?php if($result1[0]['idtipomatricula']== $row2["idtipomatricula"]) echo 'selected';?>><?php echo $row2["tipomatricula"]; ?></option>	 	  	
		    	<?php
					}//foreach
				}//if
				?>
				</select>
			</div>

		  	<div class="form-group">
	  			<label>Grupo: </label><br>
			  	<select name = "post_grupo" class="js-example-basic-single form-control" style="width: 50%" required>
			  	<?php
					if(!empty($result)) { 
						foreach($result as $row) {
				?>
	  				<option value="<?php echo $row["idgrupo"]; ?>" <?php if($result1[0]['idgrupo']== $row["idgrupo"]) echo 'selected';?>><?php echo $row["grupo"]; ?> - <?php echo $row["anio"]; ?></option>	 	  	
		    	<?php
					}//foreach
				}//if
				?>
				</select>
			</div>

			<div class="form-group">
			  	<label>Monto: </label><br>
			  	<input type="number" step="0.01" name="post_monto" class="form-control" min="0.00" max="1000.00" 
			  		value="<?php echo $result1[0]['monto']; ?>"/>
		  	</div>

  			<div class="form-check">
			  	<input type='hidden' value='0' name='post_convivio'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_convivio' value="1" class="form-check-input" <?php if($result1[0]['convivio']=="1") echo 'checked';?>>
			    		Cancelación de Convivio
			    	</label>
			</div>

			<div class="form-group">
	  			<label>Fecha de Inscripción : </label>
			  	<input name="post_fechainscripcion" type="date" class="form-control" value="<?php echo $result1[0]['fechainscripcion']; ?>" required />
			</div>

			<div class="form-check">
			  	<input type='hidden' value='0' name='post_desercion'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_desercion' value="1" class="form-check-input" <?php if($result1[0]['desercion']=="1") echo 'checked';?>>
			    		Deserción
			    	</label>
			</div>

		  	<div class="form-group">
	  			<label>Fecha de Deserción : </label>
			  	<input name="post_fechadesercion" type="date" class="form-control" value="<?php if($result1[0]['fechadesercion'] == "" or $result1[0]['fechadesercion'] == "1900-01-01") echo date('Y-m-d'); else echo $result1[0]['fechadesercion'];  ?>" />
			</div>

		  	<div class="form-group">
			  	<input name="save_record" type="submit" value="Guardar Cambios" class="btn btn-primary"
			  	onclick="return confirm('Está seguro que desea guardar los cambios ?')">
			</div>
		</form>
	</div>
</main>
	<?php include ("footer_js.php"); ?>	
	<script type="text/javascript">

		// In your Javascript (external .js resource or <script> tag)
		$(document).ready(function() {
		    $('.js-example-basic-single').select2({
		    		language: "es"
		    	}
	    	);

		});
	</script>
</body>
</html>