<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"];
$anioactivo = $_SESSION["anioactivo"];

$postsede = $_POST['post_sede'];
$postanio = $_POST['post_anio'];

require_once("db.php");
$pdo_statement = $pdo_conn->prepare("call estadisticasxanioysede ('".$postsede."','".$postanio."')");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
$cuentaFilas = $pdo_statement->rowCount();
$pdo_statement->closeCursor();

$pdo_statement1 = $pdo_conn->prepare("SELECT a.anio,a.tema, s.nombresede
		FROM anio AS a
			JOIN
				sede AS s
			ON
				s.idsede			 = a.idsede			
				AND s.borrado				 = 0
				
		 WHERE a.borrado = 0 AND a.idsede = '".$postsede."' AND a.idanio = '".$postanio."'
		 ORDER BY anio DESC
		 LIMIT 1");
$pdo_statement1->execute();
$result1 = $pdo_statement1->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Estadísticas</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">

	<?php
	if(!empty($result1)) { 
		foreach($result1 as $row1) {
		?>
			<div><h2>Estadísticas</h2><br> 
				<h3>Sede : <?php echo $row1["nombresede"]; ?><br>
				Año : <?php echo $row1["anio"]; ?><br>
  				Tema : <?php echo $row1["tema"]; ?></h3><br>
			</div>
    <?php
		}
	}
	?>
	<table class="table table-hover">
	  	<thead>
			<tr>
				
				<th scope="col">Item</th>
				<th scope="col">Total</th>
			</tr>
	  	</thead>
		<tbody id="table-body">
		<?php

			if(!empty($result)) { 
				foreach($result as $row) {
  					?>
  						<tr>
							<td><?php echo $row["item"]; ?></td>
							<td><?php echo $row["total"]; ?></td>
						</tr>
				<?php	
				}	
				?>
			
			<?php
			}
		?>
		</tbody>
	</table>
	</div>
</main>
	<?php include ("footer_js.php"); ?>
</body>
</html>