<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"];
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["save_record"])) {
	$fecha = $_POST['post_fecha'];
	$asistencia = $_POST['post_asistencia'];

	foreach ($asistencia as $idinscripcion => $value) {
			$pdo_statement1=$pdo_conn->prepare("call asistencia_update('".$idinscripcion."' ,'".$fecha."','".$idsede."','".$value."')");
			$result1 = $pdo_statement1->execute();
		}
	if (!empty($result1) ){
	  header('location:asistencia.php');
	}//if
}//if
else{
		$fecha = $_GET['fechaasistencia'];
		$grupo = $_GET['idgrupo'];
		//PARA LLENAR LOS ESTUDIANTES INSCRITOS
		$pdo_statement = $pdo_conn->prepare("CALL asistencia_edit('".$idsede."','".$fecha."','".$grupo."')");
		
		$pdo_statement->execute();
		$result = $pdo_statement->fetchAll();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Editar Lista de Asistencias</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	
		<h1>Editar Lista de Asistencia </h1>
	<form name="frmAdd" action="" method="POST">
	<div class="form-group">
		<input name="post_fecha" type="date" class="form-control" value="<?php echo $fecha; ?>" required readonly/>
	</div>
	<table class="table table-hover">
	  	<thead>
			<tr>
				
				<th scope="col">Alumnos</th>
				<th scope="col">Asistencia</th>
			</tr>
	  	</thead>
		<tbody id="table-body">
		<?php

			if(!empty($result)) { 
				foreach($result as $row) {
				?>

  						<tr class="table-row">
							<td><?php echo $row["nombrealumno"]; ?><?php echo ($row["voluntario"]==1)?" - Voluntario":""; ?></td>
							<td>
								<input type='hidden' value='0' name="post_asistencia[<?php echo $row['idinscripcion'];?>]">
		  						<input type="checkbox" class="form-check-input" name="post_asistencia[<?php echo $row['idinscripcion'];?>]" value="1" <?php if($row["asiste"]=="1") echo 'checked';?> <?php if($row['desercion']=="1") echo 'disabled';?>> <br>
							</td>
						</tr>
  					
				<?php	
				}	
					
			}
		?>


		</tbody>

	</table>
	<div class="demo-form-row">
			  	<input name="save_record" type="submit" value="Guardar cambios" class="btn btn-primary"
			  	onclick="return confirm('Está seguro que desea guardar los cambios ?')">
	</div>
	</form>
	</div>
		</main>
	<?php include ("footer_js.php"); ?>
</body>
</html>