<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"]; 
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
$pdo_statement = $pdo_conn->prepare("SELECT anio,estado,tema,idanio FROM anio where borrado = 0 and idsede = ".$idsede." ORDER BY anio");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lista de Años</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	<h1 class="mt-5">Lista de Años</h1>
	
	<a href="add_anio.php" class="btn btn-primary"><img src="crud-icon/add.png" title="Add New Record" style="vertical-align:bottom;" /> Nuevo</a>
	
	<table class="table table-hover"> 
	  	<thead>
			<tr>
				<th scope="col">Año</th>
				<th scope="col">Tema de Verano</th>
				<th scope="col">Estado</th>
				<th scope="col">Acciones</th>
			</tr>
	  	</thead>
	  	<tbody id="table-body">
		<?php
			if(!empty($result)) { 
				foreach($result as $row) {
		?>
		  	<tr class="table-row">
				<th scope = "row"><?php echo $row["anio"]; ?></th>
				<td><?php echo $row["tema"]; ?></td>
				<td><?php if($row["estado"]==1)  echo 'Abierto'; else echo 'Cerrado' ; ?></td>
				<td>
					<a href='edit_anio.php?idanio=<?php echo $row['idanio']; ?>'>
						<img src="crud-icon/edit.png" title="Editar" /></a>
					<a href='delete_anio.php?idanio=<?php echo $row['idanio']; ?>' 
						onclick="return confirm('Está seguro que desea borrar el año <?php echo $row['anio']; ?> ?')">
						<img src="crud-icon/delete.png" title="Eliminar" /></a>
				</td>
		  	</tr>
	    <?php
			}
		}
		?>
	  	</tbody>
	</table>
	</div>
</main>
	<?php include ("footer_js.php"); ?>	
</body>
</html>