<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["save_record"])) {
	$pdo_statement=$pdo_conn->prepare("update tipomatricula set 
				tipomatricula='" . $_POST[ 'post_tipomatricula' ] . 
				"' where borrado = 0 and idsede = ".$idsede." and idtipomatricula=" . $_GET["idtipomatricula"]);
	$result = $pdo_statement->execute();
	if($result) {
		header('location:tipomatricula.php');
	}//if
}//if
else{
	$pdo_statement = $pdo_conn->prepare("SELECT *
FROM tipomatricula 
	WHERE borrado = 0 and idsede = ".$idsede." and idtipomatricula=" . $_GET["idtipomatricula"]);
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();

}//else

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Editar Tipo de Matrícula</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	
		<h1 class="mt-5">Editar Tipo de Matrícula</h1>
		<form name="frmAdd" action="" method="POST">
			<div class="form-group">
			  	<label>Tipo de Matrícula: </label><br>
			  	<textarea name="post_tipomatricula" class="form-control"  rows="5" maxlenght="255" required ><?php echo $result[0]['tipomatricula']; ?></textarea>
		  	</div>

		  	<div class="form-group">
			  	<input name="save_record" type="submit" value="Guardar cambios" class="btn btn-primary"
			  	onclick="return confirm('Está seguro que desea guardar los cambios ?')">
			</div>
		</form>
	</div>

</main>
	<?php include ("footer_js.php"); ?>	
</body>
</html>