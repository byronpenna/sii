<?php
session_start(); 
require_once("db.php");
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
else{
	$nombresede = $_SESSION["nombresede"]; 
	$sede = $_SESSION["sede"]; 
	$anioactivo = $_SESSION["anioactivo"]; 
	
	//LLENA LAS SEDES EN EL SELECT
	$pdo_statement = $pdo_conn->prepare("SELECT idsede, nombresede FROM sede WHERE idsede = '".$sede."' and borrado = 0 order by idsede");
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();

	
}//else
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Menú Principal</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	
	<?php include ("navigation.php"); ?>
	<main role="main">
	<div class="container">
	<div class="jumbotron">
	  <h1 class="display-4">Sistema Verano Aventura FUSALMO </h1>
	  <p class="lead">Sede: <?php echo $result[0]['nombresede'] ?></p>
	  <hr class="my-4">
	  </p>
	</div>
	</main>
	<?php include ("footer_js.php"); ?>
	</div>	
</body>
</html>