<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["add_record"])) {

	$nombre = $_POST['post_nombre'];
	$fechanac = $_POST['post_fechanac'];
	$sexo = $_POST['post_sexo']; 
	$direccion = $_POST['post_direccion'];
	$municipio = $_POST['post_municipio'];
	$departamento = $_POST['post_departamento']; 
	$responsable = $_POST['post_responsable'];
	$parentescoresponsable = $_POST['post_parentescoresponsable'];
	$telefonofijo = $_POST['post_telefonofijo']; 
	$celular = $_POST['post_celular'];
	$centroestudios = $_POST['post_centroestudios'];
	$gradoacademico = $_POST['post_gradoacademico']; 
	$alergia = $_POST['post_alergia'];
	$asma = $_POST['post_asma'];
	$bronquitis = $_POST['post_bronquitis']; 
	$epilepsias = $_POST['post_epilepsias'];
	$fobias = $_POST['post_fobias'];
	$convulsiones = $_POST['post_convulsiones']; 
	$gastritis = $_POST['post_gastritis'];
	$colitis = $_POST['post_colitis'];
	$cardiacos = $_POST['post_cardiacos']; 
	$otros = $_POST['post_otros'];
	$voluntario = $_POST['post_voluntario'];
	
	$sql = "INSERT INTO ficha 
	(
		nombrealumno, 
		fechanacimiento, 
		sexo, 
		direccion, 
		municipio, 
		departamento, 
		responsable, 
		parentescoresponsable, 
		telefonoresponsable, 
		celularresponsable, 
		centroestudios, 
		gradoacademico, 
		alergia, 
		asma, 
		bronquitis, 
		epilepsias, 
		fobias, 
		convulsiones, 
		gastritis, 
		colitis, 
		cardiacos, 
		otros, 
		borrado,
		activo,
		idsede,
		voluntario
	)
	VALUES
	( 
		:nombrealumno, 
		:fechanacimiento, 
		:sexo, 
		:direccion, 
		:municipio, 
		:departamento, 
		:responsable, 
		:parentescoresponsable, 
		:telefonoresponsable, 
		:celularresponsable, 
		:centroestudios, 
		:gradoacademico, 
		:alergia, 
		:asma, 
		:bronquitis, 
		:epilepsias, 
		:fobias, 
		:convulsiones, 
		:gastritis, 
		:colitis, 
		:cardiacos, 
		:otros, 
		0,
		1,
		:idsede,
		:voluntario
	)";
	$pdo_statement = $pdo_conn->prepare( $sql );
	$result = $pdo_statement->execute( array( 
		':nombrealumno'=>$nombre, 
		':fechanacimiento'=>$fechanac, 
		':sexo'=>$sexo,
		':direccion'=>$direccion, 
		':municipio'=>$municipio, 
		':departamento'=>$departamento,
		':responsable'=>$responsable, 
		':parentescoresponsable'=>$parentescoresponsable, 
		':telefonoresponsable'=>$telefonofijo,
		':celularresponsable'=>$celular, 
		':centroestudios'=>$centroestudios, 
		':gradoacademico'=>$gradoacademico,
		':alergia'=>$alergia, 
		':asma'=>$asma, 
		':bronquitis'=>$bronquitis,
		':epilepsias'=>$epilepsias, 
		':fobias'=>$fobias, 
		':convulsiones'=>$convulsiones,
		':gastritis'=>$gastritis, 
		':colitis'=>$colitis, 
		':cardiacos'=>$cardiacos,
		':otros'=>$otros,
		':idsede'=>$idsede,
		':voluntario'=>$voluntario
	) );
	if (!empty($result) ){
	  header('location:ficha.php');
	}//if
	
}//if(!empty($_POST["add_record"]))

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Nueva Ficha de Alumno</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	
		<h1>Nueva Ficha</h1>
		<form name="frmAdd" action="" method="POST">
			
			<div class="form-group">
	  			<label>Nombre : </label><br>
			  	<input name="post_nombre" maxlength = "255" type="text" class="form-control" required />
			</div>

			<div class="form-group">
	  			<label>Fecha de Nacimiento : </label><br>
			  	<input name="post_fechanac" type="date" class="form-control" required />
			</div>

			<fieldset class="form-group">
			    <legend>Sexo</legend>
			    <div class="form-check">
			      <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_sexo" id="id_post_sexo1" value="M" checked>
			        Masculino
			      </label>
			    </div>
			    <div class="form-check">
			    <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_sexo" id="id_post_sexo2" value="F">
			        Femenino
			      </label>
			    </div>
			 </fieldset>


		  	<div class="form-group">
			  	<label>Dirección: </label><br>
			  	<textarea name="post_direccion" class="form-control" rows="5" maxlenght="255" ></textarea>
		  	</div>

		  	<div class="form-group">
	  			<label>Municipio: </label><br>
			  	<input name="post_municipio" maxlength = "30" type="text" class="form-control" />
			</div>
	  		
	  		<div class="form-group">
	  			<label>Departamento: </label><br>
			  	<input name="post_departamento" maxlength = "30" type="text" class="form-control" />
			</div>

			<div class="form-group">
	  			<label>Responsable: </label><br>
			  	<input name="post_responsable" maxlength = "255" type="text" class="form-control" />
			</div>

			<div class="form-group">
	  			<label>Parentesco del Responsable: </label><br>
			  	<input name="post_parentescoresponsable" maxlength = "45" type="text" class="form-control" />
			</div>

		  	<div class="form-group">
			  	<label>Teléfono Fijo: </label><br>
			  	<input type="tel" maxlength = "15" name="post_telefonofijo" class="form-control" />
		  	</div>

		  	<div class="form-group">
			  	<label>Celular: </label><br>
			  	<input type="tel" maxlength = "15" name="post_celular" class="form-control" />
		  	</div>

		  	<div class="form-group">
			  	<label>Centro de Estudios: </label><br>
			  	<input type="text" maxlength = "255" name="post_centroestudios" class="form-control" />
		  	</div>

		  	<div class="form-group">
			  	<label>Grado Académico: </label><br>
			  	<input type="text" maxlength = "45" name="post_gradoacademico" class="form-control" />
		  	</div>

		  	<fieldset class="form-group">
			    <legend>DATOS MÉDICOS:</legend>
		  		<div class="form-check">
		  			<input type='hidden' value='0' name='post_alergia'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name="post_alergia" value="1" class="form-check-input">
			    		Alergia
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_asma'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_asma' value="1" class="form-check-input">
			    		Asma
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_bronquitis'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_bronquitis' value="1" class="form-check-input">
			    		Bronquitis
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_epilepsias'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_epilepsias' value="1" class="form-check-input">
			    		Epilepsias
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_fobias'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_fobias' value="1" class="form-check-input">
			    		Fobias/Miedos
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_convulsiones'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_convulsiones' value="1" class="form-check-input">
			    		Convulsiones
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_gastritis'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_gastritis' value="1" class="form-check-input">
			    		Gastritis
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_colitis'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_colitis' value="1" class="form-check-input">
			    		Colitis
			    	</label>
			 	</div>
			 	<div class="form-check">
		  			<input type='hidden' value='0' name='post_cardiacos'>
			    	<label class="form-check-label">
			    		<input type="checkbox" name='post_cardiacos' value="1" class="form-check-input">
			    		Padecimientos Cardíacos
			    	</label>
			 	</div>
			</fieldset>

		  	<div class="form-group">
			  	<label>Otros: </label><br>
			  	<textarea name="post_otros" class="form-control" rows="5" maxlenght="255"></textarea>
		  	</div>

		  	<fieldset class="form-group">
			    <legend>Tipo</legend>
			    <div class="form-check">
			      <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_voluntario" id="id_post_voluntario1" value="0" checked>
			        Estudiante
			      </label>
			    </div>
			    <div class="form-check">
			    <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_voluntario" id="id_post_voluntario2" value="1">
			        Voluntario
			      </label>
			    </div>
			 </fieldset>
	  		
		  	
		  	<div class="form-group">
			  	<input name="add_record" type="submit" value="Añadir" class="btn btn-primary">
			</div>
		</form>
	</div> 
	</main>
	<?php include ("footer_js.php"); ?>
</body>
</html>