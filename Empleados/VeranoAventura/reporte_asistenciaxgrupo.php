<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"];
$anioactivo = $_SESSION["anioactivo"];

$postsede = $_POST['post_sede'];
$postanio = $_POST['post_anio'];
$postgrupo = $_POST['post_grupo'];
$postfechainicio = $_POST['post_fecha_inicio'];
$postfechafin = $_POST['post_fecha_fin'];


require_once("db.php");

$pdo_statement = $pdo_conn->prepare("call reporte_asistenciaxgrupo ('".$postsede."','".$postanio."','".$postgrupo."','".$postfechainicio."','".$postfechafin."')");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();

$pdo_statement->closeCursor();

$pdo_statement1 = $pdo_conn->prepare("SELECT a.anio,a.tema, s.nombresede, g.grupo
        FROM anio AS a
            JOIN
                sede AS s
            ON
                s.idsede             = a.idsede         
                AND s.borrado                = 0
            JOIN
                grupo AS g
            ON
                g.idanio            = a.idanio          
                AND g.borrado                = 0
                AND g.idsede                = s.idsede              
                
         WHERE a.borrado = 0 AND a.idsede = '".$postsede."' AND a.idanio = '".$postanio."' AND g.idgrupo = '".$postgrupo."' 
         ORDER BY anio DESC
         LIMIT 1");
$pdo_statement1->execute();
$result1 = $pdo_statement1->fetchAll();
$response = array();
foreach($result as $row) {
	$elements = array("fechaasistencia" => $row["fechaasistencia"], "nombrealumno" => $row["nombrealumno"], "asiste" => $row["asiste"]);
        array_push($response, $elements);
}
//print_r($result);
//print_r($response);
?>
<html>
<head>
    <meta charset="UTF-8">
    <title>Reporte de Asistencia por Grupo</title>
    <?php include ("header_css.php"); ?>
</head>
<body>
    <?php include ("navigation.php"); ?>
    <main role="main" class="container">
    <div class="container">
    <?php
    if(!empty($result1)) { 
        foreach($result1 as $row1) {
        ?>
            <div><h2> Reporte de Asistencia por Grupo</h2><br> 
                <h3>Sede : <?php echo $row1["nombresede"]; ?><br>
                Año : <?php echo $row1["anio"]; ?><br>
                Tema : <?php echo $row1["tema"]; ?><br>
                Grupo : <?php echo $row1["grupo"]; ?></h3><br>
            </div>
    <?php
        }
    }
    ?>
    <table class="table table-hover">
    <tr>
    <?php
    if(!empty($result)) { 
        $current_fecha = null;
        $contador_fecha = null;

        echo "<th scope='col'>Nombre</th>";        
        for($i=0; $i<count($result);$i++){
        if ($result[$i][1] != $current_fecha) {
                       
                        $current_fecha = $result[$i][1];
                        echo "<th scope='col'>".date_format(date_create($current_fecha), 'd/m/Y')."</th>";
                        $contador_fecha++;

                    }
        }
        echo "</tr>";
        $current_alumno = null;
        $contador_alumno = null;
        //ORDENA POR ORDEN ALFABÉTICO LOS NOMBRES DE LOS ALUMNOS
        array_multisort($result);
        //array_flip($result);
        //print_r($result1);

        for($i=0; $i<count($result);$i++){
            if ($result[$i][0] != $current_alumno) {
                $current_alumno = $result[$i][0];
                                $current_alumno = $result[$i][0];
                                //echo $current_alumno;
                                $contador_alumno++;

                            }
        }
        ?>
    <tr>
        <?php
        //echo "Contador alumno ".$contador_alumno;
        $contador = 0;
        //echo $contador_fecha;
        foreach ($result as $row) {
            if($contador == 0){
                echo "<td>";
                echo $row["nombrealumno"];
                echo ($row["voluntario"]==1)?" - Voluntario":"";
                echo "</td>";
            }
            echo "<td>";
            echo ($row["asiste"]==1)?"X":"-";
            echo "</td>";
            $contador++;
            if($contador == $contador_fecha){
                echo "</tr>";
                $contador = 0;
            }
            
        }
    }

        ?>
</table>
</div>
</main>
    <?php include ("footer_js.php"); ?> 
</body>
</html>


