<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"];
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");

	
	//PARA LLENAR LOS GRUPOS
	$pdo_statement = $pdo_conn->prepare("call grupos ('".$idsede."')");
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Nueva Asistencia</title>
    <?php include ("header_css.php"); ?>
	
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
		<h1>Nueva Asistencia</h1>
		<form name="frmAsistencia" action="asistencia_paso2.php" method="POST">
		  	<div class="form-group">
	  			<label>Grupo: </label><br>
			  	<select name = "post_grupo" class="js-example-basic-single form-control" style="width: 50%" required>
			  	<?php
					if(!empty($result)) { 
						foreach($result as $row) {
				?>
	  				<option value="<?php echo $row["idgrupo"]; ?>"><?php echo $row["grupo"]; ?> - <?php echo $row["anio"]; ?></option>	 	  	
		    	<?php
					}//foreach
				}//if
				?>
				</select>
			</div>

			<div class="form-group">
	  			<label>Fecha : </label>
			  	<input name="post_fecha" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required />
			</div>

		  	<div class="form-group">
			  	<input name="accept" type="submit" value="Aceptar" class="btn btn-primary">
			</div>
		</form>
	</div> 
</main>
	<?php include ("footer_js.php"); ?>
	<script type="text/javascript">
		// In your Javascript (external .js resource or <script> tag)
		$(document).ready(function() {
		    $('.js-example-basic-single').select2({
		    		language: "es"
		    	}
	    	);

		});
	</script>
</body>

</html>