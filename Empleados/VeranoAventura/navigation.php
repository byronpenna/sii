<header>
<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Verano Aventura <?php echo $nombresede;?> - <?php echo $anioactivo;?> </a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    
    </div>

    <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Opciones <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="anio.php">Años</a></li>
                    <li><a href="grupo.php">Grupos</a></li>
                    <li><a href="tipomatricula.php">Tipos de Matrícula</a></li>
                    <li><a href="ficha.php">Fichas de Alumnos</a></li>  
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Inscripciones y Asistencia <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="inscripcion.php">Inscripciones</a></li>
                    <li><a href="asistencia.php">Asistencia</a></li>
                </ul>
            </li>	  	
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="alumnosxgrupo.php" >Alumnos por Grupo</a></li>
                    <li><a href="asistenciaxgrupo.php" >Asistencia por Grupo</a></li>
                    <li><a href="desercionesxgrupo.php" >Deserciones por Grupo</a></li>
                    <li><a href="alumnosxubicacion.php" >Alumnos por Ubicación Geográfica</a></li>
                    <li><a href="alumnosxsexo.php" >Alumnos por Sexo</a></li>
                    <li><a href="alumnosxtipomatricula.php" >Alumnos por Tipo de Matrícula</a></li>
                    <li><a href="estadisticas.php" >Estadísticas</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Cambiar Sede</a></li>
        </ul>
    </div>
</nav>
</header>