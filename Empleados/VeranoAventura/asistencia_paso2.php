<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"];
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["accept"])) {

	$fecha = $_POST['post_fecha'];
	$grupo = $_POST['post_grupo'];
	$pdo_statement1 = $pdo_conn->prepare("CALL asistenciaxgrupoyfecha('".$idsede."','".$fecha."','".$grupo."')");
	$pdo_statement1->execute();
	$result1 = $pdo_statement1->fetchAll();
	$cuentaFilas = $pdo_statement1->rowCount();
	$pdo_statement1->closeCursor();
	$bandera = 0;
	$bandera2 = 0;
	if ($cuentaFilas>0){
		$bandera = 1;
		
	}
	else{
		//PARA LLENAR LOS ESTUDIANTES INSCRITOS
		$pdo_statement = $pdo_conn->prepare("call alumnosxgrupo ('".$idsede."','".$grupo."')");
		//echo("call alumnosinscritosxgrupo ('".$idsede."','".$grupo."')");
		$pdo_statement->execute();
		$result = $pdo_statement->fetchAll();
		$cuentaFilas2 = $pdo_statement->rowCount();
		if ($cuentaFilas2<1){
			$bandera2 = 1;
		}
	}
	
	
}//if(!empty($_POST["add_record"]))
else{
	header('location:asistencia.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Nueva Asistencia</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	<h1>Nueva Asistencia</h1>
	<div class="alert alert-danger" role="alert" <?php echo ($bandera == 1)?"":"hidden" ?>>
  		No se puede ingresar una Asistencia 2 veces!
	</div>
	<div class="alert alert-danger" role="alert" <?php echo ($bandera2 == 1)?"":"hidden" ?>>
  		El Grupo no tiene Alumnos Inscritos!
	</div>
	<form name="frmAdd" action="add_asistencia.php" method="POST">
	<table class="table table-hover">
	  	<thead>
			<tr>
				
				<th scope="col">Alumnos</th>
				<th scope="col">Asistencia</th>
			</tr>
	  	</thead>
		<tbody id="table-body">
			<div class="demo-form-row">
	  			<label>Fecha : </label>
			  	<input name="post_fecha" type="date" class="form-control" value="<?php echo $fecha; ?>" required readonly/>
			</div>
		<?php

			if(!empty($result)) { 
				foreach($result as $row) {
				?>

  						<tr class="table-row">
							<td><?php echo $row["nombrealumno"]; ?><?php echo ($row["voluntario"]==1)?" - Voluntario":""; ?></td>
							<td>
								<input type='hidden' value='0' name="post_asistencia[<?php echo $row['idinscripcion'];?>]">
		  						<input type="checkbox" class="form-check-input" name="post_asistencia[<?php echo $row['idinscripcion'];?>]" value="1" <?php if($row['desercion']=="1") echo 'disabled';?> > <br>
							</td>
						</tr>
  					
				<?php	
				}	
					
			}
		?>


		</tbody>

	</table>
		<div class="demo-form-row">
			  	<input name="save_record" type="submit" value="Guardar Cambios" class="btn btn-primary"
			  	onclick="return confirm('Está seguro que desea guardar los cambios ?')" <?php echo ($bandera == 1||$bandera2 == 1)?"disabled":"" ?>>
		</div>
	</form>
	</div> 
</main>
	<?php include ("footer_js.php"); ?>
</body>
</html>