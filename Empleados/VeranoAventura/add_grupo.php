<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["add_record"])) {

	$nombre_grupo = $_POST['post_nombre_grupo'];
	$idanio = $_POST['post_anio'];

	$sql = "INSERT INTO grupo ( grupo, idanio, borrado, idsede ) VALUES ( :grupo, :idanio, 0, :idsede)";
	$pdo_statement = $pdo_conn->prepare( $sql );
	$result = $pdo_statement->execute( array( ':grupo'=>$nombre_grupo, 
		':idanio'=>$idanio, ':idsede'=>$idsede) );
	
	if (!empty($result) ){
	  header('location:grupo.php');
	}//if
	
}//if(!empty($_POST["add_record"]))
else{
	//PARA LLENAR LOS AÑOS
	$pdo_statement = $pdo_conn->prepare("SELECT anio,idanio 
		FROM anio where borrado = 0 and estado = 1 and idsede = ".$idsede." ORDER BY anio DESC
		LIMIT 1");
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Nuevo Grupo</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">

		<h1>Nuevo Grupo</h1>
		<form name="frmAdd" action="" method="POST">
			<div class="form-group">
			  	<label>Nombre del Grupo: </label><br>
			  	<textarea name="post_nombre_grupo" class="form-control" rows="5" maxlenght="255" required ></textarea>
		  	</div>
		  	<div class="form-group">
	  			<label>Año: </label><br>
			  	<select name = "post_anio" class="form-control" required>
			  	<?php
					if(!empty($result)) { 
						foreach($result as $row) {
				?>
	  				<option value="<?php echo $row["idanio"]; ?>"><?php echo $row["anio"]; ?></option>	 	  	
		    	<?php
					}//foreach
				}//if
				?>
				</select>
			</div>

		  	<div class="form-group">
			  	<input name="add_record" type="submit" value="Añadir" class="btn btn-primary">
			</div>
		</form>
	</div>
</main>
	<?php include ("footer_js.php"); ?>	 
</body>
</html>