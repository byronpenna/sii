<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"]; 

require_once("db.php");
$pdo_statement = $pdo_conn->prepare("CALL	inscripciones ('".$idsede."')");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
$cuentaFilas = $pdo_statement->rowCount();
$pdo_statement->closeCursor();

$pdo_statement1 = $pdo_conn->prepare("SELECT anio,tema
		FROM anio WHERE borrado = 0 AND estado = 1 and idsede = ".$idsede." ORDER BY anio DESC
		LIMIT 1");
$pdo_statement1->execute();
$result1 = $pdo_statement1->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lista de Inscripciones</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	<h1 class="mt-5">Lista de Inscripciones</h1>
	<?php
	if(!empty($result1)) { 
		foreach($result1 as $row1) {
		?>
			<div><h2> Año : <?php echo $row1["anio"]; ?><br>
  				Tema : <?php echo $row1["tema"]; ?></h2><br>
			</div>
    <?php
		}
	}
	?>
	
		<a href="add_inscripcion.php"  class="btn btn-primary">
		<img src="crud-icon/add.png" title="Add New Record" style="vertical-align:bottom;" /> Nuevo</a>
	
	<table class="table table-hover">
	  	<thead>
			<tr>
				
				<th scope="col">Alumnos Inscritos - <?php echo $cuentaFilas; ?>  </th>
				<th scope="col">Acciones</th>
			</tr>
	  	</thead>
		<tbody id="table-body">
		<?php

			if(!empty($result)) { 
				$current_grupo = null;
				$current_activo = null;
				foreach($result as $row) {
					if ($row["grupo"] != $current_grupo) {
						$current_activo = null;
    					$current_grupo = $row["grupo"];
    					?>
   						<tr class="table-row">
							<td><b><?php echo $row["grupo"]; ?> - Total <?php echo $row["totalgrupo"]; ?></b></td>
							<td></td>
						</tr>	
    					<?php
  					}
  					if ($row["desercion"] != $current_activo) {
    					$current_activo = $row["desercion"];
    					?>
						<tr class="table-row">
							<td><b><?php echo $row["desercion"]; ?> - (<?php echo $row["totaldesercion"]; ?>)</b></td>
							<td></td>
						</tr>	
    					<?php
    				
    				}
  					?>

  						<tr class="table-row">
							<td><?php echo $row["nombrealumno"]; ?></td>
							<td>
								<a href='edit_inscripcion.php?idinscripcion=<?php echo $row['idinscripcion']; ?>'>
									<img src="crud-icon/edit.png" title="Editar" /></a>
								<a href='delete_inscripcion.php?idinscripcion=<?php echo $row['idinscripcion']; ?>' 
									onclick="return confirm('Está seguro que desea borrar la inscripción del Estudiante <?php echo $row['nombrealumno']; ?> ?')">
									<img src="crud-icon/delete.png" title="Eliminar" /></a>
							</td>
						</tr>
  					
				<?php	
				}	
					
			}
		?>
		</tbody>
	</table>
		</div>
</main>
	<?php include ("footer_js.php"); ?>	
</body>
</html>