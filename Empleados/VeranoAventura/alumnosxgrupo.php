<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
	$idsede = $_SESSION["sede"]; 
	$nombresede = $_SESSION["nombresede"]; 
	$anioactivo = $_SESSION["anioactivo"];
	
	require_once("db.php");

    //PARA LLENAR LAS SEDES
	$pdo_statement = $pdo_conn->prepare("SELECT idsede, nombresede FROM sede WHERE borrado = 0 order by idsede");
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Alumnos por Grupo</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">

		<h1>Alumnos por Grupo</h1>
		<form name="frmAdd" action="reporte_alumnosxgrupo.php" method="POST">
			<div class="form-group">
	  			<label>Sede: </label><br>
			  	<select name = "post_sede" id = "post_sede" class="form-control" required>
			  		<option value=''>------- Seleccione --------</option>
			  	<?php
					if(!empty($result)) { 
						foreach($result as $row) {
				?>
	  				<option value="<?php echo $row["idsede"]; ?>"><?php echo $row["nombresede"]; ?></option>	 	  	
		    	<?php
					}//foreach
				}//if
				?>
				</select>
			</div>

			<div class="form-group">
	  			<label>Año: </label>
	  			<select name="post_anio" class="form-control" id="post_anio" disabled="disabled">
	  				<option>------- Seleccione --------</option>
				</select>
			</div>

			<div class="form-group">
	  			<label>Grupo: </label>
	  			<select name="post_grupo" class="form-control" id="post_grupo" disabled="disabled">
	  				<option>------- Seleccione --------</option>
				</select>
			</div>

			<div class="demo-form-row">
			  	<input name="submit_asistenciaxgrupo" id="submit_asistenciaxgrupo" type="submit" value="Seleccionar" class="btn btn-primary" disabled = "disabled">
			</div>
		</form>
	</div> 
	<?php include ("footer_js.php"); ?>
	<script type="text/javascript">
		$(document).ready(function() {


			//Change in continent dropdown list will trigger this function and
			//generate dropdown options for county dropdown
			$(document).on('change','#post_sede', function() {

				var post_sede = $(this).val();
				if(post_sede != "") {
					
					$.ajax({
						url:"get_data_select.php",
						type:'POST',
						data:{post_sede:post_sede},
						success:function(response) {
							//var resp = $.trim(response);
							if(response != '') {
								$("#post_anio").removeAttr('disabled','disabled').html(response);
								$("#post_grupo").attr('disabled','disabled').html("<option value=''>------- Seleccione --------</option>");
								$("#submit_asistenciaxgrupo").attr('disabled','disabled');
								
							} else {
								$("#post_anio, #post_grupo").attr('disabled','disabled').html("<option value=''>------- Seleccione --------</option>");
								$("#submit_asistenciaxgrupo").attr('disabled','disabled');
							}
						}
					});
				} else {
					$("#post_anio, #post_grupo").attr('disabled','disabled').html("<option value=''>------- Seleccione --------</option>");
					$("#submit_asistenciaxgrupo").attr('disabled','disabled');
				}
			});


			//Change in coutry dropdown list will trigger this function and
			//generate dropdown options for state dropdown
			$(document).on('change','#post_anio', function() {
				var post_anio = $(this).val();
				var post_sede1 = $( "#post_sede" ).val();
				if(post_anio != "") {
					$.ajax({
						url:"get_data_select.php",
						type:'POST',
						data:{post_anio:post_anio,post_sede1:post_sede1},
						success:function(response) {
							//var resp = $.trim(response);
							if(response != ''){
								$("#post_grupo").removeAttr('disabled','disabled').html(response);
								
							} 
							else {
								$("#post_grupo").attr('disabled','disabled').html("<option value=''>------- Seleccione --------</option>");
								$("#submit_asistenciaxgrupo").attr('disabled','disabled');
							}
						}
					});
				} else {
					$("#post_grupo").attr('disabled','disabled').html("<option value=''>------- Seleccione --------</option>");
					$("#submit_asistenciaxgrupo").attr('disabled','disabled');
				}
			});

			$(document).on('change','#post_grupo', function() {
				var post_grupo = $(this).val();
				if(post_grupo != "") {
					$("#submit_asistenciaxgrupo").removeAttr('disabled','disabled');
				}else{
					$("#submit_asistenciaxgrupo").attr('disabled','disabled');
				}
			});

		});

	</script>
</body>
</html>
		