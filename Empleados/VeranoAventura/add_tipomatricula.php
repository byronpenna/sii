<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["add_record"])) {

	$tipomatricula = $_POST['post_tipomatricula'];

	$sql = "INSERT INTO tipomatricula ( tipomatricula, borrado, idsede ) VALUES ( :tipomatricula, 0, :idsede)";
	$pdo_statement = $pdo_conn->prepare( $sql );
	$result = $pdo_statement->execute( array( ':tipomatricula'=>$tipomatricula, ':idsede'=>$idsede) );
	
	if (!empty($result) ){
	  header('location:tipomatricula.php');
	}//if
	
}//if(!empty($_POST["add_record"]))

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Nuevo Tipo de Matrícula</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	
		<h1>Nuevo Tipo de Matrícula</h1>
		<form name="frmAdd" action="" method="POST">
			<div class="form-group">
			  	<label>Tipo de Matrícula: </label><br>
			  	<textarea name="post_tipomatricula" class="form-control"  rows="5" maxlenght="255" required ></textarea>
		  	</div>

		  	<div class="form-group">
			  	<input name="add_record" type="submit" value="Añadir" class="btn btn-primary">
			</div>
		</form>
	</div> 
	</main>
	<?php include ("footer_js.php"); ?>	
</body>
</html>