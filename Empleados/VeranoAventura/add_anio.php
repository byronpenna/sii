<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"]; 
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];
if(!empty($_POST["add_record"])) {

	$anio = $_POST['post_anio'];
	$tema = $_POST['post_tema'];
	$estado = $_POST['post_estado']; 
	require_once("db.php");

	$sql = "SELECT * FROM anio where borrado = 0 and anio = :anio and idsede = :idsede";
	$pdo_statement = $pdo_conn->prepare($sql);
	$pdo_statement->execute(array(':anio'=>$anio, ':idsede'=>$idsede));
	$cuentaFilas = $pdo_statement->rowCount();

	if(!$cuentaFilas){
		$sql = "INSERT INTO anio ( anio, estado, tema, borrado, idsede ) VALUES ( :anio, :estado, :tema , 0, :idsede)";
		$pdo_statement = $pdo_conn->prepare( $sql );
		$result = $pdo_statement->execute( array( ':anio'=>$anio, 
			':estado'=>$estado, ':tema'=>$tema, ':idsede'=>$idsede ) );
	}//if
	else{
		echo "No se pueden insertar 2 años iguales";
	}//else

	
	if (!empty($result) ){
	  header('location:anio.php');
	}//if
	
}//if(!empty($_POST["add_record"]))
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Nuevo Año</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
		<h1>Nuevo Año</h1>
		<form name="frmAdd" action="" method="POST">
		  	<div class="form-group">
			  	<label>Año: </label><br>
			  	<input type="number" name="post_anio" class="form-control"  min="2000" max="2050" required />
		  	</div>

		  	<fieldset class="form-group">
			    <legend>Estado</legend>
			    <div class="form-check">
			      <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_estado" id="id_post_estado1" value="1" checked>
			        Abierto
			      </label>
			    </div>
			    <div class="form-check">
			    <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_estado" id="id_post_estado2" value="0">
			        Cerrado
			      </label>
			    </div>
			 </fieldset>

		  	<div class="form-group">
			  	<label>Tema de Verano-Aventura: </label><br>
			  	<textarea name="post_tema" class="form-control"  rows="5" maxlenght="255" required ></textarea>
		  	</div>

		  	<div class="form-group">
			  	<input name="add_record" type="submit" value="Añadir" class="btn btn-primary">
			</div>
		</form>

	</div>
</main>
	<?php include ("footer_js.php"); ?>	
</body>
</html>