<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"]; 

require_once("db.php");
$pdo_statement = $pdo_conn->prepare("SELECT * FROM ficha where borrado = 0 and idsede = ".$idsede." order by voluntario,activo desc,nombrealumno asc");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lista de Estudiantes</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	<h1 class="mt-5">Lista de Fichas de Alumnos</h1>
	<a href="add_ficha.php" class="btn btn-primary">
		<img src="crud-icon/add.png" title="Add New Record" style="vertical-align:bottom;" /> Nuevo</a>
	<table class="table table-hover">
	  	<thead>
			<tr>
				<th scope="col">Nombre</th>
				<th scope="col">Fecha de Nacimiento</th>
				<th scope="col">Sexo</th>
				<th scope="col">Centro de Estudios</th>
				<th scope="col">Tipo</th>
				<th scope="col">Activo</th>
				<th scope="col">Acciones</th>
			</tr>
	  	</thead>
	  	<tbody id="table-body">
		<?php
			if(!empty($result)) { 
				foreach($result as $row) {
		?>
		  	<tr class="table-row">
				<td><?php echo $row["nombrealumno"]; ?></td>
				<td><?php echo date_format(date_create($row["fechanacimiento"]), 'd/m/Y'); ?></td>
				<td><?php if($row["sexo"]=="M")  echo 'Masculino'; else echo 'Femenino' ; ?></td>
				<td><?php echo $row["centroestudios"]; ?></td>
				<td><?php if($row["voluntario"]=="1")  echo 'Voluntario'; else echo 'Estudiante' ; ?></td>
				<td><?php if($row["activo"]=="1")  echo 'Activo'; else echo 'Inactivo' ; ?></td>
				<td>
					<a href='edit_ficha.php?idficha=<?php echo $row['idficha']; ?>'>
						<img src="crud-icon/edit.png" title="Editar" /></a>
					<a href='delete_ficha.php?idficha=<?php echo $row['idficha']; ?>' 
						onclick="return confirm('Está seguro que desea borrar el Estudiante <?php echo $row['nombrealumno']; ?> ?')">
						<img src="crud-icon/delete.png" title="Eliminar" /></a>
				</td>
		  	</tr>
	    <?php
			}
		}
		?>
	  	</tbody>
	</table>
	</div>
</main>
	<?php include ("footer_js.php"); ?>		
</body>
</html>