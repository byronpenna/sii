<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"]; 

require_once("db.php");
//MUESTRA SOLAMENTE LOS GRUPOS DEL AÑO QUE SE ENCUENTRA ABIERTO Y QUE NO ESTEN BORRADOS
$pdo_statement = $pdo_conn->prepare("SELECT g.idgrupo,g.grupo,a.anio, g.borrado
FROM grupo AS g
	INNER JOIN
		(
			SELECT  * FROM anio
			WHERE estado = 1
			AND borrado = 0
			and idsede = ".$idsede."
			ORDER BY anio DESC
			LIMIT 1
		)AS a
	ON
		a.idanio = g.idanio
		and a.idsede = g.idsede
	WHERE g.borrado = 0 
	and g.idsede = ".$idsede."
	ORDER BY idgrupo");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lista de Grupos</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	<h1 class="mt-5">Lista de Grupos</h1>

		<a href="add_grupo.php" class="btn btn-primary">
		<img src="crud-icon/add.png" title="Add New Record" style="vertical-align:bottom;" /> Nuevo</a>

	<table class="table table-hover">
	  	<thead>
			<tr>
				<th scope="col">Nombre del Grupo</th>
				<th scope="col">Año</th>
				<th scope="col">Acciones</th>
			</tr>
	  	</thead>
	  	<tbody id="table-body">
		<?php
			if(!empty($result)) { 
				foreach($result as $row) {
		?>
		  	<tr class="table-row">
				<td><?php echo $row["grupo"]; ?></td>
				<td><?php echo $row["anio"]; ?></td>
				<td>
					<a href='edit_grupo.php?idgrupo=<?php echo $row['idgrupo']; ?>'>
						<img src="crud-icon/edit.png" title="Editar" /></a>
					<a href='delete_grupo.php?idgrupo=<?php echo $row['idgrupo']; ?>' 
						onclick="return confirm('Está seguro que desea borrar el grupo <?php echo $row['grupo']; ?> ?')">
						<img src="crud-icon/delete.png" title="Eliminar" /></a>
				</td>
		  	</tr>
	    <?php
			}
		}
		?>
	  	</tbody>
	</table>
		</div>
</main>
	<?php include ("footer_js.php"); ?>	
</body>
</html>