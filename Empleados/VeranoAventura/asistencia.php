<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
$pdo_statement = $pdo_conn->prepare("call asistenciaxgrupo ('".$idsede."')");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
$cuentaFilas = $pdo_statement->rowCount();
$pdo_statement->closeCursor();

$pdo_statement1 = $pdo_conn->prepare("SELECT anio,tema
		FROM anio WHERE borrado = 0 and idsede = ".$idsede." AND estado = 1 ORDER BY anio DESC
		LIMIT 1");
$pdo_statement1->execute();
$result1 = $pdo_statement1->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Asistencia por Grupo</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	<h1 class="mt-5">Lista de Asistencias</h1>
	
		<a href="asistencia_paso1.php" class="btn btn-primary">
		<img src="crud-icon/add.png" title="Add New Record" style="vertical-align:bottom;" /> Nuevo</a>
	
	<table class="table table-hover">
	  	<thead>
			<tr>
				
				<th scope="col">Fecha</th>
				<th scope="col">Acciones</th>
			</tr>
	  	</thead>
		<tbody id="table-body">
		<?php

			if(!empty($result)) { 
				$current_grupo = null;
				$current_activo = null;
				foreach($result as $row) {
					if ($row["grupo"] != $current_grupo) {
						$current_activo = null;
    					$current_grupo = $row["grupo"];
    					?>
   						<tr class="table-row">
							<td><b><?php echo $row["grupo"]; ?></b></td>
							<td></td>
						</tr>	
    					<?php
  					}
  					?>
  						<tr class="table-row">
							
							<td><?php echo date_format(date_create($row["fechaasistencia"]), 'd/m/Y'); ?></td>
							<td>
								<a href='edit_asistencia.php?idgrupo=<?php echo $row['idgrupo'];?>&fechaasistencia=<?php echo $row['fechaasistencia'];?>'>
									<img src="crud-icon/edit.png" title="Editar" /></a>
								<a href='delete_asistencia.php?idgrupo=<?php echo $row['idgrupo'];?>&fechaasistencia=<?php echo $row['fechaasistencia'];?>' 
									onclick="return confirm('Está seguro que desea borrar el grupo <?php echo $row['grupo']; ?> ?')">
									<img src="crud-icon/delete.png" title="Eliminar" /></a>
							</td>
							
							
							
						</tr>
				<?php	
				}	
			}
		?>
		</tbody>
	</table>
	</div>
</main>
	<?php include ("footer_js.php"); ?>	
</body>
</html>