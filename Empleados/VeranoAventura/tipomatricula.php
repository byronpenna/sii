<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"]; 

require_once("db.php");
//MUESTRA SOLAMENTE MATRICULAS QUE NO ESTEN BORRADAS
$pdo_statement = $pdo_conn->prepare("SELECT idtipomatricula,tipomatricula,borrado
FROM tipomatricula
	WHERE borrado = 0 and idsede = ".$idsede."
	ORDER BY idtipomatricula");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lista de Tipos de Matrícula</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	<h1 class="mt-5">Lista de Tipos de Matrícula</h1>
	<a href="add_tipomatricula.php" class="btn btn-primary">
		<img src="crud-icon/add.png" title="Add New Record" style="vertical-align:bottom;" /> Nuevo</a>
	
	<table class="table table-hover">
	  	<thead>
			<tr>
				<th scope="col">Tipo de Matrícula</th>
				<th scope="col">Acciones</th>
			</tr>
	  	</thead>
	  	<tbody id="table-body">
		<?php
			if(!empty($result)) { 
				foreach($result as $row) {
		?>
		  	<tr class="table-row">
				<td><?php echo $row["tipomatricula"]; ?></td>
				<td>
					<a href='edit_tipomatricula.php?idtipomatricula=<?php echo $row['idtipomatricula']; ?>'>
						<img src="crud-icon/edit.png" title="Editar" /></a>
					<a href='delete_tipomatricula.php?idtipomatricula=<?php echo $row['idtipomatricula']; ?>' 
						onclick="return confirm('Está seguro que desea borrar el grupo <?php echo $row['tipomatricula']; ?> ?')">
						<img src="crud-icon/delete.png" title="Eliminar" /></a>
				</td>
		  	</tr>
	    <?php
			}
		}
		?>
	  	</tbody>
	</table>
	</div>
</main>
	<?php include ("footer_js.php"); ?>	
</body>
</html>