<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"];
$anioactivo = $_SESSION["anioactivo"];

$postsede = $_POST['post_sede'];
$postanio = $_POST['post_anio'];


require_once("db.php");
$pdo_statement = $pdo_conn->prepare("call alumnosxubicacion ('".$postsede."','".$postanio."')");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
$cuentaFilas = $pdo_statement->rowCount();
$pdo_statement->closeCursor();

$pdo_statement1 = $pdo_conn->prepare("SELECT a.anio,a.tema, s.nombresede
		FROM anio AS a
			JOIN
				sede AS s
			ON
				s.idsede			 = a.idsede			
				AND s.borrado				 = 0
					
				
		 WHERE a.borrado = 0 AND a.idsede = '".$postsede."' AND a.idanio = '".$postanio."'
		 ORDER BY anio DESC
		 LIMIT 1");
$pdo_statement1->execute();
$result1 = $pdo_statement1->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Reporte de Alumnos por Ubicación Geográfica</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">

	<?php
	if(!empty($result1)) { 
		foreach($result1 as $row1) {
		?>
			<div><h2>Reporte de Alumnos por Ubicación Geográfica</h2><br> 
				<h3>Sede : <?php echo $row1["nombresede"]; ?><br>
				Año : <?php echo $row1["anio"]; ?><br>
  				Tema : <?php echo $row1["tema"]; ?><br>
  				<h3>Total de Alumnos - <?php echo $cuentaFilas; ?>  </h3>
			</div>
    <?php
		}
	}
	?>
	<table class="table table-hover">
	  	<thead>
			<tr>
				
				<th scope="col">Nombre</th>
				<th scope="col">Dirección</th>
				<th scope="col">Municipio</th>
				<th scope="col">Teléfonos</th>
			</tr>
	  	</thead>
		<tbody id="table-body">
		<?php

			if(!empty($result)) { 
				$current_grupo = null;
				$current_activo = null;
				foreach($result as $row) {
					if ($row["departamento"] != $current_grupo) {
						$current_activo = null;
    					$current_grupo = $row["departamento"];
    					?>
   						<tr class="table-row">
							<td><b>Departamento <?php echo $row["departamento"]; ?> - Total <?php echo $row["totaldepartamento"]; ?></b></td>
							
						</tr>
							
    					<?php
  					}
  					?>
  						<tr>
							<td><?php echo $row["nombrealumno"]; ?></td>
							<td><?php echo $row["direccion"]; ?></td>
							<td><?php echo $row["municipio"]; ?></td>
							<td><?php echo $row["telefonoresponsable"]; ?> / <?php echo $row["celularresponsable"]; ?></td>
						</tr>
				<?php	
				}	
				?>
			
			<?php
			}
		?>
		</tbody>
	</table>
	</div>
</main>
	<?php include ("footer_js.php"); ?>
</body>
</html>