<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"];
$anioactivo = $_SESSION["anioactivo"];

$postsede = $_POST['post_sede'];
$postanio = $_POST['post_anio'];


require_once("db.php");
$pdo_statement = $pdo_conn->prepare("call alumnosxtipomatricula ('".$postsede."','".$postanio."')");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
$cuentaFilas = $pdo_statement->rowCount();
$pdo_statement->closeCursor();

$pdo_statement1 = $pdo_conn->prepare("SELECT a.anio,a.tema, s.nombresede
		FROM anio AS a
			JOIN
				sede AS s
			ON
				s.idsede			 = a.idsede			
				AND s.borrado				 = 0
					
				
		 WHERE a.borrado = 0 AND a.idsede = '".$postsede."' AND a.idanio = '".$postanio."'
		 ORDER BY anio DESC
		 LIMIT 1");
$pdo_statement1->execute();
$result1 = $pdo_statement1->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Reporte de Alumnos por Tipo de Matrícula</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">

	<?php
	if(!empty($result1)) { 
		foreach($result1 as $row1) {
		?>
			<div><h2>Reporte de Alumnos por Tipo de Matrícula</h2><br> 
				<h3>Sede : <?php echo $row1["nombresede"]; ?><br>
				Año : <?php echo $row1["anio"]; ?><br>
  				Tema : <?php echo $row1["tema"]; ?><br>
  				<h3>Total de Alumnos - <?php echo $cuentaFilas; ?>  </h3>
			</div>
    <?php
		}
	}
	?>
	<table class="table table-hover">
	  	<thead>
			<tr>
				
				<th scope="col">Nombre</th>
				<th scope="col">Fecha de Nacimiento</th>
				<th scope="col">Edad</th>
				<th scope="col">Monto de Matrícula</th>
				<th scope="col">Pago de Convivio</th>
			</tr>
	  	</thead>
		<tbody id="table-body">
		<?php

			if(!empty($result)) { 
				$current_grupo = null;
				$current_activo = null;
				foreach($result as $row) {
					if ($row["tipomatricula"] != $current_grupo) {
						$current_activo = null;
    					$current_grupo = $row["tipomatricula"];
    					?>
   						<tr class="table-row">
							<td><b><?php echo $row["tipomatricula"]; ?> - Total <?php echo $row["totaltipomatricula"]; ?></b></td>
							
						</tr>
							
    					<?php
  					}
  					?>
  						<tr>
							<td><?php echo $row["nombrealumno"]; ?></td>
							<td><?php echo $row["fechanacimiento"]; ?></td>
							<td><?php echo $row["edad"]; ?></td>
							<td><?php echo $row["monto"]; ?></td>
							<td><?php echo $row["convivio"]; ?></td>
							
						</tr>
				<?php	
				}	
				?>
			
			<?php
			}
		?>
		</tbody>
	</table>
	</div>
</main>
	<?php include ("footer_js.php"); ?>
</body>
</html>