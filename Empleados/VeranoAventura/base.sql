-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-03-2018 a las 03:59:06
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `veranoaventura2v`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE  PROCEDURE `alumnosinscritosxgrupo` (IN `idsedein` INT, IN `idanioin` INT, IN `idgrupoin` INT)  BEGIN
SELECT 
		f.idficha,
		f.nombrealumno, 
		f.fechanacimiento,
		CASE WHEN ISNULL(TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE())) = 1 THEN 0 ELSE TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE()) END edad,
		f.sexo,
		f.responsable,
		f.telefonoresponsable,
		f.celularresponsable,
		ins.idalumno, 
		ins.idgrupo,
		UCASE(ins.grupo) grupo, 
		ins.idanio, 
		ins.anio, 
		ins.idinscripcion,
		f.voluntario
	FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					and i.idsede = g.idsede
					AND g.borrado	 = 0
				jOIN
					anio AS a
				ON
					a.idanio = g.idanio
					AND a.idsede = g.idsede
					AND a.borrado = 0
			WHERE 
				i.borrado		 = 0
				and i.idsede = idsedein
				AND a.idanio = idanioin
				AND g.idgrupo = idgrupoin
				and i.desercion = 0
			) AS ins
		ON
			ins.idalumno = f.idficha
			and ins.idsede = f.idsede
		
	WHERE 
		f.borrado = 0
		and ins.desercion = 0
		and f.idsede = idsedein
	ORDER BY
		ins.grupo,
		f.nombrealumno;
    END$$

CREATE  PROCEDURE `alumnosxedad` (IN `idsedein` INT)  BEGIN
SELECT 
		f.idficha,
		f.nombrealumno, 
		ins.idalumno, 
		ins.idgrupo,
		UCASE(ins.grupo) grupo,
		ins.idanio, 
		ins.anio, 
		ins.idinscripcion, 
		CASE WHEN ins.desercion = 0 THEN 'Activos' ELSE 'Deserciones' END desercion,
		f.fechanacimiento,
		CASE WHEN ISNULL(TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE())) = 1 THEN 0 ELSE TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE()) END edad
	
	FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					and i.idsede = g.idsede
					AND g.borrado	 = 0
				JOIN
					(
					SELECT  
						* 
					FROM 
						anio
					WHERE 
						estado = 1
						and idsede = idsedein
						AND borrado = 0
					ORDER BY 
						anio DESC
					LIMIT 1
					)AS a
				ON
					a.idanio = g.idanio
					and a.idsede = g.idsede
			WHERE 
				i.borrado		 = 0
				and i.idsede = idsedein
			) AS ins
		ON
			ins.idalumno = f.idficha
			and ins.idsede = f.idsede
	WHERE 
		f.borrado = 0
		AND ins.desercion = 0
		and ins.idsede = idsedein
	ORDER BY
		TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE()),
		f.nombrealumno;
	
    END$$

CREATE  PROCEDURE `alumnosxgrupo` (IN `idsedein` INT, IN `idgrupoin` INT)  BEGIN
SELECT 
		f.idficha,
		f.nombrealumno, 
		f.fechanacimiento,
		CASE WHEN ISNULL(TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE())) = 1 THEN 0 ELSE TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE()) END edad,
		f.sexo,
		f.responsable,
		f.telefonoresponsable,
		f.celularresponsable,
		ins.idalumno, 
		ins.idgrupo,
		UCASE(ins.grupo) grupo, 
		ins.idanio, 
		ins.anio, 
		ins.idinscripcion,
		totgrupo.totalgrupo,
		ins.desercion,
		f.voluntario
	FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					and i.idsede = g.idsede
					AND g.borrado	 = 0
				JOIN
					(
					SELECT  
						* 
					FROM 
						anio
					WHERE 
						estado = 1
						AND borrado = 0
						and idsede = idsedein
					ORDER BY 
						anio DESC
					LIMIT 1
					)AS a
				ON
					a.idanio = g.idanio
					and a.idsede = g.idsede
			WHERE 
				i.borrado		 = 0
				and i.idsede = idsedein
				AND g.idgrupo = idgrupoin
				
			) AS ins
		ON
			ins.idalumno = f.idficha
			and ins.idsede = f.idsede
		LEFT JOIN
			(
			SELECT 
				ins.idgrupo,
				UCASE(ins.grupo) grupo, 
				COUNT(ins.grupo) totalgrupo,
				ins.idsede
			FROM 
				ficha f 
 				JOIN
					(
					SELECT 
						i.idalumno, 
						g.idgrupo, 
						a.idanio, 
						g.grupo, 
						a.anio, 
						i.idinscripcion, 
						i.desercion,
						i.idsede  
					FROM 
						inscripcion i
 						JOIN
							grupo AS g
						ON
							i.idgrupo = g.idgrupo
							and i.idsede = g.idsede
							AND g.borrado	 = 0
						JOIN
							(
							SELECT  
								* 
							FROM 
								anio
							WHERE 
								estado = 1
								AND borrado = 0
								and idsede = idsedein
							ORDER BY 
								anio DESC
							LIMIT 1
							)AS a
						ON
							a.idanio = g.idanio
							and a.idsede = g.idsede
					WHERE 
						i.borrado		 = 0
						
						and i.idsede = idsedein
						AND g.idgrupo = idgrupoin
					) AS ins
				ON
					ins.idalumno = f.idficha
					and ins.idsede = f.idsede
			WHERE 
				f.borrado = 0
				and f.idsede = idsedein
			GROUP BY
				ins.grupo,
				ins.idsede
	
			) totgrupo
		ON
			totgrupo.idgrupo = ins.idgrupo
			and totgrupo.idsede = ins.idsede
		
	WHERE 
		f.borrado = 0
		
		and f.idsede = idsedein
	ORDER BY
		ins.grupo,
		f.nombrealumno;
    END$$

CREATE  PROCEDURE `alumnosxmonto` ()  BEGIN
SELECT 
		f.idficha,
		f.nombrealumno, 
		
		ins.idalumno, 
		ins.idgrupo,
		UCASE(ins.grupo) grupo,
		ins.idanio, 
		ins.anio, 
		ins.idinscripcion, 
		CASE WHEN ins.desercion = 0 THEN 'Activos' ELSE 'Deserciones' END desercion,
		ins.monto
		
	
	FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion,
				i.monto
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					AND g.borrado	 = 0
				JOIN
					(
					SELECT  
						* 
					FROM 
						anio
					WHERE 
						estado = 1
						AND borrado = 0
					ORDER BY 
						anio DESC
					LIMIT 1
					)AS a
				ON
					a.idanio = g.idanio
			WHERE 
				i.borrado		 = 0
			) AS ins
		ON
			ins.idalumno = f.idficha
	WHERE 
		f.borrado = 0
		AND ins.desercion = 0
	ORDER BY
		ins.monto,
		f.nombrealumno;
    END$$

CREATE  PROCEDURE `alumnosxsexo` (IN `idsedein` INT, IN `idanioin` INT)  BEGIN
SELECT 
		f.idficha,
		f.nombrealumno, 
		ins.idalumno, 
		ins.idgrupo,
		UCASE(ins.grupo) grupo,
		ins.idanio, 
		ins.anio, 
		ins.idinscripcion, 
		CASE WHEN ins.desercion = 0 THEN 'Activos' ELSE 'Deserciones' END desercion,
		CASE WHEN f.sexo = 'F' THEN 'Femenino' ELSE 'Masculino' END sexo,
		totgrupo.totalsexo,
		f.fechanacimiento,
		CASE WHEN ISNULL(TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE())) = 1 THEN 0 ELSE TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE()) END edad
	
	FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					and i.idsede = g.idsede
					AND g.borrado	 = 0
				JOIN
					anio AS a
				ON
					a.idanio = g.idanio
					AND a.idsede = g.idsede
					AND a.borrado = 0
			WHERE 
				i.borrado		 = 0
				AND a.idanio = idanioin
				AND i.idsede = idsedein
			) AS ins
		ON
			ins.idalumno = f.idficha
			AND ins.idsede = f.idsede
		left join
		(
		SELECT 
			CASE WHEN f.sexo = 'F' THEN 'Femenino' ELSE 'Masculino' END sexo,
			count(CASE WHEN f.sexo = 'F' THEN 'Femenino' ELSE 'Masculino' END) totalsexo 
		FROM 
			ficha f 
			JOIN
				(
				SELECT 
					i.idalumno, 
					g.idgrupo, 
					a.idanio, 
					g.grupo, 
					a.anio, 
					i.idinscripcion, 
					i.desercion  ,
					i.idsede
				FROM 
					inscripcion i
					JOIN
						grupo AS g
					ON
						i.idgrupo = g.idgrupo
						and i.idsede = g.idsede
						AND g.borrado	 = 0
					JOIN
						anio AS a
					ON
						a.idanio = g.idanio
						AND a.idsede = g.idsede
						AND a.borrado = 0
				WHERE 
					i.borrado		 = 0
					AND a.idanio = idanioin
					AND i.idsede = idsedein
				) AS ins
			ON
				ins.idalumno = f.idficha
				AND ins.idsede = f.idsede
		WHERE 
			f.borrado = 0
			AND ins.desercion = 0
		group by
			CASE WHEN f.sexo = 'F' THEN 'Femenino' ELSE 'Masculino' END
		)totgrupo
	on
		totgrupo.sexo = CASE WHEN f.sexo = 'F' THEN 'Femenino' ELSE 'Masculino' END
	WHERE 
		f.borrado = 0
		AND ins.desercion = 0
		
	ORDER BY
		f.sexo,
		f.nombrealumno;
	
    END$$

CREATE  PROCEDURE `alumnosxtipomatricula` (IN `idsedein` INT, IN `idanioin` INT)  BEGIN
SELECT 
		f.idficha,
		f.nombrealumno, 
		ins.monto,
		ins.idalumno, 
		ins.idgrupo,
		UCASE(ins.grupo) grupo,
		ins.idanio, 
		ins.anio, 
		ins.idinscripcion, 
		CASE WHEN ins.desercion = 0 THEN 'Activos' ELSE 'Deserciones' END desercion,
		ins.idtipomatricula,
		ins.tipomatricula,
		totgrupo.totaltipomatricula,
		Case when ins.convivio = 1 then 'Si' else 'No' end convivio,
		f.fechanacimiento,
		CASE WHEN ISNULL(TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE())) = 1 THEN 0 ELSE TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE()) END edad
	
	FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion,
				i.monto,
				i.idtipomatricula,
				tm.tipomatricula,
				i.convivio,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					and i.idsede = g.idsede
					AND g.borrado	 = 0
				JOIN
					tipomatricula AS tm
				ON
					tm.idtipomatricula = i.idtipomatricula
					and tm.idsede = i.idsede
					AND tm.borrado = 0
				JOIN
					anio AS a
				ON
					a.idanio = g.idanio
					AND a.idsede = g.idsede
					AND a.borrado = 0
			WHERE 
				i.borrado		 = 0
				AND a.idanio = idanioin
				AND i.idsede = idsedein
			) AS ins
		ON
			ins.idalumno = f.idficha
			and ins.idsede = f.idsede
		left join
		(
		SELECT 
			
			ins.tipomatricula,
			count(ins.tipomatricula) totaltipomatricula
		FROM 
			ficha f 
			JOIN
				(
				SELECT 
					i.idalumno, 
					g.idgrupo, 
					a.idanio, 
					g.grupo, 
					a.anio, 
					i.idinscripcion, 
					i.desercion,
					i.monto,
					i.idtipomatricula,
					tm.tipomatricula,
					i.convivio,
					i.idsede
				FROM 
					inscripcion i
					JOIN
						grupo AS g
					ON
						i.idgrupo = g.idgrupo
						and i.idsede = g.idsede
						AND g.borrado	 = 0
					JOIN
						tipomatricula AS tm
					ON
						tm.idtipomatricula = i.idtipomatricula
						and tm.idsede = i.idsede
						AND tm.borrado = 0
					JOIN
						anio AS a
					ON
						a.idanio = g.idanio
						AND a.idsede = g.idsede
						AND a.borrado = 0
				WHERE 
					i.borrado		 = 0
					AND a.idanio = idanioin
					AND i.idsede = idsedein
				) AS ins
			ON
				ins.idalumno = f.idficha
				and ins.idsede = f.idsede
		WHERE 
			f.borrado = 0
			AND ins.desercion = 0
		group by
			ins.tipomatricula
		) totgrupo
	on
		totgrupo.tipomatricula = ins.tipomatricula 
	WHERE 
		f.borrado = 0
		AND ins.desercion = 0
	ORDER BY
		ins.tipomatricula,
		ins.monto,
		f.nombrealumno;
    END$$

CREATE  PROCEDURE `alumnosxubicacion` (IN `idsedein` INT, IN `idanioin` INT)  BEGIN
SELECT 
		f.idficha,
		f.nombrealumno, 
		ins.idalumno, 
		ins.idgrupo,
		UCASE(ins.grupo) grupo,
		ins.idanio, 
		ins.anio, 
		ins.idinscripcion, 
		CASE WHEN ins.desercion = 0 THEN 'Activos' ELSE 'Deserciones' END desercion,
		CASE WHEN ISNULL(f.municipio) = 1 THEN '' ELSE f.municipio END municipio,
		CASE WHEN ISNULL(f.departamento) = 1 THEN '' ELSE f.departamento END departamento,
		f.direccion,
		f.telefonoresponsable,
		f.celularresponsable,
		totgrupo.totaldepartamento
	
	FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					and i.idsede = g.idsede
					AND g.borrado	 = 0
				JOIN
					anio AS a
				ON
					a.idanio = g.idanio
					AND a.idsede = g.idsede
					AND a.borrado = 0
			WHERE 
				i.borrado		 = 0
				and a.idanio = idanioin
				and i.idsede = idsedein
			) AS ins
		ON
			ins.idalumno = f.idficha
			and ins.idsede = f.idsede
		LEFT JOIN
			(
			SELECT 
				CASE WHEN ISNULL(f.departamento) = 1 THEN '' ELSE f.departamento END departamento,
				COUNT(CASE WHEN ISNULL(f.departamento) = 1 THEN '' ELSE f.departamento END) totaldepartamento
			FROM 
				ficha f 
 				JOIN
					(
					SELECT 
						i.idalumno, 
						g.idgrupo, 
						a.idanio, 
						g.grupo, 
						a.anio, 
						i.idinscripcion, 
						i.desercion  ,
						i.idsede
					FROM 
						inscripcion i
 						JOIN
							grupo AS g
						ON
							i.idgrupo = g.idgrupo
							and i.idsede = g.idsede
							AND g.borrado	 = 0
						JOIN
							anio AS a
						ON
							a.idanio = g.idanio
							AND a.idsede = g.idsede
							AND a.borrado = 0
					WHERE 
						i.borrado		 = 0
						AND i.desercion = 0
						AND i.idsede = idsedein
						and a.idanio = idanioin
					) AS ins
				ON
					ins.idalumno = f.idficha
					and ins.idsede = f.idsede
			WHERE 
				f.borrado = 0
			GROUP BY
				CASE WHEN ISNULL(f.departamento) = 1 THEN '' ELSE f.departamento END
	
			) totgrupo
		ON
			totgrupo.departamento = CASE WHEN ISNULL(f.departamento) = 1 THEN '' ELSE f.departamento END
	WHERE 
		f.borrado = 0
		AND ins.desercion = 0
	ORDER BY
		f.departamento,
		f.municipio,
		f.nombrealumno;
    END$$

CREATE  PROCEDURE `asistenciaxgrupo` (IN `idsedein` INT)  BEGIN
			SELECT DISTINCT
			ins.idgrupo,
			ins.grupo,
			asi.fechaasistencia,
			asi.idsede			
		FROM
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					AND g.borrado	 = 0
					AND g.idsede = i.idsede
				JOIN
					(
					SELECT  
						* 
					FROM 
						anio
					WHERE 
						estado = 1
						AND borrado = 0
						AND idsede = idsedein
					ORDER BY 
						anio DESC
					LIMIT 1
					)AS a
				ON
					a.idanio = g.idanio
					AND a.idsede = g.idsede
			WHERE 
				i.borrado		 = 0
				AND i.idsede = idsedein
			) ins
			JOIN
				asistencia AS ASI
			ON
				ASI.IDSEDE = INS.IDSEDE
				AND ASI.IDINSCRIPCION = INS.IDINSCRIPCION
				AND asi.borrado	= 0
		order BY
			1,3;
				
    END$$

CREATE  PROCEDURE `asistenciaxgrupoyfecha` (IN `idsedein` INT, IN `fechaasistenciain` DATE, IN `idgrupoin` INT)  BEGIN
			SELECT DISTINCT
			ins.idgrupo,
			ins.grupo,
			asi.fechaasistencia,
			asi.idsede			
		FROM
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					AND g.borrado	 = 0
					AND g.idsede = i.idsede
				JOIN
					(
					SELECT  
						* 
					FROM 
						anio
					WHERE 
						estado = 1
						AND borrado = 0
						AND idsede = idsedein
					ORDER BY 
						anio DESC
					LIMIT 1
					)AS a
				ON
					a.idanio = g.idanio
					AND a.idsede = g.idsede
			WHERE 
				i.borrado		 = 0
				AND i.idsede = idsedein
			) ins
			JOIN
				asistencia AS ASI
			ON
				ASI.IDSEDE = INS.IDSEDE
				AND ASI.IDINSCRIPCION = INS.IDINSCRIPCION
				AND asi.borrado	= 0
		WHERE
			ins.idgrupo = idgrupoin
			and asi.fechaasistencia = fechaasistenciain
			and asi.idsede = idsedein;
    END$$

CREATE  PROCEDURE `asistencia_edit` (IN `idsedein` INT, IN `fechaasistenciain` DATE, IN `idgrupoin` INT)  BEGIN
SELECT 
			ins.idgrupo,
			ins.grupo,
			asi.fechaasistencia,
			asi.idsede,
			asi.asiste,
			asi.idinscripcion,
			ins.idalumno,
			f.nombrealumno,
			f.voluntario,
			ins.desercion			
		FROM
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
				
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					AND g.borrado	 = 0
					AND g.idsede = idsedein
				JOIN
					(
					SELECT  
						* 
					FROM 
						anio
					WHERE 
						estado = 1
						AND borrado = 0
						AND idsede = idsedein
					ORDER BY 
						anio DESC
					LIMIT 1
					)AS a
				ON
					a.idanio = g.idanio
					AND a.idsede = g.idsede
			WHERE 
				i.borrado		 = 0
				AND i.idsede = idsedein
			) ins
			JOIN
				asistencia AS ASI
			ON
				ASI.IDSEDE = INS.IDSEDE
				AND ASI.IDINSCRIPCION = INS.IDINSCRIPCION
				AND asi.borrado	= 0
			JOIN
				ficha AS f
			ON
				f.idficha = ins.idalumno
				AND f.idsede = ins.idsede		
				
				AND f.borrado = 0		 
				
		WHERE
			ins.idgrupo = idgrupoin
			AND asi.fechaasistencia = fechaasistenciain
			AND asi.idsede = idsedein
		ORDER BY
			10,9,8
		;
    END$$

CREATE  PROCEDURE `asistencia_insert` (IN `idinscripcionin` INT, IN `fechaasistenciain` DATE, IN `idsedein` INT, IN `asistein` INT)  BEGIN
	insert into asistencia(idinscripcion, fechaasistencia, borrado, idsede, asiste)
	VALUES (idinscripcionin,fechaasistenciain,0,idsedein,asistein);
    END$$

CREATE  PROCEDURE `asistencia_update` (IN `idinscripcionin` INT, IN `fechaasistenciain` DATE, IN `idsedein` INT, IN `asistein` INT)  BEGIN
	update asistencia set asiste = asistein where idinscripcion = idinscripcionin and fechaasistencia = fechaasistenciain
	and idsede = idsedein;
    END$$

CREATE  PROCEDURE `delete_inscripcion` (IN `idsedein` INT, IN `idinscripcionin` INT)  BEGIN
	update inscripcion set 
		borrado=1	
	where 
		borrado = 0 
		and idsede = idsedein
		and idinscripcion= idinscripcionin;
	update
		asistencia set
		borrado = 1
	where
		borrado = 0
		and idinscripcion = idinscripcionin
		and idsede = idsedein;
    END$$

CREATE  PROCEDURE `desercionesxgrupo` (IN `idsedein` INT, IN `idanioin` INT, IN `idgrupoin` INT)  BEGIN
SELECT 
		f.idficha,
		f.nombrealumno, 
		f.fechanacimiento,
		CASE WHEN ISNULL(TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE())) = 1 THEN 0 ELSE TIMESTAMPDIFF(YEAR, f.fechanacimiento, CURDATE()) END edad,
		f.sexo,
		f.responsable,
		f.telefonoresponsable,
		f.celularresponsable,
		ins.idalumno, 
		ins.idgrupo,
		UCASE(ins.grupo) grupo, 
		ins.idanio, 
		ins.anio, 
		ins.idinscripcion,
		ins.fechadesercion
	FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede,
				i.fechadesercion
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					AND i.idsede = g.idsede
					AND g.borrado	 = 0
				JOIN
					anio AS a
				ON
					a.idanio = g.idanio
					AND a.idsede = g.idsede
					AND a.borrado = 0
			WHERE 
				i.borrado		 = 0
				AND i.desercion = 1
				AND i.idsede = idsedein
				AND a.idanio = idanioin
				AND g.idgrupo = idgrupoin
				
			) AS ins
		ON
			ins.idalumno = f.idficha
			AND ins.idsede = f.idsede
		
	WHERE 
		f.borrado = 0
		AND f.idsede = idsedein
	ORDER BY
		ins.grupo,
		f.nombrealumno;    
END$$

CREATE  PROCEDURE `estadisticasxanioysede` (IN `idsedein` INT, IN `idanioin` INT)  BEGIN
SELECT 
'Inscripciones' item,
COUNT(*) total
FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					AND i.idsede = g.idsede
					AND g.borrado	 = 0
				JOIN
					anio AS a
				ON
					a.idanio = g.idanio
					AND a.idsede = g.idsede
					AND a.borrado = 0
			WHERE 
				i.borrado		 = 0
				AND i.idsede = idsedein
				AND a.idanio = idanioin
				AND i.desercion = 0
			) AS ins
		ON
			ins.idalumno = f.idficha
			AND ins.idsede = f.idsede
		
	WHERE 
		f.borrado = 0
		AND ins.desercion = 0
UNION ALL
SELECT 
			CASE WHEN f.sexo = 'F' THEN 'Femenino' ELSE 'Masculino' END Item,
			COUNT(CASE WHEN f.sexo = 'F' THEN 'Femenino' ELSE 'Masculino' END) Cuenta 
		FROM 
			ficha f 
			JOIN
				(
				SELECT 
					i.idalumno, 
					g.idgrupo, 
					a.idanio, 
					g.grupo, 
					a.anio, 
					i.idinscripcion, 
					i.desercion  ,
					i.idsede
				FROM 
					inscripcion i
					JOIN
						grupo AS g
					ON
						i.idgrupo = g.idgrupo
						AND i.idsede = g.idsede
						AND g.borrado	 = 0
					JOIN
						anio AS a
					ON
						a.idanio = g.idanio
						AND a.idsede = g.idsede
						AND a.borrado = 0
				WHERE 
					i.borrado		 = 0
					AND a.idanio = idanioin
					AND i.idsede = idsedein
				) AS ins
			ON
				ins.idalumno = f.idficha
				AND ins.idsede = f.idsede
		WHERE 
			f.borrado = 0
			AND ins.desercion = 0
		GROUP BY
			CASE WHEN f.sexo = 'F' THEN 'Femenino' ELSE 'Masculino' END
UNION ALL
SELECT 
'Deserciones' Item,
COUNT(*) TOTAL
FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					AND i.idsede = g.idsede
					AND g.borrado	 = 0
				JOIN
					anio AS a
				ON
					a.idanio = g.idanio
					AND a.idsede = g.idsede
					AND a.borrado = 0
			WHERE 
				i.borrado		 = 0
				AND i.idsede = idsedein
				AND a.idanio = idanioin
				AND i.desercion = 1
			) AS ins
		ON
			ins.idalumno = f.idficha
			AND ins.idsede = f.idsede
		
	WHERE 
		f.borrado = 0
UNION ALL
SELECT 
			
			ins.tipomatricula,
			COUNT(ins.tipomatricula) totaltipomatricula
		FROM 
			ficha f 
			JOIN
				(
				SELECT 
					i.idalumno, 
					g.idgrupo, 
					a.idanio, 
					g.grupo, 
					a.anio, 
					i.idinscripcion, 
					i.desercion,
					i.monto,
					i.idtipomatricula,
					tm.tipomatricula,
					i.convivio,
					i.idsede
				FROM 
					inscripcion i
					JOIN
						grupo AS g
					ON
						i.idgrupo = g.idgrupo
						AND i.idsede = g.idsede
						AND g.borrado	 = 0
					JOIN
						tipomatricula AS tm
					ON
						tm.idtipomatricula = i.idtipomatricula
						AND tm.idsede = i.idsede
						AND tm.borrado = 0
					JOIN
						anio AS a
					ON
						a.idanio = g.idanio
						AND a.idsede = g.idsede
						AND a.borrado = 0
				WHERE 
					i.borrado		 = 0
					AND a.idanio = idanioin
					AND i.idsede = idsedein
				) AS ins
			ON
				ins.idalumno = f.idficha
				AND ins.idsede = f.idsede
		WHERE 
			f.borrado = 0
			AND ins.desercion = 0
		GROUP BY
			ins.tipomatricula
UNION ALL
SELECT 
			CASE WHEN f.voluntario = '1' THEN 'Voluntario' ELSE 'Alumno' END item,
			COUNT(CASE WHEN f.voluntario = '1' THEN 'Voluntario' ELSE 'Alumno' END) total 
		FROM 
			ficha f 
			JOIN
				(
				SELECT 
					i.idalumno, 
					g.idgrupo, 
					a.idanio, 
					g.grupo, 
					a.anio, 
					i.idinscripcion, 
					i.desercion  ,
					i.idsede
				FROM 
					inscripcion i
					JOIN
						grupo AS g
					ON
						i.idgrupo = g.idgrupo
						AND i.idsede = g.idsede
						AND g.borrado	 = 0
					JOIN
						anio AS a
					ON
						a.idanio = g.idanio
						AND a.idsede = g.idsede
						AND a.borrado = 0
				WHERE 
					i.borrado		 = 0
					AND a.idanio = idanioin
					AND i.idsede = idsedein
				) AS ins
			ON
				ins.idalumno = f.idficha
				AND ins.idsede = f.idsede
		WHERE 
			f.borrado = 0
			AND ins.desercion = 0
		GROUP BY
			CASE WHEN f.voluntario	= '1' THEN 'Voluntario' ELSE 'Alumno' END
		;
    END$$

CREATE  PROCEDURE `grupos` (IN `idsedein` INT)  BEGIN
SELECT 
	g.idgrupo,g.grupo,a.anio, g.borrado
FROM grupo AS g
	INNER JOIN
		(
			SELECT  * FROM anio
			WHERE estado = 1
			AND borrado = 0
			AND idsede = idsedein
			ORDER BY anio DESC
			LIMIT 1
		)AS a
	ON
		a.idanio = g.idanio
		AND a.idsede = g.idsede
	WHERE g.borrado = 0 
	AND g.idsede = idsedein
	ORDER BY idgrupo;
    END$$

CREATE  PROCEDURE `inscripciones` (IN `idsedein` INT)  BEGIN
	SELECT 
		f.idficha,
		f.nombrealumno, 
		ins.idalumno, 
		ins.idgrupo,
		UCASE(ins.grupo) grupo,
		totgrupo.totalgrupo, 
		ins.idanio, 
		ins.anio, 
		ins.idinscripcion, 
		CASE WHEN ins.desercion = 0 THEN 'Activos' ELSE 'Deserciones' END desercion,
		totalactivos.totaldesercion
	
	FROM 
		ficha f 
 		JOIN
			(
			SELECT 
				i.idalumno, 
				g.idgrupo, 
				a.idanio, 
				g.grupo, 
				a.anio, 
				i.idinscripcion, 
				i.desercion  ,
				i.idsede
			FROM 
				inscripcion i
 				JOIN
					grupo AS g
				ON
					i.idgrupo = g.idgrupo
					AND g.borrado	 = 0
					and g.idsede = i.idsede
				JOIN
					(
					SELECT  
						* 
					FROM 
						anio
					WHERE 
						estado = 1
						AND borrado = 0
						and idsede = idsedein
					ORDER BY 
						anio DESC
					LIMIT 1
					)AS a
				ON
					a.idanio = g.idanio
					and a.idsede = g.idsede
			WHERE 
				i.borrado		 = 0
				and i.idsede = idsedein
			) AS ins
		ON
			ins.idalumno = f.idficha
			and ins.idsede = f.idsede
		
		LEFT JOIN
			(
			SELECT 
				ins.idgrupo,
				UCASE(ins.grupo) grupo, 
				COUNT(ins.grupo) totalgrupo,
				f.idsede
			FROM 
				ficha f 
 				JOIN
					(
					SELECT 
						i.idalumno, 
						g.idgrupo, 
						a.idanio, 
						g.grupo, 
						a.anio, 
						i.idinscripcion, 
						i.desercion,  
						i.idsede
					FROM 
						inscripcion i
 						JOIN
							grupo AS g
						ON
							i.idgrupo = g.idgrupo
							AND g.borrado	 = 0
							and g.idsede = i.idsede
						JOIN
							(
							SELECT  
								* 
							FROM 
								anio
							WHERE 
								estado = 1
								AND borrado = 0
								and idsede = idsedein
							ORDER BY 
								anio DESC
							LIMIT 1
							)AS a
						ON
							a.idanio = g.idanio
							and a.idsede = g.idsede
					WHERE 
						i.borrado		 = 0
						and i.idsede = idsedein
					) AS ins
				ON
					ins.idalumno = f.idficha
					and ins.idsede = f.idsede
			WHERE 
				f.borrado = 0
				and f.idsede = idsedein
			GROUP BY
				ins.grupo,
				f.idsede
			) totgrupo
		ON
			totgrupo.idgrupo = ins.idgrupo
			and totgrupo.idsede = ins.idsede
		LEFT JOIN
			(
			SELECT 
				ins.idgrupo,
				UCASE(ins.grupo) grupo, 
				COUNT(CASE WHEN ins.desercion = 0 THEN 'Activos' ELSE 'Deserciones' END) totaldesercion,
				CASE WHEN ins.desercion = 0 THEN 'Activos' ELSE 'Deserciones' END desercion,
				f.idsede
			FROM 
				ficha f 
	 			JOIN
					(
					SELECT 
						i.idalumno, 
						g.idgrupo, 
						a.idanio, 
						g.grupo, 
						a.anio, 
						i.idinscripcion, 
						i.desercion,
						i.idsede  
					FROM 
						inscripcion i
	 					JOIN
							grupo AS g
						ON
							i.idgrupo = g.idgrupo
							AND g.borrado	 = 0
							and g.idsede = i.idsede
						JOIN
							(
							SELECT  
								* 
							FROM 
								anio
							WHERE 
								estado = 1
								AND borrado = 0
								and idsede = idsedein
							ORDER BY 
								anio DESC
							LIMIT 1
							)AS a
						ON
							a.idanio = g.idanio
							and a.idsede = g.idsede
					WHERE 
						i.borrado		 = 0
						and i.idsede = idsedein
					) AS ins
				ON
					ins.idalumno = f.idficha
					and ins.idsede = f.idsede
			WHERE 
				f.borrado = 0
				and f.idsede = idsedein
			GROUP BY
				ins.grupo,
				CASE WHEN ins.desercion = 0 THEN 'Activos' ELSE 'Deserciones' END,
				f.idsede
			) AS totalactivos
		ON
			totalactivos.idgrupo = ins.idgrupo
			AND totalactivos.desercion = CASE WHEN ins.desercion = 0 THEN 'Activos' ELSE 'Deserciones' END
			and totalactivos.idsede = ins.idsede
	WHERE 
		f.borrado = 0
		and f.idsede = idsedein
	ORDER BY
		ins.grupo,
		CASE WHEN ins.desercion = 0 THEN 'Activo' ELSE 'Deserciones' END,
		f.nombrealumno;
	
    END$$

CREATE  PROCEDURE `insert_inscripcion` (IN `idalumnoin` INT, IN `idgrupoin` INT, IN `idtipomatriculain` INT, IN `montoin` DECIMAL(10,2), IN `convivioin` INT, IN `idsedein` INT, IN `fechainscripcionin` DATE)  BEGIN
	
    
	INSERT INTO inscripcion ( idalumno, idgrupo, idtipomatricula, monto, convivio, desercion, borrado, idsede, fechainscripcion) 
		VALUES ( idalumnoin, idgrupoin, idtipomatriculain, montoin, convivioin, 0, 0, idsedein, fechainscripcionin);
	set @idinscripcionin = (SELECT max(idinscripcion) FROM inscripcion);
	CREATE TEMPORARY TABLE TmpTable (id INT AUTO_INCREMENT PRIMARY KEY,fecha DATE); 
	INSERT INTO TmpTable(fecha) 
	SELECT fechagrupo FROM (
	SELECT DISTINCT fechaasistencia AS fechagrupo FROM inscripcion AS ins
	JOIN
		asistencia AS asi
	ON
		asi.idinscripcion = ins.idinscripcion
		AND asi.borrado	 = 0
		AND asi.idsede	 = ins.idsede
	WHERE
		ins.borrado = 0
		AND ins.idgrupo = idgrupoin
		AND ins.idsede = idsedein
	ORDER BY
		1) AS asigrupo
	;
	SET @total = (SELECT COUNT(*) FROM Tmptable);
	SET @cuenta := 0;
	WHILE @cuenta < @total DO
	    
	    SET @cuenta = @cuenta+1;
	    SET @fecha = (SELECT fecha FROM tmptable WHERE id = @cuenta);
	    INSERT INTO asistencia(idinscripcion, fechaasistencia, borrado, idsede, asiste)
		VALUES (@idinscripcionin,@fecha,0,idsedein,0);
	  END WHILE;
	DROP TABLE tmptable;
    END$$

CREATE  PROCEDURE `reporte_asistenciaxgrupo` (IN `idsedein` INT, IN `idanioin` INT, IN `idgrupoin` INT, IN `fechainicioin` DATE, IN `fechafinin` DATE)  BEGIN
    SELECT 
	fi.nombrealumno,
	asi.fechaasistencia	,
	asi.asiste	,
	g.grupo	,
	an.anio	,
	an.tema	,
	se.nombresede,
	fi.voluntario	
FROM 
	asistencia AS asi
	JOIN
		inscripcion AS ins
	ON
		ins.borrado	= 0
		AND ins.idinscripcion		= asi.idinscripcion				
		AND ins.idsede		= asi.idsede	
	JOIN
		ficha AS fi
	ON
		fi.idficha	 = ins.idalumno	
		AND fi.borrado		= 0
		AND fi.idsede		= ins.idsede		
	JOIN
		grupo AS g
	ON
		g.idgrupo	= ins.idgrupo	
		AND g.borrado		 = 0
		AND g.idsede		= ins.idsede	
	JOIN
		anio AS an
	ON
		an.idanio	= g.idanio	
		AND an.borrado		 = 0
		AND an.idsede		= g.idsede		
	JOIN
			
		sede AS se
	ON
		se.idsede	 = an.idsede	
		AND se.borrado		 = 0
WHERE
	asi.borrado = 0
	AND asi.idsede	= idsedein
	AND ins.idgrupo	= idgrupoin
	AND an.idanio	 = idanioin
	and (asi.fechaasistencia between fechainicioin and fechafinin)
order by
	asi.fechaasistencia,
	fi.nombrealumno;
    END$$

CREATE  PROCEDURE `update_inscripcion` (IN `post_tipomatricula` INT, IN `post_grupo` INT, IN `post_monto` DECIMAL(10,2), IN `post_convivio` INT, IN `post_desercion` INT, `post_fechainscripcion` DATE, IN `fechadesercionin` DATE, IN `idsedein` INT, `idinscripcionin` INT)  BEGIN
	
CREATE TEMPORARY TABLE TmpTable (id INT AUTO_INCREMENT PRIMARY KEY,fecha DATE); 
INSERT INTO TmpTable(fecha) SELECT fechagrupo FROM (
SELECT DISTINCT fechaasistencia AS fechagrupo FROM inscripcion AS ins
JOIN
	asistencia AS asi
ON
	asi.idinscripcion = ins.idinscripcion
	AND asi.borrado	 = 0
	AND asi.idsede	 = ins.idsede
WHERE
	ins.borrado = 0
	AND ins.idgrupo = post_grupo
	and ins.idsede = idsedein
ORDER BY
	1) AS asigrupo
LEFT JOIN
(
SELECT DISTINCT fechaasistencia AS fechainscripcion FROM inscripcion AS ins
JOIN
	asistencia AS asi
ON
	asi.idinscripcion = ins.idinscripcion
	AND asi.borrado	 = 0
	AND asi.idsede	 = ins.idsede
	
WHERE
	ins.borrado = 0
	AND ins.idinscripcion	= idinscripcionin
	and ins.idsede = idsedein
ORDER BY
	1
) AS asiinscripcion	
ON
	asigrupo.fechagrupo = asiinscripcion.fechainscripcion
WHERE asiinscripcion.fechainscripcion IS NULL;
SET @total = (SELECT COUNT(*) FROM Tmptable);
SET @cuenta := 0;
WHILE @cuenta < @total DO
    
    SET @cuenta = @cuenta+1;
    SET @fecha = (SELECT fecha FROM tmptable WHERE id = @cuenta);
    INSERT INTO asistencia(idinscripcion, fechaasistencia, borrado, idsede, asiste)
	VALUES (idinscripcionin,@fecha,0,idsedein,0);
  END WHILE;
TRUNCATE TABLE tmptable;
INSERT INTO TmpTable(fecha) SELECT fechainscripcion FROM (
SELECT DISTINCT fechaasistencia AS fechagrupo FROM inscripcion AS ins
JOIN
	asistencia AS asi
ON
	asi.idinscripcion = ins.idinscripcion
	AND asi.borrado	 = 0
	AND asi.idsede	 = ins.idsede
	
WHERE
	ins.borrado = 0
	AND ins.idgrupo = post_grupo
	and ins.idsede = idsedein
ORDER BY
	1) AS asigrupo
RIGHT JOIN
(
SELECT DISTINCT fechaasistencia AS fechainscripcion FROM inscripcion AS ins
JOIN
	asistencia AS asi
ON
	asi.idinscripcion = ins.idinscripcion
	AND asi.borrado	 = 0
	AND asi.idsede	 = ins.idsede
	
WHERE
	ins.borrado = 0
	AND ins.idinscripcion	= idinscripcionin
	and ins.idsede = idsedein
ORDER BY
	1
) AS asiinscripcion	
ON
	asigrupo.fechagrupo = asiinscripcion.fechainscripcion
WHERE asigrupo.fechagrupo IS NULL;
SET @total = (SELECT COUNT(*) FROM Tmptable);
SET @cuenta := 0;
WHILE @cuenta < @total DO
    SET @cuenta = @cuenta+1;
    
    SET @fecha =  (SELECT fecha FROM tmptable WHERE id = @cuenta);
    UPDATE asistencia SET borrado = 1 WHERE borrado = 0 AND fechaasistencia = @fecha AND idinscripcion = idinscripcionin; 
  END WHILE;
DROP TABLE tmptable;
    
    UPDATE inscripcion SET
			idtipomatricula= post_tipomatricula, 
			idgrupo=post_grupo, 
			monto=post_monto, 
			convivio=post_convivio, 
			desercion=post_desercion,
			fechainscripcion =post_fechainscripcion,
			fechadesercion = fechadesercionin
		WHERE
			borrado = 0 AND idsede = idsedein AND idinscripcion= idinscripcionin;
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anio`
--

CREATE TABLE `anio` (
  `idanio` int(11) NOT NULL COMMENT 'id del año',
  `anio` int(4) DEFAULT NULL COMMENT 'año',
  `estado` int(1) DEFAULT NULL COMMENT 'estado ya sea abierto o cerrado',
  `tema` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'tema de verano aventura',
  `borrado` int(1) DEFAULT NULL,
  `idsede` int(11) DEFAULT NULL COMMENT 'id de la sede'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 COLLATE=latin1_spanish_ci DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE `asistencia` (
  `idasistencia` int(11) NOT NULL COMMENT 'id de la asistencia',
  `idinscripcion` int(11) DEFAULT NULL,
  `fechaasistencia` date DEFAULT NULL COMMENT 'Fecha de la Asistencia',
  `borrado` int(11) DEFAULT NULL COMMENT 'borrado = 1',
  `idsede` int(11) DEFAULT NULL,
  `asiste` int(11) DEFAULT NULL COMMENT 'asistió = 1 no asistió = 0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 COLLATE=latin1_spanish_ci DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ficha`
--

CREATE TABLE `ficha` (
  `idficha` int(11) NOT NULL COMMENT 'Id de la ficha',
  `nombrealumno` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Nombre del alumno',
  `fechanacimiento` date DEFAULT NULL COMMENT 'Fecha de Nacimiento',
  `sexo` varchar(1) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Sexo M Masculino F Femenino',
  `direccion` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Direccion',
  `municipio` varchar(30) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Municipio',
  `departamento` varchar(30) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Departamento',
  `responsable` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Nombre del Responsable',
  `parentescoresponsable` varchar(45) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Tipo de Parentesco del Responsable',
  `telefonoresponsable` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `celularresponsable` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `centroestudios` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `gradoacademico` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `alergia` int(1) DEFAULT NULL COMMENT '1 = si 0 = no',
  `asma` int(1) DEFAULT NULL,
  `bronquitis` int(1) DEFAULT NULL,
  `epilepsias` int(1) DEFAULT NULL,
  `fobias` int(1) DEFAULT NULL,
  `convulsiones` int(1) DEFAULT NULL,
  `gastritis` int(1) DEFAULT NULL,
  `colitis` int(1) DEFAULT NULL,
  `cardiacos` int(1) DEFAULT NULL,
  `otros` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `borrado` int(1) DEFAULT NULL COMMENT 'borrado',
  `activo` int(1) DEFAULT NULL COMMENT 'si esta activo o no',
  `voluntario` int(1) DEFAULT NULL COMMENT '1 = voluntario, 0 = estudiante',
  `idsede` int(11) DEFAULT NULL COMMENT 'Id de la Sede'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 COLLATE=latin1_spanish_ci DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `idgrupo` int(11) NOT NULL COMMENT 'Id del Grupo',
  `grupo` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Nombre del Grupo',
  `idanio` int(11) DEFAULT NULL COMMENT 'Id del Año que pertenece',
  `borrado` int(11) DEFAULT NULL,
  `idsede` int(11) DEFAULT NULL COMMENT 'Id de la Sede'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 COLLATE=latin1_spanish_ci DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion`
--

CREATE TABLE `inscripcion` (
  `idinscripcion` int(11) NOT NULL COMMENT 'Id de la Inscripciones',
  `idalumno` int(11) DEFAULT NULL COMMENT 'Alumno',
  `idgrupo` int(11) DEFAULT NULL COMMENT 'Grupo',
  `idtipomatricula` int(11) DEFAULT NULL COMMENT 'Tipo de Matricula',
  `monto` decimal(10,2) DEFAULT NULL COMMENT 'Monto a pagar',
  `convivio` int(11) DEFAULT NULL COMMENT 'Convivio',
  `desercion` int(11) DEFAULT NULL COMMENT 'Deserción',
  `borrado` int(11) DEFAULT NULL COMMENT 'Borrado',
  `idsede` int(11) DEFAULT NULL COMMENT 'Id de la Sede',
  `fechainscripcion` date DEFAULT NULL COMMENT 'Fecha de la Inscripción',
  `fechadesercion` date DEFAULT NULL COMMENT 'Fecha de la deserción'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 COLLATE=latin1_spanish_ci DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sede`
--

CREATE TABLE `sede` (
  `idsede` int(11) NOT NULL COMMENT 'Id de la Sede',
  `nombresede` varchar(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Nombre de la Sede',
  `borrado` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `sede`
--

INSERT INTO `sede` (`idsede`, `nombresede`, `borrado`) VALUES
(1, 'Soyapango', 0),
(2, 'Santa Ana', 0),
(3, 'San Miguel', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipomatricula`
--

CREATE TABLE `tipomatricula` (
  `idtipomatricula` int(11) NOT NULL COMMENT 'Id del Tipo de Matrícula',
  `tipomatricula` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Nombre del Tipo de Matrícula',
  `borrado` int(11) DEFAULT NULL COMMENT 'Si es borrado = 1',
  `idsede` int(11) DEFAULT NULL COMMENT 'Id de la Sede'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 COLLATE=latin1_spanish_ci DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anio`
--
ALTER TABLE `anio`
  ADD PRIMARY KEY (`idanio`),
  ADD KEY `FK_anio_sede` (`idsede`);

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD PRIMARY KEY (`idasistencia`),
  ADD KEY `FK_asistencia_sede` (`idsede`),
  ADD KEY `FK_asistencia_inscripcion` (`idinscripcion`);

--
-- Indices de la tabla `ficha`
--
ALTER TABLE `ficha`
  ADD PRIMARY KEY (`idficha`),
  ADD KEY `FK_ficha_sede` (`idsede`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`idgrupo`),
  ADD KEY `FK_grupo` (`idanio`),
  ADD KEY `FK_grupo_sede` (`idsede`);

--
-- Indices de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD PRIMARY KEY (`idinscripcion`),
  ADD KEY `FK_inscripcion` (`idgrupo`),
  ADD KEY `FK_ficha` (`idalumno`),
  ADD KEY `FK_tipomatricula` (`idtipomatricula`),
  ADD KEY `FK_inscripcion_sede` (`idsede`);

--
-- Indices de la tabla `sede`
--
ALTER TABLE `sede`
  ADD PRIMARY KEY (`idsede`);

--
-- Indices de la tabla `tipomatricula`
--
ALTER TABLE `tipomatricula`
  ADD PRIMARY KEY (`idtipomatricula`),
  ADD KEY `FK_tipomatricula_sede` (`idsede`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anio`
--
ALTER TABLE `anio`
  MODIFY `idanio` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id del año', AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  MODIFY `idasistencia` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la asistencia', AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT de la tabla `ficha`
--
ALTER TABLE `ficha`
  MODIFY `idficha` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de la ficha', AUTO_INCREMENT=219;
--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `idgrupo` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del Grupo', AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  MODIFY `idinscripcion` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de la Inscripciones', AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT de la tabla `sede`
--
ALTER TABLE `sede`
  MODIFY `idsede` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de la Sede', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tipomatricula`
--
ALTER TABLE `tipomatricula`
  MODIFY `idtipomatricula` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del Tipo de Matrícula', AUTO_INCREMENT=15;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `anio`
--
ALTER TABLE `anio`
  ADD CONSTRAINT `FK_anio_sede` FOREIGN KEY (`idsede`) REFERENCES `sede` (`idsede`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD CONSTRAINT `FK_asistencia_inscripcion` FOREIGN KEY (`idinscripcion`) REFERENCES `inscripcion` (`idinscripcion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_asistencia_sede` FOREIGN KEY (`idsede`) REFERENCES `sede` (`idsede`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ficha`
--
ALTER TABLE `ficha`
  ADD CONSTRAINT `FK_ficha_sede` FOREIGN KEY (`idsede`) REFERENCES `sede` (`idsede`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `FK_grupo` FOREIGN KEY (`idanio`) REFERENCES `anio` (`idanio`),
  ADD CONSTRAINT `FK_grupo_sede` FOREIGN KEY (`idsede`) REFERENCES `sede` (`idsede`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD CONSTRAINT `FK_ficha` FOREIGN KEY (`idalumno`) REFERENCES `ficha` (`idficha`),
  ADD CONSTRAINT `FK_inscripcion` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`),
  ADD CONSTRAINT `FK_inscripcion_sede` FOREIGN KEY (`idsede`) REFERENCES `sede` (`idsede`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_tipomatricula` FOREIGN KEY (`idtipomatricula`) REFERENCES `tipomatricula` (`idtipomatricula`);

--
-- Filtros para la tabla `tipomatricula`
--
ALTER TABLE `tipomatricula`
  ADD CONSTRAINT `FK_tipomatricula_sede` FOREIGN KEY (`idsede`) REFERENCES `sede` (`idsede`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
