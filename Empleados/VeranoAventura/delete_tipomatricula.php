<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"];
$anioactivo = $_SESSION["anioactivo"];


require_once("db.php");
//CONDICIONES PARA ELIMINAR TIPO DE MATRICULA
$pdo_statement = $pdo_conn->prepare("SELECT * FROM inscripcion WHERE borrado = 0 and idsede = ".$idsede." AND idtipomatricula =".$_GET["idtipomatricula"]);
$pdo_statement->execute();
$cuentaFilas = $pdo_statement->rowCount();

if(!$cuentaFilas){
	$pdo_statement=$pdo_conn->prepare("update tipomatricula set 
				borrado='1'
				where borrado = 0 and idsede = ".$idsede." and idtipomatricula=" . $_GET["idtipomatricula"]);
	$pdo_statement->execute();
	header('location:tipomatricula.php');
}//if
else{
	$bandera = 1; //echo("No se puede Eliminar Tipo de Matricula");
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Eliminar Tipo de Matrícula</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">
	
	<div class="alert alert-danger" role="alert" <?php echo ($bandera == 1)?"":"hidden" ?>>
  		No se puede eliminar Tipo de Matrícula debido a que tienen Inscripciones Activas!
  	</div>
	</div>
	</main>
	<?php include ("footer_js.php"); ?>
</body>
</html>
