<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"];
$nombresede = $_SESSION["nombresede"];
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
$pdo_statement = $pdo_conn->prepare("call desercionesxgrupo");
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
$cuentaFilas = $pdo_statement->rowCount();
$pdo_statement->closeCursor();

$pdo_statement1 = $pdo_conn->prepare("SELECT anio,tema
		FROM anio WHERE borrado = 0 AND estado = 1 ORDER BY anio DESC
		LIMIT 1");
$pdo_statement1->execute();
$result1 = $pdo_statement1->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Listado de Deserciones por Grupo</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">

	<?php
	if(!empty($result1)) { 
		foreach($result1 as $row1) {
		?>
			<div><h2> Listado de Deserciones por Grupo<br> 
				Año : <?php echo $row1["anio"]; ?><br>
  				Tema : <?php echo $row1["tema"]; ?></h2><br>
  				<h3>Total de Alumnos - <?php echo $cuentaFilas; ?>  </h3>
			</div>
    <?php
		}
	}
	?>
	<table class="table table-hover">
	  	<thead>
			<tr>
				
				<th scope="col">Nombre</th>
				<th scope="col">Fecha de Nacimiento</th>
				<th scope="col">Edad</th>
				<th scope="col">Sexo</th>
				<th scope="col">Responsable</th>
				<th scope="col">Teléfonos</th>
			</tr>
	  	</thead>
		<tbody id="table-body">
		<?php

			if(!empty($result)) { 
				$current_grupo = null;
				$current_activo = null;
				foreach($result as $row) {
					if ($row["grupo"] != $current_grupo) {
						$current_activo = null;
    					$current_grupo = $row["grupo"];
    					?>
   						<tr class="table-row">
							<td><b><?php echo $row["grupo"]; ?> - Total <?php echo $row["totalgrupo"]; ?></b></td>
							<td></td>
						</tr>	
    					<?php
  					}
  					?>
  						<tr class="table-row">
							<td><?php echo $row["nombrealumno"]; ?></td>
							<td><?php echo date_format(date_create($row["fechanacimiento"]), 'd/m/Y'); ?></td>
							<td><?php echo $row["edad"]; ?></td>
							<td><?php if($row["sexo"]=="M")  echo 'Masculino'; else echo 'Femenino' ; ?></td>
							<td><?php echo $row["responsable"]; ?></td>
							<td><?php echo $row["telefonoresponsable"]; ?> / <?php echo $row["celularresponsable"]; ?></td>
						</tr>
				<?php	
				}	
			}
		?>
		</tbody>
	</table>
	</div>
</main>
	<?php include ("footer_js.php"); ?>
</body>
</html>