<?php
session_start(); 
if( !isset($_SESSION["sede"]) ){
    header("location:index.php");
    exit();
}
$idsede = $_SESSION["sede"]; 
$nombresede = $_SESSION["nombresede"]; 
$anioactivo = $_SESSION["anioactivo"];

require_once("db.php");
if(!empty($_POST["save_record"])) {
	$pdo_statement=$pdo_conn->prepare("update anio set 
				anio='" . $_POST[ 'post_anio' ] . 
				"', estado='" . $_POST[ 'post_estado' ]. 
				"', tema='" . $_POST[ 'post_tema' ]. 
				"' where borrado = 0 and idsede=".$idsede." and idanio=" . $_GET["idanio"]);
	$result = $pdo_statement->execute();
	if($result) {
		header('location:anio.php');
	}//if
}//if
else{
	$pdo_statement = $pdo_conn->prepare("SELECT anio,estado,tema FROM anio where borrado = 0 and idsede = ".$idsede." and idanio=" . $_GET["idanio"]);
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();
}//else
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Editar Año</title>
	<?php include ("header_css.php"); ?>
</head>
<body>
	<?php include ("navigation.php"); ?>
	<main role="main" class="container">
	<div class="container">

		<h1>Editar Año</h1>
		<form name="frmAdd" action="" method="POST">
		  	<div class="form-group">
			  	<label>Año: </label><br>
			  	<input type="number" name="post_anio" class="form-control"  min="2000" max="2050" 
			  	value="<?php echo $result[0]['anio']; ?>"
			  	required readonly/>
		  	</div>

			<fieldset class="form-group">
			    <legend>Estado</legend>
			    <div class="form-check">
			      <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_estado" id="id_post_estado1" value="1" <?php if($result[0]['estado']==1) echo 'checked';?> >
			        Abierto
			      </label>
			    </div>
			    <div class="form-check">
			    <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="post_estado" id="id_post_estado2" value="0" <?php if($result[0]['estado']==0) echo 'checked';?>>
			        Cerrado
			      </label>
			    </div>
			 </fieldset>

		  	<div class="form-group">
			  	<label>Tema de Verano-Aventura: </label><br>
			  	<textarea name="post_tema" class="form-control"  rows="5" maxlenght="255" required ><?php echo $result[0]['tema']; ?></textarea>
		  	</div>

		  	<div class="form-group">
			  	<input name="save_record" type="submit" value="Guardar cambios" class="btn btn-primary"
			  	onclick="return confirm('Está seguro que desea guardar los cambios ?')">
			</div>
		</form>
	</div>
</main>
	<?php include ("footer_js.php"); ?>	

</body>
</html>