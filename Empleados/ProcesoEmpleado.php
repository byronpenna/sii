<?php 
    
    if(isset($_GET['l']))
    {
        if($_GET['l'] != "EvaluationExecute")
        {
            if($_GET['l'] != "ResultArea")
            {
                if($_GET['l'] != "Activity")
                {
                    echo "<div style='float:left; padding-left: 52px;'>";
                    include ('Menu/meen2.php');
                    echo "</div>";   
                }   
            }                        
        }
         
       	if(isset($_GET['n']))
    	{
    		if($_GET['n'] == 1)
    			echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right'>Datos Ingresados Exitosamente!</div>";
    			
    		if($_GET['n'] == 2)
    			echo "<div id='Notificacion' name='Notificacion' style='color:blue; text-align: right'>Datos Actualizados Exitosamente!</div>";
                
    		if($_GET['n'] == 3)
    			echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right'>Datos Eliminados Exitosamente...</div>";
                
    		if($_GET['n'] == 4)
    			echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right'>Acceso Denegado...</div>"; 
    
    		if($_GET['n'] == 5)
    			echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right'>Error a sucesido un problema, comuniquese con los administradores del sistema...</div>";                                   
    	}
        
        
        
        
        /** Datos de Información **/    

        /** 1- Información Personal **/
        if($_GET['l'] == "PersonalData")
            include("Empleados/DatosInformacion/DatosPersonales.php"); 
   
        else if($_GET['l'] == "PersonalForm")
            include("Empleados/DatosInformacion/DatosPersonalesForm.php");
            
            
        /** 2- Información Familiar **/
        if($_GET['l'] == "FamiliarData")
            include("Empleados/DatosInformacion/DatosFamiliares.php"); 
   
        else if($_GET['l'] == "FamiliarForm")
            include("Empleados/DatosInformacion/DatosFamiliaresForm.php");    
            
            
        /** 3- Información Academicos **/
        if($_GET['l'] == "AcademicData")
            include("Empleados/DatosInformacion/DatosAcademicos.php"); 
   
        else if($_GET['l'] == "AcademicForm")
            include("Empleados/DatosInformacion/DatosAcademicosForm.php");
   
            
        /** 4- Información Laborales **/
        if($_GET['l'] == "EmploymentData")
            include("Empleados/DatosInformacion/DatosLaborales.php"); 
   
        else if($_GET['l'] == "EmploymentForm")
            include("Empleados/DatosInformacion/DatosLaboralesForm.php");  
            
            
        /** 5- Información de Salud **/
        if($_GET['l'] == "HealthData")
            include("Empleados/DatosInformacion/DatosSalud.php"); 
   
        else if($_GET['l'] == "HealthForm")
            include("Empleados/DatosInformacion/DatosSaludForm.php");    
            
            
        /** 6- Información Religiosa **/
        if($_GET['l'] == "ReligiousData")
            include("Empleados/DatosInformacion/DatosReligiosos.php"); 
   
        else if($_GET['l'] == "ReligiousForm")
            include("Empleados/DatosInformacion/DatosReligiososForm.php");       
        
        /** 6- Información de Documentos **/    
        if($_GET['l'] == "DocumentsData")
            include("Empleados/DatosInformacion/Documentos.php"); 
   
        else if($_GET['l'] == "DocumentsForm")
            include("Empleados/DatosInformacion/DocumentosForm.php");  
            
        else if($_GET['l'] == "Download")
            include("Empleados/DatosInformacion/DocumentosDescarga.php");                                                                                                
            
            
            
        /** Administración **/
        
        /** Busqueda de Empleados **/
        else if($_GET['l'] == "SearchEmployee")
            include("Empleados/Administracion/BusquedaEmpleado.php");   
            
        else if($_GET['l'] == "EmployeeProfile")
            include("Empleados/Administracion/PerfilEmpleado.php");  

        else if($_GET['l'] == "EmployeeProfileme")
            include("Empleados/DatosInformacion/PerfilEmpleado.php"); 
            
            
        /** Areas de FUSALMO **/
        else if($_GET['l'] == "Areas")
            include("Empleados/Administracion/Areas.php");

        else if($_GET['l'] == "AreasList")
            include("Empleados/Administracion/AreasList.php");                              
        
        /** Cargos de Areas **/
        else if($_GET['l'] == "Position")
            include("Empleados/Administracion/CargosList.php");
        
        else if($_GET['l'] == "PositionForm")
            include("Empleados/Administracion/CargosForm.php"); 
            
        else if($_GET['l'] == "Asignation")
            include("Empleados/Administracion/CargosAsignacion.php");

        else if($_GET['l'] == "AsignationDetails")
            include("Empleados/Administracion/CargosAsignacionDetalle.php");

        else if($_GET['l'] == "DetailsActions")
            include("Empleados/Administracion/CargosAsignacionAccion.php");    
            
        else if($_GET['l'] == "DetailsUpdate")
            include("Empleados/Administracion/CargosAsignacionUpdate.php");  

        else if($_GET['l'] == "AsignationFinalize")
            include("Empleados/Administracion/CargosFinalizacion.php");
            
        else if($_GET['l'] == "Hierarchy")
            include("Empleados/Administracion/CargosJerarquia.php");                                                                                   
               
        /** Areas de FUSALMO **/
        else if($_GET['l'] == "Picture")
            include("Empleados/Administracion/FotoEmpleado.php"); 
            
        else if($_GET['l'] == "Info")
            include("Empleados/DatosInformacion/DatosInstitucionalesForm.php");
                        
        /** Procesos de Area **/
        else if($_GET['l'] == "MyArea")
            include("Empleados/ProcesosArea/MiArea.php");
            
        /**  1- Acciones de Personal **/
        else if($_GET['l'] == "PersonalAction")
            include("Empleados/ProcesosArea/AccionPersonal.php");
            
        else if($_GET['l'] == "PersonalActionForm")
            include("Empleados/ProcesosArea/AccionPersonalForm.php");    
            
        else if($_GET['l'] == "Ticket")
            include("Empleados/ProcesosArea/AccionPersonalBoleta.php"); 
            
        else if($_GET['l'] == "Doings")
            include("Empleados/ProcesosArea/AccionPersonalAdministration.php");    
            
            
        /**  2- Permisos **/                                                                                      
        else if($_GET['l'] == "Permissions")
            include("Empleados/ProcesosArea/PermisoList.php");
            
        else if($_GET['l'] == "PermissionsForm")
            include("Empleados/ProcesosArea/PermisoForm.php");  
            
        else if($_GET['l'] == "Schedule")
            include("Empleados/ProcesosArea/PermisoFormSpecial.php");
            
        else if($_GET['l'] == "PermissionsoDetails")
            include("Empleados/ProcesosArea/PermisoDetails.php");            
        
        
        /** Gestión de Permisos **/    
        else if($_GET['l'] == "PermissionsControl")
            include("Empleados/ProcesosArea/PermisoControl.php");
            
        else if($_GET['l'] == "PermissionsControlView")
            include("Empleados/ProcesosArea/PermisoControlView.php");  
            
        else if($_GET['l'] == "PermissionsEmployee")
            include("Empleados/ProcesosArea/PermisoEmployee.php");
            
        else if($_GET['l'] == "PermissionsEmployeeView")
            include("Empleados/ProcesosArea/PermisoEmployeeView.php");                                    
        
        else if($_GET['l'] == "PermissionsPenalty")
            include("Empleados/ProcesosArea/PermisoPenalty.php");                                    
                        
              
        /**  3- Evaluacion de Desempeño **/                                                                                      
        else if($_GET['l'] == "Evaluation")
            include("Empleados/Evaluacion/Evaluation.php");
            
        else if($_GET['l'] == "EvaluationView")
            include("Empleados/Evaluacion/EvaluationView.php");
                
        else if($_GET['l'] == "EvaluationExecute")
            include("Empleados/Evaluacion/EvaluationExecute.php");
            
        else if($_GET['l'] == "Result")
            include("Empleados/Evaluacion/EvaluationResult.php");            
                              
        else if($_GET['l'] == "AllEvaluations")
            include("Empleados/Evaluacion/EvaluationGlobalResult.php");            
                              
        else if($_GET['l'] == "MyEvaluations")
            include("Empleados/Evaluacion/EvaluationMyResult.php");   

        else if($_GET['l'] == "ResultArea")
            include("Empleados/Evaluacion/EvaluationAreaResult.php");     
            
        else if($_GET['l'] == "Comparation")
            include("Empleados/Evaluacion/EvaluationComparation.php");
            
        else if($_GET['l'] == "ManagementEvaluation")
            include("Empleados/Evaluacion/GestionEvaluacion.php");   
            
        else if($_GET['l'] == "ManagementAllocation")
            include("Empleados/Evaluacion/GestionEvaluacionAsignacion.php");
            
        else if($_GET['l'] == "AllocationEmployee")
            include("Empleados/Evaluacion/GestionEvaluacionAsignar.php");
            
        else if($_GET['l'] == "ResultStaff")
            include("Empleados/Evaluacion/ResultadoPersonal.php");     
                                     
        else if($_GET['l'] == "Institution")
            include("Empleados/Evaluacion/EvaluationInstitution.php");
        else if($_GET['l'] == "Institution2014")
            include("Empleados/Evaluacion/EvaluationInstitution2014.php");
        else if($_GET['l'] == "Institution2015")
            include("Empleados/Evaluacion/EvaluationInstitution2015.php");
        else if($_GET['l'] == "Institution2016")
            include("Empleados/Evaluacion/EvaluationInstitution2016.php");
        else if($_GET['l'] == "Cuestionario")
            include("Empleados/Evaluacion/CuestionarioRRHH.php");
        else if($_GET['l'] == "ResultadosCuest")
            include("Empleados/Evaluacion/EvaluacionGlobalRRHH.php");
        else if($_GET['l'] == "ResultCuest")
            include("Empleados/Evaluacion/ResultadoPersonal_RRHH.php");
        else if($_GET['l'] == "ResultAreaRRHH")

            include("Empleados/Evaluacion/EvaluationAreaRRHH.php");
        else if($_GET['l'] == "vote")
            include("Empleados/vote.php");

        /** año 2017 DNC **/
       
        else if($_GET['l'] == "DNC2017")
            include("Empleados/Evaluacion/EvaluacionGlobalRRHH2017.php");
        else if($_GET['l'] == "DNCRES2017")
            include("Empleados/Evaluacion/ResultadoPersonal_RRHH2017.php");

    /** año 2018 DNC **/
       
        else if($_GET['l'] == "DNC2018")
            include("Empleados/Evaluacion/EvaluacionGlobalRRHH2018.php");
        else if($_GET['l'] == "DNCRES2018")
            include("Empleados/Evaluacion/ResultadoPersonal_RRHH2018.php");
        
 /**  44- Evaluacion de clima laboral **/                                                                                      
        else if($_GET['l'] == "Evaluations")
            include("Empleados/Evaluacion_cuest/Evaluation.php");
            
        else if($_GET['l'] == "EvaluationViews")
            include("Empleados/Evaluacion_cuest/EvaluationView.php");
                
        else if($_GET['l'] == "EvaluationExecutes")
            include("Empleados/Evaluacion_cuest/EvaluationExecute.php");
            
        else if($_GET['l'] == "Results")
            include("Empleados/Evaluacion_cuest/EvaluationResult.php");            
                              
        else if($_GET['l'] == "AllEvaluationss")
            include("Empleados/Evaluacion_cuest/EvaluationGlobalResult.php");            
                              
        else if($_GET['l'] == "MyEvaluationss")
            include("Empleados/Evaluacion_cuest/EvaluationMyResult.php");   

        else if($_GET['l'] == "ResultAreas")
            include("Empleados/Evaluacion_cuest/EvaluationAreaResult.php");     
            
        else if($_GET['l'] == "Comparations")
            include("Empleados/Evaluacion_cuest/EvaluationComparation.php");
            
        else if($_GET['l'] == "ManagementEvaluations")
            include("Empleados/Evaluacion_cuest/GestionEvaluacion.php");   
            
        else if($_GET['l'] == "ManagementAllocations")
            include("Empleados/Evaluacion_cuest/GestionEvaluacionAsignacion.php");
            
        else if($_GET['l'] == "AllocationEmployees")
            include("Empleados/Evaluacion_cuest/GestionEvaluacionAsignar.php");
            
        else if($_GET['l'] == "ResultStaffs")
            include("Empleados/Evaluacion_cuest/ResultadoPersonal.php");     
                            
        else if($_GET['l'] == "Cuestionarios")
            include("Empleados/Evaluacion_cuest/CuestionarioRRHH.php");

        else if($_GET['l'] == "ResultadosCuests")
            include("Empleados/Evaluacion_cuest/EvaluacionGlobalRRHH.php");

        else if($_GET['l'] == "ResultCuests")
            include("Empleados/Evaluacion_cuest/ResultadoPersonal_RRHH.php");

        else if($_GET['l'] == "ResultAreaRRHHs")
            include("Empleados/Evaluacion_cuest/EvaluationAreaRRHH.php");



            
        /** Proyectos **/
        else if($_GET['l'] == "ProjectSummary")
            include("Empleados/ProjectSummary/ResumenList.php");
                        
        else if($_GET['l'] == "Summary")
            include("Empleados/ProjectSummary/Resumen.php");

        else if($_GET['l'] == "ProjectSummaryForm")
            include("Empleados/ProjectSummary/ResumenForm.php");
            
        else if($_GET['l'] == "Participants")
            include("Empleados/ProjectSummary/Participants.php");
            
        else if($_GET['l'] == "Comunity")
            include("Empleados/ProjectSummary/Comunity.php");    
            

        /** Sociolaboral **/
        else if($_GET['l'] == "sociolaboral")
            include("../sociolaboral/index.php");

        /** POAS **/
        
        else if($_GET['l'] == "ProcesosPOA")
            include("Empleados/POA/index.php"); 

        else if($_GET['l'] == "AgregarPOA")
            include("Empleados/POA/formulariosPOA.php"); 

        else if($_GET['l'] == "VerPOA")
            include("Empleados/POA/poas.php");

        else if($_GET['l'] == "actividades")
            include("Empleados/POA/actividades.php");

        else if($_GET['l'] == "actividad")
            include("Empleados/POA/actividades2.php");

        else if($_GET['l'] == "agregarB")
            include("Empleados/POA/agregarBeneficiarios.php");
        
        else if($_GET['l'] == "exportda")
            include("Empleados/POA/export_data.php");

        else if($_GET['l'] == "tables")
            include("Empleados/POA/tables.php");

        else if($_GET['l'] == "formularioProceso")
            include("Empleados/POA/formularioProceso.php");

        else if($_GET['l'] == "formularioActividad")
            include("Empleados/POA/formularioActividad.php");

         else if($_GET['l'] == "metodosVerificacion")
            include("Empleados/POA/metodosVerificacion.php");

        else if($_GET['l'] == "registroPOA")
            include("Empleados/POA/registroPOA.php");

        else if($_GET['l'] == "registroProceso")
            include("Empleados/POA/registroProceso.php");

         else if($_GET['l'] == "registroActividad")
            include("Empleados/POA/registroActividad.php");

        else if($_GET['l'] == "importar")
            include("Empleados/POA/importar.php");

          else if($_GET['l'] == "registroMetodo")
            include("Empleados/POA/registroMetodo.php");

        else if($_GET['l'] == "formularioEventos")
            include("Empleados/POA/formularioEventos.php");

        else if($_GET['l'] == "pruebafotos")
            include("Empleados/POA/pruebafotos.php");

         else if($_GET['l'] == "gale")
            include("Empleados/POA/gale.php");

         else if($_GET['l'] == "formularioCapacitaciones")
            include("Empleados/POA/formularioCapacitaciones.php");

         else if($_GET['l'] == "subidaArchivos")
            include("Empleados/POA/subidaArchivos.php");

         else if($_GET['l'] == "formularioDocumentos")
            include("Empleados/POA/formularioDocumentos.php");

          else if($_GET['l'] == "formularioFinanciero")
            include("Empleados/POA/formularioFinanciero.php");

         else if($_GET['l'] == "modificarProceso")
            include("Empleados/POA/modificarProceso.php");

        else if($_GET['l'] == "updateProceso")
            include("Empleados/POA/updateProceso.php");

        else if($_GET['l'] == "eliminarProcesos")
            include("Empleados/POA/eliminarProcesos.php");

        else if($_GET['l'] == "modificarActividad")
            include("Empleados/POA/modificarActividad.php");

         else if($_GET['l'] == "updateActividades")
            include("Empleados/POA/updateActividades.php");

        else if($_GET['l'] == "eliminarActividad")
            include("Empleados/POA/eliminarActividades.php");

        else if($_GET['l'] == "calendario")
            include("Empleados/POA/calendarioWeb.php");

        else if($_GET['l'] == "calendarioProcesos")
            include("Empleados/POA/calendarioProcesos.php");

        else if($_GET['l'] == "stats")
            include("POA/estats/estadisticas.php");

        else if($_GET['l'] == "stats/proceso")
            include("POA/estats/poa.php");
       
       else if($_GET['l'] == "Planestrategico")
            include("Empleados/POA/PlanQuinquenal.php");


       else if($_GET['l'] == "Linea1")
            include("Empleados/POA/integralliberadora.php");

        else if($_GET['l'] == "Linea2")
            include("Empleados/POA/socio.php");

        else if($_GET['l'] == "Linea3")
            include("Empleados/POA/protagonismo.php");

        else if($_GET['l'] == "Linea4")
            include("Empleados/POA/tecno.php");

        else if($_GET['l'] == "Linea5")
            include("Empleados/POA/fortalecimiento.php");


        /** Donaciones **/
        else if($_GET['l'] == "Donations")
            include("Empleados/Payment/Searching.php"); 

        /** Requisiciones **/
        else if($_GET['l'] == "Request")
            include("Empleados/Requisiciones/MyRequest.php");

        else if($_GET['l'] == "RequestForm")
            include("Empleados/Requisiciones/RequestForm.php");

        else if($_GET['l'] == "ProyectForm")
            include("Empleados/Requisiciones/proyectosForm.php");            

        else if($_GET['l'] == "proyectos")
            include("Empleados/Requisiciones/proyectos.php"); 
            
        else if($_GET['l'] == "BudgetLine")
            include("Empleados/Requisiciones/proyectosLine.php"); 

        else if($_GET['l'] == "Budgetedit")
            include("Empleados/Requisiciones/proyectosLine-edit.php"); 

           else if($_GET['l'] == "editBug")
            include("Empleados/Requisiciones/proyectosLine2.php"); 

        else if($_GET['l'] == "ProyectArea")
            include("Empleados/Requisiciones/proyectosArea.php");                         
            
        else if($_GET['l'] == "report")
            include("Empleados/Requisiciones/report.php");                                                      
            
        else if($_GET['l'] == "requiemvista")
            include("Empleados/Requisiciones/requiemvista.php");   

        else if($_GET['l'] == "requiemaprove")
            include("Empleados/Requisiciones/requiemvistaaprobados.php");  

        else if($_GET['l'] == "requiemaprove2")
            include("Empleados/Requisiciones/requiemvistaaprobadosok.php");  

         else if($_GET['l'] == "requientregados")
            include("Empleados/Requisiciones/requiemvistaentregados.php"); 

         else if($_GET['l'] == "Searching")
            include("Empleados/Requisiciones/Busqueda.php"); 

         else if($_GET['l'] == "Statsproy")
            include("Empleados/Requisiciones/Requisiciones_graf/estadisticas_proyectos.php"); 


        else if($_GET['l'] == "Statsarea")
            include("Empleados/Requisiciones/Requisiciones_graf/requisicionesarea.php"); 

        else if($_GET['l'] == "Stats")
            include("Empleados/Requisiciones/Requisiciones_graf/estadistica.php"); 



         else if($_GET['l'] == "requiaceptados")
            include("Empleados/Requisiciones/requiemvistaaceptados.php"); 

         else if($_GET['l'] == "requienproceso")
            include("Empleados/Requisiciones/requiemvistaenproceso.php"); 

        else if($_GET['l'] == "requisiciones")
            include("Empleados/Requisiciones/requisiciones.php");     
     
        else if($_GET['l'] == "report_print")
            include("Empleados/Requisiciones/RequisicionPrint.php");

        else if($_GET['l'] == "map")
            include("Empleados/Requisiciones/mapa.php");

        else if($_GET['l'] == "cuadro")
            include("Empleados/Requisiciones/cuadro.php");
            

          /** Comunicaciones requis **/
        else if($_GET['l'] == "Requests")
            include("Empleados/Requiscomunicacion/MyRequest.php");

        else if($_GET['l'] == "RequestForms")
            include("Empleados/Requiscomunicacion/RequestForm.php");

        else if($_GET['l'] == "ProyectForms")
            include("Empleados/Requiscomunicacion/proyectosForm.php");            

        else if($_GET['l'] == "proyectoss")
            include("Empleados/Requiscomunicacion/proyectos.php"); 
            
        else if($_GET['l'] == "BudgetLines")
            include("Empleados/Requiscomunicacion/proyectosLine.php"); 

        else if($_GET['l'] == "Budgetedits")
            include("Empleados/Requiscomunicacion/proyectosLine-edit.php"); 

           else if($_GET['l'] == "editBugs")
            include("Empleados/Requisiciones/proyectosLine2.php"); 

        else if($_GET['l'] == "ProyectAreas")
            include("Empleados/RRequiscomunicacion/proyectosArea.php");                         
            
        else if($_GET['l'] == "reports")
            include("Empleados/RRequiscomunicacion/report.php");                                                      
            
        else if($_GET['l'] == "requiemvistass")
            include("Empleados/Requiscomunicacion/requiemvista.php");   

        else if($_GET['l'] == "requiemaproves")
            include("Empleados/Requiscomunicacion/requiemvistaaprobados.php");  

        else if($_GET['l'] == "requiemaprove2s")
            include("Empleados/Requiscomunicacion/requiemvistaaprobadosok.php");  

         else if($_GET['l'] == "requientregadoss")
            include("Empleados/Requiscomunicacion/requiemvistaentregados.php"); 

         else if($_GET['l'] == "Searchings")
            include("Empleados/Requiscomunicacion/Busqueda.php"); 

         else if($_GET['l'] == "Statsproys")
            include("Empleados/Requiscomunicacion/Requisiciones_graf/estadisticas_proyectos.php"); 


        else if($_GET['l'] == "Statsareas")
            include("Empleados/Requiscomunicacion/Requisiciones_graf/requisicionesarea.php"); 

        else if($_GET['l'] == "Statss")
            include("Empleados/Requiscomunicacion/Requisiciones_graf/estadistica.php"); 



         else if($_GET['l'] == "requiaceptadoss")
            include("Empleados/Requiscomunicacion/requiemvistaaceptados.php"); 

         else if($_GET['l'] == "requienprocesos")
            include("Empleados/Requiscomunicacion/requiemvistaenproceso.php"); 

        else if($_GET['l'] == "requisicioness")
            include("Empleados/Requiscomunicacion/requisiciones.php");     
     
        else if($_GET['l'] == "report_prints")
            include("Empleados/Requiscomunicacion/RequisicionPrint.php");

        else if($_GET['l'] == "maps")
            include("Empleados/Requiscomunicacion/mapa.php");

        else if($_GET['l'] == "cuadros")
            include("Empleados/Requiscomunicacion/cuadro.php");
            


            /** Inventario **/
        else if($_GET['l'] == "Inventario-fusalmo")
            include("Empleados/Inventario-fusalmo/inicio.php"); 

        else if($_GET['l'] == "pruebas")
            include("Empleados/pruebas.php"); 
       

         // Inventario
        else if($_GET['l'] == "Busqueda-inventario")
           include("Empleados/Inventario-fusalmo/Busqueda_inventario.php"); 
        else if($_GET['l'] == "Seleccion-inventario")
           include("Empleados/Inventario-fusalmo/seleccion_busqueda.php"); 
        else if($_GET['l'] == "Ingreso")
            include("Empleados/Inventario-fusalmo/Inventario_Ingreso.php"); 
        else if($_GET['l'] == "SolicitudAF")
            include("Empleados/Inventario-fusalmo/Solicitud_Inventario.php");
        else if($_GET['l'] == "InventarioAF")
            include("Empleados/Inventario-fusalmo/Inventario_Ingreso_Activo.php");
        else if($_GET['l'] == "InventarioOficina")
            include("Empleados/Inventario-fusalmo/Inventario_Ingreso_Oficina.php");
        else if($_GET['l'] == "form2")
            include("Empleados/Inventario-fusalmo/cont_solicitud.php");
        else if($_GET['l'] == "form3")
            include("Empleados/Inventario-fusalmo/cont_solicitud_2.php");
        else if($_GET['l'] == "actas")
            include("Empleados/Inventario-fusalmo/Actas.php");
         else if($_GET['l'] == "menuactivofijo")
            include("Empleados/Inventario-fusalmo/Nuevo.php");
        else if($_GET['l'] == "verseguimiento")
            include("Empleados/Inventario-fusalmo/Ver_seguimiento.php");
        else if($_GET['l'] == "seguimientosolicitud")
            include("Empleados/Inventario-fusalmo/Busqueda_Solicitud.php");
        else if($_GET['l'] == "updates")
            include("Empleados/Inventario-fusalmo/UpdateS.php");
        else if($_GET['l'] == "modify")
            include("Empleados/Inventario-fusalmo/ajuste.php");
        else if($_GET['l'] == "Actualizar")
            include("Empleados/Inventario-fusalmo/Vista_Actualizar.php");
        else if($_GET['l'] == "controller")
            include("Empleados/Inventario-fusalmo/inventario_controller.php");
        else if($_GET['l'] == "control")
            include("Empleados/Inventario-fusalmo/controller_ingreso.php");
        else if($_GET['l'] == "control2")
            include("Empleados/Inventario-fusalmo/controller_ingreso2.php");

        else if($_GET['l'] == "Reports")
            include("Empleados/Inventario-fusalmo/Inventario_Reportes.php");
        else if($_GET['l'] == "activo")
            include("Empleados/Inventario-fusalmo/Reportes_Activo.php");
        else if($_GET['l'] == "control2")
            include("Empleados/Inventario-fusalmo/controller_ingreso2.php");
        else if($_GET['l'] == "control2")
            include("Empleados/Inventario-fusalmo/controller_ingreso2.php");
        else if($_GET['l'] == "control2")
            include("Empleados/Inventario-fusalmo/controller_ingreso2.php");



        // Calendario de Actividades
        else if($_GET['l'] == "Calendar")
            include("Empleados/Calendario/Calendario.php");   

        else if($_GET['l'] == "ActivityForm")
            include("Empleados/Calendario/ActivityForm.php");
            
        else if($_GET['l'] == "Activity")
            include("Empleados/Calendario/Activity.php"); 


            //cuestionario
             else if($_GET['l'] == "Cuest")
            include("Empleados/Cuestionario/CuestionarioRRHH.php");                                   
                                                                                                
    }
    else
    {
        echo "<div style='float:left; padding-left: 52px;'>";
        include ('Menu/meen2.php');
        echo "</div>";
        
        include("Empleados/DatosInstitucionales.php");
    } 
        
    
    echo "<div class='clr'></div>";
?>