<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">   
<form action='Empleados/Administracion/CargosABM.php' method='post'>
<?php

   if($_SESSION["IdAC"] == "")
        Redireccion("../../Empleado.php?l=Position&n=4");
    

    $DatosEmpleado = $bddr->prepare("SELECT CA . * , C.Cargo, E.Nombre1, E.Nombre2, E.Nombre3, E.Apellido1, E.Apellido2
                                     FROM CargosAsignacion AS CA
                                     INNER JOIN Cargos AS C ON CA.IdCargo = C.IdCargos
                                     INNER JOIN Empleado AS E ON CA.IdEmpleado = E.IdEmpleado
                                     where CA.IdAsignacion = '" . $_SESSION["IdAC"]  ."'");
    $DatosEmpleado->execute();
    $Empleado = $DatosEmpleado->fetch();
     
?>    
    <table style="width: 100%;">        
    <tr><th colspan="2"><h2>Asignación de Cargo</h2><hr color='#6BAED9'/></th></tr>
    <input type="hidden" name="IdAC" value="<?php echo $_SESSION["IdAC"]; ?>" />
    <input type="hidden" name="IdC" value="<?php echo $Empleado[1]; ?>" />
    
    <tr>
        <td class="tdleft" style="width: 50%;">Nombre del Cargo:</td>
        <td><input name="Cargo" type="text" value="<?php echo $Empleado[8]; ?>" required='true' /></td>
    </tr>
    
          <tr>
              <td class="tdleft">Tipo de Contrato:</td>
              <td>
                   <select name="TipoContrato">
                    <?php 
                            $array = array(
                                           "Planilla" => "Planilla", 
                                           "Servicio Profesional" => "Servicio Profesional");
                                        
                               
                               foreach ($array as $i => $value) 
                               {
                                    if($i == $Empleado[3])
                                        echo "<option Selected='true' value='$i'>$i</option>";
                                        
                                    else
                                        echo "<option value='$i'>$i</option>";
                               }                                                                                
                    ?>                                                         
                   </select>
              </td>
          </tr>
          <tr>
              <td class="tdleft">Fuente de Financiamiento:</td>
              <td>
                  <select name="Financiamiento">
                  <?php 
                            $array = array(
                                           "Operaciones" => "Operaciones", 
                                           "Proyecto" => "Proyecto");
                                        
                               
                               foreach ($array as $i => $value) 
                               {
                                    if($i == $Empleado[4])
                                        echo "<option Selected='true' value='$i'>$i</option>";
                                        
                                    else
                                        echo "<option value='$i'>$i</option>";
                               }                                                                                
                    ?>                                                         
                   </select>
              </td>
          </tr>     
          
          <tr>
              <td class="tdleft">Salario:</td>
              <td><input name="Salario" type="text" value="<?php echo $Empleado[5]; ?>" required='true' alt="DD-MM-YYYY" /></td>
          </tr>     
          
          <tr>
              <td class="tdleft">Fecha de Inicio:</td>
              <td><input name="FechaIni" type="date" value="<?php echo $Empleado[6]; ?>"/></td>
          </tr>                                                   
          <tr>
              <th colspan="2"><input type="submit" name="Enviar" value="Modificar" title="Guarda nuevos datos y actualiza los anteriores."  class="boton"/></th>
          </tr>

    </table>
    </form>    
</div>    