<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">    
<?php
    
    if(isset($_POST['IdPosition']) || $_SESSION["IdC"] != ""  || $_SESSION["IdPosition"] != "" )
    {
        if(isset($_POST['IdPosition']))
            $IdCargo = $_POST['IdPosition'];
            
        elseif ($_SESSION["IdC"] != "")
            $IdCargo = $_SESSION["IdC"];
        
        else
            $IdCargo = $_SESSION["IdPosition"];
    }
        
    else
        Redireccion("../../Empleado.php?l=Position&n=4");

    $DatosEmpleado = $bddr->prepare("SELECT C.IdCargos, A.IdAreaDeTrabajo, C.Cargo, A.NombreAreaDeTrabajo
                                     FROM Cargos AS C
                                     INNER JOIN AreasDeTrabajo AS A ON A.IdAreaDeTrabajo = C.IdArea_Fk
                                     where C.IdCargos = $IdCargo" );
    $DatosEmpleado->execute();
    $Empleado = $DatosEmpleado->fetch();


    $DatosJefatura = $bddr->prepare("SELECT * FROM Jerarquia AS J
                                     INNER JOIN Cargos AS C ON C.IdCargos = J.IdCargoSuperior
                                     INNER JOIN CargosAsignacion as ca on ca.IdCargo = C.IdCargos
                                     WHERE J.IdCargos_Fk = $IdCargo and FechaFin = '0000-00-00'" );
    $DatosJefatura->execute();

    
    
    echo "<table style='width: 100%'>
            <tr>                
                <td><a href='Empleado.php?l=Position' style='text-decoration: none; color: blue;'><- Cargos de Area</a></td>
                <td style='text-align: right;'><h2>Detalles del Cargo</h2></td>
            </tr>
            <tr><td colspan='2'><hr color='#6CAFDA' /></td></tr>
            <tr><td class='tdleft'>Cargo:</td><td>$Empleado[2]</td></tr>
            <tr><td class='tdleft'>Area:</td><td>$Empleado[3] </td></tr>
            <tr><td style='text-align: center;' colspan='2'>
                    <table style='width: 60%; margin-left: 20%; text-align: center;'>
                        <tr>
                            <td>
                                <form action='Empleado.php?l=Asignation' method='Post' >
                                    <input type='hidden' name='IdPosition' Value='$Empleado[0]' />
                                    <input type='submit' style='width: 120px' name='Enviar' Value='Nuevo Empleado' class='boton' />
                                </form>
                            </td>
                            <td>
                                <form action='Empleado.php?l=Hierarchy' method='Post' >
                                    <input type='hidden' name='IdPosition' Value='$Empleado[0]' />
                                    <input type='submit' style='width: 120px' name='Enviar' Value='Asignar Jefe' class='boton' />
                                </form>                            
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <form action='Empleados/Administracion/CargosABM.php' method='Post' >
                                    <input type='hidden' name='IdPosition' Value='$Empleado[0]' />
                                    <input type='submit' style='width: 120px' 
                                    onclick=\"return confirm('Si elimina este registro borrara toda la información que este relacionado con el, esta seguro?')\" 
                                    name='Enviar' Value='Eliminar Cargo' class='botonE' />
                                </form>
                            </td>
                            <td>
                                <form action='Empleados/Administracion/CargosABM.php' method='Post' >
                                    <input type='hidden' name='IdPosition' Value='$Empleado[0]' />
                                    <input type='submit' style='width: 120px'  name='Enviar' Value='Actualizar Cargo' class='boton' />
                                </form>                            
                            </td>
                        </tr>

                    
                    </table>                    
                </td>
            </tr>";
    
    
    echo "  <tr><td colspan='2'><hr color='#6CAFDA' /></td></tr>
            <tr><td colspan='2'>Jefes Inmediatosaaa:</td></tr>
            <tr><td colspan='2'>
            <table style='width: 90%; margin-left:5%;'>";
    
    $i = 1;
    while($Jefes = $DatosJefatura->fetch())
    {
            echo "<tr><td>$i</td><td style='color: blue; padding-left: 20px' >$Jefes[4]</td>";
            
            $DatosNombre = $bddr->prepare("SELECT E . *  FROM CargosAsignacion AS CA
                                             INNER JOIN Empleado AS E ON E.IdEmpleado = CA.IdEmpleado
                                             WHERE CA.IdCargo = $Jefes[2] and FechaFin = '0000-00-00'");
            $DatosNombre->execute();
            $Nombre = $DatosNombre->fetch();
            
            echo "<td style='color: blue; padding-left: 20px'>  $Nombre[1] $Nombre[2] $Nombre[4] $Nombre[5]</td>
                  <td>
                      <form action='Empleados/Administracion/CargosABM.php' method='post'>
                            <input type='hidden' value='$Jefes[0]' name='IdJ'/>
                            <input type='submit' value='X' class='botonE' name='Enviar' style='width: 30px' onClick=\"return confirmar();\" />
                      </form>
                  </td>
                  </tr>";
            
            $i++;
    }
    
    if($DatosJefatura->rowCount() == 0)
        echo "<tr><td colspan='4' style='text-align: center; color: red'>Empleado si Jefe Asignado</td></tr>";
    
    echo " </table>
           </tr>";
                     
    echo "  <tr><td colspan='2'><hr color='#6CAFDA' /></td></tr>
            <tr><td colspan='2'>Personal a que ha sido asignado:</td></tr>
            <tr><td colspan='2'>
            <table style='width: 80%;'>";

    $DatosEmpleado = $bddr->prepare("SELECT * FROM CargosAsignacion AS CA
                                     INNER JOIN Empleado AS E ON E.IdEmpleado = CA.IdEmpleado
                                     WHERE CA.IdCargo =  $IdCargo  
                                     ORDER BY CA.FechaFin ASC ");
    $DatosEmpleado->execute();
    $i = 0;
    if($DatosEmpleado->rowCount() > 0)
    {            
        while($Empleado = $DatosEmpleado->fetch())
        {
            if($Empleado[7] == "0000-00-00")
            {
                $color = "blue";
                $FechaFin = "<em>Actualmente en el Puesto</em>";
                $Opciones = "<tr><td class='tdleft' style='color: $color;'>Opciones:</td>
                                 <td>
                                     <form action='Empleados/Administracion/CargosABM.php' method='POST' >
                                         <input type='hidden' name='IdAC'  value='$Empleado[0]' />
                                         <input type='hidden' name='IdPosition'  value='$Empleado[1]' />
                                         <input type='submit' name='Enviar' class='boton' value='Actualizar' style='width: 130px;' />
                                         <input type='submit' name='Enviar' class='botonE' value='Finalizar Cargo' style='width: 130px;' /><br />                                 
                                         <input type='submit' name='Enviar' class='boton' value='Cambiar Empleado' style='width: 130px;' />
                                         <input type='submit' name='Enviar' class='botonE' value='Eliminar' style='width: 130px;' onclick='return confirmar();' />
                                     </form>
                                 <td>
                             <tr>";
                $i++;
            }
            else
            {
                $color = "red";
                $FechaFin = "$Empleado[7]";
                $Opciones = "";
            }
    
       echo "     <tr style='height: 15px;'><td colspan='2' ></td></tr>
                  <tr><td class='tdleft' style='color: $color; width: 50%;'>Empleado: </td><td style='color: $color;'>$Empleado[9] $Empleado[10] $Empleado[12]</td></tr>
                  <tr><td class='tdleft' style='color: $color;'>Tipo de Contrato: </td><td style='color: $color;'>$Empleado[3]</td></tr>
                  <tr><td class='tdleft' style='color: $color;'>Financiamiento: </td><td style='color: $color;'>$Empleado[4]</td></tr>
                  <tr><td class='tdleft' style='color: $color;'>Salario: </td><td style='color: $color;'>$". number_format($Empleado[5],2)."</td></tr>
                  <tr><td class='tdleft' style='color: $color;'>Fecha de Inicio: </td><td style='color: $color;'>$Empleado[6]</td></tr>
                  <tr><td class='tdleft' style='color: $color;'>Fecha de Finalización: </td><td style='color: $color;'>$FechaFin</td></tr>
                  $Opciones";            
        }       
    }
    else
    {
        echo "<tr><td colspan='2' style='color: red;'>No hay Empleados que se les haya asignado este puesto.</td></tr>";
    }
          
    echo "</table>
          </td>
          </tr>";
          
            

    echo "</table>";
?>        
</div>

