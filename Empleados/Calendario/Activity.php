<?php

/**
 * @author Manuel Calder�n
 * @copyright 2013
 */

setlocale (LC_TIME, "es_SV");
function getMonthDays($Month, $Year)
{
   //Si la extensi�n que mencion� est� instalada, usamos esa.
   if( is_callable("cal_days_in_month"))
   {
      return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
   }
   else
   {
      //Lo hacemos a mi manera.
      return date("d",mktime(0,0,0,$Month+1,0,$Year));
   }
}

function IstheDay($DayIndex, $Date)
{
    if($DayIndex == date("N", strtotime($Date)))
        return true;
        
    else
        return false;
}


if($_POST['Enviar'] == "Ver Calendario")
{
    $IdEmpleado = $_POST['idi'];
    $_SESSION['ide'] = $IdEmpleado;
    $_SESSION['Ids'] = $IdEmpleado;
}    
else
{
    if($_SESSION['ide'] == "")
        $IdEmpleado = $_SESSION['IdUsuario'];
    
    else
    {
        $IdEmpleado = $_SESSION['ide'];
        $_SESSION['Ids'] = $IdEmpleado;
    }
        
} 

if(isset($_GET['date']))
{
    $fechas = explode("-", $_GET['date']);
    if($_GET['ref'] == "back")
    {
        if($fechas[1] == "01")
        {
            $mes = 12;
            $a�o = $fechas[0] - 1;
        }
        else
        {
            $mes = $fechas[1] - 1;
            $a�o = $fechas[0];    
        }
    }
    else if($_GET['ref'] == "forward")
    {
        if($fechas[1] == "12")
        {
            $mes = 1;
            $a�o = $fechas[0] + 1;
        }
        else
        {
            $mes = $fechas[1] + 1;
            $a�o = $fechas[0];    
        }       
    }
    else
    {
        $mes = $fechas[1];
        $a�o = $fechas[0];
    }
}
else
{
    $mes = date("m");
    $a�o = date("Y");
}

if(strlen($mes) == 1)
    $mes = "0" . $mes;

$day = "01";

$diasMes = getMonthDays($mes, $a�o);
$Date = "$a�o-$mes-$day";

$Ref = "$a�o-$mes";

if($mes == "01")$NMes = "Enero";
if($mes == "02")$NMes = "Febrero";
if($mes == "03")$NMes = "Marzo";
if($mes == "04")$NMes = "Abril";
if($mes == "05")$NMes = "Mayo";
if($mes == "06")$NMes = "Junio";
if($mes == "07")$NMes = "Julio";
if($mes == "08")$NMes = "Agosto";
if($mes == "09")$NMes = "Septiembre";
if($mes == "10")$NMes = "Octubre";
if($mes == "11")$NMes = "Noviembre";
if($mes == "12")$NMes = "Diciembre";


$query = "SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado, ca.IdAsignacion 
          FROM CargosAsignacion AS ca
          INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
          INNER JOIN Empleado AS e ON ca.IdEmpleado = e.IdEmpleado
          INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
          WHERE e.IdEmpleado = $IdEmpleado and ca.FechaFin = '0000-00-00'";

$Empleado = $bddr->prepare($query);                                            
$Empleado->execute();
$DatosE = $Empleado->fetch(); 


if(isset($_POST['Ver']))
{
    $_SESSION['Ids'] = "";
    $asis = isset($_POST['Share']) ? $_POST['Share'] : array();        
    for ($index = 0 ; $index < count($asis); $index ++) 
    {   
        $_SESSION['Ids'] .= $asis[$index];
        
        if($index < (count($asis) - 1))
            $_SESSION['Ids'] .= "-";       
    }    
}
else
{
    if($_SESSION['Ids'] == "")
        $_SESSION['Ids'] = $IdEmpleado;
}


if($_POST['Enviar'] == "Buscar")
{

        $FechaInicio = $_POST['fechai'];
        $FechaFin =  $_POST['fechah'];
        $Title = strtoupper("Actividades entre $FechaInicio y $FechaFin");
        
        If($_POST['tipo'] == "All")
            $Activity = " ";
        else
            $Activity = " AND TipoActividad = '" . $_POST['tipo'] . "'";
            
        If($_POST['Transporte'] == "Todas")
            $Transport = " ";
        else
            $Transport = " AND Transporte = '" . $_POST['Transporte'] . "'";         
       
}
else if($_POST['Enviar'] == "Fecha")
{
    $FechaInicio = $_POST['date'];
    $FechaFin =  $_POST['date'];
    $Activity = " ";
    $Transport = " ";
    $Title = strtoupper("Actividades del mes de $NMes");    
}
else
{
    $FechaInicio = "$Date";
    $FechaFin = "$Ref-$diasMes";
    $Activity = " ";
    $Transport = " ";
    $Title = strtoupper("Actividades del mes de $NMes");
}

?>
<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px;">  
<table style="width: 100%;">
    <tr><td>
            <table style="width: 100%;">
                <tr>
                    <td><h2 style="color: #1D7399;">Mis Actividades</h2></td>
                    <td style="text-align: right;"><a href="?l=Calendar"><input type="button" value="<- Atras" class="boton" /></a></td>
                </tr>
            </table>
        </td></tr>
    <tr><td><hr color='0099FF' /></td></tr>
    <tr>
        <td>
            <table style="width: 80%;">
                <tr><td  style="width: 30%;">Nombre del Empleado: </td><td><?php echo "$DatosE[0] $DatosE[1] $DatosE[2] $DatosE[3]"?></td></tr>
                <tr><td >Cargo Asignado:                     </td><td><?php echo "$DatosE[5]"?></td></tr>
                <tr><td >Area de trabajo:                    </td><td><?php echo "$DatosE[6]"?></td></tr>
            </table>
        </td>
    </tr>   
    <tr><td><hr color='0099FF' /></td></tr>
    <tr>
        <td>        
            <form action="?l=Activity" method="POST">
            <table style="width: 100%;">
                <tr>
                    <td colspan="3"><h2 style="color: #1D7399;">Filtros</h2></td>
                    <td><h2 style="color: #1D7399;">Otros Calendarios</h2></td>
                </tr>
                <tr>
                    <td>Fecha Inicial:<br /><input type="date" name="fechai" value="<?php echo "$FechaInicio";?>" /></td>
                    <td>Fecha Final:<br /><input type="date" name="fechah" value="<?php echo "$FechaFin";?>" /></td>
                    <td rowspan="3">
                        <input type="submit" name="Enviar" value="Buscar" class="boton" /><br /><br />
                        <input type="submit" name="Enviar" value="Ver Todo" class="boton" />
                    </td>
                    <td>
                        <a href="#Share"><input type="button" name="Enviar" value="Ver" class="boton" /></a>
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                    <td>Tipo de Actividad:</td>
                    <td>
                        <select name="tipo">
                            <option value='All'>Todas</option>
                            <?php
	                              $Actividades = $bddr->prepare("Select TipoActividad from Actividad_Usaid group by TipoActividad");
                                  $Actividades->execute();
                                  
                                  while($DataA = $Actividades->fetch())
                                  {
                                        if($_POST['tipo'] == $DataA[0])
                                            echo "<option value='$DataA[0]' selected='true' >$DataA[0]</option>";
                                        
                                        else
                                            echo "<option value='$DataA[0]' >$DataA[0]</option>";
                                  }
                            ?>                            
                        </select>
                    </td>
                </tr>
                <tr><td><br /></td></tr>   
                <tr>
                    <td>Solicitudes de Transporte:</td>
                    <td>
                        <select name="Transporte">
                            <option value='Todas' >Todas</option>                            
                            <?php
	                              $Actividades = $bddr->prepare("Select Transporte from Actividad_Usaid group by Transporte");
                                  $Actividades->execute();
                                  echo $Actividades->rowCount();
                                  while($DataA = $Actividades->fetch())
                                  {
                                        if($_POST['Transporte'] == $DataA[0])
                                            echo "<option value='$DataA[0]' selected='true' >$DataA[0]</option>";
                                        
                                        else
                                            echo "<option value='$DataA[0]' >$DataA[0]</option>";                                    
                                  }
                            ?>                                                      
                        </select>
                    </td>
                </tr>                               
            </table>
            </form>
        </td>
    </tr>
    <tr><td><hr color='0099FF' /></td></tr>
    <tr>
        <td colspan="2">
            <table style="width: 100%;">
            <?php echo "<tr><td style='width: 15%;'>
                                <a href='?l=Activity&date=$Ref&ref=back'><input type='button' value='<-' class='botonG' /></a>
                            </td>
                            <th colspan='5'>                                
                                <table style='width: 100%'>
                                <tr>
                                    <td>$Title</td>
                                    <td>
                                        <form action='Empleados/Calendario/ActivityExport.php' method='post'>
                                        <input type='submit' name='Enviar' value='Exportar' class='boton' />
                                        </form>
                                    </td>
                                </tr>
                                </table>
                            </th>
                            <td style='width: 15%;'>
                                <a href='?l=Activity&date=$Ref&ref=forward'><input type='button' value='->' class='botonG' /></a>
                            </td>
                        </tr>"; 
            ?>
            </table>
                        
            <div style="width: 900px; overflow-x: scroll !important; padding: 5px;">
            <table style="width: 2000px;" rules='all'>
                <tr style='height: 50px;'>
                    <th style="width: 50px;"></th>
                    <th style="width: 180px;">Empleado</th>
                    <th style="width: 100px;">Fecha</th>
                    <th style="width: 225px;">Tipo de Actividad</th>
                    <th style="width: 150px;">Departamento</th>
                    <th style="width: 150px;">Municipio</th>
                    <th style="width: 225px;">Lugar</th>
                    <th style="width: 50px;">Cohorte</th>
                    <th style="width: 100px;">Hora Inicial</th>
                    <th style="width: 100px;">Hora Final</th>
                    <th style="width: 200px;">Comentarios</th>
                    <th style="width: 100px;">Estado</th>
                    <th style="width: 75px;">Transporte</th>                    
                </tr>
            
            <?php
                            
                $pieces = explode("-",$_SESSION['Ids']);                 
                $Invitados = " ";
                
                for($i = 0 ; $i <= (count($pieces)-1); $i++)
                {
                    if($i == 0)
                        $Invitados .= "(";
                    
                    if($pieces[$i] != "")
                        $Invitados .= " IdUsuaio = $pieces[$i]";                    
                    
                    if($i == (count($pieces) - 1))
                        $Invitados .= ")";
                        
                    else
                    {
                            $Invitados .= " OR ";
                    }                   
                }                   
                    
                   $Query = "Select * from Actividad_Usaid 
                             where                              
                             (Fecha between '$FechaInicio' and '$FechaFin') AND 
                             $Invitados 
                             $Activity 
                             $Transport 
                             Order by Fecha";    
                   //echo "$Query";  
                   $_SESSION['Query'] = $Query;                     
                   $AllActividades = $bddr->prepare($Query);
                   $AllActividades->execute();

                   if($AllActividades->rowCount() > 0)
                   {
                        while($DataAA = $AllActividades->fetch())
                       {
                           $Departamentos = $bddC->prepare("SELECT Departamento FROM USAID_Departamentos where IdDepartamento = $DataAA[3]");
                           $Departamentos->execute();        
                           $DataD = $Departamentos->fetch();
                          
                           $Empleado = $bddr->prepare("SELECT Nombre1, Nombre2, Nombre3, Apellido1, Apellido2 FROM Empleado where IdEmpleado = $DataAA[12]");
                           $Empleado->execute();        
                           $DataE = $Empleado->fetch();                     
                          
                           echo "<tr style='height: 35px;'>
                                    <td style='text-align: center;'>";
                           
                           if($IdEmpleado == $_SESSION['IdUsuario'] || $_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["TipoUsuario"] == "RRHH" )
                           echo "       <form action='?l=ActivityForm&Form=Object1' method='post'>
                                            <input type='hidden' name='ida' value='$DataAA[0]' />
                                            <input type='hidden' name='date' value='$DataAA[1]' />
                                            <input type='submit' name='Enviar' value='Cambiar' class='boton' style='width: 70px' />
                                        </form>";
                                        
                                        
                           echo "</td>
                                    <td style='text-align: center;'>$DataE[0] $DataE[1] $DataE[2] <br />$DataE[3] $DataE[4]</td>
                                    <td style='text-align: center;'>$DataAA[1]</td>
                                    <td style='text-align: center;'>$DataAA[2]</td>
                                    <td style='text-align: center;'>$DataD[0]</td>
                                    <td style='text-align: center;'>$DataAA[4]</td>
                                    <td>$DataAA[5]</td>
                                    <td style='text-align: center;'>$DataAA[6]</td>
                                    <td style='text-align: center;'>$DataAA[7]</td>
                                    <td style='text-align: center;'>$DataAA[8]</td>
                                    <td>$DataAA[9]</td>
                                    <td style='text-align: center;'>$DataAA[10]</td>
                                    <td style='text-align: center;'>$DataAA[11]</td>
                                </tr>";
                       }
                   }
                   else
                   {
                        echo "<tr>
                                <td colspan='5' style='text-align: center;'><em style='color: red'>No hay actividades registradas</em></td>
                                <td colspan='6'></td>
                              </tr>";
                   }
            ?>
            </table>
            <br /><br />
            </div>
        </td>
    </tr>     
    <tr><td><hr color='0099FF' /></td></tr>    
    <tr>
        <td>       
        <a name="Share" id="Share"></a>
            
            <table style="width: 90%;">
                <tr><td colspan="2"><h2 style="color: #1D7399;">Calendarios Compartidos</h2></td></tr>
                <?php
                
                $Query = "SELECT e.IdEmpleado, e.Nombre1, e.Nombre2, e.Nombre3, e.Apellido1, e.Apellido2, t.NombreAreaDeTrabajo, c.Cargo 
                          FROM Empleado as e 
                          INNER JOIN CargosAsignacion as ca on e.IdEmpleado = ca.IdEmpleado 
                          INNER JOIN Cargos as c on ca.IdCargo = c.IdCargos 
                          INNER JOIN AreasDeTrabajo as t on c.IdArea_Fk = t.IdAreaDeTrabajo 
                          INNER JOIN Actividad_Compartir as co on e.IdEmpleado = co.IdEmpleado 
                          WHERE co.IdInvitado = $IdEmpleado
                          ORDER BY t.IdAreaDeTrabajo ";
            
                //echo "$Query";                
                $Empleados = $bddr->prepare($Query);                
                $Empleados->execute();                            
                
                echo "<form id='FormShare' name='FormShare' action='#' method='post' >
                      <table style='width: 100%; '>
                          <tr><th  style='width: 5%'>
                                <input type='button' value='Marcar' id='botoncheck' onclick='checkear();' class='boton' style='width: 75px;'>
                                </th><th  style='width: 35%'>Empleado</th><th  style='width: 30%'>Cargo</th><th style='width: 30%'>�rea</th></tr>";
                
                if($Empleados->rowCount() > 0)
                {
                    for ($index = 0 ; $index < count($asis); $index ++) 
                    {   
                        if($asis[$index] == $IdEmpleado)
                        {
                            $IsChecked = true;
                            break;
                        }
                        else
                            $IsChecked = false;
                    }          
                       
                    if(!$IsChecked)
                    {
                        if(!isset($_POST['Ver']))
                            $IsChecked = "checked='true'";  
                            
                        else
                            $IsChecked = "";     
                    }
                    else
                        $IsChecked = "checked='true'";   
                        
                    echo  "<tr style='height: 35px;'><th><input type='checkbox' name='Share[]' value='$IdEmpleado'  $IsChecked ></th>
                                   <td>$DatosE[0] $DatosE[1] $DatosE[2] $DatosE[3]<br />(Mi Calendario)</td>
                                   <td>$DatosE[5]</td>                                   
                                   <td>$DatosE[6]</td>
                               </tr>";
                    
                    $IsChecked = null;
                    
                    while($DataE = $Empleados->fetch())
                    {
                        echo  "<tr style='height: 35px;'><th>";
                        
                        for ($index = 0 ; $index < count($asis); $index ++) 
                        {   
                            if($asis[$index] == $DataE[0])
                            {
                                $IsChecked = true;
                                break;
                            }
                            else
                                $IsChecked = false;
                        }
                        
                        if($IsChecked)
                            echo "<input type='checkbox' name='Share[]' value='$DataE[0]' checked='true' >";
                        
                        else
                            echo "<input type='checkbox' name='Share[]' value='$DataE[0]' >";                        
                        
                        
                        echo "</th>
                                   <td>$DataE[1] $DataE[2] $DataE[3] $DataE[4]</td>
                                   <td>$DataE[7]</td>                                   
                                   <td>$DataE[6]</td>
                               </tr>";
                    }
                    echo  "<tr><td></td>
                               <td><br /><input type='submit' name='Ver' value='Ver Calendarios' class='botonG' ></td>
                               <td></td>                                   
                               <td></td>
                           </tr>";
                }
                else
                    echo "<tr><td colspan='4' style='text-align: center'><em style='color: red'>No se han compartido Actividades</em></td></tr>";
                    
                echo "</table>
                      </form>";
                ?>                
            </table>        
        <script>
        function checkear()
        {
            boton = document.getElementById('botoncheck');    
            if(boton.value == "Marcar")
            { 
                seleccionar_todo();
                boton.value = "Desmarcar"
            }
            else
            {
                deseleccionar_todo();
                boton.value = "Marcar"
            }
        }
        function seleccionar_todo()
        { 
           for (i = 0;i<document.FormShare.elements.length;i++) 
              if(document.FormShare.elements[i].type == "checkbox")	
                 document.FormShare.elements[i].checked = 1;  
        }
        function deseleccionar_todo()
        { 
           for (i = 0;i<document.FormShare.elements.length;i++) 
              if(document.FormShare.elements[i].type == "checkbox")	
                 document.FormShare.elements[i].checked = 0;  
        }        

        </script>             
        </td>
    </tr>
    <tr>
        <td>
            <br /><br />
            <?php if($_SESSION["TipoUsuario"]=="Administrador" || $_SESSION["TipoUsuario"]=="RRHH" || $_SESSION["IdUsuario"]== 4896){?>
                    <script>                    
                        function CargarEmpleados()
                        {    
                            var code = $("#Searching").val();
                            var code2 = <?php echo $IdEmpleado;?>;     	
                            document.getElementById('Fill').innerHTML = "<center><em style='color: green;'>Cargando datos...</em></center>";
                            
                            $.get("Empleados/Calendario/CompartirRRHH.php", { code: code, code2: code2 },
                        		function(resultado)
                        		{
                                    document.getElementById('Fill').innerHTML = "";
                        			if(resultado == false)   			
                        				alert("Error");
                        			
                        			else    				    
                        				$('#Fill').append(resultado);	
                        		}
                        	);
                        }
                    </script>                    
                    <table style="width: 100%;">
                        <tr><td><h2>Busqueda RRHH</h2></td></tr>
                        <tr>
                            <td style="width: 40%;">Escriba un nombre o un apellido para compartir:</td>
                            <td><input type="text" id="Searching" style="width: 100%;" onkeyup="CargarEmpleados()" /></td>
                            <td></td>                    
                        </tr>
                        <tr>
                            <td colspan="3"><div id="Fill"></div></td>
                        </tr>            
                    </table>  
                   <?php }?>    
        </td>
    </tr>
</table>
<br /><br />
</div>
