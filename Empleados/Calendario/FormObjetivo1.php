
<script>
function CargarListaMunicipios()
{
    var departamento = document.getElementById('Departamentos').value;    
    document.getElementById('Municipio').options.length = 0;
    
    var aux = true;
    
    $.get("USAID/SelectMunicipios.php", { departamento:departamento, aux:aux },
  		function(resultado)
  		{           
            document.getElementById('Municipio').options.length = 0;
                
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#Municipio').append(resultado);	            			
  		}
   	);        
}

function Departamentos()
{
<?php
   $Departamentos = $bddC->prepare("SELECT * FROM USAID_Departamentos");
   $Departamentos->execute();
   
   echo "var option = document.createElement('option');
         option.text = '...';
         option.value = '';
         document.getElementById('Departamentos').add(option);";
                 
   while($DataD = $Departamentos->fetch())
   {
        echo "var option = document.createElement('option');
              option.text = '$DataD[1]';
              option.value = '$DataD[0]';
              document.getElementById('Departamentos').add(option);";
   }
?>
}

function DepartamentosOficina()
{
<?php
   $Departamentos = $bddC->prepare("SELECT * FROM USAID_Departamentos");
   $Departamentos->execute();
   
   echo "var option = document.createElement('option');
         option.text = 'Soyapango';
         option.value = 'Soyapango';
         document.getElementById('Municipio').add(option);";
   
   echo "var option = document.createElement('option');
         option.text = 'San Salvador';
         option.value = 'San Salvador';
         document.getElementById('Departamentos').add(option);";
   
?>
}

function Cohorte()
{
<?php

   echo "var option = document.createElement('option');
         option.text = '...';
         option.value = '';
         document.getElementById('Cohorte').add(option);";
         
   $i = 1; 
   while($i <= 4 )
   {
        echo "var option = document.createElement('option');
              option.text = '$i';
              option.value = '$i';
              document.getElementById('Cohorte').add(option);";
        $i++;
   }
?>    
}

function Estado()
{
    var option = document.createElement('option');
    option.text = '...';
    option.value = '';
    document.getElementById('Estado').add(option);
    
    var option = document.createElement('option');
    option.text = 'Confirmada';
    option.value = 'Confirmada';
    document.getElementById('Estado').add(option);
    
    var option = document.createElement('option');
    option.text = 'Pendiente';
    option.value = 'Pendiente';
    document.getElementById('Estado').add(option);        
}

function Content()
{
    var Tipo = document.getElementById('Tipo').value;    
    if(Tipo == "Recolecci�n de Documentos"  || Tipo == "Reuni�n" || Tipo == "Evento del Proyecto" || Tipo == "Supervisi�n" || Tipo == "Otros" )
    {
        document.getElementById('Departamentos').options.length = 0;        
        document.getElementById('Departamentos').disabled = false;
        Departamentos();        
                
        document.getElementById('Municipio').disabled = false;
        document.getElementById('Municipio').options.length = 0;
                       
        document.getElementById('Cohorte').disabled = false;
        document.getElementById('Cohorte').options.length = 0;
        Cohorte();
        
        document.getElementById('Estado').options.length = 0;
        Estado();
        
        document.getElementById('Municipio').options.length = 0;        
    }
    
    if(Tipo == "Trabajo de Oficina")
    {        
        document.getElementById('ce').value = "FUSALMO";        
        document.getElementById('Departamentos').options.length = 0;                              
        document.getElementById('Municipio').options.length = 0;
        
        DepartamentosOficina();       
    }       
}
</script>
<style>
.tablatd td
{
    padding: 5px;
}
</style>
<?php

	if(isset($_POST['ida']))
    {
        $IdA = $_POST['ida'];
        $Actividad = $bddr->prepare("Select * from Actividad_Usaid where IdActividad = $IdA");
        $Actividad->execute();
        
        $DataA = $Actividad->fetch();
        $ABM = "Actualizar";
    }
    else
    {
        $ABM = "Crear Actividad";
    }
    ?>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px;">  
<table style="width: 100%;">
    <tr>
        <td><h2 style="color: #1D7399;">Mis Actividades</h2></td>
        <td style="text-align: right;">
            <table style="width: 50%; margin-left: 50%;">
                <tr>
                    <td>
                        <?php if(isset($_POST['ida'])) {?>
                        <form action="Empleados/Calendario/ActivityABM.php" method="post">
                            <input type="hidden" name="ida" value="<?php echo $_POST['ida']?>" />
                            <input type="submit" name="Enviar" class="botonE" value="Borrar" onclick="return confirm('Desea borrar realmente esta actividad??')" />
                        </form> 
                        <?php } ?>
                    </td>
                    <td>
                        <form action="?l=ActivityForm" method="post">
                            <input type="hidden" name="date" value="<?php echo $_POST['date']?>" />
                            <input type="submit" class="boton" value="<- Atras" />
                        </form> 
                    </td>
                </tr>
            </table>
           
        </td>
    </tr>
    <tr><td colspan="2"><hr color="#0099FF"/></td></tr>
    <tr>
        <td colspan="2">
            <form action="Empleados/Calendario/ActivityABM.php" method="post">
            <?php
            	if(isset($_POST['ida']))    
	                   echo "<input type='hidden' name='ida' value='$IdA' />";
            ?>
            <input type="hidden" name="Fecha" value="<?php echo $_POST['date']?>" />
            <table style="width: 80%;">
                <tr><td  style="width: 30%;">Nombre del Empleado: </td><td><?php echo "$DatosE[0] $DatosE[1] $DatosE[2] $DatosE[3]"?></td></tr>
                <tr><td >Cargo Asignado:                     </td><td><?php echo "$DatosE[5]"?></td></tr>
                <tr><td >Area de trabajo:                    </td><td><?php echo "$DatosE[6]"?></td></tr>
            </table>
            <hr color="#0099FF"/>        
            <table style="width: 100%;" class="tablatd">
                <tr>
                    <td style="width: 35%;">Fecha de Actividad:</td>
                    <td><?php 
                            	if(isset($_POST['ida']))    
	                               echo "<input type='text' name='datenew' value='".$_POST['date']."' />";
                                
                                else
                                    echo $_POST['date'];
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Tipo de Actividad:</td>
                    <td>
                        <select name="Tipo" id="Tipo" style="width: 50%;" onchange="Content()" required="true" >
                        <?php
	                           $array = array(
                                        "Asistencia T�cnica" => "Asistencia T�cnica", 
                                        "Recolecci�n de Documentos" => "Recolecci�n de Documentos",
                                        "Reuni�n en Departamental" => "Reuni�n en Departamental", 
                                        "Reuni�n en PESS" => "Reuni�n en PESS",
                                        "Reuni�n en MMPV" => "Reuni�n en MMPV", 
                                        "Reuni�n en UT" => "Reuni�n en UT",
                                        "Evento del Proyecto" => "Evento del Proyecto", 
                                        "Trabajo de Oficina" => "Trabajo de Oficina", 
                                        "Supervisi�n" => "Supervisi�n",
                                        "Otros" => "Otros(Especificar en Comentarios)");
                               
                               foreach ($array as $i => $value) {
                                    if($i == $DataA[2])
                                        echo "<option Selected='true' value='$i'>$value</option>";
                                        
                                    else
                                        echo "<option value='$i'>$value</option>";
                                }
                        ?>                                     
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Departamento:</td>
                    <td>
                        <select name="Departamentos" id="Departamentos" style="width: 50%;" onchange="CargarListaMunicipios()" required="true" >
                            <option value="">...</option>
                        <?php
	                           $array = array(
                                        "1" => "Ahuachap�n", 
                                        "2" => "Caba�as",
                                        "3" => "Chalatenango", 
                                        "4" => "Cuscatl�n",
                                        "5" => "La Libertad", 
                                        "6" => "La Paz",
                                        "7" => "La Uni�n", 
                                        "8" => "Moraz�n", 
                                        "9" => "San Miguel", 
                                        "10" => "San Salvador",  
                                        "11" => "San Vicente",
                                        "12" => "Santa Ana",     
                                        "13" => "Sonsonate",                                                                                                                            
                                        "14" => "Usulut�n");
                               
                               foreach ($array as $i => $value) {
                                    if($i == $DataA[3])
                                        echo "<option Selected='true' value='$i'>$value</option>";
                                        
                                    else
                                        echo "<option value='$i'>$value</option>";
                                }
                        ?>                                                                                  
                        </select>                    
                    </td>                    
                </tr>
                <tr>
                    <td>Municipio:</td>
                    <td>
                        <select name="Municipio" id="Municipio" style="width: 50%;" required="true" >
                            <option value="" selected="true">...</option>
                            <?php                                   
                                	if(isset($_POST['ida']))                                
                                        echo "<option Selected='true' value='$DataA[4]'>$DataA[4]</option>";
                                    
                            ?>                            
                        </select>                   
                    </td>
                </tr>
                <tr>
                    <td><label id="ActividadText">Lugar:</label></td>
                    <td><input type="text" id="ce" name="ce" value="<?php echo $DataA[5];?>" style="width: 50%;" required="true" /></td>
                </tr>   
                <tr>
                    <td>Cohorte:</td>
                    <td>
                        <select name="Cohorte" id="Cohorte" style="width: 32%;">
                        <option value="">...</option>
                        <?php
	                           $array = array(
                                        "1" => "1", 
                                        "2" => "2",
                                        "3" => "3", 
                                        "4" => "PESS");
                               
                               foreach ($array as $i => $value) 
                               {
                                    if($i == $DataA[6])
                                        echo "<option Selected='true' value='$i'>$value</option>";
                                        
                                    else
                                        echo "<option value='$i'>$value</option>";
                                }
                        ?>                          
                                             
                        </select>                     
                    </td>
                </tr>
<?php
                if(isset($_POST['ida']))
                {   
                    $Hi = substr($DataA[7],0,2);
                    $Mi = substr($DataA[7],3,2);
                    $Ti = substr($DataA[7],6,2);                    
                }
                else
                {
                    $Hi = "08";
                    $Mi = "00";
                    $Ti = "AM";                        
                }
?>                  
                <tr>
                    <td>Hora inicial programada:</td>
                    <td><input style="width: 40px;" type="number" id="HI" name="HI" min="00" max="12" required="true" value="<?php echo "$Hi";?>" title="Hora de Inicial"/>:
                        <input style="width: 40px;" type="number" id="MI" name="MI" min="00" max="59" required="true" value="<?php echo "$Mi";?>" title="Minutos de Inicial"/>
                        <select name="TI" id="TI">
                        <?php
                                    if("AM" == $Ti)
                                        echo "<option Selected='true' value='AM'>AM</option>";
                                        
                                    else
                                        echo "<option value='AM'>AM</option>";
                                        
                                    if("PM" == $Ti)
                                        echo "<option Selected='true' value='PM'>PM</option>";
                                        
                                    else
                                        echo "<option value='PM'>PM</option>";                                        
                        ?>
                        </select>
                    </td>
                </tr> 
<?php
                if(isset($_POST['ida']))
                {   
                    $Hi = substr($DataA[8],0,2);
                    $Mi = substr($DataA[8],3,2);
                    $Ti = substr($DataA[8],6,2);                    
                }
                else
                {
                    $Hi = "05";
                    $Mi = "00";
                    $Ti = "PM";                        
                }
?>                            
                <tr>
                    <td>Hora final estimada:</td>
                    <td><input style="width: 40px;" type="number" id="HF" name="HF" min="00" max="12" value="<?php echo "$Hi";?>" required="true" title="Hora de Finalizaci�n" />:
                        <input style="width: 40px;" type="number" id="MF" name="MF" min="00" max="59" value="<?php echo "$Mi";?>" required="true" title="Minutos de Finalizaci�n" />
                        <select name="TF" id="TF">
                        <?php
                                    if("AM" == $Ti)
                                        echo "<option Selected='true' value='AM'>AM</option>";
                                        
                                    else
                                        echo "<option value='AM'>AM</option>";
                                        
                                    if("PM" == $Ti)
                                        echo "<option Selected='true' value='PM'>PM</option>";
                                        
                                    else
                                        echo "<option value='PM'>PM</option>";                                        
                        ?>
                        </select>
                    </td>
                </tr>     
                <tr>
                    <td>Comentarios:</td>
                    <td><textarea name="comentarios" style="width: 50%; height: 50px;" required="true" ><?php echo $DataA[9];?></textarea></td>
                </tr> 
                <tr>
                    <td>Estado de Actividad:</td>
                    <td>
                        <select name="Estado" id="Estado" style="width: 32%;">
                            <option value="">...</option>
                            <?php
                                        if("Confirmada" == $DataA[10])
                                            echo "<option Selected='true' value='Confirmada'>Confirmada</option>";
                                            
                                        else
                                            echo "<option value='Confirmada'>Confirmada</option>";
                                            
                                        if("Pendiente" == $DataA[10])
                                            echo "<option Selected='true' value='Pendiente'>Pendiente</option>";
                                            
                                        else
                                            echo "<option value='Pendiente'>Pendiente</option>";                                        
                            ?>                                           
                        </select>                     
                    </td>
                </tr>  
                <tr>
                    <td>Requerimiento de Transporte:</td>
                    <td>
                        <select name="Transporte" id="Transporte" style="width: 32%;">
                            <?php
                                        if("Si" == $DataA[11])
                                            echo "<option Selected='true' value='Si'>Si</option>";
                                            
                                        else
                                            echo "<option value='Si'>Si</option>";
                                            
                                        if("No" == $DataA[11])
                                            echo "<option Selected='true' value='No'>No</option>";
                                            
                                        else
                                            echo "<option value='No'>No</option>";                                        
                            ?>               
                        </select>                     
                    </td>
                </tr>
                <tr><td colspan="2"><hr color="#0099FF"/></td></tr>
                <tr>
                    <td>Notificar a mis compa�eros:</td>
                    <td>
                        <select name="Notification" id="Notification" style="width: 32%;">
                            <option value="Si">Si</option>
                            <option value="No" selected="true">No</option>                 
                        </select>                     
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="Enviar" class="boton" value="<?php echo "$ABM";?>" /></td>
                </tr>                                                                                                            
            </table>
            </form>
        </td>
    </tr>    
</table>
</div>