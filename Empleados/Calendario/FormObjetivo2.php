
<script>
function CargarListaMunicipios()
{
    var departamento = document.getElementById('Departamentos').value;    
    document.getElementById('Municipio').options.length = 0;
    
    var aux = true;
    
    $.get("USAID/SelectMunicipios.php", { departamento:departamento, aux:aux },
  		function(resultado)
  		{           
            document.getElementById('Municipio').options.length = 0;
                
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#Municipio').append(resultado);	            			
  		}
   	);        
}

function Departamentos()
{
<?php
   $Departamentos = $bddC->prepare("SELECT * FROM USAID_Departamentos");
   $Departamentos->execute();
   
   echo "var option = document.createElement('option');
         option.text = '...';
         option.value = '';
         document.getElementById('Departamentos').add(option);";
                 
   while($DataD = $Departamentos->fetch())
   {
        echo "var option = document.createElement('option');
              option.text = '$DataD[1]';
              option.value = '$DataD[0]';
              document.getElementById('Departamentos').add(option);";
   }
?>
}

function Cohorte()
{
<?php

   echo "var option = document.createElement('option');
         option.text = '...';
         option.value = '';
         document.getElementById('Cohorte').add(option);";
         
   $i = 1; 
   while($i <= 4 )
   {
        echo "var option = document.createElement('option');
              option.text = '$i';
              option.value = '$i';
              document.getElementById('Cohorte').add(option);";
        $i++;
   }
?>    
}

function Estado()
{
    var option = document.createElement('option');
    option.text = '...';
    option.value = '';
    document.getElementById('Estado').add(option);
    
    var option = document.createElement('option');
    option.text = 'Confirmada';
    option.value = 'Confirmada';
    document.getElementById('Estado').add(option);
    
    var option = document.createElement('option');
    option.text = 'Pendiente';
    option.value = 'Pendiente';
    document.getElementById('Estado').add(option);        
}

function Content()
{
    var Tipo = document.getElementById('Tipo').value;    
    if(Tipo == "Recolecci�n de Documentos"  || Tipo == "Reuni�n" || Tipo == "Evento del Proyecto" || Tipo == "Supervisi�n" || Tipo == "Otros" )
    {
        document.getElementById('Departamentos').options.length = 0;        
        document.getElementById('Departamentos').disabled = false;
        Departamentos();        
                
        document.getElementById('Municipio').disabled = false;
        document.getElementById('Municipio').options.length = 0;
                       
        document.getElementById('Cohorte').disabled = false;
        document.getElementById('Cohorte').options.length = 0;
        Cohorte();
        
        document.getElementById('Estado').options.length = 0;
        Estado();
        
        document.getElementById('Municipio').options.length = 0;        
    }
    
    if(Tipo == "Trabajo de Oficina")
    {        
        document.getElementById('ce').value = "FUSALMO";
        
        document.getElementById('Departamentos').options.length = 0;
        document.getElementById('Departamentos').disabled = true;
                              
        document.getElementById('Municipio').options.length = 0;
        document.getElementById('Municipio').disabled = true;        
    }       
}
</script>
<style>
.tablatd td
{
    padding: 5px;
}
</style>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px;">  
<table style="width: 100%;">
    <tr>
        <td><h2 style="color: #1D7399;">Mis Actividades</h2></td>
        <td style="text-align: right;"><a href="?l=ActivityForm"><input type="button" class="boton" value="<- Atras" /></a></td>
    </tr>
    <tr><td colspan="2"><hr color="#0099FF"/></td></tr>
    <tr>
        <td colspan="2">
            <form action="Empleados/Calendario/ActivityABM.php" method="post">
            <input type="hidden" name="Fecha" value="<?php echo $_POST['date']?>" />
            <table style="width: 80%;">
                <tr><td  style="width: 30%;">Nombre del Empleado: </td><td><?php echo "$DatosE[0] $DatosE[1] $DatosE[2] $DatosE[3]"?></td></tr>
                <tr><td >Cargo Asignado:                     </td><td><?php echo "$DatosE[5]"?></td></tr>
                <tr><td >Area de trabajo:                    </td><td><?php echo "$DatosE[6]"?></td></tr>
            </table>
            <hr color="#0099FF"/>        
            <table style="width: 100%;" class="tablatd">
                <tr>
                    <td style="width: 35%;">Fecha de Actividad:</td>
                    <td><?php echo $_POST['date']?></td>
                </tr>
                <tr>
                    <td>Tipo de Actividad:</td>
                    <td>
                        <select name="Tipo" id="Tipo" style="width: 50%;" onchange="Content()">
                            <option value="Asistencia T�cnica">Asistencia T�cnica</option>
                            <option value="Recolecci�n de Documentos">Recolecci�n de Documentos</option>
                            <option value="Reuni�n en Departamental">Reuni�n en Departamental</option>
                            <option value="Reuni�n en PESS">Reuni�n en PESS</option>
                            <option value="Reuni�n en MMPV">Reuni�n en MMPV</option>
                            <option value="Reuni�n en UT">Reuni�n en UT</option>
                            <option value="Evento del Proyecto">Evento del Proyecto</option>
                            <option value="Trabajo de Oficina">Trabajo de Oficina</option>
                            <option value="Supervisi�n">Supervisi�n</option>
                            <option value="Otros">Otros(Especificar en Comentarios)</option>                 
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Departamento:</td>
                    <td>
                        <select name="Departamentos" id="Departamentos" style="width: 50%;" onchange="CargarListaMunicipios()" required="true" >
                            <option value="">...</option>
                            <option value="1">Ahuachap�n</option>
                            <option value="2">Caba�as</option>
                            <option value="3">Chalatenango</option>
                            <option value="4">Cuscatl�n</option>
                            <option value="5">La Libertad</option>
                            <option value="6">La Paz</option>
                            <option value="7">La Uni�n</option>
                            <option value="8">Moraz�n</option>
                            <option value="9">San Miguel</option>
                            <option value="10">San Salvador</option>
                            <option value="11">San Vicente</option>
                            <option value="12">Santa Ana</option>
                            <option value="13">Sonsonate</option>
                            <option value="14">Usulut�n</option>                      
                        </select>                    
                    </td>                    
                </tr>
                <tr>
                    <td>Municipio:</td>
                    <td>
                        <select name="Municipio" id="Municipio" style="width: 50%;" required="true">
                            <option value="" selected="true">...</option>
                        </select>                   
                    </td>
                </tr>
                <tr>
                    <td><label id="ActividadText">Lugar:</label></td>
                    <td><input type="text" id="ce" name="ce" value="" style="width: 50%;" /></td>
                </tr>   
                <tr>
                    <td>Cohorte:</td>
                    <td>
                        <select name="Cohorte" id="Cohorte" style="width: 32%;">
                            <option value="">...</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">PESS</option>                  
                        </select>                     
                    </td>
                </tr>  
                <tr>
                    <td>Hora inicial programada:</td>
                    <td><input style="width: 40px;" type="number" id="HI" name="HI" min="00" max="12" required="true" value="08" title="Hora de Inicial"/>:
                        <input style="width: 40px;" type="number" id="MI" name="MI" min="00" max="59" required="true" value="00" title="Minutos de Inicial"/>
                        <select name="TI" id="TI">
                                <option value="AM" selected="true">AM</option>
                                <option value="PM">PM</option>
                        </select>
                    </td>
                </tr> 
                <tr>
                    <td>Hora final estimada:</td>
                    <td><input style="width: 40px;" type="number" id="HF" name="HF" min="00" max="12" value="05" required="true" title="Hora de Finalizaci�n" />:
                        <input style="width: 40px;" type="number" id="MF" name="MF" min="00" max="59" value="00" required="true" title="Minutos de Finalizaci�n" />
                        <select name="TF" id="TF">
                                <option value="AM">AM</option>
                                <option value="PM" selected="true">PM</option>
                        </select>
                    </td>
                </tr>     
                <tr>
                    <td>Comentarios:</td>
                    <td><textarea name="comentarios" style="width: 50%; height: 50px;"></textarea></td>
                </tr> 
                <tr>
                    <td>Estado de Actividad:</td>
                    <td>
                        <select name="Estado" id="Estado" style="width: 32%;">
                            <option value="">...</option>
                            <option value="Confirmada">Confirmada</option>
                            <option value="Pendiente">Pendiente</option>                 
                        </select>                     
                    </td>
                </tr>  
                <tr>
                    <td>Requerimiento de Transporte:</td>
                    <td>
                        <select name="Transporte" id="Transporte" style="width: 32%;">
                            <option value="S�">S�</option>
                            <option value="No" selected="true">No</option>                 
                        </select>                     
                    </td>
                </tr>   
                <tr>
                    <td></td>
                    <td><input type="submit" name="Enviar" class="boton" value="Crear Actividad" /></td>
                </tr>                                                                                                            
            </table>
            </form>
        </td>
    </tr>
</table>
</div>