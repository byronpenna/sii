<?php
	require '../../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Crear Actividad")
    {

            if(strlen($_POST['HI']) == 1)
                $horahi = "0". $_POST['HI'];
            
            else
                $horahi = $_POST['HI'];

            // Hora final
            if(strlen($_POST['HF']) == 1)
                $horahf = "0". $_POST['HF'];
            
            else
                $horahf = $_POST['HF']; 
                
            // Minutos iniciales
            if(strlen($_POST['MI']) == 1)
                $minhi = "0". $_POST['MI'];
            
            else
                $minhi = $_POST['MI'];

            // Minutos finales
            if(strlen($_POST['MF']) == 1)
                $minhf = "0". $_POST['MF'];
            
            else
                $minhf = $_POST['MF'];                                
            
            $horai = "$horahi:$minhi " .$_POST['TI'] ;
            $horaf = "$horahf:$minhf " .$_POST['TF'] ;
        
        $Insert = $bddr->prepare("Insert into Actividad_Usaid 
                                  values(Null, :date, :Type, :Department, :Municipio, :Lugar, :Cohorte, :HoraI, :HoraF, :Comentario, :Estado, :Transport, :IdU)");
        $Insert->bindParam(':date', $_POST['Fecha']);
        $Insert->bindParam(':Type', $_POST['Tipo']);
        $Insert->bindParam(':Department', $_POST['Departamentos']);
        $Insert->bindParam(':Municipio', $_POST['Municipio']);
        $Insert->bindParam(':Lugar', $_POST['ce']);
        $Insert->bindParam(':Cohorte', $_POST['Cohorte']);
        $Insert->bindParam(':HoraI', $horai);
        $Insert->bindParam(':HoraF', $horaf);
        $Insert->bindParam(':Comentario', $_POST['comentarios']);
        $Insert->bindParam(':Estado', $_POST['Estado']);
        $Insert->bindParam(':Transport', $_POST['Transporte']);
        $Insert->bindParam(':IdU', $_SESSION['IdUsuario']);        
        $Insert->execute();
        
        if($_POST['Notification'] == "Si")
        {
            $CorreoEmpleado = $bddr->prepare("SELECT Correo FROM DatosInstitucionales where IdEmpleado = " . $DataH[5]);
            $CorreoEmpleado->execute();    
            $DataCJ = $CorreoEmpleado->fetch();     
                 
            
            $to = "$DataCJ[0]"; 
                      
            $subject = "Solicitud de Permiso de Horario Educativo";
            
            $message = "
                <h2>Solicitud de Permiso de Horario Educativo</h2><br />
                
                Nombre del Empleado: <br />
                $DataE[0] $DataE[1] $DataE[2] $DataE[3] $DataE[4] <br /><br />
                
                Instituci�n Educativa:<br />
                $DataH[9]<br /><br />
                
                Carrera de Estudio, Dipolmado o Curso:<br />
                $DataH[8]<br /><br />                
                
                Fecha de Inicio: <br />
                $DataH[1]<br /><br />
                
                Fecha de Finalizaci�n: <br />
                $DataH[2]<br /><br />
                
                Horario:<br /> 
                $TablaHorario
                
                Para aceptar o rechazar a la solicitud pulse  
                <a href='http://siifusalmo.org/PermisoView.php?h=$IdHorario'>Aqu�</a>";

            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org, permisos@fusalmo.org" . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            //mail($to,$subject,$message,$headers);            
        }
        
        $bddr = null;
        $Insert = null;
        Redireccion("../../Empleado.php?l=Calendar&n=1");      
    }
    else if($_POST['Enviar'] == "Compartir")
    {
        $Insert = $bddr->prepare("Insert into Actividad_Compartir 
                                  values(Null, :ide, :idi)");
        $Insert->bindParam(':ide', $_POST['ide']);
        $Insert->bindParam(':idi', $_POST['idi']);      
        $Insert->execute();
        
        $bddr = null;
        $Insert = null;
        Redireccion("../../Empleado.php?l=Calendar&n=1#Share");          
    }
    else if($_POST['Enviar'] == "Quitar")
    {
        $Insert = $bddr->prepare("Delete from Actividad_Compartir where IdCompartir = :idc");
        $Insert->bindParam(':idc', $_POST['idc']);      
        $Insert->execute();
        
        $bddr = null;
        $Insert = null;
        Redireccion("../../Empleado.php?l=Calendar&n=3#Share");          
    } 
    else if($_POST['Enviar'] == "Actualizar")
    {
        if(strlen($_POST['HI']) == 1)
            $horahi = "0". $_POST['HI'];
        
        else
            $horahi = $_POST['HI'];

        // Hora final
        if(strlen($_POST['HF']) == 1)
            $horahf = "0". $_POST['HF'];
        
        else
            $horahf = $_POST['HF']; 
            
        // Minutos iniciales
        if(strlen($_POST['MI']) == 1)
            $minhi = "0". $_POST['MI'];
        
        else
            $minhi = $_POST['MI'];

        // Minutos finales
        if(strlen($_POST['MF']) == 1)
            $minhf = "0". $_POST['MF'];
        
        else
            $minhf = $_POST['MF'];                                
        
        $horai = "$horahi:$minhi " .$_POST['TI'];
        $horaf = "$horahf:$minhf " .$_POST['TF'];
                    
        $Update = $bddr->prepare("Update Actividad_Usaid set 
                                    Fecha = :date,
                                    TipoActividad = :Type,
                                    Departamento = :Department,
                                    Municipio = :Municipio,
                                    CentroEscolar = :Lugar,
                                    Cohorte = :Cohorte, 
                                    HoraInicial = :HoraI,
                                    HoraFinal = :HoraF,
                                    Comentario = :Comentario,
                                    Estado = :Estado, 
                                    Transporte = :Transport 
                                  where IdActividad = :ida");
                                  
                                  
        $Update->bindParam(':date', $_POST['datenew']);
        $Update->bindParam(':Type', $_POST['Tipo']);
        $Update->bindParam(':Department', $_POST['Departamentos']);
        $Update->bindParam(':Municipio', $_POST['Municipio']);
        $Update->bindParam(':Lugar', $_POST['ce']);
        $Update->bindParam(':Cohorte', $_POST['Cohorte']);
        $Update->bindParam(':HoraI', $horai);
        $Update->bindParam(':HoraF', $horaf);
        $Update->bindParam(':Comentario', $_POST['comentarios']);
        $Update->bindParam(':Estado', $_POST['Estado']);
        $Update->bindParam(':Transport', $_POST['Transporte']);
        $Update->bindParam(':ida', $_POST['ida']);        
        $Update->execute();
        
        $bddr = null;
        $Update = null;
        Redireccion("../../Empleado.php?l=Calendar&n=2");          
    }        
    else if($_POST['Enviar'] == "Borrar")
    {
        $Delete = $bddr->prepare("Delete from Actividad_Usaid where IdActividad = :ida");
        $Delete->bindParam(':ida', $_POST['ida']);      
        $Delete->execute();
        
        $bddr = null;
        $Delete = null;
        Redireccion("../../Empleado.php?l=Calendar&n=3#Share");          
    }     
    if($_POST['Enviar'] == "Crear")
    {

            if(strlen($_POST['HI']) == 1)
                $horahi = "0". $_POST['HI'];
            
            else
                $horahi = $_POST['HI'];

            // Hora final
            if(strlen($_POST['HF']) == 1)
                $horahf = "0". $_POST['HF'];
            
            else
                $horahf = $_POST['HF']; 
                
            // Minutos iniciales
            if(strlen($_POST['MI']) == 1)
                $minhi = "0". $_POST['MI'];
            
            else
                $minhi = $_POST['MI'];

            // Minutos finales
            if(strlen($_POST['MF']) == 1)
                $minhf = "0". $_POST['MF'];
            
            else
                $minhf = $_POST['MF'];                                
            
            $horai = "$horahi:$minhi " .$_POST['TI'] ;
            $horaf = "$horahf:$minhf " .$_POST['TF'] ;
        
        $Insert = $bddr->prepare("Insert into Actividad_Usaid 
                                  values(Null, :date, :Type, :Department, :Municipio, :Lugar, '', :HoraI, :HoraF, :Comentario, :Estado, :Transport, :IdU)");
        $Insert->bindParam(':date', $_POST['Fecha']);
        $Insert->bindParam(':Type', $_POST['Tipo']);
        $Insert->bindParam(':Department', $_POST['Departamentos']);
        $Insert->bindParam(':Municipio', $_POST['Municipio']);
        $Insert->bindParam(':Lugar', $_POST['ce']);
        $Insert->bindParam(':HoraI', $horai);
        $Insert->bindParam(':HoraF', $horaf);
        $Insert->bindParam(':Comentario', $_POST['comentarios']);
        $Insert->bindParam(':Estado', $_POST['Estado']);
        $Insert->bindParam(':Transport', $_POST['Transporte']);
        $Insert->bindParam(':IdU', $_SESSION['IdUsuario']);        
        $Insert->execute();
        
        if($_POST['Notification'] == "Si")
        {
            $CorreoEmpleado = $bddr->prepare("SELECT Correo FROM DatosInstitucionales where IdEmpleado = " . $DataH[5]);
            $CorreoEmpleado->execute();    
            $DataCJ = $CorreoEmpleado->fetch();     
                 
            
            $to = "$DataCJ[0]"; 
                      
            $subject = "Solicitud de Permiso de Horario Laboral";
            
            $message = "
                <h2>Solicitud de Permiso de Horario Laboral</h2><br />
                
                Nombre del Empleado: <br />
                $DataE[0] $DataE[1] $DataE[2] $DataE[3] $DataE[4] <br /><br />
                
                Instituci�n Educativa:<br />
                $DataH[9]<br /><br />
                
                Carrera de Estudio, Dipolmado o Curso:<br />
                $DataH[8]<br /><br />                
                
                Fecha de Inicio: <br />
                $DataH[1]<br /><br />
                
                Fecha de Finalizaci�n: <br />
                $DataH[2]<br /><br />
                
                Horario:<br /> 
                $TablaHorario
                
                Para aceptar o rechazar a la solicitud pulse  
                <a href='http://siifusalmo.org/PermisoView.php?h=$IdHorario'>Aqu�</a>";

            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org, permisos@fusalmo.org" . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            //mail($to,$subject,$message,$headers);            
        }
        
        $bddr = null;
        $Insert = null;
        Redireccion("../../Empleado.php?l=Calendar&n=1");      
    }
}
?>