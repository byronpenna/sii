<?php

/**
 * @author Manuel Calder�n
 * @copyright 2013
 */


$query = "SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado, ca.IdAsignacion 
          FROM CargosAsignacion AS ca
          INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
          INNER JOIN Empleado AS e ON ca.IdEmpleado = e.IdEmpleado
          INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
          WHERE e.IdEmpleado = ". $_SESSION["IdUsuario"] ." and ca.FechaFin = '0000-00-00'";

$Empleado = $bddr->prepare($query);                                            
$Empleado->execute();
$DatosE = $Empleado->fetch(); 


if(isset($_GET['Form']))
{
    if($_GET['Form'] == "Object1")
        include("Empleados/Calendario/FormObjetivo1.php");
        
    else if($_GET['Form'] == "Object2")
        include("Empleados/Calendario/FormObjetivo2.php");
        
    else if($_GET['Form'] == "Operation")
        include("Empleados/Calendario/FormFUSALMO.php");
        
    else if($_GET['Form'] == "Proyect")
        include("Empleados/Calendario/FormProyect.php");                     
}
else
{
?>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px;">  
<table style="width: 100%;">
    <tr>
        <td colspan="3"><h2 style="color: #1D7399;">Formulario de Actividades</h2></td> 
        <td style="text-align: right;"><a href="?l=Calendar"><input type="button" class="boton" value="<- Atras" /></a></td>           
    </tr>
    <tr><td colspan="4"><hr color="0099FF"/></td></tr>
    <tr><td colspan="4">Seleccione la procedencia de su actividad:<br /><br /></td></tr>
    <tr>
        <td style="text-align: center; width: 25%;">
        <form action="?l=ActivityForm&Form=Object1" method="post">
            <input type="hidden" name="date" value="<?php echo $_POST['date']?>" />
            <input type="submit" style="width: 120px;" class="boton" value="USAID - Objetivo 1" />
        </form>
        </td>
        <td style="text-align: center; width: 25%;">
            <form action="?l=ActivityForm&Form=Object2" method="post">
                <input type="hidden" name="date" value="<?php echo $_POST['date']?>" />
                <input type="submit" style="width: 120px;" class="boton" value="USAID - Objetivo 2" />
            </form>        
        </td>
        <td style="text-align: center; width: 25%;">
            <form action="?l=ActivityForm&Form=Operation" method="post">
                <input type="hidden" name="date" value="<?php echo $_POST['date']?>" />
                <input type="submit" style="width: 120px;" class="boton" value="Operativa" />
            </form>            
        </td>
        <td style="text-align: center; width: 25%;">
            <form action="?l=ActivityForm&Form=Proyect" method="post">
                <input type="hidden" name="date" value="<?php echo $_POST['date']?>" />
                <input type="submit" style="width: 120px;" class="boton" value="Proyectos Externos" />
            </form>         
        </td>
    </tr>
</table>
<br /><br />
</div>
<?php
}   
?>