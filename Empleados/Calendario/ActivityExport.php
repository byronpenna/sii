<?php
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=Reporte de Actividades.xls");
    header('Content-Transfer-Encoding: binary');
    require '../../net.php';
?>
<table style="width: 2000px;" rules='all'>
<tr style='height: 50px;'>
    <th style="width: 180px;">Empleado</th>
    <th style="width: 100px;">Fecha</th>
    <th style="width: 225px;">Tipo de Actividad</th>
    <th style="width: 150px;">Departamento</th>
    <th style="width: 150px;">Municipio</th>
    <th style="width: 225px;">Lugar</th>
    <th style="width: 50px;">Cohorte</th>
    <th style="width: 100px;">Hora Inicial</th>
    <th style="width: 100px;">Hora Final</th>
    <th style="width: 200px;">Comentarios</th>
    <th style="width: 100px;">Estado</th>
    <th style="width: 75px;">Transporte</th>                    
</tr>
            
<?php

   $AllActividades = $bddr->prepare($_SESSION['Query']);
   $AllActividades->execute();

   if($AllActividades->rowCount() > 0)
   {
        while($DataAA = $AllActividades->fetch())
       {
           $Departamentos = $bddC->prepare("SELECT Departamento FROM USAID_Departamentos where IdDepartamento = $DataAA[3]");
           $Departamentos->execute();        
           $DataD = $Departamentos->fetch();
          
           $Empleado = $bddr->prepare("SELECT Nombre1, Nombre2, Nombre3, Apellido1, Apellido2 FROM Empleado where IdEmpleado = $DataAA[12]");
           $Empleado->execute();        
           $DataE = $Empleado->fetch();                     
          
           echo "<tr style='height: 35px;'>
                    <td style='text-align: center;'>$DataE[0] $DataE[1] $DataE[2] $DataE[3] $DataE[4]</td>
                    <td style='text-align: center;'>$DataAA[1]</td>
                    <td style='text-align: center;'>$DataAA[2]</td>
                    <td style='text-align: center;'>$DataD[0]</td>
                    <td style='text-align: center;'>$DataAA[4]</td>
                    <td>$DataAA[5]</td>
                    <td style='text-align: center;'>$DataAA[6]</td>
                    <td style='text-align: center;'>$DataAA[7]</td>
                    <td style='text-align: center;'>$DataAA[8]</td>
                    <td>$DataAA[9]</td>
                    <td style='text-align: center;'>$DataAA[10]</td>
                    <td style='text-align: center;'>$DataAA[11]</td>
                </tr>";
       }
   }
   else
   {
        echo "<tr>
                <td colspan='5' style='text-align: center;'><em style='color: red'>No hay actividades registradas</em></td>
                <td colspan='6'></td>
              </tr>";
   }    
?>
 </table>