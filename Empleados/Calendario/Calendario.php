<?php

/**
 * @author Manuel Calder�n
 * @copyright 2013
 */

setlocale (LC_TIME, "es_SV");
function getMonthDays($Month, $Year)
{
   //Si la extensi�n que mencion� est� instalada, usamos esa.
   if( is_callable("cal_days_in_month"))
   {
      return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
   }
   else
   {
      //Lo hacemos a mi manera.
      return date("d",mktime(0,0,0,$Month+1,0,$Year));
   }
}

function IstheDay($DayIndex, $Date)
{
    if($DayIndex == date("N", strtotime($Date)))
        return true;
        
    else
        return false;
}

$IdEmpleado = $_SESSION['IdUsuario'];

if(isset($_GET['date']))
{
    $fechas = explode("-", $_GET['date']);
    if($_GET['ref'] == "back")
    {
        if($fechas[1] == "01")
        {
            $mes = 12;
            $a�o = $fechas[0] - 1;
        }
        else
        {
            $mes = $fechas[1] - 1;
            $a�o = $fechas[0];    
        }
    }
    else if($_GET['ref'] == "forward")
    {
        if($fechas[1] == "12")
        {
            $mes = 1;
            $a�o = $fechas[0] + 1;
        }
        else
        {
            $mes = $fechas[1] + 1;
            $a�o = $fechas[0];    
        }       
    }
    else
    {
        $mes = $fechas[1];
        $a�o = $fechas[0];
    }
}
else
{
    $mes = date("m");
    $a�o = date("Y");
}

if(strlen($mes) == 1)
    $mes = "0" . $mes;

$day = "01";

$diasMes = getMonthDays($mes, $a�o);
$Date = "$a�o-$mes-$day";

$Ref = "$a�o-$mes";

if($mes == "01")$NMes = "Enero";
if($mes == "02")$NMes = "Febrero";
if($mes == "03")$NMes = "Marzo";
if($mes == "04")$NMes = "Abril";
if($mes == "05")$NMes = "Mayo";
if($mes == "06")$NMes = "Junio";
if($mes == "07")$NMes = "Julio";
if($mes == "08")$NMes = "Agosto";
if($mes == "09")$NMes = "Septiembre";
if($mes == "10")$NMes = "Octubre";
if($mes == "11")$NMes = "Noviembre";
if($mes == "12")$NMes = "Diciembre";


$query = "SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado, ca.IdAsignacion 
          FROM CargosAsignacion AS ca
          INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
          INNER JOIN Empleado AS e ON ca.IdEmpleado = e.IdEmpleado
          INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
          WHERE e.IdEmpleado = $IdEmpleado and ca.FechaFin = '0000-00-00'";

$Empleado = $bddr->prepare($query);                                            
$Empleado->execute();
$DatosE = $Empleado->fetch(); 

unset($_SESSION['Ids'], $_SESSION['ide']);
?>

<style>
.tdCalendar
{
    border-color: green;
    border-radius: 5px;
    border-width: 2px;	
    border-style: solid;
    background-color: white;
}
.tdCalendar:hover 
{
    border-color: blue;
    border-radius: 5px;
    border-width: 2px;	
    border-style: dashed; 
    background-color: skyblue;
}
</style>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding: 10px;">  
<table style="width: 100%;">
    <tr><td><h2 style="color: #1D7399;">Mis Actividades</h2></td></tr>
    <tr><td><hr color='0099FF' /></td></tr>
    <tr>
        <td>
            <table style="width: 80%;">
                <tr><td  style="width: 30%;">Nombre del Empleado: </td><td><?php echo "$DatosE[0] $DatosE[1] $DatosE[2] $DatosE[3]"?></td>
                    <td rowspan="3" style="text-align: right;">
                        <form action="?l=Activity" method="post">
                            <input type="submit" value="Ver Resumen" class="boton" name="Enviar" /><br /><br />
                        </form>
                        
                    </td>
                </tr>
                <tr><td >Cargo Asignado:                     </td><td><?php echo "$DatosE[5]"?></td></tr>
                <tr><td >Area de trabajo:                    </td><td><?php echo "$DatosE[6]"?></td></tr>
            </table>
        </td>
    </tr>
    <tr><td><hr color='0099FF' /></td></tr>
    <tr>
        <td>        
            <table style="width: 100%; border: 1px;">
                <?php echo "<tr><td style='width: 15%;'>
                                    <a href='?l=Calendar&date=$Ref&ref=back'><input type='button' value='<-' class='botonG' /></a>
                                </td>
                                <th colspan='5'>$NMes</th>
                                <td style='width: 15%;'>
                                    <a href='?l=Calendar&date=$Ref&ref=forward'><input type='button' value='->' class='botonG' /></a>
                                </td>
                            </tr>"; 
                ?>
                <tr><td colspan="7"><hr color='0099FF' /></td></tr>
                <tr>
                    <th style="">Lunes</th>
                    <th style="width: 14%; ">Martes</th>
                    <th style="width: 14%;">Mi�rcoles</th>
                    <th style="width: 14%;">Jueves</th>
                    <th style="width: 14%;">Viernes</th>
                    <th style="width: 14%;">S�bado</th>
                    <th style="">Domingo</th>
                </tr>
                <?php
                       
                       for($i = 1; $i <= 6; $i++)
                       {
                            echo "<tr style='height: 100px'>";
                            for($j = 1; $j <= 7; $j++)
                            {
                                echo "<td style='vertical-align: top;' class='tdCalendar'>";
                                if(IstheDay($j, $Date))
                                {
                                    if($day <= $diasMes)
                                    {
                                        //Validaci�n del d�a Actual
                                        echo "<table style='width: 100%;'>
                                              <tr><td>";
                                        if($Date == date("Y-m-d"))
                                            echo "<h2 style='color: green'><strong>$day</strong></h2>";
                                        
                                        else
                                            echo "<h2>$day</h2>";
                                        
                                        echo "</td><td style='text-align: right;'>
                                                    <form action='?l=ActivityForm' method='post'>
                                                        <input type='hidden' value='$Date' name='date' />
                                                        <input style='width: 25px' type='submit' name='Enviar' value='+' class='botonG' />
                                                    </form>
                                                    </td>";
                                        
                                        $day++;                                                                                
                                        if(strlen($day) == 1)
                                            $day = "0" . $day;
                                        
                                        echo "<tr><th colspan='2' style='vertical-align: bottom;'><br />";
                                        
                                        
                                        $Actividad = $bddr->prepare("Select IdActividad from Actividad_Usaid where IdUsuaio = $IdEmpleado and Fecha = '$Date'");
                                        $Actividad->execute();
                                        
                                        if($Actividad->rowCount() > 0)
                                        {
                                            echo "  <form action='?l=Activity' method='post'>
                                                        <input type='hidden' value='$Date' name='date' />
                                                        <input type='hidden' value='Fecha' name='Enviar' />
                                                        <input style='padding: 0px; border-color: transparent; background-color: transparent;' type='submit' name='Enviar2' value='Actividades: ".$Actividad->rowCount()."' />
                                                     </form>";
                                        }            
                                                  
                                        echo "    </th>
                                              </tr>
                                              </table>";
                                        
                                        
                                        $Date = "$a�o-$mes-$day";
                                    } 
                                }                                                             
                                echo "</td>";
                            }
                            echo "</tr>"; 
                       }     
                ?>
            </table>
        </td>
    </tr>
    <tr><td><hr color='0099FF' /></td></tr>
    <tr>
        <td>
        <?php
	           $Compartir = $bddr->prepare("Select * from Actividad_Compartir where IdEmpleado = $IdEmpleado");
               $Compartir->execute();               
        ?>
            <table style="width: 100%;">
                <a name="Share" id="Share"></a>
                <tr><td><h2>Compartir Calendario</h2></td></tr>
                <tr><td>
                    Este horario es compartido por defecto con tu Jefe Inmediato y con RRHH, pero puede ser compartido con compa�eros de trabajo para poder 
                    coordinar actividades.<br /><br />
                    Cantidad de personas con quienes has compartido tu calendario: <strong><?php echo $Compartir->rowCount();?> Personas</strong>
                    </td>
                </tr>
                <tr>
                    <td><br /><br />
                        <table style='width: 100%; '>
                        <tr><th style='width: 25%'>�rea</th><th  style='width: 20%'>Cargo</th><th  style='width: 35%'>Empleado</th><th  style='width: 20%'></th></tr>
                        <?php   
                               if($Compartir->rowCount() > 0)
                               {
                                    while($DataC = $Compartir->fetch())
                                    {
                                        $Empleados = $bddr->prepare("SELECT e.IdEmpleado, e.Nombre1, e.Nombre2, e.Nombre3, e.Apellido1, e.Apellido2, t.NombreAreaDeTrabajo, c.Cargo
                                                                     FROM Empleado as e 
                                                                     INNER JOIN CargosAsignacion as ca on e.IdEmpleado = ca.IdEmpleado
                                                                     INNER JOIN Cargos as c on ca.IdCargo = c.IdCargos 
                                                                     INNER JOIN AreasDeTrabajo as t on c.IdArea_Fk = t.IdAreaDeTrabajo
                                                                     where e.IdEmpleado = $DataC[2] and FechaFin = '0000-00-00' 
                                                                     GROUP BY e.IdEmpleado");
                                        
                                        $Empleados->execute();    
                                        $DataE = $Empleados->fetch();
                                        
                                        echo "<tr><td>$DataE[6]</td>
                                                  <td>$DataE[7]</td>
                                                  <td>$DataE[1] $DataE[2] <br /> $DataE[3] $DataE[4]</td>
                                                  <td>
                                                      <form action='Empleados/Calendario/ActivityABM.php' method='post'>
                                                          <input type='hidden' name='idc' value='$DataC[0]' />
                                                          <input type='submit' name='Enviar' value='Quitar' class='botonE' onclick='return confirm(\"�Realmente desea dejar de compartir sus actividades?\")' />
                                                      </form>
                                                  </td>
                                                </tr>";
                                                                                                                         
                                    }                                
                               }
                               else
                                echo "<tr><td colspan='4' style='text-align: center;'><em style='color: red'>No haz compartido tus actividades con alguien.</em></td></tr>";
                        ?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><br /><br />
                    <script>                    
                        function CargarEmpleados()
                        {    
                            var code = $("#Searching").val();
                            var code2 = <?php echo $IdEmpleado;?>;     	
                            document.getElementById('Fill').innerHTML = "<center><em style='color: green;'>Cargando datos...</em></center>";
                            
                            $.get("Empleados/Calendario/Compartir.php", { code: code, code2: code2 },
                        		function(resultado)
                        		{
                                    document.getElementById('Fill').innerHTML = "";
                        			if(resultado == false)   			
                        				alert("Error");
                        			
                        			else    				    
                        				$('#Fill').append(resultado);	
                        		}
                        	);    
                        }
                    </script>                    
                    <table style="width: 100%;">
                        <tr><td><h2>Buscar para Compartir</h2></td></tr>
                        <tr>
                            <td style="width: 40%;">Escriba un nombre o un apellido para compartir:</td>
                            <td><input type="text" id="Searching" style="width: 100%;" onkeyup="CargarEmpleados()"></td>
                            <td></td>                    
                        </tr>
                        <tr>
                            <td colspan="3"><div id="Fill"></div></td>
                        </tr>            
                    </table>                        
                    </td>                                        
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>