<?php

    class Headquarter
    {
        private $db;
        
        public function __construct()
        {
            require_once('Connection.php');
            $this->db = new Connection();
            $this->db->connect();
            $this->db = $this->db->connection;
        }

        public function getHeadquarters()
        {
            $obj = [];
            $query = $this->db->prepare("SELECT * FROM sedes_fusalmo;");
            $query->execute();

            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                array_push($obj, $row);
            }

            return json_encode($obj);
        }
    }
    

?>