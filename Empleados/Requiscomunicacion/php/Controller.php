<?php

    if(!isset($_SESSION["autenticado"])){
        require '../../../net.php';
    }

    require_once('Classes/Provider.php');
    require_once('Classes/Requisicion.php');
    require_once('Classes/Headquarter.php');

    $provider = new Provider();
    $requisition = new Requisicion();
    $headquarter = new Headquarter();

    if(isset($_POST['addProvider'])){
        echo $provider->addProvider(json_decode($_POST['data'], 1));
    }

    if(isset($_POST['getProviders'])){ echo $provider->getProviders($_POST['type']); } //Obtención de JSON de proveedores

    if(isset($_GET['l']) == 'listMaterial'){
        echo $requisition->loadMaterial($_GET['id'], $_GET['type']);
    }

    if(isset($_POST['insertComparation'])){
        echo $requisition->insertComparation(json_decode($_POST['data'], 1), $_POST['idRequi'], $_POST['type']);
    }

    if(isset($_POST['getHeadquarters'])){
        echo $headquarter->getHeadquarters();
    }

    if(isset($_POST['getProvidersForOrder'])){
        echo json_encode($provider->providersForOrder($_POST['idRequisition'], $_POST['type']));
    }

    if(isset($_POST['modifyRequisition'])){
        echo $requisition->tableUpdate($_POST['idRequisition'], $_POST['type']);
    }

    if(isset($_POST['updateComparative'])){
        echo $requisition->updateComparative(json_decode($_POST['data'], 1), $_POST['idRequi'], $_POST['type']);
    }
?>
