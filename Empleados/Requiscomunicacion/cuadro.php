<meta charset="UTF-8">
<link rel="stylesheet" href="/Empleados/Requisiciones/styles/main.css">
<!-- <script src="/Empleados/Requisiciones/js/jquery.js"></script> -->
<script src="/Empleados/Requisiciones/js/validate.js"></script>
<script src="/Empleados/Requisiciones/js/init.js"></script>
<script src="/Empleados/Requisiciones/js/functions.js"></script>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">

    <div class="modal" id="mdlProvider">
        <div class="content">
            <form name="frmProvider" id="frmProvider" class='row'>
                <div class='content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtName">Nombre</label>                
                    <input type="text" name="txtName" id="txtName">
                </div>
                
                <div class='content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtAddress">Direcci&oacute;n</label>
                    <input type="text" name="txtAddress" id="txtAddress">
                </div>
                <div class='content-field col l8 m10 s12 offset-l2 offset-m1'>                
                    <label for="txtCity">Ciudad</label>
                    <input type="text" name="txtCity" id="txtCity">
                </div>     
                <div class='content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtState">Estado</label>
                    <input type="text" name="txtState" id="txtState">
                </div>
                <div class='content-field col l8 m10 s12 offset-l2 offset-m1'>                
                    <label for="txtPhone">Tel&eacute;fono</label>
                    <input type="text" name="txtPhone" id="txtPhone">
                </div>
                <div class='content-field radio col l8 m10 s12 offset-l2 offset-m1'>                
                    <label for="txtCode">C&oacute;digo Postal</label>
                    <input type="text" name="txtCode" id="txtCode">
                </div>
                <div class='content-field radio col l8 m10 s12 offset-l2 offset-m1'>                
                    <input type="radio" name="rdbType" value="M"><label>Material</label> <br>
                    <input type="radio" name="rdbType" value="A"><label>Alimentos</label>               
                </div>
                <div class="content-field">
                    <button class="btn" id="btnAddProvider">Registrar</button>
                </div>
            </form>
        </div>
        <div class="footer fixed-foo">
            <a class="modal-btn close" mdl-action="close">Cerrar</a>
        </div>
    </div>
    
    <button class="modal-trigger" modal="mdlProvider">Agregar Proveedor</button>
    <div class="container table-material"></div>
    <div class="container-buttons">
        <form action="/Empleados/Requisiciones/php/Print.php" method="POST" target="_blank">
            <input type="hidden" name="idProvider" value="2">
            <input type="hidden" name="idRequision" value="69">
            <input type="hidden" name="purchaseOrder" value="true">
            <button>Orden de Compra</button>
        </form>
        <form action="/Empleados/Requisiciones/php/Print.php" method="POST" target="_blank">
            <input type="hidden" name="idRequision" value="69">
            <input type="hidden" name="comparation" value="true">
            <button>Cuadro Comparativo</button>
        </form>
     </div>
</div>
