<?php


/**
 * @author Manuel Calderón-Lilian Cordero
 * @copyright 2013 - 2016
 */


  require '../../net.php';
    
    if(isset($_POST['Enviar']))
    {    
        if($_POST['Enviar'] == "Crear requisicion")
        {
            $Addrequest = $bddr->prepare("Insert into Request_Requisiciones 
            values (Null, :IdEmpleado, :Idproyecto, :Actividad, :Fecha_Solicitud, :Fecha_entrega, :Comentario, :Linea_Presupuestaria, :SubLinea_Presupuestaria, :Tipo_Requisicion, 'Pendiente' , '', :idj )");
            $Addrequest->bindParam(':IdEmpleado', $_SESSION["IdUsuario"]);
            $Addrequest->bindParam(':Idproyecto', $_POST['proyectoR']);
            $Addrequest->bindParam(':Actividad', $_POST['actividad']);
            $Addrequest->bindParam(':Fecha_Solicitud', date("Y-m-d"));
            $Addrequest->bindParam(':Fecha_entrega', $_POST['fecha']);
            $Addrequest->bindParam(':Comentario', $_POST['comentarios']);
            $Addrequest->bindParam(':Linea_Presupuestaria', $_POST['presupuestariaR']);
            $Addrequest->bindParam(':SubLinea_Presupuestaria', $_POST['subpresupuestariaR']);
            $Addrequest->bindParam(':Tipo_Requisicion', $_POST['tipo']);
            $Addrequest->bindParam(':idj', $_POST['idj']);

            if ($Addrequest->execute()) {
                echo "Septumsembra";
            }
            else
                echo "AbadaKadabra";

            $id = $bddr->lastInsertId();

            $cantidad = isset($_POST['cantidad']) ? $_POST['cantidad'] : array();
            $espe = isset($_POST['comentario']) ? $_POST['comentario'] : array();        
            $material = isset($_POST['materiales']) ? $_POST['materiales'] : array(); 
            for ($index = 0 ; $index < count($cantidad); $index++) 
            {                                
                $AddMaterial = $bddr->prepare("Insert into Request_Material values (Null, :IdRequisicion, :Cantidad, :Espe_Tecnica, :Material )");
                $AddMaterial->bindParam(':IdRequisicion',$id);
                $AddMaterial->bindParam(':Cantidad', $cantidad[$index]);
                $AddMaterial->bindParam(':Espe_Tecnica', $espe[$index]);
                $AddMaterial->bindParam(':Material', $material[$index]);
                $AddMaterial->execute();
            }

            $to = "desarrollo@fusalmo.org";                        
            $subject = "Solicitud de Requisición";
            
            $message = "
                <h2>Solicitud de Requisición</h2><br />
            
                Nombre del Empleado: <br /> ".$_POST['NombreEmpleado']." <br /><br />
                Actividad <br /> ".$_POST['actividad']." <br /><br />
                Fecha de entrega <br /> ".$_POST['fecha']." <br /><br />
                Comentarios <br /> ".$_POST['comentarios']."
                
                Materiales: <br/>
               <table>
               <tr><th>Cantidad</th><th>Material</th><th>Especificación Tecnica</th></tr>";
                
                for ($index = 0 ; $index < count($cantidad); $index++) 
                {    
                    $message .= "
                                <tr>
                                   <td>$cantidad[$index], </td>
                                   <td>$espe[$index]</td>
                                   <td>$material[$index]</td>
                                </tr>";
                }
                $message .= "</table>";
                
                  

            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org," . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            mail($to,$subject,$message,$headers);  
            Redireccion("../../Empleado.php?l=Request");
        }
        
        else if($_POST['Enviar'] == "Actualizar")
        {
           
        }    
        
        else if($_POST['Enviar'] == "Eliminar")
        {
          
        }     

        if($_POST['Enviar'] == "Guardar")
        {
            if($_POST['Decision'] == "Denegar")
                $estado = "Denegado"; 
                
            if($_POST['Decision'] == "Aceptar")
                $estado = "Aceptado";

            $query = "UPDATE Request_Requisiciones SET Estado = '$estado' , Comentario_Jefe = '$Comentario' WHERE IdRequisicion =" . $_POST["idp"];
            $Update = $bddr->prepare($query);   
            $Update->bindParam(':Comentario_Jefe',$_POST['Comentario']);       
            $Update->execute();


           if($estado == "Aceptado") {

            $query3 = "SELECT di.Correo FROM CargosAsignacion as ca 
						INNER JOIN DatosInstitucionales as di on ca.IdEmpleado = di.IdEmpleado
						WHERE ca.IdCargo = 24 AND ca.FechaFin = '0000-00-00' ";
			$Gerencia = $bddr->prepare($query3);   
			$Gerencia ->execute();
			$DataG = $Gerencia->fetch(); 

		   $MisRequisiciones = $bddr->prepare("SELECT * FROM Request_Requisiciones as r 
                                                            inner join Request_Proyecto as p on r.IdProyecto = p.IdProyecto 
                                                            inner join Empleado AS m on r.IdEmpleado = m.IdEmpleado
                                                           " ) ;
                        $MisRequisiciones->execute();
                        $Requi = $MisRequisiciones->fetch();

			$sqlMaterial= "SELECT * FROM Request_Material as k 
                        inner join Request_Requisiciones as o on k.Idrequisicion = o.Idrequisicion
                        where k.IdRequisicion = " . $_POST["idp"];

			$stmt = $bddr->prepare($sqlMaterial);
                        $stmt->execute();
                        $result = $stmt->fetchAll(); 

            $to = $DataG[0];   
           // $to = "desarrollo@fusalmo.org" ;                    
            $subject = "Solicitud de Requisición Lic. Iglesias";
            
            $message = "
                <h2>Solicitud de Requisición Aprobada </h2><br />
            
                Nombre del Empleado: <br /> $Requi[17] $Requi[18] $Requi[20] $Requi[21] <br /><br />
                Proyecto<br /> $Requi[13] <br /><br />
                Actividad <br /> $Requi[3] <br /> <br /> 
                Fecha de entrega <br /> $Requi[5] <br /><br />
                Comentarios <br /> $Requi[6] <br/>
                
                <br/> Materiales solicitados : <br/> <br/>
               <table>
               <tr><th>Cantidad</th><th>Material</th><th>Especificación Tecnica</th></tr>";

                
               foreach($result as $row)
                                {
                                  $message .=  "  
                                    <tr><td>{$row['Cantidad']}</td>
                                    <td>{$row['Material']}</td>
                                    <td>{$row['Espe_Tecnica']}</td></tr>
                                    ";
                                }


                $message .= "</table>";
                

            echo  "<br/> Para aceptar o denegar ingresar al siipdb.fusalmo.org <br/> <br/>" ;   
                	
            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org," . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            mail($to,$subject,$message,$headers); 
            Redireccion("../../Empleado.php?l=Request&n=2"); 
          }
             
        }                   
        
         else if($_POST['Enviar'] == "Denegar")
        {
          
        }                   
              
    }

    Redireccion("../../Empleado.php?l=Request&n=4");

?>