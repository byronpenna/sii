<?php


/**
 * @author Lilian Cordero
 * @copyright 2016
 */


  require '../../net.php';
    
   

 if(isset($_POST['Enviar']))
    {    

      
        }     

        if($_POST['Enviar'] == "Crear requisicion")
        {
            $Addrequest = $bddr->prepare("Insert into Request_Requisiciones values (Null, :IdEmpleado, :Idproyecto, :Actividad, :Fecha_Solicitud, '', :Comentario, :Linea_Presupuestaria, :SubLinea_Presupuestaria, 'Alimentacion', 'Pendiente' , '', :idj, '','0' )");
            $Addrequest->bindParam(':IdEmpleado', $_SESSION["IdUsuario"]);
            $Addrequest->bindParam(':Idproyecto', $_POST['proyectoA']);
            $Addrequest->bindParam(':Actividad', $_POST['actividad']);
            $Addrequest->bindParam(':Fecha_Solicitud', date("Y-m-d"));
            $Addrequest->bindParam(':Comentario', $_POST['comentarios']);
            $Addrequest->bindParam(':Linea_Presupuestaria', $_POST['presupuestariaA']);
            $Addrequest->bindParam(':SubLinea_Presupuestaria', $_POST['subpresupuestariaA']);
            $Addrequest->bindParam(':idj', $_POST['idj']);
            $Addrequest->execute();



          //  if ($Addrequest->execute()) {
               // echo "Septumsembra";
           // }
           // else
               // echo "AbadaKadabra";

            $id = $bddr->lastInsertId();

            $fech = isset($_POST['fecha']) ? $_POST['fecha'] : array();
            $hoho = isset($_POST['hora']) ? $_POST['hora'] : array();
            $muni = isset($_POST['municipio']) ? $_POST['municipio'] : array();
            $sed = isset($_POST['sede']) ? $_POST['sede'] : array();
            $direc = isset($_POST['direccion']) ? $_POST['direccion'] : array();
            $boss = isset($_POST['encargado']) ? $_POST['encargado'] : array();
            $canti = isset($_POST['cantidad']) ? $_POST['cantidad'] : array();
            $refrial = isset($_POST['tipo']) ? $_POST['tipo'] : array();
            
           
            for ($index = 0 ; $index < count($fech); $index++) 
            {      

                $Addalimentacion = $bddr->prepare("Insert into Request_Alimentacion values (Null, :IdRequisicion, :Fecha, :Hora, :Municipio, :Sede, :Direccion, :Encargado, :Tipo , :Cantidad, '' )");
                $Addalimentacion->bindParam(':IdRequisicion', $id);
                $Addalimentacion->bindParam(':Fecha', $fech[$index]);
                $Addalimentacion->bindParam(':Hora', $hoho[$index]);
                $Addalimentacion->bindParam(':Municipio', $muni[$index]);
                $Addalimentacion->bindParam(':Sede', $sed[$index]);
                $Addalimentacion->bindParam(':Direccion', $direc[$index]);
                $Addalimentacion->bindParam(':Encargado', $boss[$index]);
                $Addalimentacion->bindParam(':Tipo', $refrial[$index]);
                $Addalimentacion->bindParam(':Cantidad',$canti[$index]);
                $Addalimentacion->execute();
             
           }
            

             $MailEmpleado = $bddr->prepare("Select Correo FROM DatosInstitucionales WHERE IdEmpleado = " . $_POST['idj']);
            $MailEmpleado->execute();
            $DataM = $MailEmpleado->fetch();

				$MisRequisiciones = $bddr->prepare("SELECT 
                                               p.Proyecto, l.LineaPresupuestaria, s.SubLinea, r.Actividad, r.Fecha_Solicitud, r.Fecha_entrega, r.Tipo_Requisicion, 
                                               r.Comentario, r.Comentario_Jefe, 
                                               m.Nombre1, m.Nombre2, m.Apellido1, m.Apellido2, 
                                               j.Nombre1, j.Nombre2, j.Apellido1, j.Apellido2 
                                               FROM Request_Requisiciones as r 
                                               inner join Request_Proyecto as p on r.IdProyecto = p.IdProyecto 
                                               inner join Request_LineaPresupuestaria as l on r.Linea_Presupuestaria = l.IdLinea 
                                               inner join Request_SubLineaPresupuestaria as s on r.SubLinea_Presupuestaria = s.IdSubLineaPresupuestaria  
                                               inner join Empleado AS m on r.IdEmpleado = m.IdEmpleado  
                                               inner join Empleado AS j on r.idjefe = j.IdEmpleado 
                                               where r.Idrequisicion = " . $_POST["idp"]) ;                                               
           $MisRequisiciones->execute();
           $Requi = $MisRequisiciones->fetch();

            //correos
            $to = "$DataM[0]";
          //  $to = "desarrollo@fusalmo.org, liligrc9221@gmail.com";                        
            $subject = "Solicitud de alimentaci&oacuten - ".$_POST['actividad']."";
            
            $message = "
                <h2>Solicitud de Alimentaci&oacuten</h2><br />
            
                Nombre del Empleado: <br /> <td>$Requi[9] $Requi[10] $Requi[11] $Requi[12]</td><br />
                Actividad: <br /> ".$_POST['actividad']." <br /><br />
                Comentarios: <br /> ".$_POST['comentarios']." <br /><br />
                
                Detalles: <br/>
               <table>
               <tr><th><br/>Fecha de entrega</th><th>Hora de entrega</th><th>Municipio</th><th>Sede</th><th>Direcci&oacuten</th><th>Tecnico encargado</th><th>Cantidad</th><th>Almuerzo / refrigerio</th></tr>";
                
                for ($index = 0 ; $index < count($canti); $index++) 
                {    
                    $message .= "
                                <tr>
                                   <th>$fech[$index] </th>
                                   <th>$hoho[$index]</th>
                                   <th>$muni[$index]</th>
                                   <th>$sed[$index] </th>
                                   <th>$boss[$index]</th>
                                   <th>$direc[$index]</th>
                                   <th>$canti[$index]</th>
                                   <th>$refrial[$index]</th>
                                </tr>";
                }
                $message .= "</table>";

                $message .= "<br /><br />Acepta o deniega dando <a href='https://siifusalmo.org/RequisicionView.php?idr=$id'> clic aquí </a>";
                
                  

            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org," . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            mail($to,$subject,$message,$headers);  
           Redireccion("../../Empleado.php?l=Request");
        }
        
        else if($_POST['Enviar'] == "Actualizar")
        {
           
        }    
        
        else if($_POST['Enviar'] == "Eliminar")
        {
          
        }    


        if($_POST['Enviar'] == "Guardar")
                {
                    if($_POST['Decision'] == "Denegar")
                        $estado = "Denegado"; 
                        
                    if($_POST['Decision'] == "Aceptar")
                        $estado = "Aceptado";

            $query = "UPDATE Request_Requisiciones SET Estado = '$estado' , Comentario_Jefe = :Comentario  WHERE IdRequisicion =" . $_POST["idp"];
            $Update = $bddr->prepare($query);   
            $Update->bindParam(':Comentario',$_POST['Comentario']);       
            $Update->execute();


           if($estado == "Aceptado") {

            $query3 = "SELECT di.Correo FROM CargosAsignacion as ca 
                        INNER JOIN DatosInstitucionales as di on ca.IdEmpleado = di.IdEmpleado
                        WHERE ca.IdCargo = 24 AND ca.FechaFin = '0000-00-00' ";
            $Gerencia = $bddr->prepare($query3);   
            $Gerencia ->execute();
            $DataG = $Gerencia->fetch(); 

           $MisRequisiciones = $bddr->prepare("SELECT 
                                               p.Proyecto, l.LineaPresupuestaria, s.SubLinea, r.Actividad, r.Fecha_Solicitud, r.Fecha_entrega, r.Tipo_Requisicion, 
                                               r.Comentario, r.Comentario_Jefe, 
                                               m.Nombre1, m.Nombre2, m.Apellido1, m.Apellido2, 
                                               j.Nombre1, j.Nombre2, j.Apellido1, j.Apellido2 
                                               FROM Request_Requisiciones as r 
                                               inner join Request_Proyecto as p on r.IdProyecto = p.IdProyecto 
                                               inner join Request_LineaPresupuestaria as l on r.Linea_Presupuestaria = l.IdLinea 
                                               inner join Request_SubLineaPresupuestaria as s on r.SubLinea_Presupuestaria = s.IdSubLineaPresupuestaria  
                                               inner join Empleado AS m on r.IdEmpleado = m.IdEmpleado  
                                               inner join Empleado AS j on r.idjefe = j.IdEmpleado 
                                               where r.Idrequisicion = " . $_POST["idp"]) ;
                        $MisRequisiciones->execute();
                        $Requi = $MisRequisiciones->fetch();

           
            $sqlAlimentacion= "SELECT * FROM Request_Alimentacion as a 
                        inner join Request_Requisiciones as o on a.Idrequisicion = o.Idrequisicion
                        where a.IdRequisicion = " . $_POST["idp"];

            $stmt2 = $bddr->prepare($sqlAlimentacion);
                        $stmt2->execute();
                        $result2 = $stmt2->fetchAll();

                  
                   $to = $DataG[0];   
            //$to = "desarrollo@fusalmo.org";                    
            $subject = "Solicitud Aceptada - ".$_POST['actividad']." ";
            
            $message = "
                <h2>Solicitud de Requisici&oacuten Aceptada por Jefe Inmediato</h2><br />
                <br />$DataG[0]<br />
                <table style='width: 70%'>
                    <tr>
                        <td style='width: 30%'>Proyecto: </td>
                        <td>$Requi[0]</td>
                    </tr>
                    <tr>
                        <td>Linea Presupuestaria: </td>
                        <td>$Requi[1]</td>
                    </tr>  
                    <tr>
                        <td>Sublínea presupuestaria: </td>
                        <td>$Requi[2]</td>
                    </tr>
                    <tr><td colspan='2'><br /></td></tr>  
                    <tr>
                        <td>Actividad: </td>
                        <td>$Requi[3]</td>
                    </tr>                                             
                    <tr>
                        <td>Fecha de solicitada: </td>
                        <td>$Requi[4]</td>
                    </tr> 
                    <tr>
                        <td>Fecha de entrega: </td>
                        <td>$Requi[5]</td>
                    </tr>
                    <tr><td colspan='2'><br /></td></tr>                     
                    <tr>
                        <td>Empleado Solicitante: </td>
                        <td>$Requi[9] $Requi[10] $Requi[11] $Requi[12]</td>
                    </tr>                     
                    <tr>
                        <td>Comentarios: </td>
                        <td>$Requi[7]</td>
                    </tr>       
                    <tr><td colspan='2'><br /></td></tr>              
                    <tr>
                        <td>Visto bueno por: </td>
                        <td>$Requi[13] $Requi[14] $Requi[15] $Requi[16]</td>
                    </tr> 
                    <tr>
                        <td>Comentarios Jefe: </td>
                        <td>$Requi[8]</td>
                    </tr>                                                                                                                                                                                    
                </table>
                <br /><br />
               <table style='width: 50%' rules='all'>
             
                       <br/> Detalles: <br/><br/>
                       <tr><th><br/>Fecha de entrega</th><th>Hora de entrega</th><th>Municipio</th><th>Sede</th><th>Direcci&oacuten</th><th>Tecnico encargado</th><th>Cantidad</th><th>Almuerzo / refrigerio</th></tr>";

                        
                       foreach($result2 as $row2)
                                {
                                     $message .= " 
                                    <tr><td>{$row2['Fecha']}</td>
                                    <th>{$row2['Hora']}</th>
                                    <th>{$row2['Municipio']}</th>
                                    <th>{$row2['Sede']}</th>
                                    <th>{$row2['Direccion']}</th>
                                    <th>{$row2['Encargado']}</th>
                                    <th>{$row2['Cantidad']}</th>
                                    <th>{$row2['Tipo']}</th></tr>
                                    ";
                                }

                         $message .= "</table>";
                          $message .=  "<br /><br />Acepta o deniega dando <a href='https://siifusalmo.org/RequisicionApproval.php?idr=".$_POST["idp"]."'> clic aquí </a>" ; 
                          

                    // More headers
                    $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
                    $headers .= "Cc: desarrollo@fusalmo.org," . "\r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    
                    mail($to,$subject,$message,$headers);  
                    Redireccion("../../Empleado.php?l=Request&n=2");
                }
         }


      if($_POST['Enviar'] == "archivar")
        {
            echo "Entro";
            echo "<br />";
            if($_POST['Decision'] == "Rechazar")
                $estado = "Rechazado"; 
                
            if($_POST['Decision'] == "Aprobar")
                $estado = "Aprobado";

            $query = "UPDATE Request_Requisiciones SET Estado = '$estado' , Comentario_finanzas = :Comentario  WHERE IdRequisicion =" . $_POST["idp"];
            $Update = $bddr->prepare($query);   
            $Update->bindParam(':Comentario',$_POST['Comentario']);       
            $Update->execute();

            $id = $bddr->lastInsertId();
            $materup = isset($_POST['Cantidad_aprobada']) ? $_POST['Cantidad_aprobada'] : array();
            $materid = isset($_POST['IdAlimentacion']) ? $_POST['IdAlimentacion'] : array();
            
            for ($index = 0 ; $index < count($materup); $index++) 
            {   
                $qute = "UPDATE Request_Alimentacion SET Cantidad_aprobada = $materup[$index] WHERE IdAlimentacion = $materid[$index]";
                $newmaterial = $bddr->prepare($qute);
                //$newmaterial->bindParam(':Cantidad_aprobada', $materup[$index]);
                $newmaterial->execute();
                echo "<br />";
            }

            $id = $bddr->lastInsertId();
            $req = isset($_POST['IdAlimentacion']) ? $_POST['IdAlimentacion'] : array();
                        //$precio = isset($_POST['Costo']) ? $_POST['Costo'] : array();
            $canti = isset($_POST['Cantidad_aprobada']) ? $_POST['Cantidad_aprobada'] : array();
            //$provee = isset($_POST['Proveedor']) ? $_POST['Proveedor'] : array();        
            //$comment = isset($_POST['Comentario']) ? $_POST['Comentario'] : array(); 
            $line = isset($_POST['sublinea']) ? $_POST['sublinea'] : array();



           for ($index = 0 ; $index < count($canti); $index++) 
            {                                
               $Addseg = $bddr->prepare("INSERT INTO Request_SeguimientoAli VALUES (Null, :Id_Alimentacion, '', :Cantidad_aprobada, '', :IdSubLineaPresupuestaria)");

               $Addseg->bindParam(':Id_Alimentacion', $req[$index]);
               $Addseg->bindParam(':Cantidad_aprobada', $canti[$index]);
               $Addseg->bindParam(':IdSubLineaPresupuestaria',$line[$index]);
               $Addseg->execute();
                
                
            }


            //if ($Addseg->execute()) 
               // echo "Je T'aime Lili";
            
            //else
               // echo "AbadaKadabra";
            
             $MisRequisiciones = $bddr->prepare("SELECT 
                                               p.Proyecto, l.LineaPresupuestaria, s.SubLinea, r.Actividad, r.Fecha_Solicitud, r.Fecha_entrega, r.Tipo_Requisicion, 
                                               r.Comentario, r.Comentario_Jefe, 
                                               m.Nombre1, m.Nombre2, m.Apellido1, m.Apellido2, 
                                               j.Nombre1, j.Nombre2, j.Apellido1, j.Apellido2 
                                               FROM Request_Requisiciones as r 
                                               inner join Request_Proyecto as p on r.IdProyecto = p.IdProyecto 
                                               inner join Request_LineaPresupuestaria as l on r.Linea_Presupuestaria = l.IdLinea 
                                               inner join Request_SubLineaPresupuestaria as s on r.SubLinea_Presupuestaria = s.IdSubLineaPresupuestaria  
                                               inner join Empleado AS m on r.IdEmpleado = m.IdEmpleado  
                                               inner join Empleado AS j on r.idjefe = j.IdEmpleado 
                                               where r.Idrequisicion = " . $_POST["idp"]) ;                                               
           $MisRequisiciones->execute();
           $Requi = $MisRequisiciones->fetch();
          
            $to = "edgar.avalos@fusalmo.org, walter.hernandez@fusalmo.org";                    
            $subject = "Solicitud de Requisici&oacuten de Alimentacion Aprobada - ".$_POST['actividad']."";
            
            $message = "
                <h2>Solicitud de Requisici&oacuten Aprobada por Compras</h2><br />
                <br />$DataG[0]<br />
                <table style='width: 70%'>
                    <tr>
                        <td style='width: 30%'>Proyecto: </td>
                        <td>$Requi[0]</td>
                    </tr>
                    <tr>
                        <td>Linea Presupuestaria: </td>
                        <td>$Requi[1]</td>
                    </tr>  
                    <tr>
                        <td>Sublínea presupuestaria: </td>
                        <td>$Requi[2]</td>
                    </tr>
                    <tr><td colspan='2'><br /></td></tr>  
                    <tr>
                        <td>Actividad: </td>
                        <td>$Requi[3]</td>
                    </tr>                                             
                    <tr>
                        <td>Fecha de solicitada: </td>
                        <td>$Requi[4]</td>
                    </tr> 
                    <tr>
                        <td>Fecha de entrega: </td>
                        <td>$Requi[5]</td>
                    </tr>
                    <tr><td colspan='2'><br /></td></tr>                     
                    <tr>
                        <td>Empleado Solicitante: </td>
                        <td>$Requi[9] $Requi[10] $Requi[11] $Requi[12]</td>
                    </tr>                     
                    <tr>
                        <td>Comentarios: </td>
                        <td>$Requi[7]</td>
                    </tr>       
                    <tr><td colspan='2'><br /></td></tr>              
                    <tr>
                        <td>Visto bueno por: </td>
                        <td>$Requi[13] $Requi[14] $Requi[15] $Requi[16]</td>
                    </tr> 
                    <tr>
                        <td>Comentarios Jefe: </td>
                        <td>$Requi[8]</td>
                    </tr>                                                                                                                                                                                    
                </table>
                <br /><br />
               <table style='width: 50%' rules='all'>
                <br/> Detalles: <br/><br/>
                       <tr><th><br/>Fecha de entrega</th><th>Hora de entrega</th><th>Municipio</th><th>Sede</th><th>Direcci&oacuten</th><th>Tecnico encargado</th><th>Cantidad</th><th>Almuerzo / refrigerio</th></tr>";

                        $MisRequisiciones = $bddr->prepare("SELECT * FROM Request_Requisiciones as r 
                                                            inner join Request_Proyecto as p on r.IdProyecto = p.IdProyecto 
                                                            inner join Empleado AS m on r.IdEmpleado = m.IdEmpleado
                                                           " ) ;
                        $MisRequisiciones->execute();
                        $Requi = $MisRequisiciones->fetch();

           
            $sqlAlimentacion= "SELECT * FROM Request_Alimentacion as a 
                        inner join Request_Requisiciones as o on a.Idrequisicion = o.Idrequisicion
                        where a.IdRequisicion = " . $_POST["idp"];

            $stmt2 = $bddr->prepare($sqlAlimentacion);
                        $stmt2->execute();
                        $result2 = $stmt2->fetchAll();

                       foreach($result2 as $row2)
                                {
                                     $message .= " 
                                    <tr><td>{$row2['Fecha']}</td>
                                    <th>{$row2['Hora']}</th>
                                    <th>{$row2['Municipio']}</th>
                                    <th>{$row2['Sede']}</th>
                                    <th>{$row2['Direccion']}</th>
                                    <th>{$row2['Encargado']}</th>
                                    <th>{$row2['Cantidad']}</th>
                                    <th>{$row2['Tipo']}</th></tr>
                                    ";
                                }
               $message .=  "<br /><br />Miralo en el siguiente enlace <a href='https://siifusalmo.org//Empleado.php?l=requiemaprove'> clic aquí </a>" ; 
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org" . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
             mail($to,$subject,$message,$headers); 
          Redireccion("../../Empleado.php?l=Request&n=2"); 
        }
             
          if($_POST['Enviar'] == "Eliminar")
        {

            
           $query = ("DELETE FROM Request_Requisiciones WHERE Idrequisicion =" . $_POST["idp"]);
            $delete = $bddr->prepare($query);  
            ECHO $query;        
            $delete->execute();

            Redireccion("../../Empleado.php?l=Request");           
    }

    Redireccion("../../Empleado.php?l=Request&n=2");
?>