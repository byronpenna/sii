    <?php
    if(!isset($_SESSION["autenticado"])){
        require '../../../net.php';
    } ?>
    
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Estadisticas de proyectos</title>
</head>
<script src="https://code.highcharts.com/highcharts.js"></script>
<body>

     <div style="float:right; width: 75%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="7"><h2 style="color: #197198;">Estadisticas de Requisiciones</h2></td></tr>
        <tr><td colspan="7"><hr color='skyblue' /></td></tr>
        <tr>


     <td style="width: 20%;"><a href="?l=Stats">
                <input type="submit" value="Vista General" name="Enviar" class="botonG" style="width: 105px;" /></a>
               
     </td>
      <td style="width: 20%;"><a href="?l=Statsproy">
                <input type="submit" value="Vista por proyecto" name="Enviar" class="botonG" style="width: 125px;" /></a>
               
     </td>

      <td style="width: 20%;"><a href="?l=Statsarea">
                <input type="submit" value="Vista por area" name="Enviar" class="botonG" style="width: 105px;" /></a>
               
     </td>

</tr>
</table>
<script src="Highcharts-6.1.0/code/highcharts.js"></script>
<script src="Highcharts-6.1.0/code/modules/exporting.js"></script>
<script src="Highcharts-6.1.0/code/modules/export-data.js"></script>

<?php


        echo "<form action=\"?l=Statsproy\" method=\"POST\">";
        echo "</br>Elija el proyecto";
        $sql = $bddr->prepare("SELECT IdProyecto, Proyecto FROM Request_Proyecto");
        $sql->execute();
        echo "<br><select name=\"Proyecto\">";
        foreach ($sql as $key => $value) {
                echo "<option value=\"$value[0]\">$value[1]</option>";
             }
        echo "</select>";
        echo "<br>filtrar por fecha <br><input type=\"date\" name=\"inicio\"> <input type=\"date\" name=\"fin\">";
        echo "<input type=\"submit\" value=\"ver proyecto\"/>";
        echo "</form>";


if (isset($_POST["Proyecto"])){

    $where = "";
    $date = "";
    if ($_POST["inicio"] != "" && $_POST["fin"] != ""){
        $where = " AND Fecha_Solicitud BETWEEN '".$_POST["inicio"]."' AND '".$_POST["fin"]."'";
        $date = " (".$_POST['inicio']." / ".$_POST['fin'].")";
    }    
        
    try {
        //realizadas
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Materiales' AND Idproyecto = ".$_POST["Proyecto"].$where);
            $sql->execute();
            $M_a = $sql->rowCount();
            //aceptados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Materiales' AND Estado = 'Aceptado' AND Idproyecto = ".$_POST["Proyecto"].$where);
            $sql->execute();
            $M_b = $sql->rowCount();
            //rechazados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Materiales' AND Estado = 'Denegado' AND Idproyecto = ".$_POST["Proyecto"].$where);
            $sql->execute();
            $M_c = $sql->rowCount();
            //entregados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Materiales' AND Estado = 'Entregado' AND Idproyecto = ".$_POST["Proyecto"].$where);
            $sql->execute();
            $M_d = $sql->rowCount();
            //pendientes
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Materiales' AND Estado = 'Pendiente' AND Idproyecto = ".$_POST["Proyecto"].$where);
            $sql->execute();
            $M_e = $sql->rowCount();


        //realizadas
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Alimentacion' AND Idproyecto = ".$_POST["Proyecto"].$where);
            $sql->execute();
            $A_a = $sql->rowCount();
            //aceptados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Alimentacion' AND Estado = 'Aceptado' AND Idproyecto = ".$_POST["Proyecto"].$where);
            $sql->execute();
            $A_b = $sql->rowCount();
            //rechazados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Alimentacion' AND Estado = 'Denegado' AND Idproyecto = ".$_POST["Proyecto"].$where);
            $sql->execute();
            $A_c = $sql->rowCount();
            //entregados
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Alimentacion' AND Estado = 'Entregado' AND Idproyecto = ".$_POST["Proyecto"].$where);
            $sql->execute();
            $A_d = $sql->rowCount();
            //pendientes
            $sql = $bddr->prepare("SELECT * from Request_Requisiciones WHERE Tipo_Requisicion = 'Alimentacion' AND Estado = 'Pendiente' AND Idproyecto = ".$_POST["Proyecto"].$where);
            $sql->execute();
            $A_e = $sql->rowCount();


            //nombre del proyecto
            $sql = $bddr->prepare("SELECT Proyecto from request_proyecto WHERE IdProyecto = ". $_POST["Proyecto"]);
            $sql->execute();
            $P = $sql->fetch()[0];
        
            $bddr = null;
            $sql = null;
        } catch (PDOException $e) {
            print "¡Error!: " . $e->getMessage() . "<br/>";
            die();
        }
}

echo "<div id=\"container\" style=\"min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto\"></div>";
?>

</div>
</body>
</html>

<script type="text/javascript">

Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Estadistica de Requisiciones'
    },
    subtitle: {
        text: <?php echo "'".$P.$date."'";  ?>
    },
    xAxis: {
        categories: ['Alimentacion', 'Materiales'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Cantidad de requisiciones',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Requisiciones'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Realizadas',
        data: [<?php echo "$A_a, $M_a"; ?>]
    }, {
        name: 'Aceptados',
        data: [<?php echo "$A_b, $M_b"; ?>]
    }, {
        name: 'Denegados',
        data: [<?php echo "$A_c, $M_c"; ?>]
    }, {
        name: 'Entregados',
        data: [<?php echo "$A_d, $M_d"; ?>]
    }, {
        name: 'Pendientes',
        data: [<?php echo "$A_e, $M_e"; ?>]
    }]
});
</script>



</body>
</html>