<?php

/**
 * @author Manuel Calder�n
 * @copyright 2013
 */

require '../../net.php';

if(isset($_POST['Enviar']))
{    
    if($_POST['Enviar'] == "Crear")
    {
        $Insert = $bddr->prepare("Insert into Request_Proyecto values(null, :name, :finances, :description, '2018')");
        $Insert->bindParam(":name", $_POST['proyecto']);
        $Insert->bindParam(":finances", $_POST['nFinanciadora']);
        $Insert->bindParam(":description", $_POST['descripcion']);
        $Insert->execute();
        
        Redireccion("../../Empleado.php?l=proyectos");
    }
    if($_POST['Enviar'] == "Crear L�nea")
    {
        
        $Insert = $bddr->prepare("INSERT INTO Request_LineaPresupuestaria VALUES (NULL, :ip, :linea)");
        $Insert->bindParam(":ip", $_POST['ip']);
        $Insert->bindParam(":linea", $_POST['Linea']);
        $Insert->execute();
                
        echo "<body onload='javascript:document.line.submit();' />
                <form method='post' action='../../Empleado.php?l=BudgetLine' name='line'>
                    <input type='hidden' name='ip' value='".$_POST['ip']."'>
	            </form>
              </body>";
    }

    if($_POST['Enviar'] == "Crear SubL�nea")
    {
        $Insert = $bddr->prepare("INSERT INTO Request_SubLineaPresupuestaria VALUES (NULL , :Lineapresupuestaria, :SubLinea, :Monto)");
        $Insert->bindParam(":Lineapresupuestaria", $_POST['lien']);
        $Insert->bindParam(":SubLinea", $_POST['subLinea']);
        $Insert->bindParam(":Monto", $_POST['subMonto']);
        $Insert->execute();        
            
        echo "<body onload='javascript:document.line.submit();' />
                <form method='post' action='../../Empleado.php?l=BudgetLine' name='line'>
                    <input type='hidden' name='ip' value='".$_POST['ip']."'>
	            </form>
              </body>";         
    }
    if($_POST['Enviar'] == "Guardar �reas")
    {
        $Delete = $bddr->prepare("Delete from Request_ProyectoArea where IdProyecto = :idp");
        $Delete->bindParam(':idp',$_POST['ip']);
        $Delete->execute();
        
        $Area = isset($_POST['Area']) ? $_POST['Area'] : array(); 
        for ($index = 0 ; $index < count($Area); $index++) 
        {                                
            $AddMaterial = $bddr->prepare("INSERT INTO Request_ProyectoArea values (Null, :idp, :ida )");
            $AddMaterial->bindParam(':idp',$_POST['ip']);
            $AddMaterial->bindParam(':ida', $Area[$index]);
            $AddMaterial->execute();
        }
        
        echo "<body onload='javascript:document.line.submit();' />
                <form method='post' action='../../Empleado.php?l=BudgetLine' name='line'>
                    <input type='hidden' name='ip' value='".$_POST['ip']."'>
	            </form>
              </body>";         
    }
    if($_POST['Enviar'] == "Guardar")
    {
        $query = ("UPDATE Request_Proyecto SET Proyecto = :Proyecto, Institucion_Financiadora = :Institucion, Descripcion = :Descripcion WHERE IdProyecto = :idp");
        $Update = $bddr->prepare($query);
        $Update->bindParam(':idp',$_POST['idp']);
        $Update->bindParam(':Proyecto',$_POST['proyecto']);       
        $Update->bindParam(':Institucion',$_POST['institucion']);       
        $Update->bindParam(':Descripcion',$_POST['descripcion']);       
        $Update->execute();


        Redireccion("../../Empleado.php?l=proyectos");
    }

     if($_POST['Enviar'] == "Actualizar")
    {

            $Rowspan = isset($_POST['rowspan']) ? $_POST['rowspan'] : array();
            $line = isset($_POST['IdLineaPresupuestaria']) ? $_POST['IdLineaPresupuestaria'] : array();
            
            for ($index = 0 ; $index < count($Rowspan); $index++) 
            {   
                $qute = "UPDATE Request_LineaPresupuestaria SET  LineaPresupuestaria = '$Rowspan[$index]' WHERE IdLinea = $line[$index]";
                //  echo "$qute";
                $newline = $bddr->prepare($qute);
                $newline->execute();
                echo "<br />";
            }


                        
            $subdos = isset($_POST['subs2']) ? $_POST['subs2'] : array();
            $subtres = isset($_POST['subs3']) ? $_POST['subs3'] : array();
            $linedos = isset($_POST['IdSubLineaPresupuestaria']) ? $_POST['IdSubLineaPresupuestaria'] : array();
            
            for ($index = 0 ; $index < count($subdos); $index++) 
            {   
                $qute2 = "UPDATE Request_SubLineaPresupuestaria SET  SubLinea = '$subdos[$index]' ,  Monto = '$subtres[$index]' WHERE IdSubLineaPresupuestaria = $linedos[$index]";
                 // echo "$qute2";
                $newline2 = $bddr->prepare($qute2);
                $newline2->execute();
                echo "<br />";
            }

        Redireccion("../../Empleado.php?l=proyectos");
    }



}
?>