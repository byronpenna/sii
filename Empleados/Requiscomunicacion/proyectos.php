<?php

/**
 * @author Lili Cordero
 * @copyright 2016
 */
              
$MisCargos = $bddr->prepare("SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo
                            FROM CargosAsignacion AS ca
                            INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                            INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
                            WHERE ca.IdEmpleado = '".$_SESSION["IdUsuario"]."'");      
                                                                  
$MisCargos->execute();

if(isset($_POST['MiCargo']))
    $IdCharge = $_POST['MiCargo'];

else
    $IdCharge = ''; 
    
$Cargos = "";
$Datos = $MisCargos->fetch();
?>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="2">M&oacutedulo de Requisiciones</td></tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td style="width: 50%; text-align: left;"><a href="?l=Request"><input type="submit" value="<- Atras" name="Enviar" class="boton"  /></a></td>
            <td style="width: 50%; text-align: right;"><h2>Proyectos</h2></td>
        </tr>
        <tr>
            <td colspan="2"><hr color='skyblue' /></td>
        </tr>

<?php
        	$Accion = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado
                                      FROM Empleado AS e
                                      INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = e.IdEmpleado
                                      INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
                                      INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
                                      where ac.IdAsignacion = $Datos[1]");                                            
            $Accion->execute();
            $DatosEmple = $Accion->fetch(); 
?>        
        <tr>
            <td colspan="2">
                <table style="width: 80%;">
                    <input type="hidden" name="NombreEmpleado" value="<?php echo "$DatosEmple[0] $DatosEmple[1] $DatosEmple[2] $DatosEmple[3]"?>" />
                    <tr><td  style="width: 30%;">Nombre del Empleado: </td><td><?php echo "$DatosEmple[0] $DatosEmple[1] $DatosEmple[2] $DatosEmple[3]"?></td></tr>
                    <tr><td >Cargo Asignado:               </td><td><?php echo "$DatosEmple[5]"?></td></tr>
                    <tr><td >Area de trabajo:                </td><td><?php echo "$DatosEmple[6]"?></td></tr>
                    <tr><td></td><td><?php echo $localidad?></td></tr>
                </table>
            </td>
        </tr>
        <td colspan="2"><hr color='skyblue' /></td>

        <tr>
            <td colspan="2">
                <table style="width: 100%;text-align: left;">
                    <tr>
                        <th style="width: 40%;">Proyecto</th>
                        <th style="width: 40%;">Instituci�n Financiadora</th>
                        <th style="width: 10%;">A�o</th>
                        <th style="width: 10%;">
                            <a href="?l=ProyectForm" ><input type="button" value="+" class='botonG' style="width: 40px;" /></a>
                        </th>
                    </tr>
                <?php
	               
                   $Proyectos = $bddr->prepare("SELECT * FROM Request_Proyecto");
                   $Proyectos->execute();
                   
                   if($Proyectos->rowCount() > 0)
                   {
                        while($DataP = $Proyectos->fetch())
                        {
                            echo "<tr>
                                      <td>$DataP[1]</td><td>$DataP[2]</td><td>$DataP[4]</td>
                                      <td>
                                        <form action='?l=BudgetLine' method='post'>
                                            <input type='hidden' name='ip' value='$DataP[0]' />
                                            <input type='submit' value='Presupuesto' class='botonG' />
                                        </form>
                                      </td>
                                  </tr>";
                        }                    
                   }
                   else
                   {
                        echo "<tr><th colspan='4' style='color: red '>No hay proyectos registrados.</th></tr>";
                   }
                   
                   
    
                ?>

                </table>
                
            </td>
        </tr>
    </table>

</div>

   