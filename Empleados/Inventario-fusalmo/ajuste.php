<!--
  @author Alejandra Martinez
  @copyright 2016
-->


<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="js/jquery.js"></script>  <!--Llama al archivo .js donde estan las funciones  -->
<script type="text/javascript">


/* Esta funcion permite enviar el id del producto junto con una variable a la pagina inventario_controller que es la encargada de eliminar
	 el material de id enviado; la funcion borrar es para el activo fijo y la funcion borrar2 es para el inventario de oficina*/
	function borrar(Correlativo_A)
{

       window.location.assign("Empleados/Inventario-fusalmo/inventario_controller.php?accion=1&id="+Correlativo_A);

}

function borrar2(Correlativo_O)
{

       window.location.assign("Empleados/Inventario-fusalmo/inventario_controller.php?accion=2&id="+Correlativo_O);

}
/* Esta funcion permite enviar el id del producto junto con una variable a la pagina controller_update que es la encargada de modificar un campo
	 del material correspondiente al id enviado; la funcion editar es para el activo fijo y la funcion editar2 es para el inventario de oficina*/
function editar(Correlativo_A)
{

       window.location.assign("Empleados/Inventario-fusalmo/Vista_Actualizar.php?accion=1&id="+Correlativo_A);

}

function editar2(Correlativo_O)
{

       window.location.assign("Empleados/Inventario-fusalmo/Vista_Actualizar.php?accion=2&id="+Correlativo_O);

}
</script>

</script>
</head>



			<div style="float:right; width: 75%; text-align: left; background-color: white; padding:10px"> <!--Es el tamaño del contenedor -->
            <td colspan="2"><hr color='skyblue' /></td>

             <TABLE style=" padding-left: 9px" >
			 <td><h1>Ajustes</h1></td> <!--Encabezado de la pàgina -->

			 </TABLE>
             <tr><td colspan="2"><hr color='skyblue' /></td></tr>


		         <div style="overflow:scroll;height:500px;width:700px;">  <!--Permite las barras movibles -->
		             <div style="width:300px;">
		                 <td><input type='button' class='boton' value='Atras' name='atras' onclick='history.back()'	/></td> <!--Botòn que regresa a pàgina anterior -->
		 			    <TABLE  style="padding-left: 0; padding-top: 15px;     overflow: scroll;"  >

		 			 	    <div id="container">

		 						<!--Pestaña 1 activa por defecto-->
		 						<input id="tab-1" type="radio" name="tab-group" checked="checked" style="visibility: hidden" />

		 						<div id="content">

		 						<!--Contenido de la Pestaña 1-->
		 						<div id="content-1">



<?php

			include("Conexion.php"); //Instancia a la página de conexion que contiene los datos de la conexión

			$con = mysqli_connect($host,$user,$pw,$db) // se realiza la conexión
			or die("problemas al conectar server");



			$color_row=array('#cccccc', 'lightblue'); // Le dá color a las filas de las tablas
			$ind_color=0;




		$combo = $_POST['lista_inv']; // variable bandera que se utiliza para realizar las operaciones en el switch
		switch($combo)
		{
			case 'Af':
				//hace la consulta a la tabla del inventario activo fijo
							$consulta1 = "SELECT   Id_Activo, Correlativo_A, Descripcion, Valor_Adquisicion, Fecha_Adquisicion, Vida_Util, Id_Bodega FROM Inventario_Activo";
							$tabla1='si'; // llama a la tabla 1  que es la del inventario activo fijo
							$tabla2='no'; // deshabilita la tabla2 que es la del inventario de oficina
			break;

			case 'Of':
			// hace la consulta del inventario de oficina
							$consulta2=("SELECT *FROM Inventario_Oficina");
							$tabla1='no'; // deshabilita la tabla 1 que es la del activo fijo
							$tabla2='si'; // habilita la tabla 2 que es la del inventario de oficina
			break;
		}

//Esta zona hace la impresion de las tablas
		if($tabla1=='si')
				{

								// realiza un registro con la consulta y la conexion
								$registro4 = mysqli_query($con,$consulta1)
								or die ("problemas en consulta:".mysqli_error($con));

										//imprime los encabezados de la tabla de activo fijo
							echo "<table border=1 align='center'>";
							echo "INVENTARIO ACTIVO FIJO ";
							echo " <tr><tr></tr>
							<td>CORRELATIVO</td>
							<td>ID</td>
							<td>DESCRIPCION</td>
							<td>VALOR ADQUISICI&OACUTEN</td>
							<td>FECHA DE ADQUISICIÓN</td>
							<td>VIDA UTIL(MESES)</td>
							<td>EDITAR</td>
							<td>ELIMINAR</td>

							  </tr>
							  ";
								// con un for each se hace el llenado de la tabla
							foreach($registro4 as $Id_Activo=>$key)
						{
							// variables que le dan colores a las filas
							$ind_color++;
							$ind_color %= 2;

							echo "<tr bgcolor=${color_row[$ind_color]}>";
							echo"<td>" .$key['Id_Activo']."</td>";
							echo"<td>" .$key['Correlativo_A']."</td>";
							echo"<td>" .$key['Descripcion']."</td>";
							echo"<td>" .$key['Valor_Adquisicion']."</td>";
							echo"<td>" .$key['Fecha_Adquisicion']."</td>";
							echo"<td>" .$key['Vida_Util']."</td>";
							echo"<td> <input type='button' name='Btn_mod' value='Editar' OnClick='editar(".$key['Id_Activo'].");'></button></td>"; //boton que hace el llamado al script de editar
							echo"<td> <button type='button' name='Btn_eleminar' value='Eliminar' OnClick='borrar(".$key['Id_Activo'].");'>Eliminar</button></td>"; // boton que hace el llamado al script borrar
							echo "</tr>";

						}

				}
						if($tabla2=='si')
						{
							// realiza un registro con la consulta y la conexion
							$registro_oficina2 = mysqli_query($con,$consulta2)
							or die ("problemas en consulta:".mysqli_error($con));

								//imprime los encabezados de la tabla del inventario de oficina
							echo "<table border=1 align='center'>";
							echo "<tr>INVENTARIO OFICINA <tr/>";
							echo " <tr><tr></tr>
							<td>CORRELATIVO</td>
								<td>ID</td>
								<td>DESCRIPCION</td>
								<td>CANTIDAD</td>
								<td>ENTRADA</td>
								<td>SALIDA</td>
								<td>CANTIDAD DISPONIBLE</td>
								<td>MARCA</td>
								<td>MODELO</td>
								<td>FECHA DE ADQUISICIÓN</td>
								<td>LUGAR DE COMPRA</td>
								<td>PRECIO COMPRA</td>
								<td>VALOR ESTIMADO</td>
								<td>BODEGA</td>
							  <td>EDITAR</td>
							  <td>ELIMINAR</td>

							  </tr>
							  ";
									// con un for each se hace el llenado de la tabla
							foreach($registro_oficina2 as $Id_Oficina=>$key)
							{
								// variables que le dan colores a las filas
								$ind_color++;
								$ind_color %= 2;

								echo "<tr bgcolor=${color_row[$ind_color]}>";
								echo"<td>" .$key['Id_Oficina']."</td>";
								echo"<td>" .$key['Correlativo_O']."</td>";
								echo"<td>" .$key['Descripcion']."</td>";
								echo"<td>" .$key['Inventario']."</td>";
								echo"<td>" .$key['Entrada']."</td>";
								echo"<td>" .$key['Salida']."</td>";
								echo"<td>" .$key['Cantidad_Disponible']."</td>";
								echo"<td>" .$key['Marca']."</td>";
								echo"<td>" .$key['Modelo']."</td>";
								echo"<td>" .$key['Fecha_Compra']."</td>";
								echo"<td>" .$key['Lugar_Compra']."</td>";
								echo"<td>" .$key['Precio_Compra']."</td>";
								echo"<td>" .$key['Valor_Estimado']."</td>";
								echo"<td>" .$key['Id_Bodega']."</td>";
								echo"<td> <input type='button' name='Btn_mod' value='Editar' OnClick='editar2(".$key['Id_Oficina'].");'> </td>"; //boton que hace el llamado al script de editar
								echo"<td> <button type='button' name='Btn_eleminar' value='Eliminar' OnClick='borrar2(".$key['Id_Oficina'].");'>Eliminar</button></td>"; // boton que hace el llamado al script borrar

								echo "</tr>";
							}

						}



?>
</div>
</div>
</div>



</TABLE>
</div>
</div>
</div>
</html>
