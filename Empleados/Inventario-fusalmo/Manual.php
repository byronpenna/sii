

<?php

/**
 * @author Marcela Lopez
 * @copyright  2016
 */



?>

<div style="float:right; width: 77%; text-align: left; background-color: white; padding:10px">
	<table style="width: 100%;">

		<tr><td><h2 style="color: #197198;"> M&oacutedulo de Inventario    </h2></td></tr>
		<tr><td colspan="2"><hr color='skyblue'/>  </td></tr>
		<tr><td style="width: 50%; text-align: left;"><input type="button" value="<- Atras" name="Enviar" onclick="history.back()" /></a></td></tr>  
		<tr><td colspan="2"><hr color='skyblue'/>  </td></tr>

		<table >
     		<tr>
     		</tr>
     	</table>

		<tr><td colspan="2"><h3 style = "color: black"> Manual de Ayuda</h3></td></tr>	
		<tr><td>
			A continuacion se le mostraran algunos de los posibles problemas que tiene con el Manejo de Inventario y sugerencias para resolverlos:</td><br><br>

		</tr>	
		<table>
		<tr>
			Problemas mas frecuentes:<br><br>
		</tr>
		<tr>
			<h4 style="color: black">1. Tipos de Datos: </h4> Los datos que se ingresan en cada una de las casillas que se encuentran en el presente Modulo de Inventario, cuentan con un tipo de dato en especifico, es por ello que es probable que se encuentre con algunas dificultades en el momento de que estos estan siendo enviado a su respectivo destino ya que una vez estos no coincidan con el formato que se debe seguir, no se podran ingresar de manera adecuada.<br><br>
		</tr>
		<tr>
			Siga las instrucciones y la secuencia de pasos asi como tambien las plantillas de los formatos que estan colocadas en cada una de las casillas para no volver a tener este tipo de errores<br><br>
		</tr>
		<tr>
			<h4 style = "color: black">2. Ingreso de Fechas: </h4> Las fechas de Adquisicion deben ser posteriores a las actuales, ya que por medio de esto se calcularan distintas oeraciones que le permite al usuario recibir en su base de datos sobre las depreciaciones de los recursos que se tienen. No seran validas fechas que sobrepasen la fecha actual.<br><br>

		</tr>
		<tr>
			<h4 style = "color: black">3. Ajustes de Inventraio:  </h4> Los ajustes que se realizaran por medio del apartado que se encuentra en la seccion de Busqueda de Inventario, le permite al usuario poder modificar e eliminar los distintos registros que se encuentran dentro de la base de datos instantaneamente. La opcion modificar nos asegura poder alterar la existencia de los registros pero un campo a la vez. La opcion Eliminar permite eliminar la fila del registro previamente ingresado, es decir que se eliminan todos los campos pertenecientes a ese registro al mismo tiempo.  <br><br>
		</tr>
		</table>


    </table>
</div>