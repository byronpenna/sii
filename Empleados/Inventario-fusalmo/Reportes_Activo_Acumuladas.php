<?php
/**
 * @author Marcela Lopez
 * @copyright  2016
 */

	//Genera el reporte de las depreciaciones acumuladas de activo fijo

	require('fpdf/fpdf.php');

	class PDF extends FPDF
	{

		/*	function AcceptPageBreak()
		{
			$this->Addpage();
			$this->SetFillColor(232,232,232);
			$this->SetFont('Arial','B',12);
			$this->SetY(40);
			$this->SetX(10);
			$this->Cell(20,6,'ID',1,0,'C',1);
			$this->SetX(30);
			$this->Cell(30,6,'Correlativo',1,0,'C',1);
			$this->SetX(60);
			$this->Cell(40,6,'Descripcion',1,0,'C',1);
			$this->SetX(100);
			$this->Cell(30,6,'Inventario',1,0,'C',1);
			$this->SetX(130);
			$this->Cell(30,6,'Entrada',1,0,'C',1);
			$this->SetX(160);
			$this->Cell(30,6,'Salida',1,0,'C',1);
			
			$this->Ln();
		}*/

		// Cabecera de página
		function Header()
		{
			// Logo
			$this->Image('images/descarga.png',10,8,33);
			// Arial bold 15
			$this->SetFont('Arial','B',15);
			// Movernos a la derecha
			$this->Cell(80);
			// Título
			$this->Cell(30,10,'FUNDACION SALVADOR DEL MUNDO (FUSALMO)',0,0,'C');
			$this->Cell(40);
			
			$this->Cell(60,30,' Inventario de Activo Fijo ',0,0,'C');
			// Salto de línea
			$this->Ln(20);
			$this->SetY(40);

			$this->SetFont('Times','',12);
			$this ->Cell(150,10,' A continuacion se muestra el registro de las depreciaciones Acumuladas de Activo Fijo hasta la fecha : ',0,0,'C');
		}
		
		// Pie de página
		function Footer()
		{
			// Posición: a 1,5 cm del final
			$this->SetY(-15);
			// Arial italic 8
			$this->SetFont('Arial','I',8);
			// Número de página
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		}
	}
	
	// Creación del objeto de la clase heredada
	$pdf = new PDF('L', 'mm', 'Legal');
	//$pdf = SetMargins(20, 18);
	$pdf->AliasNbPages();
	$pdf->AddPage();


	mysql_connect("localhost", "siif_padre", "Fusalmo2016");
	mysql_select_db("siif_EmpleadoFusalmo");

	$pdf->Ln();
		
	
		$pdf->SetY(50);


		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Times','B',10);

		$pdf->Cell(10, 5, 'Id', 1, 0, 'C');
		$pdf->Cell(20, 5, 'Correlativo', 1, 0, 'C');
		$pdf->Cell(20, 5, 'Enero 2016', 1, 0, 'C');
		$pdf->Cell(20, 5, 'Febrero 2016', 1, 0, 'C');
		$pdf->Cell(20, 5, 'Marzo 2016', 1, 0, 'C');
		$pdf->Cell(20, 5, 'Abril 2016 ', 1, 0, 'C');
		$pdf->Cell(20, 5, 'Mayo 2016 ', 1, 0, 'C');
		$pdf->Cell(20, 5, 'Junio 2016', 1, 0, 'C');
		$pdf->Cell(25, 5, 'Julio 2016', 1, 0, 'C');
		$pdf->Cell(25, 5, 'Agosto 2016', 1, 0, 'C');
		$pdf->Cell(25, 5, 'Septiembre 2016', 1, 0, 'C');
		$pdf->Cell(25, 5, 'Octubre 2016', 1, 0, 'C');
		$pdf->Cell(25, 5, 'Noviembre 2016', 1, 0, 'C');
		$pdf->Cell(25, 5, 'Diciembre 2016', 1, 1, 'C');
		
	

		$pdf->Ln();
	$sql ="SELECT  Id_Activo, Correlativo_A, Acumulada_Enero, Acumulada_Febrero, Acumulada_Marzo, Acumulada_Abril, Acumulada_Mayo, Acumulada_Junio, Acumulada_Julio, Acumulada_Agosto, Acumulada_Septiembre, Acumulada_Octubre, Acumulada_Noviembre, Acumulada_Diciembre FROM Inventario_Activo ";
	$rec = mysql_query($sql);
	$pdf->SetY(55);


 
	while ($row = mysql_fetch_array($rec))
	 {
	 	
	 	$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Times','',12);
		$pdf->Cell(10, 5, $row['Id_Activo'], 1, 0, 'C');
		$pdf->Cell(20, 5, $row['Correlativo_A'], 1, 0, 'C');
		$pdf->Cell(20, 5, $row['Acumulada_Enero'], 1, 0, 'C');
		$pdf->Cell(20, 5, $row['Acumulada_Febrero'], 1, 0, 'C');
		$pdf->Cell(20, 5, $row['Acumulada_Marzo'], 1, 0, 'C');
		$pdf->Cell(20, 5, $row['Acumulada_Abril'], 1, 0, 'C');
		$pdf->Cell(20, 5, $row['Acumulada_Mayo'], 1, 0, 'C');
		$pdf->Cell(20, 5, $row['Acumulada_Junio'], 1, 0, 'C');
		$pdf->Cell(25, 5, $row['Acumulada_Julio'], 1, 0, 'C');
		$pdf->Cell(25, 5, $row['Acumulada_Agosto'], 1, 0, 'C');
		$pdf->Cell(25, 5, $row['Acumulada_Septiembre'], 1, 0, 'C');
		$pdf->Cell(25, 5, $row['Acumulada_Octubre'], 1, 0, 'C');
		$pdf->Cell(25, 5, $row['Acumulada_Noviembre'], 1, 0, 'C');
		$pdf->Cell(25, 5, $row['Acumulada_Diciembre'], 1, 1, 'C');
		
	}

	/*$pdf->SetFillColor(232,232,232);
	$pdf->SetFont('Times','',12);
	$pdf->Cell(30,10,'Inventario de Activo Fijo ',0,0);
			$pdf->SetY(40);
			$pdf->SetX(10);
			$pdf->Cell(20,6,'ID',1,0,'C',1);
			$pdf->SetX(30);
			$pdf->Cell(30,6,'Correlativo',1,0,'C',1);
			$pdf->SetX(60);
			$pdf->Cell(40,6,'Descripcion',1,0,'C',1);
			$pdf->SetX(100);
			$pdf->Cell(30,6,'Inventario',1,0,'C',1);
			$pdf->SetX(130);
			$pdf->Cell(30,6,'Entrada',1,0,'C',1);
			$pdf->SetX(160);
			$pdf->Cell(30,6,'Salida',1,0,'C',1);*/
			
        
	$pdf->Output();
?>