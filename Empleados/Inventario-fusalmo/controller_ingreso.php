<?php




			 //  conexion
			include("Conexion.php");
			// crear la conexion y se guarda en una variale
			$con = mysqli_connect($host,$user,$pw,$db)   
			or die("problemas al conectar server");
			//  banderas  donde se define a que tabla esta haciendo la consulta
			$flat=$_POST['bandera'];  

				// switch
				switch($flat) 
				{
    				case '1':
    				$ingreso1='si'; 
    				$ingreso2='no';
    				
    				break;

    				case '2':
    				$ingreso1='no';
    				$ingreso2='si';
    				break;
				}

				// consultas
				// consulta de  activo Fijo
				if($ingreso1=='si') 
				{   
                    //funciones para calcular operaciones
                    //funcion para el valor de la cuota anual
        			function cuota($valor, $vida)
                    {
                        $cuota = $valor/$vida;
                        return $cuota;
                    }
                    //funcion para los valores de los factores de depreciacion
                    function factor($fecha_i, $fecha_f)
                    {
                        $dias = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
                        $dias   = abs($dias); 
                        $dias = floor($dias);
                        $total = $dias/365;
                        return $total;
                    }
                    //funcion para los valores de las depreciaciones acumuladas
                    function acumulada($vida, $valor, $fecha_i, $fecha_f)
                    {
                        $dato = factor($fecha_i, $fecha_f);
                        if( $dato > $vida)
                        {
                            return $valor;
                        }
                        else
                        {
                            $dato2 = cuota($valor,$vida);
                            $dato3 = $dato2 * $vida;
                            return $dato3;
                        }

                    }
                    //funcion para el valor de depreaciar_valor(campo de tabla )
                    function depreciar($vida, $valor, $fecha_i, $fecha_f1)
                    {
                        $var1 = acumulada($vida, $valor, $fecha_i, $fecha_f1);
                        
                        $resultado = $valor - $var1;
                        return $resultado;


                    }

                    //variables en las que se guardará el valor de las funciones 
                    $acu_enero = acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-01-31');
                    $acu_febrero= acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-02-29');
                    $acu_marzo=acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-03-31');
                    $acu_abril=acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-04-30');
                    $acu_mayo=acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-05-31');
                    $acu_junio=acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-06-30');
                    $acu_julio=acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-07-31');
                    $acu_agosto= acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-08-31');
                    $acu_sept=acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-09-30');
                    $acu_oct=acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-10-31');
                    $acu_nov=acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-11-30');
                    $acu_dic=acumulada($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-12-31');
                    $cuota= cuota($_POST['valor'], $_POST['vida']);
                    $fact_E=factor($_POST['fecha'], '2016-01-31');
                    $fact_F=factor($_POST['fecha'], '2016-02-29');
                    $fact_Marzo= factor($_POST['fecha'], '2016-03-31');
                    $fact_abril=factor($_POST['fecha'], '2016-04-30');
                    $fact_mayo=factor($_POST['fecha'], '2016-05-31');
                    $fact_junio=factor($_POST['fecha'], '2016-06-30');
                    $fact_julio=factor($_POST['fecha'], '2016-07-31');
                    $fact_agosto=factor($_POST['fecha'], '2016-08-31');
                    $fact_septiembre= factor($_POST['fecha'], '2016-09-30');
                    $fact_octubre=factor($_POST['fecha'], '2016-10-31');
                    $fact_nov=factor($_POST['fecha'], '2016-11-31');
                    $fact_dic=factor($_POST['fecha'], '2016-12-31');
                    $depreciacion=depreciar($_POST['vida'],$_POST['valor'], $_POST['fecha'], '2016-02-31');

                    //consulta para inventario activo 
					$consulta1 = "INSERT INTO  Inventario_Activo (Correlativo_A, Descripcion, Valor_Adquisicion, Fecha_Adquisicion,Vida_Util,Id_Bodega, Acumulada_Enero, Acumulada_Febrero, Acumulada_Marzo, Acumulada_Abril, Acumulada_Mayo, Acumulada_Junio, Acumulada_Julio, Acumulada_Agosto, Acumulada_Septiembre, Acumulada_Octubre, Acumulada_Noviembre, Acumulada_Diciembre, Factor_Enero, Factor_Febrero, Factor_Marzo, Factor_Abril, Factor_Mayo, Factor_Junio, Factor_Julio, Factor_Agosto, Factor_Septiembre, Factor_Octubre, Factor_Noviembre, Factor_Diciembre, Depreciar_Valor, Cuota_Depresicion_Anual)
						VALUES('$_POST[codigo]','$_POST[descripcion]','$_POST[valor]','$_POST[fecha]', '$_POST[vida]', '$_POST[bodega]', $acu_enero, $acu_febrero, $acu_marzo, $acu_abril, $acu_mayo, $acu_junio, $acu_julio, $acu_agosto, $acu_sept, $acu_oct, $acu_nov, $acu_dic, $fact_E, $fact_F, $fact_Marzo, $fact_abril, $fact_mayo, $fact_junio, $fact_julio, $fact_agosto, $fact_septiembre, $fact_octubre, $fact_nov, $fact_dic, $depreciacion, $cuota)";

                    // la consulta se envia por medio de la conexion
                $registro1=mysqli_query($con,$consulta1);  

				}
                // consulta para oficina
				if($ingreso2=='si') 
				{
                    //consulta de inventario oficina
					$consulta2 = "INSERT INTO Inventario_Oficina (Correlativo_O, Descripcion, Inventario, Entrada, Salida, Cantidad_Disponible, Marca, Modelo, Fecha_Compra, Lugar_Compra, Precio_Compra, Valor_Estimado, Id_Bodega)

                        VALUES('$_POST[codigo]','$_POST[nombre]','$_POST[inventario]','$_POST[entrada]', '$_POST[salida]', '$_POST[cantidad_dis]', '$_POST[marca]', '$_POST[modelo]', '$_POST[fecha]', '$_POST[lugar_compra]', '$_POST[precio]', '$_POST[valor]', '$_POST[bodega]')";
					// la consulta se envia por medio de la conexion  
					//$registro2=mysqli_query($con, $consulta2,MYSQLI_STORE_RESULT);

                    $registro2=mysqli_query($con,$consulta2);   
                   // mysqli_close($con);  
                    mysql_error();
	}

            Redireccion("../../Empleado.php?l=Inventario-fusalmo"); 
				//echo $consulta1;
               // echo $consulta2;

?>