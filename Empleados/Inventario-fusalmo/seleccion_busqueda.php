<!--
  @author Alejandra Martinez
  @copyright 2016
-->

<div style="float:right; width: 75%; text-align: left; background-color: white; padding:10px">
    <td colspan="2"><hr color='skyblue' /></td>

    <TABLE style=" padding-left: 9px" >
        <td><h1>Datos Generales</h1></td>
    </TABLE>

        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <!-- Se declaran el contendor que contendra las tablas se incluyen barras movibles -->
        <div style="overflow:scroll;height:500px;width:700px;">
            <div style="width:300px;">
                <td><input type='button' class='boton' value='Atras' name='atras' onclick='history.back()'	/></td> <!-- boton que regresa atras -->
			    <TABLE  style="padding-left: 0; padding-top: 15px;     overflow: scroll;"  >

			 	    <div id="container">

						<!--Pestaña 1 activa por defecto-->
						<input id="tab-1" type="radio" name="tab-group" checked="checked" style="visibility: hidden" />

						<div id="content">

						<!--Contenido de la Pestaña 1-->
						<div id="content-1">



<?php
        //aqui es donde se colocara la conexiones a la base de datos -->

		//require '../../net.php';
    	include("Conexion.php");
    	$con=mysqli_connect($host, $user,$pw,$db)    // Consultas por cada campo seleccionado
    	or die("problemas con la conexion a la base.");

	    $fecha = $_POST['valor_fecha']; // se recibe el valor de la fecha

	    //$fecha1 = date("d-m-Y", strtotime("$fechaa"));
		//$fecha2=date("Y-m-d",strtotime($fecha1));

			$color_row=array('#cccccc', 'lightblue'); // variables que e dan colores a las filas de las tablas
			$ind_color=0;

				// declaracion del combobox
				$listac = $_POST['lista']; // valor recibido del listbox

				// se hara un switch para cada caso de la lista
				switch($listac)
				{
					case 'A': // si se selecciona antivo fio

        			$consulta1 = "SELECT * FROM Inventario_Activo";
					$tabla1='si';
					$tabla2='no';
					break;

					case 'P': // si se selecciona inventario oficina
					$consulta2=("SELECT *FROM Inventario_Oficina ");
					$tabla1='no';
					$tabla2='si';
					break;

					case 'F': // si se selecciona busqueda por fecha
					$consulta1 = "SELECT * FROM Inventario_Activo WHERE Fecha_Adquisicion = '$fecha'";
					$consulta2 = "SELECT * FROM Inventario_Oficina WHERE Fecha_Compra = '$fecha'";
					$tabla1='si';
					$tabla2='si';
					break;

                    default: // si se selecciona por cualquier de las tres bodegas
					$consulta1=("SELECT *FROM Inventario_Activo WHERE Id_Bodega= $listac");
					$consulta2=("SELECT *FROM Inventario_Oficina WHERE Id_Bodega= $listac");
					$tabla1='si';
					$tabla2='si';
					break;
				}


        //Esta zona hace la impresion de las tablas
				if($tabla1=='si')
				{
          	// realiza un registro con la consulta y la conexion
					$registro4 = mysqli_query($con,$consulta1)
					or die ("problemas en consulta:".mysqli_error($con));

                	//imprime los encabezados de la tabla de activo fijo
							echo "<table border=1 align='center'>";
							echo "INVENTARIO ACTIVO FIJO";
							echo " <tr><tr></tr>
							<td>CORRELATIVO</td>
							<td>ID</td>
							<td>DESCRIPCION</td>
							<td>VALOR ADQUISICI&OACUTEN</td>
							<td>FECHA DE ADQUISICIÓN</td>
							<td>VIDA UTIL(MESES)</td>
							<td>CUOTA DEPRECIACION ANUAL</td>
							<td>ACUMULADA ENERO</td>
							<td>ACUMULADA FEBRERO</td>
							<td>ACUMULADA MARZO</td>
							<td>ACUMULADA ABRIL</td>
							<td>ACUMULADA MAYO</td>
							<td>ACUMULADA JUNIO</td>
							<td>ACUMULADA JULIO</td>
							<td>ACUMULADA AGOSTO</td>
							<td>ACUMULADA SEPTIEMBRE</td>
							<td>ACUMULADA OCTUBRE</td>
							<td>ACUMULADA NOVIEMBRE</td>
							<td>ACUMULADA DICIEMBRE</td>
							<td>FACTOR ENERO</td>
							<td>FACTOR FEBRERO</td>
							<td>FACTOR MARZO</td>
							<td>FACTOR ABRIL</td>
							<td>FACTOR MAYO</td>
							<td>FACTOR JUNIO</td>
							<td>FACTOR JULIO</td>
							<td>FACTOR AGOSTO</td>
							<td>FACTOR SEPTIEMBRE</td>
							<td>FACTOR OCTUBRE</td>
							<td>FACTOR NOVIEMBRE</td>
							<td>FACTOR DICIEMBRE</td>
							<td>BODEGA</td>


					        </tr>
							";

              	// con un for each se hace el llenado de la tabla
							foreach( $registro4 as $Correlativo_A=>$key)
						{
              	// variables que le dan colores a las filas
							$ind_color++;
							$ind_color %= 2;

							echo "<tr bgcolor=${color_row[$ind_color]}>";

							echo"<td>" .$key['Id_Activo']."</td>";
							echo"<td>" .$key['Correlativo_A']."</td>";
							echo"<td>" .$key['Descripcion']."</td>";
							echo"<td>" .$key['Valor_Adquisicion']."</td>";
							echo"<td>" .$key['Fecha_Adquisicion']."</td>";
							echo"<td>" .$key['Vida_Util']."</td>";
							echo"<td>" .$key['Cuota_Depresacion_Anual']."</td>";
							echo"<td>" .$key['Acumulada_Enero']."</td>";
							echo"<td>" .$key['Acumulada_Febrero']."</td>";
							echo"<td>" .$key['Acumulada_Marzo']."</td>";
							echo"<td>" .$key['Acumulada_Abril']."</td>";
							echo"<td>" .$key['Acumulada_Mayo']."</td>";
							echo"<td>" .$key['Acumulada_Junio']."</td>";
							echo"<td>" .$key['Acumulada_Julio']."</td>";
							echo"<td>" .$key['Acumulada_Agosto']."</td>";
							echo"<td>" .$key['Acumulada_Septiembre']."</td>";
							echo"<td>" .$key['Acumulada_Octubre']."</td>";
							echo"<td>" .$key['Acumulada_Noviembre']."</td>";
							echo"<td>" .$key['Acumulada_Diciembre']."</td>";
							echo"<td>" .$key['Depreciar_Valor']."</td>";
							echo"<td>" .$key['Factor_Enero']."</td>";
							echo"<td>" .$key['Factor_Febrero']."</td>";
							echo"<td>" .$key['Factor_Marzo']."</td>";
							echo"<td>" .$key['Factor_Abril']."</td>";
							echo"<td>" .$key['Factor_Mayo']."</td>";
							echo"<td>" .$key['Factor_Junio']."</td>";
							echo"<td>" .$key['Factor_Julio']."</td>";
							echo"<td>" .$key['Factor_Agosto']."</td>";
							echo"<td>" .$key['Factor_Septiembre']."</td>";
							echo"<td>" .$key['Factor_Octubre']."</td>";
							echo"<td>" .$key['Factor_Noviembre']."</td>";
							echo"<td>" .$key['Factor_Diciembre']."</td>";
							echo"<td>" .$key['Id_Bodega']."</td>";

							echo "</tr>";
						}


				}
						if($tabla2=='si')
						{
              // realiza un registro con la consulta y la conexion
							$registro_oficina2 = mysqli_query($con,$consulta2)
							or die ("problemas en consulta:".mysqli_error($con));

                	//imprime los encabezados de la tabla del inventario de oficina
							echo "<table border=1 align='center'>";
							echo "<tr>INVENTARIO OFICINA<tr/>";
							echo " <tr><tr></tr>
							  <td>CORRELATIVO</td>
							  <td>ID</td>
							  <td>DESCRIPCION</td>
							  <td>CANTIDAD</td>
							  <td>ENTRADA</td>
							  <td>SALIDA</td>
							  <td>CANTIDAD DISPONIBLE</td>
			  				  <td>MARCA</td>
			  				  <td>MODELO</td>
							  <td>FECHA DE ADQUISICIÓN</td>
							  <td>LUGAR DE COMPRA</td>
							  <td>PRECIO COMPRA</td>
							  <td>VALOR ESTIMADO</td>
							  <td>BODEGA</td>

							  </tr>
							  ";

                	// con un for each se hace el llenado de la tabla
							foreach($registro_oficina2 as $Correlativo_O=>$key)
							{
                	// variables que le dan colores a las filas
								$ind_color++;
								$ind_color %= 2;

								echo "<tr bgcolor=${color_row[$ind_color]}>";
								echo"<td>" .$key['Id_Oficina']."</td>";
								echo"<td>" .$key['Correlativo_O']."</td>";
								echo"<td>" .$key['Descripcion']."</td>";
								echo"<td>" .$key['Inventario']."</td>";
								echo"<td>" .$key['Entrada']."</td>";
								echo"<td>" .$key['Salida']."</td>";
								echo"<td>" .$key['Cantidad_Disponible']."</td>";
								echo"<td>" .$key['Marca']."</td>";
								echo"<td>" .$key['Modelo']."</td>";
								echo"<td>" .$key['Fecha_Compra']."</td>";
								echo"<td>" .$key['Lugar_Compra']."</td>";
								echo"<td>" .$key['Precio_Compra']."</td>";
								echo"<td>" .$key['Valor_Estimado']."</td>";
								echo"<td>" .$key['Id_Bodega']."</td>";

								echo "</tr>";
							}

						}
             ?>
             </div>
             </div>
             </div>


</TABLE>
</div>
</div>
</div>
