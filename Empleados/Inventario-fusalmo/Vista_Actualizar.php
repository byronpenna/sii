<!--
  @author Alejandra Martinez
  @copyright 2016
-->

<div style="float:right; width: 77%; text-align: left; background-color: white; padding:10px"> <!--Es el tamaño del contenedor -->
	 <td colspan="2"><hr color='skyblue' /></td>

             <TABLE style=" padding-left: 9px" >

		<tr><td><h2>Actualizar: </h2></td></tr>  <!--Encabezado de la pàgina -->


		</TABLE>
             <tr><td colspan="2"><hr color='skyblue' /></td></tr>

			 <TABLE  style="padding-left: 0; padding-top: 15px;  overflow: scroll;"  >



		<?php

			include("Conexion.php");  //Instancia a la página de conexion que contiene los datos de la conexión

			$con = mysqli_connect($host,$user,$pw,$db)  // se realiza la conexión
			or die("problemas al conectar server");


			$accion = $_GET['accion'];  //Recibe la accion que se envia desde la pagina de ajuste
			$id =$_GET['id']; // Recibe el id del material que se envia de la pagina de ajuste

			//echo $accion;
			//echo $id;

			switch ($accion) {
				case 1:
					$ingreso1='si'; // llama al ingreso 1  que es la del inventario activo fijo
					$ingreso2='no'; // deshabilita al ingreso 2  que es la del inventario oficina
					//edit($id,$con);
					break;
				case 2:
					$ingreso2='si'; // llama al ingreso 2  que es la del inventario oficina
					$ingreso1='no'; // deshabilita al ingreso 1  que es la del inventario activo fijo
					//edit2($id,$con);
					break;
			}

			if($ingreso1=='si')
			{ 
				$consulta1=("Select * from Inventario_Activo where Id_Activo = '$id'"); // se llama la tabla del activo fijo
				
				$registro1=mysqli_query($con,$consulta1);
				foreach($registro1 as $id=>$key)
						{
											// Se llenan cada text box con la información correspondiente al material del id enviado
							echo"<form action='Empleados/Inventario-fusalmo/controller_Update.php' id='InventarioActivoForm' method='POST' >";
					echo"<tr><td>Codigo Activo:</td><td><input style='width: 390px' type='text' value=".$key['Correlativo_A']." name='codigo' readonly ></td></tr>";
			    echo"<tr><td>Descripcion:</td><td><input style='width: 390px' type='text' value=".$key['Descripcion']." name='material' required></td></tr>";
			    echo"<tr><td>Valor Adquisicion:</td><td><input style='width: 390px' type='text'  value=".$key['Valor_Adquisicion']." name='costo' required></td></tr>";
					echo"<tr><td>Fecha:</td><td><input style='width: 390px' type='date' value=".$key['Fecha_Adquisicion']."  name='fecha' required></td></tr>";
			    echo"<tr><td>Vida:</td><td><input style='width: 390px' type='number' min='0' value=".$key['Vida_Util']." name='vida' required></td></tr>";
			    echo"<tr><td>bodega:</td><td><input style='width: 390px' type='number' min='0' value=".$key['Id_Bodega']." name='bodega' required><td></td></td></tr>";


				echo"<input type='hidden' value='1' name='bandera' >"; // se manda una bandera para poder hacer un trabajo mas eficiente al controller_update
				echo"<tr>
	              <td><input type='submit' class='boton' value='Actualizar' name='Enviar' 	/></td>
	              <td><input type='button' class='boton' value='Cancelar Ingreso' name='Enviar' onclick='history.back()'	/></td>
					</tr>";
				echo "</form>";
				}
			}
			if($ingreso2=='si')
			{
				$consulta2=("Select * from Inventario_Oficina where Id_Oficina= '$id'"); // se llama la tabla del inventario oficina
				$registro2=mysqli_query($con,$consulta2);
				foreach($registro2 as $id=>$key)
						{
										// Se llenan cada text box con la información correspondiente al material del id enviado
					echo"<form action='controller_update.php' id='InventarioOficinaForm' method='POST' >";
				  echo"<tr><td>Codigo Activo:</td><td><input style='width: 390px' type='text' value=".$key['Correlativo_O']." name='codigo' readonly ></td></tr>";
			    echo"<tr><td>Descripcion:</td><td><input style='width: 390px' type='text' value=".$key['Descripcion']." name='nombre' required></td></tr>";
			    echo"<tr><td>Valor Adquisicion:</td><td><input style='width: 390px' type='number' min='0' value=".$key['Inventario']." name='cantidad' required></td></tr>";
					echo"<tr><td>Entrada:</td><td><input style='width: 390px' type='number' min='0' value=".$key['Entrada']." name='entrada' required></td></tr>";
					echo"<tr><td>Salida:</td><td><input style='width: 390px' type='number' min='0' value=".$key['Salida']." name='salida' required></td></tr>";
			  	echo"<tr><td>Marca:</td><td><input style='width: 390px' type='text' value=".$key['Marca']." name='marca' required></td></tr>";
				  echo"<tr><td>Modelo:</td><td><input style='width: 390px' type='text' value=".$key['Modelo']." name='modelo' required></td></tr>";
				  echo"<tr><td>Fecha:</td><td><input style='width: 390px' type='date' value=".$key['Fecha_Compra']." placeholder='AAAA-MM-DD' name='fecha' required></td></tr>";
          echo"<tr><td>Lugar de compra::</td><td><input style='width: 390px' type='text' value=".$key['Lugar_Compra']." name='lugar' required><td></td></td></tr>";
				  echo"<tr><td>Precio de compra:</td><td><input style='width: 390px' type='number' value=".$key['Precio_Compra']." name='precio'  required><td></td></td></tr>";
					echo"<tr><td>Valor de compra:</td><td><input style='width: 390px' type='number' value=".$key['Valor_Estimado']." name='valor'  required><td></td></td></tr>";
				  echo"<tr><td>Bodega:</td><td><input style='width: 390px' type='number' min='0' value=".$key['Id_Bodega']." name='bodega' required><td></td></td></tr>";
				  echo"<input type='hidden' value='2' name='bandera' >";  // se manda una bandera para poder hacer un trabajo mas eficiente al controller_update



				echo"<tr>
	              <td><input type='submit' class='boton' value='Actualizar' name='Enviar' 	/></td>
	              <td><input type='button' class='boton' value='Cancelar Ingreso' name='cancelar' onclick='history.back()'/></td>
					</tr>";
				echo "</form>";

				}


			}
		?>

</div>
