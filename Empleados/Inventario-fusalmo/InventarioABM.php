<?php


/**
 * @author Manuel Calderón-Lilian Cordero
 * @copyright 2013 - 2016
 */


  require '../../net.php';
    
    if(isset($_POST['Enviar']))
    {    
        if($_POST['Enviar'] == "Ingreso")
        {
            $consult = $bddr->prepare("INSERT INTO Inventario_Oficina values (NULL, :Correlativo_O, :Descripcion, :Inventario, :Entrada, :Salida, :Cantidad_Disponible, :Marca, :Modelo, :Fecha_Compra, :Lugar_Compra, :Precio_Compra, :Valor_Estimado, :Id_Bodega)");
            $consult->bindParam(':Correlativo_O', $$_POST['codigo']);
            $consult->bindParam(':Descripcion', $_POST['nombre']);
            $consult->bindParam(':Inventario', $_POST['inventario']);
            $consult->bindParam(':Entrada', $_POST['entrada']);
            $consult->bindParam(':Salida', $_POST['salida']);
            $consult->bindParam(':Cantidad_Disponible', $_POST['cantidad_dis']);
            $consult->bindParam(':Marca', $_POST['marca']);
            $consult->bindParam(':Modelo', $_POST['modelo']);
            $consult->bindParam(':Fecha_Compra', $_POST['fecha']);
            $consult->bindParam(':Lugar_Compra', $_POST['lugar_compra']);
            $consult->bindParam(':Precio_Compra', $_POST['precio']);
            $consult->bindParam(':Valor_Estimado', $_POST['valor']);
            $consult->bindParam(':Id_Bodega', $_POST['bodega']);

            if ($consult->execute()) {
                echo "Septumsembra";
            }
            else
                echo "AbadaKadabra";
            //$consulta2->execute();

            //Redireccion("../../Empleado.php?l=InventarioOficina");
        }
        
        else if($_POST['Enviar'] == "Actualizar")
        {
           
        }    
        
        else if($_POST['Enviar'] == "Eliminar")
        {
          
        }     

        if($_POST['Enviar'] == "Guardar")
        {
            if($_POST['Decision'] == "Denegar")
                $estado = "Denegado"; 
                
            if($_POST['Decision'] == "Aceptar")
                $estado = "Aceptado";

            $query = "UPDATE Request_Requisiciones SET Estado = '$estado' , Comentario_Jefe = :Comentario  WHERE IdRequisicion =" . $_POST["idp"];
            $Update = $bddr->prepare($query);   
            $Update->bindParam(':Comentario',$_POST['Comentario']);       
            $Update->execute();


           if($estado == "Aceptado") 
           {

            $query3 = "SELECT di.Correo FROM CargosAsignacion as ca 
					   INNER JOIN DatosInstitucionales as di on ca.IdEmpleado = di.IdEmpleado
					   WHERE ca.IdCargo = 24 AND ca.FechaFin = '0000-00-00'";
                       
			$Gerencia = $bddr->prepare($query3);   
			$Gerencia ->execute();
			$DataG = $Gerencia->fetch(); 

		   $MisRequisiciones = $bddr->prepare("SELECT 
                                               p.Proyecto, l.LineaPresupuestaria, s.SubLinea, r.Actividad, r.Fecha_Solicitud, r.Fecha_entrega, r.Tipo_Requisicion, 
                                               r.Comentario, r.Comentario_Jefe, 
                                               m.Nombre1, m.Nombre2, m.Apellido1, m.Apellido2, 
                                               j.Nombre1, j.Nombre2, j.Apellido1, j.Apellido2 
                                               FROM Request_Requisiciones as r 
                                               inner join Request_Proyecto as p on r.IdProyecto = p.IdProyecto 
                                               inner join Request_LineaPresupuestaria as l on r.Linea_Presupuestaria = l.IdLinea 
                                               inner join Request_SubLineaPresupuestaria as s on r.SubLinea_Presupuestaria = s.IdSubLineaPresupuestaria  
                                               inner join Empleado AS m on r.IdEmpleado = m.IdEmpleado  
                                               inner join Empleado AS j on r.idjefe = j.IdEmpleado 
                                               where r.Idrequisicion = " . $_POST["idp"]) ;                                               
           $MisRequisiciones->execute();
           $Requi = $MisRequisiciones->fetch();

            $to = $DataG[0];   
            //$to = "liligrc9221@gmail.com";                    
            $subject = "Solicitud de Requisición Aprobada";
            
            $message = "
                <h2>Solicitud de Requisición Aprobada por Jefe Inmediato</h2><br />
                <br />$DataG[0]<br />
                <table style='width: 70%'>
                    <tr>
                        <td style='width: 30%'>Proyecto: </td>
                        <td>$Requi[0]</td>
                    </tr>
                    <tr>
                        <td>Linea Presupuestaria: </td>
                        <td>$Requi[1]</td>
                    </tr>  
                    <tr>
                        <td>Sublínea presupuestaria: </td>
                        <td>$Requi[2]</td>
                    </tr>
                    <tr><td colspan='2'><br /></td></tr>  
                    <tr>
                        <td>Actividad: </td>
                        <td>$Requi[3]</td>
                    </tr>                                             
                    <tr>
                        <td>Fecha de solicitada: </td>
                        <td>$Requi[4]</td>
                    </tr> 
                    <tr>
                        <td>Fecha de entrega: </td>
                        <td>$Requi[5]</td>
                    </tr>
                    <tr><td colspan='2'><br /></td></tr>                     
                    <tr>
                        <td>Empleado Solicitante: </td>
                        <td>$Requi[9] $Requi[10] $Requi[11] $Requi[12]</td>
                    </tr>                     
                    <tr>
                        <td>Comentarios: </td>
                        <td>$Requi[7]</td>
                    </tr>       
                    <tr><td colspan='2'><br /></td></tr>              
                    <tr>
                        <td>Visto bueno por: </td>
                        <td>$Requi[13] $Requi[14] $Requi[15] $Requi[16]</td>
                    </tr> 
                    <tr>
                        <td>Comentarios Jefe: </td>
                        <td>$Requi[8]</td>
                    </tr>                                                                                                                                                                                    
                </table>
                <br /><br />
               <table style='width: 50%' rules='all'>
               <tr><th>Cantidad</th><th>Material</th><th>Especificación Tecnica</th></tr>";

               $sqlMaterial= "SELECT * FROM Request_Material as k 
                              inner join Request_Requisiciones as o on k.Idrequisicion = o.Idrequisicion
                              where k.IdRequisicion = " . $_POST["idp"];
    
    		   $stmt = $bddr->prepare($sqlMaterial);
               $stmt->execute();
               
               While($result = $stmt->fetch())
               {
                  $message .=  "<tr>
                                    <th>$result[2]</th>
                                    <th>$result[4]</th>
                                    <th>$result[3]</th>
                                </tr>";                
               }                 
               $message .= "</table>";
               $message .=  "<br /><br />Acepta o deniega dando <a href='https://siifusalmo.org/RequisicionApproval.php?idr=".$_POST["idp"]."'> clic aquí </a>" ; 
     
                	
            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org, liligrc9221@gmail.com," . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            mail($to,$subject,$message,$headers); 
            Redireccion("../../Empleado.php?l=Request&n=2"); 
          }
             
        }                   
        
         else if($_POST['Enviar'] == "Denegar")
        {
          
        }    
        else if($_POST['Enviar'] == "archivar")
        {
            echo "Entro";
            echo "<br />";
            if($_POST['Decision'] == "Rechazar")
                $estado = "Rechazado"; 
                
            if($_POST['Decision'] == "Aprobar")
                $estado = "Aprobado";

            $query = "UPDATE Request_Requisiciones SET Estado = '$estado' , Comentario_finanzas = :Comentario  WHERE IdRequisicion =" . $_POST["idp"];
            $Update = $bddr->prepare($query);   
            $Update->bindParam(':Comentario',$_POST['Comentario']);       
            $Update->execute();

            $id = $bddr->lastInsertId();
            $materup = isset($_POST['Cantidad_aprobada']) ? $_POST['Cantidad_aprobada'] : array();
            $materid = isset($_POST['IdMaterial']) ? $_POST['IdMaterial'] : array();
            
            for ($index = 0 ; $index < count($materup); $index++) 
            {   
                $qute = "UPDATE Request_Material SET Cantidad_aprobada = $materup[$index] WHERE IdMaterial = $materid[$index]";
                $newmaterial = $bddr->prepare($qute);
                //$newmaterial->bindParam(':Cantidad_aprobada', $materup[$index]);
                $newmaterial->execute();
                echo "<br />";
            }
            
       

             mail($to,$subject,$message,$headers); 
            Redireccion("../../Empleado.php?l=Request&n=2"); 
        }
        

        //Consultas para solicitud de inventario
        else if ($_POST['Enviar'] == "Cancelar solicitud") 
        {
            echo"<script>alert('Usted esta siendo redireccionado a la pagina principal')</script>";
            Redireccion("../../Empleado.php?l=Inventario-fusalmo"); 
                   
        }
       
    if ($_POST['Enviar'] == "Crear solicitud") {
            //DATOS PARA TABLA INVENTARIO SOLICITUD
            $AddSolicitud = $bddr->prepare("Insert into Inventario_Solicitud values(Null,:ID_USUARIO, :Fecha_Solicitud,:Telefono,:Correo,:Nombre_Sede,:Jefe_Sede,:Id_Proyecto,'Pendiente')");
            $AddSolicitud->bindParam(':ID_USUARIO',$_SESSION["IdUsuario"]);
            $AddSolicitud->bindParam(':Fecha_Solicitud',date("Y-m-d"));
			      $AddSolicitud->bindParam(':Telefono',$_POST['Telefono']);
			      $AddSolicitud->bindParam(':Correo',$_POST['Correo']);
			      $AddSolicitud->bindParam(':Nombre_Sede',$_POST['Sede']);
			      $AddSolicitud->bindParam(':Jefe_Sede',$_POST['Jefe']);
            $AddSolicitud->bindParam(':Id_Proyecto',$_POST['proyectoR']);
           // $AddSolicitud->execute();
              if ($AddSolicitud->execute()) {
                echo "Pasó SOLICITUD";
            }
            else
                echo "NO PASÓ";

            
            $id = $bddr->lastInsertId();
            $fecha = date("Y-m-d");

            //Ingreso a tabla criterios
            $AddCriterios=$bddr->prepare("Insert into Inventario_Criterio values(null,:Id_Solicitud,:Precio_Porcentaje,:Plazo_Porcentaje,:Calidad_Porcentaje,:Garantia_Porcentaje,:Servicio_Porcentaje,:Otro_Porcentaje,:Total_Porcentaje)");
            $AddCriterios->bindParam(':Id_Solicitud',$_SESSION["IdUsuario"]);
            $AddCriterios->bindParam(':Precio_Porcentaje',$_POST['price']);
            $AddCriterios->bindParam(':Plazo_Porcentaje',$_POST['plazo']);
            $AddCriterios->bindParam(':Calidad_Porcentaje',$_POST['calidad']);
            $AddCriterios->bindParam(':Garantia_Porcentaje',$_POST['garantia']);
            $AddCriterios->bindParam(':Servicio_Porcentaje',$_POST['S_P']);
            $AddCriterios->bindParam(':Otro_Porcentaje',$_POST['other']);
            $AddCriterios->bindParam(':Total_Porcentaje',$_POST['total']);
    
            if ($AddCriterios->execute()) {
                echo "Pasó CRITERIOS";
            }
            else
                echo "NO PASÓ CRITERIOS";

            //VALIDANDO EXISTENCIA DE INGRESO DE VARIABLES
             $Cantidad      = isset($_POST['cantidad'])        ? $_POST['cantidad']    :         array();
             $Material      = isset($_POST['material'])        ? $_POST['material']    :         array();
             $Descripcion   = isset($_POST['descripcion'])     ? $_POST['descripcion'] :         array();

        
        for ($index = 0 ; $index < count($Cantidad); $index++) 
            { 
                //Considero que hace falta el campo de cantidad
                $Addprestamos=$bddr->prepare("Insert into Inventario_Solicitud_Material values(null,:Id_Solicitud,:Material,:Cantidad,:Descripcion)");
                $Addprestamos->bindParam(':Id_Solicitud',$id);
                $Addprestamos->bindParam(':Cantidad',$Cantidad[$index]);
                $Addprestamos->bindParam(':Material',$Material[$index]);
                $Addprestamos->bindParam(':Descripcion',$Descripcion[$index]);
                $Addprestamos->execute();
            }
                echo '<script language="javascript">alert("¡Solicitud ingresada!");</script>';
            //Redireccion("../../Empleado.php?l=Inventario-fusalmo");

            $MailEmpleado = $bddr->prepare("Select Correo FROM DatosInstitucionales WHERE IdEmpleado = " . $_POST['idj']);
            $MailEmpleado->execute();
            $DataM = $MailEmpleado->fetch();

            $to = "$DataM[0]";    
           // $to = "liligrc9221@gmail.com";                  
            $subject = "Solicitud de Materiales";
            
            $message = "
                <h2>Solicitud de Materiales</h2><br />
            
                Nombre del Empleado: <br /> ".$_POST['Nombre_Solicitante']." <br /><br />
                Proyecto: <br /> ".$_POST['proyectoR']." <br /><br />
                Fecha de solicitud: <br /> ".$fecha." <br /><br />
                Fundamentos: <br /> ".$_POST['comentarios']." <br /><br />
                
                Materiales Solicitados <br/>
               <table style='width: 70%'>
               <tr><th>Cantidad</th><th>Material</th><th>Descripción</th></tr>";
                
                for ($index = 0 ; $index < count($Cantidad); $index++) 
                {    
                    $message .= "
                                <tr>
                                   <th>$Cantidad[$index] </th>
                                   <th>$Material[$index]</th>
                                   <th>$Descripcion[$index]</th>
                                </tr>";
                }
                $message .= "</table>";                
                $message .= "<br /><br />Acepta o deniega dando <a href='https://siifusalmo.org/RequisicionView.php?idr=$id'> clic aquí </a>";              

            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org, liligrc9221@gmail.com," . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            mail($to,$subject,$message,$headers);  
           // Redireccion("../../Empleado.php?l=seguimientosolicitud");

  //Redireccion("../../Empleado.php?l=Request&n=2");
        }
      }

?>