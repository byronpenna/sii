  <?php

  /**
   * @author Marcela Lopez
   * @copyright  2016
   */

  // Este formulario es la pagina principal de los reportes
  ?>


  <!-- <link href="css1.css" rel="stylesheet" type="text/css" />
  <link href="css2.css" rel="stylesheet" type="text/css" /> -->
  <style type="text/css">
    #divTipos {
  width: 30%; 
  padding-top: 20px;
  /*border-color: blue;
  border-width: 5px;
  border-style: solid;*/
  float: left;
  margin-left: 15px;
}

#divAreas{
  width: 30%; 
  padding-top: 20px;
  
  /*border-color: blue;
  border-width: 5px;
  border-style: solid;*/
  float: left;
  margin-left: 15px;
}

#divSedes{
  width: 30%; 
  padding-top: 20px;
  
  /*border-color: blue;
  border-width: 5px;
  border-style: solid;*/
  float: left;
  margin-left: 15px;
  margin-top: 3px;
}

#A,#T{
   padding-top: 10px;
}

#C{
  margin-top: 20px;
  background-color:rgba(120,187,230,0.3);
  height: 40%;
  padding: 10px;
  border-radius: 5px;
  font-size: 18px;

  
}

#S{
   padding-top: 7px;
}

#divGeneral {
  margin-top: 30px;
  margin-left: 15px;
}

#todoSedes,#todoAreas,#todoTipos {
  margin-top: 10px;
}

#divEnviar{
  width: 125px;
  height: 75px;
  margin-top: 30%;
  margin-left: 80%;
  
}

#botonEnviar {
  width: 125px;
  height: 75px;
  font-size: 16px;
}

  </style>
  <script type="text/javascript" src="jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script type="text/javascript">

  //funcion para elementos ocultos

  function mostrar(id)
  {

  	if (id == "C")
  	{
  		$("#C").show();
  	}else{
  		$("#C").hide();
  	}

  }

  //funcion para elementos ocultos

  function checkTipos() {
          element = document.getElementById("T");
          check = document.getElementById("C1");
          if (check.checked) {
              element.style.display='block';
          }
          else {
              element.style.display='none';
          }
      }

  //funcion para elementos ocultos

  function checkArea() {
          element = document.getElementById("A");
          check = document.getElementById("C2");
          if (check.checked) {
              element.style.display='block';
          }
          else {
              element.style.display='none';
          }
      }

   function checkSedes() {
          element = document.getElementById("S");
          check = document.getElementById("C4");
          if (check.checked) {
              element.style.display='block';
          }
          else {
              element.style.display='none';
          }
      }

      function desactivarSedes() {
          element = document.getElementById("selectSedes");
          check = document.getElementById("todoSedes");
          if (check.checked) {
              element.disabled = true;
          }
          else {
              element.disabled = false;
          }
      }

       function desactivarOpciones() {
          tipos = document.getElementById("C1");
           sedes = document.getElementById("C2");
            areas = document.getElementById("C4");
          check = document.getElementById("C3");
          if (check.checked) {
              tipos.disabled = true;
              sedes.disabled = true;
              areas.disabled = true;
          }
          else {
              tipos.disabled = false;
              sedes.disabled = false;
              areas.disabled = false;
          }
      }

       function desactivarAreas() {
          element = document.getElementById("selectAreas");
          check = document.getElementById("todoAreas");
          if (check.checked) {
              element.disabled = true;
          }
          else {
              element.disabled = false;
          }
      }

       function desactivarTipos() {
          element = document.getElementById("selectTipos");
          check = document.getElementById("todoTipos");
          if (check.checked) {
              element.disabled = true;
          }
          else {
              element.disabled = false;
          }
      }


  //Codigo para buscador en los campos de select
  $(document).ready(function() {
      $('.tipos').select2();
  }); 

  $(document).ready(function() {
      $('.areas').select2();
  }); 

  $(document).ready(function() {
      $('.sedes').select2();
  });
  </script>


  <div style="float:right; width: 77%; text-align: left; background-color: white; padding:10px">
  	<form action="Empleados/Inventario-fusalmo/Reportes_Activo.php" method="post">
  		<table style="width: 100%" colspan="4">

  			<tr><td colspan="4"><h2 style="color: #197198;">M&oacutedulo de Inventario</h2></td></tr>
  			<tr><td colspan="4"><hr color='skyblue' /></td></tr>
  			<tr><td style="width: 50%; text-align: left;"><a href = "javascript:history.back()">
  			<input type="submit" value="<- Atras" name="Enviar" class="boton"  /></a>
  			</td></tr>
  			<tr><td colspan="5"><hr color='skyblue'/></td></tr>

  			<table>

  	        <table style="width: 100%;">
  	        <tr>
  	        <tr><td><h3>Inventario: </h3> </td></tr>


  	        		<td style="width: 20%;"><form action="Empleados/Inventario-fusalmo/Reportes_Activo.php" method="post">
       			    <input type="hidden" class="boton" value="Inventario Oficina" name="Enviar" onchange="mostrar('F')" style=" width: 5px;"/></form></td> 

  	        	  <td style="width: 20%;">
       			    <input type="button" class="boton" value="Boton de Pruebas" name="Enviar" onclick="mostrar('C')" style=" width: 150px;"/></td> 

  				      <td style="width: 20%;"><form action="Empleados/Inventario-fusalmo/Reportes_Activo.php" method="post">
  				      <input type="submit" class="boton" value="Inventario Activo Fijo" name="Enviar" style=" width: 150px;" /></form></td>

  	            <td style="width: 20%;"><form action="Empleados/Inventario-fusalmo/Reportes_Activo_Acumuladas.php" method="post">
  	            <input type="submit" class="boton" value="Depreciaciones Acumuladas" name="Enviar"  style=" width: 170px;" /></form></td>

  	            <td style="width: 20%;"><form action="Empleados/Inventario-fusalmo/Reportes_Activo_Factores.php" method="post">
                <input type="submit" class="boton" value="Factores Depreciables" name="Enviar"  style=" width: 140px;"/> </form></td>
              
  			</tr>
  							
  	    	</table>
            <tr>
  	    	<div id="C" class="element" style="display:none;">
    					<p><b>Seleccion de Filtros:</b></p>

              <div id="divGeneral">
                 <input type="checkbox" id="C3" name="general" onchange="javascript:desactivarOpciones()" value="G">Generar Reporte de Todo el Inventario de Activo Fijo
              </div>

              <div id="divTipos">
        			  	<input type="checkbox" id="C1" name="tipos" value="T" onchange="javascript:checkTipos()">Por Tipos
                 <div id="T" class="element" style="display:none;">
                     <select class="tipos" name="tipos" id="selectTipos" name="tipos[]" multiple="multiple" style="width: 200px;" >
                        <option value="T1">Computadoras</option>  
                        <option value="T2">Telefonos</option>
                        <option value="T3">Teclados</option>
                        <option value="T4">Mouse</option>
                        <option value="T5">Sillas</option>              
                     </select>
                     <input type="checkbox" id="todoTipos" name="tipos" onchange="javascript:desactivarTipos()">Todos los Tipos
                 </div>
              </div>
              
              <div id="divAreas">
    					   <input type="checkbox" id="C2" name="areas" value="A" onchange="javascript:checkArea()">Por Areas
                    <div id="A" class="element" style="display:none;"> 
              <select class="areas" name="areas" id="selectAreas" name="areas[]" multiple="multiple" style="width: 200px;">
                <option value="A1">Informatica</option>  
                <option value="A2">Dise&ntildeo</option>
                <option value="A3">Servidores</option>
                <option value="A4">Progra</option>
                <option value="A5">Recursos Humanos</option> 
              </select>
              <input type="checkbox" id="todoAreas" name="todoAreas" onchange="javascript:desactivarAreas()">Todas las areas

            </div> 
              </div>

              <div id="divSedes">
                  <input type="checkbox" id="C4" name="sedes" value="S" onchange="javascript:checkSedes()">Por Sedes
                 <div id="S" class="element" style="display:none;">
                     <select class="sedes" id="selectSedes" name="sedes" name="sedes[]" multiple="multiple" style="width: 200px;" >
                        <option value="S1">Sede 1</option>  
                        <option value="S2">Sede 2</option>
                        <option value="S3">Sede 3</option>
                        <option value="S4">Sede 4</option>
                     </select>
                     <input type="checkbox" id="todoSedes" name="sedes" value="A" onchange="javascript:desactivarSedes()">Todas las Sedes
                 </div>
              </div>
              <div id="divEnviar">
                <input type="submit" id="botonEnviar" value="Generar Reporte" name="Enviar" class="boton"  /></a>
              </div>
    			</div>

          </tr>
  		</table>
  	</form>
  </div>

