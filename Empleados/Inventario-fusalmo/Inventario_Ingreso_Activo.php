<?php

/**
 * @author Marcela Lopez
 * @copyright  2016
 */

$query = "SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo, a.IdAreaDeTrabajo
          FROM CargosAsignacion AS ca
          INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
          INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
          WHERE ca.IdEmpleado = '".$_SESSION["IdUsuario"]."'  and ca.FechaFin = '0000-00-00'";          
          
$MisCargos = $bddr->prepare($query);                                                                        
$MisCargos->execute();
$DataC = $MisCargos->fetch();

//Este formulario es la pagina principal del ingreso del inventario activo fijo en el cual los usuraios podran ingresar la informacion solicitante, y esta informacion se guardará en la base de datos especificada en controller ingreso
?>




<div style="float:right; width: 77%; text-align: left; background-color: white; padding:10px">
	

<table style="width: 100%">
		<tr><td><h2 style="color: #197198;">  Ingreso Inventario Activo Fijo   </h2></td></tr>
		<tr><td colspan="2"><hr color='skyblue'/></td></tr>
		<tr><td style="width: 50%; text-align: left;"><a href = "javascript:history.back()">
			<input type="submit" value="<- Atras" name="Enviar" class="boton"  /></a></td>
		</tr>
		<tr><td colspan="2"><hr color='skyblue'/>  </td></tr>

		<?php
            $query2 = "SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado
                       FROM Empleado AS e
                       INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = e.IdEmpleado
                       INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
                       INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
                       where ac.IdAsignacion = $DataC[1]";
            
        	  $Accion = $bddr->prepare($query2);                                            
            $Accion->execute();
            $DatosEmple = $Accion->fetch(); 
            
            $query = "SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado, ca.IdAsignacion 
                      FROM CargosAsignacion AS ca
                      INNER JOIN Jerarquia AS j ON ca.IdCargo = j.IdCargoSuperior
                      INNER JOIN Cargos AS c ON c.IdCargos = j.IdCargoSuperior
                      INNER JOIN Empleado AS e ON ca.IdEmpleado = e.IdEmpleado
                      INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
                      WHERE j.IdCargos_Fk = $DataC[2] and ca.FechaFin = '0000-00-00'";
                
              $DatosJefe = $bddr->prepare($query);                                            
              $DatosJefe->execute();
              $Jefe = $DatosJefe->fetch(); 
            
             $Area = $DatosEmple[6];
		?>        

		<tr>
            <td colspan="2">
                <table style="width: 80%;">
                    
                    <tr><td  style="width: 30%;">Nombre del Empleado: 	</td><td><?php echo "$DatosEmple[0] $DatosEmple[1] $DatosEmple[2] $DatosEmple[3]"?></td></tr>
                    <tr><td >Cargo Asignado:               				</td><td><?php echo "$DatosEmple[5]"?></td></tr>
                    <tr><td >Area de trabajo:                			</td><td><?php echo "$DatosEmple[6]"?></td></tr>
                    <tr><td></td><td><?php echo $localidad?>			</td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width: 80%;">
                    <tr><td  style="width: 30%;">Jefe Inmediato: </td><td><?php echo "$Jefe[0] $Jefe[1] $Jefe[2] $Jefe[3]"?></td></tr>
                    <tr><td >Cargo Asignado:                     </td><td><?php echo "$Jefe[5]"?></td></tr>
                    <tr><td >Area de trabajo:                    </td><td><?php echo "$Jefe[6]"?></td></tr>
                </table>
            </td>
        </tr>  
  


		<tr><td colspan="2"><hr color='skyblue'/>  </td></tr>

		<tr><td><h3> Ingreso de Datos: </h3></td></tr>
		
     
<table style="padding: 10px">
		<form action='?l=control' id='InventarioActivoForm' method='POST'>
			
		     	
			     	<tr><td>Codigo Activo:</td><td><input style="width: 390px" type="text" name="codigo" ></td></tr>
			     	<tr><td>Descripcion:</td><td><input style="width: 390px" type="text" name="descripcion"  ></td></tr>
			     	<tr><td>Valor Adquisicion:</td><td><input style="width: 390px" type="text" name="valor" ></td></tr>
			    	<tr><td>Fecha Adquisicion:</td><td><input style="width: 390px" type="text" placeholder="AAAA-MM-DD" name="fecha" ></td></tr>
			    	<tr><td>Vida Util (a&ntildeos):</td><td><input style="width: 390px" type="number" min="0" name="vida" ><td></td></td></tr>
			    	<tr><td>Bodega:</td><td><input style="width: 390px" type="number" min="0" max="3" name="bodega"></td></tr>

		      <tr>
          <input type='hidden' value='1' name='bandera' > <!-- bandera para diferenciar el formulario -->  
	              <td><input type="submit" class="boton" value="Ingresar Registro" name="Enviar" 	/></td>
	              <td><input type="button" class="boton" value="Cancelar Ingreso" name='cancelar' onclick='history.back()' 	/></td>  	
	        </tr>

	  </form>
</table>

      <tr><td colspan="2"><hr color='skyblue'/>  </td></tr>

</table>
  
</div>
