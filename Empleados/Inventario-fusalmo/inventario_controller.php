<!--
  @author Alejandra Martinez
  @copyright 2016
-->
<?php

			include("Conexion.php"); // llama a la pagina conexión

			$con = mysqli_connect($host,$user,$pw,$db) // realiza la conexion a la base de datos
			or die("problemas al conectar server");


			$accion = $_GET['accion']; // guarda la accion que se envia de la pagina anterior
			$id =$_GET['id']; // guarda el id del material enviado

			//echo $accion;
			//echo $id;

			switch ($accion) {
				case 1:
					eliminar($id,$con); // llama a la funcion eliminar mandandole los parametros recibido del id e incluyendo la conexión
					break;
				case 2:
					eliminar2($id,$con);  // llama a la funcion eliminar2 mandandole los parametros recibido del id e incluyendo la conexión
					break;
			}


			function eliminar($Correlativo_A,$cons){
				//esta funcion se encarga de eliminar el material que le corresponde al id enviado, de la tabla activo fijo
				$consulta1 = "DELETE   FROM Inventario_Activo WHERE Id_Activo=".$Correlativo_A.";";
				//echo($consulta1);
				$registro1 = mysqli_query($cons,$consulta1)
							or die ("problemas en consulta:".mysqli_error($cons));
			}
			function eliminar2($Correlativo_O,$cons){
				//esta funcion se encarga de eliminar el material que le corresponde al id enviado, de la tabla activo fijo
				$consulta2 = "DELETE   FROM Inventario_Oficina WHERE Id_Oficina=".$Correlativo_O.";";
				echo "$consulta2";
				$registro2 = mysqli_query($cons,$consulta2)
							or die ("problemas en consulta:".mysqli_error($cons));
			}
			// direcciona a la página busqueda _inventario
			echo '<script type="text/javascript">
			alert("Registro eliminado con exito");
			window.location.assign("Inventario-fusalmo.php");
			</script>';

?>
