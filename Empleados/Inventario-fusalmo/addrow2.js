function addRow2() {
    /* Declare variables */
    var elements2, templateRow2, rowCount2, row2, className2, newRow2, element2;
    var i, s, t;
    
    /* Get and count all "tr" elements with class="row".    The last one will
     * be serve as a template. */
    if (!document.getElementsByTagName)
        return false; /* DOM not supported */
    elements2 = document.getElementsByTagName("tr");
    templateRow2 = null;
    rowCount2 = 0;
    for (i = 0; i < elements2.length; i++) {
        row2 = elements2.item(i);
        
        /* Get the "class" attribute of the row. */
        className2 = null;
        if (row2.getAttribute)
            className2 = row2.getAttribute('class')
        if (className2 == null && row2.attributes) {    // MSIE 5
            /* getAttribute('class') always returns null on MSIE 5, and
             * row.attributes doesn't work on Firefox 1.0.    Go figure. */
            className2 = row2.attributes['class'];
            if (className2 && typeof(className2) == 'object' && className2.value) {
                // MSIE 6
                className2 = className2.value;
            }
        } 
        
        /* This is not one of the rows we're looking for.    Move along. */
        if (className2 != "row_to_clone2")
            continue;
        
        /* This *is* a row we're looking for. */
        templateRow2 = row2;
        rowCount2++;
    }
    if (templateRow2 == null)
        return false; /* Couldn't find a template row. */
    
    /* Make a copy of the template row */
    newRow2 = templateRow2.cloneNode(true);

    /* Change the form variables e.g. price[x] -> price[rowCount] */
    elements2 = newRow2.getElementsByTagName("TEXTAREA");
    for (i = 0; i < elements2.length; i++) {
        element2 = elements2.item(i);
        s = null;
        s = element2.getAttribute("name");
        if (s == null)
            continue;
        t = s.split("[");
        if (t.length < 2)
            continue;
        s = t[0] + "[" + rowCount2.toString() + "]";
        element2.setAttribute("name", s);
        element2.value = "";
    }
    
    /* Add the newly-created row to the table */
    templateRow2.parentNode.appendChild(newRow2);
    return true;
}
