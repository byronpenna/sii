<?php

/**
  *@author Alejandra Martinez
 * @copyright 2016
 */

$query = "SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo, a.IdAreaDeTrabajo
          FROM CargosAsignacion AS ca
          INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
          INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
          WHERE ca.IdEmpleado = '".$_SESSION["IdUsuario"]."'  and ca.FechaFin = '0000-00-00'";

$MisCargos = $bddr->prepare($query);
$MisCargos->execute();
$DataC = $MisCargos->fetch();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <!-- script que hace que aparesca la venta para introducir la fecha    -->
<script type="text/javascript" src="jquery.js">

 </script>
 <!-- Este script permite mostrar un contenedor oculto que contiene un textbox tipo date para poder seleccionar la fecha-->
<script type="text/javascript">
function mostrar(id)
{

	if (id == "F")
	{
		$("#F").show();
	}else{
		$("#F").hide();
	}
}
</script>



</head>

<body>

 <div style="float:right; width: 75%; text-align: left; background-color: white; padding:10px">

	<table style="width: 100%;">
	<tr><td colspan="2"><h2 style="color: #197198;">B&uacutesqueda de Inventario</h2></td></tr>
	<tr><td colspan="2"><hr color='skyblue' /></td></tr>

	<form action="?l=Seleccion-inventario" method="post" name= "busqueda">


      <td>
        <td>
  		<label><br/><select id="status "name="lista" onchange="mostrar(this.value);">  <!--Son las opciones de busqueda del listbox-->
  				<option value="A">Inventario Activo fijo</option>
  				<option value="P">Inventario Oficina</option>
  				<option value="F">Fecha</option>
  				<option value="1">Bodega #1</option>
  				<option value="2">Bodega #2</option>
  				<option value="3">Bodega #3</option>
  				</select>
  		</label>
      <!-- Al seleccionar la fecha se desplegara actuara el script mencionado arriba-->
  		<div id="F" class="element" style="display:none;">
  		<p>Ingrese fecha:
        <!--Se obtendra la fecha por la cual se quiere buscar en los inventarios -->
  		<input type="date" name="valor_fecha" /></p>
  		</div>

  		<input type="submit" value="Buscar" /></td>
	</form>
	<tr>
    	<td colspan="2"><hr color='skyblue' /></td>
	</tr>
	</table>

  <table  style="width: 100%;">
   <tr><td colspan="2"><h2 style="color: #197198;">Ajustes</h2></td></tr>
   <tr><td colspan="2"><hr color='skyblue' /></td></tr>

<!--Formulario 2 que se direccionara a la pagina de ajustes -->
   <form action="?l=modify" method="post" name="ajuste">

      <td>
        <!-- opcioones que yendra el list box de ajustes-->
        <p>Seleccione una opci&oacuten de inventario para su ajuste.</p>
        <label><br/><select id="modific" name="lista_inv">
         <option value="Af">Activo fijo</option>
         <option value="Of">Oficina</option>
       </select>
     </label>
      <input type="submit" value="Aceptar"  />

	  </form>


</table>



</div>
</body>
</html>
