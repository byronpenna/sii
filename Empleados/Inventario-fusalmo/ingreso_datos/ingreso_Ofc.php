<div style="float:right; width: 77%; text-align: left; background-color: white; padding:10px">
	<table style="width: 100%">
		<tr><td><h2 style="color: #197198;">  Ingreso Inventario Oficina   </h2></td></tr>
		
		<tr><td><h3> Ingreso de Datos: </h3></td></tr>

		
			<!-- Aqui es donde formas el formulario de ingreso -->
			<table style="padding: 10px">
			<form action='controller_ingreso.php' id='InventarioOficinaForm' method='POST' >

				<tr><td>Codigo Activo:</td><td><input style='width: 390px' type='text'  name='codigo' required></td></tr>
			    <tr><td>Descripcion:</td><td><input style='width: 390px' type='text'  name='nombre' required></td></tr>
				<tr><td>Inventario:</td><td><input style='width: 390px' type='number' min='0'  name='inventario' required></td></tr>
				<tr><td>Entrada:</td><td><input style='width: 390px' type='number' min='0'  name='entrada' required></td></tr>
				<tr><td>Salida:</td><td><input style='width: 390px' type='number' min='0'  name='salida' required></td></tr>
			    <tr><td>Cantidad disponible:</td><td><input style='width: 390px' type='number' min='0'  name='cantidad_dis' required></td></tr>
				<tr><td>Marca:</td><td><input style='width: 390px' type='text'  name='marca' required></td></tr>
				<tr><td>Modelo:</td><td><input style='width: 390px' type='text' name='modelo' required></td></tr>
			    <tr><td>Fecha de compra:</td><td><input style='width: 390px' type='text'  placeholder='AAAA-MM-DD' name='fecha' required></td></tr>
				<tr><td>Lugar de compra:</td><td><input style='width: 390px' type='text' name='lugar_compra' required></td></tr>
				<tr><td>Precio de compra:</td><td><input style='width: 390px' type='text'  name='precio' required><td></td></td></tr>
				<tr><td>Valor estimado:</td><td><input style='width: 390px' type='text'  name='valor' required><td></td></td></tr>
				<tr><td>Bodega:</td><td><input style='width: 390px' type='number' min='1' max='3' name='bodega' required><td></td></td></tr>
				<input type='hidden' value='2' name='bandera' > <!-- Esta es la bandera que te ayudará a diferenciar de cual formulario se trata
																ya se el de ingreso de activo fijo o el de ingreso de oficina -->
				
			   

				<tr>
	              <td><input type='submit' class='boton' value='Ingreso' name='Enviar' 	/></td>
	              <td><input type='button' class='boton' value='Cancelar Ingreso' name='cancelar' onclick='history.back()'/></td>	
					</tr>
				</form>
				
				</table>

<tr><td colspan='2'><hr color='skyblue'/>  </td></tr>
</table>
</div>