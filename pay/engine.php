<?php session_start();?>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<?php

/**
 * @author Manuel Calder�n
 * @copyright 2015
 */
 
date_default_timezone_set('Etc/GMT+6');

if(isset($_POST['Enviar']))
{
    
    if(utf8_decode($_POST['Enviar'])== "�Iniciar Donaci�n!")
    {
        if(isset($_POST['nombre'], $_POST['apellido'],$_POST['correo'],$_POST['pais']))
        {
           /**  Establecer conexi�n a la Base de Datos  **/
           $bdd = new PDO('mysql:host=localhost;dbname=poli09_payment', 'poli09_SiipUser', 'fusalmo2013');
           
           /**  Detalles de Valores  **/
           if($_POST['monto'] == "Otro")
                $Monto = number_format( $_POST['montootro'],2);
           
           else
                $Monto = number_format( $_POST['monto'],2);          
           
           $Data = $bdd->prepare("Select * from InformacionCliente ");
           $Data->execute();                                 
           $prefijo = substr(md5(uniqid(rand())),0,6);
           $Id= $prefijo . "-" . ( $Data->rowCount() + 1);
           
           $estado = "En Proceso de Pago";
           $correo = utf8_decode($_POST['correo']);
                
           /**  Insersi�n a la Base de Datos  **/
           $Data = $bdd->prepare("Insert into InformacionCliente values(null, :n1, :n2, :n3, :n4, :n5, :n6, :n7, :n8, :n9) ");
           $Data->bindParam(':n1', $Id);
           $Data->bindParam(':n2', utf8_decode($_POST['nombre']));
           $Data->bindParam(':n3', utf8_decode($_POST['apellido']));
           $Data->bindParam(':n4', $correo);
           $Data->bindParam(':n5', utf8_decode($_POST['pais']));
           $Data->bindParam(':n6', $Monto);
           $Data->bindParam(':n7', utf8_decode($_POST['Option']));
           $Data->bindParam(':n8', date('Y-m-d H:i:s'));
           $Data->bindParam(':n9', $estado);           
           
           if(!$Data->execute())
            echo "<script>location.href ='http://www5.fusalmo.org/index.php?option=com_content&view=article&id=157&Itemid=404&n=5';</script>";
           
           /**  Envio de Correo  **/
           $to = "$correo";
           $subject = "FUSALMO - Acceso para aportar Donativos";
            
           $message = "
                <div style='width: 90%; margin-left: 5%; text-align: justify;'>
                <hr />
                <h2>Saludos Amigo FUSALMO</h2>
                <hr />
                Has accesado para hacer un donativo con ".$_POST['Option'].", tu n&uacute;mero de referencia FUSALMO es: $Id, si tienes alguna duda 
                sobre este proceso puedes contactarnos al fusalmo@fusalmo.org
                <br /><br />
                <center>
                    <img src='http://oportunidades.fusalmo.org/uploads/3/5/6/2/3562495/306605.jpg' /><br />                        
                </center>
                <hr /> 
                <div class='paragraph' style='text-align:center;'>
                    Intersecci&oacute;n Carretera a San Miguel&nbsp;y Calle Antigua a Tonacatepeque&nbsp;<br />
                    Frente a Vidr&iacute; Soyapango.&nbsp;El Salvador
                </div>
                </div>

            ";
            
            // More headers
           $headers .= "From: <noreply-donaciones@fusalmo.org>" . "\r\n";
           $headers .= "Bcc: fusalmo@fusalmo.org, liliaiv@fusalmo.org\r\n";
           
           $headers .= "MIME-Version: 1.0\r\n";
           $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
           //mail($to,$subject,$message,$headers);
           
           
           /** Validaci�n de pago **/
           if(utf8_decode($_POST['Option']) == "pagadito")
           {
                try       
                {                  
                    require_once('config.php');
                    $soap = new SoapClient('https://sandbox.pagadito.com/comercios/wspg/charges.php?wsdl');
                    
                    $params = array(
                        "uid"           => UID,
                        "wsk"           => WSK,
                        "format_return" => "php"
                    );                    
                    $result = $soap->__call('connect', $params) ;
                    
                    $status = substr($result, 34, 6); 
                    $token = substr($result, 104, 32);   
                    
                    $cantidad = "1";

                    $details = array();                    
                    $details[] = array(
                                     "quantity"      => $cantidad,
                                     "description"   => $_POST['descripcion'],
                                     "price"         => $Monto,
                                     "url_product"   => $_POST['url']
                                 );
                                 
                    $params = array(
                        "token"                     => "$token",
                        "ern"                       => $Id,
                        "amount"                    => $Monto * 1,
                        "details"                   => json_encode($details),
                        "format_return"             => "php",
                        "currency"                  => "USD",
                        "custom_params"             => "",
                        "allow_pending_payments"    => true                                            
                    );
                    
                    $response = $soap->__call('exec_trans', $params);                    
                    $status = substr($response, 34, 6);   
                    
                    if($status == "PG1002")       
                        echo "<script>location.href = 'https://sandbox.pagadito.com/comercios/index.php?mod=login&token=$token';</script>";
                        
                    else
                        echo "<script>location.href = 'http://www5.fusalmo.org/index.php?option=com_content&view=article&id=164';</script>";
                    
                }
                catch(Exception $e) 
                {         
                    echo "<script>location.href = 'http://www5.fusalmo.org/index.php?option=com_content&view=article&id=164';</script>";  
                }                                    
           }
           
           else if(utf8_decode($_POST['Option']) == "paypal")
           { 
                echo "<form action='https://www.paypal.com/cgi-bin/webscr' method='post' name='paypal'>
                        <input type='hidden' name='cmd' value='_s-xclick'>
                        <input type='hidden' name='hosted_button_id' value='FEPHR7J6LUBHS'>
                        <img alt='' border='0' src='https://www.paypalobjects.com/en_US/i/scr/pixel.gif' width='1' height='1' onload='javascript:document.paypal.submit();'>
                      </form>";
           }
           
           else
              echo "<script>location.href ='http://www5.fusalmo.org/index.php?option=com_content&view=article&id=157&Itemid=404&n=6';</script>";
        }
        else
            echo "<script>location.href ='http://www5.fusalmo.org/index.php?option=com_content&view=article&id=157&Itemid=404&n=4';</script>";
    }
    else
        echo "<script>location.href ='http://www5.fusalmo.org/index.php?option=com_content&view=article&id=157&Itemid=404&n=3';</script>";
}

if(isset($_GET['n1'],$_GET['n2']))
{
       unset($_SESSION['statustrans'],$_SESSION['ref'],$_SESSION['datetime'],$_SESSION['status'],$_SESSION['data'],$_SESSION['ern']);
       
       require_once('config.php');
       $soap = new SoapClient('https://sandbox.pagadito.com/comercios/wspg/charges.php?wsdl');
       $bdd = new PDO('mysql:host=localhost;dbname=poli09_payment', 'poli09_SiipUser', 'fusalmo2013');
       $ern = $_GET['n2']; 
       $params = array(
           "uid"           => UID,
           "wsk"           => WSK,
           "format_return" => "php"
       );                    
       $result = $soap->__call('connect', $params) ;
       $StatusConnection = substr($result, 34, 6);
       
       
       
       
       if("PG1001" == $StatusConnection)
       { 
           $token_trans = substr($result, 104, 32);                                                   
           $token = $_GET['n1'];
           
           $params = array(
                "token"         => $token_trans,
                "token_trans"   => $token,
                "format_return" => "php"
           );
            
           $result = $soap->__call('get_status', $params) ;                            
           $codtrans = substr($result, 34, 6);
           
           if($codtrans == "PG1003")       
           {
               $statustrans = substr($result, 118, 9);
               echo "$result";
               if($statustrans == "COMPLETED" || $statustrans == "VERIFYING")
               { 
                  $_SESSION['statustrans'] = $statustrans; 
                  $_SESSION['ref'] =      substr($result, 150, 8);  
                  $_SESSION['datetime'] = substr($result, 184, 19);
               }
               else
               {               
                   $statustrans = substr($result, 119, 10);
                   if($statustrans == "REGISTERED")
                   {
                      $_SESSION['statustrans'] = $statustrans;   
                      $_SESSION['datetime'] = substr($result, 212, 19);
                   }                          
                   else if($statustrans == "REVOKED")
                   {
                      $statustrans = substr($result, 118, 7);
                      $_SESSION['statustrans'] = $statustrans;                        
                      $_SESSION['datetime'] = substr($result, 182, 19);                
                   }
               }   
                             
               $_SESSION['status'] = "Process"; 
               $_SESSION['data'] = "Petici�n llego al servidor";
               $_SESSION['ern']  = $ern;
               
                $Update = $bdd->prepare("Update InformacionCliente set Estado = 'Petici�n procesada' where IdInformacion = '$ern'");
                $Update->execute();               
                
           }
           else
           {
                $Update = $bdd->prepare("Update InformacionCliente set Estado = 'Petici�n no procesada' where IdInformacion = '$ern'");
                $Update->execute();
            
                $_SESSION['status'] = "Bad Process"; 
                $_SESSION['data'] = "No se ha sido procesada correctamente la petici�n de estado de transacci�n.";
           }
       }
       else
       {
            
            $Update = $bdd->prepare("Update InformacionCliente set Estado = 'Cancelado' where IdInformacion = '$ern'");
            $Update->execute();
            
            $_SESSION['status'] = "Cancel";
            $_SESSION['data'] = "No fue posible la conexi&oacute;n o su transacci&oacute;n fue cancelada";
            echo "<script language='javascript'>window.location='../?option=com_content&view=article&id=164'</script>";
       }
           
       echo "$StatusConnection  - $codtrans  -  $statustrans<br />".$_SESSION['status'] ." <br /> ".$_SESSION['statustrans'] ." <br /> ".$_SESSION['ref']. " <br /> ".$_SESSION['datetime']." <br /> " .  $_SESSION['ern'];    
                    
       
       if($statustrans == "COMPLETED")
       {  
            
             
           /**  Envio de Correo  **/
           $to = "calderon.m@live.com, manu.calderon06@gmail.com";
           $subject = "FUSALMO - Detalle de Transacci�n";
            
           $message = "
                <div style='width: 90%; margin-left: 5%; text-align: justify;'>
                <hr />
                <h2>Amigo $DataC[0] $DataC[1]</h2>
                <hr />
                Te mostramos los detalles de la transferencia 
                Estado de la transferencia: ". $_SESSION['status']  ."<br />
                Referencia FUSALMO: ". $_GET['n2'] ."<br />
                Referencia Pagadito: ". $_SESSION['ref'] ."<br />
                <br /><br />
                                
                Entra en <a href='http:www5.fusalmo.org/'>http:www.fusalmo.org/</a>
                para conocer mas acerca de nosotros y nuestro prouectos por la Juventud.   
                <br /><br />
                <center>
                    <img src='http:oportunidades.fusalmo.org/uploads/3/5/6/2/3562495/306605.jpg' /><br />                        
                </center>
                <hr /> 
                <div class='paragraph' style='text-align:center;'>
                    Intersecci&oacute;n Carretera a San Miguel&nbsp;y Calle Antigua a Tonacatepeque&nbsp;<br />
                    Frente a Vidr&iacute; Soyapango.&nbsp;El Salvador
                </div>
                </div>

            ";

           $headers .= "From: <noreply-donaciones@fusalmo.org>" . "\r\n";
           //$headers .= "Bcc: fusalmo@fusalmo.org, liliaiv@fusalmo.org\r\n";
           $headers .= "Bcc: m.calderon@fusalmo.org\r\n";
           
           $headers .= "MIME-Version: 1.0\r\n";
           $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
           if(mail($to,$subject,$message,$headers)) 
            echo "<br />111111 - "; 
            
           else
            echo "<br />222222";
           //echo "<script language='javascript'>window.location='../?option=com_content&view=article&id=164'</script>";
        }          
}

$to = "calderon.m@live.com, manu.calderon06@gmail.com";
$subject = "FUSALMO";

$message = "<hr />Pruebaaaa";
// More headers

$headers .= "From: <oportunidades_noreply@fusalmo.org>" . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

if(mail($to,$subject,$message,$headers))
    echo "email sent $to";
else
    echo "unsent mail";
            
            //echo "<script>location.href ='http://www5.fusalmo.org/index.php?option=com_content&view=article&id=157&Itemid=404&n=1';</script>";
?>