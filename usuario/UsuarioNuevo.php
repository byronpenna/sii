
            <?php
                   if($_SESSION["IdUsuario"] != "")
                   {
                       if(isset($_POST['NuevoUsuario']))
                       {  
                            $regresar = "Main.php?l=UsuL";
                            $texto = "<- Regresar";
                            $abm = "Agregar"  ;
                       }
                       else
                       {
                           $lista = $bdd->prepare("SELECT * FROM usuario WHERE Id = :id ");
                           $lista->bindParam(':id' , $_SESSION["IdUsuario"]);
    	                   $lista->execute();
                           
                           $datosUsuario = $lista->fetch();  
                           
                           $regresar = "../Main.php";
                           $texto = "<- Regresar al Inicio";
                           $abm = "Actualizar";                           
                       }          
                   }
                   else
                   {
                       $regresar = "../index.php";
                       $texto = "<- Regresar a Inicio";
                       $abm = "Ingresar"  ;
                                      
            ?>
            <ul>Indicaciones:
	               <li>Se lo mas sincero, tus datos seran muy confidenciales y nos serviran para darte un mejor servicio.</li>
	               <li>trata de llenar todos los campos posibles.</li>
	               <li>Los <span style="color: red;">*</span> son campos Obligatorios. </li>
                </ul>            
            <?php }?>
            <hr style="color: skyblue; background-color: skyblue; height: 5px;" />
            <h2>Usuarios del Sistema</h2><br />
            <form action="usuario/UsuarioABM.php" method="post" enctype="multipart/form-data" name="Proceso" id="Proceso">
            <table style="width: 80%; margin-left: 10%; margin-bottom: 20px;">
            <tr><td style="text-align: right; padding-right: 20px;">Escribe un nombre de Usuario:</td>
            
                <td style="color: red;"><input required="true" title="Escribe tu Nombre de Usuario"  type="text" id="Nombre" name="Nombre" style="width: 150px;" value="<?php echo $datosUsuario[1]?>"/>*</td>
                
                <?php
	               if(isset($_GET['e']))
                   {
                        if($_GET['e'] == 1)
                            echo "<td style='color: red;' id='Notificacion' ><br /><div>Nombre de Usuario existente, <br />trata con otro</div></td>";
                   }
                ?></tr>

            <tr><td style="text-align: right; padding-right: 20px;">Tu nueva contrase&ntilde;a:</td>
                <td style="color: red;"><input required="true" title="Escribe tu password" type="password" id="pass1" name="pass1" style="width: 150px;" value="<?php echo $datosUsuario[3]?>"/>*</td></tr>                
                
            <tr><td style="text-align: right; padding-right: 20px;">Confirmemos tu contrase&ntilde;a:</td>
                <td style="color: red;"><input required="true" title="Reescribe tu password" type="password" id="pass2" name="pass2" style="width: 150px;" value="<?php echo $datosUsuario[3]?>"/>*</td></tr>

<?php   if($abm == "Agregar"){ ?>
            <tr><td style="text-align: right; padding-right: 20px;">Tipo de Usuario</td>
                <td><select name="tipoUsuario" id="TipoUsuario">
      
                    <option value="Administrador">Administrador</option>
      
                    <option value="Consultor">Consultor</option>
      
                    <option value="RRHH">RRHH</option>                                                             
      
                    <option value="Coordinador">Coordinador</option>
                    <option value="Gestor">Gestor</option>
                    <option value="Joven">Joven</option>
                    <option value="Desarrollo">Desarrollo</option>
      
       <!-- Consultores USAID Equipo --!>                     
                    <option value="Buscador">Buscador</option>
                    
       <!-- Consultores USAID Equipo --!> 
                    <option value="USAID-CI">USAID-CI</option>                                                            
                    <option value="USAID-SI">USAID-SI</option>
                    <option value="USAID-SE">USAID-SE</option>    
                                    
       <!-- Consultores USAID Equipo --!> 
                </select></td></tr>
<?php } ?>
            <tr><td style="text-align: right; padding-right: 20px;">&iquest;De cual polideportivo?</td>
                <td><select name="Localidad" id="Localidad" style="125px">
	               <?php if($datosUsuario[4] == "Soyapango") 
                            echo "<option selected='true' value='Soyapango'>Polideportivo Soyapango</option>";
                         else
                            echo "<option value='Soyapango'>Polideportivo Soyapango</option>";
                            
                         if($datosUsuario[4] == "Multigimnasio") 
                            echo "<option selected='true' value='Multigimnasio'>Multigimnasio</option>";
                         else
                            echo "<option value='Multigimnasio'>Multigimnasio</option>";   

                         if($datosUsuario[4] == "Santa Ana") 
                            echo "<option selected='true' value='Santa Ana'>Polideportivo Santa Ana</option>";
                         else
                            echo "<option value='Santa Ana'>Polideportivo Santa Ana</option>";   
                            
                         if($datosUsuario[4] == "San Miguel") 
                            echo "<option selected='true' value='San Miguel'>Polideportivo San Miguel</option>";
                         else
                            echo "<option value='San Miguel'>Polideportivo San Miguel</option>";                                                                                                             
                   ?>
                    </select>
                </td></tr>   
                
            
            <tr>
                <td><center><a href="<?php echo $regresar; ?>"><input id="boton1" name="boton1" class="boton" type="button" value="<?php echo $texto; ?>" style="width: 150px;" /></a></center></td>
                <td><center><input id="Enviar" name="Enviar" class="boton" type="submit" value="<?php echo $abm; ?>" style="width: 150px;" onClick='return VerificarUser();' /></center></td>
            </tr>                                                                                                
            </table>                
            </form>

            <div class="clr"></div>      
