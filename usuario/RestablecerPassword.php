<?php
	require '../net.php';
?>
<html>
<head>
<title>Inicio SIIPDB</title>
<?php 
include("../lib.php");
?>
	<script type="text/javascript">
        $(document).ready(function () {

            $(document).ready(function () {
                $('#Notificacion').fadeOut(2500);
            });

        });
    </script>
</head>
<body>
<?php
	if(isset($_POST['Enviar']))
    {
        $Usuario = $_POST['UserName'];
        
        $Revision = $bdd->prepare("Select * from usuario where NombreUsuario = '$Usuario'");
        $Revision->execute();
        
        if($Revision->rowCount() == 0)        
            $Resultado = "<div style='text-align: center; color: red' id='Notificacion'>Error, nombre de usuario no existente</div>";
        
        else
        {
            $DataU = $Revision->fetch();
            
            if($DataU[2] == "Joven")
            {
                $Email = $bdd->prepare("SELECT Correo FROM joven where IdUsuario = $DataU[0]");
                $Email->execute();                
                $DataE = $Email->fetch();                
                $Correo = $DataE[0];                
            }
            else
            {                                
                $Email = $bddr->prepare("SELECT Correo FROM DatosInstitucionales where IdEmpleado = $DataU[0]");
                $Email->execute();  
                
                if($Email->rowCount() > 0)
                {
                    $DataE = $Email->fetch();                    
                    $Correo =  $DataE[0];
                }
                else
                {
                    $Email = $bddr->prepare("SELECT Correo FROM Empleado where IdEmpleado = $DataU[0] and Correo != ''");
                    $Email->execute();                    
                    $DataE = $Email->fetch();                    
                    $Correo =  $DataE[0];                    
                }      
            }            
        }            
    }
    else
        $Resultado = "";
?>
<div class="main">

        <div class="main_resize"><div class="header">
         <header class="clearfix">               
        <div style="float: left; width: 20%; margin-top: 12px; "><img src="../assets/img/logo-header.png"></div><div style=" float: right; width: 80%; margin-top: 12px;"><h1 style="font-family: calibri; font-size: 32pt;"><span>Sistema de Informaci&oacute;n Institucional FUSALMO</span></h1></div>
        <div class="clr"></div>
    <div class="main_resize">   
        <div class="content">                   
        <h1>Restablecer contraseña</h1>
            <div style="width: 80%; margin-left: 10%; background-color: white; border-radius: 10px;">
            <br /><br />                            
                <table style="width: 80%; margin-left: 10%;">
                <?php
                    if($Correo == "")
                    {         
                        
                ?>
                <form action="" method="post">
                    <tr><td>Si tu has guardado en tu perfil tu correo electronico institucional o correo electronico personal llegará la notificación a tu correo, caso contrario de no haber guardado 
                            estos datos te sugerimos que te comuniques con el encargado del SIIPDB en Tecnología e Innovacion.</td></tr>
                    <tr><td><br />Escribe tu nombre de usuario:<br /><br /></td></tr>
                    <tr><td><input type="text" name="UserName" required="true" style="width: 60%; margin-left: 20%;" /></td></tr>
                    <tr><td style="text-align: center;"><br /><input type="submit" name="Enviar" value="Reestablecer Contraseña" class="boton" style="width: 150px;" /></td></tr>
                
                </form>
                <?php
                        echo "$Resultado";
                	}
                    else
                    {
    
                        $to = $Correo;
                        $subject = "Recordatorio de Contraseña";
                        
                        $message = "
                        
                        Se le enviado sus datos de Usuario
                        ------------------------------------------------------
                        Usuario:    $DataU[1]
                        Contraseña: $DataU[3]
                        ------------------------------------------------------
                        Fecha de Solicitud:    ".date("Y-m-d H:i:s")."
                        
                        Si usted no ha solicitado la contraseña, favor acercarse 
                        Tecnología e Innovación y contacte con el administrador
                        del SIIPDB.
                        
                        https://siipdb.fusalmo.org/";
                        
                        // More headers
                        $headers .= 'From: <no-reply@fusalmo.org>' . "\r\n";
                        $headers .= 'Cc: desarrollo@fusalmo.org' . "\r\n";
                    
                       if(mail($to,$subject,$message,$headers))
                       {
                            $CorreoCifrado = substr($Correo,0,2);
                            $Arroba = 0;
                            for($i=2; $i<strlen($Correo); $i++)
                            {                                
                                if(substr($Correo,$i,1) == "@")                                
                                    $Arroba = $i;
                                
                                if($Arroba > 0)
                                    $CorreoCifrado .= substr($Correo,$i,1);
                                
                                else
                                    $CorreoCifrado .= "*";    
                            }

                            echo "<tr>
                                      <td style='text-align: center'><a href='https://siifusalmo.org' style='color: blue; text-decoration: none;'>
                                        <strong>Se ha enviado un correo a: $CorreoCifrado<br />vuelve al inicio dando clic aqui</strong></a>
                                      </td></tr>";
                       }
                       else
                       {
                           echo "<tr><td style='text-align: center'><a href='https://siifusalmo.org'>Error, al enviar el correo puede que no poseas registrado un correo en tus datos o haya un problema en el servidor</a></td></tr>";
                       } 
                    }
                ?>
                </table>
            <br /><br />
            </div>
        </div>
        <br /><br />
        <div class="clr"></div>      
    </div>
</div>
<div class="fbg"></div>
<div class="footer"><?php include("../footer.php");?></div>
</body>
</html>