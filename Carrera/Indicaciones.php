<?php

/**
 * @author Manuel Calder�n
 * @copyright 2014
 */

?>
<script type="text/javascript">

function Aceptar()
{
    var chequeado = document.getElementById("terminos").checked;
    if(chequeado)
    {
        $(document).ready(function () 
        {
            $('#siguiente').fadeIn(800);
        });
    }
    else
    {
        $(document).ready(function () 
        {
            $('#siguiente').fadeOut(800);
        });        
    }
}

function Inscripcion()
{
    document.getElementById('MainDiv').innerHTML = "";
    $(document).ready(function() {
    	$.get("Inscripcion.php",
        		function(resultado)
        		{
        		    document.getElementById('MainDiv').innerHTML = "<em style='color: green;'>Cargando Formulario...</em>";
        			if(resultado == false)        			
        				alert("Error");
        			
        			else
                    {
                        document.getElementById('MainDiv').innerHTML = "";
                        $('#MainDiv').append(resultado);	        			
                    }       			          				
        		}        
        	);
  });
}
</script> 

<table style="font-family: calibri; text-align: justify; width: 90%;">
<tr><td colspan="2"><h2>Reglamento e Indicaciones</h2></td></tr>
<tr><td colspan="2"><hr color='skyblue' /></td></tr>
<tr>
    <td colspan="2">
    <img src="Infografia.jpg" />
    </td>
</tr>
<tr><td colspan="2"> <hr color='skyblue' /></td></tr>
<tr><td style="padding-left: 20px;">
    <input style="padding-right: 10px;" type="checkbox" id="terminos" onclick="Aceptar();"/>
    Acepto el <strong>Reglamento</strong> y las <strong>Indicaciones</strong> de esta Carrera</td>
    <td style="text-align: right;">
    <input class="boton" id="siguiente" onclick="Inscripcion();" type="button" value="Inscribirme" style="display: none;" /></td></tr>
</table>