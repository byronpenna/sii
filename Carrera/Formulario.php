<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */



?>

<tr><td colspan="2"> <hr color='skyblue' /></td></tr>
<tr><td class='tdleft'>Nombre del Atleta:</td><td><input type="text" id="NombreAtleta" name="NombreAtleta" value="" style="width: 260px;" required="true" /></td></tr>
<tr><td class='tdleft'>Fecha de Nacimiento:</td>
    <td>
    <table>
        <tr>
       	    <td style='font-size:10pt; text-align: center;'>A&ntilde;o</td>
           	<td style='font-size:10pt; text-align: center;'>Mes</td>
           	<td style='font-size:10pt; text-align: center;'>D&iacute;a</td>
        </tr>
        <tr>
            <input type="hidden" name="edad" id="edad" value="" />
            <td>
                <select id="Anio" name='Anio' onblur="Categorias();">
                <?php
                for($i = date('Y'); $i >= 1970; $i--)                
                    echo "<option value='$i'>$i</option>";                                                
                
                ?>
                </select>
            </td>
            <td>
                <select id="Mes" name='Mes' onblur="Categorias();" >   
                <option value='01'>Enero</option>                             
                <option value='02'>Febrero</option>
                <option value='03'>Marzo</option>
                <option value='04'>Abril</option>
                <option value='05'>Mayo</option>
                <option value='06'>Junio</option>
                <option value='07'>Julio</option>
                <option value='08'>Agosto</option>
                <option value='09'>Septiembre</option>
                <option value='10'>Octubre</option>
                <option value='11'>Noviembre</option>
                <option value='12'>Diciembre</option>
                </select>
            </td> 
            <td>
                <select id="Dia" name='Dia' onblur="Categorias();"> 
                <?php
                    for($i = 1; $i <= 31; $i++)
                        echo "<option value='$i'>$i</option>";                                                                   
                ?>                               
                </select>
            </td>                                 
        </tr>
    </table>  
    </td>
</tr>
<tr>
    <td class='tdleft'>Documento de Identidad:</td>
    <td><input type="text" id="Dui" name="Dui" value="" style="width: 260px;" /></td>
</tr>
<tr>
    <td class='tdleft'>Club/Empresa/Instituci&oacute;n:</td>
    <td><input type="text" id="Institucion" name="Institucion" value="" style="width: 260px;" required="true" /></td>
</tr>
<tr>
    <td class='tdleft'>Pais:</td>
    <td><input type="text" id="Pais" name="Pais" value="" style="width: 260px;" required="true" /></td>
</tr>
<tr>
    <td class='tdleft'>Correo Electronico:</td>
    <td><input type="text" id="email" name="email" value="" style="width: 260px;" /></td>
</tr>
<tr>
    <td class='tdleft'>Telefono:</td>
    <td><input type="text" id="Telefono" name="Telefono" value="" style="width: 260px;" /></td>
</tr>

<tr><td colspan="2"><hr color='skyblue' /></td></tr>
<tr>
    <td class='tdleft'>Seleccione su categoria:</td>
    <td>
        <select id="categorias"  name="categorias" style="color: blue; width: 150px;">
            <option><em>Esperando datos...</em></option>
        </select>
        <input type="button" value="Ver Categoria" onclick="CargarCategoria();" class="boton" />
    </td>    
</tr>
<tr><td colspan="2"><hr color='skyblue' /></td></tr>
<tr>
    <td colspan="2">
        <div id="CategoriaDiv"></div>
    </td>
</tr>
</table>
