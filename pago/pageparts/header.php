<?php
session_start();
require_once('conexion.php');

if($_SESSION['tipousuario']=='usuario') {

$idinscripciones = $_SESSION['inscripciones_idinscripciones'];



$sql = "SELECT * FROM subservicios sb
inner join servicio se
on sb.servicio_idservicio = se.idservicio
inner join calendario ca
on sb.idsubservicios = ca.subservicios_idsubservicios";

$sqlsb = "SELECT * FROM subservicios sb
inner join servicio se
on sb.servicio_idservicio = se.idservicio
inner join inscripciones ins
on se.idservicio = ins.servicio_idservicio
WHERE ins.idinscripciones = $idinscripciones";

$sql2 = "SELECT * FROM inscripciones ins
inner join calendario ca
on ca.inscripciones_idinscripciones = ins.idinscripciones
WHERE ins.idinscripciones = $idinscripciones";

$req2 = $con->prepare($sql2);
$req2->execute();
$countcalendario = $req2->rowCount();

$req2 = $con->prepare($sqlsb);
$req2->execute();
$countsubservicio = $req2->rowCount();



/*******************************************************************/

$todo = $con->query("select * from inscripciones where idinscripciones = $idinscripciones");
$todo->execute();

$n=$todo->fetchAll();

$nom = "";
foreach ($n as $row) {
	$nom = $row['nombre']; 
}

/*******************************************************************/





}
elseif($_SESSION['tipousuario']=='administrador') {

$sql = "SELECT * FROM subservicios sb
inner join calendario ca
on sb.idsubservicios = ca.subservicios_idsubservicios
INNER JOIN inscripciones ic
ON ca.inscripciones_idinscripciones = ic.idinscripciones";

}
else {
	header('Location: index.php?er=true');
}


$req = $con->prepare($sql);
$req->execute();

$events = $req->fetchAll();

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Inicio</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- FullCalendar -->
	<link href='css/fullcalendar.css' rel='stylesheet' />
	
	<!--Bootstrap-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <!-- Custom CSS -->
    <style>
    body { padding-bottom: 4%; }
	#calendar {
		max-width: 800px;
	}
	.col-centered{
		float: none;
		margin: 0 auto;
	}
    </style>

    <script type="text/javascript">
    	function validarHora(){
    		var h = document.getElementById('iniciotime').value;
    		alert(h);
    	}
    </script>


</head>
<body>

<?php 
if ($_SESSION['tipousuario']=='administrador') {
?>
	<!-- -----------------------------------------MENU----------------------------------------- -->
	<div>
		<nav class="navbar navbar-default" role="navigation">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	        </div>
	        <div class="navbar-collapse collapse">
	        	<ul class="nav navbar-nav navbar-left">
	                <li><a href="calendar.php">Calendario</a></li>
	            </ul>
	            <ul class="nav navbar-nav navbar-left">
	                <li><a href="tablainscripcion.php">Inscritos</a></li>
	            </ul>
	            <ul class="nav navbar-nav navbar-left">
	                <li><a href="tablapagos.php">Pagados</a></li>
	            </ul>
	            <ul class="nav navbar-nav navbar-left">
	                <li><a href="tablacalendario.php">Calendarizados</a></li>
	            </ul>
	            <ul class="nav navbar-nav navbar-left">
	                <li><a href="importar.php">Actualizar NPE</a></li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
	            	<li><a href="cerrar.php">Cerrar Sesion (Admin)</a></li>
	            </ul>
	            <ul class="nav navbar-nav navbar-left">
	            	<li>
	            		<a data-toggle="modal" data-target="#modal-establecer-fechas">Establecer fechas disponibles</a>
	            	</li>
	            </ul>
	        </div>
	    </nav>
	</div>
<?php } else {?>
	<div>
		<nav class="navbar navbar-default" role="navigation">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	        </div>
	        <div class="navbar-collapse collapse">
	            <ul class="nav navbar-nav navbar-left">
	                <li><a>Bienvenido <b><?php echo "$nom"; ?></b></a></li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
	            	<li><a href="cerrar.php">Cerrar Sesion</a></li>
	            </ul>
	        </div>
	    </nav>
	</div>
<?php } ?>