<?php require('pageparts/header.php'); ?>
<div class="container">
	<?php
	if (isset($_GET['err']) || isset($_GET['er'])) {

		echo ("<center>
			<div class='alert alert-danger' role='alert'>
			Ese horario ya se encuentra definido
			</div>
			</center>");
	}
	if (isset($_GET['ter'])) {
		echo ("<center>
			<div class='alert alert-danger' role='alert'>
			La fecha es incorrecta
			</div>
			</center>");
	}
	?>
	<div class="row">
		<div class="col-lg-12 text-center">
			<h1>Calendarización</h1>
			<p class="lead">Completa la fecha para agendar test, entrevista y talleres predefinidas que no tendrás que cambiar!</p>
			<div id="calendar" class="col-centered">
			</div>
		</div>

	</div>
	<!-- /.row -->

	<?php if($_SESSION['tipousuario']=='usuario' && $countcalendario != $countsubservicio){?>
		


		<!-- Modal Agregar-->
		<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form class="form-horizontal" method="POST" action="addEvent.php">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Agregar Evento</h4>
						</div>
						<div class="modal-body">
							<input type="hidden" name="idinscripciones" class="form-control" 
							id="title" placeholder="" value="<?php echo $idinscripciones; ?>">
							<div class="form-group">
								<label for="title" class="col-sm-2 control-label">Servicio</label>
								<div class="col-sm-10">
									<select class="form-control" name="idsubservicios" id="idsubservicios">
										<?php

										$idinscripciones = $_SESSION['inscripciones_idinscripciones'];
										$sql2 = ("SELECT * FROM  subservicios sb 
											inner join servicio se
											on sb.servicio_idservicio = se.idservicio
											INNER JOIN inscripciones ins 
											on se.idservicio = ins.servicio_idservicio 
											where ins.idinscripciones = $idinscripciones");

										$query2 ="SELECT * FROM calendario ca
										inner join subservicios sb
										on ca.subservicios_idsubservicios = sb.idsubservicios
										WHERE ca.inscripciones_idinscripciones =$idinscripciones";

										$servicio = $con->prepare($query2);
										$servicio->execute();
										$servicios = $servicio->fetchAll();
										$x2=$servicio->rowCount();
										$req2 = $con->prepare($sql2);
										$req2->execute();
						//$req2->fetchAll();
										
										$x=0;
										while($row = $req2->fetch()){

											if($servicios[$x]['subservicio'] != $row['subservicio']){
												
												echo"<option value='".$row['idsubservicios']."'>".$row['subservicio']."</option>";

											}
											else{
												$x++;
											}

										}
										
										?>
									</select>

								</div>
							</div>
							<div class="form-group">
								<label for="start" class="col-sm-2 control-label">Fecha</label>
								<div class="col-sm-10">
									<input type="text" name="start" class="form-control" id="start" readonly>
								</div>
							</div>
							<div class="form-group">
								<label for="start" class="col-sm-2 control-label">Hora Inicio </label>
								<div class="col-sm-10">
									<input type="time" id="title" name="iniciotime" min="07:00" max="16:00" required>
									<span class="note">Horario entre 7am y 4pm</span>
								</div>
							</div>
							<p id="nota">
								
							</p>
							<p id="nota2">
								
							</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
							<button id="guardar" type="submit" name="guardar" class="btn btn-primary">Guardar</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- fin modal agregar -->

		<?php 
		
		$queryfechas = "select idsubservicios, subservicio, fecha_ini, fecha_fin, dia from subservicios";

							$fecha = $con->prepare($queryfechas);
							$fecha->execute();
							$fechas = $fecha->fetchAll();

		 ?>

		 <script type="text/javascript">
		 	window.onload = function(){

		 		<?php echo "var fechas = ".json_encode($fechas).";\n"; ?>
		 		
		 		///////////////
		 		document.getElementById('idsubservicios').onchange = function(){verFechas();}
		 		verFechas();

		 		function verFechas(){
		 			var diasarray = ["domingo","lunes","martes","miercoles","jueves","viernes","sabado"];

		 			fechas.forEach(function(value, index, array){
		 				if (document.getElementById('idsubservicios').value == value[0]) {
		 					var diasdisp = value[4].split("");
		 					document.getElementById('nota').innerHTML = "Fecha disponible: desde <b>"+value[2]+"</b> hasta <b>"+value[3]+"</b>";
		 					document.getElementById('nota2').innerHTML = "Dias disponibles: <br>";
		 					diasdisp.forEach(function(value2, index2, array2){
		 						document.getElementById('nota2').innerHTML += "<b>"+diasarray[parseInt(value2)]+", </b>";
		 					})
		 				}
		 			})
		 		

		 		}

		 		//////////////

		 		document.getElementById('guardar').onclick = function(event){
		 			var inputSubServicio = document.getElementById('idsubservicios');
		 			var inputFecha = document.getElementById('start');
		 			fechas.forEach(function(value, index, array){
		 				
		 				if (inputSubServicio.value == value[0]) {
		 					//alert("fecha nimina: "+value[2]+"\nfecha maxima: "+value[3]+ "\nfecha elegida: "+ inputFecha.value);
		 					var fechaInicio = new Date(value[2]);
							var fechaFinal = new Date(value[3]);
							var fechaElegida = new Date(inputFecha.value);
							var diaspermitidos = value[4].split("");
							
							//alert("dias permitidos: "+value[4]+"\n dia elegido: "+fechaElegida.getDay());

							if (fechaElegida > fechaInicio && fechaElegida < fechaFinal){
								var diaValido = false;
								diaspermitidos.forEach(function(value2, index2, array2){
									if (parseInt(value2) == fechaElegida.getDay()) {
										diaValido = true;
									}
								})
								if (diaValido == false) { 
									alert("Dia de la semana no disponible");
									event.preventDefault();
								}	
							}
							else{
								alert("fecha no disponible");
								event.preventDefault();
							}
		 				}
		 			})
		 		}
		 	}
		 </script>



	<?php } 

	$en = $_SESSION['tipousuario'] === 'usuario' ? "disabled" : '' ;
	?>

	<!-- Modal Editar-->
	<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<form class="form-horizontal" method="POST" action="addEvent.php">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Evento</h4>
					</div>
					<div class="modal-body">

						<input type="hidden" name="idcalendario" class="form-control" 
						id="idcalendario" >

						<input type="hidden" name="idservicio2" class="form-control" 
						id="idservicio2" >

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Servicio</label>
							<div class="col-sm-10">
								<input type="text" name="title" class="form-control" id="title" placeholder="Servicio" disabled="">
							</div>
						</div>

						<div class="form-group">
							<label for="start" class="col-sm-2 control-label">Fecha</label>
							<div class="col-sm-10">
								<input type="date" name="start" class="form-control" id="start" <?php echo "$en"; ?>>
							</div>
						</div>

						<div class="form-group">
							<label for="start" class="col-sm-2 control-label">Hora Inicio </label>
							<div class="col-sm-10">
								<input type="time" id="iniciotime" name="iniciotime" min="07:00" max="16:00"
								<?php echo "$en"; ?>>
								<span class="note">Horario (1 Hora)</span>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-10">
								<input type="hidden" id="iniciotime0" name="iniciotime0" min="07:00" max="16:00">
							</div>
						</div>


					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-sucess" data-dismiss="modal">Cerrar</button>
						<?php 
						if ($_SESSION['tipousuario']=='administrador') {
							echo "<button type='submit' class='btn btn-danger' name='eliminar' value='1'>Eliminar</button>
							<button type='submit' class='btn btn-primary' name='modificar'>Modificar</button>";
						}
						?>
					</div>
				</form>
			</div>
		</div>
	</div>




<!-- Modal establecer fechas -->
<div id="modal-establecer-fechas" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Defina las fechas que estaran disponibles</h4>
      </div>
      <div class="modal-body">
        <form action="calendar.php" method="POST">
        	<div>
        		<div class="form-group">
	        		<label for="select-subservicio" >Elija un subservicio</label>
	        		<select id="select-subservicio" class="form-control form-control-lg" name="subservicioform">
	        			<?php 

	        			$querySubServicios = "select subservicio from subservicios";

							$subServicio = $con->prepare($querySubServicios);
							$subServicio->execute();
							$subServicios = $subServicio->fetchAll();
							foreach ($subServicios as $value) {
								echo "<option value='$value[0]'>$value[0]</option>";
							}

	        			 ?>
	        		</select>
	        	</div>
	        	<div class="form-group">
	        		<label for="date-ini-fin">Elija la fecha de inicio y fecha fin</label>
	        		<div class="row">
		        		<div class="col">
		        			<input class="form-control" id="date-ini-fin" type="date" name="fechaIni" required="">	
		        		</div>
		        		-
		        		<div class="col">
		        			<input class="form-control" id="date-ini-fin" type="date" name="fechaFin" required="">	
		        		</div>
	        		</div>
        		</div>
        		<div class="form-group">
        			<label for="dia">Elija los dias de la semana</label>
        			<select class="form-control" id="dia" name="dias[]" multiple="" required="">
        				<option value="1">lunes</option>
        				<option value="2">martes</option>
        				<option value="3">miercoles</option>
        				<option value="4">jueves</option>
        				<option value="5" >viernes</option>
        				<option value="6">sabado</option>
        				<option value="0" >domingo</option>
        			</select>
        			<p style="font-size: 10pt; color: gray">(mantega presionado ctrl y seleccione los dias)</p>
        		</div>
        		<div class="form-group">
        			<input class="btn btn-success" type="submit" name="establecer_fecha" value="listo">
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<?php 

if (isset($_POST['establecer_fecha'])) {
	extract($_POST);
	$diass = "";

	echo "$subservicioform <br>";
	echo "$fechaIni <br>";
	echo "$fechaFin <br>";
	foreach ($dias as $value) {
		$diass .= $value;
	}
	echo $diass;


	$queryFechasDisp = "update subservicios set 
						fecha_ini = '$fechaIni', 
						fecha_fin = '$fechaFin', 
						dia = '$diass' 
						where subservicio = '$subservicioform'";

							$stmt = $con->prepare($queryFechasDisp);
							$stmt->execute();

}

 ?>

</div>
<!-- /.container -->

<!-- jQuery Version 1.11.1 -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- FullCalendar -->
<script src='js/moment.min.js'></script>
<script src='js/fullcalendar/fullcalendar.min.js'></script>
<script src='js/fullcalendar/fullcalendar.js'></script>
<script src='js/fullcalendar/locale/es.js'></script>


<script>

	$(document).ready(function() {

		var date = new Date();
		var yyyy = date.getFullYear().toString();
		var mm = (date.getMonth()+1).toString().length == 1 ? "0"+(date.getMonth()+1).toString() : (date.getMonth()+1).toString();
		var dd  = (date.getDate()).toString().length == 1 ? "0"+(date.getDate()).toString() : (date.getDate()).toString();
		
		$('#calendar').fullCalendar({
			header: {
				language: 'es',
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay',

			},
			defaultDate: yyyy+"-"+mm+"-"+dd,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			selectable: true,
			selectHelper: true,
			select: function(start, end) {
				
				$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD '));
				$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD '));
				$('#ModalAdd').modal('show');
			},
			eventRender: function(event, element) {
				element.bind('dblclick', function() {
					$('#ModalEdit #idcalendario').val(event.id);
					$('#ModalEdit #idservicio2').val(event.idservicio);
					$('#ModalEdit #title').val(event.title);
					$('#ModalEdit #start').val(moment(event.start).format('YYYY-MM-DD'));
					$('#ModalEdit #iniciotime').val(moment.utc(event.start).format('HH:mm'));
					$('#ModalEdit #iniciotime0').val(moment.utc(event.start).format('HH:mm'));
					$('#ModalEdit').modal('show');
				});
			},
			eventDrop: function(event, delta, revertFunc) { // si changement de position
				edit(event);
			},
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

				edit(event);

			},
			events: [
			<?php 

			foreach($events as $event):

				if($_SESSION['tipousuario']=='administrador'){
					?>
					{
						id: "<?php echo $event['idcalendario']; ?>",
						idservicio: "<?php echo $event['idsubservicios']; ?>",
						title: "<?php echo $event['subservicio']." (".$event['nombre'].")"; ?>",
						start: "<?php echo $event['fecha'].' '.$event['hora']; ?>",
						end: "<?php echo $event['fecha'].' '.$event['hora']; ?>",
						color: "#0071c5",
					},
					<?php 
				}

				else{

					if($_SESSION['tipousuario']=='usuario' 
						&& $event['inscripciones_idinscripciones']==$_SESSION['inscripciones_idinscripciones']){?>
						{	id: "<?php echo $event['idcalendario']; ?>",
					idservicio: "<?php echo $event['idsubservicios']; ?>",
					title: "<?php echo $event['subservicio']; ?>",
					start: "<?php echo $event['fecha'].' '.$event['hora']; ?>",
					end: "<?php echo $event['fecha'].' '.$event['hora']; ?>",
					color: "#0071c5",
				},
				<?php
			}
			else if($event['idservicio']==$_SESSION['idservicios']){?>
				{	id: "<?php echo $event['idcalendario']; ?>",
				idservicio: "<?php echo $event['idsubservicios']; ?>",
					//cambiar por el de reservado
					title: "<?php echo 'Reservado'.'\n'.$event['subservicio']; ?>",
					start: "<?php echo $event['fecha'].' '.$event['hora']; ?>",
					end: "<?php echo $event['fecha'].' '.$event['hora']; ?>",
					color: "#cc0000",
				},

			<?php } 
		}
	endforeach; ?>
	]
});
		
		function edit(event){
			start = event.start.format('YYYY-MM-DD HH:mm:ss');
			if(event.end){
				end = event.end.format('YYYY-MM-DD HH:mm:ss');
			}else{
				end = start;
			}
			
			id =  event.id;
			
			Event = [];
			Event[0] = id;
			Event[1] = start;
			Event[2] = end;
			
			$.ajax({
				url: 'editEventDate.php',
				type: "POST",
				data: {Event:Event},
				success: function(rep) {
					if(rep == 'OK'){
						alert('Evento se ha guardado correctamente');
					}else{
						alert('No se pudo guardar. Inténtalo de nuevo.'); 
					}
				}
			});
		}
		
	});

</script>

</body>
</html>