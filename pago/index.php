<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="Boobstrap/bootstrap.min.css">
</head>
<body>
  <?php
          if (isset($_GET['er'])) {

            echo ("<center>
              <div class='alert alert-danger' role='alert'>
              Numero de usuario incorrecto debido a falta de pago!; si ya pago comuniquese al numero +503 0000-0000 :(
            </div>
            </center>");
          }
         ?>
         <center>
    <div style="margin-top: 7.5%;"><!-- div contenedor de acceso a calendario e inscripcion -->
      <div style="width: 500px; display: inline-block; vertical-align: top;">
        <div class="card card-signin my-5" style="height: 350px;">
          <div class="card-body">

          	 <form action="validacionindex.php" method="POST" class="form-signin">
            <h5 class="card-title text-center">Consejeria de Carrera</h5>
            <h5 class="card-title text-center">Ingrese el Numero de Usuario de su boleta de pago para acceder a su calendario</h5>
              <div class="form-label-group">
                <input type="text" id="inputEmail" class="form-control" placeholder="NPE" required name="NPE">
                <label for="inputEmail">Ingresar Numero de usuario</label>
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" value="Entrar">Entrar</button>
              <hr class="my-4">
              </form>

          </div>
        </div>
      </div>

      <div style="width: 500px; display: inline-block; vertical-align: top;">
        <div class="card card-signin my-5" style="height: 350px;">
          <div class="card-body">

             <form action="form2.php" class="form-signin">
                <h5 class="card-title text-center">Inscripcion</h5>
                <h5 class="card-title text-center">Si no cuenta con un Numero de Usuario registrese en el siguiente boton</h5>
                <br>
              <button onclick="location='form2.php'" class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="Inscribirse" value="Inscribirse">Inscribirse</button>
              <hr class="my-4">
            </form>
            
          </div>
        </div>
      </div>
    </div><!-- div contenedor de acceso a calendario e inscripcion -->
    </center>
</body>