<?php 
session_start();
require_once('conexion.php');

 ?>
 <head>
	<link  href="css/bootstrap.min.css">
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="DataTables/datatables.css">
	<script type="text/javascript" charset="utf8" src="DataTables/datatables.js"></script>
	<link rel="stylesheet" type="text/css" href="css/estilotabla.css">
	<link rel="stylesheet" type="text/css" href="css/estilotabla - Copy.css">
	
	<script type="text/javascript">
	$(document).ready(function() {
    $('#tablainscripciones').DataTable();
} );
</script>
</head>
<body>
<div class="row">
	<div class="col-sm-12">
	<h2>Personas Inscritas</h2>
		<table class="display"  id="tablainscripciones">
			<thead class="thead-dark">
				<tr>
				<td>Nombre</td>
				<td>Edad</td>
				<td>Institucion</td>
				<td>Grado</td>
				<td>Correo</td>
				<td>Nombre Pago</td>
				<td>Correo Pago</td>
				<td>Telefono Pago</td>
				<td>Servicio</td>
			</tr>

			</thead>
			<tbody>
			<?php 

				$sql="SELECT * from inscripciones ins INNER JOIN servicio se on ins.servicio_idservicio=se.idservicio";
				$req=$con->prepare($sql);
				$req->execute();


				while($ver = $req->fetch()){ 


					$datos=$ver['idinscripciones']."||".
							$ver['nombre']."||".
						   $ver['edad']."||".
						   $ver['institucion']."||".
						   $ver['grado']."||".
						   $ver['correo']."||".
						   $ver['nombre_pago']."||".
						   $ver['correo_pago']."||".
						   $ver['telefono_pago']."||".
						   $ver['nombre_servicio'];
						  
			 ?>

			<tr>
				<td><?php echo $ver['nombre'] ?></td>
				<td><?php echo $ver['edad'] ?></td>
				<td><?php echo $ver['institucion'] ?></td>
				<td><?php echo $ver['grado'] ?></td>
				<td><?php echo $ver['correo'] ?></td>
				<td><?php echo $ver['nombre_pago'] ?></td>
				<td><?php echo $ver['correo_pago'] ?></td>
				<td><?php echo $ver['telefono_pago'] ?></td>
				<td><?php echo $ver['nombre_servicio'] ?></td>

			</tr>
			<?php 
		}
			 ?>
			 </tbody>
		</table>
	</div>
</div>
</body>

