<?php 
require('pageparts/header.php');
/*session_start();
require_once('conexion.php');*/
?>
 <head>
	<!--<link  href="css/bootstrap.min.css">-->
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="DataTables/datatables.css">
	<link rel="stylesheet" type="text/css" href="exportartablas/css/buttons.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="exportartablas/css/jquery.dataTables.min.css">
	<script type="text/javascript" charset="utf8" src="DataTables/datatables.js"></script>
	<script type="text/javascript" src="exportartablas/js/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="exportartablas/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="exportartablas/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="exportartablas/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="exportartablas/js/jszip.min.js"></script>
	<script type="text/javascript" src="exportartablas/js/pdfmake.min.js"></script>
	<script type="text/javascript" src="exportartablas/js/vfs_fonts.js"></script>
	<script type="text/javascript" src="exportartablas/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="exportartablas/js/buttons.print.min.js"></script>
	<!--<link rel="stylesheet" type="text/css" href="css/estilotabla.css">
	<link rel="stylesheet" type="text/css" href="css/estilotabla - Copy.css">-->
	
	<script type="text/javascript">
	$(document).ready(function() {
    $('#tablainscripciones').DataTable({
    	processiong: true,
    	pageLength: 5,
    	dom: 'Bfrtip',
        buttons: [
        'copy','excel','print',
	        {
	        extend:'pdfHtml5',
	        orientation: 'landscape',
	        pageSize:'LEGAL',

	      	}
        
        ]
    });
} );
</script>
</head>
<body>
<div class="container" style="background-color: #f8f8f8">
<div class="row">
	<div class="col-sm-12">
	<h2>Personas que han Calendarizado</h2>
		<table class="display"  id="tablainscripciones">
			<thead class="thead-dark">
				<tr>
				<td>Fecha</td>
				<td>Hora</td>
				<td>Inscripcion</td>
				<td>Subservicios</td>
			</tr>

			</thead>
			<tbody>
			<?php 

				$sql="SELECT * from calendario ca INNER JOIN subservicios sub on ca.subservicios_idsubservicios=sub.idsubservicios INNER JOIN inscripciones ins on ca.inscripciones_idinscripciones=ins.idinscripciones";
				$req=$con->prepare($sql);
				$req->execute();



				while($ver = $req->fetch()){ 


					$datos=$ver['fecha']."||".
						   $ver['hora']."||".
						   $ver['nombre']."||".
						   $ver['subservicio'];
						  
			 ?>

			<tr>
				<td><?php echo $ver['fecha'] ?></td>
				<td><?php echo $ver['hora'] ?></td>
				<td><?php echo $ver['nombre'] ?></td>
				<td><?php echo $ver['subservicio'] ?></td>
			

			</tr>
			<?php 
		}
			 ?>
			 </tbody>
		</table>
	</div>
</div>
</div>
</body>

