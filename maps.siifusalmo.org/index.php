<?php 
session_start();
include("config/net.php");?>
<html lang="en">
    <head><?php include("templates/header.php");?></head>
    <body id="top" data-spy="scroll">
    
	<!--top header-->
	<header id="home">
		<section class="top-nav hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="top-left">
                            <h2><span>Fortaleciendo Aprendizajes</span></h2>
						</div>
					</div>    
                    <?php
                	
                    if(date("m") == "01") $mes = "Enero";
                    if(date("m") == "02") $mes = "Febrero" ;
                    if(date("m") == "03") $mes = "Marzo" ;
                    if(date("m") == "04") $mes = "Abril" ;
                    if(date("m") == "05") $mes = "Mayo" ;
                    if(date("m") == "06") $mes = "Junio" ;
                    if(date("m") == "07") $mes = "Julio" ;
                    if(date("m") == "08") $mes = "Agosto" ;
                    if(date("m") == "09") $mes = "Septiembre";
                    if(date("m") == "10") $mes = "Octubre" ;
                    if(date("m") == "11") $mes = "Nombiembre" ;
                    if(date("m") == "12") $mes = "Diciembre" ;
                    
                    ?>
					<div class="col-md-6">
						<div class="top-right" style="padding-top: 10px;"  >
							<p>El Salvador, <span><?php echo date("d"). " de $mes del " . date("Y");?></span></p>
						</div>
					</div>
				</div>
			</div>
		</section>
        <?php include("templates/menu.php");?>
		<!--main-nav-->
  
	</header>
    <?php include("engine.php");?>  
    
    <?php include("templates/footer.php");?>
    </body>
</html>
