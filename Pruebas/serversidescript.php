<?php
 
// DB table to use
$table = 'students';
 
// Table's primary key
$primaryKey = 'Carnet';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'Carnet', 'dt' => 0 ),
    array( 'db' => 'Nombre', 'dt' => 1 ),
    array( 'db' => 'Carrera', 'dt' => 2 ),
    array( 'db' => 'Especialidad', 'dt' => 3 ),
    array( 'db' => 'Universidad', 'dt' => 4 ),
    array( 'db' => 'DUI', 'dt' => 5),
    array( 'db' => 'Edad', 'dt' => 6),
    array( 'db' => 'Responsable', 'dt' => 7),
    array( 'db' => 'Active', 'dt' => 8)
);
 
// SQL server connection information
$sql_details = array(
    'user' => 'root',
    'pass' => 'dy357-lx',
    'db'   => 'StudentRegistry',
    'host' => '192.168.159.130'
);
 
require( 'ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);