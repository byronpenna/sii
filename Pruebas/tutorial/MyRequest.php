<?php

/**
 * @author Manuel y Lili
 * @copyright 2015 - 2016
 */
 
$query = "SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo
            FROM CargosAsignacion AS ca
            INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
            INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE ca.IdEmpleado = '".$_SESSION["IdUsuario"]."'  and ca.FechaFin = '0000-00-00'";

$MisCargos = $bddr->prepare($query);                                                                                        
$MisCargos->execute();
$DataC = $MisCargos->fetch();

?>

<div style="float:right; width: 75%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="5"><h2 style="color: #197198;">M&oacutedulo de Requisiciones</h2></td></tr>
        <tr><td colspan="5"><hr color='skyblue' /></td></tr>
        <tr>
             <td style="width: 20%;"><a href="?l=RequestForm">
                <input type="submit" value="Crear Requisición" name="Enviar" class="botonG" style="width: 140px;" /></a>
             </td>
             <td style="width: 20%;">
                <a href="?l=requiemvista"><input type="submit" value="Requisiciones" name="Enviar" class="botonG" style=" width: 125px;" /></a>
             </td>
             <td style="width: 20%;"><a href="#">
                <?php if($DataC[2] == 22 || $DataC[2] == 23 || $DataC[2] == 24 || $_SESSION["TipoUsuario"] == "Administrador" ){?>
                <input type="submit" value="Reportes" name="Enviar" class="botonG" style=" width: 125px;" /></a>
             </td>
              <td style="width: 20%;"><a href="?l=requiemaprove">
               
                <input type="submit" value="Aprobados" name="Enviar" class="botonG" style=" width: 125px;" /></a>
             </td>
             <td style="width: 20%;"><a href="?l=proyectos">
                <input type="submit" value="Proyectos " name="Enviar" class="botonG" style="width: 140px;" /></a>
                <?php }?>
             </td>
             <td></td>
        </tr>
        <tr>
            <td colspan="5"><hr color='skyblue' /></td>
        </tr>
    </table>
</div>

<div style="float:right; width: 75%; text-align: left; background-color: white; padding:10px">
  <table style="width: 100%;">
            <tr>
             <td style="margin-left: 10px;">
               <?php
                                                            
                      
                        
                        $MisRequisiciones = $bddr->prepare("SELECT * FROM Request_Requisiciones as r 
                                                            inner join Request_Proyecto as p on r.IdProyecto = p.IdProyecto 
                                                            where IdEmpleado = " .$_SESSION["IdUsuario"] ) ;
                        $MisRequisiciones->execute();
                       
                        echo "<table style='width: 100%; text-align: center;' >
                              <tr>
                                <td colspan='5'>";
                        
                            echo "<table style='width: 100%;'>
                                    <tr>
                                        <td style='width: 50%'><h3 style='color: #197198;'>Todas tus Requisiciones</h3></td>
                                        <td style='width: 50%; text-align: right;'>
                                           
                                        </td>
                                    </tr>
                                  </table>
                                  </td>
                              </tr>";
                        
                        echo "<tr>
                                  <th style='width: 20%'>Tipo de Requisici&oacuten</th>
                                  <th style='width: 15%'>Fecha de solicitud</th>
                                  <th style='width: 15%'>Proyecto</th>
                                  <th style='width: 40%'>Actividad</th>
                                  <th style='width: 10%'>Estado</th>
                              </tr>";
                     

                        if($MisRequisiciones->rowCount() > 0)
                        {
                        
                           while($Requisicion = $MisRequisiciones->fetch())
                           {
                                
                                $MisMateriales = $bddr->prepare("SELECT * FROM Request_Material where IdRequisicion = $Requisicion[0]") ;      
                                $MisMateriales->execute();
                               echo "
                                                                                                                       
                                    <tr style='height: 50px;'>
                                          <td style='vertical-align: top;'>$Requisicion[9]</td>
                                          <td style='vertical-align: top;'>$Requisicion[4]</td>
                                          <td style='vertical-align: top;'>$Requisicion[15]</td>
                                          <td style='vertical-align: top;'>$Requisicion[3]</td>
                                          <td style='vertical-align: top;'>$Requisicion[10]</td>
                                          <td style='vertical-align: top;'>
                                           <form method='post' action='RequisicionView.php' target='_blank'>
                                                 <input type='hidden' name='idp' value='$Requisicion[0]' />
                                                 <input type='hidden' name='ide' value='$Requisicion[1]' />
                                                 <input type='hidden' name='aux' value='View' />
                                                 <input type='submit' name='Enviar' style='width: 30px' class='botonG' value='Ver' />
                                              </form>
                                            </td>
                                      </tr>";
                           } 
                       }
                       else
                            echo "<tr><th colspan='5'><p align='center' ><h2 style='color: red'>No hay Solicitudes</h2></p></th></tr>";
                            
                       echo " </td>
                              </tr>
                              </table>"; 
                                                                                                  
               
                ?>
        </tr>
    </table>
</div>