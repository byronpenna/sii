<?php

/**
 * @author Calderón
 * @copyright 2014
 */
 
 if( $_SESSION['type']   != 1)
    Redireccion('http://siipdb.fusalmo.org/Pruebas/');

?>

<script>
function Validar()
{
    var pwd1 = document.getElementById('pwd1').value;
    var pwd2 = document.getElementById('pwd2').value;
    
    if(pwd1 == pwd2)
        return true;
    
    else
    {
        alert('Error, los campos de password no coinciden');
        pwd2.focus();
        return false;    
    }
    
}
</script>
<div style="background: rgba(100,100,100,0.2); width: 80%; margin-left: 10%; color: white;">
<br /><br /><br />
<form method="post" action="Content/ABM.php">
<?php
	
    if($_SESSION['aux'] == "")
    {

        
        $ABM = "Agregar";
    }
    else
    {
        $Select = $bddC->prepare("Select * from tsv_usuario where IdUsuario = :iu");  
        $Select->bindParam(':iu', $_SESSION['aux']);
        $Select->execute();  
        
        $DataU = $Select->fetch();
        
        $ABM = "Modificar";
        echo "<input type='hidden' name='iu' value='" .  $_SESSION['aux'] . "' />";
    }
?>

<table style="padding: 10px; margin-left: 10%;">
<tr><td colspan="2"><h2>Nuevo Usuario</h2></td></tr>
<tr><td>Nombre de Usuario:</td><td style="padding: 5px;"><input type="text" class="form-control" required="true" name="UserName"  value="<?php echo $DataU[1]?>"/></td></tr>
<?php
	if($_SESSION['aux'] == "")
    {
?>
<tr><td>Contraseña:</td>                <td style="padding: 5px; color: black;"><input type="password" id="pwd1" name="pwd1" class="form-control" required="true" value="<?php echo $DataU[2]?>" /></p></td></tr>
<tr><td>Confirmación de Contraseña:</td><td style="padding: 5px; color: black;"><input type="password" id="pwd2" name="pwd2" class="form-control" required="true" value="<?php echo $DataU[2]?>" /></td></tr>
<?php        
	}
    unset($_SESSION['aux']);
?>
<tr><td>Correo Electronico:</td><td style="padding: 5px;"><input type="email" class="form-control" required="true" name="Mail" value="<?php echo $DataU[3]?>"/></td></tr>
<tr><td colspan="2" style="text-align: center; padding-top: 10px;"><input type="submit" name="Enviar" value="<?php echo $ABM?>"  class="btn btn-default" onclick="return Validar();" /></td></tr>
</table>
</form>

<hr />
<br /><br /><br />
<center>
<table style="width: 80%;">
<tr><th colspan="6"><h2>Listado de Usuarios</h2></th></tr>
<tr><th style="padding: 3px;">Id</th><th style="padding: 3px;">Nombre</th><th style="padding: 3px;">Correo Electronico</th><th colspan="3" style="text-align: center; padding: 3px;">Acciones</th></tr>
<tr><th colspan="6"><hr /></th></tr>
<?php

        $Select = $bddC->prepare("Select * from tsv_usuario");
        $Select->execute();
        
        while($DataU = $Select->fetch())   
        {
            echo "<tr><td>$DataU[0]</td><td>$DataU[1]</td><td>$DataU[3]</td>";
            
            if( $DataU[4]   != 1)
            {
                echo "<form action='Content/ABM.php' method='post'>
                        <input type='hidden' value='$DataU[0]' name='iu' />
                        <td style='text-align: center;'><input name='Enviar' type='submit' value='?' class='btn btn-success' style='width: 40px; padding: 5px' title='Reestablecer Contraseña' onclick='return confirm(\"Restableceras su contraseña a 123456, deseas hacer esto?\")'  /></td>
                        <td style='text-align: center;'><input name='Enviar' type='submit' value='X' class='btn btn-default' style='width: 40px; padding: 5px' title='Eliminar Usuario' onclick='return confirm(\"Esta seguro de eliminar a este usuario?\")' /></td>
                        <td style='text-align: center;'><input name='Enviar' type='submit' value='M' class='btn btn-primary' style='width: 40px; padding: 5px' title='Modificar datos de Usuario' /></td>
                     </form>";
            }
            else
                echo "<td colspan='3' style='text-align: center;'> Este Usuario tiene privilegios de Administrador</td>";
        }
	
?>
</table>
</center>
<br /><br />
</div>