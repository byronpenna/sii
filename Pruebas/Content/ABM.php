<?php

/**
 * @author Calderón
 * @copyright 2014
 */

include("net.php");

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Agregar" || $_POST['Enviar'] == "Registrarse")
    {

        $Insert = $bddC->prepare("Insert into tsv_usuario Values(null, :user, :pass, :mail, 2)");
        $Insert->bindParam(':user', $_POST['UserName']);
        $Insert->bindParam(':pass', $_POST['pwd1']);
        $Insert->bindParam(':mail', $_POST['Mail']);        
        $Insert->execute();  

        //Correo
        $to = $_POST['Mail'];
        $subject = "Bienvenido a TurismoSv";
        
        $message = "
        Saludos ".$_POST['UserName'].", 
        -----------------------------------------------
        Te damos la bienvenida a esta plataforma informativa, recibiras
        notificaciones de todos los lugares turisticos de El Salvador
        que se ingresen.
        
        Tu contraseña es: ".$_POST['pwd1']."
        
        Te recordamos entrar a: http://siipdb.fusalmo.org/Pruebas/
        -----------------------------------------------
        Fecha de Registro:    ".date("Y-m-d H:i:s")."
        ";
        
        // More headers
        $headers .= 'From: <no-reply@turismosv.org>' . "\r\n";
        $headers .= 'Cc: calderon.m@live.com' . "\r\n";

        mail($to,$subject,$message,$headers);
        
        if($_POST['Enviar'] == "Registrarse")
        {
            $Select = $bddC->prepare("Select * from tsv_usuario where User = :user and correo = :mail");
            $Select->bindParam(':user', $_POST['UserName']);
            $Select->bindParam(':mail', $_POST['Mail']);
            $Select->execute();   

            $DataU = $Select->fetch();
            
            $_SESSION['Id'] = $DataU[0];
            $_SESSION['User'] = $DataU[1];
            $_SESSION['Mail'] = $DataU[3];
            $_SESSION['type'] = $DataU[4];
            $_SESSION['autenticado'] = 1;

            Redireccion("http://siipdb.fusalmo.org/Pruebas/");
        }
        
        Redireccion('../?view=User');
    }
    
    else if($_POST['Enviar'] == "?")
    {
        
        $Update = $bddC->prepare("Update tsv_usuario set Pass = '123456' where IdUsuario = :iu");  
        $Update->bindParam(':iu', $_POST['iu']);
        $Update->execute();  

        
        $Select = $bddC->prepare("Select * from tsv_usuario where IdUsuario = :iu");  
        $Select->bindParam(':iu', $_POST['iu']);
        $Select->execute();  
        
        $DataU = $Select->fetch();
        
          
        //Correo
        
        $to = "$DataU[3]";
        $subject = "Se restableció tu contraseña en TurismoSv";
        
        $message = "
        
        Saludos $DataU[1], 
        -----------------------------------------------
        Quisieramos comentarte que hemos cambiado tu contraseña 
        a solicitud del administrador, ahora puedes entrar de nuevo
        a TurismoSv con la contraseña: 123456
        
        Recuerda que puedes modificar tu contraseña
        -----------------------------------------------
        Fecha de Registro:    ".date("Y-m-d H:i:s")."
        ";
        
        // More headers
        $headers .= 'From: <no-reply@turismosv.org>' . "\r\n";
        $headers .= 'Cc: calderon.m@live.com' . "\r\n";
        
        
        
        mail($to,$subject,$message,$headers);                
        
        
        Redireccion('../?view=User');
        
        
    }
    
    else if($_POST['Enviar'] == "X")
    {       
        $Select = $bddC->prepare("Select * from tsv_usuario where IdUsuario = :iu");  
        $Select->bindParam(':iu', $_POST['iu']);
        $Select->execute();  
        
        $DataU = $Select->fetch();
        
          
        //Correo
        
        $to = "$DataU[3]";
        $subject = "Despedida de TurismoSv :'(";
        
        $message = "
        
        Saludos $DataU[1], 
        -----------------------------------------------
        Quisieramos comentarte que tu usuario ha sido eliminado 
        por tu comportamiento en la plataforma.
        
        Te pedimos disculpas si has pasado un mal momento pero
        esperamos que todo se mejore en un futuro.
        -----------------------------------------------
        Fecha de Registro:    ".date("Y-m-d H:i:s")."
        ";
        
        // More headers
        $headers .= 'From: <no-reply@turismosv.org>' . "\r\n";
        $headers .= 'Cc: calderon.m@live.com' . "\r\n";
        
        
        
        mail($to,$subject,$message,$headers);
        
        $Delete = $bddC->prepare("Delete from tsv_usuario where IdUsuario = :iu");  
        $Delete->bindParam(':iu', $_POST['iu']);
        $Delete->execute(); 
        
        Redireccion('../?view=User');        
    }    
    
    else if($_POST['Enviar'] == "M")
    {       
        $_SESSION['aux'] = $_POST['iu'];
        Redireccion('../?view=User');        
    }        
    
    else if($_POST['Enviar'] == "Modificar")
    {       
        $Update = $bddC->prepare("Update tsv_usuario set User = :user, correo = :mail where IdUsuario = :iu"); 
        $Update->bindParam(':user', $_POST['UserName']);        
        $Update->bindParam(':mail', $_POST['Mail']);  
        $Update->bindParam(':iu', $_POST['iu']);
        $Update->execute();  

        $Select = $bddC->prepare("Select * from tsv_usuario where IdUsuario = :iu");  
        $Select->bindParam(':iu', $_POST['iu']);
        $Select->execute();  
        
        $DataU = $Select->fetch();
        
          
        //Correo
        
        $to = "$DataU[3]";
        $subject = "Datos Actualizados de TurismoSv";
        
        $message = "
        
        Saludos $DataU[1], 
        -----------------------------------------------
        Te informamos que tus datos han sido cambiados.
        
        Usuario: $DataU[1]
        Contraseña: $DataU[2]
        
        Recuerda entrar e informarte acerca de lugares 
        turisticos en http://siipdb.fusalmo.org/Pruebas/         
        -----------------------------------------------
        Fecha de Registro: ".date("Y-m-d H:i:s");
        
        // More headers
        $headers .= 'From: <no-reply@turismosv.org>' . "\r\n";
        $headers .= 'Cc: calderon.m@live.com' . "\r\n";
        
        mail($to,$subject,$message,$headers);
        
        Redireccion('../?view=User');                
    }

    else if($_POST['Enviar'] == "Guardar Sitio")
    {       
        $Insert = $bddC->prepare("Insert into tsv_lugar values(Null, :nombre, :departamento, :descripcion, :direccion, :telefono, :comollego, :usuario)");  
        $Insert->bindParam(':nombre', $_POST['Lugar']);
        $Insert->bindParam(':departamento', $_POST['Departamento']);
        $Insert->bindParam(':descripcion', $_POST['Descripcion']);
        $Insert->bindParam(':direccion', $_POST['Direccion']);
        $Insert->bindParam(':telefono', $_POST['Telefono']);
        $Insert->bindParam(':comollego', $_POST['Comollego']);
        $Insert->bindParam(':usuario', $_SESSION['Id']);
        $Insert->execute();  
        
        
          
        //Correo
        
        $Select = $bddC->prepare("Select * from tsv_usuario");  
        $Select->execute();  
        
        while($DataU = $Select->fetch())
        {                  
            $to = "$DataU[3]";
            $subject = "Nuevo Lugar Registrado de TurismoSv";
            
            $message = "
            
            Saludos $DataU[1], 
            -----------------------------------------------
            Te informamos de un nuevo lugar para que puedas explorar
            lo bello de nuestro pais.
            
            Nombre del Lugar: ".$_POST['Lugar']."
            Departamento: ".$_POST['Departamento']."
            Descripción: ".$_POST['Descripcion']."
            Dirección: ".$_POST['Direccion']."
            Telefono: ".$_POST['Telefono']."
            Como llego: ".$_POST['Comollego']."
            
            Recuerda entrar e informarte acerca de lugares 
            turisticos en http://siipdb.fusalmo.org/Pruebas/         
            -----------------------------------------------
            Fecha de Registro: ".date("Y-m-d H:i:s");
            
            // More headers
            $headers .= 'From: <no-reply@turismosv.org>' . "\r\n";
            $headers .= 'Cc: calderon.m@live.com' . "\r\n";
            
            mail($to,$subject,$message,$headers);
        }
        
        Redireccion('../?view=User');       
    }
    else if($_POST['Enviar'] == "x")
    {
        $Delete = $bddC->prepare("Delete from tsv_lugar where IdLugar = :il");  
        $Delete->bindParam(':il', $_POST['il']);
        $Delete->execute(); 
        
        Redireccion('../?view=Start');   
    }
          
    
              
}

?>