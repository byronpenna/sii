<?php

/**
 * @author Calder�n
 * @copyright 2014
 */

?>
<center>
<div style="width: 40%;">
<br /><br /><br />
<h2>Nuevo Sitio</h2>
<form class="form" role="form" method="post" action="Content/ABM.php">
	<div class="form-group">
		<input class="form-control" type="text" name="Lugar" required placeholder="Lugar" />
	</div>
	<div class="form-group">
		<select class="form-control" name="Departamento" required>
			<option>Ahuachapan</option>
			<option>Santa Ana</option>
			<option>Sonsonate</option>
			<option>Usulutan</option>
			<option>San Miguel</option>
			<option>Morazan</option>
			<option>La Union</option>
			<option>La Libertad</option>
			<option>Chalatenango</option>
			<option>Cuscatlan</option>
			<option>San Salvador</option>
			<option>La Paz</option>
			<option>Caba�as</option>
			<option>San Vicente</option>
		</select>
	</div>
	<div class="form-group">
        <textarea class="form-control" name="Descripcion" required placeholder="Descripci�n"></textarea>		
	</div>
	<div class="form-group">
		<input class="form-control" type="text" name="Direccion" required placeholder="Direccion" />
	</div>
	<div class="form-group">
		<input class="form-control" type="text" name="Telefono" required placeholder="2577-7777" />
	</div>
	<div class="form-group">
		<textarea class="form-control" name="Comollego" required placeholder="Como Llegar"></textarea>
	</div>
    <div class="form-group">
        <center><p class="submit"><input type="submit" name="Enviar" value="Guardar Sitio"  class="btn btn-default" /></p></center>
    </div>
</form>

</div>
</center>