<?php
    require 'net.php';
?>
<?php

/**
 * @author Manuel y Lili
 * @copyright 2015 - 2016
 */


if (isset($_POST["ide"])) 
    $id = $_POST["ide"];

else
{
    if(isset($_GET["idr"]))
    {
        $IdEmpleado = $bddr->prepare("SELECT IdEmpleado FROM Request_Requisiciones where Idrequisicion = " . $_GET["idr"]);
        $IdEmpleado->execute();        
        $DataE = $IdEmpleado->fetch();
        
        echo "<body onload='javascript:document.Request.submit();' />
                <form method='post' action='RequisicionView.php' name='Request'>
                    <input type='hidden' name='ide' value='$DataE[0]'>
                    <input type='hidden' name='idp' value='".$_GET["idr"]."'>
                    
	            </form>
              </body>";    
    }     
}


$query = "SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo
          FROM CargosAsignacion AS ca
          INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
          INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
          WHERE ca.IdEmpleado = '$id'  and ca.FechaFin = '0000-00-00'";          

$MisCargos = $bddr->prepare($query);                                                                        
$MisCargos->execute();
$DataC = $MisCargos->fetch();

?>
<html>
<head>
<title>SIIPDB</title>
<meta http-equiv="content-type" content="text/html" charset="ISO-8859-1"/>
<noscript><meta http-equiv="refresh" content="0;Error.php"/>Error!!! Javascript Desactivado</noscript>
<?php include("lib.php");?>

<script type="text/javascript" src="Empleados/Requisiciones/addrow.js"></script>
<script type="text/javascript" src="Empleados/Requisiciones/addrow2.js"></script>
<script type="text/javascript" src="Empleados/Requisiciones/addrow3.js"></script>

</head>
<body>

<div class="main">
    <div class="fbg"></div>
    <div class="main_resize"><div class="header"><?php include("header.php");?></div></div>    
    <div class="fbg"></div>    
    <div class="main_resize">    
    <div class="content">            
        <div style="float: left; margin-top: 25px;">
        <?php if($_SESSION["autenticado"] == true)
                  include("menu/menu.php")
        ?>
        </div>
        <div style="float: right; margin-right: 20%; "></div>        
        <div class="clr"></div>     
        <div class="main" style="margin-top: 30px; margin-bottom: 30px;">
       
                <table style="width: 80%; background-color: white; border-radius: 10px; margin-left: 10%;">
                    <tr><td><h2 style="color: blue; padding-left: 10px;">Solicitud de Requisici&oacuten</h2><hr color='skyblue' /></td></tr>
                    <tr>
                        <td>
                            <table style="width: 100%;  padding-left: 20px;">
                            <?php
                
                            $query2 = "SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado
                                       FROM Empleado AS e
                                       INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = e.IdEmpleado
                                       INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
                                       INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
                                       where ac.IdAsignacion = $DataC[1]";
                            //echo $query2;
                            $Accion = $bddr->prepare($query2);                                            
                            $Accion->execute();
                            $DatosEmple = $Accion->fetch(); 
                
                             $MisRequi = $bddr->prepare("SELECT r.IdEmpleado, r.Idproyecto, r.Actividad, r.Fecha_Solicitud, r.Fecha_entrega, r.Comentario, r.Linea_Presupuestaria, r.SubLinea_Presupuestaria, r.Tipo_Requisicion, r.Estado, r.Comentario_Jefe, r.Comentario_finanzas , p.IdProyecto, p.Proyecto, p.Institucion_Financiadora, p.Descripcion, l.IdLinea, l.IdProyecto, l.LineaPresupuestaria, s.IdSubLineaPresupuestaria, s.IdLineaPresupuestaria, s.SubLinea, s.Monto,r.IdRequisicion
                                                         FROM Request_Requisiciones as r 
                                                         inner join Request_Proyecto as p on r.IdProyecto = p.IdProyecto 
                                                         inner join Request_LineaPresupuestaria as l on r.Linea_Presupuestaria = l.IdLinea
                                                         inner join Request_SubLineaPresupuestaria as s on r.SubLinea_Presupuestaria = s.IdSubLineaPresupuestaria
                                               
                                                         WHERE r.Idrequisicion =" . $_POST["idp"]) ;
                            $MisRequi->execute();
                            $Requi = $MisRequi->fetch();
                            $canti = $Requi[21];
                            $materia = $Requi[23];
                            $comentaro = $Requi[22];
                
                       
                            $sqlMaterial= "SELECT * FROM Request_Material as k 
                            inner join Request_Requisiciones as o on k.Idrequisicion = o.Idrequisicion
                            right join Request_Seguimiento as w on k.IdMaterial = w.Id_Material
                            where k.IdRequisicion = " . $_POST["idp"];
                
                            $sqlAlimentacion= "SELECT * FROM Request_Alimentacion as a 
							               inner join Request_Requisiciones as o on a.Idrequisicion = o.Idrequisicion
                            inner join Request_SeguimientoAli as w on a.IdAlimentacion = w.Id_Alimentacion
                            where a.IdRequisicion = " . $_POST["idp"]; 

                            $mate= "SELECT IdSeguimiento, IdRequisicion, Costo, Cantidad_aprobada, Proveedor, Comentario, IdSubLineaPresupuestaria FROM Request_Seguimiento 
                            where IdRequisicion = " . $_POST["idp"];
                
                            $ali= "SELECT * FROM Request_Alimentacion as a 
                            inner join Request_Requisiciones as o on a.Idrequisicion = o.Idrequisicion
                            inner join Request_Seguimiento as s on a.Idrequisicion = s.IdRequisicion
                            where a.IdRequisicion = " . $_POST["idp"]; 

                            $sqlseguimiento = $bddr->prepare("SELECT w.IdSeguimiento, w.IdRequisicion, w.Costo, w.Cantidad_aprobada, w.Proveedor, w.Comentario, w.IdSubLineaPresupuestaria, r.IdEmpleado, r.Idproyecto, r.Actividad, r.Fecha_Solicitud, r.Fecha_entrega, r.Comentario, r.Linea_Presupuestaria, r.SubLinea_Presupuestaria, r.Tipo_Requisicion, r.Estado, r.Comentario_Jefe, r.Comentario_finanzas FROM Request_Seguimiento as w 
                            inner join Request_Requisiciones as r on w.Idrequisicion = r.Idrequisicion
                            where w.IdSeguimiento = $seg[0] "); 

                            $sqlseguimiento->execute();
                            $seg = $sqlseguimiento->fetch();                               
                                               
                
                            $stmt = $bddr->prepare($sqlMaterial);
                            $stmt->execute();
                            $result = $stmt->fetchAll();
                
                            $stmt2 = $bddr->prepare($sqlAlimentacion);
                            $stmt2->execute();
                            $result2 = $stmt2->fetchAll();

                            $mate = $bddr->prepare($mate);
                            $mate->execute();
                            $materiales = $mate->fetchAll();
                
                            $ali = $bddr->prepare($ali);
                            $ali->execute();
                            $alimentacion = $ali->fetchAll();


                            $lines = $bddr->prepare("SELECT * FROM Request_Material as k 
                            inner join Request_Requisiciones as o on k.Idrequisicion = o.Idrequisicion
                            right join Request_Seguimiento as w on k.IdMaterial = w.Id_Material
                            where k.IdRequisicion = " . $_POST["idp"]) ;
                            $lines->execute();
                            $lineo = $lines->fetch();

                             
                           
                           
                            
                            ?>        
                        <input type="hidden" name="NombreEmpleado" value="<?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?>" />
                        <tr><td  style="width: 30%;">Empleado solicitante: </td><td><?php echo "$DatosEmple[0] $DatosEmple[1] $DatosEmple[2] $DatosEmple[3]"?></td></tr>
                        <tr><td>Cargo Asignado:</td><td><?php echo "$DatosEmple[5]"?></td></tr>
                        <tr><td>Area de trabajo:</td><td><?php echo "$DatosEmple[6]"?></td></tr>
                    </table>
                    <hr color='skyblue' />
                    </td>               
                    </tr>
                    <tr>
                    <td>
                    <table style="width: 60%; margin-left: 5%;">
                  
                     <tr><td  style="padding-bottom: 10px;"><strong>Nombre de proyecto:</strong></td><td><?php echo  "$Requi[13]" ?></td></tr>
                     <tr><td  style="padding-bottom: 10px;"><strong>Linea Presupuestaria:</strong></td><td><?php echo  "$Requi[18]"?></td></tr>
                     <tr><td  style="padding-bottom: 10px;"><strong>Sub-Linea Presupuestaria:</strong></td><td><?php echo  "$Requi[21]"?></td></tr>
                    
                     <tr><td  style="padding-bottom: 10px;"><strong>Actividad:</strong></td><td><?php echo  "$Requi[2]"?></td></tr>
                     <tr><td  style="padding-bottom: 10px;"><strong>Fecha de la solicitud:</strong></td><td><?php echo  "$Requi[3]"?></td></tr>
                     <?php if($Requi[8] != "Alimentacion") { ?>
                     <tr><td  style="padding-bottom: 10px;"><strong>Fecha de entrega:</strong></td><td><?php echo  "$Requi[4]"?></td></tr>
                     <?php } ?>
                     <tr><td  style="padding-bottom: 10px;"><strong>Tipo de requisici&oacuten:</strong></td><td><?php echo  "$Requi[8]"?></td></tr>
                     <tr><td  style="padding-bottom: 10px; width: 50%;"><strong>Comentarios:</strong></td><td><?php echo "$Requi[5]"?></td></tr>
                     <tr><td colspan="2" style="padding-bottom: 10px;"><strong>Detalles de la requisici&oacuten:</strong><br />   
                     <input type="hidden" name="idp" value="<?php echo $_POST["idp"];?>" />
                        </table >
                        <?php if($Requi[8] == "Materiales") { ?>
                        <table colspan="8" rules="rows" style="width: 60%; margin-left: 5%; text-align: center;">
                       <form action="Empleados/Requisiciones/seguimientoABM.php" method="post">
                       <input type="hidden" name="ider" value='$lineo[1]' />
                       <input type='hidden' name='iddd' value='<?php echo  "$lineo[1]"?>' style='width: 72px;'  />

                        <tr><th>Cantidad</th><th>Cantidad Aprobada</th><th>Material</th><th>Especificaci&oacuten Tecnica</th><th>Costo</th><th>Proveedor</th></tr>
                        <?php  

                        foreach($result as $row)
                        {
                                  //$Budge = $bddr->prepare("Select DISTINCT * from Request_Seguimiento where IdSeguimiento = $DataS[0]");
                                 // $Budge->execute();                     
                           
                            echo "<tr><td>{$row['Cantidad']}</th>";
                            echo "<th>{$row['Cantidad_aprobada']}</th>";
                            echo "<th>{$row['Material']}</td>";
                            echo "<th>{$row['Espe_Tecnica']}</th>";
                            echo "<th><input type='text' name='Costo[]' value='{$row['Costo']}' style='width: 72px;  text-align: center;' /></th>";
                            echo "<th><input type='text' name='Proveedor[]' value='{$row['Proveedor']}' style='width: 245px;  text-align: center;' />
                                      </th>";
                            echo "<th><input type='hidden' name='sublinea[]' value='$Requi[7]' style='width: 72px;'  /> 
                            <input type='hidden' name='IdMaterial[]' value='{$row['IdMaterial']}' style='width: 40px; text-align: center;' />
                            <input type='hidden' name='IdRequisicion[]' value='{$row['IdRequisicion']}' style='width: 40px; text-align: center;' />
                                  <input type='hidden' name='IdSeguimiento[]' value='{$row['IdSeguimiento']}' style='width: 40px; text-align: center;' />
                                  <input type='hidden' name='Cantidad_aprobada[]' value='{$row['Cantidad_aprobada']}' style='width: 72px;'  /></td>";
                            
                        } ?>
                     </table>
                         <hr color='skyblue' /> 
                         <table>  
                         <tr></tr>
                         
                         <tr>
                         
                            <td style="padding-bottom: 10px; width: 40%;"><input type="radio" name="Decision" value="Entregado" required='true' />Entregado</td>
                            <td style="padding-bottom: 10px; width: 50%;"><input type="radio" name="Decision" value="EnProceso" required='true'/>En Proceso</td>
                        </tr>
                    <tr><td colspan="2" style="padding-bottom: 10px;"><input type="submit" class="boton" name="Enviar" value="Enregister" style="margin-top: 10px;"/></td></tr>                         
                    </table>
                    <?php } ?>

                     <?php if($Requi[8] == "Alimentacion") { ?>
                        <table colspan="8" rules="rows" style="width: 60%; margin-left: 0%; text-align: center;">
                       <form action="Empleados/Requisiciones/seguimientoAABM.php" method="post">
                        <tr><th><br/>Fecha de entrega</th><th>Hora de entrega</th><th>Municipio</th><th>  Sede</th><th>Direcci&oacuten</th><th>Tecnico encargado</th><th>Cantidad</th><th>Almuerzo / refrigerio</th><th>Costo</th><th>Proveedor</th></tr>
                        <?php  foreach($result2 as $row2)
                        {
                            
                            echo "<tr><td>{$row2['Fecha']}</td>";
                            echo "<th>{$row2['Hora']}</th>";
                            echo "<th>{$row2['Municipio']}</th>";
                            echo "<th>{$row2['Sede']}</th>";
                            echo "<th>{$row2['Direccion']}</th>";
                            echo "<th>{$row2['Encargado']}</th>";
                            echo "<th>{$row2['Cantidad']}</th>";
                            echo "<th>{$row2['Tipo']}</th>";
                           echo "<td><input type='text' name='Costo[]' value='{$row2['Costo']}' style='width: 72px;  text-align: center;' />
                           <input type='hidden' name='IdAlimentacion[]' value='{$row['Id_Alimentacion']}' style='width: 40px; text-align: center;' />
                                  <input type='hidden' name='IdRequisicion[]' value='{$row2['IdRequisicion']}' style='width: 40px; text-align: center;' />
                                  <input type='hidden' name='IdSeguimiento[]' value='{$row2['IdSeguimiento']}' style='width: 40px; text-align: center;' />
                                  <input type='hidden' name='Cantidad_aprobada[]' value='{$row2['Cantidad']}' style='width: 72px;'  />
                                      </td>";
                            echo "<td><input type='text' name='Proveedor[]' value='{$row2['Proveedor']}' style='width: 245px;  text-align: center;' />
                                      </td>";
                                                        echo "<td><input type='hidden' name='sublinea[]' value='$Requi[7]' style='width: 72px;'  /> </td></tr>";
                                                        
                        } ?>
                        </table>
                         <hr color='skyblue' /> 
                         <table>  
                         <tr></tr>
                         <input type="hidden" name="idp" value="<?php echo $_POST["idp"];?>" />
                         <tr>
                            <td style="padding-bottom: 10px; width: 40%;"><input type="radio" name="Decision" value="Entregado" required='true' />Entregado</td>
                            <td style="padding-bottom: 10px; width: 50%;"><input type="radio" name="Decision" value="EnProceso" required='true' />En Proceso</td>
                        </tr>
                    <tr><td colspan="2" style="padding-bottom: 10px;"><input type="submit" class="boton" name="Enviar" value="Guardar" /></td></tr>                         
                      
                                               
                    </table>
                     </form>
                        <?php } ?>
                    <hr color='skyblue' />                    
                </td>             
                </tr>
                
                <tr>
                <td>
                    
                    <?php
                           if(!isset($_POST['aux']) && ($Requi[9] == "Pendiente"))
                           {
                    ?>
                    <form action="Empleados/Requisiciones/requestABM.php" method="post">
                    <table style="width: 90%; margin-left: 5%;">                    
                        <input type="hidden" name="idp" value="<?php echo $_POST["idp"];?>" />
                        <tr><td colspan="2"><strong>Decisi&oacuten del Requerimiento</strong></td></tr>                        
                        <tr>
                            <td style="padding-bottom: 10px; width: 40%;"><input type="radio" name="Decision" value="Aceptar" required='true' />Aceptar</td>
                            <td style="padding-bottom: 10px; width: 50%;"><input type="radio" name="Decision" value="Denegar" required='true' />Denegar</td>
                        </tr>
                        <tr><td colspan="2" style="padding-bottom: 10px;">
                                <strong>Comentario sobre la decisi&oacuten:</strong>    
                                <br /><textarea name="Comentario" style="width: 70%; height: 50px;"></textarea>
                            </td>
                        </tr>
                        <tr><td colspan="2" style="padding-bottom: 10px;"><input type="submit" class="boton" name="Enviar" value="Guardar" /></td></tr>                                               
                    </table>
                    </form> 
                    <?php
                        }
                        else
                        {
                            if($DatosEmple[10] == "")
                                $ComentarioJefe = "<em style='color: blue'>No hay comentarios</em>";
                                
                            else
                                $ComentarioJefe = $Requi[10];
                    ?>      
                     <?php
                    
                            if($DatosEmple[11] == "")
                                $ComentarioJefe = "<em style='color: blue'>No hay comentarios</em>";
                                
                            else
                                $ComentarioJefe = $Requi[11];
                    ?>      
                        <table style="width: 90%; margin-left: 5%;">                    
                            <input type="hidden" name="idp" value="<?php echo $IdRequisicion;?>" />
                            <tr><td colspan="2"><strong>Decisi&oacuten de la Requisici&oacuten:</strong> <br /><?php echo  "$Requi[9]"?><br /><br /></td></tr>
                            <tr>
                                <td colspan="2" style="padding-bottom: 10px;">
                                    <strong>Comentario sobre la decisi&oacuten:</strong><br /><?php echo  "$Requi[10]"?>   
                                </td>
                            </tr>  
                             <tr>
                                <td colspan="2" style="padding-bottom: 10px;">
                                    <strong>Comentario sobre la decisi&oacuten: Lic Iglesias</strong><br /><?php echo  "$Requi[11]"?>   
                                </td>
                            </tr>                                              
                        </table>                        
                    <?php
                        }   
                    ?>                
                    <hr color='skyblue' />
                </td>             
            </tr>  
            <?php           
            if($_SESSION["TipoUsuario"] == "Administrador")
            {
            ?>   
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="width: 85%;">
                            <script>function cerrar() { setTimeout(window.close,1500); }</script>
                                <form action="Empleados/Requisiciones/requestABM.php" method="post">
                                    <input type="hidden" name="idp" value="<?php echo $IdRequisicion;?>" />
                                    <table style="width: 100%;">                                            
                                        <tr>
                                            <td><strong>Administración Compras</strong></td>
                                            <td><input type="submit" class="botonE" name="Enviar" value="Eliminar"  /></td>
                                            <td><input type="button" class="botonG" name="Enviar" value="Cerrar" onclick="cerrar();" /></td>
                                        </tr>                                                
                                    </table>
                                </form>
                            </td>
                            <td>
                            <form method='post' action='Requisicioncomparativa.php' target='_blank'>
                                                 <input type='hidden' name='idp' value='<?php echo $Requi[23];?>' />
                                                 <input type='hidden' name='ide' value='<?php echo $Requi[0];?>' />
                                                 <input type='hidden' name='' value='View' />
                                                 <input type='submit' name='Enviar' style='width: 30px' class='botonG' value='Ver' />
                                              </form></td>
                        </tr>
                    </table>

                </td>
            </tr>
            <?php
                }
            ?>       
        </table>
        </div>
    </div>
    </div>
  <div class="fbg"></div>
</div>
<div class="footer"><?php include("footer.php");?></div>
</body>
</html>