<?php

/**
 * @author  Jos� Manuel Calder�n
 * @copyright 2014
 */

?>
<script>
function CargarListaMunicipios()
{
    var departamento = document.getElementById('Departamentos').value;    
    document.getElementById('Municipio').options.length = 0;
    
    $.get("USAID/SelectMunicipios.php", { departamento: departamento },
  		function(resultado)
  		{           
            document.getElementById('Municipio').options.length = 0;
                
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#Municipio').append(resultado);	            			
  		}
   	);        
}
</script> 
<div style="float: right; width: 80%; ">
<form action="USAID/EventosABM.php" method="Post">
<table style="background-color: white; width:100%; border-radius: 10px;" >
<tr><td colspan="2"><h2 style="padding-left: 30px;">Formulario de Eventos</h2><hr color='Skyblue' /></td></tr>
<tr>
    <tr><td class="tdleft">Nombre del Evento:</td><td><input  style="margin-left: 5px; width: 250px;"type="text" value="" required="" name="Nombre" /></td></tr>
</tr>
<tr>
    <td class="tdleft">Componente:</td>
    <td>
        <select name='Componente'  style="width: 250px; margin-left: 5px;" > 
        <?php
                $componente = $bddC->prepare("Select * from USAID_Componente where IdDatoFUSALMO = :IdDf");
                $componente->bindParam(':IdDf', $_SESSION['IdDato']);
                $componente->execute();
                
                if($componente->rowCount() > 0)
                {                    
                    while($data = $componente->fetch())                        
                        echo "<option value='$data[0]'>$data[2]</option>";
                }
                else
                    echo "<option value='...'>No hay Componentes Agregados.</option>";                                               
        ?>                               
        </select>
    </td>
</tr>
<tr>
    <td class="tdleft">Departamento:</td>
    <td>
        <select name="Departamentos" id="Departamentos" style="width: 250px; margin-left: 5px;" onchange="CargarListaMunicipios()" >
            <option value="...">Seleccione un departamento...</option>
        <?php
                $Departamentos = $bddC->prepare("SELECT * FROM USAID_Departamentos");
                $Departamentos->execute();
                   
                while($data = $Departamentos->fetch())                        
                    echo "<option value='$data[0]'>$data[1]</option>";

                
                                                               
        ?>        
        </select>
    </td>
</tr>
<tr>
    <td class="tdleft">Municipio:</td>
    <td>
        <select name="Municipio" id="Municipio" style="width: 250px; margin-left: 5px;">
            <option value="..."> Esperando departamento ...</option>
        </select>
    </td>
</tr>
<tr><td class="tdleft">Sede:</td><td><input  style="margin-left: 5px; width: 250px;"type="text" value="" required="" name="Sede" /></td></tr>
<tr><td colspan="2" style="text-align: center;"><input  type="submit" value="Guardar" name="Enviar" class="boton" /><br /><br /></td></tr>
</table>
</form>
</div>