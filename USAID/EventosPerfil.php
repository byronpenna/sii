<?php

/**
 * @author Gilbetor
 * @copyright 2014
 */


    if(!isset($_POST['IdC']))
        Redireccion("USAID.php?u=Events");
        
    $Evento = $bddC->prepare("Select * from USAID_Evento where IdEvento =" . $_POST['IdC']);
    $Evento->execute();
    $DataE = $Evento->fetch();
    
    $Departamento = $bddC->prepare("Select * from USAID_Departamentos where IdDepartamento = $DataE[3]");
    $Departamento->execute();
    $DataD = $Departamento->fetch();

?>
<div style="width: 75%; margin-left: 22%; border-radius: 10px; background-color: white; padding-bottom: 30px; margin-bottom: 30px;">
    <table style="width: 100%;">
        
        <tr><td colspan="2"><h2>Detalles de Evento</h2></td></tr>
        <tr><td class="tdleft">Nombre del Evento:</td><td><?php echo $DataE[2]?></td></tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr><td class="tdleft">Departamento:</td><td><?php echo $DataD[1];?></td></tr>
        <tr><td class="tdleft">Municipio:</td><td><?php echo $DataE[4];?></td></tr>
        <tr><td class="tdleft">Sede del Evento:</td><td><?php echo $DataE[5];?></td></tr>
        <tr><td colspan="2"><h3>Opciones</h3></td></tr>
        <tr>            
            <td  class="tdleft">
                <form action="USAID/EventosABM.php" method="post">
                    <input type="hidden" name="IdE" value="<?php echo $DataE[0];?>" />                
                    <input type="submit" name="Enviar" value="Eliminar" class="botonE" />
                </form> 
            </td>
                <td style="text-align: left;"> <input type="submit" name="Enviar" value="Actualizar" class="boton" /></td>                
            
        </tr>
        
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td><h3>Población Meta</h3></td>
            <td style="text-align: right;">
                <form action="USAID.php?u=TargetPopulation" method="post">
                    <input type="submit" name="Enviar" value="Definir" class="boton" />
                </form>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <?php
	               $Consultores = $bddC->prepare("Select * from USAID_Consultores as c
                                                  Inner Join USAID_EventoAConsultor as ac on c.IdConsultor = ac.IdConsultor
                                                  where ac.IdEvento = $DataE[0]");
                   $Consultores->execute();
                   
                   if($Consultores->rowCount() > 0)
                   {
                    
                   }
                   else
                        echo "<div style='text-align: center; color: red;'><em>No se ha definido la Población Meta de este Evento</em></div>";
                    
                   
            ?>
            </td>
        </tr>        
        
        
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td><h3>Consultores a cargo</h3></td>
            <td style="text-align: right;">                
                <form action="USAID.php?u=AssignmentConsultant" method="post">
                    <input type="submit" name="Enviar" value="Asignar" class="boton" />
                </form>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <?php
	               $Consultores = $bddC->prepare("Select * from USAID_Consultores as c
                                                  Inner Join USAID_EventoAConsultor as ac on c.IdConsultor = ac.IdConsultor
                                                  where ac.IdEvento = $DataE[0]");
                   $Consultores->execute();
                   
                   if($Consultores->rowCount() > 0)
                   {
                    
                   }
                   else
                        echo "<div style='text-align: center; color: red;'><em>No hay consultores a cargo de este Evento</em></div>";
  
            ?>
            </td>
        </tr>
        
        
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td><h3>Jornadas del Evento</h3></td>
            <td style="text-align: right;">
                <form action="USAID.php?u=days" method="post">
                    <input type="submit" name="Enviar" value="Crear" class="boton" />
                </form>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <?php
	               $Consultores = $bddC->prepare("Select * from USAID_Jornada
                                                  where IdEvento = $DataE[0]");
                   $Consultores->execute();
                   
                   if($Consultores->rowCount() > 0)
                   {
                    
                   }
                   else
                        echo "<div style='text-align: center; color: red;'><em>No hay Jornadas creadas de este Evento</em></div>";
                    
                   
            ?>
            </td>
        </tr>                 
    </table>
</div>