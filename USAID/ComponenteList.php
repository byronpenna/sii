<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

?>

<div style="width: 75%; margin-left: 22%; border-radius: 10px; background-color: white; padding-bottom: 30px; margin-bottom: 30px;">
<h2 style="text-align: center;">Componentes Educativas</h2><hr color='skyblue' /><br />
<table style="width: 80%; margin-left: 10%; border-color: skyblue;">
<form action="USAID/ComponenteABM.php" method="POST">
    <tr>
        <td style="text-align: right; padding-right: 20px;">Escriba su nueva componente:</td>
        <td><input type="text" value="" name="NombreC"  required="true" /></td>
        <td><input type="submit" value="Agregar" class="boton" name="Enviar" /></td>
    </tr>
</form>
</table>
<br /><hr color='skyblue' /><br />

<table style="width: 80%; margin-left: 10%; text-align: center;">
<tr><th style="width: 10%;">Id</th><th style='text-align: left;'>Nombre Componente</th><th style="width: 20%;" colspan="2"></th></tr>
<?php
      $idf = $_SESSION['IdDato'];
	  $Componentes = $bddC->prepare("Select * from USAID_Componente where IdDatoFUSALMO = $idf");
      $Componentes->execute();
      
      if($Componentes->rowCount() > 0)
      {
          while($DataC = $Componentes->fetch())
          {
                echo "<tr><td>$DataC[0]</td><td style='text-align: left;'>$DataC[2]</td>
                          <td>
                              <form action='USAID/ComponenteABM.php' method='post'>
                                <input type='hidden' name='idc' value='$DataC[0]'/>
                                <input type='submit' class='botonE' name='Enviar' style='width: 30px;' value='x' onclick='return confirmar();'/>
                              </form>  
                          </td>
                      <tr>";
          }
      }
      else
          echo "<tr><td style='text-align:center;' colspan='4'><h3 style='color: red;'>No hay componentes ingresados</h3></td></tr>";
?>
</table>
</div>