<?php

/**
 * @author Manuel Calderón
 * @copyright 2014
 */

?>

<div style="width: 75%; margin-left: 22%; border-radius: 10px; background-color: white; padding-bottom: 30px; margin-bottom: 30px;">
    <table style="width: 100%;">
        <tr><td colspan="2"><h2>Definición de Población Meta</h2><hr color='skyblue' /></td></tr>
        
        <tr><td class="tdleft" style="width: 50%;">Directores</td>
            <td><input style="width: 40px;text-align: center;" type="number" name="Directores" value="0" min="0" max="100" /></td></tr>
            
        <tr><td class="tdleft" style="width: 50%;">Docentes 3° Ciclo</td>
            <td><input  style="width: 40px;text-align: center;" type="number" name="Docentes" value="0" min="0" max="100" /></td></tr>
            
        <tr><td class="tdleft" style="width: 50%;">Estudiantes 3° Ciclo</td>
            <td><input  style="width: 40px;text-align: center;" type="number" name="Estudiantes" value="0" min="0" max="100" /></td></tr>
            
        <tr><td class="tdleft" style="width: 50%;">Padres de Familia</td><td>
            <input  style="width: 40px;text-align: center;" type="number" name="Padres" value="0" min="0" max="100"/></td></tr>
            
        <tr><td class="tdleft" style="width: 50%;">Lideres de Comunidades</td>
            <td><input  style="width: 40px;text-align: center;" type="number" name="Lideres" value="0" min="0" max="100"/></td></tr>
            
        <tr><td class="tdleft" style="width: 50%;">ATP MINED</td>
            <td><input  style="width: 40px;text-align: center;" type="number" name="ATP" value="0" min="0" max="5"/></td></tr>
            
        <tr><td colspan="2" style="text-align: center;"><br /><input type="submit" class="boton" value="Guardar" name="EnviarP" /></td></tr>
    </table>
</div>