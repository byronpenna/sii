<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

if($_SESSION['IdCon'] != "")
{
     $GetDatos = $bddC->prepare('SELECT * FROM USAID_Consultor where IdConsultor = ' . $_SESSION['IdCon']);	
     $GetDatos->execute();
     
     $DataConsultor = $GetDatos->fetch();
     $abm = "Actualizar";
}
else
{
    $abm = "Guardar";
}

?>
<div style="width: 75%; margin-left: 22%; border-radius: 10px; background-color: white; padding-bottom: 30px; margin-bottom: 30px;">
<form action="USAID/ConsultorABM.php" method="post">
<?php
	   if($_SESSION['IdCon'] != "")
       {
            echo "<input type='hidden' name='IdCon' value='".$_SESSION['IdCon']."'  />";
            unset($_SESSION['IdCon']);
       }
?>
    <table style="margin-left: 20px; width: 90%;">
        <tr><td colspan="2"><h2>Formulario de Consultores</h2><hr color='skyblue' /></td></tr>
        <tr><td class="tdleft" style="width: 40%;">Nombre de Consultor:</td><td> <input type="text" name="Nombre" value="<?php echo $DataConsultor[2]?>" required="" /></td></tr>
        <tr><td class="tdleft" style="width: 40%;">Tel�fono de Casa:   </td><td> <input type="text" name="Telefono" value="<?php echo $DataConsultor[3]?>" /></td></tr>
        <tr><td class="tdleft" style="width: 40%;">Tel�fono Celular:   </td><td> <input type="text" name="Celular" value="<?php echo $DataConsultor[4]?>" required="" /></td></tr>    
        <tr><td class="tdleft" style="width: 40%;">Direcci�n de Recidencia:</td><td> <input type="text" name="Direcci�n" value="<?php echo $DataConsultor[5]?>" required="" /></td></tr>
        <tr><td class="tdleft" style="width: 40%;">Correo Electr�nico:     </td><td><input type="email" name="correo" value="<?php echo $DataConsultor[6]?>" required="" /></td></tr>
        <tr><td colspan="2" style="text-align: center;"><input type="submit" class="boton" value="<?php echo $abm?>" name="Enviar" /></td></tr>
    </table>
</form>
</div>