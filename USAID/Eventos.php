<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

?>
<div style="width: 80%; margin-left: 22%; border-radius: 10px; background-color: white; padding-bottom: 30px; margin-bottom: 30px;">    
<table style="width: 100%;">
    <tr><td><h2>Eventos</h2></td>
        <td style="text-align: right;"><a href="USAID.php?u=EventsForm"><input type="button" class="boton" value="Agregar" /></a></td></tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="2">
        <?php
        $Componentes = $bddC->prepare("Select * from USAID_Componente where IdDatoFUSALMO = " . $_SESSION['IdDato']);
        $Componentes->execute();
        
        if($Componentes->rowCount()>0)
        {
        ?> 
            <table style="width: 100%;">
                <tr>
                    <form action="#" method="post">
                    <td class="tdleft" style="width: 40%;">Seleccione el Componente:</td>
                    <td style="width: 30%;">
                        <select name="Componente" >
                        <?php               
                        $PComp = 0;
                        while($DataC = $Componentes->fetch())
                        {
                             if($PComp == 0)
                                $PComp = $DataC[0];
                                
                             echo "<option value='$DataC[0]'>$DataC[2]</option>";   
                        }               
                        ?>  
                        </select>  
                    </td>
                    <td style="width: 30%;"><input type="submit" name="Enviar" class="boton" value="Mostrar" /></td>
                    </form>
                </tr>
            </table>  
            <hr color='skyblue' />
        <?php
                    if(isset($_POST['Componente']))
                        $Query = "Select * from USAID_Evento where IdComponente = ". $_POST['Componente'];
                        
                    else
                        $Query = "Select * from USAID_Evento where IdComponente = $PComp";
                    
                    $Eventos = $bddC->prepare($Query);
                    $Eventos->execute();
                    
                    echo "<table style='width: 100%;'>
                          <th>Nombre del Evento</th><th>Departamento</th><th colspan='2' >Municipio</th>";
                    if($Eventos->rowCount() > 0)
                    {
                        while($DataE = $Eventos->fetch())
                        {
                            echo "<form action='USAID.php?u=EventProfile' method='post'>
                                  <input type='hidden' name='IdC' value='$DataE[0]'  />
                                  <tr>
                                      <td>$DataE[2]</td><td>$DataE[4]</td><td>$DataE[5]</td>
                                      <td><input type='submit' value='Detalles' class='boton' /></td>
                                  </tr>
                                  </form>";
                        }
                    }
                    else
                    {
                        echo "<tr>
                                  <td colspan='4' style='color: blue; text-align:center;'><br /><br />
                                    <a href='USAID.php?u=EventsForm' style='color: blue; text-decoration: none;'>No hay eventos, Agrega evento</a>
                                  </td>
                              </tr>";
                    }
                    echo "</table>";
                    
	    }
        else
        {
            echo "<div style='text-align: center;'>
                    <h2><a href='USAID.php?u=Component' style='color: blue; text-decoration: none;'>No hay Componentes, agrega uno</a></h2>
                  </div>";
        } 
        ?>           
        </td>
    </tr>
</table>
</div>