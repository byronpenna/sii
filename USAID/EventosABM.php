<?php

/**
 * @author Gilbetor
 * @copyright 2014
 */

require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Guardar")
    {
        $insert = $bddC->prepare("Insert into USAID_Evento values(Null, :IdC, :Nombre, :Dep, :Mun, :Sed)");
        $insert->bindParam(':IdC', $_POST['Componente']);
        $insert->bindParam(':Nombre', $_POST['Nombre']);
        $insert->bindParam(':Dep', $_POST['Departamentos']);
        $insert->bindParam(':Mun', $_POST['Municipio']);
        $insert->bindParam(':Sed', $_POST['Sede']);        
        $insert->execute();
        
        Redireccion("../USAID.php?u=Events&n=1");
    }
    
    else if($_POST['Enviar'] == "Eliminar")
    {
        $delete = $bddC->prepare("Delete from USAID_Evento where IdEvento = " . $_POST['IdE']);
        $delete->execute();
        
        Redireccion("../USAID.php?u=Events&n=3");
    }
}


?>