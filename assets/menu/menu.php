<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

        <meta charset="utf-8" />
        <meta name="author" content="Script Tutorials" />
        <title>Menu</title>

        <!-- add styles -->
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

            <?php if($_SESSION["TipoUsuario"] == "Administrador"){?>
            <div class="container">
            <ul id="nav">
            <li><a href="Main.php"><b>Inicio</b></a></li>
            <li><a href="Main.php?l=jovenl"><b>Jóvenes</b></a></li>
            <li><a href='Main.php?l=UsuL'><b>Usuarios</b></a></li>
            <li><a href='Instituciones.php'><b>Instituciones</b></a></li>
            <li><a href='emp.php'><b>Empleado</b></a></li>  
            <li><a href='monitoreo.php'><b>Mis Encuestas</b></a></li> 
            <li><a href="FUSALMO.php"><b>FUSALMO</b></a></li>
            <li><a href="Inventario.php"><b>Inventario</b></a></li>
            </ul>
            </div>

            <?php }
            if($_SESSION["TipoUsuario"] == "Joven"){?>
            <div class="container">
            <ul id="nav">
            <li><a href="Main.php"><b>Inicio</b></a></li>
            <li><a href="Main.php?l=joven"><b>Mis Datos</b></a></li>
            <li><a href="FUSALMO.php"><b>FUSALMO</b></a></li>
            <li><a href="monitoreo.php?"><b>Mis Encuestas</b></a></li>
            </ul>
            </div>

            <?php }
            if($_SESSION["TipoUsuario"] == "Consultor"){?>
            <div class="container">
            <ul id="nav">
            <li><a href="Main.php"><b>Inicio</b></a></li>
            <li><a href="emp.php"><b>Empleado</b></a></li>
            <li><a href="monitoreo.php?"><b>Mis Encuestas</b></a></li>
            </ul>
            </div>

            <?php }
            if($_SESSION["TipoUsuario"] == "RRHH"){?>
            <div class="container">
            <ul id="nav">
            <li><a href="Main.php"><b>Inicio</b></a></li>
            <li><a href="emp.php"><b>Empleado</b></a></li>
            <li><a href="monitoreo.php"><b>Mis Encuestas</b></a></li>
            </ul>
            </div>

            <?php }
            if($_SESSION["TipoUsuario"] == "Coordinador"){?>
            <div class="container">
            <ul id="nav">
            <li><a href="Main.php"><b>Inicio</b></a></li>
            <li><a href="emp.php"><b>Empleado</b></a></li>
            <li><a href="FUSALMO.php"><b>FUSALMO</b></a></li>
            <li><a href="Main.php?l=jovenl"><b>Jóvenes</b></a></li>
            <li><a href="monitoreo.php?"><b>Mis Encuestas</b></a></li>
            </ul>
            </div>

            <?php }
            if($_SESSION["TipoUsuario"] == "Gestor"){ ?>
            <div class="container">
            <ul id="nav">
            <li><a href="Main.php"><b>Inicio</b></a></li>
            <li><a href="emp.php"><b>Empleado</b></a></li>
            <li><a href="FUSALMO.php"><b>FUSALMO</b></a></li>
            <li><a href="monitoreo.php"><b>Mis Encuestas</b></a></li>
            </ul>
            </div>

            <?php }
            if($_SESSION["TipoUsuario"] == "Buscador"){ ?>
            <div class="container">
            <ul id="nav">
            <li><a href="Main.php"><b>Inicio</b></a></li>
            <li><a href="Main.php?l=UsuL"><b>Usuarios</b></a></li>
            <li><a href="Main.php?l=jovenl"><b>Jóvenes</b></a></li>
            </ul>
            </div>

            <?php }
            if($_SESSION["TipoUsuario"] == "USAID-CI" || $_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "USAID-SE"){ ?>
             <div class="container">
                        <ul id="nav">
            <li><a href="Main.php"><b>Inicio</b></a></li>
            <li><a href="emp.php"><b>Empleado</b></a></li>
            <li><a href="FUSALMO.php"><b>FUSALMO</b></a></li>
            <li><a href="monitoreo.php"><b>Mis Encuestas</b></a></li>
            <li><a href="Inventario.php"><b>Inventario</b></a></li>
            </ul>
            </div>

            <?php }
            if($_SESSION["TipoUsuario"] == "Desarrollo"){ ?>
            <div class="container">
            <ul id="nav">
            <li><a href="Main.php"><b>Inicio</b></a></li>
            <li><a href="emp.php"><b>Empleado</b></a></li>
            </ul>
            </div>
            <?php }?>

                
           
        </div>
    </body>
</html>