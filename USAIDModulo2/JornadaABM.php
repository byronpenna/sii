<?php

/**
 * @author  Jos� Manuel Calder�n
 * @copyright 2014
 */

require '../net.php';

if(isset($_POST['Enviar']))
{
    
    /** Guardando Asistencia **/
    if($_POST['Enviar'] == "Guardar")
    {        
        $Insertar = $bddC->prepare("Insert into Objetivo2_Jornadas values(null, :ic, :jornada, :fecha, :hora)");
        $Insertar->bindParam(':ic', $_POST['Ic']);
        $Insertar->bindParam(':jornada', $_POST['Jornada']);
        $Insertar->bindParam(':fecha', $_POST['FechaJ']);
        $Insertar->bindParam(':hora', $_POST['Horas']);           
        $Insertar->execute();   
        
        $Select = $bddC;                       
        $ij = $Select->lastInsertId();
        $asis = isset($_POST['Asistencia']) ? $_POST['Asistencia'] : array();        
    
        for ($index = 0 ; $index < count($asis); $index ++) 
        {   
            $InsertarNota = $bddC->prepare("Insert into Objetivo2_Asistencia values(null, :ij, :ip)");
            $InsertarNota->bindParam(':ij', $ij);
            $InsertarNota->bindParam(':ip', $asis[$index]);     
            $InsertarNota->execute();   
               
        }
        
        Redireccion("../USAID2.php?u=Course&n=1");
    }
    
    
    if($_POST['ij'] != "")
    {
        $_SESSION['ij'] = $_POST['ij'];
        Redireccion("../USAID2.php?u=Day");
    }
    
    /** Actualizando Asistencias **/
    if($_POST['Enviar'] == "Actualizar")
    {        
        $Update = $bddC->prepare("Update Objetivo2_Jornadas set
                                  Jornada = :jornada,
                                  Fecha = :fecha,
                                  Horas = :hora
                                  where IdJornada = :ij  ");
                                    
        $Update->bindParam(':jornada', $_POST['Jornada']);
        $Update->bindParam(':fecha', $_POST['FechaJ']);
        $Update->bindParam(':hora', $_POST['Horas']);
        $Update->bindParam(':ij', $_SESSION['ij']);             
        $Update->execute();   

        $Delete = $bddC->prepare("Delete from Objetivo2_Asistencia where IdJornada = " . $_SESSION['ij']);
        $Delete->execute();
                              
        $ij = $_SESSION['ij'];
        $asis = isset($_POST['Asistencia']) ? $_POST['Asistencia'] : array();        
    
        for ($index = 0 ; $index < count($asis); $index ++) 
        {   
            $InsertarNota = $bddC->prepare("Insert into Objetivo2_Asistencia values(null, :ij, :ip)");
            $InsertarNota->bindParam(':ij', $ij);
            $InsertarNota->bindParam(':ip', $asis[$index]);     
            $InsertarNota->execute();   
               
        }
        
        Redireccion("../USAID2.php?u=Course&n=1");
    }    
    
    if($_POST['Enviar'] == "Borrar Jornada")
    {         
        $Delete = $bddC->prepare("Delete from Objetivo2_Jornadas where IdJornada = " . $_SESSION['ij']);
        $Delete->execute();
        Redireccion("../USAID2.php?u=Course&n=3");
    }
}
?>