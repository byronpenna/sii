<?php

/**
 * @author Manuel Calder�n
 * @copyright 2014
 */
    
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=Detalles del Grupo.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';

	
    $Grupos = $bddC->prepare("Select * from Objetivo2_Grupos where IdGrupo = " . $_SESSION['ig']);
    $Grupos->execute();
    $DataG = $Grupos->fetch();

    $Departamento = $bddC->prepare("Select Departamento from  USAID_Departamentos where IdDepartamento = $DataG[6]");
    $Departamento->execute();
    $DepartamentoAux = $Departamento->fetch();
    
?>
<style>
.tdt
{
    border-style: solid; 
    border-color: black;
}
</style>
<table >
<tr><td colspan="6"><h2>Proyecto Educaci�n para la Ni�ez y Juventud - Objetivo 2</h2></td></tr>
<tr></tr>
<tr><td>N� de Grupo: </td><th colspan="2" style="text-align: left;"><?php echo $DataG[1];?></th></tr>
<tr><td>Fecha de Inicio: </td><th colspan="2" style="text-align: left;"><?php echo $DataG[2];?></th><td colspan="2" >Fecha de Finalizaci�n:</td><th colspan="3" style="text-align: left;"><?php echo $DataG[3];?></th></tr>
<tr><td>Centro Colaborador:</td><th colspan="2" style="text-align: left;"><?php echo $DataG[4];?></th><td colspan="2">Codigo:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[5];?></th></tr>
<tr><td>Departamento:</td><th colspan="2" style="text-align: left;"><?php echo $DepartamentoAux[0];?></th><td colspan="2">Municipio:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[7];?></th></tr>
<tr><td>Beneficiado:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[8];?></th></tr>
<tr><td style="text-align: left;" colspan="4"><h2>Detalles del Grupo</h2><hr color='skyblue' /></td></tr>
<tr>
    <td colspan="4">
        <table style="width: 100%; border: 1px;">
            <tr>
                <th rowspan="2" style="width: 40%;">Curso</th>
                <th colspan="3" style="width: 30%;">Revisi�n por Horas</th>
                <th rowspan="2" style="width: 10%;">Total de <br />Jovenes</th>
                <th rowspan="2" style="width: 10%;">Total de <br />Horas</th>
                <th rowspan="2" style="width: 10%;">Total a <br />Liquidar</th></tr>
            <tr>
                <th style="width: 10%;">0 Horas</th>
                <th style="width: 10%;">1 - 15 Horas</th>
                <th style="width: 10%;">16+ Horas</th>
            </tr>
            <tr>
                <?php
                        $Cursos = $bddC->prepare("Select * from Objetivo2_Cursos where IdGrupo = " . $_SESSION['ig']);
                        $Cursos->execute();

                        $ArrayP = array();
                        $Participantes = $bddC->prepare("SELECT * FROM Objetivo2_Participante where IdGrupo = " . $_SESSION['ig']);
                        $Participantes->execute();
                        while($DataP = $Participantes->fetch())
                            array_push($ArrayP,$DataP[0]);

                        $ArrayT = array();
                        
                        for($i = 0; $i < 6; $i++)
                            $ArrayT[$i] = 0;
                            
                        while($DataC = $Cursos->fetch())
                        {
                            $Aux0 = 0;
                            $Aux115 = 0;
                            $Aux16 = 0;
                            $AuxMonto = 0;
                            $AuxHoras = 0;
                            
                            echo "<tr><th>$DataC[2]</th>";
                            
                            foreach($ArrayP as $value)
                            {
                                $Horas = $bddC->prepare("SELECT j.Horas FROM Objetivo2_Jornadas as j 
                                                         INNER JOIN Objetivo2_Asistencia as a on j.IdJornada = a.IdJornada
                                                         WHERE a.IdParticipante = $value and j.IdCurso = $DataC[0]");
                                
                                $Horas->execute(); 
                                
                                $aux = 0;
                                while($DataH = $Horas->fetch())
                                {
                                    $aux += $DataH[0];
                                }
                                
                                
                                if($aux == 0)
                                    $Aux0++;
                                    
                                else if($aux < 16)
                                    $Aux115++;
                                    
                                else
                                    $Aux16++;
                                
                                
                                if($DataC[2] == "Refuerzo Pedag�gico")
                                    $Multipicador = 2;
                                
                                else
                                    $Multipicador = 4;
                                $AuxHoras += $aux;
                                $AuxMonto += ($aux * $Multipicador);
                            }
                            
                            echo "<th>$Aux0</th><th>$Aux115</th><th>$Aux16</th>";
                            echo "<th>" . count($ArrayP) . "</th><th>$AuxHoras</th><th>$".number_format($AuxMonto, 2) ."</th>";
                            
                            $ArrayT[0] += $Aux0;
                            $ArrayT[1] += $Aux115;
                            $ArrayT[2] += $Aux16;
                            $ArrayT[3] += count($ArrayP);
                            $ArrayT[4] += $AuxHoras;
                            $ArrayT[5] += $AuxMonto;
                        }
                ?>
            </tr>
            <tr><td colspan="7"><hr color='skyblue' /></td></tr>
            <tr>
                <th>Total</th>
                <th><?php echo $ArrayT[0];?></th>
                <th><?php echo $ArrayT[1];?></th>
                <th><?php echo $ArrayT[2];?></th>
                <th><?php echo $ArrayT[3];?></th>
                <th><?php echo $ArrayT[4];?></th>
                <th>$<?php echo number_format($ArrayT[5], 2);?></th>
            </tr>
        </table>
    </td>
</tr>
<tr><td></td></tr>
</table>