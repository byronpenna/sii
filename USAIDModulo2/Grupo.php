<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 
 unset($_SESSION['ig']);

    if(!isset($_GET['pag'])) 
	   $pag = 1;	
	
	else 
	   $pag = $_GET['pag'];
        
   	$hasta = 10;
	$desde = ($hasta * $pag) - $hasta;
    
    
    if(isset($_POST['busqueda']))
        $Busqueda = $_POST['busqueda'];

    else
    {
       if(isset($_GET['b'])) 
            $Busqueda = $_GET['b'];
       
       else
            $Busqueda = "";
    }
    
   $Busqueda = $_POST['busqueda']; 
   
   if($_SESSION['Roll'] == "Sociolaboral")
   {     
         
         if(isset($_POST['fase']))
         {
            if($_POST['fase'] == "1� Fase")
                $_SESSION['fase'] = "2� Fase";
                
            else
                $_SESSION['fase'] = "1� Fase";    
         }
         else
         {
            if($_SESSION['fase'] == "")
                $_SESSION['fase'] = "1� Fase";
         }
              
                  
   }
   
   
   if($_SESSION['Roll'] == "Sociolaboral")
   {  
       if($_SESSION['fase'] == "1� Fase")
       {
            $Query = "Select * from Objetivo2_Grupos 
                     where (Nombre like '%$Busqueda%' or Municipio like '%$Busqueda%' or Direccion like '%$Busqueda%') and IdGrupo < 97
                     group by Nombre ASC ";
       }
       else
       {
            
            $Query = "Select * from Objetivo2_Grupos 
                     where (Nombre like '%$Busqueda%' or Municipio like '%$Busqueda%' or Direccion like '%$Busqueda%') and IdGrupo >= 97
                     group by Nombre ASC ";
       } 
       
   }
   else
   {
       $Query = "Select * from Objetivo2_Grupos 
                 where Nombre like '%$Busqueda%' or Municipio like '%$Busqueda%' or Direccion like '%$Busqueda%'
                 group by Nombre ASC ";    
   }
   
   
   
   $PaginasG = $bddC->prepare($Query);   
   $PaginasG->execute();        
             
   $Query .= " LIMIT $desde, $hasta";
   
   
   $Grupos = $bddC->prepare($Query);   
   $Grupos->execute();

?>

<div class="Raiz">
    <a href="?" class='flecha' style='text-decoration:none'>Principal -></a>
    <a href="?u=Group" class='flecha' style='text-decoration:none'>Grupo</a>
</div>

<div style="width: 100%; background-color: white; border-radius: 10px;">
    <br /><br />
    <table style="width: 100%;">
        <tr>
            <td>
                <table style="width: 80%; margin-left: 10%;">
                    <tr>
                    <form action="" method="post">
                        <td style="width: 25%;" class="tdleft">Busqueda de grupo:</td>
                        <td style="width: 45%;"><input type="text" name="busqueda" style="width: 300px;" value="<?php echo $Busqueda;?>"  /></td>
                        <td style="width: 10%; text-align: center;"><input type="button" class="boton" name="busqueda" value="?" style="width: 25px;"/></td>
                        <td style="width: 10%;">
                            <?php
                                    if($_SESSION['Roll'] == "Sociolaboral")
                                       echo "<a href='?u=GroupForm'><input type='button' class='boton' value='Crear Grupo' /></a>";
                                       
                                    if($_SESSION['Roll'] == "Contabilidad")
                                       echo "<a href='?u=Reports'><input type='button' class='boton' value='Reportes' /></a>";                                       
                            ?>
                        </td>    
                    </form>
                    <form action="?u=GroupAllDetails" method="post">
                        <td style="width: 10%; text-align: center;"><input type="submit" class="boton" name="busqueda" value="Detalles"/></td>
                    </form>
                    <?php
                            if($_SESSION['Roll'] == "Sociolaboral")
                            {
                                echo "<form action='?u=Group' method='post'>
                                        <td style='width: 10%; text-align: center;'><input type='submit' class='boton' name='fase' value='".$_SESSION['fase']."'/></td>
                                     </form>";   
                            }
                    ?>                    
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        
        <tr>
            <td><hr color='skyblue' />
                <table style="width: 90%; margin-top: 30px; margin-bottom: 30px; margin-left: 5%;">
                    <tr><th>ID de Grupo</th><th>Nombre del Grupo</th><th>Municipio</th><tr></tr></tr>
                    <?php
                           if($Grupos->rowCount() > 0)
                           {
       	                       while($DataG = $Grupos->fetch())
                               {
                                    echo "<tr><th>$DataG[0]</th><th>$DataG[1]</th><th>$DataG[7]</th>
                                              <th>
                                                <form action='?u=GroupProfile' method='post'>
                                                    <input type='hidden' name='ig' value='$DataG[0]' />
                                                    <input type='submit' name='Enviar' value='Ver' class='boton' style='width: 35px;' />
                                                </form>
                                              </th>
                                          </tr>";
                               }                            
                           }
                           else
                           {
                                echo "<tr><td colspan='4' style='text-align: center;'><h2 style='color:red'>No se ha encontrado ning�n Grupo</h2></td></tr>";
                           } 
                    ?>
                </table>
            </td>
        </tr>
        <tr>
            <td>
            <?php
            	
                $paginas = ceil($PaginasG->rowCount() / $hasta);

                
                $direccion = "USAID2.php?u=Group";
                echo     "<table style='width: 50%; margin-left:25%; text-align: center;'>
                                    <tr><th colspan='3'>P�gina [$pag] de [$paginas]</th></tr>";
                              
                if ($pag > 1)
                    echo "<tr><td style='width: 33%;'><a style='text-decoration:none' href=\"$direccion&pag=1&b=$Busqueda\"> <<- </a> | <a style='text-decoration:none' href=\"$direccion&pag=" . ($pag -1) . "&b=$Busqueda\"> <- </a> | </td>"; 
                else
                    echo "<tr><td style='width: 33%;'></td>";
                            
                    echo "<td style='width: 34%;'><a style='text-decoration:none' href=\"Instituciones.php?in=InsList\">Principal</a></td>";
            
                if ($pag < $paginas)
                    echo "<td style='width: 33%;'> |<a style='text-decoration:none' href=\"$direccion&pag=" . ($pag + 1) . "&b=$Busqueda\"> -> </a> | <a style='text-decoration:none' href=\"$direccion&pag=" . ($paginas) ."&b=$Busqueda\"> ->> </a> </td>";    
                else
                    echo "<td style='width: 33%;'></td></tr>";               
            
                echo "</table>";
            ?>
                
                
            </td>
        </tr>
    </table>
</div>