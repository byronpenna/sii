<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 
require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Guardar")
    {
        $Insert = $bddC->prepare("Insert into Objetivo2_Grupos values (null, :nombre, :fechai, :fechaf, :colaborador, :codigo, :departamento, :municipio, :direccion, :usuario)");
        $Insert->bindParam('nombre', $_POST['nombre']);
        $Insert->bindParam('fechai', $_POST['fechai']);
        $Insert->bindParam('fechaf', $_POST['fechaf']);
        $Insert->bindParam('colaborador', $_POST['centro']);
        $Insert->bindParam('codigo', $_POST['codigo']);
        $Insert->bindParam('departamento', $_POST['Departamentos']);
        $Insert->bindParam('municipio', $_POST['Municipio']);
        $Insert->bindParam('direccion', $_POST['direccion']);
        $Insert->bindParam('usuario', $_SESSION['IdUsuario']);
        $Insert->execute();        
        
        Redireccion("../USAID2.php?u=Group&n=1");
    }
    
    /** A�adiendo J�venes **/
    elseif( $_POST['Enviar'] ==  "A�adir")
    {
        $Insert = $bddC->prepare("Insert into Objetivo2_Participante values (null, :idg, :nombre, :apellido)");
        $Insert->bindParam('idg', $_SESSION['ig']);
        $Insert->bindParam('nombre', $_POST['Nombre']);
        $Insert->bindParam('apellido', $_POST['Apellido']);        
        $Insert->execute();        
        
        Redireccion("../USAID2.php?u=GroupProfile&n=1");        
    }
    
    /** Modificar J�venes **/
    elseif( $_POST['Enviar'] ==  "Modificar")
    {
        $Update = $bddC->prepare("Update Objetivo2_Participante set Nombres = :nombre, Apellidos = :apellido where IdParticipante = :id");
        
        $Update->bindParam('nombre', $_POST['Nombre']);
        $Update->bindParam('apellido', $_POST['Apellido']);        
        $Update->bindParam('id', $_POST['Ip']);
        $Update->execute();        
        
        Redireccion("../USAID2.php?u=GroupProfile&n=2");        
    }    
    
    /** Eliminando Joven **/
    elseif( $_POST['Enviar'] ==  "x")
    {
        $Delete = $bddC->prepare("Delete from Objetivo2_Participante where IdParticipante = :Ip");
        $Delete->bindParam('Ip', $_POST['Ip']);        
        $Delete->execute();        
        
        Redireccion("../USAID2.php?u=GroupProfile&n=3");        
    }
    
    /** Creando el Curso **/
    elseif( $_POST['Enviar'] ==  "Crear")
    {
        $Insert = $bddC->prepare("Insert into Objetivo2_Cursos values (null, :idg, :nombre, :hm, :ht)");
        $Insert->bindParam('idg', $_SESSION['ig']);
        $Insert->bindParam('nombre', $_POST['Nombre']);
        $Insert->bindParam('hm', $_POST['Hminimas']);
        $Insert->bindParam('ht', $_POST['Htotales']);
        $Insert->execute();       
        
        Redireccion("../USAID2.php?u=GroupProfile&n=1");        
    }    
    
    /** Eliminando Curso **/
    elseif( $_POST['Enviar'] ==  "X")
    {
        $Delete = $bddC->prepare("Delete from Objetivo2_Cursos where IdCurso = :Ic");
        $Delete->bindParam('Ic', $_POST['Ic']);        
        $Delete->execute();        
        
        Redireccion("../USAID2.php?u=GroupProfile&n=3");        
    }
    
//    /** Redirecci�n de Curso **/
//    elseif( $_POST['Enviar'] ==  "Ver")
//    {
//        $_SESSION['Ic']  = $_POST['Ic'];
//        Redireccion("../USAID2.php?u=Course");        
//    } 

    /** Actualizar Grupo **/
    elseif( $_POST['Enviar'] ==  "Modificar Grupo")
    {
        Redireccion("../USAID2.php?u=GroupForm");        
    }
    
    /** Actualizar Grupo **/
    elseif( $_POST['Enviar'] ==  "Actualizar")
    {
        $Update = $bddC->prepare("Update Objetivo2_Grupos set 
                                  Nombre =:nombre, FechaInicio = :fechai, FechaFin = :fechaf, Colaborador = :colaborador, Codigo = :codigo,
                                  Departamento = :departamento, Municipio = :municipio, Direccion = :direccion 
                                  where IdGrupo = :Ig");
                                  
        $Update->bindParam('nombre', $_POST['nombre']);
        $Update->bindParam('fechai', $_POST['fechai']);
        $Update->bindParam('fechaf', $_POST['fechaf']);
        $Update->bindParam('colaborador', $_POST['centro']);
        $Update->bindParam('codigo', $_POST['codigo']);
        $Update->bindParam('departamento', $_POST['Departamentos']);
        $Update->bindParam('municipio', $_POST['Municipio']);
        $Update->bindParam('direccion', $_POST['direccion']);
        $Update->bindParam('Ig', $_SESSION['ig']);
        $Update->execute();                        
        
        Redireccion("../USAID2.php?u=GroupProfile&n=2");        
    }     
    
    /** Eliminando Curso **/
    elseif( $_POST['Enviar'] ==  "Eliminar Grupo")
    {
        $Delete = $bddC->prepare("Delete from Objetivo2_Grupos where IdGrupo = :Ig");
        $Delete->bindParam('Ig',  $_SESSION['ig']);        
        $Delete->execute();        
        
        Redireccion("../USAID2.php?u=Group&n=3");        
    }           
    
    /** Actualizar Grupo **/
    elseif( $_POST['Enviar'] ==  "Detalle de Grupo")
    {
        Redireccion("../USAID2.php?u=GroupDetails");        
    }    
        
}

?>