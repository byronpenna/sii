<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

if($_SESSION['ij'] == "")
    $Query = "Select * from Objetivo2_Grupos as g 
              inner join Objetivo2_Cursos as c on g.IdGrupo = c.IdGrupo
              where c.IdCurso = " . $_POST['Ic'];

else
    $Query = "Select * from Objetivo2_Grupos as g 
              inner join Objetivo2_Cursos as c on g.IdGrupo = c.IdGrupo
              inner join Objetivo2_Jornadas as j on j.IdCurso = c.IdCurso
              where IdJornada = " .$_SESSION['ij'];



$Select = $bddC->prepare($Query);

                          
$Select->execute();
$DataG = $Select->fetch();

if($_SESSION['ij'] != "")
{
    $Jornada = $bddC->prepare("SELECT * FROM Objetivo2_Jornadas where IdJornada = " .$_SESSION['ij']);
    $Jornada->execute();
    
    $DataJ = $Jornada->fetch();
    $ABM = "Actualizar";
}
else
{
    $ABM = "Guardar";
}

?>

<div class="Raiz">
    <a href="?" class='flecha' style='text-decoration:none'>Principal -></a>
    <a href="?u=Group" class='flecha' style='text-decoration:none'>Grupo -></a>
    <a href="?u=GroupProfile" class='flecha' style='text-decoration:none'>Perfil de Grupo -></a>
    <a href="?u=Course" class='flecha' style='text-decoration:none'>Cursos -></a>
    <a href="?u=Day" class='flecha' style='text-decoration:none'>Jornada</a>
</div>

<div style="width: 100%; background-color: white; border-radius: 10px;">    
    <table style="width: 90%; margin-left: 5%;">
    <form method="post" action="USAIDModulo2/JornadaABM.php" id="AsistenciaForm" name="AsistenciaForm">
    <input type="hidden" name="Ic" value="<?php echo $_POST['Ic']?>"/>
        <tr><td colspan="4" style="padding-left: 60px;"><h2>Administraci�n del Curso</h2></td></tr>
        <tr><td style="width: 20%;">Nombre del Grupo:</td><td style="width: 40%;"><?php echo $DataG[1]?></td>
            <td rowspan="2" colspan="2" style="text-align: center; vertical-align: central;">
            <?php
        	if($_SESSION['ij'] != "")
            {
                echo "<input type='submit' name='Enviar' class='botonE' value='Borrar Jornada' onclick='return confirm(\"�Esta seguro de borrar esta jornada?\")' />";
            }
            ?>
            </td></tr>
        <tr><td style="width: 20%;">Nombre del Curso:</td><td style="width: 40%;"><?php echo $DataG[12]?></td></tr>
        <tr>
            <td colspan="4"><hr color='skyblue' />
                <table style="width: 70%; margin-left: 15%;">
                    <tr><td colspan="3">
                        <table style="width: 70%;">
                            <tr>
                                <td>Nombre de la Jornada:</td><td><input type="text" value="<?php echo $DataJ[2]?>" name="Jornada" required="" title="Campo Requerido" /></td>
                            </tr>
                            <tr>
                                <td>Fecha de la Jornada<br /><span>(aaaa-mm-dd)</span>:</td><td><input type="text" value="<?php echo $DataJ[3]?>" name="FechaJ" required="" pattern="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])" title="Formato Incorrecto" /></td>
                            </tr>
                            <tr>
                                <td>Total de horas:</td><td><input type="number" value="<?php echo $DataJ[4]?>" name="Horas" required="" title="Formato Incorrecto" min="1" max="20" /></td>
                            </tr>
                        </table>
                    </td></tr>
                    <tr><td colspan="3"><br /></td></tr>
                    </table>
                    <table style="width: 70%; margin-left: 15%;" rules='all'>
                    <tr><th>N�</th><th>Apellidos, Nombre</th><th style="width: 20%;"> 
                    <input type='button' value='Marcar' id='botoncheck' onclick='checkear();' class='boton' style='width: 100px;' /></td></tr>                    
                    <tr>                    
                    <?php
	                       $Participantes = $bddC->prepare("Select * from Objetivo2_Participante where IdGrupo = " . $_SESSION['ig']);
                           $Participantes->execute();
                           
                           if($Participantes->rowCount() > 0)
                           {
                               $i = 0;
                               while($DataP = $Participantes->fetch())
                               {
                                    $i++;                                    
                                    $Asistencia = $bddC->prepare("Select * from Objetivo2_Asistencia where IdParticipante = $DataP[0] and IdJornada = " .  $_SESSION['ij']);
                                    $Asistencia->execute();
                                    
                                    if($Asistencia->rowCount() > 0) 
                                        $DataA = "checked";
                                        
                                    else
                                        $DataA = "";
                                    
                                    echo "<tr><td>$i</td><td>$DataP[2], $DataP[3] </td><th><input type='checkbox' name='Asistencia[]' value='$DataP[0]' $DataA /> </th></tr>";
                               }
                                    echo "<tr><td colspan='3' style='text-align: center;'><input type='submit' name='Enviar' class='boton' value='$ABM'/></td></tr>";
                           }
                           else
                                echo "<tr><td colspan='3' style='color: red; text-align:center;'>No hay Participantes Registrados</td></tr>";

                    ?>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
    </form>
    </table>
</div>    
<script>
function checkear()
{
    boton = document.getElementById('botoncheck');    
    if(boton.value == "Marcar")
    { 
        seleccionar_todo();
        boton.value = "Desmarcar"
    }
    else
    {
        deseleccionar_todo();
        boton.value = "Marcar"
    }
}

function seleccionar_todo()
{ 
   for (i=0;i<document.AsistenciaForm.elements.length;i++) 
      if(document.AsistenciaForm.elements[i].type == "checkbox")	
         document.AsistenciaForm.elements[i].checked = 1;  
}

function deseleccionar_todo()
{ 
   for (i=0;i<document.AsistenciaForm.elements.length;i++) 
      if(document.AsistenciaForm.elements[i].type == "checkbox")	
         document.AsistenciaForm.elements[i].checked = 0; 
}
</script>