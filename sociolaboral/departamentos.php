    <?php
    if(!isset($_SESSION["autenticado"])){
        require '../net.php';
    } ?>
    
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Estadisticas de proyectos</title>
</head>
<script src="code/highcharts.js"></script>
<script src="code/highcharts-3d.js"></script>
<script src="code/modules/exporting.js"></script>
<script src="code/modules/export-data.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<body>

     <div style="float:right; width: 75%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="7"><h2 style="color: #197198;">Estadisticas</h2></td></tr>
        <tr><td colspan="7"><hr color='skyblue' /></td></tr>
        <tr>

 <td style="width: 20%;"><a href="?l=Sociolaboral">
                <input type="submit" value="Municipios" name="Enviar" class="botonG" style="width: 105px;" /></a>
            </td>
</tr>
</table>
<script src="Highcharts-6.1.0/code/highcharts.js"></script>
<script src="Highcharts-6.1.0/code/modules/exporting.js"></script>
<script src="Highcharts-6.1.0/code/modules/export-data.js"></script>

<?php


 $sql = ("SELECT departamento, COUNT(*) as cantidad FROM beneficiarios GROUP BY departamento ");


echo "<div id=\"container\" style=\"min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto\"></div>";
?>

</div>
</body>
</html>

<script type="text/javascript">

Highcharts.chart('container', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: 'Beneficiarios segun su Departamento.'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        data: [

            <?php
            foreach ($bddr->query($sql) as $row) {
                //echo "<br>";
                if ($row['departamento'] == ''){print "['indefinido'," .$row['cantidad'] . "],";}else{
                print "['" . $row['departamento'] . "'," .$row['cantidad'] . "],";}
                //echo "<br>";
            }
            ?>
        ]
    }]
});
</script>



</body>
</html>