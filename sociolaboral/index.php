<?php


	//require('../net.php');

	/* Getting demo_viewer table data */

	$sql = $bddr->prepare ("SELECT COUNT(sexo) AS Mujeres FROM beneficiarios where sexo='m' and anio='2014'");

$sql->execute();


	$viewer = $sql->fetchAll();
	$viewer = json_encode(array_column($viewer, 'Mujeres'),JSON_NUMERIC_CHECK);


	/* Getting demo_click table data */

	$sql_h = $bddr->prepare("SELECT COUNT(sexo) AS Hombres FROM beneficiarios where sexo='h' and anio='2014'");
$sql_h->execute();

	$click = $sql_h->fetchAll();

	$click = json_encode(array_column($click, 'Hombres'),JSON_NUMERIC_CHECK);


?>


<!DOCTYPE html>

<html>

<head>

	<title>HighChart</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>

	<script src="https://code.highcharts.com/highcharts.js"></script>

</head>

<body>


<script type="text/javascript">


$(function () { 


    var data_click = <?php echo $click; ?>;

    var data_viewer = <?php echo $viewer; ?>;


    $('#container').highcharts({

        chart: {

            type: 'column'

        },

        title: {

            text: 'Resultados año 2014'

        },

        xAxis: {

            categories: ['Sexo']

        },

        yAxis: {

            title: {

                text: 'Personas'

            }

        },

        series: [{

            name: 'Hombres',

            data: data_click

        }, {

            name: 'Mujeres',

            data: data_viewer

        }]

    });

});


</script>


<div class="container">

	<br/>


    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">

                <div class="panel-heading">Graficos</div>

                <div class="panel-body">

                    <div id="container"></div>

                </div>

            </div>

        </div>

    </div>

</div>


</body>

</html>