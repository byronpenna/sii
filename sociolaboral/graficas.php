    <?php
    if(!isset($_SESSION["autenticado"])){
        require '../net.php';
        
    } ?>
    
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Estadisticas de proyectos</title>
   
</head>



<script src="code/highcharts.js"></script>
<script src="code/highcharts-3d.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<style type="text/css">

.styled-select {
    color: black;
   background: url(https://i62.tinypic.com/15xvbd5.png) no-repeat 96% 0;
   height: 29px;
   overflow: hidden;
   width: 240px;
  
}

.styled-select select {
    color: black;
   background: transparent;
   border: none;
   font-size: 14px;
   height: 29px;
   padding: 5px; 
   width: 268px;
}




.semi-square {
   -webkit-border-radius: 5px;
   -moz-border-radius: 5px;
   border-radius: 5px;
}

/* -------------------- Colors: Background */
.blue    { background-color: #3b8ec2; }



.blue select    { color: black; }



  .boton_f{
    text-decoration: none;
    padding: 10px;
    font-weight: 600;
    font-size: 14px;
    color: #ffffff;
    background-color: #1883ba;
    border-radius: 6px;
    border: 2px solid #0016b0;
  }
  .boton_f:hover{
    color: #1883ba;
    background-color: #ffffff;
  }
</style>


<body>
    <meta charset="utf8" />

     <div style="float:right; width: 79%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
        <tr><td colspan="7"><h1 style="color: #197198; text-align: center;">Estadisticas</h1></td></tr>
        <tr><td colspan="7"><hr color='skyblue' /></td></tr>
        <tr>

<?php 
$op = "";
$sqlgroup = "";
$sql2 = "";

if (isset($_POST['opciones'])) {
    $op = $_POST['opciones'];
    $sql2 = "SELECT b.$op, COUNT(*) as cantidad FROM beneficiarios as b";
    $sqlgroup = " GROUP BY b.$op";

    $sqlb = "select b.$op, count(*) as cantidad from beneficiarios as b join contratados as c on b.codigo = c.codigo_v";
}

$sql = "SELECT edad, COUNT(*) as cantidad FROM beneficiarios GROUP BY edad ";

$anios = 'select anio from beneficiarios group by anio';

$edades = 'select edad from beneficiarios group by edad';
$municipios = 'select municipio from beneficiarios group by municipio';
$cursos ='select curso from contratados group by curso';
$empresas = 'select empresa from contratados group by empresa ';
$filtrar = false;

 if (isset($_POST['anios']) || isset($_POST['sexo']) || isset($_POST['muni'])) {
    if ($_POST['cursos'] != 'seleccione una opcion' || $_POST['empresas'] != 'seleccione una opcion') {
        $sql2 = $sqlb;
    }
    if ($_POST['anioss'] != 'seleccione una opcion' && $_POST['anios'] != 'seleccione una opcion') {
            $sql2 = $sql2.' where b.anio between '.$_POST['anios']. ' and ' .$_POST['anioss'];
            $filtrar = true;
            echo $_POST['anios']. '  y  ' . $_POST['anioss']  ;
    }

    elseif ($_POST['anios'] != 'seleccione una opcion') {
            $sql2 = $sql2.' where b.anio = '.$_POST['anios'];
            $filtrar = true;
            echo $_POST['anios'];
    }


    if ($_POST['edad'] != 'seleccione una opcion') {
        if ($filtrar = false) {
            $sql2 = $sql2.' where edad = '.$_POST['edad'];

        }
        elseif ($filtrar = true) {
            $sql2 = $sql2.' and edad = '.$_POST['edad'];  

        }
        $filtrar = true;
    }

    if ($_POST['sexo'] != 'seleccione una opcion') {
        if ($filtrar == false) {
            $sql2 = $sql2.' where b.sexo = \''.$_POST['sexo']."'";
            $filtrar = true;
        }
        elseif ($filtrar == true) {
            $sql2 = $sql2.' and b.sexo = \''.$_POST['sexo']."'";   
        }
        
    }

    if ($_POST['muni'] != 'seleccione una opcion') {
        if ($filtrar == false) {
            $sql2 = $sql2.' where b.municipio = \''.$_POST['muni']."'";
            $filtrar = true;
        }
        elseif ($filtrar == true) {
            $sql2 = $sql2.' and b.municipio = \''.$_POST['muni']."'";   
        }
        
    }

    if ($_POST['cursos'] != 'seleccione una opcion') {
        if ($filtrar == false) {
            $sql2 = $sql2.' where c.curso = \''.$_POST['cursos']."'";
            $filtrar = true;
        }
        elseif ($filtrar == true) {
            $sql2 = $sql2.' and c.curso = \''.$_POST['cursos']."'";   
        }
        
    }

    if ($_POST['empresas'] != 'seleccione una opcion') {
        if ($filtrar == false) {
            $sql2 = $sql2.' where c.empresa = \''.$_POST['empresas']."'";
            $filtrar = true;
        }
        elseif ($filtrar == true) {
            $sql2 = $sql2.' and c.empresa = \''.$_POST['empresas']."'";   
        }
        
    }
}

 ?>
         
</tr>
</table>
<script src="code/highcharts.js"></script>
<script src="code/modules/exporting.js"></script>
<script src="code/modules/export-data.js"></script>
   <center>
 <div id="filtros"  style="width: 600px;">




     <form action="?l=Sociolaboral" method="POST">

<TABLE  BORDER = 0 CELLPADDING=5 CELLSPACING=0 >
    <TR>
        <TD>
        Graficar por
        <div class="styled-select blue semi-square">
        <select name="opciones"><br>
        <option value="edad">Edad</option>
        <option value="sexo">Sexo</option>
<!--        <option value="municipio">municipio</option> -->
        <option value="departamento">Departamento</option>
        </select>
    </div>
</TD> <TD>


     A&ntilde;os menor
<div class="styled-select blue semi-square">
       <select name="anios" class="form-control">
            <option value="seleccione una opcion">Seleccione una opcion</option>
            <?php
            foreach ($bddr->query($anios) as $row){
                echo "<option value='".$row['anio']."'>".$row['anio']."</option>";
            }?>
        </select>
        
           </div>
           </TD>

           <td>
           	A&ntilde;o mayor
           	<div  class="styled-select blue semi-square">
           		 <select name="anioss" class="form-control">
            <option value="seleccione una opcion">Seleccione una opcion</option>
            <?php
            foreach ($bddr->query($anios) as $row){
                echo "<option value='".$row['anio']."'>".$row['anio']."</option>";
            }?>
        </select>
           	</div>
           </td>
           
    </TR>
    <TR>
        <TD>
             Sexo
<div class="styled-select blue semi-square">
        
      <select name="sexo" class="form-control">
            <option value="seleccione una opcion">Seleccione una opcion</option>
            <option value="h">masculino</option>
            <option value="m">femenino</option>
            <option value="">no especificado</option>
        </select>
           </div>
           </TD> <TD>
           Municipio
<div class="styled-select blue semi-square">
         
        <select name="muni" class="form-control">
            <option value="seleccione una opcion">Seleccione una opcion</option>
             <?php
            foreach ($bddr->query($municipios) as $row){
                echo "<option value='".$row['municipio']."'>".$row['municipio']."</option>";
            }?>
         </select>
            </div>
            </TD> <TD>
            Curso
<div class="styled-select blue semi-square">

        <select name="cursos"  class="form-control">
            <option value="seleccione una opcion">Seleccione una opcion</option>
            <?php
            foreach ($bddr->query($cursos) as $row){
                echo "<option value='".$row['curso']."'>".$row['curso']."</option>";
            }?>
        </select>
           </div>
           </TD>
    </TR>
<TR><TD>

           Empresa
<div class="styled-select blue semi-square">

        <select name="empresas" class="form-control">
            <option value="seleccione una opcion">Seleccione una opcion</option>
            <?php
            foreach ($bddr->query($empresas) as $row){
                echo "<option value='".$row['empresa']."'>".$row['empresa']."</option>";
            }?>
        </select>
           </div>
       </TD>

<TD>
            Edad
<div class="styled-select blue semi-square">
       <select name="edad" class="form-control">
            <option value="seleccione una opcion">Seleccione una opcion</option>
            <?php
            foreach ($bddr->query($edades) as $row){
                echo "<option value='".$row['edad']."'>".$row['edad']."</option>";
            }?>
        </select>
           </div>
           </TD>
</TR>
</TABLE>
<br><br>
        <input type="submit" name="filtrar" value="Mostrar Grafica" class="boton_f">

     </form>

 </div>
</center>
<?php


echo "<div id=\"container\" style=\"min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto\"></div>";

echo "<div id=\"container\" style=\"min-width: 310px; max-width: 800px; height: 200px; margin: 0 auto\">"  ;
 if ($op == "") {
    echo "";
}else{

echo "<table style=\" margin: 0 auto\"  border='1' >";
 echo '<tr>';
 echo '<td><h3> '.$op.' </h3> </td>';
foreach ($bddr->query($sql2.$sqlgroup) as $row)
//print "['$op: " . $row["$op"] . " (".$row['cantidad']." personas)'," .$row['cantidad'] . "],"; 
 {
echo '<td class="textcenter"><h3>'.$row["$op"].'</h3></td>';

 }
echo ' </tr>';
echo ' <tr>';
echo ' <td><h3> Personas</h3> </td>';




foreach ($bddr->query($sql2.$sqlgroup) as $row)
//print "['$op: " . $row["$op"] . " (".$row['cantidad']." personas)'," .$row['cantidad'] . "],"; 
 {
 echo '
    <td class="textcenter"><h3>'.$row["cantidad"].'</h3></td> ';
}
echo ' </tr>
 ';

echo '</table>';
 }
 echo " </div>";


?>


</div>

</body>
</html>

<script type="text/javascript">

Highcharts.chart('container', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        <?php 
        if ($op == "") {
            echo "text: 'Beneficiarios segun su edad'";
        }else
            echo "text: 'Beneficiarios segun su ".$op."'"; 
        ?>
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Porcentaje',
        data: [

            <?php
            if ($op == "") {
                foreach ($bddr->query($sql) as $row) {
                    //echo "<br>";
                    if ($row["edad"] == ''){print "['"."edad: "."indefinido'," .$row['cantidad'] . "],";

                    }else{
                    print "['edad: " . $row["edad"] . " (".$row['cantidad']." personas)'," .$row['cantidad'] . "],";}
                    //echo "<br>";
                  

                }
            }else{
                foreach ($bddr->query($sql2.$sqlgroup) as $row) {
                    //echo "<br>";
                    if ($row["$op"] == ''){print "['"."$op: "."indefinido'," .$row['cantidad'] . "],";}else{
                    print "['$op: " . $row["$op"] . " (".$row['cantidad']." personas)'," .$row['cantidad'] . "],";}
                    //echo "<br>";
                }
            }

             ?>
        ]
    }]
});
</script>


<script src="code/modules/exporting.js"></script>
<script src="code/modules/export-data.js">
    


</script>

</body>
</html>