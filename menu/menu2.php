<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<link rel="stylesheet" type="text/css" href="css/default.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<script src="js/modernizr.custom.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="main">
				<nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
					<div class="cbp-hsinner">
						<ul class="cbp-hsmenu">
							<?php if($_SESSION["TipoUsuario"] == "Administrador"){?>
							<li><a href="Main.php">Inicio</a></li>
							<li><a href="Main.php?l=jovenl">Jovenes</a></li>
							<li><a href="Main.php?l=UsuL">Usuarios</a></li>
							<li><a href="Instituciones.php">Instituciones</a></li>
							<li><a href="Empleado.php">Empleados</a></li>
							<li><a href="monitoreo.php">Mis Encuestas</a></li>
							<li><a href="FUSALMO.php">FUSALMO</a></li>
							 <?php if($_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["IdUsuario"]== 1674 || $_SESSION["IdUsuario"]== 2622 || $_SESSION["IdUsuario"]== 213736){?> 
							<li><a href="Sociolaboral.php">Sociolaboral</a></li>
							<?php }?> 
							<li><a href="Autenticacion.php?exit=true">Salir</a></li>
						

							<?php }
								if($_SESSION["TipoUsuario"] == "Joven"){?>
							<li><a href="Main.php">Instituciones</a></li>
							<li><a href="Main.php?l=joven">Mis Datos</a></li>
							<li><a href="FUSALMO.php">FUSALMO</a></li>
							<li><a href="monitoreo.php?">Mis Encuestas</a></li>
							<li><a href="Autenticacion.php?exit=true">Salir</a></li>

							<?php }
							if($_SESSION["TipoUsuario"] == "Consultor"){?>
							<li><a href="Main.php">Inicio</a></li>
							<li><a href="Empleado.php">Empleados</a></li>
							<li><a href="FUSALMO.php">FUSALMO</a></li>
							<li><a href="monitoreo.php">Mis Encuestas</a></li>
							<?php if($_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["IdUsuario"]== 1674 || $_SESSION["IdUsuario"]== 2622 || $_SESSION["IdUsuario"]== 213736){?> 
							<li><a href="Sociolaboral.php">Sociolaboral</a></li>
							<?php }?> 
							<li><a href="Autenticacion.php?exit=true">Salir</a></li>

							<?php }
								if($_SESSION["TipoUsuario"] == "RRHH"){?>
							<li><a href="Main.php">Inicio</a></li>
							<li><a href="Empleado.php">Empleados</a></li>
							<li><a href="monitoreo.php">Mis Encuestas</a></li>
							<li><a href="Autenticacion.php?exit=true">Salir</a></li>

							<?php }
								if($_SESSION["TipoUsuario"] == "Coordinador"){?>
							<li><a href="Main.php">Inicio</a></li>
							<li><a href="Empleado.php">Empleados</a></li>
							<li><a href="FUSALMO.php">FUSALMO</a></li>
							<li><a href="Main.php?l=jovenl">Jovenes</a></li>
							<li><a href="monitoreo.php">Mis Encuestas</a></li>
							<?php if($_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["IdUsuario"]== 1674 || $_SESSION["IdUsuario"]== 2622 || $_SESSION["IdUsuario"]== 213736){?> 
							<li><a href="Sociolaboral.php">Sociolaboral</a></li>
							<?php }?> 
							<li><a href="Autenticacion.php?exit=true">Salir</a></li>
							
							<?php }
								if($_SESSION["TipoUsuario"] == "Gestor"){ ?>
							<li><a href="Main.php">Inicio</a></li>
							<li><a href="Empleado.php">Empleados</a></li>
							<li><a href="FUSALMO.php">FUSALMO</a></li>
							<li><a href="monitoreo.php">Mis Encuestas</a></li>
							<li><a href="Autenticacion.php?exit=true">Salir</a></li>

							<?php }
								if($_SESSION["TipoUsuario"] == "Buscador"){ ?>
							<li><a href="Main.php">Inicio</a></li>
							<li><a href="Main.php?l=jovenl">Jovenes</a></li>
							<li><a href="Main.php?l=UsuL">Usuarios</a></li>
							<li><a href="Autenticacion.php?exit=true">Salir</a></li>

							<?php }
								if($_SESSION["TipoUsuario"] == "USAID-CI" || $_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "USAID-SE"){ ?>
							<li><a href="Main.php">Inicio</a></li>
							<li><a href="Empleado.php">Empleados</a></li>
							<li><a href="Inventario.php">Inventario</a></li>
							<li><a href="Autenticacion.php?exit=true">Salir</a></li>

							<?php }
								if($_SESSION["TipoUsuario"] == "Desarrollo"){ ?>
							<li><a href="Main.php">Inicio</a></li>
							<li><a href="Empleado.php">Empleados</a></li>
							<li><a href="Autenticacion.php?exit=true">Salir</a></li>

						<?php }?>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<script src="js/cbpHorizontalSlideOutMenu.min.js"></script>
		<script>
			var menu = new cbpHorizontalSlideOutMenu( document.getElementById( 'cbp-hsmenu-wrapper' ) );
		</script>
	</body>
</html>
