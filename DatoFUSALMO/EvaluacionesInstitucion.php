<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script>
    $(document).ready(function(){
	   $("#IdIns").change(function(){cargar_secciones(this.form);});
       $("#IdSec").attr("disabled",true);
    });
    
    function cargar_secciones(form)
    {
        var Secciones = document.getElementById("IdSec");
        var opciones = form.IdSec.options;
        
        Secciones.options.length=0;
        
        var code = $("#IdIns").val();    	
        
        if(code != "..."){
        	$.get("DatoFUSALMO/EvaluacionSecciones.php", { code: code },
        		function(resultado)
        		{
        			if(resultado == false)
        			{
        				alert("Error");
        			}
        			else
        			{        			    
        				$("#IdSec").attr("disabled",false);
        				document.getElementById("IdSec").options.length=0;
        				$('#IdSec').append(resultado);	
        			}
        		}
        
        	);
        }
        else
        {                    
            var seleccionar = new Option("...","...","","");
            Secciones[0] = seleccionar;
        }
    }
    
    function Validar()
    {
        var Secciones = document.getElementById("IdSec");
        var Instituciones = document.getElementById("IdIns");
        
        if (Secciones[0].value == "...")
        {
            alert("Error, Seleccione una Institucion y luego la secci�n");
            return(false)
        }
        else
            return(true)
    }
</script>


<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administraci�n</a> ->
    <a href="/FUSALMO.php?d=Component" class='flecha' style='text-decoration:none'>�rea</a> ->
    <a href="/FUSALMO.php?d=EvaluationIns" class='flecha' style='text-decoration:none'>Evaluaciones</a>
</div>

<?php

    if($_SESSION['IdDato'] != "" || $_SESSION['Component']!= "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre, c.IdComponente, c.NombreComponente
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               Inner join Educacion_Componente as c on c.IdPeriodo = p.IdPeriodo                             
                               WHERE d.IdDatoFUSALMO = :id and c.IdComponente = :idc");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idc', $_SESSION['Component']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");
    
    
     if(isset($_GET['n']))
     {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right;'>Evaluaci�n Inscrita Exitosamente</div>";
            
        if($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right;'>Evaluaci�n retirada del Area</div>";                                
     }
?>

<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style='width: 90%; margin-left: 5%; margin-top: 10px;'>             
        <tr><td colspan="2" style="color: blue;">Evaluaciones de Areas de Aplicaci�n<br /><?php echo "$DatoView[1]<br />$DatoView[3]";?> </td></tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>                
        <tr><td  colspan="2" style="color: blue; text-align: right;" >Area de aplicaci�n: <br /><?php echo $DatoView[5];?><br /><br /> </td></tr>  
        <form name="evaluacion" method="post" action="DatoFUSALMO/EvaluacionesABM.php">
        <tr>
            <td>
                <table style="width: 80%;">
                <tr>
                    <td style="text-align: right; padding-right: 20px; width: 40%;">Seleccione la Institucion:</td>
                    <td><select id="IdIns" name="IdIns" style="width: 300px;">  
                        <option value='...' style='color:red'>...</option>                
                        <?php
        	                   $DatosInstituciones = $bdd->prepare("SELECT ins . * FROM  Educacion_Inscripcion AS edu_ins
                                                                    INNER JOIN Institucion AS ins ON edu_ins.IdInstitucion = ins.IdInstitucion
                                                                    where edu_ins.IdCiclo = :idP GROUP BY edu_ins.IdInstitucion ");
        
                               $DatosInstituciones->bindParam(':idP',$_SESSION["Ciclos"]);
                               $DatosInstituciones->execute();
                               
                               if($DatosInstituciones->rowCount() > 0)
                               {
                                   while($Instituciones = $DatosInstituciones->fetch())
                                   {
                                        echo "<option value='$Instituciones[0]'>$Instituciones[1]</option>";
                                   }
                               }
                               else
                                    echo "<option value='Error' style='color:red'>No hay instituciones Secciones registradas a este periodo.</option>";
                               
                        ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; padding-right: 20px; width: 40%;">Seleccione la Secci�n:</td>
                    <td><select id="IdSec" name="IdSec" style="width: 300px;">
                        <option value='...' style='color:red'>...</option>
                        </select>
                    </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr style="height: 30px;"><td></td></tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>                
        <tr style="height: 30px;"><td></td></tr> 
        <?php
	           if($_SESSION['tipo'] == "Asistencia")
                    $ABM = "Revisar Asistencias";
               
               else
                    $ABM = "Revisar Evaluaciones";
        ?>
        <tr><td colspan="2" style="text-align: center;"><input type="submit" id="Enviar" name="Enviar" onclick="return Validar();" class="boton" value="<?php echo $ABM?>" style="width: 150px;" /></td></tr>
        </form>            
</table>

</div>