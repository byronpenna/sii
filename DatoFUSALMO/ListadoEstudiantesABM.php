<?php

/**
 * @author Manuel
 * @copyright 2013
 */

require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Agregar")
    {
        $insert = $bdd->prepare("Insert into Educacion_Estudiantes values (null, :idI, :idU)");
        $insert->bindParam(':idI', $_SESSION["IdInscript"]);
        $insert->bindParam(':idU', $_POST['IdU']);
        $insert->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educaci�n", "Nuevo Estudiante en Inscripci�n: " . $_SESSION["IdInscript"] . ", Usuario: " . $_POST['IdU'], $bdd);
        Redireccion("../FUSALMO.php?d=StudentsList&n=1"); 
    }
    elseif($_POST['Enviar'] == "X")
    {
        $eliminar = $bdd->prepare("Delete from Educacion_Estudiantes where IdInscripcion = :idI and IdUsuario = :idU");
        $eliminar->bindParam(':idI', $_SESSION["IdInscript"]);
        $eliminar->bindParam(':idU', $_POST['IdU']);
        $eliminar->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educaci�n", "Eliminaci�n de Estudiante en Inscripci�n: " . $_SESSION["IdInscript"] . ", Usuario: " . $_POST['IdU'], $bdd);
        Redireccion("../FUSALMO.php?d=Students&n=1");
    }
    elseif($_POST['Enviar'] == "Deserci�n")
    {
        $insert = $bdd->prepare("Insert into Educacion_Desercion values (null, :idI, :idU)");
        $insert->bindParam(':idI', $_POST['IdU']);
        $insert->bindParam(':idU', $_SESSION["IdPeriodo"]);
        $insert->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educaci�n", "Deserci�n de Estudiante en Periodo: " . $_SESSION["IdPeriodo"] . ", Usuario: " . $_POST['IdU'], $bdd);        
        Redireccion("../FUSALMO.php?d=Students&n=2");
    }
    elseif($_POST['Enviar'] == "Cancelar Deserci�n")
    {
        $eliminar = $bdd->prepare("Delete from Educacion_Desercion where IdPeriodo = :idI and IdEstudiante = :idU");
        $eliminar->bindParam(':idI', $_SESSION["IdPeriodo"]);
        $eliminar->bindParam(':idU', $_POST['IdU']);
        $eliminar->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educaci�n", "Eliminaci�n de Deserci�n de Estudiante en Periodo: " . $_SESSION["IdPeriodo"] . ", Usuario: " . $_POST['IdU'], $bdd);
        Redireccion("../FUSALMO.php?d=Students&n=3");
    }  
    
    elseif($_POST['Enviar'] == "Crear Estudiante")
    {
        $UserName = $_POST['nombre1'] . "_" . $_POST['apellido1'] . "_" . date("y");
        $inserta = $bdd->prepare("INSERT INTO usuario VALUES (NULL , :UserName,  'Joven', '123456', '".$_SESSION["Localidad"]."')");            
        $inserta->bindParam(':UserName',  $UserName);             
        $inserta->execute();
        
        $Id = $bdd->lastInsertId();
        
        $selec = $bdd->prepare("SELECT * FROM  joven");
        $selec->execute();
        $ceros= str_repeat('0', 4- strlen($selec->rowCount())) . $selec->rowCount();    
        $codigo= substr($_POST['apellido1'], 0,1) . substr($_POST['apellido2'], 0,1) . date("y") . $ceros;            
        $comand=$bdd->prepare("INSERT INTO joven VALUES('$codigo','". $_POST['nombre1'] ." " . $_POST['nombre2'] ."' ,'".$_POST['apellido1']."','".$_POST['apellido2']."','".$_POST['sexo']."','','','','','' ,'' ,'' ,'' ,'' ,'' ,'' ,'','$Id')");
        $comand->execute();
        
        $insert = $bdd->prepare("Insert into Educacion_Estudiantes values (null, :idI, :idU)");
        $insert->bindParam(':idI', $_SESSION["IdInscript"]);
        $insert->bindParam(':idU', $Id);
        $insert->execute();       
            
        LogReg($_SESSION["IdUsuario"], "Educaci�n", " Nuevo Estudiante Creado: " . $_SESSION["IdInscript"] . ", Usuario: " . $Id, $bdd);
        Redireccion("../FUSALMO.php?d=Students&n=3");
    }        
}
?>