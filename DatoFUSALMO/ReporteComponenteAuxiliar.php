<?php

    $nombre = $_POST['name'];
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=" . $nombre .".xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';
    
    if($_SESSION['IdDato'] != "")
    {        
        if(isset($_GET['i']))
            $IdC = $_GET['i'];
            
        else
            Redireccion("FUSALMO.php");
            
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre, c.IdComponente, c.NombreComponente
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               Inner join Educacion_Componente as c on c.IdPeriodo = p.IdPeriodo                             
                               WHERE d.IdDatoFUSALMO = :id and c.IdComponente = :idc");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idc', $IdC);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");
            
            
function win2ascii($str)    
{    

    $str = StrTr($str,
        "\xE1\xE8\xEF\xEC\xE9\xED\xF2",
        "\x61\x63\x64\x65\x65\x69\x6E");
        
    $str = StrTr($str,
        "\xF3\xF8\x9A\x9D\xF9\xFA\xFD\x9E\xF4\xBC\xBE",
        "\x6F\x72\x73\x74\x75\x75\x79\x7A\x6F\x4C\x6C");
        
    $str = StrTr($str,
        "\xC1\xC8\xCF\xCC\xC9\xCD\xC2\xD3\xD8",
        "\x41\x43\x44\x45\x45\x49\x4E\x4F\x52");
        
    $str = StrTr($str,
        "\x8A\x8D\xDA\xDD\x8E\xD2\xD9\xEF\xCF",
        "\x53\x54\x55\x59\x5A\x4E\x55\x64\x44");

    return $str;
}
?>
<table >             
        <tr><td colspan="2" style="color: blue;">Reportes de Componente<br /><?php echo "$DatoView[1]<br />$DatoView[3] <br /> $DatoView[5]";?> </td></tr>
        <tr><td colspan="10" style="height: 5px; background-color: skyblue;"></td></tr>
        <tr><td colspan="2" style="height: 20px; background-color: white;"></td></tr> 
</table>
<?php

/**
 * @author Manuel
 * @copyright 2013
 */

        $idSec = $_GET['s'];
        $idiclo = $_SESSION["Ciclos"];
        
        $BuscarEvaluaciones = $bdd->prepare("Select * from Educacion_Evaluaciones 
                                             where IdComponente = :IdC and IdPeriodo = :IdP");
        $BuscarEvaluaciones->bindParam(':IdC', $IdC);
        $BuscarEvaluaciones->bindParam(':IdP', $_SESSION['IdPeriodo']);
        $BuscarEvaluaciones->execute();
        
        echo "<table>
                <tr><td colspan='4' class='centrar' > Tabla de Evaluaciones</td></tr>
                <tr style='height: 3px;' ><td colspan='4' style='background-color: skyblue;'></td></tr>
                <tr><td class='centrar' style='width: 20px'>N�mero</td><td class='centrar'>Id</td><td class='centrar'>Evaluaci�n</td><td class='centrar'>Ponderaci�n</td></tr>
                <tr style='height: 3px;'><td colspan='4' style='background-color: skyblue;'></td></tr>";
        
        $i = 0;
        $ponderacion = 0;
        $EvaArray = array();
        
        while($Evaluaciones = $BuscarEvaluaciones->fetch())
        {
            $i++;
            echo "<tr>
                        <td class='centrar;' style='color: green;'>$i</td>
                        <td class='centrar'>$Evaluaciones[0]</td>
                        <td class='centrar'>$Evaluaciones[3]</td>
                        <td class='centrar'>$Evaluaciones[4]%</td>
                  </tr>";   
                  
            $ponderacion =  $ponderacion + $Evaluaciones[4];
             array_push($EvaArray, $Evaluaciones[0]);
        }        
        
        if($ponderacion < 100)
            $color = "red";

        else
            $color = "blue";

        echo "<tr><td colspan='3'></td><td class='centrar' style='color: $color'>$ponderacion%</td></tr>
              <tr style='height: 25px'><td></td></tr>
              <tr><td colspan='10' style='height: 5px; background-color: skyblue;'></td></tr>
              <tr style='height: 25px'><td></td></tr>";
              
        
          

        $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2 
                                       FROM Educacion_Estudiantes  as est
                                       inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                       INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario                                                          
                                       where ins.IdCiclo = $idiclo and ins.IdSeccion = $idSec
                                       ORDER BY  jov.Apellido1 ASC");
        $listaAlumnos->execute();
        
        echo "<table>
              <tr>
                    <td style='width: 20px' rowspan='2'>Id</td>
                    <td style='text-align: center;' rowspan='2'>Estudiante</td>
                    <td style='text-align: center;'  colspan='". count($EvaArray) ."'> Evaluaciones</td>
                    <td style='text-align: center;' rowspan='2'>Promedio</td>
              </tr>";
        
        echo "<tr>";
        
        $i=0;
        foreach($EvaArray as $val)
        {
            $i++;
            echo "<td style='text-align: center; color: green; width: 200px;'>$i</td>";
        }
        echo "</tr>";
                      
        if($listaAlumnos->rowCount() > 0)
        {                        
            $i = 0;    
            $Aprobados = 0;
            $Reprobados = 0;                    
            while($alumno = $listaAlumnos->fetch())
            {
                $i++;
                $nombre = $alumno[2] . ' ' . $alumno[3] . ' ' . $alumno[1];
                echo "<tr style='padding-top: 50px' >
                          <td style='text-align: center; width: 50px'>$i</td>
                          <td style='text-align: left;'>" . strtoupper(win2ascii($nombre)). "</td>";

                $Promedio = 0;
                
                foreach($EvaArray as $val)
                {
                    $notas = $bdd->prepare("Select Nota.*, eva.Ponderacion from Educacion_Notas  as Nota
                                            INNER JOIN Educacion_Evaluaciones AS eva ON eva.IdEvaluacion = Nota.IdEvaluacion 
                                            where Nota.IdEvaluacion = $val and Nota.IdUsuario = $alumno[0]");
                                            
                    $notas->execute();   
                    $n = $notas->fetch(); 
                    
                    echo "<td style='text-align: center;'>$n[4]</td>";
                    
                    $Promedio = $Promedio + (($n[5] / 100) * $n[4]);
                    
                }
                
                if($Promedio < 6)
                {
                    $color = "red";
                    $Aprobados++;
                }
                else
                {
                    $color = "blue";
                    $Reprobados++;
                }
                echo "<td style='text-align: center;color: $color'>$Promedio</td>
                      </tr>";                        
            }
        }
        echo "      <tr style='height: 50px'><td></td></tr> 
                    <tr><td colspan='3' style='text-align: center;'>Detalles</td></tr>                       
                    <tr><td style='text-align: center;' colspan='2'>Estudiantes Aprobados:</td><td style='text-align: center;'>$Aprobados</td></tr>
                    <tr><td style='text-align: center;' colspan='2'>Estudiantes Reprobados:</td><td style='text-align: center;'>$Reprobados</td></tr>
                    <tr><td style='text-align: center;' colspan='2'>Total de Estudiantes:</td><td style='text-align: center;'>".$listaAlumnos->rowCount()."</td></tr>                    
              </table>";
?>

