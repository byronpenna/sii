<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=ReporteNotas.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';
    
    if(!isset($_POST['Enviar']))
            Redireccion("../FUSALMO.phpd=Adm");

    $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                             FROM  Institucion as inst 
                                             inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                             where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
        
    $DatosInstituciones->bindParam(':idIns', $_POST['IdIns']);
    $DatosInstituciones->bindParam(':idSec', $_POST['IdSec']);   
    $DatosInstituciones->execute();
    $InfoSeccion = $DatosInstituciones->fetch();  
        
      
    $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2, ins.IdInscripcion
                                   FROM Educacion_Estudiantes  as est
                                   inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                   INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                   where ins.IdCiclo = :idC and ins.IdInstitucion = :idI and ins.IdSeccion = :idS
                                   ORDER BY  jov.Apellido1 ASC, jov.Apellido2 ASC  ");
              
    $listaAlumnos->bindParam(':idC', $_SESSION['Ciclos']);
    $listaAlumnos->bindParam(':idI', $_POST['IdIns']);
    $listaAlumnos->bindParam(':idS', $_POST['IdSec']);               
    $listaAlumnos->execute();

    $asis = isset($_POST['Componente']) ? $_POST['Componente'] : array();

    $PromedioNotas = array();
    $rows = count($asis) * 2  + 3;
    

        echo "<table style='width: 100%; background: white; padding: 5px; border-radius:10px' >
              <tr><td style='color: blue;' colspan='$rows'>Reporte Personalizado <hr color='skyblue' /></td></tr>
              <tr><td style='width: 15%;'>Instituci�n: </td><td colspan='$rows'>$InfoSeccion[1]</td></tr>
              <tr><td style='width: 15%;'>Secci�n:     </td><td colspan='$rows'>$InfoSeccion[3]</td></tr>
              <tr><td colspan='$rows'><hr color='skyblue' /></td></tr>
              <tr><td colspan='$rows' style='text-align: center; width: 90%; margin-left: 5%;'>
              
              <table rules='all' style='width: 90%; margin-left: 5%'>
              <tr>
                  <th style='width: 5%'>N�</th>
                  <th style='width: 40%' style='text-align: center; color: blue;'> Alumnos </th>";
              
              for ($index = 0 ; $index < count($asis); $index ++)
              {
                  $Componente = $bdd->prepare("SELECT * FROM  Educacion_Componente where IdComponente = $asis[$index]");
                  $Componente->execute();
                  $NombreComponente = $Componente->fetch();   
                     
                  echo "<td style='text-align: center; color: blue;' colspan='2'>$NombreComponente[2]</td>";   
              }
              echo "<th style='width: 100px'>Promedio de <br />Asistencia</th>";              
              echo "</tr>";
              
              
        $i = 1;
        $TDesercion = 0;
        
        $aux = 0;
        while($Alumno = $listaAlumnos->fetch())
        {
            if($aux >= count($asis))
                $aux = 0;
            
            echo "<tr>
                      <th>$i</th><td > $Alumno[2] $Alumno[3] $Alumno[1]</td>";
            
            $PromedioAsistencia = 0;
                                    
            for ($index = 0 ; $index < count($asis); $index ++)
            {
               $Desercion = $bdd->prepare("Select * from Educacion_Desercion where IdPeriodo = :idP and IdEstudiante = :idU");
               $Desercion->bindParam(':idP', $_SESSION['IdPeriodo']);
               $Desercion->bindParam(':idU', $Alumno[0]);
               $Desercion->execute();
               
               if($Desercion->rowCount() == 0)
               {
                        
                   $Evaluaciones = $bdd->prepare("SELECT eva.Ponderacion, nota.Nota FROM Educacion_Evaluaciones as eva
                                                  inner join Educacion_Notas as nota on eva.IdEvaluacion = nota.IdEvaluacion
                                                  where eva.IdPeriodo = :idP and IdSeccion = :idS and eva.IdComponente = :idC and nota.IdUsuario = :idU
                                                  and eva.IdEvaluacion not in(SELECT IdEvaluacion FROM Educacion_EvaluacionesAsistencia)");
                   
                   $Evaluaciones->bindParam(':idP', $_SESSION['IdPeriodo']);
                   $Evaluaciones->bindParam(':idS', $_POST['IdSec']);
                   $Evaluaciones->bindParam(':idC', $asis[$index]); 
                   $Evaluaciones->bindParam(':idU', $Alumno[0]);                
                   $Evaluaciones->execute();
                   
                   $Nota = 0;
                   
                   while($datanotas = $Evaluaciones->fetch())
                   {
                        $Nota = $Nota + ($datanotas[0] * $datanotas[1] / 100);
                   }
                   

                   
                   
                   $NotaAsistencia = $bdd->prepare("SELECT * FROM Educacion_Evaluaciones AS e 
                                                     INNER JOIN Educacion_EvaluacionesAsistencia AS ea ON e.IdEvaluacion = ea.IdEvaluacion
                                                     WHERE IdPeriodo = :idP AND IdSeccion = :idS AND IdComponente = :idC");
                   $NotaAsistencia->bindParam(':idP', $_SESSION['IdPeriodo']);
                   $NotaAsistencia->bindParam(':idS', $_POST['IdSec']);
                   $NotaAsistencia->bindParam(':idC', $asis[$index]); 
                   $NotaAsistencia->execute();
                   
                   if($NotaAsistencia->rowCount() > 0)
                   {
                        
                        $Ponderacion = $NotaAsistencia->fetch();
                        
                        $VerAsistencias = $bdd->prepare("Select * from Educacion_Asistencias 
                                                         where IdComponente = :IdC and IdPeriodo = :IdP and IdSeccion = :IdS and IdEstudiante = :IdU");
                        $VerAsistencias->bindParam(':IdC', $asis[$index]);
                        $VerAsistencias->bindParam(':IdP', $_SESSION['IdPeriodo']);              
                        $VerAsistencias->bindParam(':IdS', $_POST['IdSec']);
                        $VerAsistencias->bindParam(':IdU', $Alumno[0]);  
                        $VerAsistencias->execute();
                        
                        $TotalAsistenciaEstudiante = $VerAsistencias->rowCount();         
                        
                        
                        $VerificacionEvaluacion= $bdd->prepare("Select * from Educacion_Asistencias
                                                                where IdComponente = :IdC and IdPeriodo = :IdP and IdSeccion = :IdS
                                                                GROUP BY Fecha");
                                                      
                        $VerificacionEvaluacion->bindParam(':IdC', $asis[$index]);
                        $VerificacionEvaluacion->bindParam(':IdP', $_SESSION['IdPeriodo']);              
                        $VerificacionEvaluacion->bindParam(':IdS', $_POST['IdSec']);                                          
                        $VerificacionEvaluacion->execute();     
                        
                        
                        
                        if($VerificacionEvaluacion->rowCount() > 0)          
                            $NotaAsistencia2 = $TotalAsistenciaEstudiante / $VerificacionEvaluacion->rowCount() * 10 ;
    
                        else
                            $NotaAsistencia2 = 0;
                            
    
                        $Nota = $Nota + ($Ponderacion[4] * $NotaAsistencia2 / 100);
                        
                   if($Nota < 6)
                        $Color = "Red";
                   
                   else
                        $Color = "Green";
                   
                   echo "<td style='text-align: center; width: 60px; color: $Color'>" . number_format($Nota ,2) . "</td>";                   
                   $PromedioNotas[$aux] = $PromedioNotas[$aux] + $Nota;
                   $aux++;                        
                        
                        
                        $PromedioAsistencia = $PromedioAsistencia +  $NotaAsistencia2;  
                        
                       if($NotaAsistencia2 < 6)
                            $Color = "Red";
                       
                       else
                            $Color = "Green";                   
                       
                       echo "<td style='text-align: center; width: 70px; color: $Color'>" . number_format($NotaAsistencia2 * 10 ,2) . "%</td>";
                       
                       $PromedioNotas[$aux] = $PromedioNotas[$aux] + $NotaAsistencia2;
                       $aux++;
                                      
                   }
                   else
                   {
                       echo "<td style='text-align: center; width: 70px; color: black'> - </td>";
                   }

                   
               }
               else
               {
                   echo "<td style='text-align: center; width: 70px;'>-</td>";
                   echo "<td style='text-align: center; width: 70px;'>-</td>";                   
               }                                               
            }
            
            
            if(count($asis) > 0)
                $Promedio = $PromedioAsistencia / count($asis) * 10;
            
            else
                $Promedio = 0;
            
            
            if($Desercion->rowCount() > 0)            
                echo "<td style='text-align: center;'></td>";
            
            else            
                echo "<td style='text-align: center;'> " . number_format($Promedio ,2) . "%</td>";
                
            
                                        
            
            $i++;
            
            if($Desercion->rowCount() > 0)
                $TDesercion++;                                    
        }
        
        $i--;
        
        $colspan = count($PromedioNotas)*2 + 3;
        echo "<tr>
                  <th  colspan='2'>Promedios: </th>";
                  
                  $aux = 1;
                  foreach($PromedioNotas as $value)   
                  {               
                        if(($aux % 2) == 0) 
                            echo "<th style='text-align: center;'>" . number_format($value / ($i - $TDesercion)* 10 ,2) . "%</th>";
                        
                        else                    
                            echo "<th style='text-align: center;'>" . number_format($value / ($i - $TDesercion),2) . "</th>";
                        
                        $aux++;                                                                           
                  }
                  
            echo "<td></td>
                </tr>
                </table>    
            </td>
        </tr>
        </table>";

?>