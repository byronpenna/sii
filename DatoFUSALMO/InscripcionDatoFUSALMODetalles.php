<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 
?>
<script>
    $(document).ready(function(){
        
       $("#Ciclos").change(function(){Cargar_Periodos(this.form);});
       $("#Institucion").change(function(){Cargar_Grupos(this.form);});      
              
       $("#Periodos").attr("disabled",true);
       $("#Institucion").attr("disabled",true);
       $("#Grupo").attr("disabled",true);  
          
    });
    
    function Cargar_Periodos(form)
    {        
        var code = $("#Ciclos").val(); 
        if(code != "..."){            
            
        	$.get("DatoFUSALMO/SelectPeriodos.php", { code: code },
        		function(resultado)
        		{
        			if(resultado == false)
        			{
        				alert("Error");
        			}
        			else
        			{        			    
        				$("#Periodos").attr("disabled",false);
        				document.getElementById("Periodos").options.length = 0;
        				$('#Periodos').append(resultado);	
        			}
        		}        
        	);
            
           	$.get("DatoFUSALMO/SelectInstituciones.php", { code: code },
        		function(resultado)
        		{
        			if(resultado == false)
        			{
        				alert("Error");
        			}
        			else
        			{        			    
        				$("#Institucion").attr("disabled",false);
        				document.getElementById("Institucion").options.length = 0;
        				$('#Institucion').append(resultado);	
        			}
        		}        
        	);
        }      
    }

    function Cargar_Grupos(form)
    {
        var code = $("#Institucion").val(); 
        var code2 = $("#Ciclos").val(); 
        if(code != "..."){
            
        	$.get("DatoFUSALMO/SelectSecciones.php", { code: code, code2: code2 },
        		function(resultado)
        		{
        			if(resultado == false)
        			{
        				alert("Error");
        			}
        			else
        			{        			    
        				$("#Grupo").attr("disabled",false);
        				document.getElementById("Grupo").options.length = 0;
        				$('#Grupo').append(resultado);	
        			}
        		}        
        	);            
        }      
    }
    

</script>
    <form method="post" action="DatoFUSALMO/InscripcionABM.php" >
     <h1>Inscripci�n de Dato FUSALMO</h1>
     <table style='width: 80%; background-color: white; border-radius: 10px; margin-left: 10%; border-radius: 10px; border-color: skyblue; border-style: groove; padding: 10px; margin-top: 20px;'>
        
        <?php
            
           $id = $_POST['IdDatoF'];
           echo "<input type='hidden' value='$id' name='dato' /> ";
           
            
	       $DatosFusalmo = $bdd->prepare("SELECT * FROM  DatoFusalmo where IdDatoFUSALMO = $id" );
           $DatosFusalmo->execute();
           
           if($DatosFusalmo->rowCount() > 0)
           {
                $dato = $DatosFusalmo->fetch();                
           }
           else
                Redireccion("FUSALMO.php");                        
         ?>            
         <tr><td style="text-align: right; padding-right: 20px;">Dato FUSALMO:</td><td><?php echo $dato[1];?></td></tr>
         <tr>
             <td style="text-align: right; padding-right: 20px;">Seleccione el Ciclo:</td>
             <td>
                <select id="Ciclos" name="Ciclos" style="width: 250px;">
                    <option value='...'>...</option>
                    <?php
	                       	$DatosCiclos = $bdd->prepare("SELECT * FROM  Educacion_Ciclos where IdDatoFusalmo = $id" );
                            $DatosCiclos->execute();
                            
                            while($Ciclos = $DatosCiclos->fetch())                            
                                echo "<option value='$Ciclos[0]'>$Ciclos[2]</option>";                             
                    ?>
                </select>             
             </td>
         </tr>
         <tr>
            <td style="text-align: right; padding-right: 20px;">Seleccione el Periodo</td>
            <td>
                <select id="Periodos" name="Periodos" style="width: 250px;">
                    <option value="...">...</option>
                </select>             
            </td>
         </tr>
         <tr>
            <td style="text-align: right; padding-right: 20px;">Seleccione la Instituci�n</td>
            <td>
                <select id="Institucion" name="Institucion" style="width: 250px;">
                    <option value="...">...</option>
                </select>             
            </td>
         </tr>
         <tr>
            <td style="text-align: right; padding-right: 20px;">Seleccione el Grupo</td>
            <td>
                <select id="Grupo" name="Grupo" style="width: 250px;">
                    <option value="...">...</option>
                </select>             
            </td>
         </tr>         
         <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" value="Registrar" name="enviar" class="boton" /></td>
         </tr>         
     </table>
     </form>