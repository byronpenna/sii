<?php
	   require '../net.php';
?>
<script >
function cerrar() { setTimeout(window.close,1500); }
</script>
<body onload="window.print();cerrar();" style="font-family: Calibri;  font-size:x-small;">
<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 


if(!isset($_POST['Enviar']))
    Redireccion("../FUSALMO.phpd=Adm");

if(!isset($_POST['Enviar']))
    Redireccion("../FUSALMO.phpd=Adm");

        $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                             FROM  Institucion as inst 
                                             inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                             where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
        
        $DatosInstituciones->bindParam(':idIns', $_POST['IdIns']);
        $DatosInstituciones->bindParam(':idSec', $_POST['IdSec']);   
        $DatosInstituciones->execute();
        $InfoSeccion = $DatosInstituciones->fetch();  
        
        $DatosComponente = $bdd->prepare("SELECT * FROM  Educacion_Componente where IdComponente = :IdComp");
        
        $DatosComponente->bindParam(':IdComp', $_POST['IdComp']);
        $DatosComponente->execute();
        $InfoComp = $DatosComponente->fetch();    
        
              
        $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2, ins.IdInscripcion
                                       FROM Educacion_Estudiantes  as est
                                       inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                       INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                       where ins.IdCiclo = :idC and ins.IdInstitucion = :idI and ins.IdSeccion = :idS
                                       ORDER BY  jov.Apellido1 ASC, jov.Apellido2 ASC  ");
                  
        $listaAlumnos->bindParam(':idC', $_SESSION['Ciclos']);
        $listaAlumnos->bindParam(':idI', $_POST['IdIns']);
        $listaAlumnos->bindParam(':idS', $_POST['IdSec']);               
        $listaAlumnos->execute();


        $Evaluaciones = $bdd->prepare("SELECT * FROM Educacion_Evaluaciones where IdComponente = :IdC and IdPeriodo = :IdP and IdSeccion = :IdS");        
        $Evaluaciones->bindParam(':IdC', $_POST['IdComp']);
        $Evaluaciones->bindParam(':IdP', $_SESSION['IdPeriodo']);              
        $Evaluaciones->bindParam(':IdS', $_POST['IdSec']);
        $Evaluaciones->execute();
        
        $VerificacionEvaluacion= $bdd->prepare("Select * from Educacion_Asistencias
                                                where IdComponente = :IdC and IdPeriodo = :IdP and IdSeccion = :IdS
                                                GROUP BY Fecha");
                                                  
        $VerificacionEvaluacion->bindParam(':IdC', $_POST['IdComp']);
        $VerificacionEvaluacion->bindParam(':IdP', $_SESSION['IdPeriodo']);              
        $VerificacionEvaluacion->bindParam(':IdS', $_POST['IdSec']);                                          
        $VerificacionEvaluacion->execute();        
        
        
        $DatosAsistencia = $bdd->prepare("SELECT * FROM Educacion_EvaluacionesAsistencia as eva   
                                          WHERE IdEvaluacion IN (
                                                                      Select IdEvaluacion from Educacion_Evaluaciones 
                                                                      where IdComponente = :idC and IdPeriodo = :idP and IdSeccion = :idSec
                                                                    )");
        $DatosAsistencia->bindParam(':idC', $_POST['IdComp'] ) ;
        $DatosAsistencia->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
        $DatosAsistencia->bindParam(':idSec', $_POST['IdSec'] ) ;
        $DatosAsistencia->execute();     
           
        $EvaluacionAsistencia = $DatosAsistencia->fetch();   
                
        $rows = 3 + $Evaluaciones->rowCount();

        echo "<table style='width: 100%; background: white; padding: 5px; border-radius:10px; font-size: small;  page-break-after:always;'>
              <tr><td style='color: blue;' colspan='$rows'>Reporte de Componente <hr color='skyblue' /></td></tr>
              <tr><td style='width: 15%;'>Instituci�n: </td><td colspan='$rows'>$InfoSeccion[1]</td></tr>
              <tr><td style='width: 15%;'>Secci�n:     </td><td colspan='$rows'>$InfoSeccion[3]</td></tr>
              <tr><td style='width: 15%;'>Componente:     </td><td colspan='$rows'>$InfoComp[2]</td></tr>                            
              <tr><td colspan='$rows'><hr color='skyblue' /></td></tr>
              <tr><td colspan='2' style='width: 35%' style='text-align: center; color: blue;'>Alumnos </td>";
              
              $array = array();
              while($NombreEvaluaciones = $Evaluaciones->fetch())      
              {
                echo "<td style='text-align: center; color: blue; width:15%'>$NombreEvaluaciones[3] <br /> $NombreEvaluaciones[4]%</td>";
                
                array_push($array, $NombreEvaluaciones[0]);
              }          
                   
              
              echo "<td style='text-align: center; color: blue;'><strong>Promedio</strong></td>
                    </tr>";
                          
        $i = 0;
        
        $PromediosAsistencia = 0;
        $PromediosNota = 0;
        $tDesercion = 0;
        
        while($Alumno = $listaAlumnos->fetch())
        {   
            $i++;
            $Nota = 0;
            
            echo "<tr><td colspan='2' >$i - $Alumno[2] $Alumno[3] $Alumno[1]</td>";
            
            $Desercion = $bdd->prepare("Select * from Educacion_Desercion where IdPeriodo = :idP and IdEstudiante = :idU");
            $Desercion->bindParam(':idP', $_SESSION['IdPeriodo']);
            $Desercion->bindParam(':idU', $Alumno[0]);
            $Desercion->execute();
            
            if($Desercion->rowCount() > 0)
                $tDesercion++;
            
                        
            foreach ($array as $k => $v) 
            {
               if($Desercion->rowCount() == 0)
               {
                   if($EvaluacionAsistencia[1] != $v)
                   {          
                    
                       $Evaluaciones = $bdd->prepare("SELECT eva.Ponderacion, nota.Nota FROM Educacion_Evaluaciones as eva
                                                      inner join Educacion_Notas as nota on eva.IdEvaluacion = nota.IdEvaluacion
                                                      where eva.IdEvaluacion = :IdE and nota.IdUsuario = :idU");
                       
                       $Evaluaciones->bindParam(':IdE', $v);
                       $Evaluaciones->bindParam(':idU', $Alumno[0]);                
                       $Evaluaciones->execute();
        
                       $datanotas = $Evaluaciones->fetch();
                       
                       $Nota = $Nota + ($datanotas[0] * $datanotas[1] / 100);
                       
                       if($datanotas[1] < 6)
                            $color = "red"; 
        
                       else
                            $color = "green";
                       
                       echo "<td style='text-align: center; color: $color;'>". number_format($datanotas[1], 2) ."</td>";
                       
                       
                   }
                   else
                   {   
                        $VerAsistencias = $bdd->prepare("Select * from Educacion_Asistencias 
                                                         where IdComponente = :IdC and IdPeriodo = :IdP and IdSeccion = :IdS and IdEstudiante = :IdU");
                        $VerAsistencias->bindParam(':IdC', $_POST['IdComp']);
                        $VerAsistencias->bindParam(':IdP', $_SESSION['IdPeriodo']);              
                        $VerAsistencias->bindParam(':IdS', $_POST['IdSec']);
                        $VerAsistencias->bindParam(':IdU', $Alumno[0]);  
                        $VerAsistencias->execute();
                          
                        $TotalAsistenciaEstudiante = $VerAsistencias->rowCount();                                                           
                        $NotaAsistencia = $TotalAsistenciaEstudiante / $VerificacionEvaluacion->rowCount() * 10 ;
                       
                        if($NotaAsistencia < 6)
                            $color = "red"; 
        
                        else
                            $color = "green";
                            
                            
                        $Evaluaciones = $bdd->prepare("SELECT * FROM Educacion_Evaluaciones as eva where eva.IdEvaluacion = :IdE ");
                       
                       $Evaluaciones->bindParam(':IdE', $EvaluacionAsistencia[1]);      
                       $Evaluaciones->execute();
        
                       $datanotas = $Evaluaciones->fetch();
                       
                       $Nota = $Nota + ($datanotas[4] * $NotaAsistencia / 100);
                                                
                       echo "<td style='text-align: center; color: $color;'>". number_format($NotaAsistencia, 2) . "</td>"; 
                       
                       $PromediosAsistencia = $PromediosAsistencia + $NotaAsistencia;
                       
                   }
               }
               else
               {
                    echo "<td style='text-align: center; color: red;'>-</td>";
                    
               }
                    
            }
                  
            if($Nota < 7)
                    $color = "red"; 
            else
                    $color = "green";
                    
            if($Desercion->rowCount() == 0)
            {
                echo "<td style='text-align: center; color: blue;'>". number_format($Nota, 2) ."</td>";
                $PromediosNota = $PromediosNota + $Nota;
            }
                
        }       
        
        $PromediosAsistencia = $PromediosAsistencia / ($i - $tDesercion )*10;
        $PromediosNota = $PromediosNota / ($i - $tDesercion ) ;
        
        $Colspan =  count($array) + 3;
        
        echo "</table>
              <table style='width: 100%; background: white; padding: 5px; border-radius:10px;'>
              <tr><td style='color: blue;' colspan='$rows'>Reporte de Componente <hr color='skyblue' /></td></tr>
              <tr><td style='width: 15%;'>Instituci�n: </td><td colspan='$rows'>$InfoSeccion[1]</td></tr>
              <tr><td style='width: 15%;'>Secci�n:     </td><td colspan='$rows'>$InfoSeccion[3]</td></tr>
              <tr><td style='width: 15%;'>Componente:     </td><td colspan='$rows'>$InfoComp[2]</td></tr>                            
              <tr><td colspan='$rows'><hr color='skyblue' /></td></tr>              
              
              <tr>
                  <td colspan='$Colspan'>
                  <br />
                  <table style='width: 60%; margin-left: 20%'>
                    <tr><td colspan='2'><h3>Detalles de Notas</h3></td>
                    <tr><td class='tdleft' style='width: 50%'>Total Estudiantes:</td><td>$i</td><tr>
                    <tr><td class='tdleft'>Total Desecci�n:</td><td>$tDesercion</td><tr>
                    <tr><td class='tdleft'>Promedio de Asistencia:</td><td>". number_format($PromediosAsistencia, 2) ."%</td><tr>
                    <tr><td class='tdleft'>Promedio de Notas:</td><td>". number_format($PromediosNota, 2) ."</td><tr>
                  </table>
                  </td>
              </tr>"; 
        echo "</table>";
?>
</body>