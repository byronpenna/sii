<?php

require '../net.php';

if(isset($_POST['Enviar']))
{    
    if($_POST['Enviar'] == "Agregar"){
        
        $busca = $bdd->prepare("select * from InscripcionDatoFusalmo where IdUsuario = :idusu and IdDatoFUSALMO = :iddato");
        $busca->bindParam(':idusu', $_POST['Id']);
        $busca->bindParam(':iddato', $_SESSION['IdDato']);
        $busca->execute();

        if($busca->rowCount() == 0)
        {
            $insert = $bdd->prepare("insert into InscripcionDatoFusalmo values (NULL, :idusu, :iddato)");
            $insert->bindParam(':idusu', $_POST['Id']);
            $insert->bindParam(':iddato', $_SESSION['IdDato']);
            $insert->execute();

            Redireccion("../FUSALMO.php?d=PrList&n=1");
        }
        else
            Redireccion("../FUSALMO.php?d=PrList&n=3");
        
    }
    
    
    
    
    elseif ($_POST['Enviar'] == "Quitar")
    {
        $delete = $bdd->prepare("Delete from InscripcionDatoFusalmo where IdInscripcion = :id");
        $delete->bindParam(':id', $_POST['Id']);
        $delete->execute();
        
        Redireccion("../FUSALMO.php?d=PrList&n=2");
    }
}
?>