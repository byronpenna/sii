<?php

/**
 * @author Manuel
 * @copyright 2013
 */

require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Agregar")
    {        
        $Evaluacion = $bdd->prepare("Insert into Educacion_Evaluaciones values(null, :idC, :idP, :Nombre, :Porcentaje, :IdSec)");
        $Evaluacion->bindParam(':idC', $_SESSION['Component'] );
        $Evaluacion->bindParam(':idP', $_SESSION['IdPeriodo'] );
        $Evaluacion->bindParam(':Nombre', $_POST['nombre'] );
        $Evaluacion->bindParam(':Porcentaje', $_POST['ponderacion']);
        $Evaluacion->bindParam(':IdSec', $_SESSION['IdSec']);
        $Evaluacion->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educación", "Crear la Evaluación; Nombre: " . $_POST['nombre'] . " , Ponderación: " . $_POST['ponderacion'], $bdd);
                
        Redireccion("../FUSALMO.php?d=Evaluation&n=1");
    }

    if($_POST['Enviar'] == "X")
    {
        $Evaluacion = $bdd->prepare("Delete from Educacion_Evaluaciones where IdEvaluacion = :idE");
        $Evaluacion->bindParam(':idE',$_POST['idE'] );
        $Evaluacion->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educación", "Eliminación de Evaluación; IdEvaluación: " . $_POST['idE'], $bdd);
        
        Redireccion("../FUSALMO.php?d=Evaluation&n=2");
    }
    
    if(isset($_POST['IdIns']) && isset($_POST['IdSec']))
    {
        
        $_SESSION['IdIns'] = $_POST['IdIns'];
        $_SESSION['IdSec'] = $_POST['IdSec'];
        
        if($_POST['Enviar'] == "Revisar Asistencias")
            Redireccion("../FUSALMO.php?d=AttendanceList");
            
        else
            Redireccion("../FUSALMO.php?d=Evaluation");
    }
    
    if($_POST['Enviar'] == "Notas")
    {
        $_SESSION['IdEva'] = $_POST['idE'];
        Redireccion("../FUSALMO.php?d=qualification");
    }
    
    if($_POST['Enviar'] == "Q")
    {
        $Evaluacion = $bdd->prepare("Delete from Educacion_EvaluacionesAsistencia where IdEvaluacion = :idE");
        $Evaluacion->bindParam(':idE',$_POST['idE'] );
        $Evaluacion->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educación", "Evaluacion No Asistencia; IdEvaluación: " . $_POST['idE'], $bdd);
        
        Redireccion("../FUSALMO.php?d=Evaluation&n=2");
    } 
    
    if($_POST['Enviar'] == "A")
    {
        $Evaluacion = $bdd->prepare("Insert into Educacion_EvaluacionesAsistencia values (Null, :idE)");
        $Evaluacion->bindParam(':idE',$_POST['idE'] );
        $Evaluacion->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educación", "Evaluacion Asistencia; IdEvaluación: " . $_POST['idE'], $bdd);
        
        Redireccion("../FUSALMO.php?d=Evaluation&n=1");
    }        
    
    
    
        
}

?>