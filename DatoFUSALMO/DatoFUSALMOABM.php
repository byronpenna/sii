<?php

require '../net.php';

if(isset($_POST['enviar'])){
    
    if($_POST['enviar'] == "Insertar"){
        
        if($_POST['Categoria'] == "Proyecto")  {      
            $fechafin =  $_POST['FechaFin'];
            $fechainicio = $_POST['FechaInicio'];
        }    
        elseif($_POST['Categoria'] == "Programa"){
            $fechafin =  "0000-00-00";  
            $fechainicio = $_POST['a�o'] . "-01-01";
        }
        $Dato = $bdd->prepare("Insert into DatoFusalmo 
                               values(NULL, :nombre, :Descripcion, :Categoria, :Linea, :FechaInicio, :FechaFin, 'Activo')");
        
        $Dato->bindParam('nombre', $_POST['Nombre']);
        $Dato->bindParam('Descripcion', $_POST['Descripcion']);
        $Dato->bindParam('Categoria', $_POST['Categoria']);
        $Dato->bindParam('Linea', $_POST['Linea']);
        $Dato->bindParam('FechaInicio', $fechainicio);
        $Dato->bindParam('FechaFin', $fechafin);    
        $Dato->execute();
        
        Redireccion("../FUSALMO.php?n=1");
        
    }
        
    else if($_POST['enviar'] == "Modificar")
    {
        if($_POST['Categoria'] == "Proyecto")  {      
            $fechafin =  $_POST['FechaFin'];
            $fechainicio = $_POST['FechaInicio'];
        }    
        elseif($_POST['Categoria'] == "Programa"){
            $fechafin =  "0000-00-00";  
            $fechainicio = $_POST['a�o'] . "-01-01";
        }
        
        $Dato = $bdd->prepare("Update DatoFusalmo 
                               Set Nombre = :nombre,
                               Descripcion = :Descripcion,
                               Categoria = :Categoria, 
                               LineaTrabajo = :Linea, 
                               FechaInicio = :FechaInicio, 
                               FechaFin = :FechaFin
                               where IdDatoFUSALMO = " .$_SESSION['IdDato']);
        
        $Dato->bindParam('nombre', $_POST['Nombre']);
        $Dato->bindParam('Descripcion', $_POST['Descripcion']);
        $Dato->bindParam('Categoria', $_POST['Categoria']);
        $Dato->bindParam('Linea', $_POST['Linea']);
        $Dato->bindParam('FechaInicio', $fechainicio);
        $Dato->bindParam('FechaFin', $fechafin);
        $Dato->execute();
        
        Redireccion("../FUSALMO.php?d=dato&n=1");
    }
    
    else if($_POST['enviar'] == "Ejecutar")
    {
        $Inscripcion = $bdd->prepare("Insert into EjecucionPrograma VALUES (NULL , :id,  :anio)");
        $Inscripcion->bindParam(':id', $_SESSION['IdDato']);
        $Inscripcion->bindParam(':anio', $_POST['a�o']);
        $Inscripcion->execute();
        
        $_SESSION["IdA�oDato"] = $_POST['a�o'];
        
        Redireccion("../FUSALMO.php?d=Imp&n=1");        
    }

    else if($_POST['enviar'] == "Eliminar A�o")
    {
        $Inscripcion = $bdd->prepare("Delete from EjecucionPrograma where IdEjecucion = :id");
        $Inscripcion->bindParam(':id', $_POST['IdProA�o']);        
        $Inscripcion->execute();
        
        Redireccion("../FUSALMO.php?d=Imp&n=2");
    }    
    
    else if($_POST['enviar'] == "Ejecutar este A�o")
    {
        
        $_SESSION["IdA�oDato"] = $_POST['IdA�o'];
        Redireccion("../FUSALMO.php?d=Imp&n=3");
    }
}
elseif(isset($_GET['i']))
{
        $Dato = $bdd->prepare("Delete from DatoFusalmo where IdDatoFUSALMO = " . $_SESSION['IdDato']);
        $Dato->execute(); 
        Redireccion("../FUSALMO.php?n=2"); 
}
else
    Redireccion("../FUSALMO.php");

?>