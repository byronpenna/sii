<?php
     if($_SESSION['IdDato'] != "")
    {   
        
        $Dato = $bdd->prepare("Select * from  DatoFusalmo where IdDatoFusalmo = :id");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->execute();
        
        if($Dato->rowCount() == 1)
        $DatosFUSALMO = $Dato->fetch();
       
        else
            Redireccion("FUSALMO.php");
    }
    else
        Redireccion("FUSALMO.php");
?>   

<div class="Raiz">
<a href="/Main.php" class='flecha' style='text-decoration:none'>Principal</a> ->
<a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
<a href="/FUSALMO.php?d=PrList" class='flecha' style='text-decoration:none'>Personal</a>
</div>

<div style="width: 90%; background-color: white; border-radius: 20px; margin-left: 5%; ">
<h1 style="margin-left: 60px;margin-bottom: 10px;">Personal de Proyecto</h1> 
<hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 15px;" />
        <table style="margin-bottom: 20px; float:left; width: 100%;">
        <tr><th colspan="4"><h2>Asignar nuevo Personal a <br /><?php echo $DatosFUSALMO[1] ?></h2></th></tr>
        <tr>
            <form action="#" method="post">
                <td style="width: 50%; text-align: right;" colspan="2">Escoja el tipo de usuario que desea Asignar:</td>
                <td>
                    <select id="tipoUbusqueda" name="tipoUbusqueda">                   
                        <?php
                           $tipos = $bdd->prepare("SELECT TipoUsuario FROM usuario where TipoUsuario != 'Administrador' and TipoUsuario != 'joven'                                                    
                                                   GROUP BY TipoUsuario ORDER BY TipoUsuario DESC") ;
                       	   $tipos->execute();
                           
                           while($tiposdatos = $tipos->fetch())
                           {
                                if($tiposdatos[0] == $_POST['tipoUbusqueda'])
                                    echo "<option value='$tiposdatos[0]' selected='true'>$tiposdatos[0]</option>";
                                
                                else
                                    echo "<option value='$tiposdatos[0]'>$tiposdatos[0]</option>";                                    
                           }    
                        ?>
                    </select>                    
                </td> 
                <td><input type="submit" id="enviar" name="enviar" class="boton" value="Buscar" /></td>      
            </form>
        </tr>        
                <?php 
                    if(isset($_POST['tipoUbusqueda']))
                    {
                        $usuarios = $bdd->prepare(" Select * from usuario where TipoUsuario like '" . $_POST['tipoUbusqueda'] . "'");
                        $usuarios->execute();
                        
                        if($usuarios->rowCount() > 0)
                        {
                            while($datosUsuario = $usuarios->fetch())
                            {
                                echo "<tr>
                                          <td style='padding-top: 10px'>$datosUsuario[0]</td>
                                          <td style='padding-top: 10px'>$datosUsuario[1]</td>
                                          <td style='padding-top: 10px'>$datosUsuario[2]</td>
                                          <td style='padding-top: 10px'>
                                              <form action='DatoFUSALMO/PersonalABM.php' method='post'>
                                                   <input type='hidden' name='Id' id='Id' value='$datosUsuario[0]' />
                                                   <input type='submit' value='Agregar' class='boton' name='Enviar' id='Enviar'  />
                                              </form>
                                          </td>
                                      </tr>";
                            }
                        }                    
                        else
                        {
                                echo "<tr>
                                          <td style='padding-top: 10px; text-align: center;' colspan='4'><h2 style='color: red;'>Ya no hay personal para ingregar a este proyecto.</h2></td>
                                      </tr>";
                        }                    
                    }
                    
                   ?>     
        </table>
<div class="clr"></div> 
</div>