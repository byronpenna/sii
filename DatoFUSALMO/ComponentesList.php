<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administraci�n</a> ->
    <a href="/FUSALMO.php?d=Component" class='flecha' style='text-decoration:none'>�rea</a> ->
</div>

<?php
    
    unset($_SESSION['tipo']);
    
	if($_SESSION['IdDato'] != "")
    {        
        $Dato = $bdd->prepare("SELECT * FROM DatoFusalmo WHERE IdDatoFUSALMO = :id");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");    
?>
<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: #004d93; border-style: groove; ">
    <table style='width: 90%; margin-left: 5%; margin-top: 10px;'>    
        <tr><td style="color: blue;">�reas de Aplicaci�n - <?php echo $DatoView[1];?></td></tr>
        <tr><td> <hr color='#004d93' /></td></tr>
        <tr>
            <td style="text-align: right;">
            <?php
                if(isset($_GET['n']))
                {
                    if($_GET['n'] == 1)
                        echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Datos Ingresados Exitosamente</div>";
                        
                    if($_GET['n'] == 2)
                        echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Datos Eliminados Exitosamente</div>";
                        
                    if($_GET['n'] == 3)
                        echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Educadores Asignados Exitosamente</div>";                                                          
                }
            
                if($_SESSION["TipoUsuario"] != "Gestor"){
	       
            ?>
                <form action="DatoFUSALMO/ComponentesABM.php" method="post">
                    <table style="width: 90%; padding: 5px; margin-left: 5%;">
                        <tr><td colspan="3" style="text-align: center;"><h3>Formulario de �rea aplicaci�n</h3></td></tr>                        
                        <tr>
                            <td class="tdleft">Escriba el nombre de la  nueva �rea aplicaci�n:</td>
                            <td><input type='text' name="nombre" value="" required="true" title="Escribe el Nombre para el �rea" /></td>
                            <td><input type="submit" class="boton" name="Enviar" value="Agregar" /></td>
                        </tr>
                    </table>
                </form>
                
            </td>
        </tr>
        <tr><td style="padding-top: 50px;color: blue">Lista de �reas</td></tr>        
        <tr><td> <hr color='skyblue' /></td></tr>
        <?php }?>
        <tr>
            <td>
                <table style="width: 100%;">
                <tr><td style="width: 5%;">Id</td><td style="width: 40%; text-align: center;">Nombre</td><td style="width: 55%; text-align: center;">Acciones</td></tr>
                    <?php
	                       $DatosComponentes = $bdd->prepare("Select * from Educacion_Componente where IdPeriodo = :Id");
                           $DatosComponentes->bindParam(':Id', $_SESSION['IdPeriodo']);
                           $DatosComponentes->execute();
                           
                           if($DatosComponentes->rowCount() == 0)
                                echo "<tr><td colspan='3' style='color:red; text-align: center'>No hay �reas Registrados</td></tr>";
                                
                           else
                           {
                                while($Componentes = $DatosComponentes->fetch())
                                {
                                    
                                    $EsEdu = $bdd->prepare("Select * from Educacion_AsignacionEducador 
                                                            where IdComponente = :idCo and IdCiclo = :idCi and IdUsuario = :idUs");
                                    $EsEdu->bindParam(':idCo', $Componentes[0] );
                                    $EsEdu->bindParam(':idCi', $_SESSION['Ciclos']);
                                    $EsEdu->bindParam(':idUs', $_SESSION["IdUsuario"]);
                                    $EsEdu->execute();
                                    
                                    if($EsEdu->rowCount() > 0 || $_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["TipoUsuario"] == "Coordinador")
                                    {
                                        echo "<tr style='height: 35px'><td style='color:blue'>$Componentes[0]</td><td style='color: blue'>$Componentes[2]</td>
                                          <td style='text-align:center'>
                                              <form method='post' action='DatoFUSALMO/ComponentesABM.php' >
                                              <center>
                                              <table>
                                                    <tr> <input type='hidden' value='$Componentes[0]' name='id' />";
                                                    
                                                    if($_SESSION["TipoUsuario"] != "Gestor")
                                                    {
                                                        echo "<td><input type='submit' value='X' onclick='return confirmar();' name='Enviar' class='botonE' alt='X' style='width:30px;' /></td> 
                                                              <td><input type='submit' value='Educadores' name='Enviar' class='boton' alt='Asignar Educadores' /></td>";
                                                    }
                                                        echo "<td><input type='submit' value='Evaluaciones' name='Enviar' class='boton' alt='Asignaciones' /></td>";                                                                                                         
                                                        echo "<td><input type='submit' value='Asistencias' name='Enviar' class='boton' alt='Asistencias' /></td>";                                                        
                                                        
                                    echo "          </tr>
                                                    </table>
                                                    </center>
                                             </form>
                                          </td>";
                                    }
                                }
                           }
                    ?>
                </table>
            </td>
        </tr>
    </table>
</div>
