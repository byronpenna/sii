<?php

/**
 * @author Manuel
 * @copyright 2013
 */

require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Agregar")
    {
        $agregar = $bdd->prepare("Insert into Educacion_Componente Values(null, :idD, :nombre)");
        $agregar->bindParam(':idD', $_SESSION['IdPeriodo']);
        $agregar->bindParam(':nombre', $_POST['nombre']);        
        $agregar->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educación", "Crear Componente", $bdd);

        Redireccion("../FUSALMO.php?d=Component&n=1");
    }
    
    elseif($_POST['Enviar'] == "X")
    {
        $eliminar = $bdd->prepare("Delete from Educacion_Componente where IdComponente = :id");
        $eliminar->bindParam(':id', $_POST['id']);   
        $eliminar->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educación", "Eliminar Componente con Id" . $_POST['id'], $bdd);

        Redireccion("../FUSALMO.php?d=Component&n=2");
    }
    
    elseif($_POST['Enviar'] == "Educadores")
    {
        $_SESSION['Component'] =  $_POST['id'];
        Redireccion("../FUSALMO.php?d=EduComponent");
    }    
    
    elseif($_POST['Enviar'] == "Evaluaciones" || $_POST['Enviar'] == "Asistencias")
    {
        $_SESSION['Component'] =  $_POST['id'];
        
        if( $_POST['Enviar'] == "Asistencias")
            $_SESSION['tipo'] = "Asistencia";
        
        else
            $_SESSION['tipo'] = "Evaluacion";
        
        Redireccion("../FUSALMO.php?d=EvaluationIns");
    }     
    
    elseif($_POST['Enviar'] == "Guardar")
    {   
        $delete = $bdd->prepare("Delete from Educacion_AsignacionEducador where IdComponente = :idC and IdCiclo = :idP ");                                 
        $delete->bindParam(':idC', $_SESSION['Component']);  
        $delete->bindParam(':idP', $_SESSION['Ciclos']);         
        $delete->execute();   
        
        $IdUsuario = isset($_POST['UsuarioChequeado']) ? $_POST['UsuarioChequeado'] : array();            
        LogReg($_SESSION["IdUsuario"], "Educación", "Asignación Educadores a Componente " . $_SESSION['Component'], $bdd);
        
        foreach($IdUsuario as $idU) 
        {
            $insert = $bdd->prepare("Insert into Educacion_AsignacionEducador 
                                     values (null, :idC, :idP, :idU)");
                                     
            $insert->bindParam(':idC', $_SESSION['Component']);  
            $insert->bindParam(':idP', $_SESSION['Ciclos']);
            $insert->bindParam(':idU', $idU);           
            $insert->execute();                         
        }

        Redireccion("../FUSALMO.php?d=Component&n=3");
    }        
}
?>