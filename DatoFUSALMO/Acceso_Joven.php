<?php

/**
 * @author Manuel
 * @copyright 2013
 */

?>

<h1>Mis Datos FUSALMO</h1>

<?php
         if(isset($_GET['n']))
         {
            if($_GET['n'] == 1)
                echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right;'>Dato FUSALMO inscrito Exitosamente</div>";
                
            if($_GET['n'] == 2)
                echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right;'>Dato FUSALMO quitado Exitosamente</div>";                                
         }
?>
         <table style='width: 90%; background-color: white; border-radius: 10px; margin-left: 5%; border-radius: 10px; border-color: skyblue; border-style: groove; padding: 10px; margin-top: 20px;'>
         <tr>
            <th style='width: 80%;'><h2>Dato Fusalmo</h2></th>
            <td colspan="2"><a style="text-decoration: none;" href="FUSALMO.php?d=InsDat"><input type="button" value="Agregar Dato" style="width: 150px;"  class="boton"  /></a></td>
         </tr>
         <tr style="height: 5px;"><td colspan="4" style="background-color: skyblue;"></td></tr>
         <tr><td colspan="2">
         <table style="width: 100%;">
         <?php
	       $DatosFusalmo2 = $bdd->prepare("SELECT df.IdDatoFusalmo, df.Nombre, c.Ciclo, c.Tiempo, ins.NombreInstitucion , sec.SeccionEducativa FROM Educacion_Estudiantes AS e 
                                           Inner join Educacion_Inscripcion as i on i.IdInscripcion = e.IdInscripcion 
                                           Inner join Institucion as ins on ins.IdInstitucion = i.IdInstitucion
                                           Inner join Institucion_Seccion as sec on sec.IdSeccion = i.IdSeccion
                                           Inner join Educacion_Ciclos as c on c.IdCiclo = i.IdCiclo 
                                           Inner join DatoFusalmo as df on df.IdDatoFusalmo = c.IdDatoFusalmo 
                                           where IdUsuario = :IdUser");
                                           
           $DatosFusalmo2->bindParam(':IdUser' , $_SESSION["IdUsuario"]);
           $DatosFusalmo2->execute();
           
           if($DatosFusalmo2->rowCount() > 0)
           {
                while($datos = $DatosFusalmo2->fetch())
                {
                     echo "<tr>
                             <td style='text-align: center; width: 30%;' >$datos[1]</td>
                             <td style='text-align: center; width: 20%;' >$datos[2]<br /> $datos[3]</td>
                             <td style='text-align: center; width: 40%;' >$datos[4]<br /> $datos[5]</td>                                                           
                           </tr>";
                }
           }
           else
           {
                echo "<tr style='height: 5px;'>
                        <td colspan='2' style='text-align: center' ><h2 style='color:red'>No hay datos FUSALMO registrados</h2></td>
                      </tr>";
           }
         ?>
         </table>
         </td></tr>
	     </table>