<?php

/**
 * @author Manuel
 * @copyright 2013
 */
require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Guardar Notas")
    {
           
        $joven = isset($_POST['id']) ? $_POST['id'] : array();
        $nota = isset($_POST['nota']) ? $_POST['nota'] : array();        

        for ($index = 0 ; $index < count($joven); $index++) 
        {                    
          $EliminarNota = $bdd->prepare("Delete from Educacion_Notas where IdEvaluacion = :IdEva and IdUsuario = :IdUsu");
          $EliminarNota->bindParam(':IdEva', $_SESSION['IdEva']);
          $EliminarNota->bindParam(':IdUsu', $joven[$index]);
          $EliminarNota->execute();
          
          $InsertarNota = $bdd->prepare("Insert into Educacion_Notas values(null, :IdEva, :idJov, :idEdu, :nota)");
          $InsertarNota->bindParam(':IdEva', $_SESSION['IdEva']);
          $InsertarNota->bindParam(':idJov', $joven[$index]);
          $InsertarNota->bindParam(':idEdu', $_SESSION['IdUsuario']);
          $InsertarNota->bindParam(':nota', $nota[$index]);
          $InsertarNota->execute();
          
        }
        
        LogReg($_SESSION["IdUsuario"], "Educación", " Notas Guardadas, IdEvaluación  " . $_SESSION['IdEva'] , $bdd); 
        Redireccion("../FUSALMO.php?d=Evaluation&n=3");        
    }    
    
    if($_POST['Enviar'] == "Guardar todas las Notas")
    {
        $joven = isset($_POST['id']) ? $_POST['id'] : array();
        $Evaluacion = isset($_POST['Evaluacion']) ? $_POST['Evaluacion'] : array();


        for ($i = 0 ; $i < count($joven); $i++) 
        {            
            for ($j = 0 ; $j < count($Evaluacion); $j++) 
            {
                $EliminarNota = $bdd->prepare("Delete from Educacion_Notas where IdEvaluacion = :IdEva and IdUsuario = :IdUsu");
                $EliminarNota->bindParam(':IdEva', $Evaluacion[$j]);
                $EliminarNota->bindParam(':IdUsu', $joven[$i]);
                $EliminarNota->execute();

                $InsertarNota = $bdd->prepare("Insert into Educacion_Notas values(null, :IdEva, :idJov, :idEdu, :nota)");
                $InsertarNota->bindParam(':IdEva', $Evaluacion[$j]);
                $InsertarNota->bindParam(':idJov', $joven[$i]);
                $InsertarNota->bindParam(':idEdu', $_SESSION['IdUsuario']);
                $InsertarNota->bindParam(':nota', $_POST[$joven[$i]."_".$Evaluacion[$j]]);
                $InsertarNota->execute();
            }
        }
        
        for ($j = 0 ; $j < count($Evaluacion); $j++) 
            $Eva .= $Evaluacion[$j] . ", ";
        
        LogReg($_SESSION["IdUsuario"], "Educación", " Notas Guardadas desde Matriz Evaluaciones: { $Eva }", $bdd);
        Redireccion("../FUSALMO.php?d=Matriz&n=1");
    }    
}
?>
