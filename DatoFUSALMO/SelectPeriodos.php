<?php

/**
 * @author Manuel Calder�n
 * @copyright 2014
 */
    
   require '../net.php';

   $DatosInstituciones = $bdd->prepare("SELECT * FROM Educacion_Periodos AS p
                                        INNER JOIN Educacion_CicloPeriodo AS cp ON p.IdPeriodo = cp.IdPeriodo
                                        WHERE cp.IdCiclo = :idIns");
   $DatosInstituciones->bindParam(':idIns', $_GET["code"]);
   $DatosInstituciones->execute();
   
   echo "<option value='...'>...</option>";
    
   if($DatosInstituciones->rowCount() > 0)
   {
       while($Instituciones = $DatosInstituciones->fetch())
       {       
            echo "<option value='$Instituciones[0]'>" . $Instituciones[2] . "</option>";
       }
   }
   else
        echo "<option value='Error' style='color:red'>No hay instituciones Secciones registradas a este periodo.</option>";

?>

