<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administraci�n</a> ->
    <a href="/FUSALMO.php?d=Component" class='flecha' style='text-decoration:none'>�rea</a> ->
    <a href="/FUSALMO.php?d=EvaluationIns" class='flecha' style='text-decoration:none'>Evaluaciones</a> ->
    <a href="/FUSALMO.php?d=Evaluation" class='flecha' style='text-decoration:none'>Asignaci�n</a>
</div>

<?php
    unset($_SESSION['IdEva']);

    if($_SESSION['IdDato'] != "" || $_SESSION['Component']!= "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre, c.IdComponente, c.NombreComponente
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               Inner join Educacion_Componente as c on c.IdPeriodo = p.IdPeriodo                             
                               WHERE d.IdDatoFUSALMO = :id and c.IdComponente = :idc");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idc', $_SESSION['Component']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");
            

        $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                             FROM  Institucion as inst 
                                             inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                             where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
        
        $DatosInstituciones->bindParam(':idIns', $_SESSION['IdIns']);
        $DatosInstituciones->bindParam(':idSec', $_SESSION['IdSec']);   
        
        $DatosInstituciones->execute();
        $InfoSeccion = $DatosInstituciones->fetch();                                      
    
    
     if(isset($_GET['n']))
     {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right;'>Evaluaci�n Inscrita Exitosamente</div>";
            
        if($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right;'>Evaluaci�n retirada del Area</div>"; 
            
        if($_GET['n'] == 3)
            echo "<div id='Notificacion' name='Notificacion' style='color:blue; text-align: right;'>Notas de Evaluaci�n guardadas</div>";                                 
     }
?>

<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style='width: 90%; margin-left: 5%; margin-top: 10px;'>             
        <tr><td colspan="2" style="color: blue;">Evaluaciones del Area<br /><?php echo "$DatoView[1]<br />$DatoView[3]";?> </td></tr>
        <tr><td colspan="2" style="color: green; text-align: right;"><?php echo "$InfoSeccion[1]<br />$InfoSeccion[3]";?> </td></tr>
        
        
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr><td colspan="2" style="height: 20px; background-color: white;"></td></tr>        
        <tr><td  colspan="2" style="color: blue; text-align: right;" >Area: <?php echo $DatoView[5];?> </td></tr>  
        <tr>
            <td>
                <?php
	                   $DatosAsistencia = $bdd->prepare("SELECT * FROM Educacion_EvaluacionesAsistencia  
                                                         WHERE IdEvaluacion IN (
                                                                                  Select IdEvaluacion from Educacion_Evaluaciones 
                                                                                  where IdComponente = :idC and IdPeriodo = :idP and IdSeccion = :idSec
                                                                                )");
                       $DatosAsistencia->bindParam(':idC',$_SESSION['Component'] ) ;
                       $DatosAsistencia->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
                       $DatosAsistencia->bindParam(':idSec',$_SESSION['IdSec'] ) ;
                       $DatosAsistencia->execute();     
                       
                       $EvaluacionAsistencia = $DatosAsistencia->fetch();           
                
                
	                   $DatosEvaluacion = $bdd->prepare("Select * from Educacion_Evaluaciones 
                                                        where IdComponente = :idC and IdPeriodo = :idP and IdSeccion = :idSec");
                       $DatosEvaluacion->bindParam(':idC',$_SESSION['Component'] ) ;
                       $DatosEvaluacion->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
                       $DatosEvaluacion->bindParam(':idSec',$_SESSION['IdSec'] ) ;
                       $DatosEvaluacion->execute();
                       

                ?>
                <table style="width: 100%;">
                    <tr><td style="width: 5%;">N�</td> <td style="width: 50%;">Nombre</td> <td style="width: 25%;">Ponderaci�n</td> 
                    <td style="width: 20%; text-align: center;">
                        <?php
                                
	                           if($DatosEvaluacion->rowCount() > 0 && $DatosAsistencia->rowCount() > 0)
                               {
                                    echo "<a href='?d=Matriz'><input class='boton' style='width: 80px' type='button' name='Enviar' value='Matriz' /></a>";
                               }
                        ?>
                    </td></tr>
                    <?php
                            $maximo = 100;
                            
                            if($DatosEvaluacion->rowCount() > 0)
                            {
                               $i = 0;
	                           while($evaluaciones = $DatosEvaluacion->fetch())
                               {
                                   $i++;
	                               echo "<tr><td>$i</td><td>$evaluaciones[3]</td><td>$evaluaciones[4]%</td>
                                             <td><table>
                                                  <tr>
                                                    <td>
                                                        <form action='DatoFUSALMO/EvaluacionesABM.php' method='post'>
                                                            <input type='hidden' value='$evaluaciones[0]' name='idE'   />
                                                            <input type='submit' value='X' name='Enviar' onclick='return confirmar();' style='width: 30px;' class='botonE' />                                              
                                                        </form>
                                                    </td>";
                                              
                                              if($DatosAsistencia->rowCount() == 0)
                                                 echo "<td>
                                                        <form action='DatoFUSALMO/EvaluacionesABM.php' method='post'>
                                                            <input type='hidden' value='$evaluaciones[0]' name='idE'   />
                                                            <input type='submit' value='A' name='Enviar' onclick='return confirm(\"Estas Seguro que esta Evaluaci�n sea tu asistencia?\");' style='width: 30px;' class='botonG' />
                                                        </form>
                                                       <td>";
                                              
                                              else
                                              {
                                                    if($EvaluacionAsistencia[1] != $evaluaciones[0])
                                                        echo "<td>
                                                                <form action='DatoFUSALMO/EvaluacionesABM.php' method='post'>
                                                                    <input type='hidden' value='$evaluaciones[0]' name='idE'   />
                                                                    <input type='submit' value='Notas' name='Enviar' style='width: 75px;' class='boton' />
                                                                </form>
                                                              </td>";
                                                        
                                                    else
                                                        echo "<td>
                                                                <form action='FUSALMO.php?d=ReportStudentsDetails' method='post' target='_blank'>
                                                                    <input type='submit' value='Asistencias' name='Enviar' style='width: 75px;' class='boton' />
                                                                    <input type='hidden'  value='".$_SESSION['IdIns']."' name='IdIns' style='width: 75px;' class='boton' />
                                                                    <input type='hidden'  value='".$_SESSION['IdSec']."' name='IdSec' style='width: 75px;' class='boton' />
                                                                    <input type='hidden'  value='".$_SESSION['Component']."' name='Componente' style='width: 75px;' class='boton' />
                                                                </form>
                                                              </td>   
                                                              <td>
                                                                <form action='DatoFUSALMO/EvaluacionesABM.php' method='post'>
                                                                    <input type='hidden' value='$evaluaciones[0]' name='idE'   />
                                                                    <input type='submit' value='Q' name='Enviar' onclick='return confirm(\"Estas Seguro que esta Evaluaci�n ya no sea tu asistencia?\");' style='width: 30px;' class='botonE' />
                                                                </form>
                                                              </td>";
                                              }
                                                
                                              echo "
                                             </table></td>
                                         </tr>";
                                   $maximo = $maximo - $evaluaciones[4];
	                           }
                            }
                            else    
                                echo "<tr><td colspan='4' style='color: red; text-align: center;'> No hay Evaluaciones Registradas...</td></tr>";
                    ?>
                </table>
            </td>
        </tr>
        
        <tr style="height: 30px;"><td></td></tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr style="height: 30px;"><td></td></tr>
        <tr>
            <td>
                <table style="width: 80%;">
                <form action="DatoFUSALMO/EvaluacionesABM.php" method="post">
                    <tr><td colspan="3" style="text-align: center;">Agregar Evaluaci�n </td></tr>
                    <tr><td style="text-align: right; padding-right: 20px">Nombre de Evaluaci�n:</td><td colspan="2"><input type="text" name="nombre" value="" required="true" title="Necesitamos el Nombre"/></td></tr>
                    <tr><td style="text-align: right; padding-right: 20px">Ponderaci�n de Evaluaci�n:</td><td><input type="number" name="ponderacion" min="1" max="<?php echo $maximo;?>" required="true" title="Ponderaci�n no Valida" /></td>
                        <td>Restante: <?php echo $maximo;?>%</td></tr>       
                    <?php if($maximo > 0){ ?>             
                        <tr><td colspan="3" style="text-align: center;"><input type="submit" value="Agregar" name="Enviar" class="boton" /></td></tr>                    
                    <?php }
                          else
                            echo "<tr><td colspan='3' style='text-align: center;'><input type='buttom' style='text-align: center;' value='0%' name='Enviar' class='botonE' />  </td></tr>";
                                
                    ?>
                </form>
                </table>
            </td>
        </tr>        
</table>

</div>