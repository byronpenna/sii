<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administración</a> ->
    <a href="/FUSALMO.php?d=Institution" class='flecha' style='text-decoration:none'>Instituciones</a> ->
    <a href="/FUSALMO.php?d=InstitutionGroup" class='flecha' style='text-decoration:none'>Secciones</a> ->
</div>

<?php
	if($_SESSION['IdDato'] != "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre 
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               WHERE d.IdDatoFUSALMO = :idD and p.IdPeriodo = :idP");
                               
        $Dato->bindParam(':idD', $_SESSION['IdDato']);
        $Dato->bindParam(':idP', $_SESSION['IdPeriodo']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    
  
             
        $idIns = $_SESSION["Institucion"]; 
        
        $DatosInstitucion = $bdd->prepare("SELECT * FROM Institucion where IdInstitucion = $idIns ");
        $DatosInstitucion->execute();
        $Ins = $DatosInstitucion->fetch();
      
        if($idIns == "")
            Redireccion("../FUSALMO.php?d=Institution");  
            
        else
            $_SESSION["Instituto"] = $idIns;
            
            
            
        $DatosCiclo = $bdd->prepare("SELECT * FROM Educacion_Ciclos where IdCiclo = ".$_SESSION['Ciclos']);
        $DatosCiclo->execute();
        $ciclo = $DatosCiclo->fetch();            
        

    }
    else
            Redireccion("../FUSALMO.php");    
?>
<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style=" width: 90%; margin-left: 5%; margin-top: 20px;">
<tr><td colspan="3" style="color: blue;">Listado de Instituciones Relacionadas</td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $DatoView[1];?></td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $DatoView[3];?></td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $Ins[1];?></td></tr>
        <tr><td colspan="3" style="height: 20px; background-color: white;"></td></tr>  
        <tr><td colspan="3" style=" background-color: skyblue; height: 3px;"></td></tr>
        <tr><td colspan="3" style="height: 20px; background-color: white;"></td></tr>          
        <tr>
            <td>
            <table style="width: 80%; margin-left: 10%;">
            <tr><td style="color: green;">Nota: </td><td colspan="2" style="color: green;">Seleccione las Secciones de la Institucion <br /> 
                para este Ciclo: <?php echo $ciclo[2];?></strong></td></tr>
            <tr><td colspan="3" style=" background-color: skyblue; height: 3px;"></td></tr>
            <tr><td style="width: 10%">Id</td><td style="width: 80%;">Secciones</td><td style="width: 10%">
            <input type="button" value="Marcar todas" id="botoncheck" onclick="checkear();" class="boton" style="width: 125px;" />
                          </td></tr>
            <form name="form1" action='DatoFUSALMO/InstitucionesABM.php' method='post'> 
            <?php

                                                        
                        $Instituciones = $bdd->prepare("SELECT inst.IdSeccion, inst.SeccionEducativa 
                                                        FROM InscripcionSeccion AS ins
                                                        INNER JOIN Institucion_Seccion AS inst ON ins.IdSeccion = inst.IdSeccion                                                       
                                                        WHERE ins.IdDatoFusalmo = $DatoView[0] and ins.IdInstitucion =  $idIns");
                        $Instituciones->execute();
                        
                        if($Instituciones->rowCount() > 0)
                        {
                            while($Secciones = $Instituciones->fetch()) 
                            {                                                 
                                echo "<tr><td>$Secciones[0]</td><td>$Secciones[1]</td><td style='text-align: center;'>";
        
                                $chequeado = $bdd->prepare("SELECT * FROM Educacion_Inscripcion                                                      
                                                            WHERE IdCiclo = ".$_SESSION['Ciclos'] ." and IdSeccion =  $Secciones[0]");
                                $chequeado->execute();                        
    
                                    if($chequeado->rowCount() > 0)
                                        echo "<input type='checkbox' name='Seccion[]' value='$Secciones[0]' checked='true' /></td>";
    
                                    else
                                        echo "<input type='checkbox' name='Seccion[]' value='$Secciones[0]' />";                       
    
                                echo "</tr>";
                            }
                        }
                        else
                            echo "<tr><td style='text-align: center;' colspan='3'>Error, no hay Secciones Agregadas.</td></tr>";
            ?>
            
            <tr><td style="text-align: center;" colspan="3"><input type="submit" class="boton" value="Guardar" name="Enviar" onclick='return ConfirmarSecciones();' /></td></tr>
            </form>
            </table>
            </td>
        </tr>
</table>
</div>
<script>

function checkear()
{
    boton = document.getElementById('botoncheck');
    
    if(boton.value == "Marcar todas")
    { 
        seleccionar_todo();
        boton.value = "Desmarcar todas"
    }
    else
    {
        deseleccionar_todo();
        boton.value = "Marcar todas"
    }
}

function seleccionar_todo(){ 
   for (i=0;i<document.form1.elements.length;i++) 
      if(document.form1.elements[i].type == "checkbox")	
         document.form1.elements[i].checked=1 
}

function deseleccionar_todo(){ 
   for (i=0;i<document.form1.elements.length;i++) 
      if(document.form1.elements[i].type == "checkbox")	
         document.form1.elements[i].checked=0 
} 
</script>