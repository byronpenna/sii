<?php

/**
 * @author Manuel Calder�n
 * @copyright 2014
 */

   require '../net.php';

   $DatosInstituciones = $bdd->prepare("SELECT i.* FROM Institucion as i
                                        inner join Educacion_Inscripcion as ei on i.IdInstitucion = ei.IdInstitucion
                                        where ei.IdCiclo = :idIns
                                        group by i.IdInstitucion");
                                        
   $DatosInstituciones->bindParam(':idIns', $_GET["code"]);
   $DatosInstituciones->execute();
   
   echo "<option value='...'>...</option>";
   
   if($DatosInstituciones->rowCount() > 0)
   {
       while($Instituciones = $DatosInstituciones->fetch())
       {       
            echo "<option value='$Instituciones[0]'>" . $Instituciones[1] . "</option>";
       }
   }
   else
        echo "<option value='Error' style='color:red'>No hay instituciones Secciones registradas a este periodo.</option>";

?>