<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=ReporteInventario.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';
    
    $Verificacion = $bddC->prepare($_SESSION['AuxQuery']);
    $Verificacion->execute();
?>
    <table>
    <tr><th style="width: 5%;">N�</th>
    <th style="width: 10%;">Equipo</th>
    <th style="width: 20%;">Modelo</th>
    <th style="width: 30%;">N�mero de Serial</th>
    <th style="width: 10%;">Estado de Verificaci�n</th>
    <th style="width: 10%;">Estado</th>
    <th style="width: 15%;">Fecha</th>     
     
    <?php                    
    if($Verificacion->rowCount())
    {
        $i = 1;
        while($Equipo = $Verificacion->fetch())
        {
            if($Equipo[6] == "Verificado")
                $color = "Green";
            else
                $color = "Black";
                
            echo "<tr>
                      <td style='color: $color'>$i</td>  
                      <td style='color: $color'>$Equipo[1]</td>
                      <td style='color: $color'>$Equipo[2]</td>
                      <td style='color: $color'>$Equipo[3]</td>
                      <td style='color: $color'>$Equipo[6]</td>
                      <td style='color: $color'>$Equipo[5]</td>
                      <td style='color: $color'>$Equipo[8]</td>
                  </tr>";
    
            $i++;
        }
    }
    else                        
        echo "<tr><td colspan='10' style='text-align: center;'><br /><h3 style='color: blue;'>No hay datos ingresados para el dia de hoy</h3></td></tr>";                        
    ?>
</table>
