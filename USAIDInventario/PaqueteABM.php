<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 
require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Ver Paquete")
    {
        $_SESSION['IC'] = $_POST["IC"];
        Redireccion("../Inventario.php?i=PackageEquip");        
    }
    elseif($_POST['Enviar'] == "A�adir")
    {
//        $Buscar = $bddC->prepare("Select * from Inventario_Paquete where IdEquipo = " .  $_SESSION['IC']);
//        $Buscar->execute();
//        
//        if($Buscar->rowCount() == 0)
//        {   
            $Insert = $bddC->prepare("Insert into Inventario_Paquete values(null,:IdC , :Paquete, :IE, 'No Verificada', :date, :user)");
            $Insert->bindParam('IdC', $_SESSION['IC']);
            $Insert->bindParam('Paquete', $_SESSION['Paquete']);
            $Insert->bindParam('IE', $_POST['IE']);
            $Insert->bindParam('date', date("Y-m-d"));
            $Insert->bindParam('user', $_SESSION["IdUsuario"]);            
            $Insert->execute();

            $Query = "Update Inventario_Equipos set Estado = 'En Paquete' where IdEquipo = " .$_POST['IE'];
            $Update = $bddC->prepare($Query);
            $Update->execute();             
            
            
            /** Guardando al log **/
            $Datos = "Equipo: " . $_POST['IE'] . " , Instituto: " . $_SESSION['IC'] ;
            
            $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'A�adiendo al paquete = $Datos', :Usuario, 'Paquetes', :Fecha)");
            $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
            $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));        
            $Insert->execute();        
            
           Redireccion("../Inventario.php?i=PackageEquip&n=1");  
//        }        
//        else        
//            Redireccion("../Inventario.php?i=PackageEquipn=4");  
        
    }
    elseif($_POST['Enviar'] == "x")
    {
        $Delete = $bddC->prepare("Delete from Inventario_Paquete where IdEquipo = " . $_POST['IE']);
        $Delete->execute();

        $Query = "Update Inventario_Equipos set Estado = 'Bodega' where IdEquipo = " .$_POST['IE'];
        $Update = $bddC->prepare($Query);
        $Update->execute();           
            
        
        /** Guardando al log **/
        $Datos = "Equipo: " . $_POST['IE'] . " , Instituto: " . $_SESSION['IC'] ;
        
        $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Quitando al paquete = $Datos', :Usuario, 'Paquetes', :Fecha)");
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));        
        $Insert->execute();     
            
        
        Redireccion("../Inventario.php?i=PackageEquip&n=3"); 
    }
    elseif($_POST['Enviar'] == "Subir")
    {     
            // obtenemos los datos del archivo
            $tamano = $_FILES["archivo"]['size'];
            $tipo = $_FILES["archivo"]['type'];
            $archivo = $_FILES["archivo"]['name'];
            $prefijo = substr(md5(uniqid(rand())),0,6);  
            $destino =  $_SERVER['DOCUMENT_ROOT']."/USAIDInventario/Actas/".$prefijo."_".$archivo;
            
            if(copy($_FILES['archivo']['tmp_name'], $destino))
            {                

                $AddFile = $bddC->prepare("Insert into Inventario_Acta values (Null,".$_SESSION['IC'].",'".$_SESSION['Paquete']."','" . $prefijo."_".$archivo."')");
                $AddFile->execute();
                
                /** Guardando al log **/
                $Datos = "Paquete: " . $_SESSION['Paquete'] . " , Instituto: " . $_SESSION['IC'] . ", Archivo: " . $prefijo."_".$archivo ;
                
                $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Subiendo Acta al paquete = $Datos', :Usuario, 'Paquetes', :Fecha)");
                $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
                $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));        
                $Insert->execute();     

                
                
                Redireccion("../Inventario.php?i=PackageEquip&n=1"); 
                
            }            
            else
                Redireccion("../Inventario.php?i=PackageEquip&n=4"); 
    }
    elseif($_POST['Enviar'] == "Quitar")
    {
        $Query = "Delete from Inventario_Acta where Archivo = '" . $_POST['file']. "'";
        $Delete = $bddC->prepare($Query);
        $Delete->execute();    
        
        $destino = $_SERVER['DOCUMENT_ROOT']."/USAIDInventario/Actas/";
                            
        //unlink($destino . $_POST['file']);
        
        
        /** Guardando al log **/
        $Datos = " Archivo: " . $_POST['file'] ;
        
        $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Eliminando Acta al paquete = $Datos', :Usuario, 'Paquetes', :Fecha)");
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));        
        $Insert->execute();     
        
        Redireccion("../Inventario.php?i=PackageEquip&n=3");     
    }
    elseif($_POST['Enviar'] == "Validar")
    {
        
        $Query = "Update Inventario_Paquete set Verificacion = 'Validada' where IdInstitucion = '" . $_SESSION['IC']. "' and Paquete = '" . $_SESSION['Paquete']. "'";
        $Update = $bddC->prepare($Query);
        $Update->execute();    

        /** Guardando al log **/
        $Datos = "IdInstitucion = '" . $_SESSION['IC']. "', Paquete = '" . $_SESSION['Paquete']. "'";
        
        $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Paquete Validado = $Datos', :Usuario, 'Paquetes', :Fecha)");
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));        
        $Insert->execute();    
        
        Redireccion("../Inventario.php?i=PackageEquip&n=2");          
    }
}
?>