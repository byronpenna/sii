<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 
if(isset($_POST['Busqueda']))
    $Busqueda = $_POST['Busqueda'];
else
    $Busqueda = "";
    
unset($_SESSION['IC']);
    
?>

<div style="width: 100%; background-color: white; border-radius: 5px;">    
    <div class="Raiz" style="padding-top: 10px;">
        <a href="/Inventario.php" class='flecha' style='text-decoration:none'> Inventario </a> ->
        <a href="/Inventario.php?i=Package" class='flecha' style='text-decoration:none'> Paquetes </a>
    </div>
    
    <table style="width: 90%; margin-left: 5%;">
    <tr><td><h2 style="color: blue;">Paquetes</h2><hr color='skyblue' /></td></tr>
    <tr>
        <td>
            <table style="width: 90%; margin-left: 5%;">
                <tr>
                    <td colspan="4">
                    <em style="color: green; text-align: center;">Indicaciones: Escriba el nombre del centro escolar al que es dirigido el Paquete, 
                    en el encontrara el equipo destinado al centro escolar.</em>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%;text-align: right; padding-right: 10px;">Busqueda de Paquetes:</td>                    
                    <form action="" method="post">
                        <td style="width: 45%;"><input type="text" name="Busqueda" value="<?php echo $Busqueda?>"  style="width: 90%;" /></td>
                        <td style="width: 15%;">                            
                            <input type="submit" class="boton" name="Enviar" value="Buscar" />
                        </td>
                    </form>  
                    <td>
                        <form action="USAIDInventario/PaqueteExport.php" method="post">
                        <input type="submit" class="boton" name="Enviar" value="Exportar Paquetes" style="width: 120px;" />
                        </form>
                    </td>                
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
        <?php
           if(isset($_POST['Busqueda']))
           { 
               
               $CentroEscolar = $bddC->prepare("Select * from USAID_Instituciones where Nombre like '%$Busqueda%' or Municipio like '%$Busqueda%' or Codigo like '%$Busqueda%'");
               $CentroEscolar->execute();
               
               if($CentroEscolar->rowCount() > 0)
               {
                   echo "<table style='width: 100%'>";
                   while($DataC = $CentroEscolar->fetch())
                   {
                        
                        echo "<tr>
                                  <form action='USAIDInventario/PaqueteABM.php' method='post'>
                                  <td>$DataC[0]</td><td>$DataC[3]</td><td>$DataC[5]</td>                                    
                                  <td>
                                      <input type='hidden' name='IC' value='$DataC[0]' />
                                      <input type='submit' class='boton' name='Enviar' value='Ver Paquete' />
                                  </td>
                                  </form>                                  
                              </tr>";
                               
                   }
                   echo "</table>";
               }
               else
                   echo "<tr><td colspan='2' style='text-align: center'><br /><h3 style='color: red;'>No hay resultados de la busqueda...</h3></td></tr>";
	           
           }
        ?>
        </td>
    </tr>
    </table>
    <br /><br />
</div>