<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Guardar Recurso")
    {
        $Insert = $bddC->prepare("Insert into Inventario_Equipos values(Null, :Equipo, :Modelo, :SerialNumber, :MAC, 'Bodega', 'No Verificado', :garantia, :Fecha, :Usuario)");
        $Insert->bindParam(':Equipo', $_POST['TipoRecurso']);
        $Insert->bindParam(':Modelo', $_POST['Modelo']);
        $Insert->bindParam(':SerialNumber', $_POST['Serial']);
        $Insert->bindParam(':MAC', $_POST['MAC']);
        $Insert->bindParam(':garantia', $_POST['garantia']);
        $Insert->bindParam(':Fecha', date("Y-m-d"));
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->execute();
        
        
        /** Guardando al log **/
        $Datos = $_POST['TipoRecurso'] . " - " . $_POST['Modelo'] . " - " . $_POST['Serial'] ;
        
        $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Creaci�n Equipo = $Datos', :Usuario, 'Equipos', :Fecha)");
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));        
        $Insert->execute();        
        
        Redireccion("../Inventario.php?n=1");
    }
    
    elseif($_POST['Enviar'] == "Actualizar Recurso")
    {
        $Update = $bddC->prepare("Update Inventario_Equipos set
                                  Equipo = :Equipo, 
                                  Modelo = :Modelo,
                                  SerialNumber = :SerialNumber,
                                  MAC = :MAC, 
                                  Garantia = :Garantia
                                  Where IdEquipo = :IdE");
                                  
        $Update->bindParam(':Equipo', $_POST['TipoRecurso']);
        $Update->bindParam(':Modelo', $_POST['Modelo']);
        $Update->bindParam(':SerialNumber', $_POST['Serial']);
        $Update->bindParam(':MAC', $_POST['MAC']);
        $Update->bindParam(':Garantia', $_POST['garantia']);
        $Update->bindParam(':IdE', $_SESSION["IdEquipo"]);
        $Update->execute();
        
        $Datos = $_POST['TipoRecurso'] . " - " . $_POST['Modelo'] . " - " . $_POST['Serial'] ;
        
        /** Guardando al log **/
        $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Actualizaci�n Equipo = $Datos', :Usuario, 'Equipos', :Fecha)");
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));        
        $Insert->execute();     
        
        Redireccion("../Inventario.php?i=Equip&n=2");
    }
        
    elseif($_POST['Enviar'] == "Ver")
    {
        $_SESSION["IdEquipo"] = $_POST['IE'];
        Redireccion("../Inventario.php?i=Equip");
    }
    
    elseif($_POST['Enviar'] == "Modificar")
    {
        $_SESSION["IdEquipo"] = $_POST['IE'];
        Redireccion("../Inventario.php?i=EquipForm");
    }
    
    elseif($_POST['Enviar'] == "Eliminar")
    {
        $Delete = $bddC->prepare("Delete from Inventario_Equipos where IdEquipo =" . $_POST['IE']);
        $Delete->execute();
        
        $Datos = $_POST['IE'];
        
        /** Guardando al log **/
        $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Eliminaci�n Equipo Id = $Datos', :Usuario, 'Equipos', :Fecha)");
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));        
        $Insert->execute();           
        
        Redireccion("../Inventario.php?n=1");
    } 
    
    elseif($_POST['Enviar'] == "Verificar")
    {
        $Update = $bddC->prepare("Update Inventario_Equipos set EstadoV = 'Verificado' where IdEquipo =" . $_POST['IE']);
        $Update->execute();
        
        $Datos = $_POST['IE'];
        
        /** Guardando al log **/
        $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Verificaci�n Equipo Id = $Datos', :Usuario, 'Equipos', :Fecha)");
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));        
        $Insert->execute();           
        
        Redireccion("../Inventario.php?i=Equip&n=1");
    }        
    
    
    
    elseif($_POST['Enviar'] == "Guardar Licencia")
    {
        $Insert = $bddC->prepare("Insert into Inventario_Licencia values(Null, :Equipo, :Software, :Licencia, :Tipo, :Usuario)");
        $Insert->bindParam(':Equipo', $_SESSION['IdEquipo']);
        $Insert->bindParam(':Software', $_POST['Software']);
        $Insert->bindParam(':Licencia', $_POST["Licencia"]);
        $Insert->bindParam(':Tipo', $_POST["Tipo"]);
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->execute();
        
        /** Guardando al log **/
        $Datos = $_POST['Software'] . " - " . $_POST["Licencia"];
        
        $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Adici�n de Licencia = $Datos', :Usuario, 'Licencias', :Fecha)");
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));    
        $Insert->execute();     
        
        Redireccion("../Inventario.php?i=Equip&n=1");
    }    
    
    elseif($_POST['Enviar'] == "x")
    {
        $Delete = $bddC->prepare("Delete from Inventario_Licencia where IdLicencia =" . $_POST['IL']);
        $Delete->execute();
        
        $Datos = $_POST['IL'];
        
        /** Guardando al log **/
        $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Eliminaci�n Licencia Id = $Datos', :Usuario, 'Licencia', :Fecha)");
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));        
        $Insert->execute();           
        
        Redireccion("../Inventario.php?i=Equip&n=3");
    }
    
    elseif($_POST['Enviar'] == "Cambiar")
    {
        $_SESSION["IdEquipo"] = $_POST['IE'];
        Redireccion("../Inventario.php?i=State");
    }   
    elseif($_POST['Enviar'] == "Actualizar Estado")
    {
        $Query = "Update Inventario_Equipos set Estado = '".$_POST['Estado']."' where IdEquipo = " .$_SESSION['IdEquipo'];
        $Update = $bddC->prepare($Query);
        $Update->execute();             
        
        /** Guardando al log **/
        $Datos = $_SESSION['IdEquipo'] . " - " . $_POST["Estado"];
        
        $Insert = $bddC->prepare("Insert into Inventario_Log values(Null, 'Cambio de Estado = $Datos', :Usuario, 'Equipo', :Fecha)");
        $Insert->bindParam(':Usuario', $_SESSION["IdUsuario"]);
        $Insert->bindParam(':Fecha', date("Y-m-d H:i:s"));    
        $Insert->execute();            

        Redireccion("../Inventario.php?i=Equip&n=2");                    
    }        
    
    
}
else
    Redireccion("../Inventario.php?n=5");


?>