<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 
 if($_SESSION["IdEquipo"] != "")
 {
    $Update = $bddC->prepare("Select * from Inventario_Equipos where IdEquipo = " . $_SESSION["IdEquipo"]);
    $Update->execute();
    
    $DataUpdate = $Update->fetch();
    
    $ABM = "Actualizar Estado";
 }
 else
 {
    Redireccion("../Inventario.php?n=5");
 }
?>


<div style="width: 100%; background-color: white; border-radius: 5px;">    
    <div class="Raiz" style="padding-top: 10px;">
        <a href="/Inventario.php" class='flecha' style='text-decoration:none'> Inventario </a> ->
        <a href="/Inventario.php?i=EquipForm" class='flecha' style='text-decoration:none'> Agregar Equipo </a>
    </div>
    <hr color='Skyblue' style="width: 90%; margin-left: 5%;" />
    <form method="Post" action="USAIDInventario/EquipoABM.php">
    <table style="width: 90%; margin-left: 5%;">
        <tr>
            <td style="width: 50%;"><h2 style="color: blue; padding-left: 30px;">Formulario de Equipo</h2></td>
            <td style="text-align: left;">
            </td>
        </tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td colspan="2">
                <table style="width: 60%; margin-left: 20%;">
                    <tr><td class="tdleft">Tipo de Equipo:</td><td><?php echo $DataUpdate[1];?></td></tr>
                    <tr><td class="tdleft" >Modelo:</td><td><?php echo $DataUpdate[2];?></td></tr>
                    <tr><td class="tdleft" style="vertical-align: top;">Numero Serial i/o <br />Numero del Producto:</td><td><?php echo $DataUpdate[3];?></td></tr>
                    <tr><td class="tdleft" style="vertical-align: top;">Direcci�n MAC:</td><td><?php echo $DataUpdate[4];?></td></tr>
                    <tr><td class="tdleft">Periodo de Garantia:</td><td><?php echo $DataUpdate[7];?></td></tr>
                    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
                    <tr>
                        <td class="tdleft">Seleccione el Estado:</td>
                        <td>
                            <select name="Estado">
                                <?php
	                               echo $DataUpdate[5] == "Bodega"      ? "<option selected='true' value='Bodega'>Bodega</option>"         : "<option value='Bodega'>Bodega</option>";
                                   echo $DataUpdate[5] == "En Uso"      ? "<option selected='true' value='En Uso'>En Uso</option>"         : "<option value='En Uso'>En Uso</option>";
                                   echo $DataUpdate[5] == "En FUSALMO"  ? "<option selected='true' value='En FUSALMO'>En FUSALMO</option>" : "<option value='En FUSALMO'>En FUSALMO</option>";
                                   echo $DataUpdate[5] == "Defectuoso"  ? "<option selected='true' value='Defectuoso'>Defectuoso</option>" : "<option value='Defectuoso'>Defectuoso</option>";   
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr><td colspan="2" style="text-align: center;  padding-top: 10px;"><input type="submit" name="Enviar" class="boton" value="<?php echo $ABM;?>" style="width: 120px;" /></td></tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <br /><br />
</div>