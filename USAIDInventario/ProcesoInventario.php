<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

if(isset($_GET['n']))
{
    if($_GET['n'] == 1)
        echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right;'>Dato Ingresado Exitosamente</div>";
                    
    if($_GET['n'] == 2)
        echo "<div id='Notificacion' name='Notificacion' style='color:blue; text-align: right;'>Dato Actualizado Exitosamente</div>";

    if($_GET['n'] == 3)
        echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right;'>Dato Quitado exitosamente ...</div>";   
        
    if($_GET['n'] == 4)
        echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right;'>Error, No se pudo ejecutar la acci�n ...</div>";   
                    
    if($_GET['n'] == 5)
        echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right;'>Error, Acceso denegado...</div>";                                              
}

if(isset($_GET['i']))
{
       if($_GET['i'] == "EquipForm")       
            include("USAIDInventario/EquipoForm.php");
       
       elseif($_GET['i'] == "Equip")
            include("USAIDInventario/Equipo.php");
            
       elseif($_GET['i'] == "Package")
            include("USAIDInventario/PaqueteList.php"); 
            
       elseif($_GET['i'] == "PackageForm")
            include("USAIDInventario/PaqueteForm.php");                        
                        
       elseif($_GET['i'] == "PackageEquip")
            include("USAIDInventario/PaqueteEquip.php");                                                
                        
       elseif($_GET['i'] == "Statistics")
            include("USAIDInventario/DatosEstadisticos.php");                        
                                                
       elseif($_GET['i'] == "State")
            include("USAIDInventario/EquipoState.php");                        
                                                
                    
                        
       else
            Redireccion("Inventario.php");
}

else
{
    if($_SESSION["TipoUsuario"] == "USAID-CI" || $_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "Administrador")    
        include("USAIDInventario/EquipoList.php");
        
    else 
        include("USAIDInventario/PaqueteList.php");
    
}
?>