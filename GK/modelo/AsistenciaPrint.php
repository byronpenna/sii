<?php

/**
 * @author Lilian Cordero
 * @copyright 2014
 */
require '../net.php';

if($_SESSION['IdDato'] != "" || $_SESSION['IdPeriodo']!= "")
    {        
        $Dato = $bdd->prepare("SELECT d.Nombre, c.Ciclo, p.Nombre 
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFusalmo = d.IdDatoFusalmo
                               inner join Educacion_Ciclos as c on c.IdDatoFusalmo = d.IdDatoFusalmo  
                               where p.IdDatoFusalmo = :id and p.IdPeriodo = :idp and c.IdCiclo = :idc");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idp', $_SESSION['IdPeriodo']);
        $Dato->bindParam(':idc', $_SESSION['Ciclos']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");
    
    
    $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                         FROM  Institucion as inst 
                                         inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                         where sec.IdSeccion = :idSec");
    $DatosInstituciones->bindParam(':idSec', $_SESSION['Seccion']);   
    
    $DatosInstituciones->execute();
    $InfoSeccion = $DatosInstituciones->fetch();    

                       
?>
<script >
function cerrar() { setTimeout(window.close,1500); }
</script>
<style>
.
{
    text-align: right;
    padding-right: 10px;
}
</style>
<body onload="window.print();cerrar();" style="font-family: Calibri; font-size: small;">
<div style="width: 90%;margin-left: 5%; background-color: white; ">
<table style='width: 100%; border-style: double;' rules='all'>             
        
        <tr>
            <td colspan="2">
                <table style="width: 100%;">
                    <tr><td colspan="tdleft" style="width: 35%;">Dato FUSALMO:</td><td><?php echo $DatoView[0];?></td></tr>
                    <tr><td colspan="tdleft">Ciclo:</td><td><?php echo $DatoView[1];?></td></tr>
                    <tr><td colspan="tdleft">Periodo:</td><td><?php echo $DatoView[2];?></td></tr>
                    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
                    <tr><td colspan="tdleft">Instituci�n:</td><td><?php echo $InfoSeccion[1];?></td></tr>
                    <tr><td colspan="tdleft">Grupo � Secci�n:</td><td><?php echo $InfoSeccion[3];?></td></tr>
                </table>
            </td> 
            <td colspan="5" style="text-align: center;">            
                <img src="../images/logo.jpg" style="width: 90%; height: 35%;" />
            </td>           
        </tr>
        <tr><td colspan="7"></td></tr>      
        <form name="asistencia" method="post" action="DatoFUSALMO/AsistenciaABM.php" name="form1" id="form1" >                
        <tr><td style="width: 10%; text-align: center;">N�</td>
            <td style="width: 50%; text-align: center;">Nombre</td>
            <td style="width: 8%;"></td>
            <td style="width: 8%;"></td>
            <td style="width: 8%;"></td>
            <td style="width: 8%;"></td>
            <td style="width: 8%;"></td>
            </tr>
        
        <?php
               $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2, ins.IdInscripcion
                                              FROM Educacion_Estudiantes  as est
                                              inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                              INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                              where ins.IdCiclo = :idC and ins.IdInscripcion = :idI and ins.IdSeccion = :idS
                                              ORDER BY  jov.Apellido1 ASC, jov.Apellido2 ASC  ");
                                              
               $listaAlumnos->bindParam(':idC', $_SESSION['Ciclos']);
               $listaAlumnos->bindParam(':idI', $_SESSION['IdInscript']);
               $listaAlumnos->bindParam(':idS', $_SESSION['Seccion']);               
               $listaAlumnos->execute();
               
               $i = 0;
               $asistencia = 0;

               if($listaAlumnos->rowCount() > 0)
               {
                   while($alumno = $listaAlumnos->fetch())
                   {
                        $i++;      
                        echo "<tr>                                
                                <td style='text-align: center;'>$i</td>
                                <td style='text-align: left;'>$alumno[2] $alumno[3], $alumno[1]</td>
                                <td style='text-align: center;'><input type='checkbox' /></td>
                                <td style='text-align: center;'><input type='checkbox' /></td>
                                <td style='text-align: center;'><input type='checkbox' /></td>
                                <td style='text-align: center;'><input type='checkbox' /></td>
                                <td style='text-align: center;'><input type='checkbox' /></td>
                              </tr>";
                   }
                   
               }
               else
                    echo "<tr><td colspan='4' style='color:red; text-align: center;'>No hay Alumnos registrados a esta Secci�n</td></tr>";
        ?>
        </form>            
        
</table>
</div>
</body>