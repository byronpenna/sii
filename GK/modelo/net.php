<?php session_start();
header('Content-type: text/html; charset=ISO-8859-1');
ini_set("session.cookie_lifetime","86400");
ini_set("session.gc_maxlifetime","86400");


date_default_timezone_set('Etc/GMT+6');


try
{
    $bdd = new PDO('mysql:host=localhost;dbname=usuarios', 'root', '');
        
    function Redireccion($Redirecion)
    {    
        echo"<script language='javascript'>window.location='$Redirecion'</script>";
    }
    
    function Seguridad()
    {
        if($_SESSION["autenticado"] != true)
        echo"<script language='javascript'>window.location='http://localhost/biblioteca/index.html?error=2'</script>";
    
    }
    
    function LogReg($iduser, $net)
    {
        $datetime =  date("Y-m-d H:i:s");
        date_default_timezone_set('Etc/GMT+6');
        $log = $net->prepare("Insert into session_report values(null, :IdUser, :date)");
        $log->bindParam(':user', $iduser);
        $log->bindParam(':date', $datetime);        
        $log->execute();   
    }  
}
catch (PDOException $e)
{
    echo "Servidor sobresaturado, espere un momento";
}
?>