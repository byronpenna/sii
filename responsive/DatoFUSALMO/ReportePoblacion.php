<?php

    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=ReportePoblación.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';

    
    $ArrayC = $_POST['Institucion'];

?>

<table style=" width: 100%;" rules='all'>
    <tr>
        <th style="width: 30%;" rowspan="2">Centros Escolares</th>
        <th rowspan="2" style="width: 25%;">Secciones</th>
        <th colspan="2">Estudiantes</th>
        <th colspan="2">Deserci&oacute;n</th>
        <th rowspan="2">Total</th>
    </tr>
    <tr><th>M</th><th>F</th><th>M</th><th>F</th></tr>
    <?php
           
           for($i = 0; $i < count($ArrayC) ; $i++)
           {
                if($i == 0)
                    $CentrosE .= " i.IdInstitucion = $ArrayC[$i] ";
                
                else
                    $CentrosE .= " or i.IdInstitucion = $ArrayC[$i] ";
           }                      
           
           $CentrosEscolares = $bdd->prepare("Select ins.* from Educacion_Inscripcion as i
                                              Inner join Institucion as ins on i.IdInstitucion = ins.IdInstitucion
                                              where i.IdCiclo = " . $_SESSION['Ciclos'] . " and ( $CentrosE )
                                              Group by ins.IdInstitucion
                                              ORDER BY ins.NombreInstitucion ASC");

           $CentrosEscolares->execute();
           echo "<tr><td colspan='7'><hr color='white' /></td></tr>";
           
           $ArrayTotal = array(); 
           $ArrayTotal[0] = 0;
           $ArrayTotal[1] = 0;
           $ArrayTotal[2] = 0;
           $ArrayTotal[3] = 0;          
           
           $ArrayData = array();
           $TDesercionF = 0;
           $TDesercionM = 0; 
               
           
           while($DataCE = $CentrosEscolares->fetch())
           {
               
               $AuxTotalSeccion = 0;
               unset($ArrayPoblation);
               $ArrayPoblation = array();    
               
               
               $Secciones = $bdd->prepare("Select s.* from Educacion_Inscripcion as i 
                                           Inner join Institucion_Seccion as s on i.IdSeccion = s.IdSeccion
                                           where i.IdInstitucion = $DataCE[0] and i.IdCiclo = " . $_SESSION['Ciclos'].
                                           " ORDER BY s.SeccionEducativa ASC");
               $Secciones->execute();   
                                           
               echo "<tr><td rowspan='".$Secciones->rowCount()."' style='text-align: center; vertical-align: central;'>".($DataCE[1]) ."</td>";
               
               
               
               while($DataS = $Secciones->fetch())
               {
                    $TotalSección = 0;
                    $i = 0;
                    echo "<td style='padding-left: 10px'>". ( $DataS[2]) ."</td>";
                    
                    /** Total de Participantes **/
                    $Estudiantes = $bdd->prepare("SELECT j.Sexo, COUNT(e.IdUsuario) FROM Educacion_Estudiantes as e
                                                  INNER JOIN joven as j on e.IdUsuario = j.IdUsuario
                                                  INNER JOIN Educacion_Inscripcion as i on e.IdInscripcion = i.IdInscripcion
                                                  where i.IdSeccion = $DataS[0] and i.IdCiclo = " . $_SESSION['Ciclos'] . "
                                                  GROUP by j.Sexo 
                                                  ORDER BY j.Sexo DESC");
                    $Estudiantes->execute();
                    
                    if($Estudiantes->rowCount() > 0)
                    {
                        while($DataE = $Estudiantes->fetch())
                        {
                            echo "<th>$DataE[1]</th>";
                            $TotalSección += $DataE[1];
                            $ArrayTotal[$i] += $DataE[1];
                            $i++;                            
                        }                                                                      
                    }
                    else
                        echo "<th>0</th><th>0</th>";
                        
                    /** Total de Participantes en descersion**/
                    $Desercion = $bdd->prepare("SELECT j.Sexo, COUNT(e.IdUsuario) FROM Educacion_Estudiantes as e 
                                                INNER JOIN Educacion_Desercion as d on e.IdUsuario = d.IdEstudiante  
                                                INNER JOIN joven as j on e.IdUsuario = j.IdUsuario
                                                INNER JOIN Educacion_Inscripcion as i on e.IdInscripcion = i.IdInscripcion
                                                where i.IdSeccion = $DataS[0] and i.IdCiclo = " . $_SESSION['Ciclos'] . "
                                                GROUP by j.Sexo
                                                ORDER BY j.Sexo DESC");
                    $Desercion->execute();
                    
                    if($Desercion->rowCount() > 0)
                    {
                        while($DataD = $Desercion->fetch())
                        {        
                            if($Desercion->rowCount() == 2) 
                            {
                                $i++;
                                $ArrayTotal[$i] += $DataD[1];  
                                
                                echo "<th>$DataD[1]</th>";
                                
                                if($DataD[0] == "Masculino")
                                    $TDesercionM  += $DataD[1];
                                
                                else
                                    $TDesercionF += $DataD[1];                                                                                            
                            }                           
                            else
                            {
                                if($DataD[0] == "Masculino")
                                {
                                    $i++;
                                    $ArrayTotal[$i] += $DataD[1]; 
                                    
                                    $i++;
                                    $ArrayTotal[$i] += 0; 
                                                                        
                                    echo "<th>$DataD[1]</th><th>0</th>";     
                                    $TDesercionM  += $DataD[1];                                                                   
                                }
                                else
                                {
                                    $i++;                                    
                                    $ArrayTotal[$i] += 0;
                                    
                                    $i++;
                                    $ArrayTotal[$i] += $DataD[1];
                                     
                                    echo "<th>0</th><th>$DataD[1] </th>";
                                    $TDesercionF += $DataD[1];                                            
                                }  
                            }   
                            $TotalSección -= $DataD[1];                                                                                                                                         
                        }                                                                                                                       
                    }
                    else                            
                        echo "<th>0</th><th>0</th>";
                                                                                               
                    echo "<th>$TotalSección</td></tr>";
                                                                          
               }

               echo "<tr><td colspan='7'><hr color='white' /></td></tr>"; 
                            
           }
           $total = $ArrayTotal[0] + $ArrayTotal[1] - $TDesercionF - $TDesercionM;
           echo "<tr><th colspan='2'>Totales</th><th>$ArrayTotal[0]</th><th>$ArrayTotal[1]</th><th>$TDesercionM</th><th>$TDesercionF</th><th>$total</th></tr>";
           


    ?>
</table>