<?php

/**
 * @author Manuel
 * @copyright 2013
 */
 
require '../net.php';


if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Agregar")
    {
        if(isset($_POST['IdC']))        
        {
            $agregar = $bdd->prepare("Insert into Educacion_Periodos values(null, :idD, :nombre, :inicio, :fin, 'No Seleccionado')");
            $agregar->bindParam(':idD', $_SESSION['IdDato']);
            $agregar->bindParam(':nombre', $_POST['Nombre']);
            $agregar->bindParam(':inicio', $_POST['inicio']);
            $agregar->bindParam(':fin', $_POST['fin']);
            
            $agregar->execute();
            
            if(isset($_POST['IdC']))
            {            
                $buscar = $bdd->prepare("Select * from Educacion_Periodos where IdDatoFUSALMO = :idD and Nombre = :nombre");
                $buscar->bindParam(':idD', $_SESSION['IdDato']);
                $buscar->bindParam(':nombre', $_POST['Nombre']);
                
                $buscar->execute();
                $idPeriodo = $buscar->fetch();
                
                
                $agregar = $bdd->prepare("Insert into Educacion_CicloPeriodo values(null, :idC, :idP)");
                $agregar->bindParam(':idC', $_POST['IdC']);
                $agregar->bindParam(':idP', $idPeriodo[0]);
                
                $agregar->execute();            
            }
                        
            LogReg($_SESSION["IdUsuario"], "Educación", "Ciclo Agregado, Nombre: " . $_POST['Nombre'] , $bdd);
            Redireccion("../FUSALMO.php?d=cycle&n=3");   
        }
        else
        {
            $agregar = $bdd->prepare("Insert into Educacion_Periodos values(null, :idD, :nombre, :inicio, :fin, 'Seleccionado')");
            $agregar->bindParam(':idD', $_SESSION['IdDato']);
            $agregar->bindParam(':nombre', $_POST['Nombre']);
            $agregar->bindParam(':inicio', $_POST['inicio']);
            $agregar->bindParam(':fin', $_POST['fin']);
            
            $agregar->execute();
            LogReg($_SESSION["IdUsuario"], "Educación", "Ciclo Agregado, Nombre: " . $_POST['Nombre'] , $bdd);    
            Redireccion("../FUSALMO.php?d=Adm&n=1");            
        }
        
        

    }
    else
    Redireccion("../FUSALMO.php");
}

?>