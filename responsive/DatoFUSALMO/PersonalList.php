<?php
     if($_SESSION['IdDato'] != "")
    {   
        
        $Dato = $bdd->prepare("Select * from  DatoFusalmo where IdDatoFusalmo = :id");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->execute();
        
        if($Dato->rowCount() == 1)
        $DatosFUSALMO = $Dato->fetch();
       
        else
            Redireccion("FUSALMO.php");
    }
    else
        Redireccion("FUSALMO.php");
?>   

<div class="Raiz">
<a href="/Main.php" class='flecha' style='text-decoration:none'>Principal</a> ->
<a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
<a href="/FUSALMO.php?d=PrList" class='flecha' style='text-decoration:none'>Personal</a>
</div>
<?php
    
    if(isset($_GET['n']))
    {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Usuario Agregado Exitosamente</div>";
            
        if($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Usuario quitado Exitosamente</div>";  

        if($_GET['n'] == 3)
            echo "<div id='Notificacion' name='Notificacion' style='color:red;'>El Usuario ya estaba registrado anteriormente</div>";                        
    }
?>

<div style="width: 90%; background-color: white; border-radius: 20px; margin-left: 5%; ">
<h1 style="margin-left: 60px;margin-bottom: 10px;">Personal de Proyecto</h1> 
<hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 15px;" />
        <table style="margin-bottom: 20px; float:left; width: 100%; ;text-align: center;">
        <tr><th colspan="4"><h2>Personal asignado a <br /><?php echo $DatosFUSALMO[1] ?></h2></th></tr>
        <tr><td colspan="4" style="height: 10px; background-color: skyblue;"></td></tr>
        <tr><td colspan="3" ><h2>Ususarios Relacionados</h2></td>
            <td style="text-align: center;"><a href="FUSALMO.php?d=Prn" style="text-decoration: none;">
            <input type="button" value="Agregar Usuario" class="boton" style="width: 125px;" /></a></td>
        </tr>
                <?php 
                        $usuarios = $bdd->prepare("Select * , ins.IdInscripcion from usuario as usu 
                                                   Inner Join InscripcionDatoFusalmo as ins on usu.Id = ins.IdUsuario
                                                   where (usu.TipoUsuario = 'Coordinador' or usu.TipoUsuario = 'Gestor')
                                                   and  ins.IdDatoFUSALMO = :id                                                  
                                                   ORDER BY usu.TipoUsuario ASC");
                        $usuarios->bindParam(':id', $_SESSION['IdDato']);
                        $usuarios->execute();
                        
                       
                        
                        if($usuarios->rowCount() >= 1){
                            
                            echo "<tr><td>Id</td><td>Usuario</td><td>Tipo de Usuario </td><td>Acciones</td></tr>";
                            
                            while($datosUsuario = $usuarios->fetch())
                            {
                                echo "<tr>
                                          <td style='text-align: center;'>$datosUsuario[0]</td>
                                          <td style='text-align: center;'>$datosUsuario[1]<br />$datosUsuario[4]</td>
                                          <td style='text-align: center;'>$datosUsuario[2]</td>
                                          <td style='text-align: center;'>
                                              <form action='DatoFUSALMO/PersonalABM.php' method='post'>
                                                    <input type='hidden' name='Id' id='Id' value='$datosUsuario[5]' />
                                                    <input type='submit' value='Quitar' class='boton' name='Enviar' id='Enviar'  />
                                              </form>
                                          </td>
                                      </tr>";
                            }
                        }   
                        else
                        {
                            echo "<tr>
                                      <td colspan='4' style='color:Red; text-align: center;'><h3>No hay Usuarios registrados para este Dato FUSALMO</h3></td>
                                  </tr>";
                        }                          
                ?>          
        </table> 
<div class="clr"></div> 
</div>