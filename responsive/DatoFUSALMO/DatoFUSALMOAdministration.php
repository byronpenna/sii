
<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administraci�n</a> ->
</div>

<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
        
        <div style="float: left; width: 90%; margin-left: 5%; margin-top: 3%;">
<?php           
        if($_SESSION['IdDato'] != "")
        {        
            $Dato = $bdd->prepare("SELECT p.IdPeriodo, p.Nombre, p.FechaInicio, p.FechaFin, f.IdDatoFUSALMO, f.Nombre
                                   FROM  Educacion_Periodos AS p
                                   INNER JOIN DatoFusalmo AS f ON p.IdDatoFUSALMO = f.IdDatoFUSALMO
                                   WHERE f.IdDatoFUSALMO = :id AND p.Status =  'Seleccionado'");
            $Dato->bindParam(':id', $_SESSION['IdDato']);
            $Dato->execute();
            
            if($Dato->rowCount() == 0)
            {   	
?>              <h2 style="color: red;">No hay perdiodos registrados,<br /> crea un nuevo Periodo</h2><br />
                <a style="text-decoration: none;" href="FUSALMO.php?d=Period" ><input type="button" style="width: 150px;" class="boton" value="Crear Periodo" /></a>        
<?php    
            }
            else
            {
                $DatosFUSALMO = $Dato->fetch();                
                $_SESSION['IdPeriodo'] = $DatosFUSALMO[0];                               
                
                  $dto = new DateTime();
                  $semanai = $dto->setISODate( date("Y"), substr($DatosFUSALMO[2],6,2));
                  
                  $dto2 = new DateTime();
                  $semanaf = $dto2->setISODate( date("Y"), substr($DatosFUSALMO[3],6,2));
                  
                if($_SESSION['IdDato'] == 39 || $_SESSION['IdDato'] == 49 || $_SESSION['IdDato'] == 50)
                    $Img = "pijdb.png";
                
                else if($_SESSION['IdDato'] == 43 || $_SESSION['IdDato'] == 57)
                    $Img = "logo_compumovil.JPG";

                else if($_SESSION['IdDato'] == 44 )
                    $Img = "JCF.jpg";                    
                
                else
                    $Img = "Colegio.jpg";
                
                echo "<table style='width: 100%'>
                        <tr>
                            <td>
                                <h2 style='color: Blue;'>Datos del per�odo<br />$DatosFUSALMO[5]</h2> 
                            </td>
                            <td rowspan='2' style='width:50%; text-align: center;'>
                                <img src='images/$Img' style='width: 200px; height: 220px; margin-bottom: 10px; border-radius: 10px;' />
                            </td>
                        </tr>
                        <tr>
                            <td>  
                                <table style='width: 90%'>
                                    <tr><td style='text-align: right; padding-right: 20px;'>Semana actual:</td>
                                        <td>" . date("W") ." del a�o " . date("Y"). "</td></tr>
                                    <tr><td style='text-align: right; padding-right: 20px;'>Periodo:</td>
                                        <td>$DatosFUSALMO[1]</td></tr>
                                    <tr><td style='text-align: right; padding-right: 20px;'>Fecha de inicio:</td>
                                        <td>". $semanai->format('d-m-Y') . "</td></tr>
                                    <tr><td style='text-align: right; padding-right: 20px;'>Fecha de fin:</td>
                                        <td>". $semanaf->format('d-m-Y') . "</td></tr>                                    
                                </table>
                            </td>
                        </tr>";
                
                
                    $Ciclo = $bdd->prepare("SELECT * FROM Educacion_CicloPeriodo as CP 
                                            inner join Educacion_Ciclos as C on CP.IdCiclo = C.IdCiclo 
                                            where IdPeriodo = :id group by C.IdCiclo ");                                           
                    $Ciclo->bindParam(':id', $_SESSION['IdPeriodo']);
                    $Ciclo->execute();
                    
                    if($Ciclo->rowCount() > 0)
                    {
                        $IdCiclo = $Ciclo->fetch();
                        echo "<tr><td></td><td style='text-align: center; color: blue;'>$IdCiclo[5]</td></tr>";                        
                        $_SESSION['Ciclos'] = $IdCiclo[1];
                    }
                    else
                        echo "<tr><td></td><td  style='text-align: center; color: red;'>Error, Verificar el ciclo</td></tr>";                
                
                if(date("W") < substr($DatosFUSALMO[2],6,2) || date("W") > substr($DatosFUSALMO[3],6,2))
                    echo "<tr><td></td><td style='text-align: center; color: red;'>Periodo fuera de rango...</td></tr>";
                
                

                if($_SESSION["TipoUsuario"] == "Coordinador" || $_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["TipoUsuario"] == "Gestor")
                {      
                    echo "  <tr><td colspan='2'><br /><hr color='skyblue' /></td></tr>
                            <tr><td colspan='2' style='padding-bottom: 10px; padding-top: 10px; Color:blue'>Opciones de periodo: </td></tr>
                            <tr><td colspan='2'>
                                    <table style='width: 100%'>
                                        <tr>
                                        <td style='text-align: center; padding-bottom: 10px; padding-top: 10px'>
                                            <a style='text-decoration: none;' href='FUSALMO.php?d=Component'>
                                                <input style='width: 150px;' type='button' class='boton' value='�reas de Aplicaci�n' />
                                            </a>
                                        </td> 
                                        <td style='text-align: center; padding-bottom: 10px; padding-top: 10px'>
                                            <a style='text-decoration: none;' href='FUSALMO.php?d=ReportStudents'>
                                                <input style='width: 150px;' type='button' class='boton' value='Reporte' />
                                            </a>
                                        </td>
                                        <td style='text-align: center; padding-bottom: 10px; padding-top: 10px'>";
                                        
                    if($_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["IdUsuario"] == 2657)
                    {
                        echo "  <a style='text-decoration: none;' href='FUSALMO.php?d=Actions'>
                                    <input style='width: 150px;' type='button' class='botonG' value='Reporte de Acciones' />                                    
                                </a>";
                    }                                        
                                        
                    echo "              </td>
                                        </tr>
                                    </table>
                                </td>                           
                            </tr>
                            <tr style='height: 40px'><td style='background-color: white;'></td></tr>     
                            <tr><td colspan='2'><br /><hr color='skyblue' /></td></tr>";
                            
                    echo "  <tr><td colspan='2' style='padding-bottom: 10px; padding-top: 10px; Color:blue'>Opciones de ciclo: </td></tr>
                            <tr><td colspan='2'>
                                    <table style='width: 100%'>
                                    <tr>";
                                    
                    if($_SESSION["TipoUsuario"] == "Coordinador" || $_SESSION["TipoUsuario"] == "Administrador")
                    {     
                        echo "          <td style='text-align: center; padding-bottom: 10px; padding-top: 10px'>
                                                <a style='text-decoration: none;' href='FUSALMO.php?d=cycle'>
                                                    <input style='width: 150px;' type='button' class='botonPr' value='Ver Ciclos' />
                                                </a>
                                        </td>";
                                        
                    }
                                        
                    echo "              <td style='text-align: center; padding-bottom: 10px; padding-top: 10px'>
                                                <a style='text-decoration: none;' href='FUSALMO.php?d=StatisticsData'>
                                                    <input style='width: 150px;' type='button' class='botonPr' value='Datos Estadisticos' />
                                                </a>
                                        </td>";
                    
                    echo "              <td style='text-align: center; padding-bottom: 10px; padding-top: 10px'>
                                            <a style='text-decoration: none;' href='FUSALMO.php?d=Institution'>
                                                <input style='width: 150px;' type='button' class='botonPr' value='Ver Instituciones' />
                                            </a>
                                        </td>
                                    </tr>
                                    </table>
                                </td>                           
                            </tr>
                            <tr><td colspan='2'><br /><hr color='skyblue' /></td></tr>";
                            
                    echo "  <tr><td colspan='2' style='padding-bottom: 10px; padding-top: 10px; Color:blue'>Buscador de j�venes</td></tr>
                            <tr><td colspan='2'>
                                    <form action='?d=Search' method='post'>
                                    <table style='width: 90%; margin-left: 5%'>
                                        <tr>
                                            <td class='tdleft'>
                                                Escribe los nombres del joven:    
                                            </td>
                                            <td style='padding-bottom: 10px; padding-top: 10px; width: 60%'>
                                                <input type='text' name='nombres' value='' style='width: 250px' />
                                            </td>    
                                        </tr>
                                        <tr>
                                            <td class='tdleft'>
                                                Escribe el primer apellido:    
                                            </td>
                                            <td style='padding-bottom: 10px; padding-top: 10px; width: 60%'>
                                                <input type='text' name='apellido1' value='' style='width: 175px' />    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class='tdleft'>
                                                Escribe el segundo apellido:    
                                            </td>
                                            <td style='padding-bottom: 10px; padding-top: 10px; width: 60%'>
                                                <input type='text' name='apellido2' value='' style='width: 175px' />    
                                            </td>                  
                                        </tr>
                                        <tr>
                                            <td class='tdleft'>
                                                Escribe el Id del joven:    
                                            </td>
                                            <td style='padding-bottom: 10px; padding-top: 10px; width: 60%'>
                                                <input type='text' name='id' value='' style='width: 175px' />    
                                            </td>                  
                                        </tr>                                            
                                        <tr>   
                                            <td class='tdleft'>                                                
                                                Forzar busqueda exacta
                                                <input type='checkbox' name='Exacta' value='true' />
                                            </td>                                                                                     
                                            <td style='text-align: left; padding-bottom: 10px; padding-top: 10px;'>
                                                <input type='submit' name='Enviar' class='boton' value='Buscar' />                                                    
                                            </td>                                                          
                                        </tr>
                                    </table>
                                    </form>";
                                
                                //$Busqueda = $bdd->prepare("");
                                
                                
                                
                    echo "      </td>                           
                            </tr>";                            
                }
                else
                    Redireccion("../FUSALMO.php?n=2000");
                
                echo "</table>";
            }
        }
        else
            Redireccion("../FUSALMO.php?n=1000");
?>     
        </div>
        <div class="clr"></div> 
</div>