<script>
function ValidarNota(form)
{
    var nota = 0;
    for (i=0;i < form.elements.length;i++)
    {
        if(form.elements[i].type == "text")        
        {       
            nota = form.elements[i].value;
                        
            if (!/^([0-9])*[.]?[0-9]*$/.test(nota))
            {
                form.elements[i].focus();
                form.elements[i].style.color = "red";
                return false;    
            }            
            else
            {
                if(nota >= 0 && nota <= 10)
                {
                    form.elements[i].style.color = "green";                     
                } 
                else
                {
                    form.elements[i].focus();
                    form.elements[i].style.color = "red";
                    return false; 
                }                           
            }            
        }            
    }
    return true;    
}
</script>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administraci�n</a> ->
    <a href="/FUSALMO.php?d=Component" class='flecha' style='text-decoration:none'>�rea</a> ->
    <a href="/FUSALMO.php?d=EvaluationIns" class='flecha' style='text-decoration:none'>Evaluaciones</a> ->
    <a href="/FUSALMO.php?d=Evaluation" class='flecha' style='text-decoration:none'>Asignaci�n</a> ->
    <a href="/FUSALMO.php?d=qualification" class='flecha' style='text-decoration:none'>Notas</a>
</div>

<?php

    if($_SESSION['IdDato'] != "" || $_SESSION['Component']!= "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre, c.IdComponente, c.NombreComponente
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               Inner join Educacion_Componente as c on c.IdPeriodo = p.IdPeriodo                             
                               WHERE d.IdDatoFUSALMO = :id and c.IdComponente = :idc");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idc', $_SESSION['Component']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");
    
    
    $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                         FROM  Institucion as inst 
                                         inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                         where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
    
    $DatosInstituciones->bindParam(':idIns', $_SESSION['IdIns']);
    $DatosInstituciones->bindParam(':idSec', $_SESSION['IdSec']);   
    
    $DatosInstituciones->execute();
    $InfoSeccion = $DatosInstituciones->fetch();    
    
    $evaluacion = $bdd->prepare("Select * from Educacion_Evaluaciones where IdEvaluacion = :idE");
    $evaluacion->bindParam(':idE', $_SESSION['IdEva']);
    $evaluacion->execute();
    $eva = $evaluacion->fetch();
        
     if(isset($_GET['n']))
     {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right;'>Evaluaci�n Inscrita Exitosamente</div>";
            
        if($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right;'>Evaluaci�n retirada del Area</div>";                                                      
     }
     
     $AuxNotas= $bdd->prepare("Select Nota from Educacion_Notas where IdEvaluacion = :idEva");
     $AuxNotas->bindParam(':idEva', $_SESSION['IdEva']);
     $AuxNotas->execute();
     
     if($AuxNotas->rowCount() > 0)
     {
        $aviso = "<em style='color: green;'>Notas guardadas</em>";
     }
     else
     {
        $aviso = "<em style='color: red;'>No has guardado las Notas</em>";
     }
     
          
?>

<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style='width: 90%; margin-left: 5%; margin-top: 10px;'>             
        <tr><td colspan="5" style="color: blue;">Evaluaciones del Area<br /><?php echo "$DatoView[1]<br />$DatoView[3]";?> </td></tr>
        <tr><td colspan="5" style="color: green; text-align: right;"><?php echo "$InfoSeccion[1]<br />$InfoSeccion[3]";?> </td></tr>
        <tr><td colspan="5" style="height: 5px; background-color: skyblue;"></td></tr>
        <tr><td colspan="5" style="height: 20px; background-color: white;"></td></tr>        
        <tr><td><?php echo $aviso;?></td><td colspan="4" style="color: blue; text-align: right;" >Area: <?php echo $DatoView[5];?> </td></tr> 
        <tr><td style="color: blue; text-align: left;">Evaluaci�n:   </td> <td style="color: blue;"><?php echo $eva[3];?>     </td></tr>
        <tr><td style="color: blue; text-align: left;">Ponderaci�n:  </td> <td style="color: blue;"><?php echo $eva[4].'%';?> </td></tr>
        
        <tr><td style="width: 15%; text-align: center;">N� de Lista</td><td>Nombre del Estudiante</td><td style="width: 10%;text-align: center;">Nota</td></tr>
        <form name="evaluacion" method="post" action="DatoFUSALMO/NotasABM.php" name="form1" id="form1" >
        
        <?php
               $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2, ins.IdInscripcion
                                              FROM Educacion_Estudiantes  as est
                                              inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                              INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                              where ins.IdCiclo = :idC and ins.IdInstitucion = :idI and ins.IdSeccion = :idS 
                                              ORDER BY  jov.Apellido1 ASC, jov.Apellido2 ASC ");
                                              
               $listaAlumnos->bindParam(':idC', $_SESSION['Ciclos']);
               $listaAlumnos->bindParam(':idI', $_SESSION['IdIns']);
               $listaAlumnos->bindParam(':idS', $_SESSION['IdSec']);
               $listaAlumnos->execute();
               
               $i = 0;
               
               if($listaAlumnos->rowCount() > 0)
               {
                   while($alumno = $listaAlumnos->fetch())
                   {                        
                        $nota = $bdd->prepare("Select Nota from Educacion_Notas where IdEvaluacion = :idEva and IdUsuario = :idUsu");
                        $nota->bindParam(':idEva', $_SESSION['IdEva']);
                        $nota->bindParam(':idUsu', $alumno[0]);
                        $nota->execute();                        
                        $n = $nota->fetch();
                                                             
                        $Desercion = $bdd->prepare("Select * from Educacion_Desercion where IdPeriodo = :idP and IdEstudiante = :idU");
                        $Desercion->bindParam(':idP', $_SESSION['IdPeriodo']);
                        $Desercion->bindParam(':idU', $alumno[0]);
                        $Desercion->execute();                                             
                        
                        $i++;
                        echo "<tr><td style='text-align: center;'>$i</td>
                                  <td style='text-align: left;'> $alumno[2] $alumno[3], $alumno[1]</td>";
                        
                        if($Desercion->rowCount() == 0)   
                            echo "<td style='text-align: center;'>
                                        <input type='hidden' name='id[]' value='$alumno[0]'  />
                                        <input type='text' name='nota[]' value='$n[0]' style='width: 40px;' required='true' title='Nota requerida' />
                                  </td>";
                        
                        else
                            echo "<td style='text-align: center; color: red;'>-</td>";
                        
                        echo "</tr>";
                   }
               }
               else
                    echo "<tr><td colspan='4' style='color:red; text-align: center;'>No hay Alumnos registrados a esta Secci�n</td></tr>";

               echo "<tr><td colspan='3' style='text-align: center;'><input class='boton' type='submit' name='Enviar' value='Guardar Notas' onclick='return ValidarNota(this.form);' /></td></tr>";
        ?>
        </form>            
        
</table>
</div>