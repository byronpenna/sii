<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administración</a> ->
    <a href="/FUSALMO.php?d=Actions" class='flecha' style='text-decoration:none'>Reporte de Acciones</a>
</div>
<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style='width: 90%; margin-left: 5%; margin-top: 10px;'>
<?php    
    $Dato = $bdd->prepare("SELECT * FROM DatoFusalmo WHERE IdDatoFUSALMO = :id");
    $Dato->bindParam(':id', $_SESSION['IdDato']);
    $Dato->execute();
    
    $DatoView = $Dato->fetch();
?>
<tr><td style="color: blue;">Acciones de Usuarios - <?php echo $DatoView[1];?></td></tr>
<tr><td><hr color='skyblue' /></td></tr>
<?php
    $Query = "SELECT l.IdUsuario FROM Log_Report as l
              Inner join Educacion_AsignacionEducador as a on l.IdUsuario = a.IdUsuario 
              WHERE a.IdCiclo = ".$_SESSION['Ciclos']."
              GROUP BY l.IdUsuario
              ORDER BY IdLog DESC";

    $Log = $bdd->prepare($Query);
    $Log->execute();    
   	
    $ArrayU = array();
    
    while($DataU = $Log->fetch())
        array_push($ArrayU, $DataU[0]);
?>
<tr>
    <td colspan="2">
        <form action="?d=Actions" method="post">
        <table style="width: 90%; margin-left: 5%;">
            <tr>                
                <td style="width: 30%;">Usuarios:</td>
                <td style="width: 70%;">
                    <select name="iu" id="iu" style="width: 300px;">  
                        <option value="All" > Todos los Usuarios de este Módulo</option>
                        <?php
                               foreach($ArrayU as $value)
                               {
                                    $Accion = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2 FROM Empleado AS e
                                                              where e.IdEmpleado = $value");                                            
                                    $Accion->execute();
                                    $Datos = $Accion->fetch();
                                    
                                    if($_POST['iu'] == $value || $_SESSION['AuxLog'] == $value)
                                        echo "<option value='$value' selected='true'>$Datos[0] $Datos[1] $Datos[2] $Datos[3]</option>";
                                    
                                    else
                                        echo "<option value='$value' >$Datos[0] $Datos[1] $Datos[2] $Datos[3]</option>";
                               }	                               
                        ?>
                    </select>
                </td>                           
            </tr>
            <tr><td colspan="2" style="text-align: center;"><input type="submit" class="boton" name="Enviar" value="Ver Acciones" /></td></tr>
        </table>
        </form> 
    </td>
</tr>
<tr><td><hr color='skyblue' /></td></tr>
<tr>
    <td>
        <table style="width: 100%;" rules="All">
        <tr><td colspan="3"><h2>Acciones realizadas por los Usuarios</h2></td></tr>
        <tr>
            <th style="width: 25%; padding: 5px;">Usuario</th>
            <th style="padding: 5px;">Acciones</th>
            <th style="width: 15%;padding: 5px;" >Fecha y Hora</th>
        </tr>
        <?php
        
        if(!isset($_GET['pag'])) 
    	   $pag = 1;	
        	
    	else 
    	   $pag = $_GET['pag'];
            
       	$hasta = 10;
    	$desde = ($hasta * $pag) - $hasta;
        
        
        if($_SESSION['AuxLog'] != "" || isset($_POST['iu']))
        {   
            if($_POST['iu'] == "All")
            {
                $aux = true;  // Utilizo el valor de Auxlog
                $_SESSION['AuxLog'] = "";                        
            }
            else
            {
                $aux = false;  // Utilizo el valor de Auxlog
                if(isset($_POST['iu']))
                    $_SESSION['AuxLog'] = $_POST['iu'];                    
            }                        
        }     
        else        
            $aux = true; // Muestro todo
           

        if($aux)
        { 
        
            $Query = "SELECT * FROM Log_Report as l
                      Inner join Educacion_AsignacionEducador as a on l.IdUsuario = a.IdUsuario 
                      WHERE a.IdCiclo = ".$_SESSION['Ciclos']."
                      GROUP by IdLog
                      ORDER BY IdLog DESC";
            
            $resultado = $Query . " LIMIT $desde, $hasta";
                
            $LogList = $bdd->prepare($resultado);
            $LogList->execute();  
             
            $paginado = $bdd->prepare($Query);
    	    $paginado->execute();
            
            while($DataL = $LogList->fetch())
            {
                $Accion = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2 FROM Empleado AS e
                                          where e.IdEmpleado = $DataL[1]");                                            
                $Accion->execute();
                $Datos = $Accion->fetch();
                
                echo "<tr><th style='padding: 5px;'>$Datos[0] $Datos[1]<br />$Datos[2] $Datos[3]</th><td style='padding: 5px;'>$DataL[3]</td><th style='padding: 5px;'>$DataL[4]</th></tr>";
                
            } 

        }
        else
        {            
            
            $Query = "SELECT * FROM Log_Report as l
                      Inner join Educacion_AsignacionEducador as a on l.IdUsuario = a.IdUsuario 
                      WHERE a.IdCiclo = ".$_SESSION['Ciclos']." and l.IdUsuario = ". $_SESSION['AuxLog'] ."
                      GROUP by IdLog
                      ORDER BY IdLog DESC";
            
            $resultado = $Query . " LIMIT $desde, $hasta";
                
            $LogList = $bdd->prepare($resultado);
            $LogList->execute();  
             
            $paginado = $bdd->prepare($Query);
    	    $paginado->execute();
            
            while($DataL = $LogList->fetch())
            {
                $Accion = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2 FROM Empleado AS e
                                          where e.IdEmpleado = $DataL[1]");                                            
                $Accion->execute();
                $Datos = $Accion->fetch();
                
                echo "<tr><th style='padding: 5px;'>$Datos[0] $Datos[1]<br />$Datos[2] $Datos[3]</th><td style='padding: 5px;'>$DataL[3]</td><th style='padding: 5px;'>$DataL[4]</th></tr>";
                
            }             
        }        
        
        
        $paginas = ceil($paginado->rowCount() / $hasta);
    
        $direccion = "FUSALMO.php?d=Actions";
        echo     "<tr><td colspan='3' style='text-align:center;'>
                            <table style='width: 50%; margin-left:25%; text-align: center;'>
                            <tr><th colspan='3'>Página [$pag] de [$paginas]</th></tr>";
                      
        if ($pag > 1)
            echo "<tr><td style='width: 33%;'><a style='text-decoration:none' href=\"$direccion&pag=1\"> <<- </a> | <a style='text-decoration:none' href=\"$direccion&pag=" . ($pag -1) . "\"> <- </a> | </td>"; 
        else
            echo "<tr><td style='width: 33%;'></td>";
                    
            echo "<td style='width: 34%;'><a style='text-decoration:none' href=\"$direccion\">Principal</a></td>";
    
        if ($pag < $paginas)
            echo "<td style='width: 33%;'> |<a style='text-decoration:none' href=\"$direccion&pag=" . ($pag + 1) . "\"> -> </a> | <a style='text-decoration:none' href=\"$direccion&pag=" . ($paginas) ."\"> ->> </a> </td>";    
        else
            echo "<td style='width: 33%;'></td></tr>";               
    
        echo "</table>
              </td>
              </tr>  ";
        
        ?>
        </table>
    </td>
</tr>
</table>
</div>