<?php

/**
 * @author Manuel
 * @copyright 2013
 */
require '../net.php';

if(isset($_POST['Enviar']))
{    
    if($_POST['Enviar'] == "Limpiar Asistencia")
    {
        $Eliminar = $bdd->prepare("Delete from Educacion_Asistencias 
                                       where IdComponente = :IdCom and IdPeriodo = :IdPer and IdSeccion = :IdSec and Fecha = :fecha");
                                       
        $Eliminar->bindParam(':IdCom', $_SESSION['Component']);
        $Eliminar->bindParam(':IdPer', $_SESSION['IdPeriodo']);
        $Eliminar->bindParam(':IdSec', $_SESSION['IdSec']);
        $Eliminar->bindParam(':fecha', $_POST['Fecha']);
        $Eliminar->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educación", "Limpieza de Asistencia; Fecha: " . $_POST['Fecha'] . " , Componente: " . $_SESSION['Component'], $bdd);
        Redireccion("../FUSALMO.php?d=AttendanceList&n=2");
    }  
    
    if($_POST['Enviar'] == "Ver")
    {
        $_SESSION['fecha'] = $_POST['fecha'];
        Redireccion("../FUSALMO.php?d=Attendance");
    }  
    
    if($_POST['Enviar'] == "Guardar")
    {

        $Eliminar = $bdd->prepare("Delete from Educacion_Asistencias 
                                       where IdComponente = :IdCom and IdPeriodo = :IdPer and IdSeccion = :IdSec and Fecha = :fecha");
                                       
        $Eliminar->bindParam(':IdCom', $_SESSION['Component']);
        $Eliminar->bindParam(':IdPer', $_SESSION['IdPeriodo']);
        $Eliminar->bindParam(':IdSec', $_SESSION['IdSec']);
        $Eliminar->bindParam(':fecha', $_POST['fecha']);
        $Eliminar->execute();
                
        
        $asis = isset($_POST['asistencia']) ? $_POST['asistencia'] : array();        

        for ($index = 0 ; $index < count($asis); $index ++) 
        {                    
          $InsertarNota = $bdd->prepare("Insert into Educacion_Asistencias values(null, :IdCom, :IdPer, :IdSec, :IdEst, :Fecha)");
          $InsertarNota->bindParam(':IdCom', $_SESSION['Component']);
          $InsertarNota->bindParam(':IdPer', $_SESSION['IdPeriodo']);
          $InsertarNota->bindParam(':IdSec', $_SESSION['IdSec']);
          $InsertarNota->bindParam(':IdEst', $asis[$index]);
          $InsertarNota->bindParam(':Fecha', $_POST['fecha']);        
          $InsertarNota->execute();                    
        }
        
        
        LogReg($_SESSION["IdUsuario"], "Educación", "Guardar de Asistencia; Fecha: " . $_POST['fecha'] . " , Componente: " . $_SESSION['Component'], $bdd);
        Redireccion("../FUSALMO.php?d=AttendanceList&n=3");
    }
    
    if($_POST['Enviar'] == "Cambiar Fecha")
    {

        $Update = $bdd->prepare("Update Educacion_Asistencias set  Fecha = :NuevaFecha
                                   where IdComponente = :IdCom and IdPeriodo = :IdPer and IdSeccion = :IdSec and Fecha = :fecha");
                                       
        $Update->bindParam(':NuevaFecha', $_POST['FechaUpdate']);
        $Update->bindParam(':IdCom', $_SESSION['Component']);
        $Update->bindParam(':IdPer', $_SESSION['IdPeriodo']);
        $Update->bindParam(':IdSec', $_SESSION['IdSec']);
        $Update->bindParam(':fecha', $_POST['Fecha']);
        $Update->execute();
        
        LogReg($_SESSION["IdUsuario"], "Educación", "Cambio de Fecha de Asistencia; Fecha Nueva: " . $_POST['Fecha'] . " , Componente: " . $_SESSION['Component'], $bdd);
        Redireccion("../FUSALMO.php?d=AttendanceList&n=30");
    }    
             
}

?>

