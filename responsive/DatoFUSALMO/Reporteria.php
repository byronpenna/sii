<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function(){
   $("#IdIns").change(function(){cargar_secciones(this.form);});
   $("#IdSec").attr("disabled",true);
});

function cargar_secciones(form)
{
    var Secciones = document.getElementById("IdSec");
    var opciones = form.IdSec.options;
    
    Secciones.options.length=0;
    
    var code = $("#IdIns").val();    	
    
    if(code != "..."){
    	$.get("DatoFUSALMO/EvaluacionSecciones.php", { code: code },
    		function(resultado)
    		{
    			if(resultado == false)
    			{
    				alert("Error");
    			}
    			else
    			{	    
    				$("#IdSec").attr("disabled",false);
    				document.getElementById("IdSec").options.length=0;
    				$('#IdSec').append(resultado);	
    			}
    		}
    	);
    }
    else
    {                    
        var seleccionar = new Option("...","...","","");
        Secciones[0] = seleccionar;
    }
}
</script>
<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administración</a> ->
    <a href="/FUSALMO.php?d=ReportStudents" class='flecha' style='text-decoration:none'>Reporteria</a> 
</div>
<?php

    if($_SESSION['IdDato'] != "" || $_SESSION['Component']!= "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre, c.IdComponente, c.NombreComponente
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               Inner join Educacion_Componente as c on c.IdPeriodo = p.IdPeriodo                             
                               WHERE d.IdDatoFUSALMO = :id and c.IdComponente = :idc");
                               
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idc', $_SESSION['Component']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
        Redireccion("../FUSALMO.php");

     if(isset($_GET['n']))
     {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right;'>Evaluación Inscrita Exitosamente</div>";
            
        if($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right;'>Evaluación retirada del Area</div>";                                
     }
?>
<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
    <form id="Form" method="post" action="?d=ReportStudentsDetails" target="_blank">
    <br /><div style="color: blue; margin-left: 25px;">Reporteria</div><hr color='skyblue' />
    <table style="width: 94%; margin-left: 3%;">        
        <tr><td>Seleccion de &Aacute;reas:</td></tr>
        <tr>
            <td>
                <table style="text-align: left; width: 100%;">
                <?php                      
                      
                      $Query = "Select * from Educacion_Componente where IdPeriodo = :IdP";
	                  $Componentes = $bdd->prepare($Query);
                      $Componentes->bindParam(':IdP', $_SESSION['IdPeriodo']);
                      $Componentes->execute();
                      
                      $td = 1;
                      while($DataComponente = $Componentes->fetch())
                      { 
                        if($td == 1)
                        {
                            echo "<tr><td style='width: 50%;'><input type='checkbox' name='Componente[]' value='$DataComponente[0]' style='margin-right: 15px' />$DataComponente[2]</td>";
                            $td = 0;
                        }
                        else
                        {
                            echo "<td><input type='checkbox' name='Componente[]' value='$DataComponente[0]' style='margin-right: 15px' />$DataComponente[2]</td></tr>";
                            $td = 1;
                        }
                      }
                ?>                    
                </table><br />
            </td>
        </tr>
        <tr><td>Selección de Visualización:</td></tr>
        <tr>
            <td>
                <table style="text-align: left; margin-left: 10%; width: 80%;">
                    <tr>
                        <td style="width: 50%;"><input type="radio" name="TipoV" value="Evaluaciones" style='margin-right: 15px' checked="checked"  />Evaluaciones</td>
                        <td><input type="radio" name="TipoV" value="Asistencias"  style='margin-right: 15px' />Asistencias</td>                        
                    </tr>
                </table><br />        
            </td>
        </tr>
        
        <tr><td style="text-align: left; padding-right: 20px; width: 40%;">Seleccione la Institucion:</td></tr>
        <tr>
            <td style="text-align: left; padding-left: 20%;">
                <select id="IdIns" name="IdIns" style="width: 260px;">  
                <option value='...' style='color:red'>...</option>                
                <?php
	                   $DatosInstituciones = $bdd->prepare("SELECT ins . * FROM  Educacion_Inscripcion AS edu_ins
                                                            INNER JOIN Institucion AS ins ON edu_ins.IdInstitucion = ins.IdInstitucion
                                                            where edu_ins.IdCiclo = :idP GROUP BY edu_ins.IdInstitucion ");

                       $DatosInstituciones->bindParam(':idP',$_SESSION["Ciclos"]);
                       $DatosInstituciones->execute();
                       
                       if($DatosInstituciones->rowCount() > 0)
                       {
                           while($Instituciones = $DatosInstituciones->fetch())                           
                                echo "<option value='$Instituciones[0]'>$Instituciones[1]</option>";
                       }
                       else
                            echo "<option value='Error' style='color:red'>No hay instituciones Secciones registradas a este periodo.</option>";
                       
                ?>
                </select><br /><br />
            </td>
        </tr>
        <tr><td style="text-align: left; padding-right: 20px; width: 40%;">Seleccione la Sección:</td></tr>
        <tr>
            <td style="text-align: left; padding-left: 20%;">
                <select id="IdSec" name="IdSec" style="width: 260px;">
                <option value='...' style='color:red'>Esperando datos</option>
                </select><br /><br />
            </td>
        </tr> 
        <tr><td style="text-align: center;"><input type="submit" name="Enviar" value="Generar" class="boton" onclick="return Validar();" /></td></tr>
    </table>
    </form>
</div>
<script>
function Validar()
{
    
    var sec = document.getElementById('IdSec').value;
    
    if(sec == '...')
        return false;
        
    else
        return true;
}
</script>