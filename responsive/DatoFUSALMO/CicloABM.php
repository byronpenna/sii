<?php

/**
 * @author Manuel
 * @copyright 2013
 */

require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Crear")
    {
        $a�o = date("Y");
        
        $agregar = $bdd->prepare("Insert into Educacion_Ciclos values(null, :IdDf, :Nombre, :Year)");
        $agregar->bindParam(':IdDf', $_SESSION['IdDato']);
        $agregar->bindParam(':Nombre', $_POST['Nombre']);
        $agregar->bindParam(':Year', $a�o);        
        $agregar->execute();
        
        if(isset($_POST['primer']))
        {
            $buscar = $bdd->prepare("SELECT * FROM Educacion_Ciclos 
                                     where IdDatoFusalmo = :IdDf and Ciclo = :Nombre and Tiempo = :Year");
                                     
            $buscar->bindParam(':IdDf', $_SESSION['IdDato']);
            $buscar->bindParam(':Nombre', $_POST['Nombre']);            
            $buscar->bindParam(':Year', $a�o);        
            $buscar->execute();
            
            $resultado = $buscar->fetch();
        
        
            $agregarre = $bdd->prepare("Insert into Educacion_CicloPeriodo values(null, :idC, :idP)");
            $agregarre->bindParam(':idC', $resultado[0]);
            $agregarre->bindParam(':idP', $_SESSION['IdPeriodo']);
            $agregarre->execute();                    
        }        
        
        LogReg($_SESSION["IdUsuario"], "Educaci�n", "Creaci�n de Ciclo " . $_POST['Nombre'], $bdd);
        Redireccion("../FUSALMO.php?d=cycle&n=1");
    }
    
    if($_POST['Enviar'] == "X")
    {
        if(isset($_POST['IdC']))
        {
            $buscar = $bdd->prepare("SELECT * FROM  Educacion_CicloPeriodo  as CP
                                     Inner join Educacion_Periodos as P on CP.IdPeriodo = P.IdPeriodo
                                     where CP.IdCiclo = :IdC");
                                     
            $buscar->bindParam(':IdC', $_POST['IdC']);  
            $buscar->execute();
            
            while($Periodo = $buscar->fetch())
            {
                $eliminarPeriodo = $bdd->prepare("Delete FROM Educacion_Periodos where IdPeriodo = :IdP ");
                                     
                $eliminarPeriodo->bindParam(':IdP', $Periodo[3]);  
                $eliminarPeriodo->execute();   
            
                echo $Periodo[3] . $_POST['IdC'];
            }
            
            $eliminar = $bdd->prepare("Delete FROM Educacion_Ciclos where IdCiclo = :IdC ");
                                     
            $eliminar->bindParam(':IdC', $_POST['IdC']);  
            $eliminar->execute();            
            
            Redireccion("../FUSALMO.php?d=cycle&n=2");
        }
    }
    
    if($_POST['Enviar'] == "Nuevo")
    {            
            $_SESSION['IdCiclo'] = $_POST['IdC'];
            Redireccion("../FUSALMO.php?d=Period");        
    }  

    if($_POST['Enviar'] == "Elegir")
    {            

        $actualizar = $bdd->prepare("Update Educacion_Periodos set Status =  'No Seleccionado' where IdPeriodo = :IdP and IdDatoFUSALMO = :IdF");
        $actualizar->bindParam(':IdP', $_SESSION['IdPeriodo']);
        $actualizar->bindParam(':IdF', $_SESSION['IdDato']); 
        $actualizar->execute();

        $actualizar = $bdd->prepare("Update Educacion_Periodos set Status =  'Seleccionado' where IdPeriodo = :IdP and IdDatoFUSALMO = :IdF");
        $actualizar->bindParam(':IdP', $_POST['IdP']); 
        $actualizar->bindParam(':IdF', $_SESSION['IdDato']);
        $actualizar->execute();

        $_SESSION['IdPeriodo'] = $_POST['IdP'];
        $_SESSION['Ciclos'] = $_POST['IdC'];
        
        Redireccion("../FUSALMO.php?d=cycle&n=4");        
    }  
    
    if($_POST['Enviar'] == "Eliminar")
    {            
        if(isset($_POST['IdP']))
        {
            $buscar = $bdd->prepare("Delete FROM Educacion_Periodos where IdPeriodo = :IdP ");                                     
            $buscar->bindParam(':IdP', $_POST['IdP']);  
            $buscar->execute();
            
            LogReg($_SESSION["IdUsuario"], "Educaci�n", "Eliminaci�n de Ciclo Id:" . $_POST['IdP'], $bdd);
            Redireccion("../FUSALMO.php?d=cycle&n=5");
        }   
    }        
}
Redireccion("../FUSALMO.php?d=cycle");
?>