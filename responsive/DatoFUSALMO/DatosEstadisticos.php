<script>
function CargarTodo()
{                    
    var inputs = document.getElementsByTagName("input"); //or document.forms[0].elements;
    var cbs = []; //will contain all checkboxes
    var checked = []; //will contain all checked checkboxes
    for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].type == "checkbox") {
        cbs.push(inputs[i]);
        
        if (inputs[i].checked) {
          checked.push(inputs[i].value);
        }
        
      }
    }


    document.getElementById('Loader').innerHTML = "<center><em style='color: green;'>Cargando datos...</em></center>";
    $.get("DatoFUSALMO/DatosEstadisticosCount.php", { "checked[]": checked },
  		function(resultado)
  		{
            document.getElementById('Loader').innerHTML = "";                        
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#Loader').append(resultado);	            			
  		}
   	);        
}

function Promedios()
{                    
    var inputs = document.getElementsByTagName("input"); //or document.forms[0].elements;
    var cbs = []; //will contain all checkboxes
    var checked = []; //will contain all checked checkboxes
    for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].type == "checkbox") {
        cbs.push(inputs[i]);
        
        if (inputs[i].checked) {
          checked.push(inputs[i].value);
        }
        
      }
    }
    
    if(checked.length == 1)
    {   
        var Periodo = document.getElementById('periodo').value;
        document.getElementById('Loader').innerHTML = "<center><em style='color: green;'>Cargando datos...</em></center>";
        $.get("DatoFUSALMO/DatosEstadisticosPromedios.php", { "checked[]": checked , "Periodo": Periodo},
        	function(resultado)
        	{
                document.getElementById('Loader').innerHTML = "";                        
        		if(resultado == false)     			
        			alert("Error");
        		
        		else            			        			    
        			$('#Loader').append(resultado);	            			
        	}
        );
    }
    else
    {
        alert("Debes seleccionar solo un centro escolar");
    }        
}
</script>
<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administración</a> ->
    <a href="/FUSALMO.php?d=StatisticsData" class='flecha' style='text-decoration:none'>Datos Estadisticos</a>
</div>
<?php
        $DatoFUSALMO = $bdd->prepare("Select df.Nombre, c.Ciclo from DatoFusalmo as df
                                      Inner join Educacion_Ciclos as c on df.IdDatoFUSALMO = c.IdDatoFUSALMO
                                      where c.IdCiclo = :id");
        $DatoFUSALMO->bindParam(':id', $_SESSION['Ciclos']);
        $DatoFUSALMO->execute();
        $Data = $DatoFUSALMO->fetch();

?>
<div style="width: 100%; border-radius: 10px; border-color: skyblue; border-style: groove; padding: 5px; background-color: white; ">    
    <table style="width: 100%; ">
        <tr><td colspan="2"><h2>Datos Estadisticos</h2><hr color='skyblue' /></td></tr>
        <tr><td class="tdleft" style="width:  30%;">Dato FUSALMO:</td><td> <?php echo $Data[0];?></td></tr>
        <tr><td class="tdleft" style="width:  30%;">Ciclo:</td><td> <?php echo $Data[1];?></td></tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td class="tdleft" style="width:  30%;">Periodos:</td>
            <td>
                <select style="width: 300px;" id="periodo">                    
<?php
                $Trimestres = $bdd->prepare("Select dp.* from Educacion_Periodos as dp
                                             Inner join Educacion_CicloPeriodo as c on dp.IdPeriodo = c.IdPeriodo
                                             where c.IdCiclo = :id");
                $Trimestres->bindParam(':id', $_SESSION['Ciclos']);
                $Trimestres->execute();
                
                while($DataT = $Trimestres->fetch())
                {
                    if($DataT[0] == $_SESSION['IdPeriodo'])
                        $Select = "Selected='true'";
                    else
                        $Select = "";
                        
                    echo "<option value='$DataT[0]' $Select>$DataT[2]</option>";
                }                        
?>                    
                </select>
            </td>
        </tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td colspan="2" style="padding-left: 10%;">Instituciones:</td>
        <tr>        
            <td colspan="2">   
                <table style="width: 80%; margin-left: 10%;">     
<?php     
                                                   
                $CentrosEscolares = $bdd->prepare("Select ins.* from Educacion_Inscripcion as i
                                                   Inner join Institucion as ins on i.IdInstitucion = ins.IdInstitucion
                                                   where i.IdCiclo = " . $_SESSION['Ciclos'] . " 
                                                   Group by ins.IdInstitucion
                                                   ORDER BY ins.NombreInstitucion ASC");

                $CentrosEscolares->execute();
                
                $a = 0;
                while($DataC = $CentrosEscolares->fetch())        
                {
                    if($a == 0)
                    {
                        echo "<tr><td><input name='institucion' type='checkbox' value='$DataC[0]'>$DataC[1]</td>";
                        $a = 1;    
                    }
                    else
                    {
                        echo "<td><input name='institucion' type='checkbox' value='$DataC[0]'>$DataC[1]</ td></tr>";
                        $a = 0;    
                    }
                }
?>                    
                </table>
            </td>
        </tr>        
        <tr>
            <td colspan="2">
                <hr color='skyblue' />
                    <table style="width: 80%; margin-left: 10%;">
                        <tr>
                            <td><input type="button" class="boton" name="CargarTodo" onclick="CargarTodo()" value="Ver Población" /></td>
                            <td><input type="button" class="boton" name="Promedios" onclick="Promedios()" value="Ver Promedios" /></td>
                        </tr>                    
                    </table>
                <hr color='skyblue' />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="Loader">

                </div>
            </td>
        </tr>         
    </table>
</div>
