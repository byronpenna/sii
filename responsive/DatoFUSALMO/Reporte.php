<?php

/**
 * @author Manuel
 * @copyright 2013
 */
 
    $nombre = $_POST['name'];
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=" . $nombre .".xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';

	if($_SESSION['IdDato'] != "")
    {
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre 
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               WHERE d.IdDatoFUSALMO = :idD and p.IdPeriodo = :idP");
                               
        $Dato->bindParam(':idD', $_SESSION['IdDato']);
        $Dato->bindParam(':idP', $_SESSION['IdPeriodo']);
        $Dato->execute();

        $DatoView = $Dato->fetch();

        $idIns = $_SESSION["Institucion"]; 
        $idSec = $_SESSION["Seccion"];
        $inscr = $_SESSION["IdInscript"];

        $DatosInstitucion = $bdd->prepare("SELECT * FROM Educacion_Inscripcion as ins
                                           inner join Institucion_Seccion as sec on ins.IdInstitucion = sec.IdInstitucion
                                           inner join Institucion as inst on inst.IdInstitucion = ins.IdInstitucion 
                                           where inst.IdInstitucion = $idIns  and ins.IdInscripcion = $inscr and sec.IdSeccion = $idSec");
        $DatosInstitucion->execute();
        $Ins = $DatosInstitucion->fetch();
        
        if(isset($_GET['n']))
        {
            if($_GET['n'] == 1)
                echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Estudiante fuera del grupo</div>";                           
        }
    }
    else
            Redireccion("../FUSALMO.php");    
?>
<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style=" width: 90%; margin-left: 5%; margin-top: 20px;">
<tr><td colspan="3" style="color: blue;">Listado de Estudiantes</td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $DatoView[1];?></td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $DatoView[3];?></td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $Ins[8];?></td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $Ins[6];?></td></tr>        
        <tr><td colspan="3" style="height: 20px; background-color: white; text-align: right;"></td></tr>
        <tr><td colspan="3" style="height: 20px; background-color: white;"></td></tr>  
        <tr><td colspan="3" style=" background-color: skyblue; height: 3px;"></td></tr>
        <tr><th>Id</th><th>Nombre</th><th>Primer Apellido</th> <th>Segundo Apellido</th><th></th></tr>                                              
        <?php
                    $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2 FROM Educacion_Estudiantes  as est
                                                   INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario                                                          
                                                   where est.IdInscripcion = $inscr");
                    $listaAlumnos->execute();
                    
                    if($listaAlumnos->rowCount() > 0)
                        while($alumno = $listaAlumnos->fetch())
                            echo "<tr><td style='text-align: center;'>$alumno[0]</td>
                                      <td style='text-align: center;'>$alumno[1]</td>
                                      <td style='text-align: center;'>$alumno[2]</td>
                                      <td style='text-align: center;'>$alumno[3]</td>
                                  </tr>";
                    else
                        echo "<tr><td colspan='4' style='color:red; text-align: center;'>No hay Alumnos registrados a esta Secci�n</td></tr>";
        ?>                                
        </td>
    </tr>
</table>
</div>