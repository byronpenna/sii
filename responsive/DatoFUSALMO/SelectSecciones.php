<?php

/**
 * @author Gilbetor
 * @copyright 2014
 */

   require '../net.php';

   $DatosInstituciones = $bdd->prepare("SELECT s.* FROM Institucion_Seccion as s 
                                        INNER JOIN Educacion_Inscripcion as ins on ins.IdSeccion = s.IdSeccion 
                                        where ins.IdCiclo = :idcic and ins.IdInstitucion = :idIns 
                                        ORDER BY SeccionEducativa ASC");
                                        
   $DatosInstituciones->bindParam(':idcic', $_GET["code2"]);
   $DatosInstituciones->bindParam(':idIns', $_GET["code"]);   
   $DatosInstituciones->execute();   

   if($DatosInstituciones->rowCount() > 0)
   {
       while($Instituciones = $DatosInstituciones->fetch())
       {       
            echo "<option value='$Instituciones[0]'>" . utf8_encode($Instituciones[2]) . "</option>";
       }
   }
   else
        echo "<option value='Error' style='color:red'> " . $_GET["code"] ." - " . $_GET["code2"] ." No hay instituciones Secciones registradas a este periodo.</option>";
?>
