<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
require '../net.php';

$dia = $_GET["code"];
$mes = $_GET["code2"];
$a�o = $_GET["code3"];
$fecha = "$a�o-$mes-$dia";

if($_SESSION['IdDato'] != "" || $_SESSION['Component']!= "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre, c.IdComponente, c.NombreComponente
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               Inner join Educacion_Componente as c on c.IdPeriodo = p.IdPeriodo                             
                               WHERE d.IdDatoFUSALMO = :id and c.IdComponente = :idc");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idc', $_SESSION['Component']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");
    
    
    $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                         FROM  Institucion as inst 
                                         inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                         where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
    
    $DatosInstituciones->bindParam(':idIns', $_SESSION['IdIns']);
    $DatosInstituciones->bindParam(':idSec', $_SESSION['IdSec']);   
    
    $DatosInstituciones->execute();
    $InfoSeccion = $DatosInstituciones->fetch();    
    
    $evaluacion = $bdd->prepare("Select * from Educacion_Asistencias where IdAsistencia = :idAsis");
    $evaluacion->bindParam(':idAsis', $_SESSION['idAsis']);
    $evaluacion->execute();
    $eva = $evaluacion->fetch();
    
    $DatosAsistencia = $bdd->prepare("SELECT * FROM  Educacion_Asistencias
                                      Where IdComponente = :idC and IdPeriodo = :idP and 
                                      IdSeccion = :idSec and Fecha = :fecha");
                                      
    $DatosAsistencia->bindParam(':idC',$_SESSION['Component'] ) ;
    $DatosAsistencia->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
    $DatosAsistencia->bindParam(':idSec',$_SESSION['IdSec'] ) ;
    $DatosAsistencia->bindParam(':fecha', $fecha);
    $DatosAsistencia->execute();

?>

<div style="width: 90%;margin-left: 5%; background-color: white; ">
<table style='width: 100%;'>             
        
        <tr><td colspan="4" style="color: blue; text-align: left; padding-bottom: 10px;">
            Fecha de Asistencia: <?php echo "$a�o-$mes-$dia";?><br />
            Asistencia total: <?php echo $DatosAsistencia->rowCount()?>
        </td></tr>                  
        <tr><td style="width: 10%; text-align: center;"><input type='button' value='M' id='botoncheck' onclick='checkear();' class='boton' style='width: 25px;' /></td>
            <td style="width: 10%; text-align: center;"><?php echo utf8_encode("N�");?></td><td style="width: 80%;text-align: left;">Nombre de Estudiante</td><td></td></tr>
            <input type="hidden" name="fecha" value="<?php echo "$a�o-$mes-$dia";?>" />
        
        <?php
               $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2, ins.IdInscripcion
                                              FROM Educacion_Estudiantes  as est
                                              inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                              INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                              where ins.IdCiclo = :idC and ins.IdInstitucion = :idI and ins.IdSeccion = :idS
                                              ORDER BY  jov.Apellido1 ASC, jov.Apellido2 ASC  ");
                                              
               $listaAlumnos->bindParam(':idC', $_SESSION['Ciclos']);
               $listaAlumnos->bindParam(':idI', $_SESSION['IdIns']);
               $listaAlumnos->bindParam(':idS', $_SESSION['IdSec']);               
               $listaAlumnos->execute();
               
               $i = 0;
               $asistencia = 0;

               if($listaAlumnos->rowCount() > 0)
               {
                   while($alumno = $listaAlumnos->fetch())
                   {
                       $Desercion = $bdd->prepare("Select * from Educacion_Desercion where IdPeriodo = :idP and IdEstudiante = :idU");
                       $Desercion->bindParam(':idP', $_SESSION['IdPeriodo']);
                       $Desercion->bindParam(':idU', $alumno[0]);
                       $Desercion->execute();
                    
	                   $DatosAsistencia = $bdd->prepare("SELECT * , COUNT( IdAsistencia ) 
                                                        FROM  Educacion_Asistencias
                                                        Where IdComponente = :idC and IdPeriodo = :idP and 
                                                              IdSeccion = :idSec and IdEstudiante = :Est and Fecha = :fecha
                                                        GROUP BY Fecha ORDER BY Fecha ASC ");
                       $DatosAsistencia->bindParam(':idC',$_SESSION['Component'] ) ;
                       $DatosAsistencia->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
                       $DatosAsistencia->bindParam(':idSec',$_SESSION['IdSec'] ) ;
                       $DatosAsistencia->bindParam(':fecha', $fecha);
                       $DatosAsistencia->bindParam(':Est',$alumno[0] ) ;
                       $DatosAsistencia->execute();
                        
                        $i++;      
                        
                        echo "<tr>";
                        
                       if($Desercion->rowCount() == 0)
                       { 
                           if($DatosAsistencia->rowCount() > 0)
                           {
                                echo "  <td style='text-align: center;'><input type='checkbox'  name='asistencia[]' checked='true' value='$alumno[0]'/></td>";
                                $asistencia++;
                           }
                           else
                                echo "  <td style='text-align: center;'><input type='checkbox'  name='asistencia[]' value='$alumno[0]'/></td>";
                       }
                       else
                           echo "<td></td>";     
                        
                       echo "  <td style='text-align: center;'>$i</td>
                               <td style='text-align: left;'>" .utf8_encode("$alumno[2] $alumno[3], $alumno[1]") ."</td>
                               <td style='text-align: center;'>";
                       echo "</tr>";
                   }
   
               }
               else
                    echo "<tr><td colspan='4' style='color:red; text-align: center;'>No hay Alumnos registrados a esta Secci�n</td></tr>";

               echo "<tr>
                         <td colspan='3' style='text-align: center;'></td></tr>";
        ?>
         
        
</table>
</div>
<script>

function Ventana(id)
{
    alert(id);
}
function checkear()
{
    boton = document.getElementById('botoncheck');
    
    if(boton.value == "M")
    { 
        seleccionar_todo();
        boton.value = "D"
    }
    else
    {
        deseleccionar_todo();
        boton.value = "M"
    }
}

function seleccionar_todo()
{ 
   for (i=0;i<document.AsistenciaForm.elements.length;i++) 
      if(document.AsistenciaForm.elements[i].type == "checkbox")	
         document.AsistenciaForm.elements[i].checked = 1;  
}

function deseleccionar_todo()
{ 
   for (i=0;i<document.AsistenciaForm.elements.length;i++) 
      if(document.AsistenciaForm.elements[i].type == "checkbox")	
         document.AsistenciaForm.elements[i].checked = 0; 
}
</script>
