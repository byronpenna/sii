<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administración</a> ->
    <a href="/FUSALMO.php?d=Institution" class='flecha' style='text-decoration:none'>Instituciones</a> ->
    <a href="/FUSALMO.php?d=Section" class='flecha' style='text-decoration:none'>Secciones</a> ->
    <a href="/FUSALMO.php?d=Students" class='flecha' style='text-decoration:none'>Estudiantes</a> ->
</div>

<?php
	if($_SESSION['IdDato'] != "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre 
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               WHERE d.IdDatoFUSALMO = :idD and p.IdPeriodo = :idP");
                               
        $Dato->bindParam(':idD', $_SESSION['IdDato']);
        $Dato->bindParam(':idP', $_SESSION['IdPeriodo']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
                   
        $idIns = $_SESSION["Institucion"]; 
        $idSec = $_SESSION["Seccion"];
        $inscr = $_SESSION["IdInscript"];

        $DatosInstitucion = $bdd->prepare("SELECT * FROM Educacion_Inscripcion as ins
                                           inner join Institucion_Seccion as sec on ins.IdInstitucion = sec.IdInstitucion
                                           inner join Institucion as inst on inst.IdInstitucion = ins.IdInstitucion 
                                           where inst.IdInstitucion = $idIns  and ins.IdInscripcion = $inscr and sec.IdSeccion = $idSec");
        $DatosInstitucion->execute();
        $Ins = $DatosInstitucion->fetch();

        
        $TotalEstudiantes = $bdd->prepare("Select * from Educacion_Estudiantes where IdInscripcion = $inscr");
        $TotalEstudiantes->execute();
    }
    else
            Redireccion("../FUSALMO.php");    
?>

<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style=" width: 90%; margin-left: 5%; margin-top: 20px;">
    <tr><td colspan="5" style="color: blue;">Listado de Estudiantes</td></tr>
    <tr><td colspan="5" style="color: blue;"><?php echo $DatoView[1];?></td></tr>
    <tr><td colspan="5" style="color: blue;"><?php echo $DatoView[3];?></td></tr>
    <tr><td colspan="5" style="color: blue;"><?php echo $Ins[8];?></td></tr>
    <tr><td colspan="3" style="color: blue;"><?php echo $Ins[6];?></td>
        <td colspan="2" style="text-align: right; color: blue;">Total de Estudiantes: <?php echo $TotalEstudiantes->rowCount();?></td>
    </tr>
    <tr><td colspan="5" style="height: 20px; background-color: white;"></td></tr>  
    <tr><td colspan="5" style=" background-color: skyblue; height: 3px;"></td></tr>
    <tr>
        <td style="padding-top: 20px;" colspan="5">
            <form method="post" action="DatoFUSALMO/ListadoEstudiantesABM.php">
            <table style="width: 50%;">
                <tr><td class="tdleft">Primer Nombre:</td><td><input type="text" name="nombre1" required="true" /></td></tr>
                <tr><td class="tdleft">Segundo Nombre:</td><td><input type="text" name="nombre2" required="true" /></td></tr>
                <tr><td class="tdleft">Primer Apellido:</td><td><input type="text" name="apellido1" required="true" /></td></tr>
                <tr><td class="tdleft">Segundo Apellido:</td><td><input type="text" name="apellido2" required="true" /></td></tr>
                
                <tr><td class="tdleft">Sexo:</td>
                    <td>
                        <select name="sexo">
                            <option value="Masculino">Masculino</option>
                            <option value="Femenino">Femenino</option>
                        </select>
                    </td>
                </tr>
                <tr><td colspan="2" style="text-align: center;"><input type="submit" name="Enviar" value="Crear Estudiante" class="boton" style="width: 120px;" /></td></tr>
            </table>
            </form>
        </td>
    </tr>
</table>