<?php        
if(isset($_GET['d']))
{
    /** Formulario para desaroollar Dato FUSALMO **/
    if($_GET['d'] == "Form")
        include("DatoFUSALMO/DatoFUSALMOForm.php");
    
    
    /** Perfil Principal de los Datos FUSALMO **/
    elseif($_GET['d'] == "dato")
            include("DatoFUSALMO/DatoFUSALMO.php");                           
          
          
    /** Mantenimiento de Personal Relacionado con el Dato FUSALMO **/
    elseif($_GET['d'] == "PrList")
            include("DatoFUSALMO/PersonalList.php");
            
    elseif($_GET['d'] == "Prn")
            include("DatoFUSALMO/Personal.php");


    /** Inscripci�n para los Jovenes al Dato FUSALMO **/
    elseif($_GET['d'] == "InsDat")
            include("DatoFUSALMO/InscripcionDatoFUSALMO.php");
            
    elseif($_GET['d'] == "InsDet")
            include("DatoFUSALMO/InscripcionDatoFUSALMODetalles.php");                                                                                                                        
                                                
                                                
    /** Administraci�n del Dato FUSALMO **/
    elseif($_GET['d'] == "Adm")
            include("DatoFUSALMO/DatoFUSALMOAdministration.php");   

    /** Periodos del Dato FUSALMO **/
    elseif($_GET['d'] == "Period")
            include("DatoFUSALMO/PeriodoForm.php"); 
            
            
    /** Componentes del Dato FUSALMO **/
    elseif($_GET['d'] == "Component")
            include("DatoFUSALMO/ComponentesList.php");  
                        
    /** Asignaci�n del Educador al Componentes del Dato FUSALMO **/
    elseif($_GET['d'] == "EduComponent")
            include("DatoFUSALMO/ComponenteEducador.php");                                                                                                                                                                                                     

    /** Asignaci�n del Evaluaciones al Componentes del Dato FUSALMO **/
    elseif($_GET['d'] == "EvaluationIns")
            include("DatoFUSALMO/EvaluacionesInstitucion.php");   
            
    /** Asignaci�n del Evaluaciones al Componentes del Dato FUSALMO **/
    elseif($_GET['d'] == "Evaluation")
            include("DatoFUSALMO/EvaluacionesList.php");
            
    /** Asignaci�n del Evaluaciones al Componentes del Dato FUSALMO **/
    elseif($_GET['d'] == "Matriz")
            include("DatoFUSALMO/EvaluacionesMatriz.php");                               
    
            
            
            
    /** Verificar Secciones y su poblaci�n **/
    elseif($_GET['d'] == "Institution")
            include("DatoFUSALMO/Instituciones.php");   
    
    /** Verificaci�n de Estudiantes por Secci�n **/
    elseif($_GET['d'] == "InstitutionGroup")
            include("DatoFUSALMO/InstitucionesGroup.php");  
            
    /** Verificaci�n de Estudiantes por Secci�n **/
    elseif($_GET['d'] == "Section")
            include("DatoFUSALMO/InscripcionEstudiantes.php"); 
            
    /** Verificaci�n de Estudiantes por Secci�n **/
    elseif($_GET['d'] == "StudentsListPrint")
            include("DatoFUSALMO/AsistenciaPrint.php");              

    /** listado de Estudiantes para agregar por secci�n **/
    elseif($_GET['d'] == "Students")
            include("DatoFUSALMO/InscripcionEstudiantesList.php");             
                                                    
    /** listado de Estudiantes para agregar por secci�n **/
    elseif($_GET['d'] == "StudentsList")
            include("DatoFUSALMO/ListadoEstudiantes.php"); 


    /** Manejo de Notas **/            
    elseif($_GET['d'] == "cycle")
            include("DatoFUSALMO/Ciclo.php");  
            


                        
    /** Manejo de Notas **/            
    elseif($_GET['d'] == "qualification")
            include("DatoFUSALMO/Notas.php");  

    /** Manejo de Reporte **/            
    elseif($_GET['d'] == "ReportStudents")
            include("DatoFUSALMO/Reporteria.php"); 

    /** Manejo de Reporte **/            
    elseif($_GET['d'] == "ReportStudentsDetails")
            include("DatoFUSALMO/ReporteriaAuxiliar.php");                           

    /** Manejo de Reporte **/            
    elseif($_GET['d'] == "ReportComponente")
            include("DatoFUSALMO/ReporteComponente.php"); 
            
    /** Control de Asistencias **/            
    elseif($_GET['d'] == "AttendanceList")
            include("DatoFUSALMO/AsistenciaCalendar.php");                                                                                                                                             
                                          
    /** Asistencias **/            
    elseif($_GET['d'] == "Attendance")
            include("DatoFUSALMO/Asistencia.php");                                          
            
    /** Datos Estadisticos **/            
    elseif($_GET['d'] == "StatisticsData")    
        include("DatoFUSALMO/DatosEstadisticos.php");  
                                                 



    /** Buscador **/            
    elseif($_GET['d'] == "Search")    
        include("DatoFUSALMO/Buscador.php");
        
    /** Buscador **/            
    elseif($_GET['d'] == "PersonalQualification")    
        include("DatoFUSALMO/BuscadorNotas.php"); 
        
        
    elseif($_GET['d'] == "Actions")    
        include("DatoFUSALMO/Acciones.php");                         
   
    
    
    
    /** USAID **/            
    elseif($_GET['d'] == "USAID")    
        Redireccion("USAID.php");                       
                                              
    else
        Redireccion("FUSALMO.php");
}
else 
{
        include 'ClearSession.php';
        $_SESSION['IdDato'] = "";
        $_SESSION["IdA�oDato"] = "";
        
        if($_SESSION["TipoUsuario"] == "Administrador")            
            include("DatoFUSALMO/Acceso_Administrador.php"); 
           

        elseif($_SESSION["TipoUsuario"] == "Joven")
            include("DatoFUSALMO/Acceso_Joven.php"); 
        
        
        elseif($_SESSION["TipoUsuario"] == "Coordinador" || $_SESSION["TipoUsuario"] == "Gestor")
            include("DatoFUSALMO/Acceso_Coordinador.php"); 
}
?>