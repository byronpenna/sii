<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administraci�n</a> ->
    <a href="/FUSALMO.php?d=Institution" class='flecha' style='text-decoration:none'>Instituciones</a> ->
    <a href="/FUSALMO.php?d=Section" class='flecha' style='text-decoration:none'>Secciones</a> ->
    <a href="/FUSALMO.php?d=Students" class='flecha' style='text-decoration:none'>Estudiantes</a> ->
</div>

<?php

	if($_SESSION['IdDato'] != "")
    {
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre 
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               WHERE d.IdDatoFUSALMO = :idD and p.IdPeriodo = :idP");
                               
        $Dato->bindParam(':idD', $_SESSION['IdDato']);
        $Dato->bindParam(':idP', $_SESSION['IdPeriodo']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
                   
        $idIns = $_SESSION["Institucion"]; 
        $idSec = $_SESSION["Seccion"];
        $inscr = $_SESSION["IdInscript"];

        $DatosInstitucion = $bdd->prepare("SELECT * FROM Educacion_Inscripcion as ins
                                           inner join Institucion_Seccion as sec on ins.IdInstitucion = sec.IdInstitucion
                                           inner join Institucion as inst on inst.IdInstitucion = ins.IdInstitucion 
                                           where inst.IdInstitucion = $idIns  and ins.IdInscripcion = $inscr and sec.IdSeccion = $idSec");
        $DatosInstitucion->execute();
        $Ins = $DatosInstitucion->fetch();
        
        if(isset($_GET['n']))
        {
            if($_GET['n'] == 1)
                echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Estudiante fuera del grupo</div>";

            if($_GET['n'] == 2)
                echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Estudiante a sido marcado en deserci�n</div>";
                
            if($_GET['n'] == 3)
                echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Estudiante se le ha quitado la marcado en deserci�n</div>";                                                           
        }
    }
    else
            Redireccion("../FUSALMO.php");    
?>

<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style=" width: 90%; margin-left: 5%; margin-top: 20px;">
<tr><td colspan="3" style="color: blue;">Listado de Estudiantes</td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $DatoView[1];?></td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $DatoView[3];?></td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $Ins[8];?></td></tr>
        <tr><td colspan="3" style="color: blue;"><?php echo $Ins[6];?></td></tr>        
        <tr><td colspan="3" style="height: 20px; background-color: white; text-align: right;">
                <form action='DatoFUSALMO/Reporte.php' method='post'>
                    <input type='hidden' name='name' value='<?php echo $Ins[8] . ' - ' . $Ins[6]?>' />                
                    <input type='submit' class='boton' name='Enviar' value='Reporte' style='width: 150px;'  />
                </form>
                <a href="FUSALMO.php?d=StudentsList"><input type="button" value="Agregar Estudiante" style="width: 150px;" class="boton" /></a>
                

            </td>
        </tr>
        <tr><td colspan="3" style="height: 20px; background-color: white;"></td></tr>  
        <tr><td colspan="3" style=" background-color: skyblue; height: 3px;"></td></tr>
        

            <table style="width: 80%; margin-left: 10%;">  
            <tr><th>N�</th><th>Nombre</th><th>Primer Apellido</th> <th>Segundo Apellido</th><th></th></tr>                                              
                <?php
                            $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2 FROM Educacion_Estudiantes  as est
                                                           INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario                                                          
                                                           where est.IdInscripcion = $inscr 
                                                           group by  jov.IdUsuario
                                                           ORDER BY jov.Apellido1 ASC , jov.Apellido2 ASC ");
                            $listaAlumnos->execute();
                            $q = 0;
                            if($listaAlumnos->rowCount() > 0)
                            {
                                
                                while($alumno = $listaAlumnos->fetch())
                                {
                                    $q++;
                                    echo "<tr style='height: 35px'><td style='text-align: center;'>$q</td>
                                              <td style='text-align: center;'>$alumno[1]</td>
                                              <td style='text-align: center;'>$alumno[2]</td>
                                              <td style='text-align: center;'>$alumno[3]</td>
                                              <td style='text-align: center;'>";
                                    if($_SESSION["TipoUsuario"] == "Coordinador" || $_SESSION["TipoUsuario"]  == "Administrador")
                                    {
                                        echo "<form action='DatoFUSALMO/ListadoEstudiantesABM.php' method='post'>
                                                 <input type='hidden' name='IdU' value='$alumno[0]' />
                                                 <input type='submit' class='botonE' name='Enviar' value='X' style='width: 30px;'  onclick='return confirmar();' />";
                                                    
                                     
                                                $Desercion = $bdd->prepare("Select * from Educacion_Desercion where IdPeriodo = :idP and IdEstudiante = :idU");
                                                $Desercion->bindParam(':idP', $_SESSION['IdPeriodo']);
                                                $Desercion->bindParam(':idU', $alumno[0]);
                                                $Desercion->execute();
                                                
                                                
                                                if($Desercion->rowCount() > 0)
                                                    echo "<input type='submit' class='boton' name='Enviar' value='Cancelar Deserci�n' style='width: 120px;'  onclick='return confirm(\"Quitaras la se�al que este estudiante ha desertado este ciclo\");' />";
                                                
                                                else
                                                    echo "<input type='submit' class='botonG' name='Enviar' value='Deserci�n' style='width: 120px;'  onclick='return confirm(\"Se�alaras que este estudiante ha desertado este ciclo\");' />";
                                                
                                        echo "</form>";
                                    }
                                    
                                    echo "  </td>
                                        </tr>";
                                }
                            }
                            else
                                echo "<tr><td colspan='4' style='color:red; text-align: center;'>No hay Alumnos registrados a esta Secci�n</td></tr>";
                ?>                                
            </table>
            </td>
        </tr>
</table>