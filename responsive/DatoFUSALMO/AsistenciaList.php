<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administración</a> ->
    <a href="/FUSALMO.php?d=Component" class='flecha' style='text-decoration:none'>Área</a> ->
    <a href="/FUSALMO.php?d=EvaluationIns" class='flecha' style='text-decoration:none'>Evaluaciones</a> ->
    <a href="/FUSALMO.php?d=AttendanceList" class='flecha' style='text-decoration:none'>Asistencias</a>
</div>

<?php
    unset($_SESSION['fecha']);
    
    if($_SESSION['IdDato'] != "" || $_SESSION['Component']!= "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre, c.IdComponente, c.NombreComponente
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               Inner join Educacion_Componente as c on c.IdPeriodo = p.IdPeriodo                             
                               WHERE d.IdDatoFUSALMO = :id and c.IdComponente = :idc");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idc', $_SESSION['Component']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");
            

        $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                             FROM  Institucion as inst 
                                             inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                             where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
        
        $DatosInstituciones->bindParam(':idIns', $_SESSION['IdIns']);
        $DatosInstituciones->bindParam(':idSec', $_SESSION['IdSec']);   
        
        $DatosInstituciones->execute();
        $InfoSeccion = $DatosInstituciones->fetch();                                      
    
    
     if(isset($_GET['n']))
     {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right;'>Asistencia Inscrita Exitosamente</div>";
            
        if($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right;'>Asistencia retirada del Area</div>"; 
            
        if($_GET['n'] == 3)
            echo "<div id='Notificacion' name='Notificacion' style='color:blue; text-align: right;'>Asistencia guardadas</div>";                                 
     }
?>

<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style='width: 90%; margin-left: 5%; margin-top: 10px;'>             
        <tr><td colspan="2" style="color: blue;">Asistencias del Area<br /><?php echo "$DatoView[1]<br />$DatoView[3]";?> </td></tr>
        <tr><td colspan="2" style="color: green; text-align: right;"><?php echo "$InfoSeccion[1]<br />$InfoSeccion[3]";?> </td></tr>
        
        
        <tr><td colspan="2" style="height: 5px; background-color: skyblue;"></td></tr>
        <tr><td colspan="2" style="height: 20px; background-color: white;"></td></tr>        
        <tr><td  colspan="2" style="color: blue; text-align: right;" >Area: <?php echo $DatoView[5];?> </td></tr>  
        <tr>
            <td>
                <?php
	                   $DatosAsistencia = $bdd->prepare("SELECT * , COUNT( IdAsistencia ) 
                                                        FROM  Educacion_Asistencias
                                                        Where IdComponente = :idC and IdPeriodo = :idP and IdSeccion = :idSec 
                                                        GROUP BY Fecha ORDER BY Fecha ASC ");
                       $DatosAsistencia->bindParam(':idC',$_SESSION['Component'] ) ;
                       $DatosAsistencia->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
                       $DatosAsistencia->bindParam(':idSec',$_SESSION['IdSec'] ) ;
                       $DatosAsistencia->execute();
                ?>
                <table style="width: 100%;">
                    <tr><td style="width: 40%;text-align: center;">Fecha</td><td style="width: 40%;text-align: center;">Cantidad de Alumnos</td><td style="width: 15%;"></td></tr>
                    <?php
                            
                            
                            if($DatosAsistencia->rowCount() > 0)
                            {
	                           while($Asistencia = $DatosAsistencia->fetch())
                               {
	                               echo "<tr><td style='text-align:  center;'>$Asistencia[5]</td><td style='text-align: center;'>$Asistencia[6]</td>
                                             <td>
                                                <form action='DatoFUSALMO/AsistenciaABM.php' method='post'>
                                                <input type='hidden' name='fecha' value='$Asistencia[5]' />";
                                                    
                                              if($_SESSION["TipoUsuario"] == "Administrador" || $_SESSION["TipoUsuario"] == "Coordinador")      
                                              echo "<input type='submit' value='X' name='Enviar' onclick='return confirmar();' style='width: 30px;' class='botonE' />";
                                              
                                              echo "<input type='submit' value='Ver' name='Enviar' style='width: 50px;' class='boton' />
                                                </form>
                                             </td>
                                         </tr>";
                                   
	                           }
                            }
                            else    
                                echo "<tr><td colspan='4' style='color: red; text-align: center;'>... No hay Asistencias Registradas...</td></tr>";
                    ?>
                </table>
            </td>
        </tr>
        
        <tr style="height: 30px;"><td></td></tr>
        <tr><td colspan="2" style="height: 5px; background-color: skyblue;"></td></tr>
        <tr style="height: 30px;"><td></td></tr>    
        <tr><td colspan="2" style="text-align: center;"><a href="FUSALMO.php?d=Attendance"><input type="button" class="boton" value="Agregar" /></a></td></tr> 
</table>

</div>