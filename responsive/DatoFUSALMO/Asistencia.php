<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administraci�n</a> ->
    <a href="/FUSALMO.php?d=Component" class='flecha' style='text-decoration:none'>�rea</a> ->
    <a href="/FUSALMO.php?d=EvaluationIns" class='flecha' style='text-decoration:none'>Evaluaciones</a> ->
    <a href="/FUSALMO.php?d=AttendanceList" class='flecha' style='text-decoration:none'>Asistencias</a>
</div>

<?php

    if($_SESSION['IdDato'] != "" || $_SESSION['Component']!= "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre, c.IdComponente, c.NombreComponente
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               Inner join Educacion_Componente as c on c.IdPeriodo = p.IdPeriodo                             
                               WHERE d.IdDatoFUSALMO = :id and c.IdComponente = :idc");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idc', $_SESSION['Component']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");
    
    
    $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                         FROM  Institucion as inst 
                                         inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                         where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
    
    $DatosInstituciones->bindParam(':idIns', $_SESSION['IdIns']);
    $DatosInstituciones->bindParam(':idSec', $_SESSION['IdSec']);   
    
    $DatosInstituciones->execute();
    $InfoSeccion = $DatosInstituciones->fetch();    
    
    $evaluacion = $bdd->prepare("Select * from Educacion_Asistencias where IdAsistencia = :idAsis");
    $evaluacion->bindParam(':idAsis', $_SESSION['idAsis']);
    $evaluacion->execute();
    $eva = $evaluacion->fetch();
        
    if($_SESSION['fecha'] == "")   
        $ABM = "Guardar";
    
    else
        $ABM = "Actualizar";
    
     if(isset($_GET['n']))
     {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right;'>Evaluaci�n Inscrita Exitosamente</div>";
            
        if($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right;'>Evaluaci�n retirada del Area</div>";            
                                          
     }
?>

<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style='width: 90%; margin-left: 5%; margin-top: 10px;'>             
        <tr><td colspan="5" style="color: blue;">Asistencias del Area<br /><?php echo "$DatoView[1]<br />$DatoView[3]";?> </td></tr>
        <tr><td colspan="5" style="color: green; text-align: right;"><?php echo "$InfoSeccion[1]<br />$InfoSeccion[3]";?> </td></tr>
        <tr><td colspan="5" style="height: 5px; background-color: skyblue;"></td></tr>
        <tr><td colspan="5" style="height: 20px; background-color: white;"></td></tr>        
        <tr><td colspan="5" style="color: blue; text-align: left;" >Area: <?php echo $DatoView[5];?> <br /></td></tr> 

        <?php $expresion = "^(19|20)\d{2}[\-](0?[1-9]|1[012])[\-](0?[1-9]|[12][0-9]|3[01])$"
	       
?>
        <form name="asistencia" method="post" action="DatoFUSALMO/AsistenciaABM.php" name="form1" id="form1" >
        <tr><td colspan="3" style="color: blue; text-align: left; padding-bottom: 10px;">Fecha de Asistencia (AAAA-MM-DD):
            <input style="margin-left: 20px; width: 80px;" required="true" type="text" pattern="<?php echo $expresion; ?>" name="fecha" value="<?php echo $_SESSION['fecha'];?>"/><br /></td></tr>        
        <tr><td style="width: 10%; text-align: center;"><input type='button' value='Marcar todas' id='botoncheck' onclick='checkear();' class='boton' style='width: 125px;' /></td>
            <td style="width: 10%; text-align: center;">N�</td><td style="width: 80%;text-align: left;">Nombre de Estudiante</td></tr>
        
        
        <?php
               $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2, ins.IdInscripcion
                                              FROM Educacion_Estudiantes  as est
                                              inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                              INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                              where ins.IdCiclo = :idC and ins.IdInstitucion = :idI and ins.IdSeccion = :idS
                                              ORDER BY  jov.Apellido1 ASC, jov.Apellido2 ASC  ");
                                              
               $listaAlumnos->bindParam(':idC', $_SESSION['Ciclos']);
               $listaAlumnos->bindParam(':idI', $_SESSION['IdIns']);
               $listaAlumnos->bindParam(':idS', $_SESSION['IdSec']);               
               $listaAlumnos->execute();
               
               $i = 0;
               
               if($listaAlumnos->rowCount() > 0)
                   while($alumno = $listaAlumnos->fetch())
                   {
	                   $DatosAsistencia = $bdd->prepare("SELECT * , COUNT( IdAsistencia ) 
                                                        FROM  Educacion_Asistencias
                                                        Where IdComponente = :idC and IdPeriodo = :idP and 
                                                              IdSeccion = :idSec and IdEstudiante = :Est and Fecha = :fecha
                                                        GROUP BY Fecha ORDER BY Fecha ASC ");
                       $DatosAsistencia->bindParam(':idC',$_SESSION['Component'] ) ;
                       $DatosAsistencia->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
                       $DatosAsistencia->bindParam(':idSec',$_SESSION['IdSec'] ) ;
                       $DatosAsistencia->bindParam(':fecha', $_SESSION['fecha']);
                       $DatosAsistencia->bindParam(':Est',$alumno[0] ) ;
                       $DatosAsistencia->execute();
                        
                        $i++;      
                        
                        echo "<tr>";
                        if($DatosAsistencia->rowCount() > 0)
                        echo "<td style='text-align: center;'><input type='checkbox'  name='asistencia[]' checked='true' value='$alumno[0]'/></td>";
                        
                        else
                        echo "<td style='text-align: center;'><input type='checkbox'  name='asistencia[]' value='$alumno[0]'/></td>";
                            
                        echo "<td style='text-align: center;'>$i</td>
                                  <td style='text-align: left;'>$alumno[2] $alumno[3], $alumno[1]</td>";
                        

                        
                        echo "</tr>";
                   }
               else
                    echo "<tr><td colspan='4' style='color:red; text-align: center;'>No hay Alumnos registrados a esta Secci�n</td></tr>";

               echo "<tr>
                         <td colspan='3' style='text-align: center;'><input class='boton' type='submit' name='Enviar' value='$ABM' /></td></tr>";
        ?>
        </form>            
        
</table>
</div>
<script>

function checkear()
{
    boton = document.getElementById('botoncheck');
    
    if(boton.value == "Marcar todas")
    { 
        seleccionar_todo();
        boton.value = "Desmarcar todas"
    }
    else
    {
        deseleccionar_todo();
        boton.value = "Marcar todas"
    }
}

function seleccionar_todo(){ 
   for (i=0;i<document.asistencia.elements.length;i++) 
      if(document.asistencia.elements[i].type == "checkbox")	
         document.asistencia.elements[i].checked=1 
}

function deseleccionar_todo(){ 
   for (i=0;i<document.asistencia.elements.length;i++) 
      if(document.asistencia.elements[i].type == "checkbox")	
         document.asistencia.elements[i].checked=0 
} 
</script>