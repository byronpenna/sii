<?php
    
    
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=ReporteAsistencia.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';
    	
        
    if(!isset($_POST['Enviar']))
            Redireccion("../FUSALMO.phpd=Adm");
            
                $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                             FROM  Institucion as inst 
                                             inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                             where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
        
        $DatosInstituciones->bindParam(':idIns', $_POST['IdIns']);
        $DatosInstituciones->bindParam(':idSec', $_POST['IdSec']);   
        $DatosInstituciones->execute();
        $InfoSeccion = $DatosInstituciones->fetch();  
            
          
        $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2, ins.IdInscripcion
                                       FROM Educacion_Estudiantes  as est
                                       inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                       INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                       where ins.IdCiclo = :idC and ins.IdInstitucion = :idI and ins.IdSeccion = :idS
                                       ORDER BY  jov.Apellido1 ASC, jov.Apellido2 ASC  ");
                  
        $listaAlumnos->bindParam(':idC', $_SESSION['Ciclos']);
        $listaAlumnos->bindParam(':idI', $_POST['IdIns']);
        $listaAlumnos->bindParam(':idS', $_POST['IdSec']);               
        $listaAlumnos->execute();

        $asis = isset($_POST['Componente']) ? $_POST['Componente'] : array();
        $rows = count($asis) + 3; 
        
        if(is_array($asis))                                             
            $Componente = $asis[0];                  
                  
        else                                 
            $Componente = $asis;                    
                  

 echo "<table style='width: 100%; background: white; padding: 5px; border-radius:10px'>
              <tr><td style='color: blue;' colspan='$rows'>Reporte Personalizado                   
                  <hr color='skyblue' /></td></tr>
              <tr><td style='width: 15%;'>Instituci�n: </td><td colspan='$rows'>$InfoSeccion[1]</td></tr>
              <tr><td style='width: 15%;'>Secci�n:     </td><td colspan='$rows'>$InfoSeccion[3]</td></tr>
              <tr><td colspan='$rows'><hr color='skyblue' /></td></tr>
              <tr><td colspan='$rows'>
              <table style='overflow-x: scroll; width:100%' rules='all'>
              <tr><td colspan='2' style='width: 35%' style='text-align: center; color: blue;'> Alumnos </td>";
              

              for ($index = 0 ; $index < count($asis); $index ++)
              {
                  $Asistencias= $bdd->prepare("SELECT Fecha FROM  Educacion_Asistencias 
                                               where IdComponente = $Componente and IdPeriodo = ".$_SESSION['IdPeriodo']." and IdSeccion = ".$_POST['IdSec']."
                                               group by Fecha");
                  $Asistencias->execute();

                  $totalAsistencias = $Asistencias->rowCount();
                  while($fecha = $Asistencias->fetch())
                  {
                        echo "<td style='text-align: center;'>$fecha[0]</td>";
                  }   
              }
              echo "<td style='text-align: center;'>Promedio de <br />Asistencia</td>";              
              echo "</tr>";
        $i = 1;
        while($Alumno = $listaAlumnos->fetch())
        {
            $CantidadA = 0;
            echo "<tr>
                      <td colspan='2' >$i - $Alumno[2] $Alumno[3] $Alumno[1]</td>";
                     
            $Desercion = $bdd->prepare("Select * from Educacion_Desercion where IdPeriodo = :idP and IdEstudiante = :idU");
            $Desercion->bindParam(':idP', $_SESSION['IdPeriodo']);
            $Desercion->bindParam(':idU', $Alumno[0]);
            $Desercion->execute();                      
                      
              for ($index = 0 ; $index < count($asis); $index ++)
              {     
                  if(is_array($asis))                  
                    $Componente = $asis[0];
                  
                  else                  
                    $Componente = $asis;
                          
                  $Asistencias= $bdd->prepare("SELECT Fecha FROM  Educacion_Asistencias 
                                               where IdComponente = $Componente and IdPeriodo = ".$_SESSION['IdPeriodo']." and IdSeccion = ".$_POST['IdSec']."
                                               group by Fecha");
                  $Asistencias->execute();
                  
                  while($fecha = $Asistencias->fetch())
                  {
                        $AsisJoven = $bdd->prepare("SELECT Fecha FROM  Educacion_Asistencias 
                                                    where IdEstudiante = $Alumno[0] and Fecha = '$fecha[0]' and IdComponente = $Componente");
                        $AsisJoven->execute();
                        
                        
                        
                        
                        if($Desercion->rowCount() > 0)
                            $tDesercion++;
                        
                        
                        if($Desercion->rowCount() == 0)
                        {
                            if($AsisJoven->rowCount() > 0)
                            {
                                echo "<td style='text-align: center;'>Ok</td>";
                                $CantidadA++;
                            }
                            else                        
                                echo "<td style='text-align: center;' >-</td>";
                        }
                        else
                        {
                            echo "<td style='text-align: center;'> </td>";
                        }     
                  }   
              }                              
              $i++;                        
            
            if($Desercion->rowCount() == 0)
            {
                $total = ($CantidadA / $totalAsistencias * 100);
                echo "<td style='text-align: center;'>".number_format($total,2)."%</td></tr>";
            }
        }
        
        echo "</table>
              </td>
              </tr>
        </table>";
?>