<?php

/**
 * @author Manuel
 * @copyright 2013
 */

require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Guardar")
    {
        
        
        
        $IdSecciones = isset($_POST['Seccion']) ? $_POST['Seccion'] : array();    
           
        foreach($IdSecciones as $ids) 
        {
            $insert = $bdd->prepare("Insert into Educacion_Inscripcion 
                                     values (null, :idC, :idI, :ids)");
                                     
            $insert->bindParam(':idC', $_SESSION["Ciclos"]);  
            $insert->bindParam(':idI', $_SESSION["Instituto"]);
            $insert->bindParam(':ids', $ids);          
            $insert->execute();                        
        }
        Redireccion("../FUSALMO.php?d=Institution&n=1");  
    }
    elseif($_POST['Enviar'] == "Verificar Grupos")
    {
        $_SESSION["Institucion"] = $_POST['IdI'];        
        Redireccion("../FUSALMO.php?d=InstitutionGroup");
    }
    
    elseif($_POST['Enviar'] == "Ver Estudiantes")
    {
        $_SESSION["Institucion"] = $_POST['IdI'];        
        Redireccion("../FUSALMO.php?d=Section");
    }         

    elseif($_POST['Enviar'] == "Ver Estudiantes")
    {
        $_SESSION["Institucion"] = $_POST['IdI'];        
        Redireccion("../FUSALMO.php?d=Section");
    }
    elseif($_POST['Enviar'] == "Imprimir Lista")
    {
        $_SESSION["Seccion"] = $_POST['IdSection'];
        $_SESSION["IdInscript"] = $_POST['IdInscript'];       
        Redireccion("AsistenciaPrint.php");
    }   
    
    elseif($_POST['Enviar'] == "Usuarios")
    {
        $_SESSION["Seccion"] = $_POST['IdSection'];
        $_SESSION["IdInscript"] = $_POST['IdInscript'];       
        Redireccion("AsistenciaPrinUsers.php");
    }       
    
    elseif($_POST['Enviar'] == "Firma")
    {
        $_SESSION["Seccion"] = $_POST['IdSection'];
        $_SESSION["IdInscript"] = $_POST['IdInscript'];       
        Redireccion("AsistenciaPrintFirma.php");
    }   
    
    elseif($_POST['Enviar'] == "Ficha")
    {
        $_SESSION["Seccion"] = $_POST['IdSection'];
        $_SESSION["IdInscript"] = $_POST['IdInscript'];       
        Redireccion("Ficha.php");
    }           
        
    elseif(isset($_POST['IdSection']))
    {

        $_SESSION["Seccion"] = $_POST['IdSection'];
        $_SESSION["IdInscript"] = $_POST['IdInscript'];
        
        Redireccion("../FUSALMO.php?d=Students");
    }
    else
        Redireccion("../FUSALMO.php");
}

?>