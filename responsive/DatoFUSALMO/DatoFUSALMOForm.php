<script>
        function datos(form)
        {
             var texto =  document.getElementById('Especificaciones1');
             var input =  document.getElementById('DatoEspecificaciones1');
             
             var texto2 =  document.getElementById('Especificaciones2');
             var input2 =  document.getElementById('DatoEspecificaciones2');  

             var texto3 =  document.getElementById('Especificaciones3');
             var input3 =  document.getElementById('DatoEspecificaciones3');                           
             
             var selec = form.Categoria.options;
             
             if(selec[1].selected == true)
             {
                texto.style.display = "none";                
                texto2.style.display = "none";
                texto3.style.display = "none";
                
                input2.style.display = "none";                
                input.style.display = "none";
                input3.style.display = "none";  
                                
                texto.style.display = "inherit";
                texto.style.cssText = "text-align: right; padding-right: 20px;";      

                texto2.style.display = "inherit";
                texto2.style.cssText = "text-align: right; padding-right: 20px;";
                
                input.style.display = "inherit";                
                input2.style.display = "inherit";                
             }
             else if(selec[2].selected == true)
             {   
                texto.style.display = "none";                
                texto2.style.display = "none";
                texto3.style.display = "none";
                
                input2.style.display = "none";                
                input.style.display = "none";
                input3.style.display = "none";
                
                texto3.style.display = "inherit";
                texto3.style.cssText = "text-align: right; padding-right: 20px;";      
                
                input3.style.display = "inherit";                             
             }             
             else
             {                
                texto.style.display = "none";                
                texto2.style.display = "none";
                texto3.style.display = "none";                
                
                input2.style.display = "none";                
                input.style.display = "none";
                input3.style.display = "none";                
                
             }
        }
        
        function validar(form)
        {
             var selec = form.Categoria.options;
             
             var fecha1 = document.getElementById('DatoEspecificaciones1');
             var fecha2 = document.getElementById('DatoEspecificaciones2'); 
             var fecha3 =  document.getElementById('DatoEspecificaciones3');
               
             if(selec[0].selected == true)
             {
                alert("Error!, Selecciona una categoria")
                return false;
             }                                               
        }
</script>     
<?php
	if(!isset($_GET['s']) && $_GET['s'] != "si")
    {
        $DatosServicios = $bdd->prepare("Select * from DatoFusalmo where IdDatoFUSALMO = ". $_SESSION['IdDato']);
        $DatosServicios->execute();
        $ServiciosInst = $DatosServicios->fetch();
       
        $ABM = "Modificar"; 
    }
    else
    {
       // date("Y-m-d");
       $ABM = "Insertar";
    }
    
?>   
<div style="width: 90%; background-color: white; border-radius: 20px; margin-left: 5%; ">
<form action="DatoFUSALMO/DatoFUSALMOABM.php" method="POST" name="DatoFUSALMOABM" id="DatoFUSALMOABM">        
<table style=" width: 80%; margin-left: 10%;">
<tr>
	<td colspan="2" style="text-align: center;"><h2>Registro de Nuevo Dato FUSALMO</h2>
        <hr style="color: skyblue; background-color: skyblue; height: 5px; width: 80%;" />  
    </td>
</tr>
<tr>
	<td style="text-align: right; padding-right: 20px">Nombre para nuestro Dato:</td>
	<td style="color: red;"><input type="text" name="Nombre" id="Nombre" value="<?php echo $ServiciosInst[1]?>" style="width: 200px;" required="true" title="Campo Requerido" />*</td>
</tr>
<tr>
	<td style="text-align: right; padding-right: 20px">Componente:</td>
	<td><select name="Linea" id="Linea" style="width: 250px;">
    <?php
           $array = array(
                    "Educaci�n Integral Liberadora" => "Educaci�n Integral Liberadora", 
                    "Gesti�n Sociolaboral" => "Gesti�n Sociolaboral",
                    "Participaci�n y Protagonismo Juvenil" => "Participaci�n y Protagonismo Juvenil",
                    "M�dulo USAID - Objetivo 1" => "M�dulo USAID - Objetivo 1",
                    "M�dulo USAID - Objetivo 2" => "M�dulo USAID - Objetivo 2");
           
           foreach ($array as $i => $value) {
                if($i == $ServiciosInst[4])
                    echo "<option Selected='true' value='$i'>$i</option>";
                    
                else
                    echo "<option value='$i'>$i</option>";
            }
    ?></select></td>
</tr>
<tr>
	<td style="text-align: right; padding-right: 20px">Descripci�n del Dato:</td>
	<td><textarea name="Descripcion" id="Descripcion" style="width: 250px;"  rows="3"><?php echo $ServiciosInst[2]?></textarea></td>
</tr>
<tr>
	<td style="text-align: right; padding-right: 20px">Categoria:</td>
	<td><select name="Categoria" id="Categoria" style="width: 200px;" onchange="datos(this.form)">
    <?php
	                           $array = array(
                                        "Seleccione..." => "Seleccione...",
                                        "Proyecto" => "Proyecto", 
                                        "Programa" => "Programa",                                                                                                                       
                                        "Servicio" => "Servicio");
                               
                               foreach ($array as $i => $value) {
                                    if($i == $ServiciosInst[3])
                                        echo "<option Selected='true' value='$i'>$i</option>";
                                        
                                    else
                                        echo "<option value='$i'>$i</option>";
                                }
    ?></select></td>
</tr>

        <?php
        if($ServiciosInst[3] == "Proyecto")
        {
            $fechaInicio = $ServiciosInst[5];
            $display1 = "display: inherit;";
            $display2 = "display: none;";
        }
        else if($ServiciosInst[3] == "Programa")
        {
            $fechaInicio = substr($ServiciosInst[5],0,4);
            $display1 = "display: none;";
            $display2 = "display: inherit;";
        }
        else
        {       
            $fechaInicio = "";
            $display1 = "display: none;";
            $display2 = "display: none;";
        }
        ?>
<tr>
    <td id="Especificaciones1" style="text-align: right; padding-right: 20px; <?php echo $display1?>">
    Inicio del Proyecto:
    </td>
    <td id="DatoEspecificaciones1" style="<?php echo $display1?>">
    <input type="date" name="FechaInicio" id="FechaInicio" value="<?php echo $fechaInicio?>" title="Fecha incorrecta"  />
    </td>
</tr>
<tr>
    <td id="Especificaciones2" style="text-align: right; padding-right: 20px; <?php echo $display1?>">
    Finalizaci�n del Proyecto:
    </td>
    <td id="DatoEspecificaciones2" style="<?php echo $display1?>">
    <input type="date" name="FechaFin" id="FechaFin" value="<?php echo $ServiciosInst[6];?>" title="Fecha incorrecta"  />
    </td>
</tr>
<tr>
    <td id="Especificaciones3" style="text-align: right; padding-right: 20px;<?php echo $display2?>">
    Apertura del Programa:
    </td>
    <td id="DatoEspecificaciones3" style="<?php echo $display2?>">
    <input type="number" name="a�o" id="a�o" min="2001" max="2013" value="<?php echo $fechaInicio?>" title="Fecha no en rango" />
    </td>
</tr>
<tr><td colspan="2" style="text-align: center;">
    <input class="boton" type="submit" value="<?php echo $ABM?>" name="enviar" onclick="return validar(this.form);" />
</td></tr>
</table>
</form>          
</div>

<div class="clr"></div> 