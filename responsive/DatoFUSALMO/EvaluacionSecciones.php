<?php

/**
 * @author Manuel
 * @copyright 2013
 */
    
   require '../net.php';


   $DatosInstituciones = $bdd->prepare("SELECT * FROM  Institucion_Seccion as s 
                                        inner join Educacion_Inscripcion as i on i.IdSeccion = s.IdSeccion
                                        Where s.IdInstitucion = :idIns and IdCiclo = :idC 
                                        Order by s.SeccionEducativa ASC");
                                        
   $DatosInstituciones->bindParam(':idIns', $_GET["code"]);
   $DatosInstituciones->bindParam(':idC', $_SESSION["Ciclos"]);
   $DatosInstituciones->execute();

   if($DatosInstituciones->rowCount() > 0)
   {
       while($Instituciones = $DatosInstituciones->fetch())
       {       
            echo "<option value='$Instituciones[0]'>" . utf8_encode($Instituciones[2]) . "</option>";
       }
   }
   else
        echo "<option value='Error' style='color:red'>No hay instituciones Secciones registradas a este periodo.</option>";

?>