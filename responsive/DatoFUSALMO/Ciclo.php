<script>
function showAlert() {
	alert('Error, el periodo actual esta en Uso.');
    return false;
}
</script>

<?php

/**
 * @author Manuel
 * @copyright 2013
 */

    if($_SESSION['IdDato'] != "")
    {
            
            $Dato = $bdd->prepare("SELECT IdDatoFUSALMO, Nombre
                                   FROM  DatoFusalmo  WHERE IdDatoFUSALMO = :id ");
                                   
            $Dato->bindParam(':id', $_SESSION['IdDato']);
            $Dato->execute();
            $fusalmo = $Dato->fetch();
    }

?>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administraci�n</a> ->
    <a href="/FUSALMO.php?d=cycle" class='flecha' style='text-decoration:none'>Ciclos</a>
</div>



<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
        
        <div style="float: left; width: 90%; margin-left: 5%; margin-top: 3%;">
        <table>
            <tr><td style="color: blue;">Ciclos Inscritos <br /><?php echo $fusalmo[1];?></td></tr>
        </table>
        
        <?php
	            $Dato = $bdd->prepare("SELECT * FROM  Educacion_Ciclos where IdDatoFusalmo = :id");
                $Dato->bindParam(':id', $_SESSION['IdDato']);
                $Dato->execute();
            
                if($Dato->rowCount() == 0)
                {
        ?>
                <br /><br />
                <h2 style="color: red; text-align: center;">No hay Ciclos registrados te sugerimos registrar uno para poder asignarlo al Periodo </h2><br />
                <table style="width: 80%; margin-left: 10%;">
                    <form action="DatoFUSALMO/CicloABM.php" method="Post">
                        <tr>
                            <td style="text-align: right; padding-right: 20px">Nombre del Ciclo:</td>
                            <td><input type="text" required="true" name="Nombre" /></td>
                            <td><input type="hidden" name="primer" value="primer" /></td>
                        </tr>
                        <tr><td colspan="2" style="text-align: center;"><input style='width: 150px;' name="Enviar" type='submit' class='boton' value='Crear' /></td></tr>
                    </form>
                </table>
                </div>  
        <?php
                }
                else
                {   
                    if(isset($_GET['n']))
                    {
                        if($_GET['n'] == 1)
                            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Ciclo Agregado Exitosamente</div>";
                            
                        if($_GET['n'] == 2)
                            echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Ciclo Eliminado Exitosamente</div>";

                        if($_GET['n'] == 3)
                            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Periodo Agregado Exitosamente</div>"; 

                        if($_GET['n'] == 4)
                            echo "<div id='Notificacion' name='Notificacion' style='color:blue;'>Periodo Cambiado Exitosamente</div>";

                        if($_GET['n'] == 5)
                            echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Periodo Eliminado Exitosamente</div>";                                                                                                                                                                                                 
                    }
            
                    echo "<table style='width: 100%;'>
                          <tr><td colspan='2' style='text-align: right;'>Acciones</td></tr>";

                    while($ciclos = $Dato->fetch())
                    {
                        echo "<table style='width: 90%; margin-left: 5%;'>
                              <form action='DatoFUSALMO/CicloABM.php' method='Post'>
                              <tr><td colspan='4'>Nombre de ciclo: <em style='color: blue'>$ciclos[2]</em></td>
                                  <td style='text-align: right;'>";
                                  
                        if($Dato->rowCount() > 1)
                        {
                            $Seleccion = $bdd->prepare("SELECT P.* FROM `Educacion_Ciclos`  as C
                                                        inner join Educacion_CicloPeriodo as CP on C.IdCiclo = CP.IdCiclo
                                                        inner join Educacion_Periodos as P on P.IdPeriodo = CP.IdPeriodo
                                                        where P.Status = 'Seleccionado' and C.IdCiclo = $ciclos[0]");
                            $Seleccion->execute();
                            
                            if($Seleccion->rowCount() == 0)
                            {
                                $_SESSION['Ciclos'] = $ciclos[0];
                                if($_SESSION["TipoUsuario"] == "Coordinador" || $_SESSION["TipoUsuario"]  == "Administrador")
                                echo "      <input type='submit' value='X' name='Enviar' onclick='return confirmar();' class='botonE' style='width: 30px' />";
                            }
                        }
                        echo "          <input type='hidden' value='$ciclos[0]' name='IdC'  />";
                            
                            if($_SESSION["TipoUsuario"] == "Coordinador" || $_SESSION["TipoUsuario"] == "Administrador")
                            echo "      <input type='submit' value='Nuevo' name='Enviar' alt='Nuevo Periodo' class='boton' style='width: 60px' />";
                        
                        echo "                                       
                                  </td>
                              </tr>
                              </form>";
                                                
                        $Periodos = $bdd->prepare("SELECT P.* FROM  Educacion_CicloPeriodo as CP
                                                   inner join Educacion_Periodos as P on CP.IdPeriodo = P.IdPeriodo 
                                                   where CP.IdCiclo = $ciclos[0]");
                        $Periodos->execute();

                        while($periodo = $Periodos->fetch()){
                            
                            if($periodo[5] == "Seleccionado")
                            {
                                $color = "green";
                                $action = "onClick='return showAlert();'";
                            }
                            else
                            {
                                $color = "black";
                                $action = "onclick='return confirmar();'";
                            }

                            echo "<tr><form action='DatoFUSALMO/CicloABM.php' method='Post'>
                                      <td style='background-color: skyblue; width: 1%'></td>
                                      <td style='text-align: center;color:$color'>$periodo[2]
                                      <input type='hidden' value='$periodo[0]' name='IdP'  />
                                      </td>
                                      <td style='text-align: center;color:$color'>Semana " . substr($periodo[3],6,2) . " del a�o " .substr($periodo[3],0,4) ." </td>
                                      <td style='text-align: center;color:$color'>Semana " . substr($periodo[4],6,2) . " del a�o " .substr($periodo[4],0,4) ." </td>
                                      <td style='text-align: right; width: 35%'>--->";

                            if($_SESSION["TipoUsuario"] == "Coordinador" || $_SESSION["TipoUsuario"] == "Administrador")                                      
                                echo "<input type='submit' value='Eliminar' alt='borrar' name='Enviar' class='botonE' style='width: 60px' $action />";
                                
                                echo "<input type='submit' value='Elegir' name='Enviar' alt='Seleccionar Ciclo' class='botonG' style='width: 60px' />
                                      </form></td>
                                  </tr>
                                  <tr style='height:10px'><td colspan='5' style='background-color: white'></td></tr>";
                        }                                      
                        echo "<tr style='height:5px'><td colspan='5' style='background-color: skyblue'></td></tr>";
                        echo "<tr style='height:30px'><td colspan='5' style='background-color: white'></td></tr>";                        
                    }
                    echo "</table>";
                
        ?>            
        </div>        
        <div class="clr" style="padding-top: 50px;">
            <table style="width: 80%; margin-left: 10%;">
                <form action="DatoFUSALMO/CicloABM.php" method="Post">
                    <tr>
                        <td style="text-align: right; padding-right: 20px">Nombre del Ciclo:</td>
                        <td><input type="text" required="true" name="Nombre" /></td>
                    <td><input style='width: 150px;' name="Enviar" type='submit' class='boton' value='Crear' /></td></tr>
                </form>
            </table>
        </div> 
        <?php }  ?>
        <div class="clr" style="padding-top: 50px;"></div>
</div>