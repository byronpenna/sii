<script>
function ValidarNota(form)
{
    var nota = 0;
    for (i=0;i < form.elements.length;i++)
    {
        if(form.elements[i].type == "text")        
        {       
            nota = form.elements[i].value;
                        
            if (!/^([0-9])*[.]?[0-9]*$/.test(nota))
            {
                form.elements[i].focus();
                form.elements[i].style.color = "red";
                return false;    
            }            
            else
            {
                if(nota >= 0 && nota <= 10)
                {
                    form.elements[i].style.color = "green";                     
                } 
                else
                {
                    form.elements[i].focus();
                    form.elements[i].style.color = "red";
                    return false; 
                }                           
            }            
        }            
    }
    return true;    
}
</script>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administraci�n</a> ->
    <a href="/FUSALMO.php?d=Component" class='flecha' style='text-decoration:none'>�rea</a> ->
    <a href="/FUSALMO.php?d=EvaluationIns" class='flecha' style='text-decoration:none'>Evaluaciones</a> ->
    <a href="/FUSALMO.php?d=Evaluation" class='flecha' style='text-decoration:none'>Asignaci�n</a> ->
</div>

<?php

    if($_SESSION['IdDato'] != "" || $_SESSION['Component']!= "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre, c.IdComponente, c.NombreComponente
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               Inner join Educacion_Componente as c on c.IdPeriodo = p.IdPeriodo                             
                               WHERE d.IdDatoFUSALMO = :id and c.IdComponente = :idc");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idc', $_SESSION['Component']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");
    
    /** Datos FUSALMO e Institucion **/
    $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                         FROM  Institucion as inst 
                                         inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                         where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
    
    $DatosInstituciones->bindParam(':idIns', $_SESSION['IdIns']);
    $DatosInstituciones->bindParam(':idSec', $_SESSION['IdSec']);   
    
    $DatosInstituciones->execute();
    $InfoSeccion = $DatosInstituciones->fetch();    
    
    /** Asistencias **/
    $DatosAsistencias = $bdd->prepare("SELECT * FROM Educacion_Asistencias
                                      Where IdComponente = :idC and IdPeriodo = :idP and IdSeccion = :idSec 
                                      GROUP BY Fecha ORDER BY Fecha ASC ");
                                      
    $DatosAsistencias->bindParam(':idC',$_SESSION['Component'] ) ;
    $DatosAsistencias->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
    $DatosAsistencias->bindParam(':idSec',$_SESSION['IdSec'] ) ;
    $DatosAsistencias->execute();   
                             
?>
<style>
.tdnotas
{
    text-align: center;
    padding: 4px;
}

.tdpromedio
{
    border-color: transparent;
    background-color: transparent;
}
</style>
<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style='width: 90%; margin-left: 5%; margin-top: 10px;'>             
        <tr><td colspan="5" style="color: blue;">Evaluaciones del Area<br /><?php echo "$DatoView[1]<br />$DatoView[3]";?> </td></tr>
        <tr><td colspan="5" style="color: green; text-align: right;"><?php echo "$InfoSeccion[1]<br />$InfoSeccion[3]";?> </td></tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr><td colspan="5" style="height: 20px; background-color: white;"></td></tr>        
        <tr><td></td><td colspan="4" style="color: blue; text-align: right;" >Area: <?php echo $DatoView[5];?> </td></tr>
        <tr><td colspan="5"> 
            <form name="evaluacion" method="post" action="DatoFUSALMO/NotasABM.php" name="form1" id="form1" >
            <table style="width: 100%;" rules='all'>      
                <tr><td style="width: 7%; text-align: center;">N� de<br /> Lista</td><td>Nombre del Estudiante</td>
                <?php
                        $Evaluaciones = $bdd->prepare("SELECT * FROM Educacion_Evaluaciones where IdComponente = " . $_SESSION['Component'] . " and IdPeriodo = " . $_SESSION['IdPeriodo'] . " and IdSeccion = " . $_SESSION['IdSec']);
                        $Evaluaciones->execute();
                        $ArrayE = array();
                         
                        while($DataE = $Evaluaciones->fetch())
                        {
                            echo "<td style='width: 10%;text-align:  center;'>" . substr($DataE[3],0,7). "... <br /> $DataE[4]% 
                                  <input type='hidden' name='Evaluacion[]' value='$DataE[0]' />
                                  </td>";
                            array_push($ArrayE, array($DataE[0],$DataE[4]));                         
                        }	
                ?>
                <td>Promedio</td>
                </tr>
                
                
                
                <?php   
                       $PromedioS = 0;
                       $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2, ins.IdInscripcion
                                                      FROM Educacion_Estudiantes  as est
                                                      inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                                      INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                                      where ins.IdCiclo = :idC and ins.IdInstitucion = :idI and ins.IdSeccion = :idS 
                                                      ORDER BY  jov.Apellido1 ASC, jov.Apellido2 ASC ");
                                                      
                       $listaAlumnos->bindParam(':idC', $_SESSION['Ciclos']);
                       $listaAlumnos->bindParam(':idI', $_SESSION['IdIns']);
                       $listaAlumnos->bindParam(':idS', $_SESSION['IdSec']);
                       $listaAlumnos->execute();
                       
                       $i = 0;
                       $TotalDesercion = 0;
                       
                       if($listaAlumnos->rowCount() > 0)
                       {
                            
                           while($alumno = $listaAlumnos->fetch())
                           {
                                $i++;
                                $PromedioE = 0; 
                                echo "<tr><td style='text-align: center;'>$i</td>
                                          <td style='text-align: left;'> $alumno[2] $alumno[3], $alumno[1]<input type='hidden' name='id[]' value='$alumno[0]'  /></td>";
                                                                            
                                $Evaluaciones = $bdd->prepare("SELECT * FROM Educacion_Evaluaciones where IdComponente = " . $_SESSION['Component'] . " and IdPeriodo = " . $_SESSION['IdPeriodo'] . " and IdSeccion = " . $_SESSION['IdSec']);
                                $Evaluaciones->execute();
                                
                                if($Evaluaciones->rowCount() > 0)
                                {   
                                    $j = o;
                                    
                                    $Desercion = $bdd->prepare("Select * from Educacion_Desercion where IdPeriodo = :idP and IdEstudiante = :idU");
                                    $Desercion->bindParam(':idP', $_SESSION['IdPeriodo']);
                                    $Desercion->bindParam(':idU', $alumno[0]);
                                    $Desercion->execute();
                                        
                                    foreach($ArrayE as $Value)
                                    {                                        
                                        $j++;
                                        $NotaE = "-";
                                        
                                        $EvaluacionAsistencia = $bdd->prepare("SELECT * FROM Educacion_EvaluacionesAsistencia where IdEvaluacion = $Value[0]");
                                        $EvaluacionAsistencia->execute();                                        
                                                                                
                                        if($Desercion->rowCount() > 0)                                                                                        
                                            echo "<td style='text-align: center; color: red'> - </td>";
                                                       
                                        else
                                        {
                                            
                                                    
                                        if($EvaluacionAsistencia->rowCount() == 1)
                                        {
                                            $NotaAsistencia = $bdd->prepare("SELECT * FROM Educacion_Asistencias
                                                                              Where IdComponente = :idC and IdPeriodo = :idP and IdSeccion = :idSec and IdEstudiante = :idUsu 
                                                                              GROUP BY Fecha ORDER BY Fecha ASC ");
                                                                              
                                            $NotaAsistencia->bindParam(':idC',$_SESSION['Component'] ) ;
                                            $NotaAsistencia->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
                                            $NotaAsistencia->bindParam(':idSec',$_SESSION['IdSec'] ) ;
                                            $NotaAsistencia->bindParam(':idUsu', $alumno[0]);
                                            $NotaAsistencia->execute();
                                            
                                            if($DatosAsistencias->rowCount() == 0)                                            
                                                $PromedioAsistencia = " - ";
                                                
                                            else
                                            {
                                                $NotaE = number_format(($NotaAsistencia->rowCount() / $DatosAsistencias->rowCount()) * 10,2);
                                            }
                                            
                                            echo "<td style='text-align: center;'>
                                                  <input class='tdnotas' type='hidden' name='$alumno[0]_$Value[0]' value='$NotaE' style='width: 40px;' required='true' title='Nota requerida' />$NotaE</td>";
                                        }
                                        else
                                        {
                                            $nota = $bdd->prepare("Select Nota from Educacion_Notas where IdEvaluacion = :idEva and IdUsuario = :idUsu");
                                            $nota->bindParam(':idEva', $Value[0]);
                                            $nota->bindParam(':idUsu', $alumno[0]);
                                            $nota->execute();                        
                                            $n = $nota->fetch();                    
                                            
                                            $NotaE = number_format($n[0],2);
                                            echo "";
                                            
                                                $Desercion = $bdd->prepare("Select * from Educacion_Desercion where IdCiclo = :idI and IdEstudiante = :idU");
                                                $Desercion->bindParam(':idI', $_SESSION["Ciclos"]);
                                                $Desercion->bindParam(':idU', $alumno[0]);
                                                $Desercion->execute();
                                                
                                                
                                                if($Desercion->rowCount() == 0)                                                
                                                    echo "<td style='text-align: center;'>
                                                              <input class='tdnotas' type='text' name='$alumno[0]_$Value[0]' value='$NotaE' style='width: 40px;' required='true' title='Nota requerida' />
                                                          </td>";
                                                        
                                                else
                                                    echo "<td style='text-align: center; color: red'> - </td>";
                                                        
                                            echo "";                
                                        }
                                        
                                        
                                            $PromedioE = $PromedioE + ($NotaE * ($Value[1] / 100));
                                        }
                                    }
                                }
                                else
                                    echo "<td>-</td>";
                                
                                if($PromedioE < 6)
                                    $Color = "Red";
                                    
                                else
                                    $Color = "Green";
                                
                                if($Desercion->rowCount() > 0)
                                {
                                    echo "<th style='color: red'>-<th>";
                                    $TotalDesercion++;
                                }
                                else
                                    echo "<th style='color: $Color'>".number_format($PromedioE,2)."<th>";
                                
                                echo "</tr>";
                                
                                $PromedioS += number_format($PromedioE,2);
                           }
                           
                           $Colspan = count($ArrayE);
                           
                           
                           $PromedioS = $PromedioS / ($listaAlumnos->rowCount() - $TotalDesercion );
                           
                           if($PromedioS < 6)
                                $Color = "Red";
                                    
                           else
                                $Color = "Green";
                                    
                            
                           echo "<tr>
                                     <td colspan='2' style='text-align: center;'><input class='boton' style='width: 150px; margin-top: 20px; margin-bottom: 20px' type='submit' name='Enviar' value='Guardar todas las Notas' onclick='return ValidarNota(this.form);' /></td>
                                     <th colspan='$Colspan' class='tdleft' style='color: blue; text-align: right; margin-top: 20px'>Promedio: </th>
                                     <th style='color: $Color; margin-top: 20px'>".number_format($PromedioS,2)."</th>   
                                 </tr>";
                       }
                       else
                            echo "<tr><td colspan='4' style='color:red; text-align: center;'>No hay Alumnos registrados a esta Secci�n</td></tr>";
                       
                        
                       echo "<tr>
                                <td colspan='2'>
                                    <table style=' width: 90%' >
                                        <tr><td colspan='2' style='width: 70%;' class='tdleft' > Total en deserci�n: </td><td>$TotalDesercion</td></tr>
                                        <tr><td colspan='2' style='width: 70%;' class='tdleft'> Total de Estudiantes: </td><td>".$listaAlumnos->rowCount()."</td></tr>
                                    </table>
                                </td>
                            </tr>";
                       
                ?>
                
                
            </table> 
            </form> 
            </td>
        </tr>          
        
</table>
</div>