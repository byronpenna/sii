<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administraci�n</a> ->
    <a href="/FUSALMO.php?d=Component" class='flecha' style='text-decoration:none'>�rea</a> ->
    <a href="/FUSALMO.php?d=AttendanceList" class='flecha' style='text-decoration:none'>Asistencias</a>
</div>
<script>
function CargarLista(id, mes, a�o)
    {
        var code = document.getElementById(id).value;
        var code2 = mes;
        var code3 = a�o;   
            	
        document.getElementById('AsistenciaList').innerHTML = "<center><em style='color: green;'>Cargando datos...</em></center>";

        $.get("DatoFUSALMO/AsistenciaStudents.php", { code: code, code2: code2, code3: code3 },
      		function(resultado)
      		{
                document.getElementById('AsistenciaList').innerHTML = "";                        
     			if(resultado == false)     			
    				alert("Error");
     			
     			else            			        			    
    				$('#AsistenciaList').append(resultado);	            			
      		}
       	);        
    }
</script>
<?php
    unset($_SESSION['fecha']);
    
    if($_SESSION['IdDato'] != "" || $_SESSION['Component']!= "")
    {        
        $Dato = $bdd->prepare("SELECT d.IdDatoFUSALMO, d.Nombre, p.IdPeriodo, p.Nombre, c.IdComponente, c.NombreComponente
                               FROM DatoFusalmo as d 
                               inner join Educacion_Periodos as p on p.IdDatoFUSALMO = d.IdDatoFUSALMO
                               Inner join Educacion_Componente as c on c.IdPeriodo = p.IdPeriodo                             
                               WHERE d.IdDatoFUSALMO = :id and c.IdComponente = :idc");
        $Dato->bindParam(':id', $_SESSION['IdDato']);
        $Dato->bindParam(':idc', $_SESSION['Component']);
        $Dato->execute();
        
        $DatoView = $Dato->fetch();
    }
    else
            Redireccion("../FUSALMO.php");
            

        $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                             FROM  Institucion as inst 
                                             inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                             where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
        
        $DatosInstituciones->bindParam(':idIns', $_SESSION['IdIns']);
        $DatosInstituciones->bindParam(':idSec', $_SESSION['IdSec']);   
        
        $DatosInstituciones->execute();
        $InfoSeccion = $DatosInstituciones->fetch();                                      
        
        $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2, ins.IdInscripcion
                                       FROM Educacion_Estudiantes  as est
                                       inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                       INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                       where ins.IdCiclo = :idC and ins.IdInstitucion = :idI and ins.IdSeccion = :idS
                                       ORDER BY  jov.Apellido1 ASC, jov.Apellido2 ASC  ");
                              
        $listaAlumnos->bindParam(':idC', $_SESSION['Ciclos']);
        $listaAlumnos->bindParam(':idI', $_SESSION['IdIns']);
        $listaAlumnos->bindParam(':idS', $_SESSION['IdSec']);               
        $listaAlumnos->execute(); 
        $TotalEstudiantes = $listaAlumnos->rowCount();         
        
        function diaSemana($ano,$mes,$dia)
        {
        	// 0->domingo	 | 6->sabado
        	$dia= date("w",mktime(0, 0, 0, $mes, $dia, $ano));
        		return $dia;
        }
        
        function getMonthDays($Month, $Year)
        {
           //Si la extensi�n que mencion� est� instalada, usamos esa.
           if( is_callable("cal_days_in_month"))
           {
              return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
           }
           else
           {
              //Lo hacemos a mi manera.
              return date("d",mktime(0,0,0,$Month+1,0,$Year));
           }
        }
?>

<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
<table style='width: 90%; margin-left: 5%; margin-top: 10px;'>             
        <tr><td colspan="2" style="color: blue;">Asistencias del Area<br /><?php echo "$DatoView[1]<br />$DatoView[3]";?> </td></tr>
        <tr><td colspan="2" style="color: green; text-align: right;"><?php echo "$InfoSeccion[1]<br />$InfoSeccion[3]";?> </td></tr>

        
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td colspan="2">
                <?php
	                    $DatosAsistencias = $bdd->prepare("SELECT * FROM Educacion_Asistencias
                                                          Where IdComponente = :idC and IdPeriodo = :idP and IdSeccion = :idSec 
                                                          GROUP BY Fecha ORDER BY Fecha ASC ");
                                                          
                        $DatosAsistencias->bindParam(':idC',$_SESSION['Component'] ) ;
                        $DatosAsistencias->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
                        $DatosAsistencias->bindParam(':idSec',$_SESSION['IdSec'] ) ;
                        $DatosAsistencias->execute();
                ?>
                <form method="post" action="DatoFUSALMO/AsistenciaABM.php">
                <table  style="width: 100%;">
                <tr>
                    <td style="width: 40%;">
                        
                        <table style="width: 100%; text-align: center;" >
                        <?php
                                if($DatosAsistencias->rowCount() > 0)
                                {
                                    echo "<tr><th style='width: 5%;'></th><th>Fecha</th><th>Total en <br />Secci�n</th><th>Total <br />Asistidos</th></tr>";
        
                                    while($DataF = $DatosAsistencias->fetch())
                                    {
                                        $fecha = "$DataF[5]";
                                        list($a�o, $mes, $d�a) = split('[/.-]', $fecha);
                                        
                                        echo "<tr><td><input type='radio' value='$DataF[5]' name='Fecha' OnClick='CargarLista(\"$d�a\",\"$mes\",\"$a�o\")' required='true' /></td><td>$DataF[5]</td><td>$TotalEstudiantes</td>";
                	                   
                                       $DatosFecha = $bdd->prepare("SELECT * FROM Educacion_Asistencias
                                                                    WHERE IdComponente = :idC and IdPeriodo = :idP and IdSeccion = :idSec and Fecha = :fecha
                                                                    ORDER BY Fecha ASC ");
                                       $DatosFecha->bindParam(':idC',$_SESSION['Component'] ) ;
                                       $DatosFecha->bindParam(':idP',$_SESSION['IdPeriodo'] ) ;
                                       $DatosFecha->bindParam(':idSec',$_SESSION['IdSec'] ) ;
                                       $DatosFecha->bindParam(':fecha', $DataF[5]);
                                       $DatosFecha->execute();
                                       
                                       echo "<td>". $DatosFecha->rowCount()."</td></tr>";
                                    }
                                }
                                else                        
                                    echo "<tr><td style='color: red'>No hay Asistencias</td></tr>";
                                
                        ?>
                        </table>
                    </td>
                    <td style="">
                        <table style="width: 100%; text-align: center; padding: 10px;">
                            <tr><th>Cambia la fecha de la Asistencia<br />(A�o-Mes-D�a)</th></tr>
                            <tr><td><input style="text-align: center;" type="date" id="FechaUpdate" name="FechaUpdate" value="" pattern="^\d{4}\-\d{2}\-\d{2}$" /></td></tr>
                            <tr><td><input style="width: 150px;" type="submit" name="Enviar" class="boton" value="Cambiar Fecha" /></td></tr>
                            <tr><td><input style="width: 150px;" type="submit" name="Enviar" class="botonE" value="Limpiar Asistencia" onclick="return confirm('Deseas Borrar esta Asistencia?')" /></td></tr>                                                                           
                        </table>
                    </td> 
                </tr>
                </form>
                </table>             
            </td>
        </tr>
        <tr><td colspan="2"> <hr color='skyblue' /></td></tr>
        <tr><td colspan="2" style="height: 20px; background-color: white;"></td></tr>        
        <tr><td  colspan="2" style="color: blue; text-align: right;" >Area: <?php echo $DatoView[5];?> </td></tr>  
        <tr>
            <td valign="top">
                    <?php

                           if(isset($_POST['Cambio']))
                           {
                                if($_POST['Cambio'] == "->")
                                {
                                    if($_POST['mes'] == 12)
                                    {
                                        $a�o = $_POST['a�o'] + 1;
                                        $mes = 1;
                                    }
                                    else
                                    {
                                        $mes = $_POST['mes'] + 1;
                                        $a�o = date("Y");
                                    }
                                }
                                else
                                {
                                    if($_POST['mes'] == 1)
                                    {
                                        $a�o = $_POST['a�o'] - 1;
                                        $mes = 12;
                                    }
                                    else
                                    {
                                        $mes = $_POST['mes'] - 1;
                                        $a�o = date("Y");
                                    }
                                }
                           }    
                           else
                           {
                                $mes = date("m");
                                $a�o = date("Y");
                           }
                                
                           $dia = date("d"); 
                           
                           if(strlen($mes) == 1)
                                $mes = "0" . $mes;
	                          
                             
                           $TotalDias = getMonthDays($mes, $a�o); 
                                                
                           if($mes == "01") $Mes = "Enero";
                           if($mes == "02") $Mes = "Febrero";
                           if($mes == "03") $Mes = "Marzo";
                           if($mes == "04") $Mes = "Abril";
                           if($mes == "05") $Mes = "Mayo";
                           if($mes == "06") $Mes = "Junio";
                           if($mes == "07") $Mes = "Julio";
                           if($mes == "08") $Mes = "Agosto";
                           if($mes == "09") $Mes = "Septiembre";
                           if($mes == "10") $Mes = "Octubre";
                           if($mes == "11") $Mes = "Noviembre";
                           if($mes == "12") $Mes = "Diciembre";     
                    ?>            
                <table style="width: 100%; text-align: center;">
                    <tr><form method="post" action="#">
                        <input type="hidden" name="mes" value="<?php echo $mes?>" />
                        <input type="hidden" name="a�o" value="<?php echo $a�o?>" />
                        <td><input type="submit" style="width: 30px;" class="botonG" name="Cambio" value="<-" /></td>
                        <td colspan="5"> Mes de <?php echo "$Mes $a�o"?></td>
                        <td><input type="submit" style="width: 30px;" class="botonG" name="Cambio" value="->" /></td>
                        </form>
                    </tr>
                    <tr><td>Do</td><td>Lu</td><td>Ma</td><td>Mi</td><td>Ju</td><td>Vi</td><td>S�</td></tr>
                    <?php   
                            $aux = 1;
                            for($i = 0; $i < 6; $i++)
                            {
                                echo "<tr>";
                                
                                for($j = 0; $j < 7; $j++)
                                {
                                    echo "<td>";
                                        
                                        if(diaSemana($a�o,$mes,$aux) == $j)
                                        {
                                            if($aux <= $TotalDias)
                                            {   
                                                if(strlen($aux) == 1)
                                                $aux = "0" . $aux;
                                                
                                                
                                                if($aux == $dia && $mes == date("m") && $a�o == date("Y"))
                                                    echo "<input type='button' class='botonG' style='width: 30px; text-align: center;' id='$aux' OnClick='CargarLista(\"$aux\",\"$mes\",\"$a�o\")' value='$aux' />" ;
                                                
                                                else
                                                    echo "<input type='button' class='boton' style='width: 30px; text-align: center;' id='$aux' OnClick='CargarLista(\"$aux\",\"$mes\",\"$a�o\")' value='$aux' />" ;
                                                
                                                $aux++;
                                            }
                                        }
                                    echo "</td>";
                                }
                                
                                echo "</tr>";
                            }
                    ?>
                    <tr><td colspan="7" style="text-align: center;"><a href="FUSALMO.php?d=AttendanceList"><input type="button" class="botonG" value="Hoy" style="width: 50px;"  /></a></td></tr>
                </table>
            </td>
            <td style="width: 60%;">
                <form name="AsistenciaForm" id="AsistenciaForm" method="post" action="DatoFUSALMO/AsistenciaABM.php"  >
                <div id="AsistenciaList">
                    <center>
                    <?php
                	     if(isset($_GET['n']))
                         {
                            if($_GET['n'] == 1)
                                echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: center;'>Asistencia Inscrita Exitosamente</div>";
                                
                            if($_GET['n'] == 2)
                                echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: center;'>Asistencia Eliminada Correctamente</div>";            
                            
                            if($_GET['n'] == 3)
                                echo "<div id='Notificacion' name='Notificacion' style='color:blue; text-align: center;'>Asistencia Actualizada Correctamente</div>";
                                                              
                         }
                    ?>
                    <em style="color: blue;">Esperando datos...</em></center>
                </div>
                <?php
                    if($asistencia == 0)   
                       $ABM = "Guardar";
                    
                    else
                       $ABM = "Actualizar";
                ?>
                <center><input class='boton' type='submit' name='Enviar' value='<?php echo $ABM;?>' /></center>
                </form>   
                
            </td>
        </tr>
        <tr style="height: 30px;"><td></td></tr>

</table>

</div>