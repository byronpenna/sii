<?php

/**
 * @author Manuel
 * @copyright 2013
 */
 
?>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administración</a> ->
    <a href="/FUSALMO.php?d=Period" class='flecha' style='text-decoration:none'>Periodos</a> ->
</div>


<div style="width: 90%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; margin-left: 5%; border-color: skyblue; border-style: groove; ">
        
        <table style="width: 80%; margin-left: 10%;">
        <form action="DatoFUSALMO/PeriodoABM.php" method="post">
        
            <?php
    	           if($_SESSION['IdCiclo'] != "")
                   {
                        $idC = $_SESSION['IdCiclo'];
                        echo "<input type='hidden' value='$idC' name='IdC'  />";
                        
                        $buscar = $bdd->prepare("SELECT * FROM Educacion_Ciclos where IdCiclo = $idC");       
                        $buscar->execute();
                        
                        $nombreCiclo = $buscar->fetch();
                        $ciclo = "Nombre Ciclo: " . $nombreCiclo[2];
                   }
                   else 
                        $ciclo = "";
            ?>
            <tr><td style="padding-top: 20px;"><h2>Nuevo Periodo</h2></td><td style="text-align: right; color: blue;"><?php echo $ciclo?></td></tr>
            <tr>
                <td style="text-align: right; padding-right: 20px; padding-top: 15px;">Nombre para el Periodo:</td>
	            <td style="color: red;"><input type="text" name="Nombre" id="Nombre" value="" style="width: 200px;" required="true" title="Campo Requerido" />*</td>
            </tr>
            <tr>
                <td style="text-align: right; padding-right: 20px; padding-top: 15px;">Fecha de Inicio :</td>
	            <td style="color: red;"><input type="week" name="inicio" required="true" title="Seleccione la semana inicial" />*</td>
            </tr>            
            <tr>
                <td style="text-align: right; padding-right: 20px; padding-top: 15px;">Fecha de Finalización:</td>
	            <td style="color: red;"><input type="week" name="fin" required="true" title="Seleccione la semana final" />*</td>
            </tr>                  
            <tr><td colspan="2" style="text-align: center; padding-top: 15px;"><input type="submit" class="boton" name="Enviar" value="Agregar"   /></td></tr>   
        </form>
        </table>
</div>