<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/FUSALMO.php?d=dato" class='flecha' style='text-decoration:none'>Dato Fusalmo</a> ->
    <a href="/FUSALMO.php?d=Adm" class='flecha' style='text-decoration:none'>Administración</a> ->
    <a href="/FUSALMO.php?d=ReportStudents" class='flecha' style='text-decoration:none'>Reporteria</a> 
</div>

<?php

/**
 * @author Gilbetor
 * @copyright 2014
 */
 
         if(!isset($_POST['Enviar']))
            Redireccion("../FUSALMO.phpd=Adm");
        
        $DatosInstituciones = $bdd->prepare("SELECT inst.IdInstitucion, inst.NombreInstitucion, sec.IdSeccion, sec.SeccionEducativa
                                             FROM  Institucion as inst 
                                             inner join Institucion_Seccion as sec on inst.IdInstitucion = sec.IdInstitucion
                                             where sec.IdInstitucion = :idIns and sec.IdSeccion = :idSec");
        
        $DatosInstituciones->bindParam(':idIns', $_POST['IdIns']);
        $DatosInstituciones->bindParam(':idSec', $_POST['IdSec']);   
        $DatosInstituciones->execute();
        $InfoSeccion = $DatosInstituciones->fetch();  
            
          
        $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario, jov.Nombre, jov.Apellido1, jov.Apellido2, ins.IdInscripcion
                                       FROM Educacion_Estudiantes  as est
                                       inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                       INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                       where ins.IdCiclo = :idC and ins.IdInstitucion = :idI and ins.IdSeccion = :idS
                                       ORDER BY  jov.Apellido1 ASC, jov.Apellido2 ASC  ");
                  
        $listaAlumnos->bindParam(':idC', $_SESSION['Ciclos']);
        $listaAlumnos->bindParam(':idI', $_POST['IdIns']);
        $listaAlumnos->bindParam(':idS', $_POST['IdSec']);               
        $listaAlumnos->execute();

        $asis = isset($_POST['Componente']) ? $_POST['Componente'] : array();
        $rows = count($asis) + 3;    

        
        

        if($_POST['TipoV'] == "Evaluaciones")
            include("DatoFUSALMO/ReporteEva.php");
            
        else
            include("DatoFUSALMO/ReporteAsi.php");
?>