<?php

require '../net.php';

if(isset($_POST['enviar']))
{
    if($_POST['enviar'] == "Agregar")
    {
        $Dato = $bdd->prepare("Insert into InscripcionDatoFusalmo values (null , :IdJoven, :IdDato)");
        
        $Dato->bindParam('IdJoven', $_SESSION["IdUsuario"]);
        $Dato->bindParam('IdDato', $_POST['IdDatoFusalmo']);        
        $Dato->execute();               
        
        LogReg($_SESSION["IdUsuario"], "Dato FUSALMO", "Inscripción de Dato Fusalmo a " . $_POST['IdDatoFusalmo'], $bdd);
        
        Redireccion("../FUSALMO.php?n=1");
    }
    
    if($_POST['enviar'] == "Registrar")
    {           
        $Dato = $bdd->prepare("Select IdInscripcion from Educacion_Inscripcion 
                               where 
                               IdCiclo = :IdC and
                               IdInstitucion = :IdI and  
                               IdSeccion = :IdS");
        
        $Dato->bindParam('IdC', $_POST["Ciclos"]);
        $Dato->bindParam('IdI', $_POST['Institucion']);
        $Dato->bindParam('IdS', $_POST['Grupo']);        
        $Dato->execute();              
        
        $Id = $Dato->fetch();
        
        $IdU = $_SESSION["IdUsuario"];
        $Insert = $bdd->prepare("Insert into Educacion_Estudiantes Values(null,$Id[0],$IdU)");
        $Insert->execute(); 
        
        $Dato = $bdd->prepare("Insert into InscripcionDatoFusalmo values (null , $IdU, :IdDato )");        
        $Dato->bindParam('IdDato', $_POST['dato']);        
        $Dato->execute();     
        
        LogReg($_SESSION["IdUsuario"], "Dato FUSALMO", "Registro de Dato Fusalmo a " . $_POST['IdDatoFusalmo'] . " del Usuario $Id[0]", $bdd);
        
        Redireccion("../FUSALMO.php?n=1");
    }    
}

if(isset($_GET['i'])){

        $Dato = $bdd->prepare("Delete from InscripcionDatoFusalmo where IdInscripcion = :id");        
        $Dato->bindParam('id', $_GET['i']);                
        $Dato->execute();               
        
        LogReg($_SESSION["IdUsuario"], "Dato FUSALMO", "Eliminación de Inscripción del Usuario: " . $_GET['i'], $bdd);
        
        Redireccion("../FUSALMO.php?n=2");        
}
?>