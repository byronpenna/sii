<?php

    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=ReporteNotas.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';


    $CE = $_POST['ce'];
    $Periodo= $_POST['ip'];
    
    
    
    $Componentes = $bdd->prepare("SELECT * FROM Educacion_Componente where IdPeriodo = $Periodo");
    $Componentes->execute();
    
    $Secciones = $bdd->prepare("SELECT s.*, i.IdCiclo FROM Educacion_Inscripcion as i 
                                INNER join Educacion_CicloPeriodo as c on i.IdCiclo = c.IdCiclo 
                                INNER JOIN Institucion_Seccion as s on s.IdSeccion = i.IdSeccion 
                                where c.IdPeriodo = $Periodo and s.IdInstitucion = $CE 
                                ORDER BY SeccionEducativa ASC");
    $Secciones->execute();
    $ArraS = array();
    
    while($DataS = $Secciones->fetch())
    {
        array_push($ArraS, $DataS[2]);
        array_push($ArraS, $DataS[0]);
        
        $ic = $DataS[3];
    }
    
    $i = 0;
    echo "<table>
          <tr>";
    
    while($DataC = $Componentes->fetch())
    {
        echo "<td>";
                    
        echo "<table>
                    <tr><td colspan='7' style='text-align: center;'><h3>$DataC[2]</h3></td></tr>
                    <tr><th style='width: 30%'>Secciones</th><th>Promedio</th><th>Asistencia</th><th>Aprobados</th><th>Reprobados</th><th>Deserción</th><th>Total Estudiantes</th></tr>";
        
        foreach($ArraS as $value)
        {                
            if($i == 0)
            {
                echo "<tr><th style='width: 40%'>$value</th>";
                $i++;    
            }
            else
            {
                /** Asistencias **/
                $DatosAsistencias = $bdd->prepare("SELECT * FROM Educacion_Asistencias
                                                   WHERE IdComponente = $DataC[0] and IdPeriodo = $Periodo and IdSeccion = $value 
                                                   GROUP BY Fecha ORDER BY Fecha ASC ");
                $DatosAsistencias->execute();   
                
                                            
                /** Asistencias **/                                                 
                $Evaluaciones = $bdd->prepare("SELECT * FROM Educacion_Evaluaciones where IdComponente = $DataC[0] and IdPeriodo = $Periodo and IdSeccion = $value");
                $Evaluaciones->execute();
                $ArrayE = array();
                 
                while($DataE = $Evaluaciones->fetch())    
                    array_push($ArrayE, array($DataE[0],$DataE[4]));
                
                $listaAlumnos = $bdd->prepare("SELECT jov.IdUsuario 
                                               FROM Educacion_Estudiantes  as est 
                                               inner join Educacion_Inscripcion as ins on est.IdInscripcion = ins.IdInscripcion
                                               INNER JOIN joven as jov on est.IdUsuario = jov.IdUsuario        
                                               where ins.IdCiclo = $ic and ins.IdSeccion = $value");
                                              
                $listaAlumnos->execute();
                
                $TotalEstudiantes = $listaAlumnos->rowCount();
                $TotalDesercion = 0;
                $TotalAsistencia = 0;
                $TotalPromedio = 0;
                
                $TotalAprobados = 0;
                $TotalReprobados = 0;    
                
                $AuxAsistencia = 0;
                $AuxPromedio = 0;
                
                while($DataEstudiante = $listaAlumnos->fetch())
                {

                    $Desercion = $bdd->prepare("Select * from Educacion_Desercion where IdPeriodo = $Periodo and IdEstudiante = $DataEstudiante[0]");                    
                    $Desercion->execute();
                    
                    if($Desercion->rowCount() == 1)
                    {
                        $TotalDesercion++;
                    }                    
                    else
                    {
                        $Promedio = 0;
                        foreach($ArrayE as $ValueEva)
                        {
                            $EvaluacionAsistencia = $bdd->prepare("SELECT * FROM Educacion_EvaluacionesAsistencia where IdEvaluacion = $ValueEva[0]");
                            $EvaluacionAsistencia->execute();
                            
                            if($EvaluacionAsistencia->rowCount() == 1)
                            {                        
                                   $NotaAsistencia = $bdd->prepare("SELECT * FROM Educacion_Asistencias
                                                                    Where IdComponente = $DataC[0] and IdPeriodo = $Periodo and IdSeccion = $value and IdEstudiante = $DataEstudiante[0] 
                                                                    GROUP BY Fecha 
                                                                    ORDER BY Fecha ASC ");
                                   $NotaAsistencia->execute(); 

                                   if($DatosAsistencias->rowCount() > 0)
                                   {                                       
                                       $NotaE = number_format(($NotaAsistencia->rowCount() / $DatosAsistencias->rowCount()) * 10,2);
                                       $AuxAsistencia += $NotaE;                                       
                                   }
                                   else
                                   {
                                       $NotaE = 0;
                                       $AuxAsistencia += $NotaE;                                         
                                   }
                                   
                                   $Promedio += $NotaE * ($ValueEva[1] / 100);
                            }                                      
                            else
                            {
                                        $nota = $bdd->prepare("Select Nota from Educacion_Notas where IdEvaluacion =  $ValueEva[0] and IdUsuario = $DataEstudiante[0]");
                                        $nota->execute();                        
                                        $n = $nota->fetch(); 
                                        $NotaE = $n[0];          
                                        $Promedio += $NotaE * ($ValueEva[1] / 100);                                                                                          
                            }                                        
                        }
                        $AuxPromedio += $Promedio;                        
                        
                        if($Promedio >= 6)
                            $TotalAprobados++;
                            
                        else
                            $TotalReprobados++;
                        
                        
                    }
                    
                    $TotalAsistencia = $AuxAsistencia / ($TotalEstudiantes - $TotalDesercion) * 10;
                    $TotalPromedio = $AuxPromedio / ($TotalEstudiantes - $TotalDesercion);
                }

                echo "<th>".  number_format($TotalPromedio, 2) . "</th><th>".  number_format($TotalAsistencia, 2) . "%</th><th>$TotalAprobados</th><th>$TotalReprobados</th><th>$TotalDesercion</th><th>$TotalEstudiantes</th></tr>";
                $i = 0;
            }
        }
                
        echo "</table>
                  <br /><br />";
                  
        echo "</td>";
    }
    
    echo "</tr>
    </table>";    
?>