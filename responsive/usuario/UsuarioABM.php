<?php
require '../net.php';

if(isset($_POST['Enviar']))
{
        if($_POST['Enviar'] == "Ingresar" || $_POST['Enviar']  == "Agregar")
        {   
            $Buscar = $bdd->prepare('Select * from usuario where NombreUsuario = :UserName');            
            $Buscar->bindParam(':UserName',  $_POST['Nombre']);   
            $Buscar->execute();
            
            if($Buscar->rowCount() == 0)
            {
            
            if( $_POST['Enviar'] == "Ingresar")
            $Tipo = "Joven";
            
            else 
            $Tipo = $_POST['tipoUsuario'];
                        
            $inserta = $bdd->prepare('INSERT INTO usuario VALUES (NULL , :UserName,  :UserType, :UserPass, :Localidad)');            
            $inserta->bindParam(':UserName',  $_POST['Nombre']);             
            $inserta->bindParam(':UserType',  $Tipo);
            $inserta->bindParam(':UserPass',  $_POST['pass1']);
            $inserta->bindParam(':Localidad', $_POST['Localidad']);
            $inserta->execute();
           
           
                if( $_POST['Enviar'] == "Ingresar")
                {
                        $DatosUsuario = $bdd->prepare('SELECT * from usuario where NombreUsuario	 = :UserName');
                        $DatosUsuario->bindParam(':UserName',  $_POST['Nombre']);    
                        $DatosUsuario->execute();
                                                                       
                        $Usuario = $DatosUsuario->fetch() ;                     
                        
                        $_SESSION["autenticado"] = true;                    
                        $_SESSION["IdUsuario"]     = $Usuario[0];
                        $_SESSION["Usuario"]       = $_POST['Nombre'];
                        $_SESSION["TipoUsuario"]   = $Tipo;
        	            $_SESSION["Contraseņa"]    = $_POST['pass1'];
                        $_SESSION["Localidad"]     = $_POST['Localidad'];
                        
                        echo $_SESSION["Usuario"] . "  " . $_SESSION["IdUsuario"];
                        Redireccion("../Main.php");
                }         
                
                else
                         Redireccion("../Main.php?l=UsuL");
                
            
            }else
                Redireccion("UsuarioForm.php?e=1&nu=" . $_POST['Nombre']);
                     
        }
        else if($_POST['Enviar'] == "Actualizar")   
        {   
            
            if($_POST['Nombre'] != $_SESSION["Usuario"])
            {
                    $DatosUsuario = $bdd->prepare('SELECT * from usuario where NombreUsuario = :UserName');
                    $DatosUsuario->bindParam(':UserName',  $_POST['Nombre']);    
                    $DatosUsuario->execute();
                    
                    if($DatosUsuario->rowCount() > 0)
                    {
                        if(isset($_POST['cambiotipo']))
                            $nombreRepetido = false;
                            
                        else
                            $nombreRepetido = true;
                    }
                        
                        
                    else
                        $nombreRepetido = false;                        
            }
            
            if(!$nombreRepetido)
            {
                
                $update = $bdd->prepare("Update usuario Set  NombreUsuario = :UserName , Contraseņa = :UserPass , Localidad= :Localidad, TipoUsuario = :TipoU 
                                         WHERE Id = :id ");    
                                                 
                $update->bindParam(':UserName',  $_POST['Nombre']);                         
                $update->bindParam(':UserPass',  $_POST['pass1']);
                $update->bindParam(':Localidad', $_POST['Localidad']);
                $update->bindParam(':TipoU', $_POST['tipoUsuario']);    
                $update->bindParam(':id', $_POST['iu']);                            
                $update->execute();
    
                $_SESSION["Usuario"]       = $_POST['Nombre'];
                $_SESSION["Contraseņa"]    = $_POST['pass1'];
                $_SESSION["Localidad"]     = $_POST['Localidad'];
                        
                Redireccion("../Main.php");
                
            }else
                Redireccion("UsuarioForm.php?e=1");
                     
            

                
        }
        else if($_POST['Enviar'] == "Eliminar")   
        {
            $Eliminar = $bdd->prepare('Delete from usuario where Id = :Id');            
            $Eliminar->bindParam(':Id',  $_POST['idUsu']);   
            $Eliminar->execute();   
            
            Redireccion("../Main.php?l=UsuL&n=3");         
        }
}
else
Redireccion("../index.php");
?>