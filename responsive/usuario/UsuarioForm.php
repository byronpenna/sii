<?php
	require '../net.php';
?>
<html>
<head>
<title>Inicio SIIF</title>
<?php 
include("../lib.php");
?>
	<script type="text/javascript">
        $(document).ready(function () {

            $(document).ready(function () {
                $('#Notificacion').fadeOut(2500);
            });

        });
    </script>
</head>
<body>
<div class="col-sm-8 col-sm-offset-2 text">
    <div class="col-sm-8 col-sm-offset-2 text"></div>
        <div class="inner2-bg"><div class="col-sm-8 col-sm-offset-2 text">
    
            <h1><span>Sistema de Informaci&oacute;n Institucional  <br />de Polideportivos Don Bosco</span></h1>
        </div>    
        <div class="clr"></div>
        </div></div>    
    <div class="fbg"></div>    
    <div class="col-sm-8 col-sm-offset-2 text">   
        <div class="col-sm-8 col-sm-offset-2 text"> <!-- Aqui empieza a trabajar -->
            <div><br />
            <?php
                   if($_SESSION["IdUsuario"] != "")
                   {	                   
                       if(isset($_POST['Enviar']))
                            $Id = $_POST['iu'];
                       
                       else
                            $Id = $_SESSION["IdUsuario"];
                        
                       $lista = $bdd->prepare("SELECT * FROM usuario WHERE Id = :id ");
                       $lista->bindParam(':id' , $Id);
	                   $lista->execute();
                       
                       $datosUsuario = $lista->fetch();  
                       
                       $regresar = "../Main.php";
                       $texto = "<- Regresar al Inicio";
                       $abm = "Actualizar";                        
                   }
                   else
                   {
                       $regresar = "../index.php";
                       $texto = "<- Regresar a Inicio";
                       $abm = "Ingresar"  ;
                                      
            ?>
            <ul>Indicaciones:
	               <li>Se lo mas sincero, tus datos seran muy confidenciales y nos serviran para darte un mejor servicio.</li>
	               <li>trata de llenar todos los campos posibles.</li>
	               <li>Los <span style="color: red;">*</span> son campos Obligatorios. </li>
                </ul>            
            <?php }?>
            <hr style="color: skyblue; background-color: skyblue; height: 5px;" />
            <h2>Usuarios del Sistema</h2><br />
            <form action="UsuarioABM.php" method="post" enctype="multipart/form-data" name="Proceso" id="Proceso">
            <table style="width: 80%; margin-left: 10%; margin-bottom: 20px;">
            <tr><td style="text-align: right; padding-right: 20px;">Escribe un nombre de Usuario:</td>
            
                <td style="color: red;"><input required="true" title="Escribe tu Nombre de Usuario"  type="text" id="Nombre" name="Nombre" style="width: 150px;" value="<?php echo $datosUsuario[1]?>"/>*</td>
                
                <?php
	               if(isset($_GET['e']))
                   {
                        if($_GET['e'] == 1)
                            echo "<td style='color: red;' id='Notificacion' ><br /><div>Nombre de Usuario existente, <br />trata con otro</div></td>";
                   }
                ?></tr>

            <tr><td style="text-align: right; padding-right: 20px;">Tu nueva contrase&ntilde;a:</td>
                <td style="color: red;"><input required="true" title="Escribe tu password" type="password" id="pass1" name="pass1" style="width: 150px;" value="<?php echo $datosUsuario[3]?>"/>*</td></tr>                
                
            <tr><td style="text-align: right; padding-right: 20px;">Confirmemos tu contrase&ntilde;a:</td>
                <td style="color: red;"><input required="true" title="Reescribe tu password" type="password" id="pass2" name="pass2" style="width: 150px;" value="<?php echo $datosUsuario[3]?>"/>*</td></tr>
            
            <?php 
                  if($_SESSION["TipoUsuario"] == "Administrador")
                  { 
            ?>
                    <tr><td style="text-align: right; padding-right: 20px;">Tipo de Usuario</td>
                    <td>
                        <input type='hidden' name='cambiotipo' value='true' />
                        <select name="tipoUsuario" id="TipoUsuario">    
                            
                        <?php
                            
                            if($datosUsuario[2] == "Administrador") echo "<option value='Administrador' selected>Administrador</option>"; else echo "<option value='Administrador' >Administrador</option>";
                            if($datosUsuario[2] == "Consultor") echo "<option value='Consultor' selected>Consultor</option>"; else echo "<option value='Consultor' >Consultor</option>";
                            if($datosUsuario[2] == "RRHH") echo "<option value='RRHH' selected>RRHH</option>"; else echo "<option value='RRHH' >RRHH</option>";
                            if($datosUsuario[2] == "Coordinador") echo "<option value='Coordinador' selected>Coordinador</option>"; else echo "<option value='Coordinador' >Coordinador</option>";
                            if($datosUsuario[2] == "Gestor") echo "<option value='Gestor' selected>Gestor</option>"; else echo "<option value='Gestor' >Gestor</option>";
                            if($datosUsuario[2] == "Joven") echo "<option value='Joven' selected>Joven</option>"; else echo "<option value='Joven' >Joven</option>";
                            if($datosUsuario[2] == "USAID-CI") echo "<option value='USAID-CI' selected>USAID-CI</option>"; else echo "<option value='USAID-CI' >USAID-CI</option>";
                            if($datosUsuario[2] == "USAID-SI") echo "<option value='USAID-SI' selected>USAID-SI</option>"; else echo "<option value='USAID-SI' >USAID-SI</option>";
                            if($datosUsuario[2] == "USAID-SE") echo "<option value='USAID-SE' selected>USAID-SE</option>"; else echo "<option value='USAID-SE' >USAID-SE</option>";	
                        ?>                
   
                        </select>
                    </td>
                    </tr>
            <?php }
                  else
                  {
                        echo "<input type='hidden' name='tipoUsuario' value='$datosUsuario[2]' />"; 
                  }
            ?>   

            
            <tr><td style="text-align: right; padding-right: 20px;">&iquest;De cual polideportivo?</td>
                <td><select name="Localidad" id="Localidad" style="125px">
	               <?php if($datosUsuario[4] == "Soyapango")
                            echo "<option selected='true' value='Soyapango'>Polideportivo Soyapango</option>";
                         else
                            echo "<option value='Soyapango'>Polideportivo Soyapango</option>";
                            
                         if($datosUsuario[4] == "Multigimnasio") 
                            echo "<option selected='true' value='Multigimnasio'>Multigimnasio</option>";
                         else
                            echo "<option value='Multigimnasio'>Multigimnasio</option>";   

                         if($datosUsuario[4] == "Santa Ana") 
                            echo "<option selected='true' value='Santa Ana'>Polideportivo Santa Ana</option>";
                         else
                            echo "<option value='Santa Ana'>Polideportivo Santa Ana</option>";   
                            
                         if($datosUsuario[4] == "San Miguel") 
                            echo "<option selected='true' value='San Miguel'>Polideportivo San Miguel</option>";
                         else
                            echo "<option value='San Miguel'>Polideportivo San Miguel</option>";                                                                                                             
                   ?>
                    </select>
                </td></tr>   
                
            
            <tr>
                <td><center><a href="<?php echo $regresar; ?>"><input id="boton1" name="boton1" class="boton" type="button" value="<?php echo $texto; ?>" style="width: 150px;" /></a></center></td>
                <td>
                    <center>
                        <input type="hidden" name="iu" value="<?php echo $Id;?>" />
                        <input id="Enviar" name="Enviar" class="boton" type="submit" value="<?php echo $abm; ?>" style="width: 150px;" onClick='return VerificarUser();' />
                    </center>
                </td>
            </tr>                                                                                                
            </table>                
            </form>
            <form>
            
            </form>
            </div>
            <div class="clr"></div>      
        </div>
    </div>
  <div class="fbg"></div>  
</div>
<div class="footer"><?php include("../footer.php");?></div>
<script> 
	var pass2 = new LiveValidation('pass2');
	pass2.add(  Validate.Confirmation, { match: 'pass1' } );
</script>
</body>
</html>