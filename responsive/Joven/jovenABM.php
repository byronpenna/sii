<?php
require '../net.php';

if(isset($_POST['enviar_joven'])){

    if($_POST['enviar_joven']=="Insertar"){                
    $selec = $bdd->prepare("SELECT * FROM  joven");
    $selec->execute();    
    $ceros= str_repeat('0', 4- strlen($selec->rowCount())) . $selec->rowCount();    
    $codigo= substr($_POST['apellido1'], 0,1) . substr($_POST['apellido2'], 0,1) . "14" . $ceros;    
    $FechaNacimiento = $_POST['anio'] . "/" . $_POST['mes'] . "/" . $_POST['dia']; 
    $comand=$bdd->prepare("INSERT INTO joven VALUES('$codigo','". $_POST['nombres'] ."' ,'".$_POST['apellido1']."','".$_POST['apellido2']."','".$_POST['sexo']."','". $FechaNacimiento ."','".$_POST['documento']."','".$_POST['direccion']."','". $_POST['municipio'] ."','". $_POST['departamento'] ."' ,'". $_POST['tel_fijo'] ."' ,'". $_POST['tel_celular'] ."' ,'". $_POST['estatura'] ."' ,'". $_POST['peso'] ."' ,'". $_POST['estado_civil'] ."' ,'". $_POST['profesion'] ."' ,'". $_POST['correo'] ."','" . $_SESSION["IdUsuario"] . "')");
    $comand->execute();
    
     Redireccion("../Main.php?l=joven&n=1");
    
}elseif($_POST['enviar_joven']=="Modificar"){
    
        $FechaNacimiento = $_POST['anio'] . "/" . $_POST['mes'] . "/" . $_POST['dia'];

            $comand=$bdd->prepare("UPDATE joven SET 
            Nombre = :nombre , 
            Apellido1 = :apellido1 , 
            Apellido2 = :apellido2, 
            Sexo = :sexo, 
            FechaNac = :fechanac, 
            DUI = :dui, 
            DireccionResi = :direccionresi, 
            Municipio = :municipio, 
            Departamento = :departamento, 
            TelFijo = :telfijo, 
            TelCel = :telcel, 
            Estatura = :estatura, 
            Peso = :peso, 
            EstadoCivil = :estadocivil, 
            Profesion = :profesion, 
            Correo = :correo 
            WHERE IdUsuario = :id");

            $comand->bindParam(':nombre',$_POST['nombres']);
            $comand->bindParam(':apellido1',$_POST['apellido1']); 
            $comand->bindParam(':apellido2',$_POST['apellido2']);
            $comand->bindParam(':sexo',$_POST['sexo']);
            $comand->bindParam(':fechanac',$FechaNacimiento);
            $comand->bindParam(':dui',$_POST['documento']);
            $comand->bindParam(':direccionresi',$_POST['direccion']);
            $comand->bindParam(':municipio',$_POST['municipio']);
            $comand->bindParam(':departamento',$_POST['departamento']);
            $comand->bindParam(':telfijo',$_POST['tel_fijo']);
            $comand->bindParam(':telcel',$_POST['tel_celular']);
            $comand->bindParam(':estatura',$_POST['estatura']);
            $comand->bindParam(':peso',$_POST['peso']);
            $comand->bindParam(':estadocivil',$_POST['estado_civil']);
            $comand->bindParam(':profesion',$_POST['profesion']);
            $comand->bindParam(':correo',$_POST['correo']);
            $comand->bindParam(':id',$_POST["IdJoven"]);
            
            $comand->execute();
            
            if(isset($_POST['dj']))
            Redireccion("../Main.php?l=joven&n=2&dj=" . $_POST["IdJoven"]);
            
            else
            Redireccion("../Main.php?l=joven&n=2");

    }
    
    elseif($_POST['enviar_joven']=="Eliminar")
    {
        $comand=$bdd->prepare("DELETE FROM joven WHERE CodigoJoven = '" . $_POST['idU'] . "'");
        $comand->execute();
        
        Redireccion("../Main.php?l=jovenl&n=1&pag=".$_POST['pagi']);
    }

}

?>