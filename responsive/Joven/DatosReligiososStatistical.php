<?php
include( 'GoogChart.class.php' );

    $lista = $bdd->prepare("SELECT * FROM joven");
	$lista->execute();
    
    $catolico = 0;
    $nopractica = 0;
    $evangelico = 0;
    
    $notienen = 0;
    $tiene = 0;


                $religion = $bdd->prepare("SELECT  ReligionPraticante , COUNT( * ) AS repeticion
                                           FROM  datosreligion GROUP BY ReligionPraticante");
                $religion->execute();                
                
                while($datoReligion = $religion->fetch())
                {                
                    if($datoReligion[0] == "Cristiano Catolico")
                    $catolico = $datoReligion[1];
                    
                    if($datoReligion[0] == "Cristiano Evangelico")
                    $evangelico= $datoReligion[1];
                    
                    if($datoReligion[0] == "No Pratico")
                    $nopractica= $datoReligion[1];
                    
                    $tiene = $tiene + $datoReligion[1]; 
                }
              
              $notienen = $lista->rowCount() - $tiene;
    
    $chart = new GoogChart();	
?>
<table style="width: 100%; border-radius: 10px; border-color: skyblue; border-style: groove; background-color: white; ">
<tr>
	<th colspan="2"><h2>Datos Religiosos - Estadisticas</h2></th>
</tr>
<tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
<tr style="">
	<td colspan="2" style="text-align: center;"> <h2>Total de Registrados: <?php echo $lista->rowCount();?> Jovenes</h2></td>
</tr>
<tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
<tr><td>
<?php
	    $dato1 = ($catolico /  $tiene)*100;
        $dato2 = ($evangelico /  $tiene)*100;
        $dato3 = ($nopractica /  $tiene)*100;
    
        $data = array(
    			'Catolicos '. number_format($dato1,2) . '%' => $dato1,
    			'Evangelicos '. number_format($dato2,2) . '%' => $dato2,
                'No pranticante'. number_format($dato3,2) . '%' => $dato3
    		     );

        
        $color = array(
        			'#99C754',
        			'#54C7C5',
        			'#78bbe6',
        		);

        $chart->setChartAttrs( array(
        	'type' => 'pie',
        	'title' => 'Religion Practicante de los Jovenes',
        	'data' => $data,
        	'size' => array( 500, 300 ),
        	'color' => $color
        	));
     
    echo $chart;

    ?>
    </td>
        <td>
            Catolicos: <br /><?php echo $catolico;?> j�venes<br /><br />
            Evangelicos: <br /><?php echo $evangelico;?> j�venes<br /><br />
            No Practican: <br /><?php echo $nopractica;?> j�venes
        </td>
    </tr>    
    <tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
    
    <tr style="">
    	<td colspan="2" style="text-align: center;">Total de Datos Religiosos: <?php echo $tiene;?> Jovenes</td>
    </tr>
    <tr style="">
    	<td colspan="2" style="text-align: center;">Total de Datos Religiosos faltantes: <?php echo $notienen;?> Jovenes</td>
    </tr>    
</table>