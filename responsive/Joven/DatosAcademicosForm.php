<div style="float:left; width: 20%;">
<?php include ('menuJoven.php');?>
</div>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; ">

<?php
	
	if(isset($_GET['da'])){
		$lista = $bdd->prepare("SELECT * FROM  `datosacademicos` WHERE IdDatoAcademico =" . $_GET['da']);
		$lista->execute();
		$datoslista=$lista->fetch();

		$abm="Modificar";

	}else{
		$datoslista[0]="";
		$datoslista[1]="";
		$datoslista[2]="";
		$datoslista[3]="";
		$abm="Insertar";
	}


?>

<script>
function cargarSecciones(form)//Llena el select dependiente de las instituciones.
{
        var Instituciones = form.Instituciones.options;
        var Secciones = form.Secciones.options;
        var i = 0;
        
        Secciones.length = null;
        
        if (Instituciones[0].selected == true)
        {            
            var seleccionar = new Option("Esperando a que seleccione una instituci�n...","","","");
            Secciones[0] = seleccionar;
        }
                
        <?php
	            $DatoS = $bdd->prepare("Select * from Seccion ORDER BY SeccionEducativa ASC");
                $DatoS->execute();
                
                while($secciones = $DatoS->fetch())
                {
        ?>
                    if(Instituciones[Instituciones.selectedIndex].value == <?php echo $secciones[1]?>)
                    {
                         var seleccionar = new Option("<?php echo $secciones[2]?>","<?php echo $secciones[0]?>","","");
                         seleccionar.label="<?php echo $secciones[2]?>";//Label guarda el nombre de la secci�n seleccionada para usarlo en el input hidden.
                         Secciones[i] = seleccionar; 
                         i++;
                    }
        <?php 
	            }
        ?>
}
function Validar(form)//Valida si se seleccion� alguna instituci�n para poder generar el reporte.
{
    var Instituciones = form.Instituciones.options;
        if(Instituciones[Instituciones.selectedIndex].value=="")
        {
            alert("Seleccione una instituci�n y luego una secci�n.");
        }
        
        return(Instituciones[Instituciones.selectedIndex].value!="");
}
function CargarHidden(form)//Carga el nombre de la instituci�n y secci�n seleccionados en 2 hidden.
{
     var Instituciones = form.Instituciones.options;
     var Secciones = form.Secciones.options;
     document.getElementById('institucion').value = Instituciones[Instituciones.selectedIndex].label;
     document.getElementById('nivel').value = Secciones[Secciones.selectedIndex].label;
}
</script>


	<form action="Joven/DatosAcademicosABM.php" method="POST" onsubmit="return Validar(this)">
		<?php
		if(isset($_GET['da'])) 
        echo "<input type='hidden' name='IdDa' value='$datoslista[0]' />";
        
        if(isset($_GET['dj']))
        echo "<input type='hidden' name='dj' id='dj' value='".$_GET['dj']."' />";
		?>
		<table class='Datos' style="padding: 10px; width: 100%;">
            <tr>
                <th colspan="2" style="text-align: center;"><h2>Informaci&oacute;n Academica</h2>
                <hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%;" />
                </th>
            </tr>  
			<tr>
				<td style='text-align: right; width:250px'><label>Instituci&oacute;n Educativa:</label></td>
<!--v Cargando nombre de todas las instituciones o de una seleccionada------------------------------------------------->				
                <td style=' padding-left: 3%; color: red;'> 
                    <select name="Instituciones" id='Instituciones' size='1' onChange='cargarSecciones(this.form)' title="�D�nde Estudiaste?">
                        <option value='' label=''>Seleccione una instituci�n...</option>
                        <?php   
                            $datoslistaIns=$bdd->prepare("SELECT IdInstitucion, NombreInstitucion
                                                        FROM  Institucion 
                                                        ORDER BY NombreInstitucion ASC ");
                            $datoslistaIns->execute();
                            
                            while($listainst=$datoslistaIns->fetch())
                            {    
                                //$datoslista contiene datos de la tabla datosacademicos.
                                if(($datoslista[1]!="") && ($datoslista[1]==$listainst[1])) //Selecciona el instituto segun nombre en tabla datosacademicos y los imprime seg�n tabla institucion.
                                {
                                    echo "<option value='$listainst[0]' label='$listainst[1]' selected>$listainst[1]</option>";
                                    $IdInsti=$listainst[0];//Guarda el id del instituto seleccionado, se usar� para mostrar sus secciones.
                                }
                                else   
                                {
                                        echo "<option value='$listainst[0]' label='$listainst[1]'>$listainst[1]</option>";
                                }                     
                            }                
                        ?> 
                    </select>
                    <!--<input required="true" title="�Donde Estudiaste?" type="text" name="institucion" value="<?php //echo $datoslista[1] ?>"/>-->                 
                </td>
<!--^ Cargando nombre de todas las instituciones o de una seleccionada------------------------------------------------->                
			</tr>
			<tr>
				<td style='text-align: right; width:250px'><label>Grado Acad�mico:</label>
<!--v Cargando nombre de todas las secciones o de una seleccionada-------------------------------------------------> 
				<td style=' padding-left: 3%; color: red;'>
                    <select name="Secciones" size="1" title="�Qu� Grado?">
                        <?php 
                            if($datoslista[1]=="")                        
                                echo"<option value=''>Esperando a que seleccione una instituci�n...</option>";
                         ?>       
                        
                        <!--Aqu� se crean las <option> con javascript-->
                        <?php
                            //Se cargan las secciones correspondientes a la instituci�n preseleccionada para moficaci�n, esto se hace solo al principio, luego 
                            //javascript se encarga del trabajo.
                            if($datoslista[1]!="")
                            {                                
                                $datoslistaSec=$bdd->prepare("Select * from Seccion where IdInstitucion='$IdInsti' ORDER BY SeccionEducativa ASC ");
                                $datoslistaSec->execute();
                                
                                while($listasecc=$datoslistaSec->fetch())
                                {
                                    //$datoslista contiene datos de la tabla datosacademicos.
                                    if($datoslista[2]==$listasecc[2]) //Seleccionar secci�n a la que pertenece el joven.
                                        echo "<option value='$listasecc[0]' label='$listasecc[2]' selected>$listasecc[2]</option>";
                                    else   
                                        echo "<option value='$listasecc[0]' label='$listasecc[2]'>$listasecc[2]</option>";                     
                                }
                                $IdInsti="";//Si no se est� haciendo una modificaci�n(entonces es una inserci�n) en los registros, su valor es vac�o(aunque no afecta dejarla con datos).
                            }
                            else
                                                
                        ?> 
                    </select>                   
                    <!--<input required="true" title="�Que Grado?" type="text" name="nivel" value="<?php //echo $datoslista[2] ?>"/>-->                    
                </td>
<!--^ Cargando nombre de todas las secciones o de una seleccionada------------------------------------------------->    
<!--vEnviando nombres de instituci�n y secci�n---------------------------------------------------------------------->
                <input type="hidden" id="institucion" name="institucion"/>
                <input type="hidden" id="nivel" name="nivel"/>            
<!--^Enviando nombres de instituci�n y secci�n---------------------------------------------------------------------->                
			</tr>
			<tr>
				<td style='text-align: right; width:250px'><label>A&ntilde;o:</label></td>
				<td style=' padding-left: 3%; color: red;'>
            <?php	
                            echo "          <select id='a�o' name='a�o'>";
                                            for($i = 2013; $i >= 1980; $i--){
                                                if( substr($datoslista[5],0,4) == $i)
                            echo "                  <option selected='true' value='$i'>$i</option>";
                                                else
                            echo "                  <option value='$i'>$i</option>";                                                
                                            }
                            echo "          </select>";
            ?>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center ;"><input class="boton" type="submit" value="<?php echo $abm ?>" name="enviaracademicos" onclick="CargarHidden(this.form)" /></td>
			</tr>
		</table>
	</form>
</div>
<div class="clr"></div>     