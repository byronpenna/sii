<?php
include( 'GoogChart.class.php' );

    $lista = $bdd->prepare("SELECT * FROM joven");
	$lista->execute();
    
    $edad18arriba = 0;
    $edadmenor = 0;
    
    $edad05   = 0;
    $edad610  = 0;
    $edad1115 = 0;
    $edad1620 = 0;
    $edad2125 = 0;
    $edad26A  = 0;
    
    $masculino = 0;
    $femenino = 0;
    
    
    
    while($datos=$lista->fetch())
    {

                $edad = $bdd->prepare("SELECT TIMESTAMPDIFF( YEAR,  '$datos[5]', NOW( ) ) AS Edad");
                $edad->execute();
                $datoEdad = $edad->fetch();
                
                if($datoEdad[0] >= 18)
                $edad18arriba++;
                
                else
                $edadmenor++;
                
                if($datoEdad[0]<= 5 && $datoEdad[0]>0 )
                $edad05++;
                
                elseif($datoEdad[0]<= 10 && $datoEdad[0]>6 )
                $edad610++; 
                
                elseif($datoEdad[0]<= 15 && $datoEdad[0]>11 )
                $edad1115++;                                

                elseif($datoEdad[0]<= 20 && $datoEdad[0]>16 )
                $edad1620++;  
                
                elseif($datoEdad[0]<= 25 && $datoEdad[0]>21 )
                $edad2125++;  
                
                elseif($datoEdad[0]<= 26  )
                $edad26A++;                                                
       
                if($datos[4] == "Masculino")
                $masculino++;
                
                else
                $femenino++;
                
    }          
    
    $chart = new GoogChart();	
?>
<table style="width: 100%; border-radius: 10px; border-color: skyblue; border-style: groove; background-color: white; ">
<tr>
	<th colspan="2"><h2>Datos Academicos - Estadisticas</h2></th>
</tr>
<tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
<tr style="">
	<td colspan="2" style="text-align: center;"> <h2>Total de Registrados: <?php echo $lista->rowCount();?> Jovenes</h2></td>
</tr>
<tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
<tr><td>
<?php
	    $dato1 = ($edad18arriba /  $lista->rowCount())*100;
        $dato2 = ($edadmenor /  $lista->rowCount())*100;
    
        $data = array(
    			'Mayores '. number_format($dato1,2) . '%' => $dato1,
    			'Menores '. number_format($dato2,2) . '%' => $dato2
    		     );

        
        $color = array(
        			'#99C754',
        			'#54C7C5',
        			'#78bbe6',
        		);

        $chart->setChartAttrs( array(
        	'type' => 'pie',
        	'title' => 'Edades de Jovenes',
        	'data' => $data,
        	'size' => array( 500, 300 ),
        	'color' => $color
        	));
    // Print chart
    echo $chart;

    ?>
    </td>
        <td>
            Mayores de Edad: <br /><?php echo $edad18arriba;?> j�venes<br /><br />
            Mayores de Edad: <br /><?php echo $edadmenor;?> j�venes
        </td>
    </tr>    
    <tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
    <tr>
	<td>
    <?php
	    $dato1 = ($masculino /  $lista->rowCount())*100;
        $dato2 = ($femenino /  $lista->rowCount())*100;
    
        $data = array(
    			'Hombres '. number_format($dato1,2) . '%' => $dato1,
    			'Mujeres '. number_format($dato2,2) . '%' => $dato2
    		     );

        
        $color = array(
        			'#99C754',
        			'#54C7C5',
        			'#78bbe6',
        		);

        $chart->setChartAttrs( array(
        	'type' => 'pie',
        	'title' => 'Sexo de Jovenes',
        	'data' => $data,
        	'size' => array( 500, 300 ),
        	'color' => $color
        	));
    // Print chart
    echo $chart;

    ?>
    </td>
	<td>
    Hombres: <?php echo $masculino?> j�venes<br /><br />
    Mujeres: <?php echo $femenino?> j�venes<br />
    </td>
</tr>
</table>