<?php
/**
 * @authores Manuel, Gilberto.
 * @copyright 2013
 */
     if($_SESSION["TipoUsuario"]!="Administrador")
    {   
        Redireccion("FUSALMO.php");
    }
        
?>   
<script>
    //Filtros para permitir s�lo n�meros, letras o s�mbolos seg�n campo a llenar.
    function numeros(e)
    {
        var keynum = window.event ? window.event.keyCode : e.which;        
        return /\d/.test(String.fromCharCode(keynum));
        //Obtenido de: http://blog.freshware.es/solo-permitir-numeros-en-input-text-html/
    }
    function sololetras(e)
    {
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " �����abcdefghijklmn�opqrstuvwxyz�����";
       especiales = [];

       tecla_especial = false
       for(var i in especiales)
       {
            if(key == especiales[i])
            {
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial)
        {
            return false;
        }
         //Obtenido de: http://fgualambo.blogspot.com/2011/09/validar-campo-de-texto-solo-letras.html
    }    
     
</script>

<div style="width: 90%; background-color: white; border-radius: 20px; margin-left: 5%; ">
<h1 style="margin-left: 60px;margin-bottom: 10px;">Generaci�n de Reportes</h1> 
<hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 15px;" />
<table style="width: 100%; padding-bottom: 20px;" >
    <tr><td colspan="2" style="text-align: center;"><h2><?php echo $DatosFUSALMO[1]?></h2></td></tr>
    <tr><td colspan="2" style="text-align: center;">
        <form action="Joven/JoPreReporte.php" method="Post" target="_blank">
            <table style="width: 80%; margin-left: 10%; margin-bottom: 20px; margin-top: 20px; border-radius: 10px; border-color: skyblue; border-style: groove; padding: 10px;">
            <tr>
            	<td colspan="2" style="text-align: center;">Generaci�n de reporte personalizado.</td>
            </tr>
            <tr>
            	<td style="text-align: right; padding-right: 20px;">Seleccione un g�nero:</td>
                <td style="text-align: left; padding-right: 20px;">                
                <select name="genero" id='genero' size='1'>
                <option value='' label=''>...</option>
                <option value='Femenino' label='Femenino'>Femenino</option>
                <option value='Masculino' label='Masculino'>Masculino</option>
                </select>              
                </td>                
            </tr>    
            <tr><td style="text-align: center; padding-right: 0px;" colspan="2">Seleccione un rango de edad:</td></tr>
            <tr><td style="text-align: right; padding-right: 20px;">Edad M�nima:</td><td style="text-align: left; padding-right: 20px;"><input type="text" name="EdadMin" id="EdadMin" onkeypress="return numeros(event)" maxlength="2" size="1" value=""/></td></tr>                                                                                         
            <tr><td style="text-align: right; padding-right: 20px;">Edad M�xima:</td><td style="text-align: left; padding-right: 20px;"><input type="text" name="EdadMax" id="EdadMax" onkeypress="return numeros(event)" maxlength="2" size="1" value=""/></td></tr>
            <tr><td style="text-align: right; padding-right: 20px;">Escriba parte de una direcci�n:</td><td style="text-align: left; padding-right: 20px;"><input type="text" name="Direc" onkeypress="return sololetras(event)" maxlength="10" size="10"/></td></tr>
            <input type="hidden" name="TipoDato" value="DatosPersonales" />
            <tr><td colspan="2" style="text-align: center;"> <input type="submit" class="boton" value="Reporte" /> </td></tr>
            </table>
        </form>
    </td></tr>    
</table>

</div>
<div class="clr"></div>