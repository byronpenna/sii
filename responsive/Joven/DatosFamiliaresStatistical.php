<?php
include( 'GoogChart.class.php' );

    $lista = $bdd->prepare("SELECT * FROM joven");
	$lista->execute();
    

    $Abuela = 0;
    $Abuelo	= 0;
    $Hermana = 0;
    $Hermano = 0;
    $Madre = 0;
    $Otro = 0;
    $Padrastro = 0;
    $Padre = 0;
    $Prima = 0;
    $Tia = 0;
    $Tio = 0;   
    
    $notienen = 0;
    $tiene = 0;


                $familiar = $bdd->prepare("SELECT Parentesco, COUNT( * ) AS repeticion
                                           FROM datosfamiliares WHERE Responsable =  'Si' GROUP BY Parentesco");
                $familiar->execute();                
                
                while($datofamilia = $familiar->fetch())
                {                
                    if($datofamilia[0] == "Abuela")
                    $Abuela = $datofamilia[1];

                    if($datofamilia[0] == "Abuelo")
                    $Abuelo = $datofamilia[1];
                    
                    if($datofamilia[0] == "Hermana")
                    $Hermana = $datofamilia[1];
                    
                    if($datofamilia[0] == "Hermano")
                    $Hermano = $datofamilia[1];
                    
                    if($datofamilia[0] == "Madre")
                    $Madre = $datofamilia[1];
                    
                    if($datofamilia[0] == "Otro")
                    $Otro = $datofamilia[1];
                    
                    if($datofamilia[0] == "Padrastro")
                    $Padrastro = $datofamilia[1];

                    if($datofamilia[0] == "Padre")
                    $Padre = $datofamilia[1];                       

                    if($datofamilia[0] == "Prima")
                    $Prima = $datofamilia[1];   
                    
                    if($datofamilia[0] == "Tia")
                    $Tia = $datofamilia[1];   
                    
                    if($datofamilia[0] == "Tio")
                    $Tio = $datofamilia[1];                                                                                                                                                                                    
                    
                    
                    $tiene = $tiene + $datofamilia[1]; 
                }

                $familiarm = $bdd->prepare("SELECT * FROM  datosfamiliares WHERE  Migro =  'Si'");
                $familiarm->execute();                  
              
              $notienen = $lista->rowCount() - $tiene;
    
    $chart = new GoogChart();	
?>
<table style="width: 100%; border-radius: 10px; border-color: skyblue; border-style: groove; background-color: white; ">
<tr>
	<th colspan="2"><h2>Datos Familiares - Estadisticas</h2></th>
</tr>
<tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
<tr style="">
	<td colspan="2" style="text-align: center;"> <h2>Total de Registrados: <?php echo $lista->rowCount();?> Jovenes</h2></td>
</tr>
<tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
<tr><td>
<?php
	    $dato1 = ($Abuela /  $tiene)*100;
        $dato2 = ($Abuelo /  $tiene)*100;
        $dato3 = ($Hermana /  $tiene)*100;
	    $dato4 = ($Hermano /  $tiene)*100;
        $dato5 = ($Madre /  $tiene)*100;
        $dato6 = ($Otro /  $tiene)*100;
	    $dato7 = ($Padrastro /  $tiene)*100;
        $dato8 = ($Padre /  $tiene)*100;
        $dato9 = ($Prima /  $tiene)*100;
	    $dato10 = ($Tio /  $tiene)*100;
        $dato11 = ($Tia /  $tiene)*100;
                        
    
            $data = array(
    			'Abuela '. number_format($dato1,2) . '%' => $dato1,
    			'Abuelo '. number_format($dato2,2) . '%' => $dato2,
                'Hermano '. number_format($dato3,2) . '%' => $dato3,
    			'Hermana '. number_format($dato4,2) . '%' => $dato4,
    			'Madre '. number_format($dato5,2) . '%' => $dato5,
                'Otro '. number_format($dato6,2) . '%' => $dato6,
    			'Padrastro '. number_format($dato7,2) . '%' => $dato7,
    			'Padre '. number_format($dato8,2) . '%' => $dato8,
                'Prima '. number_format($dato9,2) . '%' => $dato9,
    			'Tio '. number_format($dato10,2) . '%' => $dato10,
    			'Tia '. number_format($dato11,2) . '%' => $dato11                                                
    		     );

        
        $color = array(
        			'#99C754',
        			'#54C7C5',
        			'#78bbe6',
        		);

        $chart->setChartAttrs( array(
        	'type' => 'pie',
        	'title' => 'Familiares responsables los Jovenes',
        	'data' => $data,
        	'size' => array( 500, 300 ),
        	'color' => $color
        	));
     
    echo $chart;

    ?>
    </td>      
        <td><br />
                Abuela = <?php echo $Abuela;?> jovenes<br /><br />
                Abuelo = <?php echo $Abuelo;?> jovenes<br /><br />	
                Hermana = <?php echo $Hermana;?> jovenes<br /><br />
                Hermano = <?php echo $Hermano;?> jovenes<br /><br /> 
                Madre = <?php echo $Madre;?> jovenes<br /><br />
                Otro = <?php echo $Otro;?> jovenes<br /><br />
                Padrastro = <?php echo $Padrastro;?> jovenes<br /><br />
                Padre = <?php echo $Padre;?> jovenes<br /><br />
                Prima = <?php echo $Prima;?> jovenes<br /><br />
                Tia = <?php echo $Tia;?> jovenes<br /><br />
                Tio = <?php echo $Tio;?> jovenes<br /><br />
        </td>
    </tr>    
    <tr><td colspan='2' style='text-align: left; background-color: skyblue; height: 10px;'></td></tr>
    
    
    <tr style="">
    	<td colspan="2" style="text-align: center;">Total de Familiares Registrados: <?php echo $tiene;?> </td>
    </tr>
<tr style="">
    	<td colspan="2" style="text-align: center;">Total Familiares fuera del Pais: <?php echo $familiarm->rowCount();?></td>
    </tr>       
</table>