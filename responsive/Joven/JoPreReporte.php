<?php
    session_start();
/**
 * @author Gilberto.
 * @copyright 2013
 */
    //Generador de reporte en Excel.
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=".$_POST['TipoDato'].".xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';    
    
    //Obteniendo Nombre de Instituci�n.
    $datos=$bdd->prepare("SELECT * FROM joven WHERE 
                            Sexo like '%".$_POST['genero']."%' 
                            AND (DireccionResi like '%".$_POST['Direc']."%' 
                            OR Municipio like '%".$_POST['Direc']."%' 
                            OR Departamento like '%".$_POST['Direc']."%')");
    $datos->execute();
    
?>
    <table border='0' align='center'>
        <tr><td colspan="17"></td></tr>
        <tr>
            <td></td>
            <th>N�</th><th>C�digo</th><th>Apellidos</th><th>Nombres</th><th>Sexo</th><th>Fecha de nacimiento</th>
            <th>Edad (A�os)</th><th>Documento de identidad</th><th>Direccion de residencia</th><th>Tel�fono fijo</th>
            <th>Tel�fono celular</th><th>Estatura (m)</th><th>Peso (Lb)</th><th>Estado civil</th><th>Profesi�n</th><th>Correo electr�nico</th>
            <td></td>
        </tr>
        <?php
            $contador=1;
            while($Jovenes=$datos->fetch())
            {
                //Obteniendo la edad del joven.
                $edad = $bdd->prepare("SELECT TIMESTAMPDIFF( YEAR,  '$Jovenes[5]', NOW( ) ) AS Edad");
                $edad->execute();
                $datoEdad = $edad->fetch();
                $EdadMin=$_POST['EdadMin'];
                $EdadMax=$_POST['EdadMax'];
                if(($EdadMin!="")xor($EdadMax!=""))//Si se introdujo s�lo una edad, ambos limites ser�n los mismos por lo tanto se mostrar�n los jovenes con dicha edad.
                {
                    if(($EdadMin!="")&&($EdadMax==""))
                        $EdadMax=$EdadMin;
                    else
                        $EdadMin=$EdadMax;
                }

                if(($EdadMin!="")&&($EdadMax!=""))//Mostrar j�venes que tienen una edad comprendida en el rango seleccionado.
                {                  
                    if(($datoEdad[0]>=$EdadMin)&&($datoEdad[0]<=$EdadMax))
                    {
                    
                        echo "      <tr>
                                        <td></td>
                                        <th>$contador</th><td>".strtoupper(strtolower($Jovenes[0]))."</td><td>".ucfirst(strtolower($Jovenes[2]))." ".ucfirst(strtolower($Jovenes[3]))."</td><td>".ucwords(strtolower($Jovenes[1]))."</td><td>$Jovenes[4]</td><td>$Jovenes[5]</td>
                                        <td>$datoEdad[0]</td><td>".ucwords(strtolower($Jovenes[6]))."</td><td>".ucwords(strtolower($Jovenes[7])).", ".ucwords(strtolower($Jovenes[8])).", ".ucwords(strtolower($Jovenes[9]))."</td><td>$Jovenes[10]</td>
                                        <td>$Jovenes[11]</td><td>$Jovenes[12]</td><td>$Jovenes[13]</td><td>$Jovenes[14]</td><td>".ucwords(strtolower($Jovenes[15]))."</td><td>$Jovenes[16]</td>
                                        <td></td>
                                    </tr>";
                        $contador++;
                    }
                }else//Mostrar j�venes sin usar el filtro de edad.
                {                  
                    
                    echo "      <tr>
                                       <td></td>
                                       <th>$contador</th><td>".strtoupper(strtolower($Jovenes[0]))."</td><td>".ucfirst(strtolower($Jovenes[2]))." ".ucfirst(strtolower($Jovenes[3]))."</td><td>".ucwords(strtolower($Jovenes[1]))."</td><td>$Jovenes[4]</td><td>$Jovenes[5]</td>
                                       <td>$datoEdad[0]</td><td>".ucwords(strtolower($Jovenes[6]))."</td><td>".ucwords(strtolower($Jovenes[7])).", ".ucwords(strtolower($Jovenes[8])).", ".ucwords(strtolower($Jovenes[9]))."</td><td>$Jovenes[10]</td>
                                       <td>$Jovenes[11]</td><td>$Jovenes[12]</td><td>$Jovenes[13]</td><td>$Jovenes[14]</td><td>".ucwords(strtolower($Jovenes[15]))."</td><td>$Jovenes[16]</td>
                                       <td></td>
                                   </tr>";
                       $contador++;
                    
                }
                
            }
	
        ?>
    </table>