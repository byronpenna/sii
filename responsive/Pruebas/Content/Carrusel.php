<?php

/**
 * @author Calderón
 * @copyright 2014
 */

?>

<div class="jumbotron" style="background: rgba(100,100,100,0.2);" id="top">
<br /><br />
<div class="container-fluid">
	<div class="col-md-6">
		<div id="carousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
			    <li data-target="#carousel" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel" data-slide-to="1"></li>
			    <li data-target="#carousel" data-slide-to="2"></li>
		  	</ol>
		  	<div class="carousel-inner">
			    <div class="item active">
					<img class="img-rounded" src="Images/summer_beach-960x0.jpg" alt="..." />
			      	<div class="carousel-caption">
			        	<h3>Olas Permanentes</h3>
			      	</div>
			    </div>
			    <div class="item">
			    	<img class="img-rounded" src="Images/parque-libertad.jpg" alt="..." />
			      	<div class="carousel-caption">
			        	<h3>Parque Libertad</h3>
			      	</div>
			    </div>
			    <div class="item">
			    	<img class="img-rounded" src="Images/lago-de-ilopango.jpg" alt="..." />
			      	<div class="carousel-caption">
			        	<h3>Lago de Ilopango</h3>
			      	</div>
			    </div>
			  </div>
		</div>
	</div>
	<div class="col-md-6" style="background-color: rgba(100,100,100,0.2);text-align: justify; border-radius: 10px;">
		<p><h2 style="color: white;"><strong>Bienvenido</strong></h2></p>
        <h2 style="color: white "><strong>Conoce acerca de tus lugares turisticos favoritos, consulta información y danos tu opinión <br /><br /> !Esto es TurismoSV!</strong></h2>
	</div>
</div>
<br /><br />
<hr />
</div>