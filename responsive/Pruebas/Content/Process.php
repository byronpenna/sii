<?php

/**
 * @author Calder�n
 * @copyright 2014
 */

if(isset($_GET['view']))
{
    if($_GET['view'] == "Login")    
        include('Content/Login.php');
        
    else if($_GET['view'] == "Start")   
    {
        include('Content/Carrusel.php');
        include('Content/Lugar.php');              
    }
    else if($_GET['view'] == "About")    
        include('Content/Acerca de.php');    
    
    else if($_GET['view'] == "LogOff")   
    {
        session_destroy();    
        Redireccion('http://siipdb.fusalmo.org/Pruebas/');
    } 
    
    else if($_GET['view'] == "User")    
        include('Content/Usuarios.php');       
    
    
    else if($_GET['view'] == "NewUser")    
        include('Content/UsuariosNew.php');  

    else if($_GET['view'] == "Site")    
        include('Content/Sitio.php');               
        
    
    else
    {
        Redireccion('http://siipdb.fusalmo.org/Pruebas/');
    }
}
else
{
    include('Content/Carrusel.php');
    include('Content/Lugar.php');
}
?>

