<?php

/**
 * @author Calderón
 * @copyright 2014
 */

?>
<script>
function Validar()
{
    var pwd1 = document.getElementById('pwd1').value;
    var pwd2 = document.getElementById('pwd2').value;
    
    if(pwd1 == pwd2)
        return true;
    
    else
    {
        alert('Error, los campos de password no coinciden');
        pwd2.focus();
        return false;    
    }
    
}
</script>

<div style="background: rgba(100,100,100,0.2); width: 80%; margin-left: 10%; color: white;">
<br /><br /><br />
<form method="post" action="Content/ABM.php">
<?php	
    if($_SESSION['aux'] == "")
    {
        $ABM = "Registrarse";
    }
    else
    {
        $Select = $bddC->prepare("Select * from tsv_usuario where IdUsuario = :iu");  
        $Select->bindParam(':iu', $_SESSION['aux']);
        $Select->execute();  
        
        $DataU = $Select->fetch();
        
        $ABM = "Modificar";
        echo "<input type='hidden' name='iu' value='" .  $_SESSION['aux'] . "' />";
    }
?>

<table style="padding: 10px; margin-left: 10%;">
<tr><td colspan="2"><h2>Nuevo Usuario</h2></td></tr>
<tr><td>Nombre de Usuario:</td><td style="padding: 5px;"><input type="text" class="form-control" required="true" name="UserName"  value="<?php echo $DataU[1]?>"/></td></tr>
<tr><td>Contraseña:</td>                <td style="padding: 5px; color: black;"><input type="password" id="pwd1" name="pwd1" class="form-control" required="true" value="<?php echo $DataU[2]?>" /></p></td></tr>
<tr><td>Confirmación de Contraseña:</td><td style="padding: 5px; color: black;"><input type="password" id="pwd2" name="pwd2" class="form-control" required="true" value="<?php echo $DataU[2]?>" /></td></tr>
<tr><td>Correo Electronico:</td><td style="padding: 5px;"><input type="email" class="form-control" required="true" name="Mail" value="<?php echo $DataU[3]?>"/></td></tr>
<?php     if($_SESSION['aux'] == "")    
          {
?>
<tr><td>La suma de 1 + 1:</td><td style="padding: 5px;"><input type="text" class="form-control" required="true" pattern="2" title="Error, realiza la suma bien" name="prueba" value="" id="prueba"/></td></tr>
<?php     }
          unset($_SESSION['aux']);
?>
<tr><td colspan="2" style="text-align: center; padding-top: 10px;"><input type="submit" name="Enviar" value="<?php echo $ABM?>"  class="btn btn-default" onclick="return Validar();" /></td></tr>
</table>
</form>
</div>
<br /><br /><br />