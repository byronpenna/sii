<?php

/**
 * @author Calder�n
 * @copyright 2014
 */



?>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">				
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand">TurismoSV</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
          	<ul class="nav navbar-nav">
                <li class="navigator"><a href="?view=Start">Inicio</a></li>
            	<li><a id="exploreLink" class="navigator" href="?view=Start#explore">Explora</a></li>
            	<li><a id="aboutLink" class="navigator" href="?view=About">Acerca de</a></li>

            <?php
	               if($_SESSION['type']   == 1)
                   {
                        echo "<li class='navigator'><a href='?view=Site'>Sitios</a></li>";
                        echo "<li class='navigator'><a href='?view=User'>Usuarios</a></li>";
                   }
            ?>                      
          	</ul>
          	<ul class="nav navbar-nav navbar-right">
            <?php
	               if($_SESSION['autenticado'] != 1)
                        echo "<li class='navigator'><a href='?view=Login'>Login</a></li>";
                
                   else
                   {
                        echo "<li class='navigator'><a style='Color: blue'>Usuario: ".$_SESSION['User']."</a></li>";
                        echo "<li class='navigator'><a href='?view=LogOff'>Cerrar Sesi�n</a></li>";
                   } 
            ?>            
            	
          	</ul>
        </div>
	</div>
</nav>