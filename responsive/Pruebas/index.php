<?php include('Content/net.php')?>
<!DOCTYPE html>
<html>
	<head>
        <meta http-equiv="content-type" content="text/html" charset="ISO-8859-1"/>
		<title>TurismoSV</title>
		<?php include('Content/Libs.php')?>
        <style>
        #footer{
            bottom: 0px;
        }
        </style>
	</head>
	<body>
		<div class="container-fluid">
            <?php include('Content/MainMenu.php')?>
            <?php include('Content/Process.php')?>                        
		</div>
        <div id="footer" class="footer" style="width: 100%; background-color: #FAFAFA; float: bottom;">
            <?php include('Content/footer.php')?> 
        </div>
    </body>
</html>