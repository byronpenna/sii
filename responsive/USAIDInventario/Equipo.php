<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

    $Update = $bddC->prepare("Select * from Inventario_Equipos where IdEquipo = " . $_SESSION["IdEquipo"]);
    $Update->execute();
    
    $DataUpdate = $Update->fetch();

    $User = $bdd->prepare("Select NombreUsuario from usuario where Id = " . $DataUpdate[9]);
    $User->execute();
    
    $NombreUsuario = $User->fetch();
    
?>
<div style="width: 100%; background-color: white; border-radius: 5px;">
    <div class="Raiz" style="padding-top: 10px;">
        <a href="/Inventario.php" class='flecha' style='text-decoration:none'>Inventario </a> ->
        <a href="/Inventario.php?i=Equip" class='flecha' style='text-decoration:none'>Perfil del Equipo </a>
    </div> 

    <hr color='Skyblue' style="width: 90%; margin-left: 5%;" />
    
        <input type='hidden' name='IE' value='<?php echo $_SESSION["IdEquipo"]?>' />        
        <table style="width: 100%;">
            <tr><td colspan="2" style="text-align: center;"><h2 style="color: blue; margin-left: 30px;">Perfil de Equipo</h2></td></tr>
            <tr>
                <td style="width: 40%; vertical-align: text-top;">
                    <form method='post' action="USAIDInventario/EquipoABM.php">
                    <input type="hidden" name="IE" value="<?php echo $DataUpdate[0];?>" />
                    <table style="width: 100%; border-color: skyblue; border-style: groove; border-radius: 10px; padding: 5px; margin-left: 5%;">
                        <tr><td colspan="2" style="padding-left: 50px;"><h2 style="color: blue;">Detalles de Equipo</h2><br /></td></tr>
                        <tr><td style="; height: 30px;"><strong>Tipo de Equipo:   </strong></td><td style="width: 55%;"><?php echo $DataUpdate[1];?></td></tr>
                        <tr><td style="; height: 30px;"><strong>Modelo:           </strong></td><td><?php echo $DataUpdate[2];?></td></tr>
                        <tr><td style="; height: 30px;"><strong>Numero Serial:    </strong></td><td><?php echo $DataUpdate[3];?></td></tr>
                        <tr><td style="; height: 30px;"><strong>Direcci�n MAC:      </strong></td><td><?php echo $DataUpdate[4];?></td></tr>
                        <tr><td style="; height: 30px;"><strong>Estado:           </strong></td><td>
                            <?php echo "<table style='width: 100%'>
                                        <tr><td>" .$DataUpdate[5]."</td>";
	                              if($_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "Administrador" )
                                  {
                                    echo "<td style='text-align: center;'>
                                                <input type='hidden' name='IE' value='$DataUpdate[0]' />
                                                <input type='submit' name='Enviar' class='Boton' value='Cambiar' />

                                          </td>";
                                  }
                                  echo "</tr>
                                        </table>";
                            ?>
                            </td></tr>
                        <tr><td style="; height: 30px;"><strong>Estado de Supervisi�n:</strong></td><td><?php echo $DataUpdate[6];?></td></tr>
                        <tr><td style="; height: 30px;"><strong>Periodo de Garantia:          </strong></td><td><?php echo $DataUpdate[7];?></td></tr>
                        <tr><td style="; height: 30px;"><strong>Fecha de Registro:</strong></td><td><?php echo $DataUpdate[8];?></td></tr>
                        <tr><td style="; height: 30px;"><strong>Usuario:          </strong></td><td><?php echo $NombreUsuario[0];?></td></tr>                        
                        <tr><td style="text-align: center;" colspan="2">
                            <br />
                                <?php
                                       if($DataUpdate[6] != "Verificado")
                                       {
                                           echo "<table style='width: 100%'>
                                                   <tr>
                                                       <td style='width: 33%; text-align: center'><input type='submit' class='boton' name='Enviar' value='Modificar' /></td>
                                                       <td style='width: 33%; text-align: center'><input type='submit' class='botonE' name='Enviar' value='Eliminar' Onclick='return confirm(\"�Realmente desea borrar este registro?\")' /></td>";
                                                        
                                           if($_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "Administrador" )
                                           {
                                               echo "<td style='width: 34%; text-align: center'><input type='submit' class='botonG' name='Enviar' value='Verificar' /></td>";
                                           }
                                           echo "       </tr>
                                                 </table>";
                                       }
                                ?>
                            <br />
                            </td>
                        </tr>        
                    </table>      
                    </form>   
                </td>
                <td style="width: 55%;margin-left: 5%; vertical-align: top;">
                    <table style="width: 90%; border-color: skyblue; border-style: groove; border-radius: 10px; padding: 5px; margin-left: 5%;">
                        
                        
                        <tr><td colspan="2" style="text-align: center;"><h2 style="color: blue;">Formulario de Licencia Software</h2></td></tr>
                        <form method='post' action="USAIDInventario/EquipoABM.php">
                        <?php
	                           if($DataUpdate[1] == "Computadoras" || $DataUpdate[1] == "Kit de Lego EV3" || $DataUpdate[1] == "Laptop")
                               {
                                    if($DataUpdate[6] != "Verificado")
                                    {
                        ?>
                                <tr><td style="padding-left: 50px;">Software:</td><td><input type="text" name="Software" style="width: 200px;" required="true" /></td></tr>
                                <tr><td style="padding-left: 50px;">Licencia:</td><td><input type="text" name="Licencia" style="width: 200px;"/></td></tr>
                                <tr><td style="padding-left: 50px;">Tipo de Licencia:</td>
                                    <td>
                                        <select name="Tipo">
                                            <option value="F�sica">F�sica (En CD)</option>
                                            <option value="Instalada">Instalado Software</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td colspan="2" style="text-align: center;"><input type="submit" class="boton" name="Enviar" value="Guardar Licencia" style="width: 120px;" /></td></tr>
                        <?php
                                    }
                                    else
                                        echo "<tr><td colspan='2' style='text-align: center;'><br /><h3 style='color: Green'>Este equipo ya fue Verificado</h3><br /></td></tr>";
	                           }
                               else
                                    echo "<tr><td colspan='2' style='text-align: center;'><br /><h3 style='color: Green'>Este equipo no poseen <br />licenciamientos de software</h3><br /></td></tr>";
                        ?>
                        </form>
                        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
                        <tr>
                            <td colspan="2">
                                
                                <table style="width: 100%; padding-bottom: 20px;">
                                    <tr><td colspan="3" style="text-align: center;"><h2 style="color: blue;">Listado de Licencias</h2></td></tr>
                                    <tr><th style="width: 5%;">N�</th><th style="width: 40%;">Software</th><th style="width: 55%;">Licencia</th><th style="width: 55%;">Tipo de Licencia</th></tr>
                                    <?php
                                            $Licencias = $bddC->prepare("Select * from Inventario_Licencia where IdEquipo = " . $_SESSION["IdEquipo"]);
                                            $Licencias->execute();
                                            
                                            if($Licencias->rowCount() > 0)
                                            {
                                                $i = 0;
                                                while($DataL = $Licencias->fetch())
                                                {
                                                    $i++;
                                                    echo "<tr>
                                                              <td style='text-align: center;'>$i</td>
                                                              <td style='text-align: center;'>$DataL[2]</td>
                                                              <td style='text-align: center;'>$DataL[3]</td>
                                                              <td style='text-align: center;'>$DataL[4]</td>";
                                                    if($DataUpdate[6] != "Verificado")
                                                    {
                                                        if($_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "Administrador" )
                                                        {
                                                            echo "<td>
                                                                    <form method='post' action='USAIDInventario/EquipoABM.php'>
                                                                        <input type='submit' style='width: 30px' class='botonE' name='Enviar' value='x' Onclick='return confirm(\"�Realmente desea borrar este registro?\")' />
                                                                        <input type='hidden' name='IL' value='$DataL[0]' />
                                                                    </form>
                                                                  </td>";
                                                        }
                                                    }
                                                    echo "</tr>";    
                                                }                                                
                                            }
                                            else
                                                echo "<tr><td colspan='3' style='text-align: center;'><br /><em style='color: green;'>Este equipo no posee licencias asignadas</em></td></tr>";
                                    ?>
                                </table>
                            </td>
                        </tr>
                        
                    </table>
                </td>                
            </tr>
        </table>
    <br /><br />
</div>
