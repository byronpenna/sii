<?php

/**
 * @author Gilbetor
 * @copyright 2014
 */

    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=Paquetes_CentrosEscolares.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';
    
    $CentroEscolar = $bddC->prepare("Select * from USAID_Instituciones 
                                     where IdInstitucion in (SELECT IdInstitucion FROM Inventario_Paquete group by IdInstitucion)");
    
    $CentroEscolar->execute();
    
?>

    <table>
        <tr>
            <th>IdInstitucion</th><th>Codigo</th><th>A�o</th><th>Nombre del Centro Escolar</th><th>Municipio</th><th>Direcci�n</th><th>Telefono</th>
            <th>Director</th><th>Telefono Director</th><th>Subdirectora</th><th>Telefono Subdirectora</th>
            <th>Equipo 1</th><th>Equipo 1 - Modelo</th><th>Equipo 1 - Numero Serial</th><th>Equipo 1 - MAC</th>
            <th>Equipo 1 - ESET</th><th>Equipo 1 - Office 2013</th><th>Equipo 1 - Windows 8.1</th>
            
            <th>Equipo 2</th><th>Equipo 2 - Modelo</th><th>Equipo 2 - Numero Serial</th>            
            <th>Equipo 3</th><th>Equipo 3 - Modelo</th><th>Equipo 3 - Numero Serial</th>
            <th>Equipo 4</th><th>Equipo 4 - Modelo</th><th>Equipo 4 - Numero Serial</th>
            <th>Equipo 4 - ESET</th><th>Equipo 4 - Office 2013</th><th>Equipo 4 - Windows 8.1</th>
            
            <th>Equipo 5</th><th>Equipo 5 - Modelo</th><th>Equipo 5 - Numero Serial</th>
            <th>Equipo 6</th><th>Equipo 6 - Modelo</th><th>Equipo 6 - Numero Serial</th>
            <th>Equipo 7</th><th>Equipo 7 - Modelo</th><th>Equipo 7 - Numero Serial</th>
            <th>Equipo 8</th><th>Equipo 8 - Modelo</th><th>Equipo 8 - Numero Serial</th>
            <th>Equipo 9</th><th>Equipo 9 - Modelo</th><th>Equipo 9 - Numero Serial</th>
            <th>Equipo 10</th><th>Equipo 10 - Modelo</th><th>Equipo 10 - Numero Serial</th>                                                                                    
            
        </tr>        
<?php
       while($DataC = $CentroEscolar->fetch())
       {
            echo "<tr>
                    <td>$DataC[0]</td><td>$DataC[1]</td><td>$DataC[2]</td><td>$DataC[3]</td><td>$DataC[4]</td><td>$DataC[5]</td>
                    <td>$DataC[6]</td><td>$DataC[7]</td><td>$DataC[8]</td><td>$DataC[9]</td><td>$DataC[10]</td>";
            
         
            $Paquete = $bddC->prepare("Select * from Inventario_Equipos as e
                                       inner join Inventario_Paquete as p on e.IdEquipo = p.IdEquipo
                                       where p.IdInstitucion = $DataC[0]
                                       ORDER BY  e.Equipo ASC");            
            $Paquete->execute();
            

            while($DataP = $Paquete->fetch())
            {
                echo "<td>$DataP[1] </td><td> $DataP[2] </td><td> $DataP[3] </td>";
                
                if($DataP[1] == "Computadoras")
                    echo "<td> $DataP[4] </td>";
                    
                if($DataP[1] == "Laptop" || $DataP[1] == "Computadoras")
                {
                    
                    $Licencia = $bddC->prepare("Select * from Inventario_Licencia where IdEquipo =  $DataP[0] ORDER BY Software ASC");            
                    $Licencia->execute();
                    
                    while($DataL = $Licencia->fetch())
                    {
                        echo "<td>$DataL[3]</td>";
                    }
                }                
            }     
    
                    
                    
            echo "</tr>";
       }
?>
    </table>