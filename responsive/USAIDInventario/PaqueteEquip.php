<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
      
if(isset($_POST['Busqueda']))
    $Busqueda = $_POST['Busqueda'];
else
    $Busqueda = "";   
            
$_SESSION['Paquete'] = "Paquete 1";            

?>

<div style="width: 100%; background-color: white; border-radius: 5px;">    
    <div class="Raiz" style="padding-top: 10px;">
        <a href="/Inventario.php" class='flecha' style='text-decoration:none'> Inventario </a> ->
        <a href="/Inventario.php?i=Package" class='flecha' style='text-decoration:none'> Paquetes </a>->
        <a href="/Inventario.php?i=Package" class='flecha' style='text-decoration:none'> Nuevo Paquetes </a>->
    </div>
     
    <table style="width: 90%; margin-left: 5%;">    
     
    <tr><td colspan="2"><h2 style="color: blue;">Datos del Paquete</h2><hr color='skyblue' /></td></tr>
    <tr><td colspan="2">
    <?php
	 
               $CentroEscolar = $bddC->prepare("Select * from USAID_Instituciones where IdInstitucion =" . $_SESSION['IC']);
               $CentroEscolar->execute();
               $DataC = $CentroEscolar->fetch();
               
               echo "<table style='width: 80%;'>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Centro escolar destino: </td><td>$DataC[3]</td></tr>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Municipio: </td><td>$DataC[4]</td></tr>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Direcci�n: </td><td>$DataC[5]</td></tr>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Tel�fono: </td><td>$DataC[6]</td></tr>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Director: </td><td>$DataC[7]</td></tr>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Celular director: </td><td>$DataC[8]</td></tr>
                     </table>
                     ";
    ?>         
        <hr color='skyblue' />
        </td>
    </tr>
    <tr><td colspan="2"><h2 style="color: blue;">Equipo del Paquete</h2></td></tr>
    <tr>    
        <td colspan="2">
            <table style="width: 80%; margin-left: 10%;text-align: center;">
                <?php
	                   $EquipoPaqueteAux = $bddC->prepare("Select * from Inventario_Equipos as e
                                                           inner join Inventario_Paquete as p on e.IdEquipo = p.IdEquipo 
                                                           where p.IdInstitucion = " . $_SESSION['IC'] . " Group by p.IdEquipo");
                       $EquipoPaqueteAux->execute();
                       
                       if($EquipoPaqueteAux->rowCount() == 0)
                            echo "<tr><td colspan='4' style='color: red;'><h3>No hay equipos en este paquete...</h3></td></tr>";
                            
                       else
                            echo "<tr><th>Equipo</th><th>Modelo</th><th>N�mero Serial</th></tr>";
                        
                       while($DataEq = $EquipoPaqueteAux->fetch())
                       {
                            echo "<tr>
                                      <td>$DataEq[1]</td><td>$DataEq[2]</td><td>$DataEq[3]</td>
                                      <td><form action='USAIDInventario/PaqueteABM.php' method='post'>
                                            <input type='hidden' name='IE' value='$DataEq[0]' />";
                                            
                            if($_SESSION["TipoUsuario"] == "USAID-CI" || $_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "Administrador")
                                echo "      <input style='width: 50px' type='submit' class='botonE' name='Enviar' value='x' onClick='return confirm(\"Seguro de quitar este equipo?\")' />";
                            
                            echo "         </form>
                                      </td>
                                  </tr>";
                                  
                            $VPaquete = $DataEq[14];
                       }
                                        
                ?>
            </table>
            <br /><br />
            <form action="USAIDInventario/PaquetePrint.php" method="post" target="_blank">
            <div style="text-align: right; width: 10%; float: right; padding-right: 20px;">
                <input type='submit' value='Imprimir' class='boton' />
            </div>
            
            <table style="width: 50%;">
                <tr><td colspan="2">Equipo Extra:</td></tr>
                <tr><td style="padding-left: 30px;"><input type="checkbox" value="Malet�n" name="Equipo[]" />Malet�n</td>
                    <td><input type="checkbox" value="D-link Ethernet" name="Equipo[]" />D-link Ethernet</td></tr>
                <tr><td style="padding-left: 30px;"><input type="checkbox" value="Audifonos de PC" name="Equipo[]" />Audifonos de PC</td>
                    <td><input type="checkbox" value="Audifonos para Laptop" name="Equipo[]" />Audifonos para Laptop</td></tr>
            </table>
            </form>
            <br />
            
            <hr color='skyblue' />
        </td>        
    </tr>
    
    <?php	
    if($_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "Administrador")
    {
    ?>
    
    <form action="" method="post">
    <tr><td colspan="2"><h2 style="color: blue;">Busqueda de Equipo</h2></td></tr>
    <tr>
        <td style="text-align: right; padding-right: 10px;width: 50%;">Escriba el N�mero de Serial o Numero del Producto:</td>
        <td><input type="text" name="Busqueda" value="<?php echo $Busqueda?>" style="width: 80%;" />
    </tr>
    </form>
    <tr>
        <td colspan="2">
        <br />
        <?php
           if(isset($_POST['Busqueda']))
           { 
               
               $CentroEscolar = $bddC->prepare("Select * from Inventario_Equipos where SerialNumber like '%$Busqueda%' or MAC like '%$Busqueda%'");
               $CentroEscolar->execute();
               
               echo "<table style='width: 100%'>
                     <tr><th>Equipo</th><th>Modelo</th><th>N�mero Serial</th><th>MAC</th><th>Estado</th><th>Acciones</th>";
               
               if($CentroEscolar->rowCount() > 0)
               {                  
                   while($DataE = $CentroEscolar->fetch())
                   {                        
                        echo "<tr>
                                  <form action='USAIDInventario/PaqueteABM.php' method='post'>
                                  <td>$DataE[1]</td><td>$DataE[2]</td><td>$DataE[3]</td><td>$DataE[4]</td><td>$DataE[5]</td>
                                  ";
                                  
                        if($DataE[6] == "Verificado")
                        {
                            $EquipoPaquete = $bddC->prepare("Select * from Inventario_Paquete where IdEquipo = $DataE[0]");
                            $EquipoPaquete->execute();
                            
                            if($EquipoPaquete->rowCount() == 0)
                            {
                                echo "<td>
                                          <input type='hidden' name='IE' value='$DataE[0]' />
                                          <input type='submit' class='botonG' name='Enviar' value='A�adir' />
                                          
                                      </td>
                                      </form>                                  
                                  </tr>";
                            }
                            else
                                echo "<td><em style='color: green'>A�adido</em></td></tr>";
                        }          
                        else
                            echo "<td><em style='color: red'>$DataE[6]</em></td></tr>";                     
                   }               
               }
               else
                   echo "<tr><td colspan='2' style='text-align: center'><br /><h3 style='color: red;'>No hay resultados de la busqueda...</h3></td></tr>";
               
               echo "</table>";
           }
        ?>
        
        </td>
    </tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <?php
    }
    if($_SESSION["TipoUsuario"] == "USAID-SE" || $_SESSION["TipoUsuario"] == "Administrador")
    {
        if($VPaquete == "No Verificada")
        $Color = "red";
        
        else 
        $Color = "green";
        
        echo "<tr><td colspan='2'><h2 style='color: blue;'>Estado del Paquete</h2></td></tr>
              <tr><td colspan='2'>
                  <form action='USAIDInventario/PaqueteABM.php' method='post'>
                  <table style='width: 50%; margin-left: 25%'>
                        <tr>
                            <td>Estado del Paquete: <em style='padding-left: 20px; color: $Color;'>$VPaquete</em></td>";
                            
                            if($VPaquete == "No Verificada")
                                echo "<td><input type='submit' class='botonG' name='Enviar' value='Validar' /></td>";
                                
                  echo "</tr>
                  </table>
                  </form>
                  </td>
              </tr>
              <tr><td colspan='2'><hr color='skyblue' /></td></tr>";
              
          
        
        $Actas = $bddC->prepare("Select * from Inventario_Acta where IdInstitucion = " . $_SESSION['IC']);
        $Actas->execute();
        
        echo "<tr><td colspan='2'><h2 style='color: blue;'>Seguimiento del Paquete</h2></td></tr>
              <tr>
              <td colspan='2'>";

                if($Actas->rowCount()> 0)
                {
                    echo "<table style='width: 80%; margin-left: 10%'>
                          <tr><th>Paquete</th><th>Nombre del Acta</th><th colspan='2'>Acciones</th></tr>";
                          
                    while($DataA = $Actas->fetch())
                    {
                        echo "<tr>
                                  <td>$DataA[2]</td>
                                  <td>" . substr($DataA[3],7,strlen($DataA[3])-6)."</td>
                                  <th>  
                                      <a target='_blank' href='DocumentosDescarga.php?file=$DataA[3]&aux=se' >
                                         <img src='images/icono_descarga.jpg' style='width: 25px; height: 25px;'  />
                                      </a>
                                  </th>  
                                  <th>
                                  <form action='USAIDInventario/PaqueteABM.php' method='post'>
                                     <input type='hidden' name='file' value='$DataA[3]' />";
                                     
                                     if($_SESSION["TipoUsuario"] == "Administrador")
                                        echo "<input type='submit' name='Enviar' Value='Quitar' class='botonE' style='width: 60px;' Onclick='return confirm(\"Seguro de Borrar este archivo?\");'/>";
                                        
                        echo "    </form>
                                  </th>
                              </tr>";
                    }
                    echo "</table>
                          <hr color='skyblue' style='width: 80%;' />";
                }
                else
                    echo "<tr><td></td></tr>";
            
        echo "</td>
              </tr>";
        
        echo "<tr><td colspan='2'>
                  <form action='USAIDInventario/PaqueteABM.php' method='post' enctype='multipart/form-data'>  
                  <table style='width: 80%; margin-left: 10%'>
                        <tr>
                            <td>Anexe las Actas de Entrega:</td>
                            <td><input name='archivo' type='file' required='true' title='Busque el Archivo del Acta'/></td>
                            <td><input type='submit' class='botonG' name='Enviar' value='Subir' /></td>
                        </tr>
                  </table>
                  </form>
                  </td>
              </tr>";              
    }
    ?>
    
    </table>
    <br /><br />
    
    
    
</div>