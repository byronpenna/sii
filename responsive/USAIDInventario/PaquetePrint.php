<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

?>
<?php
	session_start();
    require '../net.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Ficha del Empleado</title>
</head>
<script >
function cerrar() { setTimeout(window.close,1500); }
</script>

<body onload="window.print();cerrar();" style="font-family: Calibri; font-size: small;">
<div style="width: 94%; margin-left: 3%;">

<div style="width: 100%; background-color: white; border-radius: 5px;">    
    <table style="width: 90%; margin-left: 5%;">    
    <tr><td colspan="2"><h2 style="color: blue;">Datos del Paquete</h2><hr color='skyblue' /></td></tr>
    <tr><td colspan="2">    
    <?php
	 
               $CentroEscolar = $bddC->prepare("Select * from USAID_Instituciones where IdInstitucion =" . $_SESSION['IC']);
               $CentroEscolar->execute();
               $DataC = $CentroEscolar->fetch();
               
               echo "<table style='width: 80%;'>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Centro escolar destino: </td><td>$DataC[3]</td></tr>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Municipio: </td><td>$DataC[4]</td></tr>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Direcci�n: </td><td>$DataC[5]</td></tr>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Tel�fono: </td><td>$DataC[6]</td></tr>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Director: </td><td>$DataC[7]</td></tr>
                       <tr><td style='width: 25%; text-align: right; padding-right: 10px;'>Celular director: </td><td>$DataC[8]</td></tr>
                     </table>
                     ";
    ?>         
        <hr color='skyblue' />
        </td>
    </tr>
    <tr><td colspan="2"><h2 style="color: blue;">Equipo del Paquete</h2></td></tr>
    <tr>    
        <td colspan="2">
            <table style="width: 80%; margin-left: 10%;text-align: center;">
                <?php
	                   $EquipoPaqueteAux = $bddC->prepare("Select * from Inventario_Equipos as e
                                                           inner join Inventario_Paquete as p on e.IdEquipo = p.IdEquipo 
                                                           where p.IdInstitucion = " . $_SESSION['IC'] . " Group by p.IdEquipo");
                       $EquipoPaqueteAux->execute();
                       
                       if($EquipoPaqueteAux->rowCount() == 0)
                            echo "<tr><td colspan='4' style='color: red;'><h3>No hay equipos en este paquete...</h3></td></tr>";
                            
                       else
                            echo "<tr><th>Equipo</th><th>Modelo</th><th>N�mero Serial</th></tr>";
                        
                       while($DataEq = $EquipoPaqueteAux->fetch())
                       {
                            echo "<tr><td>$DataEq[1]</td><td>$DataEq[2]</td><td>$DataEq[3]</td></tr>";
                                  
                            $VPaquete = $DataEq[14];
                       }                                              
                       
         
                ?>
            </table>
            <br /><br />
<?php
                       $Extra =  $_POST['Equipo'];
                       
                       echo "Equipo extra:<br />";
                       if(empty($Extra))                       
                            echo("No hay equipo Extra en este paquete");
                        
                       else
                       {
                           $N = count($Extra);
                           echo "<ol>";
                           for($i=0; $i < $N; $i++)
                           {
                             echo("<li>" . $Extra[$i] . "</li>");
                           }
                           echo "</ol>";
                       }    
?>            
            <br />
            <br />
            <hr color='skyblue' />                  
        </td>        
    </tr>   
    <tr>
        <td style="text-align: center;">
            <table style="width: 80%;">
                <tr><td>Supervisor Inventario:</td><td>_______________________</td></tr>
                <tr><td>Firma:</td><td>_______________________</td></tr>
            </table>      
        </td>
        <td style="text-align: center;">
            <table style="width: 80%;"> 
                <tr><td>Supervisor Entrega:</td><td>_______________________</td></tr>
                <tr><td>Firma:</td><td>_______________________</td></tr>
            </table>      
        </td>        
    </tr> 
</div>
</body>
</html>