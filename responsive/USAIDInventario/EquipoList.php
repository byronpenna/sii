<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */


    unset($_SESSION["IdEquipo"]);    
    
    if(!isset($_POST['fecha']))
        $fecha = date("Y-m-d");
        
    else
        $fecha = $_POST['fecha'];
        
    
    $Query = "Select * from Inventario_Equipos ";

    if(isset($_POST['Enviar']))
    {
        if($_POST['Enviar'] == "Ver por Fecha")
        {              
            if($_POST['intervalo'] == '')
                $Query .= " where FechaCreacion = '$fecha' and Estado != 'Borrado' ";
            
            else 
                $Query .= " where (FechaCreacion BETWEEN '$fecha' and '".$_POST['fecha2']."') and Estado != 'Borrado' ";
        }
        else                
            $Query .= "where Estado != 'Borrado' ";
            
            
        if($_POST['TipoRecurso'] != "All")
            $Query .= " and Equipo = '".$_POST['TipoRecurso']."' ";
    }
    else                     
        $Query .= "where FechaCreacion = '" . date("Y-m-d") ."' and Estado != 'Borrado' ";        
    
    if(isset($_POST['Consultores']))
    {
        if($_POST['Consultores'] != "All")
        $Query .= " and Usuario = " . $_POST["Consultores"];
    }
    
    if(isset($_POST['BusquedaE']))
        $Query .= " and (SerialNumber like '%" . $_POST['BusquedaE'] ."%' or Modelo like '%" . $_POST['BusquedaE'] ."%')";
        
    
    
    if($_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "Administrador")
        $Query .= " ORDER BY  IdEquipo DESC";
    
    else
       $Query .= " and Usuario = ". $_SESSION["IdUsuario"] ." ORDER BY  IdEquipo DESC";

    $_SESSION['AuxQuery'] = $Query;
    
    
    $Verificacion = $bddC->prepare($Query);
    $Verificacion->execute();
    
    $EquiposTotal = $bddC->prepare("Select * from Inventario_Equipos where Estado != 'Borrado'");
    $EquiposTotal->execute();
    

//echo $_POST['intervalo'] . "<br /> $Query";      
?>
<script>

function fechafinal()
{
    if(document.getElementById("intervalo").checked == true){
        document.getElementById("fecha2").disabled= false;
    } else {
        document.getElementById("fecha2").disabled= true;
    }
}

</script>
<div style="width: 100%; background-color: white; border-radius: 5px;">
    <div class="Raiz" style="padding-top: 10px;">
        <a href="/Inventario.php" class='flecha' style='text-decoration:none'> Inventario </a>
    </div> 
    <hr color='Skyblue' style="width: 90%; margin-left: 5%;" />
        <table style="width: 90%; margin-left: 5%;">
        <tr>
            <td style="padding-left: 20px;">
                <table style="width: 100%;">
                    <tr><td style="width: 50%;"><h2 style="color: blue;">Inventario de Recursos Tecnol�gicos</h2></td>
                        <td style="text-align: right;">
                        <table style="width: 50%; padding-left: 50%;">
                            <tr>
                            <?php
    	                           if($_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "Administrador")
                                   {                              
                                        echo "<td>
                                              <form action='Inventario.php?i=Package' method='Post'>
                                                <input type='submit' class='boton' value='Paquetes' />
                                              </form>
                                              </td>";                                          
                                   }
                                   if($_SESSION["TipoUsuario"] == "Administrador")
                                   {
                                       echo "<td>
                                              <form action='Inventario.php?i=Statistics' method='Post'>
                                                <input type='submit' class='boton' value='Detalles' />
                                              </form>
                                             </td>";
                                   }   
                            ?>
                            </tr>
                        </table>
                        </td>
                    </tr>                    
                </table>
            </td>
        </tr>
        <tr><td><hr color='skyblue' /></td></tr>
        <tr>
            <td>
                <table style="width: 100%; text-align: center;">
                    <tr>
                        <td colspan="10">
                        <table style="width: 100%;">                            
                            <tr>
                                <td class="tdleft" style="width: 40%;">Fecha del Hoy:</td>
                                <td style="text-align: left; width: 20%;"><?php echo date("Y-m-d"); ?></td>
                                <form action="Inventario.php?i=EquipForm" method="post">
                                    <td colspan="2" style="width: 50%;">                                    
                                        <input type="submit" value="Agregar Equipo" class="boton" style="width: 120px;" />                                    
                                    </td>
                                </form>
                            </tr>
                            <form action="Inventario.php" method="post">                        
                            <tr>
                                
                                    <td class="tdleft">Seleccione los equipos a observar:</td>
                                    <td>
                                        <select name="TipoRecurso" style="width: 150px;">
                                        <?php
                                            echo $_POST['TipoRecurso'] == "All"             ? "<option selected='true' value='All'>Todos</option>"                          : "<option value='All'>Todos</option>";                                            
                                            echo $_POST['TipoRecurso'] == "Computadoras"    ? "<option selected='true' value='Computadoras'>Computadoras</option>"          : "<option value='Computadoras'>Computadoras</option>";
                                            echo $_POST['TipoRecurso'] == "Laptop"          ? "<option selected='true' value='Laptop'>Laptop</option>"                      : "<option value='Laptop'>Laptop</option>";
                                            echo $_POST['TipoRecurso'] == "Teclado"         ? "<option selected='true' value='Teclado'>Teclado</option>"                    : "<option value='Teclado'>Teclado</option>";
                                            echo $_POST['TipoRecurso'] == "Mouse"           ? "<option selected='true' value='Mouse'>Mouse</option>"                        : "<option value='Mouse'>Mouse</option>";
                                            echo $_POST['TipoRecurso'] == "Kit de Lego EV3" ? "<option selected='true' value='Kit de Lego EV3'>Kit de Lego EV3</option>"    : "<option value='Kit de Lego EV3'>Kit de Lego EV3</option>";
                                            echo $_POST['TipoRecurso'] == "Kit de K-Nex"    ? "<option selected='true' value='Kit de K-Nex'>Kit de K-Nex</option>"          : "<option value='Kit de K-Nex'>Kit de K-Nex</option>";
                                            echo $_POST['TipoRecurso'] == "UPS"             ? "<option selected='true' value='UPS'>UPS</option>"                            : "<option value='UPS'>UPS</option>";
                                            echo $_POST['TipoRecurso'] == "Modems"          ? "<option selected='true' value='Modems'>Modems</option>"                      : "<option value='Modems'>Modems</option>";
                                            echo $_POST['TipoRecurso'] == "Fuente de Poder" ? "<option selected='true' value='Fuente de Poder'>Fuente de Poder</option>"    : "<option value='Fuente de Poder'>Fuente de Poder</option>";
                                        ?>
                                        </select>
                                    </td>
                                    <td colspan="2"><input type="submit" value="Ver por Fecha" name="Enviar" class="boton" style="width: 120px;" /></td>  
                            </tr> 
                                                           
                            <?php
                                    if($_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "Administrador")
                                    {
                                        echo "<td class='tdleft'>Seleccione a un Consultor:</td>
                                              <td>
                                                  <select name='Consultores' style='width: 150px;'>";
                                        
                                        $Consultores = $bddC->prepare("SELECT Usuario FROM Inventario_Equipos GROUP BY Usuario");
                                        $Consultores->execute();
                                        
                                        echo "<option selected='true' value='All'>Todos</option>";
                                        
                                        while($DataC = $Consultores->fetch())
                                        {
                                            $Usuario = $bdd->prepare("Select NombreUsuario from usuario where Id = $DataC[0]");
                                            $Usuario->execute();
                                            $DataU = $Usuario->fetch();
                                            
                                            if($_POST['Consultores'] == $DataC[0])                                        
                                                echo "<option selected='true' value='$DataC[0]'>$DataU[0]</option>";
                                            
                                            else
                                                echo "<option value='$DataC[0]'>$DataU[0]</option>";
                                        }


                               
                                        echo "    </select>
                                              </td>
                                              <td><input type='submit' value='Ver Todos' name='Enviar' class='boton' style='width: 120px;' /></td>";
                                    }
                            ?>
                            <tr>
                                <td class="tdleft"> Busqueda por N�mero Serial o Modelo</td>
                                <td colspan="2"><input type="text" style="width: 32%;" name="BusquedaE" value="<? echo $_POST['BusquedaE']?>" /></td>
                            </tr>
                            <tr>
                                <td class="tdleft"> Fecha Inicial</td>
                                <td><input type="date" name="fecha" style="width: 150px" value="<? echo $fecha; ?>" placeholder="aaaa-mm-dd" /></td>
                                <td colspan="2"><input type="checkbox" id="intervalo" name="intervalo" value="intervalo" onclick='fechafinal();' /> Verificar por Intervalos de Fecha</td>                                
                            </tr>
                            <tr>
                                <td class="tdleft"> Fecha Final</td>
                                <td colspan="2"><input type="date" id="fecha2" name="fecha2" disabled="true" style="width: 150px;" value="<? echo $_POST['fecha2']; ?>" placeholder="aaaa-mm-dd" /></td>                                
                            </tr>                            
                            </form>
                            <tr>
                                <td class="tdleft">Resultado de Busqueda:</td>
                                <td><?php echo $Verificacion->rowCount(); ?></td>
                                <td colspan="2" style="border-left-width: 10px; border-left-color: skyblue;">
                                <form action="USAIDInventario/EquipoListExport.php" method="post" >                                                                
                                    <input type="submit" value="Exportar" name="Enviar" class="boton" style="width: 120px;" /></td>
                                </form>
                            </tr>                                               
                        </table>
                        <br />
                        <hr color='skyblue' />
                        <br />
                        </td>
                    </tr>  
                    <tr><th style="width: 5%;">N�</th>
                        <th style="width: 10%;">Equipo</th>
                        <th style="width: 20%;">Modelo</th>
                        <th style="width: 30%;">N�mero de Serial</th>
                        <th style="width: 10%;">Estado de Verificaci�n</th>
                        <th style="width: 10%;">Estado</th>
                        <!--<th style="width: 15%;">Fecha</th>--!>
                        <th colspan="2">Acciones</th>
                    </tr>      
                    <?php                    
                        if($Verificacion->rowCount())
                        {
                            $i = 1;
                            while($Equipo = $Verificacion->fetch())
                            {
                                if($Equipo[6] == "Verificado")
                                    $color = "Green";
                                else
                                    $color = "Black";
                                
                                // agregar <td style='color: $color'>$Equipo[8]</td> para la fecha
                                echo "<tr>
                                          <td style='color: $color'>$i</td>  
                                          <td style='color: $color'>$Equipo[1]</td>
                                          <td style='color: $color'>$Equipo[2]</td>
                                          <td style='color: $color'>$Equipo[3]</td>
                                          <td style='color: $color'>$Equipo[6]</td>
                                          <td style='color: $color'>$Equipo[5]</td>
                                          
                                          <td colspan='2'>
                                            <form action='USAIDInventario/EquipoABM.php' method='post'>
                                                <input type='hidden' name='IE' value='$Equipo[0]' />
                                                <input type='submit' class='botonG' name='Enviar' value='Ver' style='width: 80px;' />                                                                                                
                                            </form>
                                          </td>
                                      </tr>";

                                $i++;
                            }
                        }
                        else                        
                            echo "<tr><td colspan='10' style='text-align: center;'><br /><h3 style='color: blue;'>No hay datos ingresados para el dia de hoy</h3></td></tr>";                        
                    ?>
                </table>
            </td>        
        </tr>
    </table>
    <br />
</div>