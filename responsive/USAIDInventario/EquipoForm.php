<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 
 if($_SESSION["IdEquipo"] != "")
 {
    $Update = $bddC->prepare("Select * from Inventario_Equipos where IdEquipo = " . $_SESSION["IdEquipo"]);
    $Update->execute();
    
    $DataUpdate = $Update->fetch();
    
    $ABM = "Actualizar Recurso";
 }
 else
 {
    unset($DataUpdate);
    $ABM = "Guardar Recurso";
    
    unset($DataUpdate);
 }
?>
<script>
function verificacion()
{
    
    var seleccion = document.getElementById('TipoRecurso').value;
    document.getElementById("MAC").value = "";
    
    if((seleccion == "Computadoras") || (seleccion == "Laptop"))
        document.getElementById("MAC").style.display = "block";        
    
    else
    {
        document.getElementById("MAC").value = "-";
        document.getElementById("MAC").style.display = "none";
    }                
}
</script>

<div style="width: 100%; background-color: white; border-radius: 5px;">    
    <div class="Raiz" style="padding-top: 10px;">
        <a href="/Inventario.php" class='flecha' style='text-decoration:none'> Inventario </a> ->
        <a href="/Inventario.php?i=EquipForm" class='flecha' style='text-decoration:none'> Agregar Equipo </a>
    </div>
    <hr color='Skyblue' style="width: 90%; margin-left: 5%;" />
    <form method="Post" action="USAIDInventario/EquipoABM.php">
    <table style="width: 90%; margin-left: 5%;">
        <tr>
            <td style="width: 50%;"><h2 style="color: blue; padding-left: 30px;">Formulario de Equipo</h2></td>
            <td style="text-align: left;">
            </td>
        </tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td colspan="2">
                <table style="width: 60%; margin-left: 20%;">
                    <tr>
                        <td class="tdleft">Tipo de Equipo:</td>
                        <td>
                            <select name="TipoRecurso" id="TipoRecurso" onchange="verificacion();">
                                <?php
                                    echo $DataUpdate[1] == "Computadoras"    ? "<option selected='true' value='Computadoras'>Computadoras</option>"          : "<option value='Computadoras'>Computadoras</option>";
                                    echo $DataUpdate[1] == "Laptop"          ? "<option selected='true' value='Laptop'>Laptop</option>"                      : "<option value='Laptop'>Laptop</option>";
                                    echo $DataUpdate[1] == "Teclado"         ? "<option selected='true' value='Teclado'>Teclado</option>"                    : "<option value='Teclado'>Teclado</option>";
                                    echo $DataUpdate[1] == "Mouse"           ? "<option selected='true' value='Mouse'>Mouse</option>"                        : "<option value='Mouse'>Mouse</option>";                                    
                                    echo $DataUpdate[1] == "Kit de Lego EV3" ? "<option selected='true' value='Kit de Lego EV3'>Kit de Lego EV3</option>"    : "<option value='Kit de Lego EV3'>Kit de Lego EV3</option>";
                                    echo $DataUpdate[1] == "Kit de K-Nex"    ? "<option selected='true' value='Kit de K-Nex'>Kit de K-Nex</option>"          : "<option value='Kit de K-Nex'>Kit de K-Nex</option>";
                                    echo $DataUpdate[1] == "UPS"             ? "<option selected='true' value='UPS'>UPS</option>"                            : "<option value='UPS'>UPS</option>";
                                    echo $DataUpdate[1] == "Modems"          ? "<option selected='true' value='Modems'>Modems</option>"                      : "<option value='Modems'>Modems</option>";
                                    echo $DataUpdate[1] == "Fuente de Poder" ? "<option selected='true' value='Fuente de Poder'>Fuente de Poder</option>"    : "<option value='Fuente de Poder'>Fuente de Poder</option>";
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr><td class="tdleft" >Modelo:</td><td><input type="text" name="Modelo" value="<?php echo $DataUpdate[2];?>" required="true" title="Campo Obligatorio" /></td></tr>
                    <tr><td class="tdleft" style="vertical-align: top;">Numero Serial i/o <br />Numero del Producto:</td><td><textarea cols="30" rows="5" id="Serial" name="Serial" required="true"><?php echo $DataUpdate[3];?></textarea></td></tr>
                    <tr><td class="tdleft" style="vertical-align: top;">Direcci�n MAC:</td><td><textarea cols="30" rows="5" id="MAC" name="MAC"><?php echo $DataUpdate[4];?></textarea></td></tr>
                    <tr><td class="tdleft">Periodo de Garantia:</td><td><input type="text" name="garantia" value="<?php echo $DataUpdate[7];?>"  /></td></tr>
                    <tr><td colspan="2" style="text-align: center;  padding-top: 10px;"><input type="submit" name="Enviar" class="boton" value="<?php echo $ABM;?>" style="width: 120px;" /></td></tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <br /><br />
</div>