<?php

/**
 * @author Manuel
 * @copyright 2013
 */
         
?>
        <div class="Raiz">
            <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
            <a href="/Instituciones.php" class='flecha' style='text-decoration:none'>Instituciones</a> ->
            <a href="/Instituciones.php?in=InsList" class='flecha' style='text-decoration:none'>Lista de Instituciones</a> ->
            <a href="/Instituciones.php?in=Profile" class='flecha' style='text-decoration:none'>Perfil de Institución</a> ->
            <a href="/Instituciones.php?in=Group" class='flecha' style='text-decoration:none'>Grupos</a> ->            
        </div>
<?php
        $datosinstitucion = $bdd->prepare("SELECT * FROM  Institucion where IdInstitucion = :idIns ");
        $datosinstitucion->bindParam(':idIns', $_SESSION["Ins"]);
        $datosinstitucion->execute();
        
        if($datosinstitucion->rowCount() == 0)
        Redireccion("../Instituciones.php");
        
        $institucion = $datosinstitucion->fetch();
?>
<div style='width: 90%; background-color: white; border-radius: 20px; border-color: skyblue; border-style: groove; margin-left: 5%; margin-top: 20px; padding:20px;'>
        <h2 style="float: left; width: 50%">Listado de Grupos o Secciones</h2>         
        <?php
            if(isset($_GET['n']))
            {
                if($_GET['n'] == 1)
                    echo "<div id='Notificacion' name='Notificacion' style='color:green; float: right; width: 300px; height: auto; padding-bottom:20px;'>Contacto Ingresados Exitosamente</div>";  
                     
                if($_GET['n'] == 2)
                    echo "<div id='Notificacion' name='Notificacion' style='color:red; float: right; width: 300px; height: auto; padding-bottom:20px;'>Contacto Exliminado Exitosamente</div>";                                                     
            } 
        ?>
        <table style="width: 90%; margin-left: 5%;">
            <tr><td style="width: 30%; text-align: right; padding-right: 20px;"><h2>Institución:</h2></td><td><h2><?php echo "$institucion[1]";?></h2></td></tr>
            <tr><td colspan="2">
                <table style="width: 40%; float: left;">
                    <tr><td style="width: 50%;"></td><td style="width: 50%;"></td></tr>                    
                    <?php
	                       $datosSeccion = $bdd->prepare("SELECT * FROM  Institucion_Seccion where IdInstitucion = :idIns ");
                           $datosSeccion->bindParam(':idIns', $_SESSION["Ins"]);
                           $datosSeccion->execute();
                           
                           if($datosSeccion->rowCount() == 0)
                           echo "<tr><td colspan='5' style='text-align: center;'><h2 style='color: red'>No hay Grupos Registrados</h2></td></tr>";
                           
                           else
                           {
                                while($seccion = $datosSeccion->fetch())
                                {
                                    echo "<tr><td>$seccion[0] </td>
                                              <td style='width: 50%; text-align: right; padding-right: 20px;'>$seccion[2]</td>
                                              <td>
                                                    <form action='Instituciones/InstitucionesABM.php' method='post' name='Institución' id='Institución'>
                                                    <input type='hidden' name='IdGroup' value='$seccion[0]' />                     
                                                    <input id='Enviar' name='Enviar' class='botonE' type='submit' value='x' style='width: 35px;' onClick='return confirmar();' />
                                                    </form>
                                               <td>
                                          </tr>";
                                }
                           }
                    ?>
                </table>
                <table style="width: 50%; float: right; margin-right: 5%;">
                    <form action='Instituciones/InstitucionesABM.php' method='post'> 
                        <tr><td colspan="2" style="text-align: center;">Nuevo Grupo o Sección</td></tr>
                        <tr><td style='width: 50%; text-align: right; padding-right: 20px;'>Nombre Completo: </td>
                            <td style='color: red;'><input  required='true' title='Designe el nombre del Grupo o Sección' type='text' name='Nombre' value='' />*</td>
                        </tr>
                        <tr><td colspan='2' style='text-align: center;'><input type='submit' style="width: 150px;" class='boton' name='Enviar' id='Enviar' value='Agregar Grupo' /></td></tr>
                    </form>                                
                </table>
            </td></tr>
        </table>
    </div>