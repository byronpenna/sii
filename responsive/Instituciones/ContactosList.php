<?php

/**
 * @author Manuel
 * @copyright 2013
 */
         
?>
        <div class="Raiz">
            <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
            <a href="/Instituciones.php" class='flecha' style='text-decoration:none'>Instituciones</a> ->
            <a href="/Instituciones.php?in=InsList" class='flecha' style='text-decoration:none'>Lista de Instituciones</a> ->
            <a href="/Instituciones.php?in=Profile" class='flecha' style='text-decoration:none'>Perfil de Institución</a> ->
            <a href="/Instituciones.php?in=Contact" class='flecha' style='text-decoration:none'>Contactos</a> ->            
        </div>
<?php
        $datosinstitucion = $bdd->prepare("SELECT * FROM  Institucion where IdInstitucion = :idIns ");
        $datosinstitucion->bindParam(':idIns', $_SESSION["Ins"]);
        $datosinstitucion->execute();
        
        if($datosinstitucion->rowCount() == 0)
        Redireccion("../Instituciones.php");
        
        $institucion = $datosinstitucion->fetch();
     	 
?>
    <div style='width: 90%; background-color: white; border-radius: 20px; border-color: skyblue; border-style: groove; margin-left: 5%; margin-top: 20px; padding:20px;'>
        <h2 style="float: left; width: 50%">Directorio de Contactos</h2> 
        <a style="text-decoration: none;" href="Instituciones.php?in=ContactForm"><input type="button" style="float: right; width: 150px; margin-right: 5%; margin-top: 10px;" class="boton" value="Agregar Contacto" /></a>
        
        <?php
            if(isset($_GET['n']))
            {
                if($_GET['n'] == 1)
                    echo "<div id='Notificacion' name='Notificacion' style='color:green; float: right; width: 300px; height: auto; padding-bottom:20px;'>Contacto Ingresados Exitosamente</div>";  
                     
                if($_GET['n'] == 2)
                    echo "<div id='Notificacion' name='Notificacion' style='color:red; float: right; width: 300px; height: auto; padding-bottom:20px;'>Contacto Exliminado Exitosamente</div>";                                                     
            } 
        ?>
        <table style="width: 90%; margin-left: 5%;">
            <tr><td style="width: 30%; text-align: right; padding-right: 20px;"><h2>Institución:</h2></td><td><h2><?php echo "$institucion[1]";?></h2></td></tr>
            <tr><td colspan="2">
                <table style="width: 100%;">
                    <tr><td style="width: 5%;">Id</td><td style="width: 40%;">Nombre Completo</td>
                        <td style="width: 30%;">Cargo</td><td style="width: 15%;">Telefono</td><td style="width: 10%;"></td></tr>                    
                    <?php
	                       $datoscontactos = $bdd->prepare("SELECT * FROM  Institucion_Contactos where IdInstitucion = :idIns ");
                           $datoscontactos->bindParam(':idIns', $_SESSION["Ins"]);
                           $datoscontactos->execute();
                           
                           if($datoscontactos->rowCount() == 0)
                           echo "<tr><td colspan='5' style='text-align: center;'><h2 style='color: red'>No hay contactos Registrados</h2></td></tr>";
                           
                           else
                           {
                                while($contactos = $datoscontactos->fetch())
                                {
                                    echo "<tr><td>$contactos[0]</td><td>$contactos[2]</td><td>$contactos[3]</td><td>$contactos[4]</td>
                                               <td>
                                                    <form action='Instituciones/ContactoABM.php' method='post' name='Institución' id='Institución'>
                                                    <input type='hidden' name='IdCon' value='$contactos[0]' />                     
                                                    <input id='Enviar' name='Enviar' class='boton' type='submit' value='Eliminar' style='width: 100px;' onClick='return confirmar();' />
                                                    </form>
                                               <td>
                                          </tr>";
                                }
                           }                           
                    ?>
                </table>
            </td></tr>
        </table>
    </div>