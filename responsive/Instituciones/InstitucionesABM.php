<?php

require '../net.php';

/**
 * @author Manuel
 * @copyright 2013
 */

if(isset($_POST['Enviar'])){

     /** Inserción de Institución */
    if($_POST['Enviar']=="Agregar"){                
    
        $instituto = $bdd->prepare("insert into Institucion values(NULL, :nombre, :direccion, :telefono, :descripcion, 'Verificado')") ;
        $instituto->bindParam(':nombre',$_POST['Nombre']);
        $instituto->bindParam(':direccion',$_POST['Direccion']);
        $instituto->bindParam(':telefono',$_POST['Telefono']);
        $instituto->bindParam(':descripcion',$_POST['Descripcion']);
        $instituto->execute();
        
        Redireccion("../Instituciones.php?n=1");
    }
    
    /** Eliminación de Institución */
    elseif($_POST['Enviar'] == "Eliminar")
    {                
        if(isset($_POST['IdIns']))
        {
            $idIns = $_POST['IdIns']; 
            $instituto = $bdd->prepare("DELETE FROM Institucion WHERE IdInstitucion = $idIns") ;        
            $instituto->execute();
    
            Redireccion("../Instituciones.php?n=2");
        }
        Redireccion("../Instituciones.php");
    } 
    
    /** Ver Perfil de Institución */
    elseif($_POST['Enviar'] == "Accesar")
    {               
        if(isset($_POST['IdIns']))
        {
            $_SESSION["Ins"] = $_POST['IdIns']; 
            Redireccion("../Instituciones.php?in=Profile");
        }
        Redireccion("../Instituciones.php");
    }
    
     /** Opciones de Perfil de Institución */
    elseif($_POST['Enviar'] == "Actualizar Datos")
    {               
        if(isset($_POST['IdIns']))
        {
            $_SESSION["Ins"] = $_POST['IdIns']; 
            Redireccion("../Instituciones.php?in=InsForm&m=true");
        }
        Redireccion("../Instituciones.php");
    }
    
    /** Opciones de Perfil de Institución */
    elseif($_POST['Enviar'] == "Actualizar Datos")
    {               
        if(isset($_POST['IdIns']))
        {
            $_SESSION["Ins"] = $_POST['IdIns']; 
            Redireccion("../Instituciones.php?in=InsForm&m=true");
        }
        Redireccion("../Instituciones.php");
    }
    
    /** Opciones de Perfil de Institución */
    elseif($_POST['Enviar'] == "Contactos")
    {               
        if(isset($_POST['IdIns']))
        {
            $_SESSION["Ins"] = $_POST['IdIns']; 
            Redireccion("../Instituciones.php?in=Contact");
        }
        Redireccion("../Instituciones.php");
    }
    
    /** Opciones de Perfil de Institución */
    elseif($_POST['Enviar'] == "Grupos")
    {               
        if(isset($_POST['IdIns']))
        {
            $_SESSION["Ins"] = $_POST['IdIns']; 
            Redireccion("../Instituciones.php?in=Group");
        }
        Redireccion("../Instituciones.php");
    }

    /** Opciones de Perfil de Institución */
    elseif($_POST['Enviar'] == "Datos FUSALMO")
    {               
        if(isset($_POST['IdIns']))
        {
            $_SESSION["Ins"] = $_POST['IdIns']; 
            Redireccion("../Instituciones.php?in=Join");
        }
        Redireccion("../Instituciones.php");
    }
            
    /** Opciones de Perfil de Institución */
    elseif($_POST['Enviar'] == "Modificar")
    {               
        $instituto = $bdd->prepare("Update Institucion set 
                                    NombreInstitucion = :nombre,
                                    Direccion = :direccion, 
                                    Telefono = :telefono, 
                                    Descripcion = :descripcion
                                    where IdInstitucion = :id") ;
                                    
        $instituto->bindParam(':nombre',$_POST['Nombre']);
        $instituto->bindParam(':direccion',$_POST['Direccion']);
        $instituto->bindParam(':telefono',$_POST['Telefono']);
        $instituto->bindParam(':descripcion',$_POST['Descripcion']);
        $instituto->bindParam(':id', $_SESSION["Ins"]);
        $instituto->execute();
        
        Redireccion("../Instituciones.php?in=Profile");
    }    

    /** Agregando secciones a Institución */    
    elseif($_POST['Enviar'] == "Agregar Grupo")
    { 
        $seccion = $bdd->prepare("insert into Institucion_Seccion values(null, :idIns, :nombre)");
        $seccion->bindParam(':idIns', $_SESSION["Ins"]);
        $seccion->bindParam(':nombre', $_POST['Nombre']);
        $seccion->execute();        
        
        Redireccion("../Instituciones.php?in=Group");
    }
    
    /** Agregando secciones a Institución */
    elseif($_POST['Enviar'] == "x")
    {        
        $idgroup = $_POST['IdGroup'];
        $seccion = $bdd->prepare("DELETE FROM Institucion_Seccion WHERE IdSeccion = $idgroup");
        $seccion->execute();

        Redireccion("../Instituciones.php?in=Group");        
    }
       
}
Redireccion("../Instituciones.php");
    
?>