<?php

/**
 * @author Manuel
 * @copyright 2013
 */

    if($_SESSION["TipoUsuario"] == "Administrador")
    {
	    if(isset($_GET['n']))
        {
            if($_GET['n'] == 1)
                echo "<div id='Notificacion' name='Notificacion' style='color:green; float: right; width: 300px; height: 50px; padding-bottom:20px;'>Datos Ingresados Exitosamente</div>";                                     
        }
        
        
        $datosinstitucion = $bdd->prepare("SELECT * FROM  Institucion where IdInstitucion = :idIns ");
        $datosinstitucion->bindParam(':idIns', $_SESSION["Ins"]);
        $datosinstitucion->execute();
        
        if($datosinstitucion->rowCount() == 0)
        Redireccion("../Instituciones.php");
        
        $institucion = $datosinstitucion->fetch();
        
        
        if(!isset($_POST['IdDatoFUSALMO']))
            Redireccion("../Instituciones.php?in=Join");
        
        
        $datosFusalmo = $bdd->prepare("Select * from DatoFusalmo where IdDatoFUSALMO = " . $_POST['IdDatoFUSALMO']);
        $datosFusalmo->execute();
                               
        $fusalmo = $datosFusalmo->fetch();

?>
        <div class="Raiz">
            <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
            <a href="/Instituciones.php" class='flecha' style='text-decoration:none'>Instituciones</a> ->
            <a href="/Instituciones.php?in=InsList" class='flecha' style='text-decoration:none'>Lista de Instituciones</a> ->
            <a href="/Instituciones.php?in=Profile" class='flecha' style='text-decoration:none'>Perfil de Institución</a> ->
            <a href="/Instituciones.php?in=Join" class='flecha' style='text-decoration:none'>Relaciones FUSALMO</a> ->
        </div>
        
        <div style='width: 90%; background-color: white; border-radius: 20px; border-color: skyblue; border-style: groove; margin-left: 5%; margin-top: 20px; padding:20px;'>       	
            <form action="Instituciones/JoinABM.php" method="post">
            <input type="hidden" value="<?php echo $_POST['IdDatoFUSALMO'];?>" name="idD" />
            <table style="width: 90%; margin-left: 5%;">
            <tr><td colspan="2" style="color: blue;">Relacion FUSALMO - Instituciones</td></tr>
            <tr><td colspan="2" style="color: blue;"><?php echo $institucion[1];?></td></tr>
            <tr><td colspan="2" style="color: blue;"><?php echo $fusalmo[1];?></td></tr>
            <tr><td colspan="2" style="height: 5px; background-color: skyblue;"></td></tr>
            <tr><td colspan="2" style="height: 20px; background-color: white;"></td></tr> 
            <tr>
                <td style='width: 45%; text-align: right; padding-right: 20px;'>Grupos: </td>
                <td>
                    <table>
                        
                                <?php
	                                   $DatosGrupos = $bdd->prepare("Select * from Institucion_Seccion as sec
                                                                     inner join Institucion as ins on sec.IdInstitucion = ins.IdInstitucion
                                                                     where sec.IdInstitucion = :idI");
                                       $DatosGrupos->bindParam(':idI' , $_SESSION["Ins"] );
                                       $DatosGrupos->execute();
                                       
                                       if($DatosGrupos->rowCount() > 0){
                                           while($Grupos = $DatosGrupos->fetch())
                                           {
                                                $Checkeado = $bdd->prepare("Select * from InscripcionSeccion                                                                              
                                                                            where IdDatoFusalmo = :idD and IdSeccion = :idS");
                                                                                                                            
                                                $Checkeado->bindParam(':idD' , $_POST['IdDatoFUSALMO'] );
                                                $Checkeado->bindParam(':idS' , $Grupos[0] );
                                                $Checkeado->execute();
                                                
                                                if($Checkeado->rowCount() > 0)
                                                    echo "<tr><td>$Grupos[2]</td><td><input type='checkbox' name='Grupo[]' value='$Grupos[0]' checked='true' /></td></tr>";
                                                
                                                else
                                                    echo "<tr><td>$Grupos[2]</td><td><input type='checkbox' name='Grupo[]' value='$Grupos[0]' /></td></tr>";
                                           }
                                       }
                                       else
                                            echo "<tr><td colspan='2' style='color:red'> No hay grupos creados </td></tr>";
                                ?> 
                                                    
                    </table>
                </td>                
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; padding-top: 20px;">
                    <input type="submit" style="width: 150px;" class="Boton" value="Guardar Grupos" name="Enviar" />
                </td>
            </tr>
            </table>
             </form> 
        </div>
<?php    
    }
?>
