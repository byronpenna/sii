<?php

/**
 * @author Manuel
 * @copyright 2013
 */


    if($_SESSION["TipoUsuario"] == "Administrador")
    {
                
        $datosinstitucion = $bdd->prepare("SELECT * FROM  Institucion where IdInstitucion = :idIns ");
        $datosinstitucion->bindParam(':idIns', $_SESSION["Ins"]);
        $datosinstitucion->execute();
        
        if($datosinstitucion->rowCount() == 0)
        Redireccion("../Instituciones.php");
        
        $institucion = $datosinstitucion->fetch();
?>
        <div class="Raiz">
            <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
            <a href="/Instituciones.php" class='flecha' style='text-decoration:none'>Instituciones</a> ->
            <a href="/Instituciones.php?in=InsList" class='flecha' style='text-decoration:none'>Lista de Instituciones</a> ->
            <a href="/Instituciones.php?in=Profile" class='flecha' style='text-decoration:none'>Perfil de Institución</a> ->
            <a href="/Instituciones.php?in=Join" class='flecha' style='text-decoration:none'>Relaciones FUSALMO</a> ->
        </div>
        <div style='width: 90%; background-color: white; border-radius: 20px; border-color: skyblue; border-style: groove; margin-left: 5%; margin-top: 20px; padding:20px;'>       	
            <table style="width: 90%; margin-left: 5%;">
            <tr><td colspan="2" style="color: blue;">Relacion FUSALMO - Instituciones</td></tr>
            <tr><td colspan="2" style="color: blue;"><?php echo $institucion[1];?></td></tr>
            <tr><td colspan="2" style="height: 5px; background-color: skyblue;"></td></tr>
            <tr><td colspan="2" style="height: 20px; background-color: white;"></td></tr> 
            <?php
            if(isset($_GET['n']))
            {
                if($_GET['n'] == 1)
                    echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right;'>Datos Guardados Exitosamente</div>";                                     
            }
?>
            <form action="Instituciones.php?in=JoinGroup" method="post">
            <tr>
                <td style='width: 45%; text-align: right; padding-right: 20px;'>Seleccione del Dato FUSALMO</td>
                <td>
                    <select name="IdDatoFUSALMO">                    
                        <?php
	                           $datosFusalmo = $bdd->prepare("Select * from DatoFusalmo order by Nombre ASC ");
                               $datosFusalmo->execute();
                               
                               while($fusalmo = $datosFusalmo->fetch())
                                    echo "<option  value='$fusalmo[0]' >$fusalmo[1]</option>"; 
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; padding-top: 20px;">
                    <input type="submit" style="width: 150px;" class="Boton" value="Verificar Grupos" name="Enviar" />                    
                </td>
            </tr>
            </form>
            </table>
        </div>
<?php    
    }
?>
