<?php

require '../net.php';

/**
 * @author Manuel
 * @copyright 2013
 */

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Agregar Contacto")
    {
        $contacto = $bdd->prepare("insert into Institucion_Contactos values(NULL, :idI, :Nombre, :cargo, :telefono)") ;
        $contacto->bindParam(':idI', $_SESSION["Ins"]);
        $contacto->bindParam(':Nombre',$_POST['Nombre']);
        $contacto->bindParam(':cargo',$_POST['profesion']);
        $contacto->bindParam(':telefono',$_POST['telefono']);
        $contacto->execute();
        
        Redireccion("../Instituciones.php?in=Contact&n=1");
    }
    
    elseif($_POST['Enviar'] == "Eliminar")
    {                
        if(isset($_POST['IdCon']))
        {
            $IdCon = $_POST['IdCon']; 
            $instituto = $bdd->prepare("DELETE FROM Institucion_Contactos WHERE IdContacto = $IdCon") ;        
            $instituto->execute();
    
            Redireccion("../Instituciones.php?in=Contact&n=2");
        }
        Redireccion("../Instituciones.php");
    } 
}    
        


?>