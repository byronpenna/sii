 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml2/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html" charset="ISO-8859-1" />
<title>Menu</title>
<style type="assets/css/style.css">

/*Credits: CSSpplay */
/*URL: http://www.cssplay.co.uk/menus/pro_six */

#pro6 ul {margin:0 auto; padding:0; list-style:none; display:table; white-space:nowrap; list-style:none; height:35px; position:relative; background:#fff; font-size:11px;}
#pro6 li {display:table-cell; margin:0; padding:0;}
#pro6 li a {display:block; float:left; height:35px; line-height:30px; color:#333; text-decoration:none; font-family:arial, verdana, sans-serif; font-weight:bold; text-align:center; padding:0 0 0 10px; cursor:pointer; background:url(menu/pro_six_0a.gif) no-repeat;}
#pro6 li a b {float:left; display:block; padding:0 25px 5px 15px; background:url(menu/pro_six_0b.gif) no-repeat right top;}
#pro6 li.current a {color:#fff; background:url(menu/pro_six_2a.gif) no-repeat;}
#pro6 li.current a b {background:url(menu/pro_six_2b.gif) no-repeat right top;}
#pro6 li a:hover {color:#fff; background: url(menu/pro_six_1a.gif) no-repeat;}
#pro6 li a:hover b {background:url(menu/pro_six_1b.gif) no-repeat right top;}
#pro6 li.current a:hover {color:#fff; background: url(menu/pro_six_2a.gif) no-repeat; cursor:default;}
#pro6 li.current a:hover b {background:url(menu/pro_six_2b.gif) no-repeat right top;}

</style>

<!--[if IE]>
<style type="text/css">
#pro6 ul {display:inline-block;}
#pro6 ul {display:inline;}
#pro6 ul li {float:left;}
#pro6 {text-align:center;}
</style>
<![endif]-->

</head>

<body>
<?php if($_SESSION["TipoUsuario"] == "Administrador"){?>
<div id="pro6">
<ul style="background-color: transparent;">
<li style="font-family: Comic Sans MS;"><a href="Main.php"><b>Inicio</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Main.php?l=jovenl"><b>J�venes</b></a></li>
<?php

    if($_SESSION["IdUsuario"] != 2580)
    {
        echo "<li style='font-family: Comic Sans MS;'><a href='Main.php?l=UsuL'><b>Usuarios</b></a></li>
              <li style=''font-family: Comic Sans MS;'><a href='Instituciones.php'><b>Instituciones</b></a></li>
              <li style='font-family: Comic Sans MS;'><a href='Empleado.php'><b>Empleado</b></a></li>";    
    }                      	
?> 
<li style='font-family: Comic Sans MS;'><a href='monitoreo.php'><b>Mis Encuestas</b></a></li> 
<li style="font-family: Comic Sans MS;"><a href="FUSALMO.php"><b>FUSALMO</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Inventario.php"><b>Inventario</b></a></li>
</ul>
</div>

<?php }
if($_SESSION["TipoUsuario"] == "Joven"){?>
<div id="pro6">
<ul style="background-color: transparent;">
<li style="font-family: Comic Sans MS;"><a href="Main.php"><b>Inicio</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Main.php?l=joven"><b>Mis Datos</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="FUSALMO.php"><b>FUSALMO</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="monitoreo.php?"><b>Mis Encuestas</b></a></li>
</ul>
</div>

<?php }
if($_SESSION["TipoUsuario"] == "Consultor"){?>
<div id="pro6">
<ul style="background-color: transparent;">
<li style="font-family: Comic Sans MS;"><a href="Main.php"><b>Inicio</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Empleado.php"><b>Empleado</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="monitoreo.php?"><b>Mis Encuestas</b></a></li>
</ul>
</div>
<?php }
if($_SESSION["TipoUsuario"] == "RRHH"){?>
<div id="pro6">
<ul style="background-color: transparent;">
<li style="font-family: Comic Sans MS;"><a href="Main.php"><b>Inicio</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Empleado.php"><b>Empleado</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="monitoreo.php"><b>Mis Encuestas</b></a></li>
</ul>
</div>
<?php }
if($_SESSION["TipoUsuario"] == "Coordinador"){?>
<div id="pro6">
<ul style="background-color: transparent;">
<li style="font-family: Comic Sans MS;"><a href="Main.php"><b>Inicio</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Empleado.php"><b>Empleado</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="FUSALMO.php"><b>FUSALMO</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Main.php?l=jovenl"><b>J�venes</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="monitoreo.php?"><b>Mis Encuestas</b></a></li>
</ul>
</div>
<?php }
if($_SESSION["TipoUsuario"] == "Gestor"){ ?>
<div id="pro6">
<ul style="background-color: transparent;">
<li style="font-family: Comic Sans MS;"><a href="Main.php"><b>Inicio</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Empleado.php"><b>Empleado</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="FUSALMO.php"><b>FUSALMO</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="monitoreo.php"><b>Mis Encuestas</b></a></li>
</ul>
</div>
<?php }
if($_SESSION["TipoUsuario"] == "Buscador"){ ?>
<div id="pro6">
<ul style="background-color: transparent;">
<li style="font-family: Comic Sans MS;"><a href="Main.php"><b>Inicio</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Main.php?l=UsuL"><b>Usuarios</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Main.php?l=jovenl"><b>J�venes</b></a></li>
</ul>
</div>
<?php }
if($_SESSION["TipoUsuario"] == "USAID-CI" || $_SESSION["TipoUsuario"] == "USAID-SI" || $_SESSION["TipoUsuario"] == "USAID-SE"){ ?>
<div id="pro6">
<ul style="background-color: transparent;">
<li style="font-family: Comic Sans MS;"><a href="Main.php"><b>Inicio</b></a></li>
<li style="font-family: Comic Sans MS;"><a href="Inventario.php"><b>Inventario</b></a></li>
</ul>
</div>
<?php }?>

</body>
</html>