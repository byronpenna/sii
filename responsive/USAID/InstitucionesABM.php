<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

include ('../net.php');

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Guardar")
    {
        $Insert = $bddC->prepare("Insert into USAID_Instituciones values(Null, :Cod, :year, :name, :muni, :dir, :celE, :director, :celD, :subdirector, :celS )");
        $Insert->bindParam('Cod', $_POST['codigo']);
        $Insert->bindParam('year', $_POST['year']);
        $Insert->bindParam('name', $_POST['Nombre']);
        $Insert->bindParam('muni', $_POST['Municipio']);
        $Insert->bindParam('dir', $_POST['Direccion']);
        $Insert->bindParam('celE', $_POST['TelefonoEmpresa']);
        $Insert->bindParam('director', $_POST['Director']);
        $Insert->bindParam('celD', $_POST['TelefonoDirector']);
        $Insert->bindParam('subdirector', $_POST['Subdirector']);
        $Insert->bindParam('celS', $_POST['TelefonoSubdirector']);
        $Insert->execute();
        
        Redireccion("../USAID.php?u=Institutions&n=1");
    }
    else if($_POST['Enviar'] == "Actualizar")
    {
        $Update = $bddC->prepare("Update USAID_Instituciones Set 
                                  Codigo = :Cod, 
                                  A�o = :year, 
                                  Nombre = :name, 
                                  Municipio = :muni, 
                                  Direccion = :dir, 
                                  Telefono = :celE, 
                                  Director = :director, 
                                  CelularD = :celD, 
                                  SubDirector = :subdirector, 
                                  CelularSubD = :celS  
                                  Where IdInstitucion = " . $_POST['IdI']);
                                  
        $Update->bindParam('Cod', $_POST['codigo']);
        $Update->bindParam('year', $_POST['year']);
        $Update->bindParam('name', $_POST['Nombre']);
        $Update->bindParam('muni', $_POST['Municipio']);
        $Update->bindParam('dir', $_POST['Direccion']);
        $Update->bindParam('celE', $_POST['TelefonoEmpresa']);
        $Update->bindParam('director', $_POST['Director']);
        $Update->bindParam('celD', $_POST['TelefonoDirector']);
        $Update->bindParam('subdirector', $_POST['Subdirector']);
        $Update->bindParam('celS', $_POST['TelefonoSubdirector']);        
        $Update->execute();
        
        Redireccion("../USAID.php?u=Institutions&n=2");
    }   
    else if($_POST['Enviar'] == "Eliminar")
    {
        $Delete = $bddC->prepare("Delete from USAID_Instituciones Where IdInstitucion = " . $_POST['IdI']);
        $Delete->execute();
        
        Redireccion("../USAID.php?u=Institutions&n=3");
    }
}

?>
