<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 
require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Guardar")
    {
        $insert = $bddC->prepare("Insert into USAID_Consultor values(Null,:IdDF, :Nombre, :TelCasa, :TelCel, :Direccion, :Email)");
        $insert->bindParam(':IdDF', $_SESSION['IdDato']);
        $insert->bindParam(':Nombre', $_POST['Nombre']);
        $insert->bindParam(':TelCasa', $_POST['Telefono']);
        $insert->bindParam(':TelCel', $_POST['Celular']);
        $insert->bindParam(':Direccion', $_POST['Direcci�n']);
        $insert->bindParam(':Email', $_POST['correo']);        
        $insert->execute();
        
        Redireccion("../USAID.php?u=ConsultantList&n=1");

    }
    if($_POST['Enviar'] == "X")
    {
        $delete = $bddC->prepare("Delete from USAID_Consultor where IdConsultor = :IdC");
        $delete->bindParam(':IdC', $_POST['IdC']);        
        $delete->execute();
        
        Redireccion("../USAID.php?u=ConsultantList&n=3");        
    }
    
    if($_POST['Enviar'] == "A")
    {
        $_SESSION['IdCon'] = $_POST['IdC']; 
        Redireccion("../USAID.php?u=ConsultantForm");        
    }    
    
    if($_POST['Enviar'] == "Actualizar")
    {
        $update = $bddC->prepare("Update USAID_Consultor set 
                                    Consultor = :Nombre, 
                                    Telefono = :TelCasa, 
                                    TelCel = :TelCel,
                                    Direccion = :Direccion,
                                    Correo = :Email
                                    where IdConsultor = :IdCon");
                                    
        $update->bindParam(':Nombre', $_POST['Nombre']);
        $update->bindParam(':TelCasa', $_POST['Telefono']);
        $update->bindParam(':TelCel', $_POST['Celular']);
        $update->bindParam(':Direccion', $_POST['Direcci�n']);
        $update->bindParam(':Email', $_POST['correo']);        
        $update->bindParam(':IdCon', $_POST['IdCon']);
        $update->execute();
                
        Redireccion("../USAID.php?u=ConsultantList&n=2");        
    }        
}

?>