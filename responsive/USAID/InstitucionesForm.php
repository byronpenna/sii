<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

if(isset($_POST['IdI']))
{
    $Institucion = $bddC->prepare("Select * from USAID_Instituciones where IdInstitucion = " . $_POST['IdI']);
    $Institucion->execute();
    
    $DataI = $Institucion->fetch();
    $ABM = "Actualizar";
}
else
{
    $ABM = "Guardar";
}
?>
<script>
function CargarListaMunicipios()
{
    var departamento = document.getElementById('Departamentos').value;    
    document.getElementById('Municipio').options.length = 0;
    
    var aux = true;
    
    $.get("USAID/SelectMunicipios.php", { departamento:departamento, aux:aux },
  		function(resultado)
  		{           
            document.getElementById('Municipio').options.length = 0;
                
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#Municipio').append(resultado);	            			
  		}
   	);        
}
</script> 
<div style="width: 75%; margin-left: 22%; border-radius: 10px; background-color: white; padding-bottom: 30px; margin-bottom: 30px;">
    <table style="width: 90%; margin-left: 5%;">
        <form action="USAID/InstitucionesABM.php" method="Post">
        <?php
	            if(isset($_POST['IdI']))              
                    echo "<input type='hidden' name='IdI' value='".$_POST['IdI']."' />";
        ?>
        <tr><td colspan="2"><h2>Formulario de Instituciones</h2></td></tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr><td class="tdleft" style="width: 35%;">Nombre de la Instituci�n:</td><td><input style="width: 200px;" type="text" name="Nombre" value="<?php echo  $DataI[3]?>" required="" /></td></tr>
        <tr><td class="tdleft">C�digo de Identificaci�n:</td><td><input style="width: 200px;" type="text" name="codigo" value="<?php echo $DataI[1]?>" /></td></tr>
        <tr>
            <td class="tdleft">A�o:</td>
            <td>
                <select name="year" style="width: 200px;">
                    <?php
                            
                                if(isset($_POST['IdI']))
                                {
                                    for($i = 2013; $i <= 2018 ; $i++)
                                    {   
                                        if($i == $DataI[2])
                                            echo "<option value='$i' selected='true'>$i</option>";
                                        
                                        else
                                            echo "<option value='$i'>$i</option>";
                                    }                               
                                }
                                else
                                {
                                    for($i = 2013; $i <= 2018 ; $i++)
                                    {   
                                        if(date("Y") == $i)
                                            echo "<option value='$i' selected='true'>$i</option>";
                                        
                                        else
                                            echo "<option value='$i'>$i</option>";
                                    }
                                }
                    ?>        
                </select>
            </td>
        </tr>
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr><td class="tdleft">Departamento:</td>
            <td>
                <select name='Departamentos' id='Departamentos' style='width: 200px;' onchange='CargarListaMunicipios()' >
                    <option value="...">...</option>
                        <?php
                                if(isset($_POST['IdI']))
                                {
                                    $auxDepartamento = $bddC->prepare("Select d.* from USAID_Departamentos as d
                                                                       inner join USAID_Municipios as m on d.IdDepartamento = m.Departamento
                                                                       where m.Municipio = '$DataI[4]'");
                                    $auxDepartamento->execute();
                                    $AuxDataD = $auxDepartamento->fetch();
                                }
                                
                                $Departamentos = $bddC->prepare("Select * from USAID_Departamentos ");
                                $Departamentos->execute();
    
                            	while($DataD = $Departamentos->fetch()) 
                                {
                                    if($AuxDataD[0] == $DataD[0])
                                       echo "<option value='$DataD[0]' selected='true'>$DataD[1]</option>";    
                                    
                                    else
                                        echo "<option value='$DataD[0]'>$DataD[1]</option>";
                                }    
                                        
                        ?>
                 </select>
            </td>
        </tr>               
        <tr>
            <td class="tdleft">Municipio:</td>
            <td>    
                <select name='Municipio' id='Municipio' style='width: 200px;'>";
                    <option value="...">...</option>
                    <?php
                            if(isset($_POST['IdI']))
                            {
                                $auxMunicipio = $bddC->prepare("Select m.* from USAID_Departamentos as d
                                                                inner join USAID_Municipios as m on d.IdDepartamento = m.Departamento
                                                                WHERE d.IdDepartamento =  $AuxDataD[0]");
                                $auxMunicipio->execute();
                                                               
                            	while($AuxDataM = $auxMunicipio->fetch()) 
                                {
                                    if($DataI[4] == $AuxDataM[2])
                                       echo "<option value='$AuxDataM[2]' selected='true'>$AuxDataM[2]</option>";    
                                    
                                    else
                                        echo "<option value='$AuxDataM[2]'>$AuxDataM[2]</option>";
                                } 
                            }
            
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="tdleft">Direcci�n de Instituci�n:</td>
            <td><textarea name="Direccion"><?php echo $DataI[5]?></textarea></td>
        </tr>
        <tr>
            <td class="tdleft">Telefono de Instituci�n:</td>
            <td><input type="text" style="width: 200px;" name="TelefonoEmpresa" placeholder="0000-0000"  value="<?php echo $DataI[6]?>"/></td>
        </tr>        
        <tr><td colspan="2"><hr color='skyblue' /></td></tr>
        <tr>
            <td class="tdleft">Director:</td>
            <td><input style="width: 200px;" type="text" name="Director" value="<?php echo $DataI[7]?>" required="" /></td>
        </tr>
        <tr>
            <td class="tdleft">Tel�fono Director:</td>
            <td><input type="text" style="width: 200px;" name="TelefonoDirector" placeholder="0000-0000"  value="<?php echo $DataI[8]?>" /></td>
        </tr>
                <tr>
            <td class="tdleft">Sub-director:</td>
            <td><input style="width: 200px;" type="text" name="Subdirector" value="<?php echo $DataI[9]?>" /></td>
        </tr>
        <tr>
            <td class="tdleft">Tel�fono Sub-director:</td>
            <td><input type="text" style="width: 200px;" name="TelefonoSubdirector" placeholder="0000-0000"  value="<?php echo $DataI[10]?>" /></td>
        </tr>
        <tr><td colspan="2"><h2><input style="margin-left: 30%;" type="submit" value="<?php echo $ABM?>" name="Enviar" class="boton" /></h2></td></tr>
        </form>                 
    </table>    
</div>