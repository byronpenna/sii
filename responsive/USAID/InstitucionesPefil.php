<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

$IdInstitucion = $_POST['IdI'];
$Institucion = $bddC->prepare("Select * from USAID_Instituciones where IdInstitucion = $IdInstitucion");
$Institucion->execute();
$DataI = $Institucion->fetch();

$Departamento = $bddC->prepare("SELECT d.Departamento FROM USAID_Departamentos AS d
                                INNER JOIN USAID_Municipios AS m ON d.IdDepartamento = m.Departamento
                                INNER JOIN USAID_Instituciones as i ON m.Municipio = i.Municipio 
                                WHERE i.IdInstitucion = $IdInstitucion");
$Departamento->execute();
$DataD = $Departamento->fetch();

?>
<div style="width: 75%; margin-left: 22%; border-radius: 10px; background-color: white; padding-bottom: 30px; margin-bottom: 30px;">
    <table style="width: 90%; margin-left: 5%;">
        <tr><td colspan="2"><h2>Perfil de Instituci�n</h2></td>
            <td style="text-align: right;">
                <form action="?u=InstitutionsForm" method="post">
                    <input type="hidden" name="IdI" value="<?php echo $DataI[0]?>" />
                    <input type="submit" name="Enviar" value="Editar" class="boton" />
                </form>
            </td>
        </tr>
        <tr><td colspan="3"><hr color='skyblue' /></td></tr>
        <tr><td class="tdleft" style="width: 40%;">C�digo de la Institucion:</td> <td colspan="2"><?php echo $DataI[1]?></td></tr>
        <tr><td class="tdleft" style="width: 40%;">Nombre de la Institucion:</td> <td colspan="2"><?php echo $DataI[3]?></td></tr>
        <tr><td class="tdleft">Municipio:</td><td colspan="2"><?php echo $DataI[4]?></td></tr>
        <tr><td class="tdleft">Departamento:</td><td colspan="2"><?php echo $DataD[0]?></td></tr>
        <tr><td class="tdleft">Direcci�n:</td> <td colspan="2"><?php echo $DataI[5]?></td></tr>
        <tr><td class="tdleft">Telefono de la Instituci�n:</td> <td colspan="2"><?php echo $DataI[6]?></td></tr>
        
        <tr><td colspan="3" style="padding-top: 10px;"><h3>Contactos</h3></td>
        <tr><td class="tdleft">Director:</td> <td colspan="2"><?php echo $DataI[7]?></td></tr>
        <tr><td class="tdleft">Telefono del Director:</td> <td colspan="2"><?php echo $DataI[8]?></td></tr>
        <tr><td class="tdleft">Sub-Director:</td> <td colspan="2"><?php echo $DataI[9]?></td></tr>
        <tr><td class="tdleft">Telefono del Sub-Director:</td> <td colspan="2"><?php echo $DataI[10]?></td></tr>
        
        <tr><td colspan="3" style="padding-top: 10px;"><h3>Opciones</h3></td>
        <tr>
            <td class="tdleft" colspan="3">                
                <table style="width: 100%;">
                    <tr>
                    <form action="USAID/InstitucionesABM.php" method="POST">
                        <input type="hidden" value="<?php echo $DataI[0]?>" name="IdI" />
                        <td style="text-align: left;"><input type="submit" class="botonE" name="Enviar" onclick="return confirm('Desea eliminar realmente la instituci�n')" value="Eliminar" /></td>
                        <td style="text-align: right;"></td>
                    </form>
                    </tr>
                </table>
            </td>
        </tr>                
    </table>
</div>