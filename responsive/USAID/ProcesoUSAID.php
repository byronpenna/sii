<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

if(isset($_GET['n']))
{
	if($_GET['n'] == 1)
		echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right'>Datos Ingresados Exitosamente!</div>";
		
	if($_GET['n'] == 2)
		echo "<div id='Notificacion' name='Notificacion' style='color:blue; text-align: right'>Datos Actualizados Exitosamente!</div>";
        
	if($_GET['n'] == 3)
		echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right'>Datos Eliminados Exitosamente...</div>";
        
	if($_GET['n'] == 4)
		echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right'>Acceso Denegado...</div>"; 

	if($_GET['n'] == 5)
		echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right'>Error a sucesido un problema, comuniquese con los administradores del sistema...</div>";                                   
}
            
echo "<div style='float:left; width: 20%;'>";
include ('Menu/menu.php');
echo "</div>";

if(isset($_GET['u']))
{
    /** Componente **/
    if($_GET['u'] == "Component")
        include("USAID/ComponenteList.php");
    
    /** Consultor **/    
    else if($_GET['u'] == "ConsultantForm")
        include("USAID/ConsultorForm.php");
        
    else if($_GET['u'] == "ConsultantList")
        include("USAID/ConsultorList.php");
        
    /** Componente **/        
    else if($_GET['u'] == "Institutions")
        include("USAID/InstitucionesList.php");
    
    else if($_GET['u'] == "InstitutionsProfile")
        include("USAID/InstitucionesPefil.php");
        
    else if($_GET['u'] == "InstitutionsForm")
        include("USAID/InstitucionesForm.php");        
        
        
    /** Eventos **/
    else if($_GET['u'] == "Events")
        include("USAID/Eventos.php");  
        
    else if($_GET['u'] == "EventsForm")
        include("USAID/EventosForm.php");   
        
    else if($_GET['u'] == "EventProfile")
        include("USAID/EventosPerfil.php");
        
    else if($_GET['u'] == "TargetPopulation")
        include("USAID/PoblacionMetaForm.php");  

    else if($_GET['u'] == "AssignmentConsultant")
        include("USAID/EventosAsignacionConsultor.php");
        
    else if($_GET['u'] == "days")
        include("USAID/JornadaForm.php");
        
    else if($_GET['u'] == "process")
        include("USAID/EventosABM.php");            
        
        
        
    else
        Redireccion("../USAID.php");
}
else
    include("USAID/Main.php");
    
echo "<div class='clr'></div>";
?>