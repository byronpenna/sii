<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

?>
<script>
function CargarListaMunicipios()
{
    var departamento = document.getElementById('Departamentos').value;    
    document.getElementById('Municipio').options.length = 0;
    
    var aux = true;
    
    $.get("USAID/SelectMunicipios.php", { departamento:departamento, aux:aux },
  		function(resultado)
  		{           
            document.getElementById('Municipio').options.length = 0;
                
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#Municipio').append(resultado);	            			
  		}
   	);        
}
</script> 
<div style="width: 75%; margin-left: 22%; border-radius: 10px; background-color: white; padding-bottom: 30px; margin-bottom: 30px;">
<?php
    $Departamentos = $bddC->prepare("Select * from USAID_Departamentos ");
    $Departamentos->execute();
    
  
    
    if(isset($_POST['Busqueda']))
        $busqueda = $_POST['Busqueda'];
    else
        $busqueda = "";
    
    echo "<table style='width: 100%;'>
            <tr><td><h2 style='padding-left:50px;'>Instituciones</h2> </td>
                <td style='text-align: right;'><a href='?u=InstitutionsForm'><input type='button' style='margin-right: 5%;' value='Agregar' class='boton' /></a></td></tr>
            <tr><td colspan='2'><hr color='skyblue' /></td></tr>
            <tr><td colspan='2'>
                    <center>
                    <table style='width: 100%;'>
                        <form action='#' method='post'>
                        <tr>
                            <td>Buscador de <br /> Centro Escolar: </td>
                            <td colspan='3'><input type='text' name='Busqueda' value='$busqueda' style='width: 300px; margin-left: 5px;' /></td> 
                        </tr>
                        <tr>
                            <td>Departamento:</td>
                            <td>
                                <select name='Departamentos' id='Departamentos' style='width: 150px; margin-left: 5px;' onchange='CargarListaMunicipios()' >
                                    <option value='Todos'>Todos</option>";
                                
                                    while($DataD = $Departamentos->fetch()) 
                                    {
                                        if($_POST['Departamentos'] == $DataD[0])
                                                echo "<option value='$DataD[0]' selected='true'>$DataD[1]</option>";    
                                    
                                        else
                                            echo "<option value='$DataD[0]'>$DataD[1]</option>";
                                    }    
                                        
    echo "                      </select>
                            </td>
                            <td style='padding-left:30px;'>Municipio</td>
                            <td>
                                <select name='Municipio' id='Municipio' style='width: 150px; margin-left: 5px;'>";
                                $a = false;
                                
                                if(isset($_POST['Departamentos']))
                                {
                                    $Municipios = $bddC->prepare("SELECT * FROM  USAID_Municipios where Departamento = " . $_POST['Departamentos']);
                                    $Municipios->execute();  
    
                                    while($DataM = $Municipios->fetch()) 
                                    {
                                        if($_POST['Municipio'] == $DataM[0])
                                        {
                                                echo "<option value='$DataM[0]' selected='true'>$DataM[2]</option>";
                                                $a = true;    
                                        }
                                        else
                                            echo "<option value='$DataM[0]'>$DataM[2]</option>";
                                    }
                                }
                 
                                if($a)    
    echo "                          <option value='Todos'>Todos</option>";
    
                                else
    echo "                          <option value='Todos' selected='true'>Todos</option>";

    echo "                      </select>
                            </td>
                            <td style='padding-left:30px;'>
                                <input type='submit' name='Enviar' class='boton' value='Buscar' /> 
                            </td>                            
                        </tr>
                        </form>    
                    </table>     
                    </center>
                </td>
            </tr>
            
            <tr>
                <td colspan='2'>
                    <div>";
    
        if (isset($_POST['Departamentos']) && isset($_POST['Municipio']))
        {
            $busqueda = $_POST['Busqueda'];
            $departamento = $_POST['Departamentos'];
            $municipio = $_POST['Municipio'];
            
            if($_POST['Departamentos'] == "Todos")
            {            
                $Query = "SELECT i.A�o, i.Nombre, i.Municipio, d.Departamento, i.IdInstitucion FROM USAID_Instituciones AS i
                          INNER JOIN USAID_Municipios AS m ON i.Municipio = m.Municipio
                          INNER JOIN USAID_Departamentos AS d ON m.Departamento = d.IdDepartamento
                          WHERE Nombre LIKE '%$busqueda%'";    
            }
            else
            {
                if($_POST['Municipio'] == "Todos")
                {            
                    $Query = "Select i.A�o, i.Nombre, i.Municipio, d.Departamento, i.IdInstitucion from USAID_Instituciones as i 
                              Inner Join USAID_Municipios as m on i.Municipio = m.Municipio
                              INNER JOIN USAID_Departamentos AS d ON m.Departamento = d.IdDepartamento
                              where d.IdDepartamento = $departamento and Nombre like '%$busqueda%'";    
                }
                else
                {
                    $Query = "Select i.A�o, i.Nombre, i.Municipio, d.Departamento, i.IdInstitucion from USAID_Instituciones as i 
                              Inner Join USAID_Municipios as m on i.Municipio = m.Municipio
                              INNER JOIN USAID_Departamentos AS d ON m.Departamento = d.IdDepartamento
                              where d.IdDepartamento = $departamento AND m.IdMunicipio = $municipio AND Nombre like '%$busqueda%'";
                }
            }
            
 
            
            $TotalBusqueda = $bddC->prepare($Query);
            $TotalBusqueda->execute();

            
            $DataCentrosEscolares = $bddC->prepare($Query);
            $DataCentrosEscolares->execute();
            
            echo "<table style='width: 100%;'>
                    <tr><th colspan='4'> Total de Coincidencias encontradas: " . $TotalBusqueda->rowCount() ." <br /></th></tr>
                    <tr><td colspan='4'><hr color='skyblue' /></td></tr>
                    <tr><th style='width: 60%; '>Centro Escolar</th><th style='width: 15%;'>Municipio</th><th style='width: 10%;'>Departamento</th><th></th></tr>";
            
            if($DataCentrosEscolares->rowCount() > 0)
            {
                while($DataC = $DataCentrosEscolares->fetch())
                {
                    echo "<form action='USAID.php?u=InstitutionsProfile' method='POST'>
                            <input type='hidden' name='IdI' value='$DataC[4]'  />
                            <tr><td>$DataC[1]</td><td>$DataC[2]</td><td>$DataC[3]</td><td><input type='submit' style='width: 50px' name='Enviar' value='Ver' class='boton' /></td></tr>
                          </form>";
                }
            }
            else
                echo "<tr><td style='text-align: center;' colspan='3'><br /><h3 style='color: red;'>Error, no hemos encontrado coincidencias</h3></td></tr>";
            
            echo "</table>";
        }
        else
            echo "<br /><br /><br /><center> <em style='color: blue;'>Esperando Datos...</em> </center><br /><br /><br />";
    
    echo "      
                  
                </div>
                </td>
            </tr>
        </table>";
?>

</div>