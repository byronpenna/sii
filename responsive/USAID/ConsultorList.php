<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

?>
<div style="width: 75%; margin-left: 22%; border-radius: 10px; background-color: white; padding-bottom: 30px; margin-bottom: 30px;">
<?php
	
    $GetDatos = $bddC->prepare('SELECT * FROM USAID_Consultor where IdDatoFUSALMO = ' . $_SESSION['IdDato']);	
    $GetDatos->execute();
    
    $GetDatoFUSALMO = $bdd->prepare('SELECT * FROM DatoFusalmo where IdDatoFUSALMO = ' . $_SESSION['IdDato']);	
    $GetDatoFUSALMO->execute();
    
    $DatoFUSALMO = $GetDatoFUSALMO->fetch();  

    echo "<table style='width: 100%;'>
            <tr>
                <td><h2 align='center'>Listado de Consultores<br /> $DatoFUSALMO[1] </h2></td>
                <td style='text-align: right;'><a href='?u=ConsultantForm'><input type='button' value='Agregar' class='boton'/></a></td>
            </tr>
            <tr><td colspan='2'><hr color='0099FF'></td></tr>
          </table>
            ";
    
	if ($GetDatos->rowCount()>0)
    {
        echo "<table style='width: 96%; margin-left: 2%;'>
              <tr>
                  <td style='width: 40%; text-align: center;'>Consultor</td>
                  <td style='width: 15%; text-align: center;'>Celular</td>
                  <td style='width: 15%; text-align: center;'>Correo Electronico</td>
                  <td style='width: 30%; text-align: center;'>Acciones</td>
              <tr>";
        while($DatosConsultores = $GetDatos->fetch())   
        {
        echo "<tr><td>$DatosConsultores[2]</td>
                  <td>$DatosConsultores[4]</td>
                  <td>$DatosConsultores[6]</td>
                  <td style='text-align: center;'>
                      <form action='USAID/ConsultorABM.php' method='post'>
                        <input type='hidden' name='IdC' value='$DatosConsultores[0]'  />
                        <input type='submit' style='width: 30px' value='X' class='botonE' name='Enviar' onclick=\"return confirm('Desea borrar realmente este registro?')\" />
                        <input type='submit' style='width: 30px' value='A' class='boton' name='Enviar' />
                      </form> 
                  </td>
              </tr>";
        }
    }
    else
    {
        echo "<div style='text-align: center;'>
                <em>
                    <a href='USAID.php?u=ConsultantForm' style='color: red; text-decoration: none;'>No hay Consultores Inscritos<br /> haz clic aqui para agregar uno nuevo</a>
                </em>
                <br /><br /><br />
              </div>";
    }
          
    echo "</table>";
?>

</div>