<?php

/**
 * @author Gilbetor
 * @copyright 2014
 */

$categoria = $_GET['code'];
$atleta = $_GET['code2'];
$edad = $_GET['edad'];

echo "<table style='width: 100%;'>";

if($categoria == "Familiar")
{
?>

    <tr><td colspan="6">
            <table style="2" style="width: 80%;">
                <tr><td colspan="2" style="text-align: center;"><strong style="font-size: large;">Categoria Familiar</strong></td></tr>
                <tr><td class="tdleft" style="width: 40%;">Costo:</td><td>15 Dolares</td></tr>
                <tr><td class="tdleft" style="width: 40%;">Distancia:</td><td>3 KM</td></tr>
            </table>
        </td>
    </tr>
    <tr><td>1 - Lider:</td><td><?php echo $atleta;?></td>
        <td>Edad:</td><td><?php echo $edad;?></td>
        <td>Talla de Camisa:</td>
        <td>
            <select name="Talla">
                <option>12</option>
                <option>14</option>
                <option>16</option>
                <option>S</option>
                <option>M</option>
                <option>X</option>
                <option>XL</option>
            </select>
        </td>        
    </tr>
    <tr> 
        <td>2 - Nombre:</td>
        <td><input type="text" name="Nombre1" required="true"  /></td>
        <td>Edad:</td>
        <td><input type="text" name="Edad1" style="width: 50px;" required="true"  /></td>
        <td>Talla de Camisa:</td>
        <td>
            <select name="Talla1">
                <option>12</option>
                <option>14</option>
                <option>16</option>
                <option>S</option>
                <option>M</option>
                <option>X</option>
                <option>XL</option>
            </select>
        </td>        
    </tr>
    <tr>
        <td>3 - Nombre:</td>
        <td><input type="text" name="Nombre2"  required="true" /></td>
        <td>Edad:</td>
        <td><input type="text" name="Edad2" style="width: 50px;" required="true" /></td>
        <td>Talla de Camisa:</td>
        <td>
            <select name="Talla2">
                <option>12</option>
                <option>14</option>
                <option>16</option>
                <option>S</option>
                <option>M</option>
                <option>X</option>
                <option>XL</option>
            </select>
        </td>  
    </tr>
    <tr>
        <td>4 - Nombre:</td>
        <td><input type="text" name="Nombre3"  required="true" /></td>
        <td>Edad:</td>
        <td><input type="text" name="Edad3" style="width: 50px;" required="true"  /></td>
        <td>Talla de Camisa:</td>
        <td>
            <select name="Talla3">
                <option>12</option>
                <option>14</option>
                <option>16</option>
                <option>S</option>
                <option>M</option>
                <option>X</option>
                <option>XL</option>
            </select>
        </td>  
    </tr>

<?php
}
else
{
    if($categoria == "Militar" || $categoria == "Libre 10 Km")
        $distancia = "10 Km";
        
    if($categoria == "Juvenil" || $categoria == "Libre 5 Km")
        $distancia = "5 Km";        

    $costo = "10 Dolares";

        
?>
    <tr><td colspan="6">
            <table style="width: 400px; margin-left: 65px;">
                <tr><td colspan="2" style="text-align: center;"><strong style="font-size: large;">Categoria <?php echo $categoria?> </strong></td></tr>
                <tr><td class="tdleft" style="width: 4%;">Costo:</td><td><?php echo $costo?></td></tr>
                <tr><td class="tdleft" style="width: 40%;">Distancia:</td><td><?php echo $distancia?></td></tr>
                <tr>
                    <td class="tdleft">Talla de Camisa:</td>
                    <td>
                        <select name="Talla">
                            <option>12</option>
                            <option>14</option>
                            <option>16</option>
                            <option>S</option>
                            <option>M</option>
                            <option>X</option>
                            <option>XL</option>
                        </select>
                    </td> 
                </tr>
            </table>
        </td>
    </tr>
<?php
}
?>
<tr><td colspan="6" style="text-align: center;"><input type="submit" name="Enviar" value="Inscribeme" class="boton" /></td></tr>
</table>

