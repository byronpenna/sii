<?php

/**
 * @author Gilbetor
 * @copyright 2014
 */

?>
<script>
    function Verificar()
    {
        var Codigo = document.getElementById("Codigo").value;  
        if(!isNaN(Codigo))
        {
            document.getElementById('Formulario').innerHTML = "";
            $(document).ready(function() {
            	$.get("Verificar.php", { Codigo : Codigo },
                		function(resultado)
                		{
                		    document.getElementById('Formulario').innerHTML = "<em style='color: green;'>Cargando Datos...</em>";
                			if(resultado == false)        			
                				alert("Error");
                			
                			else
                            {
                                document.getElementById('Formulario').innerHTML = "";
                                $('#Formulario').append(resultado);	        			
                            }       			          				
                		}        
                	);
            });
        }
        else
            alert("Error, Dato no Valido");
    }
</script>

<form action="CarreraABM.php" method="post">
<table style="font-family: calibri; text-align: justify; width: 800px;">
<tr><td colspan="3"><h2>Inscripci&oacute;n</h2></td></tr>
<tr><td colspan="3"> <hr color='skyblue' /></td></tr>
<tr><td>Ingrese el n&uacute;mero de su Tickets:</td><td><input type="text" id="Codigo" name="Codigo" style="width: 260px;" required="true"  /></td>
    <td><input type="button" id="verificar" value="Verificar" class="boton" onclick="Verificar();" /></td>
</tr>
<tr><td colspan="3">
        <div id="Formulario" style="text-align: center;">Esperando Datos...</div>
    </td>
</tr>
</form>
</table>