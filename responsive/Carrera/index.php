<?php

/**
 * @author Manuel Calder�n
 * @copyright 2014
 */

?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
  $(document).ready(function() {
    	$.get("Indicaciones.php",
        		function(resultado)
        		{
        			if(resultado == false)        			
        				alert("Error");
        			
        			else        			  
        				$('#MainDiv').append(resultado);	        			
        		}
        
        	);
  });
</script>
<style>
.boton
{
    border-radius: 10px;
	padding: 5px;
	font-family: Calibri;
	color: #FFF;
	background-color: #78bbe6;	
    width: 100px;
}

.boton:hover
{
    border-radius: 10px;
	padding: 5px;
	font-family: Calibri;
	color: #78bbe6;
	background-color: #fff;	
    width: 100px;
}

.tdleft
{
    text-align: right;
    padding-right: 20px;
    width: 200px;
}
</style>
<table>
<tr><td colspan="2" style="padding-left: 20px;;"><h1 style="font-family: Calibri;">Formulario de Inscripci�n</h1></td></tr>
<tr>
    <td colspan="2">
        <div id="MainDiv" style="width: 100%;">
        </div>
    </td>
</tr>
</table>