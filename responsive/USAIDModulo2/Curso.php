<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
unset($_SESSION['ij']);

?>
<style>
td
{
   padding: 5px; 
}
</style>
<div class="Raiz">
    <a href="?" class='flecha' style='text-decoration:none'>Principal -></a>
    <a href="?u=Group" class='flecha' style='text-decoration:none'>Grupo -></a>
    <a href="?u=GroupProfile" class='flecha' style='text-decoration:none'>Perfil de Grupo -></a>
    <a href="?u=Course" class='flecha' style='text-decoration:none'>Cursos</a>
</div>

<div style="width: 100%; background-color: white; border-radius: 10px;">  
<h2 style="padding: 60px;">Administraci�n del Curso</h2>
<?php
	
    $Cursos = $bddC->prepare("Select * from Objetivo2_Cursos where IdGrupo = " . $_SESSION['ig']);
    $Cursos->execute();
    
    $ArrayH = array();
    $ArrayP = array();
    $ArrayT = array();
    
    
    $j = 0;
    
    $Participantes = $bddC->prepare("Select * from Objetivo2_Participante where IdGrupo = " . $_SESSION['ig']);
    $Participantes->execute();
    
    while($DataP = $Participantes->fetch())
    {
        $ArrayD[$j] = 0;
        $ArrayP[$j] = 0;
        $ArrayT[$j] = 0;
        
        $j++;
    }
    
    
    
    if($Cursos->rowCount() > 0)
    {
        while($DataC = $Cursos->fetch())
        {       
            $Select = $bddC->prepare("Select * from Objetivo2_Grupos as g 
                                      inner join Objetivo2_Cursos as c on g.IdGrupo = c.IdGrupo
                                      where c.IdCurso = $DataC[0]");
            $Select->execute();
            $DataG = $Select->fetch();            
            $ArrayHoras = array();
?>  
<hr color='skyblue' />
<table style="width: 90%; margin-left: 5%;" >
<tr><td style="width: 20%;">Nombre del Grupo:<br /><br />Nombre del Curso:</td><td style="width: 40%;"><?php echo $DataG[1] . " <br /><br /> " .$DataG[12]?></td>
    <td colspan="2" style="text-align: right;">
        <form action="USAIDModulo2/CursoExport.php" method="post">
        <input type="submit" class="boton" value="Exportar" name="Enviar" style="width: 120px;" />
        <input type="hidden" name="Ic" value="<?php echo $DataC[0]?>"/>
        </form>
    </td>
</tr>
<tr>
    <td style="width: 20%;">Horas minimas del Grupo:<br /><br />Horas totales del Curso</td><td style="width: 40%;"><?php echo $DataG[13] . " horas <br /><br /> " . $DataG[14] . " horas"?></td>
    <td colspan="2" style="text-align: right;">
        <?php if($_SESSION['Roll'] == "Sociolaboral"){?>
        <form action="?u=Day" method="post">            
            <input type="submit" class="boton" value="Agregar Jornada" name="Enviar" style="width: 120px;" />
            <input type="hidden" name="Ic" value="<?php echo $DataC[0]?>"/>
        </form>
        <?php }?>
    </td>
</tr>
<tr>
    <td colspan="4" >
    <div style="width: 850px; overflow-x: scroll !important;">
        <table style="table-layout: fixed; white-space: nowrap; margin: 5px; padding-bottom: 20px;" rules='all'>
                <?php

                           $Jornada = $bddC->prepare("Select * from Objetivo2_Jornadas where IdCurso = " . $DataC[0] . " 
                                                      ORDER BY Fecha ASC ");
                           $Jornada->execute();
                           $aux = $Jornada->rowCount();   
                ?>
                <tr>
                    <th rowspan="2" style="width: 60px; table-layout: fixed;" >N�</th>
                    <th rowspan="2" style="padding: 5px" >Nombre del/la joven</th>
                    <th colspan="<?php echo $aux?>">Jornadas</th>
                    <th rowspan="2">Total Horas <br />Asistidas</th>
                    <?php if($_SESSION['Roll'] == "Sociolaboral"){?>
                        <th rowspan="2">Porcentaje <br />de Asistencia</th>
                    <?php }?>
                    
                    <?php if($_SESSION['Roll'] == "Contabilidad"){?>
                        <th rowspan="2">Valor <br />por hora</th>
                        <th rowspan="2">Valor <br />a Liquidar</th>
                    <?php }?>                                
                </tr>
                <tr>
                    <?php
                            $ArrayJ = array();
                           if($Jornada->rowCount() > 0)
                           {
                                
                               $i = 0;
                               while($DataJ = $Jornada->fetch())
                               {
                                    array_push($ArrayJ,$DataJ[0]);
                                    $i++;
                                    
                                    if($_SESSION['Roll'] == "Sociolaboral")
                                    echo "<th style='padding: 10px'>$DataJ[3]<br />
                                          <form action='USAIDModulo2/JornadaABM.php' method='post'>
                                              <input type='hidden' value='$DataJ[0]' name='ij' />
                                              <input type='submit' value='$DataJ[2]' name='Enviar' style='background-color: transparent; color: darkblue; border-color: transparent;' />
                                          </form>
                                          </th>";
                                          
                                    else
                                        echo "<th style='padding: 10px'>$DataJ[3]<br />$DataJ[2]</th>";
                               }
                           }
                           else
                                echo "<th style='color: red; text-align:center;'>No hay Jornadas <br /> Registradas</th>";
                        ?>
                </tr> 
                 <?php
                   $Participantes = $bddC->prepare("Select * from Objetivo2_Participante where IdGrupo = " . $_SESSION['ig']);
                   $Participantes->execute();
                   
                   
                   if($Participantes->rowCount() > 0)
                   {
                       $i = 0;
                       $j = 0;
                       
                       while($DataP = $Participantes->fetch())
                       {
                            $k = 0;
                            $i++;
                            $thoras = 0;
                            $porcentaje = 0;
                            
                            echo "<tr style='height: 30px;'><td>$i</td><td>$DataP[2], $DataP[3]</td>";
                            
                            if (!empty($ArrayJ)) 
                            {
                                foreach($ArrayJ as $value)
                                {
                                    $Query = "Select j.Horas from Objetivo2_Asistencia as a
                                              inner join Objetivo2_Jornadas as j on a.IdJornada = j.IdJornada
                                              where a.IdJornada = $value and a.IdParticipante = $DataP[0]";
                                              
                                    $Asistencia = $bddC->prepare($Query);
                                    $Asistencia->execute();
                                    
                                    if($Asistencia->rowCount() > 0)
                                    {
                                        $DataA = $Asistencia->fetch();
                                        echo "<th>$DataA[0]</th>";
                                        $thoras += $DataA[0];
                                        $ArrayHoras[$k] += $DataA[0];
                                    }
                                    else
                                    {
                                        echo "<th>0</th>";
                                        $ArrayHoras[$k] += 0;
                                    }
                                        
                                        
                                    $k++;    
                                }                                    
                            }
                            else
                                echo "<th>-</th>";  
                                                                                                      
                            $porcentaje = $thoras / $DataG[14] * 100;
                            
                            
                            
                            if($_SESSION['Roll'] == "Sociolaboral")
                                echo "<th>$thoras</th><th>" . number_format($porcentaje,2) ."%</th>";
                            
                            if($_SESSION['Roll'] == "Contabilidad")
                            {
                                $AuxDinero = $thoras * 4;
                                echo "<th>$thoras</th><th>$" . number_format(4,2) ."</th><th>$". number_format($AuxDinero,2);
                            }
                            echo "</tr>"; 
                            
                            $ArrayH[$j] += $thoras;
                            $ArrayP[$j] += $porcentaje;
                            $ArrayT[$j] += $DataG[14];
                            $j++;

                       }
                   }
                   else
                        echo "<tr><td colspan='3' style='color: red; text-align:center;'>No hay Participantes Registrados</td></tr>";
                   
                   echo "<tr><td colspan='2' style='text-align: center;'>Totales</td>";
                   
                   $auxHoras = 0;
                   foreach($ArrayHoras as $value)
                   {
                        $auxHoras += $value;
                        echo "<th style='color: darkblue;'>$value</th>";
                   }
                   $auxNumero = $auxHoras * 4;
                   echo "<th style='color: darkblue;'>$auxHoras</th>";
                   
                   if($_SESSION['Roll'] == "Contabilidad")
                        echo "<th style='color: darkblue;'>$". number_format(4,2) ."</th><th style='color: darkblue;'>$". number_format($auxNumero,2) ."</th></tr>";
                      
                   if($_SESSION['Roll'] == "Sociolaboral")
                   {
                        $auxNumero = $auxHoras / ($DataG[14] * $Participantes->rowCount()) * 100;
                        echo "<th style='color: darkblue;'>".number_format($auxNumero,2)."%</th></tr>";
                   }
        ?>                     
        </table>
    </div>


    </td>
</tr>
</table>
<br />
<?php
	        }
    }
?>
<br />
    <hr color='skyblue' />
    
    <table style="width: 90%; margin-left: 5%;">
    <tr>
        <td colspan="4" style="padding-left: 20px;"><h2>Resumen por Participante</h2></td>
        <td style="text-align: right;" colspan="2">
            <form action='USAIDModulo2/CursoExportDetails.php' method='post'>
              <input type='submit' value='Exportar' name='Enviar' class="boton"/>
            </form>
        </td>
    </tr>
    <tr>
        <th>N�</th>
        <th>Nombre del/la joven</th>
        <th>Total de Horas <br />asistidas en <br />ambos cursos</th>
        <th>Total de Horas <br />impartidas en <br />ambos cursos</th>
        
        <?php if($_SESSION['Roll'] == "Contabilidad"){?>
                  <th>Valor <br />por Hora</th>
                  <th>Total <br />a Liquidar</th>
        <?php }
              
              if($_SESSION['Roll'] == "Sociolaboral"){?>
                  <th>Procentaje de asistencia <br />en ambos cursos</th>
        <?php }?>
    </tr>
    
    
     <?php
       $Participantes = $bddC->prepare("Select * from Objetivo2_Participante where IdGrupo = " . $_SESSION['ig']);
       $Participantes->execute();
       
       if($Participantes->rowCount() > 0)
       {
            $i = 0;
            $j = 0;
           while($DataP = $Participantes->fetch())
           {
                $j++;
                $thoras = 0;
                $porcentaje = 0;
                $porcentaje = $ArrayH[$i] / $ArrayT[$i] * 100;
                
                echo "<tr style='height: 30px;'><td>$j</td><td>$DataP[2], $DataP[3]</td>
                      <th>$ArrayH[$i] horas</th><th>$ArrayT[$i] horas</th>";
                      
                if($_SESSION['Roll'] == "Sociolaboral")
                    echo "<th>". number_format($porcentaje,2) ."%</th></tr>";
                    
                if($_SESSION['Roll'] == "Contabilidad")
                {
                    $AuxDinero = $ArrayH[$i] * 4;
                    echo "<th>$". number_format(4,2) ."</th><th>$". number_format($AuxDinero,2) ."</th></tr>";
                }
                $i++;
           }
       }
       else
            echo "<tr><td colspan='3' style='color: red; text-align:center;'>No hay Participantes Registrados</td></tr>"
    ?>                          
    </table>                 
</div>