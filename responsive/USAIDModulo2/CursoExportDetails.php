<?php

/**
 * @author Gilbetor
 * @copyright 2014
 */
    
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=Detalles del Grupo.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';

	
    $Grupos = $bddC->prepare("Select * from Objetivo2_Grupos where IdGrupo = " . $_SESSION['ig']);
    $Grupos->execute();
    $DataG = $Grupos->fetch();

    $Departamento = $bddC->prepare("Select Departamento from  USAID_Departamentos where IdDepartamento = $DataG[6]");
    $Departamento->execute();
    $DepartamentoAux = $Departamento->fetch();
    
?>
<style>
.tdt
{
    border-style: solid; 
    border-color: black;
}
</style>
<table rules=all>
<tr><td></td><td colspan="6"><h2>Proyecto Educaci�n para la Ni�ez y Juventud - Objetivo 2</h2></td></tr>
<tr><td></td></tr>
<tr><td colspan="2"></td><td>N� de Grupo: </td><th colspan="2" style="text-align: left;"><?php echo $DataG[1];?></th></tr>
<tr><td colspan="2"></td><td>Fecha de Inicio: </td><th colspan="2" style="text-align: left;"><?php echo $DataG[2];?></th><td colspan="2" >Fecha de Finalizaci�n:</td><th colspan="3" style="text-align: left;"><?php echo $DataG[3];?></th></tr>
<tr><td colspan="2"></td><td>Centro Colaborador:</td><th colspan="2" style="text-align: left;"><?php echo $DataG[4];?></th><td colspan="2">Codigo:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[5];?></th></tr>
<tr><td colspan="2"></td><td>Departamento:</td><th colspan="2" style="text-align: left;"><?php echo $DepartamentoAux[0];?></th><td colspan="2">Municipio:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[7];?></th></tr>
<tr><td colspan="2"></td><td>Beneficiado:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[8];?></th></tr>
<tr><td></td></tr>

<?php

    $Participantes = $bddC->prepare("Select * from Objetivo2_Participante where IdGrupo = " . $_SESSION['ig']);
    $Participantes->execute();
    
    if($Participantes->rowCount() > 0)
    {
       $i = 0;
       $j = 0;
        
       echo "<tr><td rowspan='2'></td><th rowspan='2' class='tdt'>N�</th><th rowspan='2' class='tdt'>Apellidos, Nombres </th>";
       
       $Cursos = $bddC->prepare("Select * from Objetivo2_Cursos where IdGrupo = " . $_SESSION['ig'] );
       $Cursos->execute();
       $ArrayJ = array();
               
       while($DataC = $Cursos->fetch())
       {
           $Jornadas = $bddC->prepare("Select * from Objetivo2_Jornadas where IdCurso = " . $DataC[0] . " ORDER BY Fecha ASC " );
           $Jornadas->execute();            
           
           array_push($ArrayJ, $DataC[0]);
           $aux = $Jornadas->rowCount() + 2 ;
           echo "<th colspan='$aux' class='tdt' >$DataC[2]</th>";
           
       } 
       echo "<th class='tdt' colspan='2'> Resumen del Participante</th>";
       echo "<tr>";
       foreach($ArrayJ as $value)
       {
           $Query = "Select * from Objetivo2_Jornadas where IdCurso = " . $value . " ORDER BY Fecha ASC  " ;
        
           $Jornadas = $bddC->prepare($Query);
           $Jornadas->execute(); 
           
           while($DataJ = $Jornadas->fetch())
           {           
                echo "<th class='tdt'>$DataJ[2] <br /> $DataJ[3]</th>";
           }         
           echo "<th class='tdt'>Total <br />de Horas</th>";
           echo "<th class='tdt'>Porcentaje <br />de Asistencia</th>";
       }
       echo "<th class='tdt'>Total de <br />horas Recibidas</th>";
       echo "<th class='tdt'>Porcentaje <br />Total</th>";       
       echo "</tr>";
        
       while($DataP = $Participantes->fetch())
       {
            $j++;
            $i++;
            $horasT = 0;
            $porcentajet = 0;
            $AuxHoras = 0;
            echo "<tr><td></td><th class='tdt'>$i</th><td style='width: 250px;' class='tdt'>$DataP[2], $DataP[3]</td>";
            
            foreach($ArrayJ as $value)
            {
                $horas = 0;
                $porcentaje = 0;
                
                
                $Query = "Select * from Objetivo2_Jornadas where IdCurso = " . $value ;
             
                $Jornadas = $bddC->prepare($Query);
                $Jornadas->execute(); 
               
                while($DataJ = $Jornadas->fetch())
                {           
                    $Query = "Select * from Objetivo2_Asistencia where IdJornada = $DataJ[0] and IdParticipante = $DataP[0]";
                    
                    $Asistencia = $bddC->prepare($Query);
                    $Asistencia->execute(); 
                    
                    if($Asistencia->rowCount() > 0)
                    {                                         
                        echo "<th class='tdt'>$DataJ[4]</th>";
                        $horas += $DataJ[4];
                    }    
                    else
                        echo "<th class='tdt'> - </th>";
                }
                echo "<th class='tdt'>$horas</th>";         
                $horasT += $horas;
                
                $HorasCurso = $bddC->prepare("Select HorasTotales from Objetivo2_Cursos where IdCurso = " . $value);
                $HorasCurso->execute();
                $HorasTotales = $HorasCurso->fetch();
                
                $porcentaje = $horas / $HorasTotales[0]  * 100;
                $AuxHoras += $HorasTotales[0];
                
                echo "<th class='tdt'>$porcentaje%</th>";
                
            } 
            echo "<th class='tdt'>$horasT</th>";
            
            $porcentajet = $horasT / $AuxHoras * 100;
            echo "<th class='tdt'>". number_format($porcentajet,2) ."%</th>";           
            echo "</tr>";

       }
    }
    else
        echo "<tr><td colspan='3' style='color: red; text-align:center;'>No hay Participantes Registrados</td></tr>";             
?>
</table>