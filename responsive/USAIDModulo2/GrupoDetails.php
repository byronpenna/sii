<div class="Raiz">
    <a href="?" class='flecha' style='text-decoration:none'>Principal -></a>
    <a href="?u=Group" class='flecha' style='text-decoration:none'>Grupo -></a>
    <a href="?u=GroupProfile" class='flecha' style='text-decoration:none'>Perfil de Grupo -></a>
    <a href="?u=GroupDetails" class='flecha' style='text-decoration:none'>Detalles -></a>
</div>
<div style="width: 100%; background-color: white; border-radius: 10px;">
<table style="width: 100%;">
    <tr><td style="text-align: left;" colspan="2"><h2>Detalles del Grupo</h2></td>
        <td style="text-align: right; width: 15%;"><form action="USAIDModulo2/GrupoDetailsExport.php" method="post"><input type="submit" class="boton" name="Enviar" value="Exportar" /></form></td>
        <td style="text-align: right; width: 15%;"><form action="USAIDModulo2/GrupoDetailsPrint.php" method="post" target="_blank"><input type="submit" class="boton" name="Enviar" value="Imprimir" /></form></td>
    </tr>
    <tr><td colspan="4"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="4">
            <table style="width: 100%;">
                <tr>
                    <th rowspan="2" style="width: 40%;">Curso</th>
                    <th colspan="3" style="width: 30%;">Revisión por Horas</th>
                    <th rowspan="2" style="width: 10%;">Total de <br />Jovenes</th>
                    <th rowspan="2" style="width: 10%;">Total de <br />Horas</th>
                    <th rowspan="2" style="width: 10%;">Total a <br />Liquidar</th></tr>
                <tr>
                    <th style="width: 10%;">0 Horas</th>
                    <th style="width: 10%;">1 - 15 Horas</th>
                    <th style="width: 10%;">16+ Horas</th>
                </tr>
                <tr>
                    <?php
                            $Cursos = $bddC->prepare("Select * from Objetivo2_Cursos where IdGrupo = " . $_SESSION['ig']);
                            $Cursos->execute();

                            $ArrayP = array();
                            $Participantes = $bddC->prepare("SELECT * FROM Objetivo2_Participante where IdGrupo = " . $_SESSION['ig']);
                            $Participantes->execute();
                            while($DataP = $Participantes->fetch())
                                array_push($ArrayP,$DataP[0]);

                            $ArrayT = array();
                            
                            for($i = 0; $i < 6; $i++)
                                $ArrayT[$i] = 0;
                                
                            while($DataC = $Cursos->fetch())
                            {
                                $Aux0 = 0;
                                $Aux115 = 0;
                                $Aux16 = 0;
                                $AuxMonto = 0;
                                $AuxHoras = 0;
                                
                                echo "<tr><th>$DataC[2]</th>";
                                
                                foreach($ArrayP as $value)
                                {
                                    $Horas = $bddC->prepare("SELECT j.Horas FROM Objetivo2_Jornadas as j 
                                                             INNER JOIN Objetivo2_Asistencia as a on j.IdJornada = a.IdJornada
                                                             WHERE a.IdParticipante = $value and j.IdCurso = $DataC[0]");
                                    
                                    $Horas->execute(); 
                                    
                                    $aux = 0;
                                    while($DataH = $Horas->fetch())
                                    {
                                        $aux += $DataH[0];
                                    }
                                    
                                    
                                    if($aux == 0)
                                        $Aux0++;
                                        
                                    else if($aux < 16)
                                        $Aux115++;
                                        
                                    else
                                        $Aux16++;
                                    
                                    
                                    if($DataC[2] == "Refuerzo Pedagógico")
                                        $Multipicador = 2;
                                    
                                    else
                                        $Multipicador = 4;
                                    $AuxHoras += $aux;
                                    $AuxMonto += ($aux * $Multipicador);
                                }
                                
                                echo "<th>$Aux0</th><th>$Aux115</th><th>$Aux16</th>";
                                echo "<th>" . count($ArrayP) . "</th><th>$AuxHoras</th><th>$".number_format($AuxMonto, 2) ."</th>";
                                
                                $ArrayT[0] += $Aux0;
                                $ArrayT[1] += $Aux115;
                                $ArrayT[2] += $Aux16;
                                $ArrayT[3] += count($ArrayP);
                                $ArrayT[4] += $AuxHoras;
                                $ArrayT[5] += $AuxMonto;
                            }
                    ?>
                </tr>
                <tr><td colspan="7"><hr color='skyblue' /></td></tr>
                <tr>
                    <th>Total</th>
                    <th><?php echo $ArrayT[0];?></th>
                    <th><?php echo $ArrayT[1];?></th>
                    <th><?php echo $ArrayT[2];?></th>
                    <th><?php echo $ArrayT[3];?></th>
                    <th><?php echo $ArrayT[4];?></th>
                    <th>$<?php echo number_format($ArrayT[5], 2);?></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td></td></tr>
</table>
</div>