<?php

/**
 * @author Manuel Calder�n
 * @copyright 2014
 */
    
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=Consolidado de los Grupos.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';

	

?>
<style>
.tdt
{
    border-style: solid; 
    border-color: black;
}
</style>
<table >
<tr><td colspan="6"><h2>Proyecto Educaci�n para la Ni�ez y Juventud - Objetivo 2</h2></td></tr>
<tr></tr>
<tr><td style="text-align: left;" colspan="4"><h2>Detalles del Grupo</h2><hr color='skyblue' /></td></tr>
<tr>
    <td colspan="4">
           <tr><td colspan="4"><hr color='skyblue' /></td></tr>

        <tr>
        <td colspan="4"><br />
            <table style="width: 100%;">
                <tr>
                    <th rowspan="2" style="width: 5%;">Grupo</th>
                    <th rowspan="2" style="width: 35%;">Curso</th>
                    <th colspan="3" style="width: 30%;">Revisi�n por Horas</th>
                    <th rowspan="2" style="width: 10%;">Total de <br />Jovenes</th>
                    <th rowspan="2" style="width: 10%;">Total de <br />Horas</th>
                    <th rowspan="2" style="width: 10%;">Total a <br />Liquidar</th></tr>
                <tr>
                    <th style="width: 10%;">0 Horas</th>
                    <th style="width: 10%;">1 - 15 Horas</th>
                    <th style="width: 10%;">16+ Horas</th>
                </tr>
                <?php
                    $ArrayT = array();
                    
                    for($i = 0; $i < 6; $i++)
                        $ArrayT[$i] = 0;   
                    
                     $Grupos = $bddC->prepare("SELECT * FROM Objetivo2_Grupos ORDER BY Nombre ASC");
                     $Grupos->execute();
                     while($DataG = $Grupos->fetch())	    
                     {  	
                        $Cursos = $bddC->prepare("Select * from Objetivo2_Cursos where IdGrupo = $DataG[0]");
                        $Cursos->execute();

                        $ArrayP = array();
                        $Participantes = $bddC->prepare("SELECT * FROM Objetivo2_Participante where IdGrupo = $DataG[0]" );
                        $Participantes->execute();
                        while($DataP = $Participantes->fetch())
                            array_push($ArrayP,$DataP[0]);


                            
                        while($DataC = $Cursos->fetch())
                        {
                            $Aux0 = 0;
                            $Aux115 = 0;
                            $Aux16 = 0;
                            $AuxMonto = 0;
                            $AuxHoras = 0;
                            
                            echo "<tr><th>$DataG[1]</th><th>$DataC[2]</th>";
                            
                            foreach($ArrayP as $value)
                            {
                                $Horas = $bddC->prepare("SELECT j.Horas FROM Objetivo2_Jornadas as j 
                                                         INNER JOIN Objetivo2_Asistencia as a on j.IdJornada = a.IdJornada
                                                         WHERE a.IdParticipante = $value and j.IdCurso = $DataC[0]");
                                
                                $Horas->execute(); 
                                
                                $aux = 0;
                                while($DataH = $Horas->fetch())
                                {
                                    $aux += $DataH[0];
                                }
                                
                                
                                if($aux == 0)
                                    $Aux0++;
                                    
                                else if($aux < 16)
                                    $Aux115++;
                                    
                                else
                                    $Aux16++;
                                
                                
                                if($DataC[2] == "Refuerzo Pedag�gico")
                                    $Multipicador = 2;
                                
                                else
                                    $Multipicador = 4;
                                $AuxHoras += $aux;
                                $AuxMonto += ($aux * $Multipicador);
                            }
                            
                            echo "<th>$Aux0</th><th>$Aux115</th><th>$Aux16</th>";
                            echo "<th>" . count($ArrayP) . "</th><th>$AuxHoras</th><th>$".number_format($AuxMonto, 2) ."</th>";
                            
                            $ArrayT[0] += $Aux0;
                            $ArrayT[1] += $Aux115;
                            $ArrayT[2] += $Aux16;
                            $ArrayT[3] += count($ArrayP);
                            $ArrayT[4] += $AuxHoras;
                            $ArrayT[5] += $AuxMonto;
                        }
                }
                ?>                
                <tr><td colspan="8"><hr color='skyblue' /></td></tr>
                <tr>
                    <th colspan="2">Total</th>
                    <th><?php echo $ArrayT[0];?></th>
                    <th><?php echo $ArrayT[1];?></th>
                    <th><?php echo $ArrayT[2];?></th>
                    <th><?php echo $ArrayT[3];?></th>
                    <th><?php echo $ArrayT[4];?></th>
                    <th>$<?php echo number_format($ArrayT[5], 2);?></th>
                </tr>
            </table>
        </td>
    </tr>

    <tr><td></td></tr>
</table>