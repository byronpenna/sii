<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 

if($_SESSION['ig'] != "")
{
    $Institucion = $bddC->prepare("Select * from Objetivo2_Grupos where IdGrupo = " . $_SESSION['ig']);
    $Institucion->execute();
    
    $DataG = $Institucion->fetch();
    $ABM = "Actualizar";
}
else
{
    $ABM = "Guardar";
}

?>
<script>
function CargarListaMunicipios()
{
    var departamento = document.getElementById('Departamentos').value;    
    document.getElementById('Municipio').options.length = 0;
    
    var aux = true;
    
    $.get("USAID/SelectMunicipios.php", { departamento:departamento, aux:aux },
  		function(resultado)
  		{           
            document.getElementById('Municipio').options.length = 0;
                
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#Municipio').append(resultado);	            			
  		}
   	);        
}
</script>
<div class="Raiz">
    <a href="?" class='flecha' style='text-decoration:none'>Principal -></a>
    <a href="?u=Group" class='flecha' style='text-decoration:none'>Grupo</a>
</div>

<div style="width: 100%; background-color: white; border-radius: 10px;">
<form action="USAIDModulo2/GrupoABM.php" method="post">
    <table style="width: 100%; margin-top: 20px;">
        <tr><td colspan="2" style=" padding-left: 20%;"><h2 style="color: blue;">Formulario de Grupo</h2></td></tr>
        <tr>
            <td class="tdleft" style="width: 30%; height: 50px;">Nombre del Grupo:</td>
            <td><input type="text" name="nombre" value="<?php echo $DataG[1];?>" required="" /></td>
        </tr>
        <tr>
            <td class="tdleft" style="width: 30%; height: 50px;">Departamento:</td>
            <td>
                <select name='Departamentos' id='Departamentos' style='width: 250px;' onchange='CargarListaMunicipios()' >
                    <option value="...">...</option>
                        <?php

                                
                                $Departamentos = $bddC->prepare("Select * from USAID_Departamentos ");
                                $Departamentos->execute();
    
                            	while($DataD = $Departamentos->fetch()) 
                                {
                                    if($DataG[6] == $DataD[0])
                                       echo "<option value='$DataD[0]' selected='true'>$DataD[1]</option>";    
                                    
                                    else
                                        echo "<option value='$DataD[0]'>$DataD[1]</option>";
                                }    
                                        
                        ?>
                 </select>            
            </td>
        </tr>
        <tr>
            <td class="tdleft" style="width: 30%; height: 50px;">Municipio:</td>
            <td>
                <select name='Municipio' id='Municipio' style='width: 250px;'>";
                    <option value="...">...</option>
                    <?php
                            if($_SESSION['ig'] != "")
                            {
                                $auxMunicipio = $bddC->prepare("Select m.* from USAID_Departamentos as d
                                                                inner join USAID_Municipios as m on d.IdDepartamento = m.Departamento
                                                                WHERE d.IdDepartamento =  $DataG[6]");
                                $auxMunicipio->execute();
                                                               
                            	while($AuxDataM = $auxMunicipio->fetch()) 
                                {
                                    if($DataG[7] == $AuxDataM[2])
                                       echo "<option value='$AuxDataM[2]' selected='true'>$AuxDataM[2]</option>";    
                                    
                                    else
                                        echo "<option value='$AuxDataM[2]'>$AuxDataM[2]</option>";
                                } 
                            }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="tdleft" style="width: 30%; height: 50px;">Centro Colaborador:</td>
            <td><input type="text" name="centro" value="<?php echo $DataG[4];?>" required="" /></td>
        </tr>  
        <tr>
            <td class="tdleft" style="width: 30%; height: 50px;">Codigo del Centro Colaborador:</td>
            <td><input type="text" name="codigo" value="<?php echo $DataG[5];?>" required="" /></td>
        </tr>  
        <tr>
            <td class="tdleft" style="width: 30%; height: 50px;">Centro, Cant�n o Comunidad Beneficiada:</td>
            <td><textarea name="direccion" style="width: 250px; height: 50px;"><?php echo $DataG[8];?></textarea></td>
        </tr>                                        
        <tr>
            <td class="tdleft" style="width: 30%; height: 50px;" >Fecha de Inicio:<br /><span>(aaaa-mm-dd)</span></td>
            <td><input type="text" name="fechai" name="date" value="<?php echo $DataG[2];?>" required="" pattern="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])" title="Formato Incorrecto" /></td>
        </tr>
        <tr>
            <td class="tdleft" style="width: 30%; height: 50px;">Fecha de Fin:<br /><span>(aaaa-mm-dd)</span></td>
            <td><input type="text" name="fechaf" name="date" value="<?php echo $DataG[3];?>" required="" pattern="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])" title="Formato Incorrecto" /></td>
        </tr>
        <tr><td></td><td><input type="submit" value="<?php echo $ABM;?>" name="Enviar" class="boton" /></td></tr>
    </table>
</form>
</div>
