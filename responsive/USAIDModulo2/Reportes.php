<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

?>
<script>
function CargarReporte()
{
    var code2 = document.getElementById('FechaInicio').value;
    var code3 = document.getElementById('FechaFin').value;   
        	
    document.getElementById('ReportArea').innerHTML = "<center><em style='color: green;'>Cargando datos...</em></center>";

    $.get("USAIDModulo2/ReportesAuxiliar.php", { code2: code2, code3: code3 },
  		function(resultado)
  		{
            document.getElementById('ReportArea').innerHTML = "";                        
 			if(resultado == false)     			
				alert("Error");
 			
 			else            			        			    
				$('#ReportArea').append(resultado);	            			
  		}
   	);        
}
</script>
</script>
<div class="Raiz">
    <a href="?" class='flecha' style='text-decoration:none'>Principal -></a>
    <a href="?u=Group" class='flecha' style='text-decoration:none'>Grupo -></a>
    <a href="?u=Reports" class='flecha' style='text-decoration:none'>Reportes </a>
</div>
<div style="width: 100%; background-color: white; border-radius: 10px;">
<br />  
    <table style="width: 90%; margin-left: 5%;">
        <tr><td><h2 style="padding-left: 60px;">Reporteria de Grupos</h2></td></tr>
        <tr>
            <td>
                <table style="width: 60%;">
                    <tr>
                        <td class="tdleft"> Fecha de Inicio:<br /><span style="color: blue; font-size: smaller;">(aaaa-mm-dd)</span></td>
                        <td><input type="date" value="" name="FechaInicio" id="FechaInicio" /></td>
                        <td rowspan="2"><input type="submit" value="Buscar" class="boton" onclick="CargarReporte();" /></td>
                    </tr>
                    <tr>
                        <td class="tdleft"> Fecha de Finalizaci�n:<br /><span style="color: blue; font-size: smaller;">(aaaa-mm-dd)</span></td>
                        <td><input type="date" value="" name="FechaFin" id="FechaFin" /></td> 
                    </tr>                    
                </table>
            </td>
        </tr>
        <tr><td><hr color='skyblue' /></td></tr>
        <tr>
            <td>
                <div id="ReportArea" style="width: 850px; overflow-x: scroll !important;">
                    <br /><br />
                    <center>
                    <em style="color: blue;">Esperando Datos</em>
                    </center>
                    <br /><br />
                </div>
                <br /><br />
            </td>
        </tr>
    </table>
</div>