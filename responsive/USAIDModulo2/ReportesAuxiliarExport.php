<?php

/**
 * @author Manuel Calder�n
 * @copyright 2014
 */
    
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=Detalles General del Grupo.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';

	$FechaInicio = $_POST["code2"];
    $FechaFin = $_POST["code3"];  
    $IdGrupo = $_POST['ig'];    

    
    $Query = "SELECT g.* FROM Objetivo2_Jornadas AS j
              INNER JOIN Objetivo2_Cursos AS c ON c.IdCurso = j.IdCurso
              INNER JOIN Objetivo2_Grupos AS g ON g.IdGrupo = c.IdGrupo
              WHERE Fecha BETWEEN  '$FechaInicio' AND  '$FechaFin' AND g.IdGrupo = $IdGrupo 
              Group by g.IdGrupo
              ORDER BY j.Fecha ASC";
              
    $jornadasG = $bddC->prepare($Query);
    $jornadasG->execute();
    
    if($jornadasG->rowCount() > 0)
    {         
         
         while($dataJ = $jornadasG->fetch() )
         {     
               ?> 
               
               <div style="width: 100%;">               
                 <table style='width: 100%; font-size: medium;'>
                    <tr><th colspan="11">FUNDACION SALVADOR DEL MUNDO</th>                        
                    <tr><th colspan="11">PROYECTO "Educaci�n para la Ni�ez y Juventud"</th></tr>
                    <tr><th colspan="11">OBJETIVO II</th></tr>
                    <tr><th colspan="11">CONSOLIDADO DE ASISTENCIAS</th></tr>												
                    <tr><th colspan="11">Grupo: <?php echo $dataJ[1] . " - " .  $dataJ[8];?></th></tr>
                    <tr><th colspan="11">Periodo: <?php echo $FechaInicio . " al " . $FechaFin;?></th></tr>
                 </table>
               </div>  
               
               <br /><br />              	
               
               <?php

               $Query = "SELECT j.Jornada, j.Fecha FROM Objetivo2_Jornadas AS j
                         INNER JOIN Objetivo2_Cursos AS c ON c.IdCurso = j.IdCurso
                         WHERE Fecha BETWEEN  '$FechaInicio' AND  '$FechaFin' AND c.IdGrupo = $dataJ[0]
                         group by j.Fecha
                         ORDER BY j.Fecha ASC";
                         
               $Jornadas = $bddC->prepare($Query);
               $Jornadas->execute();
               $arrayJ = array();
                              
               while($JornadaH = $Jornadas->fetch())                
                    array_push($arrayJ, $JornadaH[1]);


               
               echo "<table style='table-layout: fixed; white-space: nowrap; margin: 5px; padding-bottom: 20px;' rules='all'>
                     <tr><th rowspan='2'>Taller</th>";
               
               foreach($arrayJ as $value)
               {            
                    echo "<th colspan='2'>$value</th>";
               }
               
               echo "<th rowspan='2' >Total <br />Horas</th>
                     <th rowspan='2' >Valor *<br /> Horas</th>
                     <th rowspan='2' >Valor <br />en $</th></tr>
                     <tr>";
               
               foreach($arrayJ as $value)
               {
                    echo "<th>Cantidad de <br /> J&oacute;venes</th><th>Cantidad <br />de Horas</th>"; 
               }
               echo "<tr>";
               
               $Query = "SELECT c.IdCurso, c.Curso FROM Objetivo2_Jornadas AS j
                         INNER JOIN Objetivo2_Cursos AS c ON c.IdCurso = j.IdCurso
                         WHERE c.IdGrupo = $dataJ[0]
                         group by j.IdCurso
                         ORDER BY j.Fecha ASC";
                         
               $Curso = $bddC->prepare($Query);
               $Curso->execute();   
                
               $LiquidacionT = 0;
               
               while($DataC = $Curso->fetch())
               {
                   $auxHoras = 0;
                   echo "<tr><th>$DataC[1]</th>";
                   foreach($arrayJ as $value)
                   {

                       $Query = "SELECT * FROM Objetivo2_Jornadas where IdCurso = $DataC[0] and Fecha = '$value' ORDER BY Fecha ASC";                                                        
                       $JornadaP = $bddC->prepare($Query);
                       $JornadaP->execute();     
                       $DataJP = $JornadaP->fetch();
                       
                       $Asistencia = $bddC->prepare("SELECT * FROM Objetivo2_Asistencia where IdJornada = $DataJP[0]");
                       $Asistencia->execute();
                       
                       $auxh = $Asistencia->rowCount() * $DataJP[4];
                       echo "<th>" .  $Asistencia->rowCount() . "</th><th>$auxh</th>"; 
                       $auxHoras += $auxh;              
                   } 
                   
                   $Liquidacion = $auxHoras * 4; 
                   echo "<th>$auxHoras</th><th>$4.00</td><th>$". number_format($Liquidacion,2)."</td>";
                   echo "</tr>";
                   
                   $LiquidacionT += $Liquidacion;
               }               
               
               $aux = count($arrayJ) * 2 + 3;
               echo "<tr><th colspan='$aux' class='tdleft'> Total a Liquidar</th><th>$". number_format($LiquidacionT,2)."</th></tr>";
                                   
                         
               echo "</table>
                     <br /> ";                            
         }
    }
    ?>