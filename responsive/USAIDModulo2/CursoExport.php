<?php

/**
 * @author Gilbetor
 * @copyright 2014
 */
    
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=Curso.xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';
    

    $Select = $bddC->prepare("Select * from Objetivo2_Grupos as g 
                              inner join Objetivo2_Cursos as c on g.IdGrupo = c.IdGrupo
                              where c.IdCurso = " . $_POST['Ic'] );
    $Select->execute();
    $DataG = $Select->fetch();

    $Departamento = $bddC->prepare("Select Departamento from  USAID_Departamentos where IdDepartamento = $DataG[6]");
    $Departamento->execute();
    $DepartamentoAux = $Departamento->fetch();    
    
?>

 <table style="width: 90%; margin-left: 5%;" >
        
<tr><td></td><td colspan="7" style="text-align: center;"><h3>Proyecto Educaci�n para la Ni�ez y Juventud - Objetivo 2</h3></td></tr>
<tr><td></td></tr>
<tr><td></td><td>Nombre del Curso: </td><th style="text-align: left;" colspan="2"><?php echo $DataG[12];?></th><td colspan="2" >N� de Grupo:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[1];?></th></tr>
<tr><td></td><td>Fecha de Inicio: </td><th style="text-align: left;"><?php echo $DataG[2];?></th><td></td><td colspan="2" >Fecha de Finalizaci�n:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[3];?></th></tr>
<tr><td></td><td>Centro Colaborador:</td><th style="text-align: left;"><?php echo $DataG[4];?></th><td></td><td colspan="2">Codigo:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[5];?></th></tr>
<tr><td></td><td>Departamento:</td><th style="text-align: left;"><?php echo $DepartamentoAux[0];?></th><td></td><td colspan="2">Municipio:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[7];?></th></tr>
<tr><td></td><td>Beneficiado:</td><th colspan="4" style="text-align: left;"><?php echo $DataG[8];?></th><td></td></tr>
<tr><td></td></tr>
        <tr>
        <table style="table-layout: fixed; white-space: nowrap; margin: 5px; padding-bottom: 20px;" rules='all'>
                <?php

                           $Jornada = $bddC->prepare("Select * from Objetivo2_Jornadas where IdCurso = " . $_POST['Ic'] . " 
                                                      ORDER BY Fecha ASC ");
                           $Jornada->execute();
                           $aux = $Jornada->rowCount();   
                ?>
                <tr>
                    <th rowspan="2" style="width: 60px; table-layout: fixed;" >N�</th>
                    <th rowspan="2" style="padding: 5px" >Nombre del/la joven</th>
                    <th colspan="<?php echo $aux?>">Jornadas</th>
                    <th rowspan="2">Total Horas <br />Asistidas</th>
                    <?php if($_SESSION['Roll'] == "Sociolaboral"){?>
                        <th rowspan="2">Porcentaje <br />de Asistencia</th>
                    <?php }?>
                    
                    <?php if($_SESSION['Roll'] == "Contabilidad"){?>
                        <th rowspan="2">Valor <br />por hora</th>
                        <th rowspan="2">Valor <br />a Liquidar</th>
                    <?php }?>                                
                </tr>
                <tr>
                    <?php
                            $ArrayJ = array();
                           if($Jornada->rowCount() > 0)
                           {
                                
                               $i = 0;
                               while($DataJ = $Jornada->fetch())
                               {
                                    array_push($ArrayJ,$DataJ[0]);
                                    $i++;
                                    
                                    if($_SESSION['Roll'] == "Sociolaboral")
                                    echo "<th style='padding: 10px'>$DataJ[3]<br />
                                          <form action='USAIDModulo2/JornadaABM.php' method='post'>
                                              <input type='hidden' value='$DataJ[0]' name='ij' />
                                              <input type='submit' value='$DataJ[2]' name='Enviar' style='background-color: transparent; color: darkblue; border-color: transparent;' />
                                          </form>
                                          </th>";
                                          
                                    else
                                        echo "<th style='padding: 10px'>$DataJ[3]<br />$DataJ[2]</th>";
                               }
                           }
                           else
                                echo "<th style='color: red; text-align:center;'>No hay Jornadas <br /> Registradas</th>";
                        ?>
                </tr> 
                 <?php
                   $Participantes = $bddC->prepare("Select * from Objetivo2_Participante where IdGrupo = " . $_SESSION['ig']);
                   $Participantes->execute();
                   
                   
                   if($Participantes->rowCount() > 0)
                   {
                       $i = 0;
                       $j = 0;
                       
                       while($DataP = $Participantes->fetch())
                       {
                            $k = 0;
                            $i++;
                            $thoras = 0;
                            $porcentaje = 0;
                            
                            echo "<tr style='height: 30px;'><td>$i</td><td>$DataP[2], $DataP[3]</td>";
                            
                            if (!empty($ArrayJ)) 
                            {
                                foreach($ArrayJ as $value)
                                {
                                    $Query = "Select j.Horas from Objetivo2_Asistencia as a
                                              inner join Objetivo2_Jornadas as j on a.IdJornada = j.IdJornada
                                              where a.IdJornada = $value and a.IdParticipante = $DataP[0]";
                                              
                                    $Asistencia = $bddC->prepare($Query);
                                    $Asistencia->execute();
                                    
                                    if($Asistencia->rowCount() > 0)
                                    {
                                        $DataA = $Asistencia->fetch();
                                        echo "<th>$DataA[0]</th>";
                                        $thoras += $DataA[0];
                                        $ArrayHoras[$k] += $DataA[0];
                                    }
                                    else
                                    {
                                        echo "<th>0</th>";
                                        $ArrayHoras[$k] += 0;
                                    }
                                        
                                        
                                    $k++;    
                                }                                    
                            }
                            else
                                echo "<th>-</th>";  
                                                                                                      
                            $porcentaje = $thoras / $DataG[14] * 100;
                            
                            
                            
                            if($_SESSION['Roll'] == "Sociolaboral")
                                echo "<th>$thoras</th><th>" . number_format($porcentaje,2) ."%</th>";
                            
                            if($_SESSION['Roll'] == "Contabilidad")
                            {
                                if($DataG[12] == "Refuerzo Pedag�gico")
                                    $multi = 2;
                                    
                                else
                                    $multi = 4;
                                
                                $AuxDinero = $thoras * $multi;
                                echo "<th>$thoras</th><th>$" . number_format($multi,2) ."</th><th>$". number_format($AuxDinero,2);
                            }
                            echo "</tr>"; 
                            
                            $ArrayH[$j] += $thoras;
                            $ArrayP[$j] += $porcentaje;
                            $ArrayT[$j] += $DataG[14];
                            $j++;

                       }
                   }
                   else
                        echo "<tr><td colspan='3' style='color: red; text-align:center;'>No hay Participantes Registrados</td></tr>";
                   
                   echo "<tr><td colspan='2' style='text-align: center;'>Totales</td>";
                   
                   $auxHoras = 0;
                   foreach($ArrayHoras as $value)
                   {
                        $auxHoras += $value;
                        echo "<th style='color: darkblue;'>$value</th>";
                   }
                   $auxNumero = $auxHoras * 4;
                   echo "<th style='color: darkblue;'>$auxHoras</th>";
                   
                   if($_SESSION['Roll'] == "Contabilidad")
                        echo "<th style='color: darkblue;'>$". number_format(4,2) ."</th><th style='color: darkblue;'>$". number_format($auxNumero,2) ."</th></tr>";
                      
                   if($_SESSION['Roll'] == "Sociolaboral")
                   {
                        $auxNumero = $auxHoras / ($DataG[14] * $Participantes->rowCount()) * 100;
                        echo "<th style='color: darkblue;'>".number_format($auxNumero,2)."%</th></tr>";
                   }
        ?>                     
        </table>
        </tr>
        </table>
