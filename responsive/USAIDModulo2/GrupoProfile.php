<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

unset($_SESSION['Ic']);

if(isset($_POST['ig']))
    $_SESSION['ig'] = $_POST['ig'];
    

$Select = $bddC->prepare("Select * from Objetivo2_Grupos where IdGrupo = " . $_SESSION['ig']);
$Select->execute();
$DataG = $Select->fetch();

?>

<div class="Raiz">
    <a href="?" class='flecha' style='text-decoration:none'>Principal -></a>
    <a href="?u=Group" class='flecha' style='text-decoration:none'>Grupo -></a>
    <a href="?u=GroupProfile" class='flecha' style='text-decoration:none'>Perfil de Grupo</a>
</div>
<div style="width: 100%; background-color: white; border-radius: 10px;">    
    <table style="width: 100%;">
    <tr><td style="text-align: center;" colspan="4"><h2>Perfil de Grupo</h2><hr color='skyblue' /></td></tr>
    <tr>
        <td  style="width: 75%;"colspan="3">
            <table style="width: 80%; margin-left: 10%;">
                <tr><td colspan="2"><h2>Datos del Grupo</h2></td></tr>
                <tr><td style="width: 35%;">Nombre del Programa:</td><td><?php echo $DataG[1];?></td></tr>
                <tr><td style="width: 35%;">Periodo:</td><td><?php echo $DataG[2] . "  a  " .$DataG[3];?></td></tr>
                <tr><td style="width: 35%;">Centro colaborador:</td><td><?php echo $DataG[4];?></td></tr>
                <tr><td style="width: 35%;">C�digo:</td><td><?php echo $DataG[5];?></td></tr>
                <tr><td style="width: 35%;">Departamento:</td><td><?php echo $DataG[6];?></td></tr>
                <tr><td style="width: 35%;">Municipio:</td><td><?php echo $DataG[7];?></td></tr>
                <tr><td style="width: 35%;">Beneficado:</td><td><?php echo $DataG[8];?></td></tr>
            </table>
        </td>
        <td style="vertical-align: text-top; width: 25%;">
        <?php if($_SESSION['Roll'] == "Sociolaboral"){?>
            <table>
                <tr><td><h2>Opciones</h2></td></tr>
                <form action="USAIDModulo2/GrupoABM.php" method="post">
                <tr><td><input type="submit" name="Enviar" class="boton" value="Modificar Grupo" /></td></tr>
                <tr><td><input type="submit" name="Enviar" class="botonE" value="Eliminar Grupo" onclick="return confirm('Estas seguro de eliminar este grupo, si lo haces borraras todo lo relacionado con el')" /></td></tr>
                <tr><td><input type="submit" name="Enviar" class="boton" value="Detalle de Grupo" /></td></tr>
                </form>                
            </table>
        <?php }?>        
        </td>
    </tr>    
    <tr><td colspan="4"><hr color='skyblue' style="width: 100%;" /><br /></td></tr>
    <tr><?php 
        if($_SESSION['Roll'] == "Sociolaboral")        
        {
            if(isset($_POST['Ip']))
            {
                $Abm = "Modificar";
                echo "Select * from Objetivo2_Participante where IdParticipante = " . $_POST['Ip'];
                $Participantes = $bddC->prepare("Select * from Objetivo2_Participante where IdParticipante = " . $_POST['Ip']);
                $Participantes->execute();
                $DataPa = $Participantes->fetch();
                
            }
            else
            {
                $Abm = "A�adir";
            }
        
        ?>
        <td colspan="2" style="width: 50%; vertical-align: text-top;" >            
            <table style="width: 90%; margin-left: 5%;">
            
            <form action="USAIDModulo2/GrupoABM.php" method="post">
            <?php
	                if(isset($_POST['Ip']))
                        echo "<input type='hidden' name='Ip' value='".$_POST['Ip']."' />";
            ?>
                <tr><td colspan="2"><h2>Formulario de Participante</h2></td></tr>
                <tr><td>Nombre del Participante:   </td><td><input type="text" name="Nombre" value="<?php echo $DataPa[2];?>"  required='true'/></td></tr>
                <tr><td>Apellidos del Participante:</td><td><input type="text" name="Apellido" value="<?php echo $DataPa[3];?>"  required='true'/></td></tr>
                <tr><td colspan="2" style="text-align: center;"> <input type="submit" name="Enviar" class="boton" value="<?php echo $Abm;?>" /></td></tr>        
            </form>
            </table>
        </td> 
        <?php }?>     
        <td colspan="2" style="width: 50%; vertical-align: text-top;">            
            <table style="width: 100%; padding: 5px;">
                <tr><td style="padding-left: 60px;" colspan="3"><strong><em><h2>Listado de Participantes</h2></em></strong></td></tr>
                <tr><td style="width: 10%;">No.</td><th style="width: 45%;">Nombre </th><th style="width: 45%;" colspan="2">Apellidos</th></tr>
                <tr>
                    <?php
	                       $Participantes = $bddC->prepare("Select * from Objetivo2_Participante where IdGrupo = " . $_SESSION['ig']);
                           $Participantes->execute();
                           
                           if($Participantes->rowCount() > 0)
                           {
                                $i = 0;
                               while($DataP = $Participantes->fetch())
                               {
                                    $i++;
                                    echo "<tr><th>$i</th><td>$DataP[2]</td><td>$DataP[3]</td>";
                                              
                                    if($_SESSION['Roll'] == "Sociolaboral")
                                    {
                                        echo "<td>
                                                  <form action='USAIDModulo2/GrupoABM.php' method='post'> 
                                                    <input type='hidden' name='Ip' value='$DataP[0]' />
                                                    <input type='submit' name='Enviar' value='x' class='botonE' style='width: 30px' onclick='return confirm(\"Desea borrar este Registro, si lo hace borrara todas sus asistencias y su informaci�n relacionada\")' />
                                                  </form>
                                              </td>  
                                              <td>
                                                  <form action='#' method='post'> 
                                                    <input type='hidden' name='Ip' value='$DataP[0]' />
                                                    <input type='submit' name='Enviar' value='M' class='boton' style='width: 30px'  />                                                    
                                                  </form>
                                              </td>
                                              </tr>";
                                    }    
                                    
                                    echo "</tr>";
                               }
                           }
                           else
                                echo "<tr><td colspan='3' style='color: red; text-align:center;'>No hay Participantes Registrados</td></tr>";
                    ?>
                </tr>
            </table>
            <br />
        </td>
    <?php if($_SESSION['Roll'] == "Sociolaboral"){?>  
    </tr>
    <tr><td colspan="4"><hr color='skyblue' style="width: 100%;" /><br /></td></tr>
    <tr>
        <td colspan="2">
            <h2 style="padding-left: 60px;">Nuevo Curso</h2>
            <table style="width: 100%;">
            <form action="USAIDModulo2/GrupoABM.php" method="post">
                <tr>
                    <td style="width: 50%;" class="tdleft">Nombre para el Curso:   </td>
                    <td><input type="text" name="Nombre" value="" required='true' /></td>
                </tr>
                <tr>
                    <td style="width: 50%;" class="tdleft">Horas de Aprobaci�n:</td>
                    <td><input type="number" name="Hminimas" value="" required='true' min="0" max="1000" /></td>
                </tr>
                <tr>
                    <td style="width: 50%;" class="tdleft">Horas Totales :</td>
                    <td><input type="number" name="Htotales" value="" required='true' min="0" max="1000" /></td>
                </tr>
                <tr><td colspan="2" style="text-align: center;"> <input type="submit" name="Enviar" class="boton" value="Crear" /></td></tr>        
            </form>
            </table>        
        </td>
    <?php }?> 
        <td colspan="2" style="vertical-align: text-top;">
            <h2 style="padding-left: 60px;">Listado de Cursos</h2>
            <table style="width: 100%; text-align: center;">
                <tr><th>Curso</th><th>Horas Minimas</th><th>Horas Totales</th></tr>
                <?php
	                   
                       $Cursos = $bddC->prepare("Select * from Objetivo2_Cursos where IdGrupo = " . $_SESSION['ig']);
                       $Cursos->execute();
                       
                       if($Cursos->rowCount() > 0)
                       {
                            while($DataC = $Cursos->fetch())
                            {
                                echo "<tr><td>$DataC[2]</td><td>$DataC[3]</td><td>$DataC[4]</td>
                                          <form action='USAIDModulo2/GrupoABM.php' method='post'>
                                          <input type='hidden' name='Ic' value='$DataC[0]' />";
                                if($_SESSION['Roll'] == "Sociolaboral")
                                    echo "<td><input type='submit' name='Enviar' value='X' class='botonE' style='width: 30px' onclick='return confirm(\"Desea borrar este Registro, si lo hace borrara todas sus asistencias y su informaci�n relacionada\")' /></td>";
                                    
                                echo "    </form>
                                      </tr>";
                            }
                            echo "<tr><td></td>
                                      <td><br />
                                           <a href='?u=Course' ><input type='submit' value='Ver Detalles' class='boton' /></a>
                                      </td>
                                 </tr>";
                       }
                       else
                            echo "<tr><td colspan='3' sytle='text-align: center;'><em style='color: red'>No hay Cursos Registrados</em></td></tr>";
                ?>
            </table>
        </td>
    </tr>
    </table>    
</div>
