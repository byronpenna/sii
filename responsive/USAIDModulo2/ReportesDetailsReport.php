<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 * 
 * 
 * 
 */
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=20 Horas Detalles del Grupo.xls");
    header('Content-Transfer-Encoding: binary');
    
    require '../net.php';
    $FechaInicio = $_POST["code2"];
    $FechaFin = $_POST["code3"];  
    $IdGrupo = $_POST['ig'];
    $IdCurso = $_POST['Ic'];
    
	
    $Query = "SELECT c . *  FROM Objetivo2_Jornadas AS j
              INNER JOIN Objetivo2_Cursos AS c ON c.IdCurso = j.IdCurso
              INNER JOIN Objetivo2_Grupos AS g ON g.IdGrupo = c.IdGrupo
              WHERE Fecha BETWEEN  '$FechaInicio' AND  '$FechaFin' AND g.IdGrupo = $IdGrupo and c.IdCurso = $IdCurso 
              GROUP BY IdCurso
              ORDER BY j.Fecha ASC";
                           
    $Cursos = $bddC->prepare($Query);
    $Cursos->execute();  
    $ArrayH = array();
    $ArrayP = array();
    $ArrayT = array();
    
    
    $j = 0;
    
    $Participantes = $bddC->prepare("Select * from Objetivo2_Participante where IdGrupo = $IdGrupo");
    $Participantes->execute();
    
    while($DataP = $Participantes->fetch())
    {
        $ArrayD[$j] = 0;
        $ArrayP[$j] = 0;
        $ArrayT[$j] = 0;
        
        $j++;
    }
    
    
    
    if($Cursos->rowCount() > 0)
    {
        while($DataC = $Cursos->fetch())
        {       
            $Select = $bddC->prepare("Select * from Objetivo2_Grupos as g 
                                      inner join Objetivo2_Cursos as c on g.IdGrupo = c.IdGrupo
                                      where c.IdCurso = $DataC[0]");
            $Select->execute();
            $DataG = $Select->fetch();            
            $ArrayHoras = array();
            
              $Query = "SELECT g.* FROM Objetivo2_Jornadas AS j
                          INNER JOIN Objetivo2_Cursos AS c ON c.IdCurso = j.IdCurso
                          INNER JOIN Objetivo2_Grupos AS g ON g.IdGrupo = c.IdGrupo
                          WHERE Fecha BETWEEN  '$FechaInicio' AND  '$FechaFin' AND g.IdGrupo = $IdGrupo 
                          Group by g.IdGrupo
                          ORDER BY j.Fecha ASC";
              
              $jornadasG = $bddC->prepare($Query);
              $jornadasG->execute();
              $dataJ = $jornadasG->fetch()
?>  
<table style="width: 100%;font-size: small;z-index: 1" >
<tr><th colspan="7">FUNDACION SALVADOR DEL MUNDO</th>      </tr>                  
<tr><th colspan="7">PROYECTO "Educaci�n para la Ni�ez y Juventud"</th></tr>
<tr><th colspan="7">OBJETIVO II</th></tr>
<tr><th colspan="7">CONSOLIDADO DE ASISTENCIAS</th></tr>
<tr><th colspan="7">Curso: <?php echo $DataG[12];?></th></tr>												
<tr><th colspan="7">Grupo: <?php echo $dataJ[1] . " - " .  $dataJ[8];?></th></tr>
<tr><th colspan="7">Periodo: <?php echo $FechaInicio . " al " . $FechaFin;?></th></tr>

                <?php
                           $Jornada = $bddC->prepare(" SELECT j. * FROM Objetivo2_Jornadas AS j
                                                       INNER JOIN Objetivo2_Cursos AS c ON c.IdCurso = j.IdCurso
                                                       INNER JOIN Objetivo2_Grupos AS g ON g.IdGrupo = c.IdGrupo
                                                       WHERE Fecha BETWEEN  '$FechaInicio' AND  '$FechaFin' AND j.IdCurso = $DataC[0] 
                                                       ORDER BY j.Fecha ASC");
                           $Jornada->execute();
                           $aux = $Jornada->rowCount();   
                ?>
                <tr>
                    <th rowspan="2" style="width: 60px; table-layout: fixed;" >N�</th>
                    <th rowspan="2" style="padding: 5px" >Nombre del/la joven</th>
                    <th colspan="<?php echo $aux?>">Jornadas</th>
                    <th rowspan="2">Total Horas <br />Asistidas</th>
                    <?php if($_SESSION['Roll'] == "Sociolaboral"){?>
                        <th rowspan="2">Porcentaje <br />de Asistencia</th>
                    <?php }?>
                    
                    <?php if($_SESSION['Roll'] == "Contabilidad"){?>
                        <th rowspan="2">Valor <br />por hora</th>
                        <th rowspan="2">Valor <br />a Liquidar</th>
                    <?php }?>                                
                </tr>
                <tr>
                    <?php
                           if($Jornada->rowCount() > 0)
                           {
                               $ArrayJ = array(); 
                               $i = 0;
                               while($DataJ = $Jornada->fetch())
                               {
                                    array_push($ArrayJ,$DataJ[0]);
                                    $i++;
                                    
                                    if($_SESSION['Roll'] == "Sociolaboral")
                                    echo "<th style='padding: 10px'>$DataJ[3]<br />
                                          <form action='USAIDModulo2/JornadaABM.php' method='post'>
                                              <input type='hidden' value='$DataJ[0]' name='ij' />
                                              <input type='submit' value='$DataJ[2]' name='Enviar' style='background-color: transparent; color: darkblue; border-color: transparent;' />
                                          </form>
                                          </th>";
                                          
                                    else
                                        echo "<th style='padding: 10px'>$DataJ[3]<br />$DataJ[2]</th>";
                               }
                           }
                           else
                                echo "<th style='color: red; text-align:center;'>No hay Jornadas <br /> Registradas</th>";
                        ?>
                </tr> 
                 <?php
                   $Participantes = $bddC->prepare("Select * from Objetivo2_Participante where IdGrupo = $IdGrupo");
                   $Participantes->execute();
                   
                   
                   if($Participantes->rowCount() > 0)
                   {
                       $i = 0;
                       $j = 0;
                       
                       while($DataP = $Participantes->fetch())
                       {
                            $k = 0;
                            $i++;
                            $thoras = 0;
                            $porcentaje = 0;
                            
                            $detalle = "<tr style='height: 30px;'><td>$i</td><td>$DataP[2], $DataP[3]</td>";
                            
                            if (!empty($ArrayJ)) 
                            {
                                foreach($ArrayJ as $value)
                                {
                                    $Query = "Select j.Horas from Objetivo2_Asistencia as a
                                              inner join Objetivo2_Jornadas as j on a.IdJornada = j.IdJornada
                                              where a.IdJornada = $value and a.IdParticipante = $DataP[0]
                                              ORDER BY j.Fecha ASC";
                                              
                                    $Asistencia = $bddC->prepare($Query);
                                    $Asistencia->execute();
                                    
                                    if($Asistencia->rowCount() > 0)
                                    {
                                        $DataA = $Asistencia->fetch();
                                        $detalle .= "<th>$DataA[0]</th>";
                                        $thoras += $DataA[0];
                                        $ArrayHoras[$k] += $DataA[0];
                                        
                                    }
                                    else
                                    {
                                        $detalle .= "<th>0</th>";
                                        $ArrayHoras[$k] += 0;
                                    }
                                        
                                        
                                    $k++;    
                                }                                    
                            }
                            else
                                $detalle .= "<th>-</th>";  
                                                                                                      
                            $porcentaje = $thoras / $DataG[14] * 100;
                            
                            
                            
                            if($_SESSION['Roll'] == "Sociolaboral")
                                $detalle .= "<th>$thoras</th><th>" . number_format($porcentaje,2) ."%</th>";
                            
                            if($_SESSION['Roll'] == "Contabilidad")
                            {
                                if($DataG[12] == "Refuerzo Pedag�gico")
                                {
                                    $Mult = 2;
                                    if($thoras >= 20)
                                        $AuxDinero = 40;
                                                                            
                                    else
                                        $AuxDinero = $thoras * $Mult;                                                                            
                                }
                                else
                                {
                                    $Mult = 4;
                                    $AuxDinero = $thoras * $Mult;
                                }

                                $detalle .= "<th>$thoras</th><th>$" . number_format($Mult,2) ."</th><th>$". number_format($AuxDinero,2);                                                                                                
                            }
                            $detalle .= "</tr>"; 
                            
                            if($_POST['Enviar'] == "Exportar 20 Horas")
                            {
                                if($thoras >= 20)
                                    echo "$detalle";
                            }        
         
                            $ArrayH[$j] += $thoras;
                            $ArrayP[$j] += $porcentaje;
                            $ArrayT[$j] += $DataG[14];
                            $j++;

                       }
                   }
                   else
                        echo "<tr><td colspan='3' style='color: red; text-align:center;'>No hay Participantes Registrados</td></tr>";
                   
                   echo "<tr><td colspan='2' style='text-align: center;'>Totales</td>";
                   
                   //$auxHoras = 0;
//                   foreach($ArrayHoras as $value)
//                   {
//                        $auxHoras += $value;
//                        echo "<th style='color: darkblue;'>$value</th>";
//                   }
//                   
//                   $auxNumero = $auxHoras * $Mult;
//                   echo "<th style='color: darkblue;'>$auxHoras</th>";
//                   
//                   if($_SESSION['Roll'] == "Contabilidad")
//                        echo "<th style='color: darkblue;'>$". number_format($Mult,2) ."</th><th style='color: darkblue;'>$". number_format($auxNumero,2) ."</th></tr>";                   
        ?>                     
</table>
<?php
	        }
    }
?>