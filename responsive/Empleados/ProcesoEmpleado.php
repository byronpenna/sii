<?php
    
    if(isset($_GET['l']))
    {
        if($_GET['l'] != "EvaluationExecute")
        {
            if($_GET['l'] != "ResultArea")
            {
                echo "<div style='float:left; width: 20%;'class='col-sm-8 col-sm-offset-2 text'>";
                include ('Menu/menu.php');
                echo "</div>";   
            }
        }
         
       	if(isset($_GET['n']))
    	{
    		if($_GET['n'] == 1)
    			echo "<div id='Notificacion' name='Notificacion' style='color:green; text-align: right'>Datos Ingresados Exitosamente!</div>";
    			
    		if($_GET['n'] == 2)
    			echo "<div id='Notificacion' name='Notificacion' style='color:blue; text-align: right'>Datos Actualizados Exitosamente!</div>";
                
    		if($_GET['n'] == 3)
    			echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right'>Datos Eliminados Exitosamente...</div>";
                
    		if($_GET['n'] == 4)
    			echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right'>Acceso Denegado...</div>"; 
    
    		if($_GET['n'] == 5)
    			echo "<div id='Notificacion' name='Notificacion' style='color:red; text-align: right'>Error a sucesido un problema, comuniquese con los administradores del sistema...</div>";                                   
    	}
        
        
        
        
        /** Datos de Información **/    

        /** 1- Información Personal **/
        if($_GET['l'] == "PersonalData")
            include("Empleados/DatosInformacion/DatosPersonales.php"); 
   
        else if($_GET['l'] == "PersonalForm")
            include("Empleados/DatosInformacion/DatosPersonalesForm.php");
            
            
        /** 2- Información Familiar **/
        if($_GET['l'] == "FamiliarData")
            include("Empleados/DatosInformacion/DatosFamiliares.php"); 
   
        else if($_GET['l'] == "FamiliarForm")
            include("Empleados/DatosInformacion/DatosFamiliaresForm.php");    
            
            
        /** 3- Información Academicos **/
        if($_GET['l'] == "AcademicData")
            include("Empleados/DatosInformacion/DatosAcademicos.php"); 
   
        else if($_GET['l'] == "AcademicForm")
            include("Empleados/DatosInformacion/DatosAcademicosForm.php");
   
            
        /** 4- Información Laborales **/
        if($_GET['l'] == "EmploymentData")
            include("Empleados/DatosInformacion/DatosLaborales.php"); 
   
        else if($_GET['l'] == "EmploymentForm")
            include("Empleados/DatosInformacion/DatosLaboralesForm.php");  
            
            
        /** 5- Información de Salud **/
        if($_GET['l'] == "HealthData")
            include("Empleados/DatosInformacion/DatosSalud.php"); 
   
        else if($_GET['l'] == "HealthForm")
            include("Empleados/DatosInformacion/DatosSaludForm.php");    
            
            
        /** 6- Información Religiosa **/
        if($_GET['l'] == "ReligiousData")
            include("Empleados/DatosInformacion/DatosReligiosos.php"); 
   
        else if($_GET['l'] == "ReligiousForm")
            include("Empleados/DatosInformacion/DatosReligiososForm.php");       
        
        /** 6- Información de Documentos **/    
        if($_GET['l'] == "DocumentsData")
            include("Empleados/DatosInformacion/Documentos.php"); 
   
        else if($_GET['l'] == "DocumentsForm")
            include("Empleados/DatosInformacion/DocumentosForm.php");  
            
        else if($_GET['l'] == "Download")
            include("Empleados/DatosInformacion/DocumentosDescarga.php");                                                                                                
            
            
            
        /** Administración **/
        
        /** Busqueda de Empleados **/
        else if($_GET['l'] == "SearchEmployee")
            include("Empleados/Administracion/BusquedaEmpleado.php");   
            
        else if($_GET['l'] == "EmployeeProfile")
            include("Empleados/Administracion/PerfilEmpleado.php");   
            
            
        /** Areas de FUSALMO **/
        else if($_GET['l'] == "Areas")
            include("Empleados/Administracion/Areas.php");

        else if($_GET['l'] == "AreasList")
            include("Empleados/Administracion/AreasList.php");                              
        
        /** Cargos de Areas **/
        else if($_GET['l'] == "Position")
            include("Empleados/Administracion/CargosList.php");
        
        else if($_GET['l'] == "PositionForm")
            include("Empleados/Administracion/CargosForm.php"); 
            
        else if($_GET['l'] == "Asignation")
            include("Empleados/Administracion/CargosAsignacion.php");

        else if($_GET['l'] == "AsignationDetails")
            include("Empleados/Administracion/CargosAsignacionDetalle.php");

        else if($_GET['l'] == "DetailsActions")
            include("Empleados/Administracion/CargosAsignacionAccion.php");    
            
        else if($_GET['l'] == "DetailsUpdate")
            include("Empleados/Administracion/CargosAsignacionUpdate.php");  

        else if($_GET['l'] == "AsignationFinalize")
            include("Empleados/Administracion/CargosFinalizacion.php");
            
        else if($_GET['l'] == "Hierarchy")
            include("Empleados/Administracion/CargosJerarquia.php");                                                                                   
               
        /** Areas de FUSALMO **/
        else if($_GET['l'] == "Picture")
            include("Empleados/Administracion/FotoEmpleado.php"); 
            
        else if($_GET['l'] == "Info")
            include("Empleados/DatosInformacion/DatosInstitucionalesForm.php");
                        
        /** Procesos de Area **/
        else if($_GET['l'] == "MyArea")
            include("Empleados/ProcesosArea/MiArea.php");
            
        /**  1- Acciones de Personal **/
        else if($_GET['l'] == "PersonalAction")
            include("Empleados/ProcesosArea/AccionPersonal.php");
            
        else if($_GET['l'] == "PersonalActionForm")
            include("Empleados/ProcesosArea/AccionPersonalForm.php");    
            
        else if($_GET['l'] == "Ticket")
            include("Empleados/ProcesosArea/AccionPersonalBoleta.php"); 
            
        else if($_GET['l'] == "Doings")
            include("Empleados/ProcesosArea/AccionPersonalAdministration.php");    
            
            
        /**  2- Permisos **/                                                                                      
        else if($_GET['l'] == "Permissions")
            include("Empleados/ProcesosArea/PermisoList.php");
            
        else if($_GET['l'] == "PermissionsForm")
            include("Empleados/ProcesosArea/PermisoForm.php");  
            
        else if($_GET['l'] == "Schedule")
            include("Empleados/ProcesosArea/PermisoFormSpecial.php");
            
        else if($_GET['l'] == "PermissionsoDetails")
            include("Empleados/ProcesosArea/PermisoDetails.php");            
        
        
        /** Gestión de Permisos **/    
        else if($_GET['l'] == "PermissionsControl")
            include("Empleados/ProcesosArea/PermisoControl.php");
            
        else if($_GET['l'] == "PermissionsControlView")
            include("Empleados/ProcesosArea/PermisoControlView.php");  
            
        else if($_GET['l'] == "PermissionsEmployee")
            include("Empleados/ProcesosArea/PermisoEmployee.php");
            
        else if($_GET['l'] == "PermissionsEmployeeView")
            include("Empleados/ProcesosArea/PermisoEmployeeView.php");                                    
        
        else if($_GET['l'] == "PermissionsPenalty")
            include("Empleados/ProcesosArea/PermisoPenalty.php");                                    
                        
              
        /**  3- Evaluacion de Desempeño **/                                                                                      
        else if($_GET['l'] == "Evaluation")
            include("Empleados/Evaluacion/Evaluation.php");
            
        else if($_GET['l'] == "EvaluationView")
            include("Empleados/Evaluacion/EvaluationView.php");
                
        else if($_GET['l'] == "EvaluationExecute")
            include("Empleados/Evaluacion/EvaluationExecute.php");
            
        else if($_GET['l'] == "Result")
            include("Empleados/Evaluacion/EvaluationResult.php");            
                              
        else if($_GET['l'] == "AllEvaluations")
            include("Empleados/Evaluacion/EvaluationGlobalResult.php");            
                              
        else if($_GET['l'] == "MyEvaluations")
            include("Empleados/Evaluacion/EvaluationMyResult.php");   

        else if($_GET['l'] == "ResultArea")
            include("Empleados/Evaluacion/EvaluationAreaResult.php");     
            
        else if($_GET['l'] == "Comparation")
            include("Empleados/Evaluacion/EvaluationComparation.php");
            
        else if($_GET['l'] == "ManagementEvaluation")
            include("Empleados/Evaluacion/GestionEvaluacion.php");   
            
        else if($_GET['l'] == "ManagementAllocation")
            include("Empleados/Evaluacion/GestionEvaluacionAsignacion.php");
            
        else if($_GET['l'] == "AllocationEmployee")
            include("Empleados/Evaluacion/GestionEvaluacionAsignar.php");
            
        else if($_GET['l'] == "ResultStaff")
            include("Empleados/Evaluacion/ResultadoPersonal.php");     
                                     
        else if($_GET['l'] == "Institution")
            include("Empleados/Evaluacion/EvaluationInstitution.php");
            
        else if($_GET['l'] == "vote")
            include("Empleados/vote.php");
            
            

        /** Proyectos **/
        else if($_GET['l'] == "ProjectSummary")
            include("Empleados/ProjectSummary/ResumenList.php");
                        
        else if($_GET['l'] == "Summary")
            include("Empleados/ProjectSummary/Resumen.php");

        else if($_GET['l'] == "ProjectSummaryForm")
            include("Empleados/ProjectSummary/ResumenForm.php");
            
        else if($_GET['l'] == "Participants")
            include("Empleados/ProjectSummary/Participants.php");
            
        else if($_GET['l'] == "Comunity")
            include("Empleados/ProjectSummary/Comunity.php");    
            

        /** Requisiciones **/
        else if($_GET['l'] == "Donations")
            include("Empleados/Payment/Searching.php"); 

        /** Requisiciones **/
        else if($_GET['l'] == "Request")
            include("Empleados/Requisiciones/MyRequest.php");          
     else if($_GET['l'] == "Mapa")
            include("Empleados/Requisiciones/Mapa.php");                                             
            
            
     
     
                                                                                                
    }
    else
    {
        echo "<div style='float: left; width: 0%; margin-left: 0;' >";
        include ('Empleados/Menu/vertical/menu.php'); echo "</div><div style='float: right; width: 96%;'>"; 
        include("Empleados/DatosInstitucionales.php");
        echo" </div>";
    } 
        
    
    echo "<div class='clr'></div>";
?>