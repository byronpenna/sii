<?php
	
    include("NetPay.php")
        
?>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px"> 
    <table style="width: 100%;">
        <tr><td style="text-align: left;"><h2>Donaciones - Busqueda de transacciones</h2></td></tr>
        <tr>
            <td>
            <hr color='skyblue' />
                <br />
                <form action="#" method="post">
                <table style="width: 90%; ">
                    <tr>
                        <td style="width: 45%; text-align: right; padding-right: 20px;">N�mero de Referencia FUSALMO:</td>
                        <td><input type="text" name="iRef" value="" style="width: 100%;"  /></td>                        
                    </tr>
                </table>
                </form>
                <br />
            </td>
        </tr>
        <tr>
            <td>
            <hr color='skyblue' />
            <br />
            <h2>Donaciones - Informaci�n Donantes</h2>
            <?php
                    
                if(isset($_POST['iRef']))
                {
                    $Information = $bddp->prepare("Select * from InformacionCliente where IdInformacion = :iref");
                    $Information->bindParam(':iref',$_POST['iRef']);
                    $Information->execute();
                    
                    if($Information->rowCount() > 0)
                    {
                        $DataI = $Information->fetch(); 
                        
                        echo "<table style='width: 80%; margin-left: 10%'>
                                <tr>
                                    <td style='width: 35%'>Id Referencia FUSALMO</td>
                                    <td>$DataI[1]</td>
                                </tr>
                                <tr>
                                    <td>Nombres: </td>
                                    <td>$DataI[2]</td>
                                </tr>
                                <tr>
                                    <td>Apellidos:</td>
                                    <td>$DataI[3]</td>
                                </tr>  
                                <tr>
                                    <td>Correo:</td>
                                    <td>$DataI[4]</td>
                                </tr>         
                                <tr>
                                    <td>Pais:</td>
                                    <td>$DataI[5]</td>
                                </tr>     
                                <tr>
                                    <td>Monto Donado:</td>
                                    <td>$" . number_format($DataI[6],2). " </td>
                                </tr>     
                                <tr>
                                    <td>Metodo de Pago:</td>
                                    <td>$DataI[7]</td>
                                </tr> 
                                <tr>
                                    <td>Fecha de Transacci�n:</td>
                                    <td>$DataI[8]</td>
                                </tr>";
                                
                         if($DataI[7] == "paypal")                         
                            $Estado = "Verificar en plataforma de Paypal";  
                         
                         else
                            $Estado = $DataI[9];
                            
                         
                         
                         if($DataI[10] == "")
                            $NIT = "-";
                         
                         else
                            $NIT = $DataI[10];
                                
                          echo "<tr>
                                    <td>Estado:</td>
                                    <td>$Estado</td>
                                </tr>  
                                <tr>
                                    <td>NIT:</td>
                                    <td>$NIT</td>
                                </tr>                                                                                                 
                                                                                                                                                                                                                                             
                              </table>";                                
                          
                    }
                    else
                    {
                        echo "<center><em style='color: red'>No se han encontrado coincidencias</em><center>";
                    }                    
                }
                else
                    echo "<center><em style='color: green'>Esperando dato</em><center>";
            ?>
            </td>
        </tr>
    </table>
</div>