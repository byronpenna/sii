
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

        <meta charset="utf-8" />
      
        <!-- add styles -->
        <link href="Empleados/Menu/css/menu.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="container">
            <ul id="nav">
                <li><a  href="Empleado.php"><span><?php  $Datos = $nombreapellido->fetch(); echo "$Datos[0] $Datos[1]"; ?></span></a></li>
                <li><form method='POST' action='Main.php?l=UsuPer' name='UsuaPerfil'>
            <?php 
                $Datos=$nombreusuario->fetch();
                if($_SESSION["TipoUsuario"]=="Administrador")
                {
                        echo "<input type='hidden' name='idUsu' value='$IdEmp' />
                        <a href='#' onclick='javascript:document.UsuaPerfil.submit();'><span>Usuario: $Datos[0]</span></a></a>";
                }
                else                
                    echo "<a href='#'><span>Usuario: $Datos[0]</span></a>";
                                        
            ?>
            </form>      
            </li>
            <li><br /></li> 
            <li><a href="#" class="container"><span>Mi Area</span></a>
                <div><ul>
                    <span id="s1"></span>
                    <ul class="subs">
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=MyArea">Mis compañeros</a>
                        <li><a href="#">Requisiciones</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=PersonalAction">Acciones personales</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=Permissions">Notificaciones y servicios</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=ProjectSummary">Resumen de proyectos</a></li>
                        </li>
                        </li>
                    </ul>
                </li>
                <li><a href="#">Mis Datos</a>
                    <span id="s2"></span>
                    <ul class="subs">
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=PersonalData">Datos personales</a>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=FamiliarData">Datos familiares</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=AcademicData">Datos Academicos</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=EmploymentData">Datos Laborales</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=HealthData">Datos de salud</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=ReligiousData">Datos religiosos</a></li>                     
                        </li>
                    </ul>
                </li>
                <li><a href="http://siif.fusalmo.org/Empleado.php?l=DocumentsData">Mis documentos</a></li>
                <li><a href="#">Evaluaciones</a>
                <span id="s3"></span>
                    <ul class="subs">
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=MyEvaluations">Mis evaluaciones</a>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=Evaluation">Solicitud de evaluacion</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=ResultStaff">Resumen personal</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=AllEvaluations">Resultados de Area</a></li>                   
                        </li>
                    </ul>
                </li>
                <li><a href="#">Administración</a>
                  <span id="s4"></span>
                    <ul class="subs">
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=AreasList">Areas</a>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=ManagementEvaluation">Gestion de evaluaciones</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=Doings">Acciones</a></li>
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=PermissionsControl">Permisos</a></li>         
                        <li><a href="http://siif.fusalmo.org/Empleado.php?l=SearchEmployee">Buscar empleados</a></li>             
                        </li>
                    </ul>
                </li>  
                <li><a href="http://siif.fusalmo.org/Empleado.php?l=vote">Votación</a></li>
                <li><a href="http://siif.fusalmo.org/Empleado.php?l=Donations">Donaciones</a></li>
            </ul>
        </div>
    </body>
</html>