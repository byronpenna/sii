<html>
    <head>
    <!--Usando jquey-->
    <link type="text/css" href="Empleados/Menu/menu.css" rel="stylesheet" />
     <meta http-equiv="content-type" content="text/html" charset="ISO-8859-1"/>
	<script type="text/javascript" src="Empleados/Menu/jquery.js"></script>
	<script type="text/javascript" src="Empleados/Menu/menu.js"></script>       
	<script type="text/javascript">
    $(document).ready(function(){
    $("body div:last").remove();
    });
    </script>
    <?php
            if($_SESSION["TipoUsuario"] == "Joven")            
                Redireccion("Main.php"); 
            

    		$IdEmp = $_SESSION["IdUsuario"];

			//Obteniendo nombre, apellido y nombre de usuario del empleado con Id=$IdEmp.
            $nombreapellido = $bddr->prepare("SELECT Nombre1,Apellido1 from Empleado where IdEmpleado = '$IdEmp'");
            $nombreapellido->execute();

            $nombreusuario = $bdd->prepare("SELECT NombreUsuario from usuario where Id = '$IdEmp'");
            $nombreusuario->execute();           
     ?>
    </head>
    <body>        
	<div id="menu">
		<ul class="menu">
        	<li><a href="Empleado.php"><span><?php  $Datos = $nombreapellido->fetch(); echo "$Datos[0] $Datos[1]"; ?></span></a></li>
            <li>
            <form method='POST' action='Main.php?l=UsuPer' name='UsuaPerfil'>
            <?php 
                $Datos=$nombreusuario->fetch();
                if($_SESSION["TipoUsuario"]=="Administrador")
                {
                        echo "<input type='hidden' name='idUsu' value='$IdEmp'/>
                        <a href='#' onclick='javascript:document.UsuaPerfil.submit();'><h3>Usuario: $Datos[0]</h3></a></a>";
                }
                else                
                    echo "<a href='#'><span>Usuario: $Datos[0]</span></a>";
                                        
            ?>
            </form>      
            </li>
            <li><br /></li>    
            <li><a href="#" class="parent"><span>Mi Area</span></a>
				<div><ul>
					<li>
                    	<form method='post' action='Empleado.php?l=MyArea' name="MyArea">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.MyArea.submit();'><span>Mis Compañeros</span></a>
						</form>
                    </li>
					<li>
                    	<form method='post' action='Empleado.php?l=Request' name="Requisiciones">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.Requisiciones.submit();'><span>Requisiciones</span></a>
						</form>	
                    </li>                    
                    <li>
                    	<form method='post' action='Empleado.php?l=PersonalAction' name="AccPer">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.AccPer.submit();'><span>Acciones Personales</span></a>
						</form>
                    </li>
					<li>
                    	<form method='post' action='Empleado.php?l=Permissions' name="PermisoPedir">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.PermisoPedir.submit();'><span>Notificaciones / Permisos</span></a>
						</form>	
                    </li>
					<li>
                    	<form method='post' action='Empleado.php?l=ProjectSummary' name="ProjectSummary">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.ProjectSummary.submit();'><span>Resumen de Proyectos</span></a>
						</form>	
                    </li>                    
				</ul></div>
			</li>        
			<li><a href="#" class="parent"><span>Mis Datos</span></a>
				<div>
                <ul>
					<li>
                    	<form method='post' action='Empleado.php?l=PersonalData' name="DatPer">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.DatPer.submit();'><span>Datos Personales</span></a>
						</form>	
                    </li>
                    <li>
                    	<form method='post' action='Empleado.php?l=FamiliarData' name="DatFam">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.DatFam.submit();'><span>Datos Familiares</span></a>
						</form>
                    </li> 
					<li>
                    	<form method='post' action='Empleado.php?l=AcademicData' name="DatAca">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.DatAca.submit();'><span>Datos Acad&eacute;micos</span></a>
						</form>                  
                    </li>                                                           
					<li>
                    	<form method='post' action='Empleado.php?l=EmploymentData' name="DatLab">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.DatLab.submit();'><span>Datos Laborales</span></a>
						</form>	
                    </li>
                    <li>
                    	<form method='post' action='Empleado.php?l=HealthData' name="DatSal">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.DatSal.submit();'><span>Datos de Salud</span></a>
						</form>
                    </li>
                    <li>
                    	<form method='post' action='Empleado.php?l=ReligiousData' name="DatRel">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.DatRel.submit();'><span>Datos Religiosos</span></a>
						</form>
                    </li>                 
				</ul>
                </div>
			</li>
            <li>
            	<form method='post' action='Empleado.php?l=DocumentsData' name="Doc">
						<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                        <a href='#' onclick='javascript:document.Doc.submit();'><span>Mis Documentos</span></a>
				</form>
            </li>   
            <li><a href='#' class="parent"><span>Evaluaciones</span></a>            
                <div>
                <ul>
                    <li>
                        <form method='post' action='Empleado.php?l=MyEvaluations' name="MyEva">
						        <input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.MyEva.submit();'><span>Mis Evaluaciones</span></a>
				        </form>
                    </li>
                    <li>
                        <form method='post' action='Empleado.php?l=Evaluation' name="Eva">
						        <input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.Eva.submit();'><span>Solicitudes de Evaluación</span></a>
				        </form>
                    </li>                                        
					
                    <li>
                    	<form method='post' action='Empleado.php?l=ResultStaff' name="personal">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.personal.submit();'><span>Resumen Personal</span></a>
						</form>
                    </li>                                                               
					
                    <li>
                    	<form method='post' action='Empleado.php?l=AllEvaluations' name="Todas">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.Todas.submit();'><span>Resultados de Areas</span></a>
						</form>
                    </li> 
                </ul>
                </div>
            </li>                  			
            <li><br /></li>
            <?php if($_SESSION["TipoUsuario"]=="Administrador" || $_SESSION["TipoUsuario"]=="RRHH" || $_SESSION["IdUsuario"]== 4896){?>
			<li><a href="#" class="parent"><span>Administración</span></a>
				<div>
                <ul>
					<li>
                    	<form method='post' action='Empleado.php?l=AreasList' name="Areas">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.Areas.submit();'><span>&Aacute;reas</span></a>
						</form>
                    </li>
                    <?php if($_SESSION["IdUsuario"] != 5301){?>  
					<li>
                    	<form method='post' action='Empleado.php?l=ManagementEvaluation' name="Gestion">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.Gestion.submit();'><span>Gestión de Evaluaciones</span></a>
						</form>
                    </li>  
                    <?php }?>                                  
					<li>
                    	<form method='post' action='Empleado.php?l=Doings' name="Acciones">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.Acciones.submit();'><span>Acciones</span></a>
						</form>	
                    </li>
					<li>
                    	<form method='post' action='Empleado.php?l=PermissionsControl' name="Permisos">
								<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                                <a href='#' onclick='javascript:document.Permisos.submit();'><span>Permisos</span></a>
						</form>	
                    </li>                    
                    <li>
                    	<form method='post' action='Empleado.php?l=SearchEmployee' name="Busqueda">
        					<input type='hidden' name='IdEmp' value='<?php echo $IdEmp; ?>'/>
                      		<a href='#' onclick='javascript:document.Busqueda.submit();'><span>Buscar Empleado</span></a>
        				</form>	
                    </li>                                 
				</ul>
                </div>
			</li>                                                  
            <?php }?>
            <?php if($_SESSION["TipoUsuario"]=="Administrador" || $_SESSION["TipoUsuario"]=="RRHH" || $_SESSION["IdUsuario"]== 4896){?> 
            <li><a href="Empleado.php?l=vote" ><span>Votación</span></a>
            <?php }?>   
            
            <?php if($_SESSION["TipoUsuario"]=="Administrador" || $_SESSION["IdUsuario"]== 2596){?> 
            <li><a href="Empleado.php?l=Donations" ><span>Donaciones</span></a>
            <?php }?>                         
		</ul>
	</div>
    </body>
</html>