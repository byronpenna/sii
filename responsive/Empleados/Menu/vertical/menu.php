<!doctype html>
<html lang=''>
<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="Empleados/Menu/vertical/styles.css">
   <script src="Empleados/Menu/vertical/jquery-latest.min.js" type="text/javascript"></script>
   <script src="Empleados/Menu/vertical/script.js"></script>
   <title>SIIFMENU</title>
    <?php
            if($_SESSION["TipoUsuario"] == "Joven")            
                Redireccion("Main.php"); 
            

            $IdEmp = $_SESSION["IdUsuario"];

            //Obteniendo nombre, apellido y nombre de usuario del empleado con Id=$IdEmp.
            $nombreapellido = $bddr->prepare("SELECT Nombre1,Apellido1 from Empleado where IdEmpleado = '$IdEmp'");
            $nombreapellido->execute();

            $nombreusuario = $bdd->prepare("SELECT NombreUsuario from usuario where Id = '$IdEmp'");
            $nombreusuario->execute();           
     ?>
</head>
<body>

<div id='cssmenu'>
<ul>
   <li><a  href="Empleado.php"><span><?php  $Datos = $nombreapellido->fetch(); echo "$Datos[0] $Datos[1]"; ?></span></a></li>
                <li><form method='POST' action='Main.php?l=UsuPer' name='UsuaPerfil'>
            <?php 
                $Datos=$nombreusuario->fetch();
                if($_SESSION["TipoUsuario"]=="Administrador")
                {
                        echo "<input type='hidden' name='idUsu' value='$IdEmp' />
                        <a href='#' onclick='javascript:document.UsuaPerfil.submit();'><span>Usuario: $Datos[0]</span></a></a>";
                }
                else                
                    echo "<a href='#'><span>Usuario: $Datos[0]</span></a>";
                                        
            ?>
            </form>      
            </li>
   <li class='active has-sub'><a href='#'>Mi Area</a>
      <ul>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=MyArea">Mis compa&ntildeeros</a>
                        <li><a href="#">Requisiciones</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=PersonalAction">Acciones personales</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=Permissions">Notificaciones y servicios</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=ProjectSummary">Resumen de proyectos</a></li>
         </li>
      </ul>
   </li>
   <li class='active has-sub'><a href='#'>Mis datos</a>
      <ul>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=PersonalData">Datos personales</a>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=FamiliarData">Datos familiares</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=AcademicData">Datos Academicos</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=EmploymentData">Datos Laborales</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=HealthData">Datos de salud</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=ReligiousData">Datos religiosos</a></li>
         </li>
      </ul>
   </li>
   <li><a href="http://siif.fusalmo.org/Empleado.php?l=DocumentsData">Mis documentos</a></li>
   <li class='active has-sub'><a href='#'>Evaluaciones</a>
      <ul>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=MyEvaluations">Mis evaluaciones</a>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=Evaluation">Solicitud de evaluacion</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=ResultStaff">Resumen personal</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=AllEvaluations">Resultados de Area</a></li>
         </li>
      </ul>
   </li>
   <li class='active has-sub'><a href='#'>Administraci&oacuten</a>
      <ul>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=AreasList">Areas</a>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=ManagementEvaluation">Gestion de evaluaciones</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=Doings">Acciones</a></li>
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=PermissionsControl">Permisos</a></li>         
                        <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=SearchEmployee">Buscar empleados</a></li>
         </li>
      </ul>
   </li>
   <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=vote">Votaci&oacuten</a></li>
   <li><a href="http://responsive.siifusalmo.org/Empleado.php?l=Donations">Donaciones</a></li>
</ul>
</div>

</body>
<html>
