<?php

/**
 * @author Manuel Calder�n
 * @copyright 2013
 */


  require '../../net.php';
    
    if(isset($_POST['Enviar']))
    {    
        if($_POST['Enviar'] == "Guardar")
        {
            $AddArea = $bddr->prepare("Insert into AreasDeTrabajo values (Null, :NombreArea, :Abreviacion)");
            $AddArea->bindParam(':NombreArea', $_POST['Area']);
            $AddArea->bindParam(':Abreviacion', $_POST['Abreviatura']);
            $AddArea->execute();
            
            Redireccion("../../Empleado.php?l=AreasList&n=1");
        }
        
        else if($_POST['Enviar'] == "Modificar")
        {
            $_SESSION["IdA"] = $_POST['IdA']; 
            Redireccion("../../Empleado.php?l=Areas"); 
        }   
        else if($_POST['Enviar'] == "Actualizar")
        {
            $UpdateArea = $bddr->prepare("Update AreasDeTrabajo set 
                                          NombreAreaDeTrabajo = :NombreArea,
                                          Abrevacion = :Abreviacion
                                          where IdAreaDeTrabajo = :IdA");
                                          
            $UpdateArea->bindParam(':IdA', $_POST['IdA']);                                          
            $UpdateArea->bindParam(':NombreArea', $_POST['Area']);
            $UpdateArea->bindParam(':Abreviacion', $_POST['Abreviatura']);
            $UpdateArea->execute();
            
            Redireccion("../../Empleado.php?l=AreasList&n=2");
        }    
        
        else if($_POST['Enviar'] == "Eliminar")
        {
            $UpdateArea = $bddr->prepare("Delete from AreasDeTrabajo where IdAreaDeTrabajo = :IdA");                                          
            $UpdateArea->bindParam(':IdA', $_POST['IdA']);                                          
            $UpdateArea->execute();
            
            Redireccion("../../Empleado.php?l=AreasList&n=3");
        }            
        
        else if($_POST['Enviar'] == "Cargos")
        {
            $_SESSION["IdA"] = $_POST['IdArea']; 
            Redireccion("../../Empleado.php?l=Position"); 
        }                  
    }
    Redireccion("../../Empleado.php?l=Areas&n=4");
?>