<?php

/**
 * @author Manuel Calder�n
 * @copyright 2013
 */
 
  require '../../net.php';
    
  if(isset($_POST['Enviar']))
  {    
      if($_POST['Enviar'] == "Guardar")
      {
          $AddArea = $bddr->prepare("Insert into Cargos values (Null, :Cargo, :Detalles, :IdA)");
          $AddArea->bindParam(':Cargo', $_POST['Cargo']);
          $AddArea->bindParam(':Detalles', $_POST['Detalles']);
          $AddArea->bindParam(':IdA', $_POST['IdA']);
          $AddArea->execute();

          Redireccion("../../Empleado.php?l=Position&n=1");
      }
      
      else if($_POST['Enviar'] == "Asignar") 
      {          
          $AddPosition = $bddr->prepare("Insert into CargosAsignacion values (Null, :IdP, :IdE, :TipoContrato, :Financiamiento, :Salario, :FechaIni, '0000-00-00')");
          $AddPosition->bindParam(':IdP', $_POST['IdPosition']);
          $AddPosition->bindParam(':IdE', $_POST['IdEmp']);
          $AddPosition->bindParam(':TipoContrato', $_POST['TipoContrato']);
          $AddPosition->bindParam(':Financiamiento', $_POST['Financiamiento']);
          $AddPosition->bindParam(':Salario', $_POST['Salario']);
          $AddPosition->bindParam(':FechaIni', $_POST['FechaIni']);                            
          $AddPosition->execute();

          Redireccion("../../Empleado.php?l=Position&n=1");         
      }
      
      else if($_POST['Enviar'] == "Eliminar") 
      {
          $DeletePosition = $bddr->prepare("Delete from CargosAsignacion where IdAsignacion = :IdAC");
          $DeletePosition->bindParam(':IdAC', $_POST['IdAC']);
          $DeletePosition->execute();

          Redireccion("../../Empleado.php?l=Position&n=3");         
      }          
      else if($_POST['Enviar'] == "Actualizar") 
      {
          $_SESSION["IdAC"] = $_POST['IdAC'];     
          Redireccion("../../Empleado.php?l=DetailsUpdate");         
      }     
      
      else if($_POST['Enviar'] == "Modificar") 
      {
          $UpdatePosition = $bddr->prepare("Update CargosAsignacion set 
                                            TipoContrato = :TipoContrato, 
                                            Financiamiento = :Financiamiento,
                                            Salario = :Salario,
                                            FechaInicio = :FechaIni
                                            where IdAsignacion = :IdAC");

          $UpdatePosition->bindParam(':TipoContrato', $_POST['TipoContrato']);
          $UpdatePosition->bindParam(':Financiamiento', $_POST['Financiamiento']);
          $UpdatePosition->bindParam(':Salario', $_POST['Salario']);
          $UpdatePosition->bindParam(':FechaIni', $_POST['FechaIni']);
          $UpdatePosition->bindParam(':IdAC', $_POST['IdAC']);                                      
          $UpdatePosition->execute();          

          $UpdatePosition = $bddr->prepare("Update Cargos set 
                                            Cargo = :Cargo
                                            where IdCargos = :IdC");
                                            
          $UpdatePosition->bindParam(':Cargo', $_POST['Cargo']);
          $UpdatePosition->bindParam(':IdC', $_POST['IdC']);
          $UpdatePosition->execute();                
          
          unset($_SESSION["IdAC"]);
          $_SESSION["IdC"] = $_POST['IdC'];
          
          Redireccion("../../Empleado.php?l=DetailsActions&n=2");         
      }
      
      else if($_POST['Enviar'] == "Finalizar Cargo") 
      {
          $_SESSION["IdAC"] = $_POST['IdAC'];     
          Redireccion("../../Empleado.php?l=AsignationFinalize");         
      }    
      else if($_POST['Enviar'] == "Cancelar") 
      {
          unset($_SESSION["IdAC"]);     
          Redireccion("../../Empleado.php?l=Position");         
      }
      
      else if($_POST['Enviar'] == "Finalizar") 
      {
          $UpdatePosition = $bddr->prepare("Update CargosAsignacion set 
                                            FechaFin = :fin
                                            where IdAsignacion = :IdAC");
                                            
          $UpdatePosition->bindParam(':fin', $_POST['FechaFin']);
          $UpdatePosition->bindParam(':IdAC', $_POST['IdAC']);
          $UpdatePosition->execute();    
          
          Redireccion("../../Empleado.php?l=Position");         
      }    
      
      else if($_POST['Enviar'] == "Cambiar Empleado") 
      {
           $_SESSION["IdAC"] = $_POST['IdAC'];
           $_SESSION["IdC"] = $_POST['IdPosition'];    
           
          Redireccion("../../Empleado.php?l=Asignation");         
      } 
      
      else if($_POST['Enviar'] == "Cambiar") 
      {
          $UpdatePosition = $bddr->prepare("Update CargosAsignacion set 
                                            IdEmpleado = :IdEmp
                                            where IdAsignacion = :IdCA");
                                            
          $UpdatePosition->bindParam(':IdEmp', $_POST['IdEmp']);
          $UpdatePosition->bindParam(':IdCA', $_SESSION["IdAC"]);
          $UpdatePosition->execute();    
          
          unset($_SESSION["IdAC"]);
          unset($_SESSION["IdC"]);
          
          Redireccion("../../Empleado.php?l=Position");          
      }    
      
      else if($_POST['Enviar'] == "Asignar como Jefe") 
      {
          $JerarquiaPosition = $bddr->prepare("Insert into Jerarquia values (null, :IdPosition, :IdBoss)");
                                            
          $JerarquiaPosition->bindParam(':IdPosition', $_POST['IdPosition'] );
          $JerarquiaPosition->bindParam(':IdBoss', $_POST['IdBoss']);          
          $JerarquiaPosition->execute();    
          
          $_SESSION["IdC"] = $_POST['IdPosition'];
           
          
           
          Redireccion("../../Empleado.php?l=DetailsActions&n=1");         
      }     
      else if($_POST['Enviar'] == "X") 
      {
          $DeletePosition = $bddr->prepare("Delete from Jerarquia where IdJerarquia = :IdJ");
          $DeletePosition->bindParam(':IdJ', $_POST['IdJ']);
          $DeletePosition->execute();

          Redireccion("../../Empleado.php?l=DetailsActions&n=3");                  
      }  
      
      else if($_POST['Enviar'] == "Eliminar Cargo") 
      {
          $DeletePosition = $bddr->prepare("Delete from Cargos where IdCargos = :IdP");
          $DeletePosition->bindParam(':IdP', $_POST['IdPosition']);
          $DeletePosition->execute();

          Redireccion("../../Empleado.php?l=Position&n=3");                  
      } 
      
      else if($_POST['Enviar'] == "Actualizar Cargo") 
      { 
          $_SESSION["IdPosition"] = $_POST['IdPosition'];  
          Redireccion("../../Empleado.php?l=PositionForm");                  
      }
      
      else if($_POST['Enviar'] == "Modificar Cargo") 
      {           

          $UpdatePosition = $bddr->prepare("Update Cargos set
                                            Cargo = :Cargo,
                                            Detalle = :Detalles 
                                            Where IdCargos = " . $_POST['IdPosition']);
                                            
          $UpdatePosition->bindParam(':Cargo', $_POST['Cargo']);
          $UpdatePosition->bindParam(':Detalles', $_POST['Detalles']);
          $UpdatePosition->execute();
          
          Redireccion("../../Empleado.php?l=DetailsActions&n=3");                   
      }      
      
             
      
                                                                                   
  }
  Redireccion("../../Empleado.php?l=Position&n=4");
?>
