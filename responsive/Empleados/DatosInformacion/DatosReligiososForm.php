<script>
	$(document).ready(function(){
	//Opci�n para deshabilitar y habilitar checks y text seg�n la religi�n seleccionada.
			$('#Religion').change(function(){
				var OpcionSeleccionada = $(this).find('option:selected');//Obteniendo valor del 
				var Valor = $(OpcionSeleccionada).val();//<option> seleccionado.
				if(Valor=='Cristiano Cat�lico')
				{
					$("#Catolico1").slideDown("fast");
					$("#Catolico2").slideDown("fast");
					$("#Catolico3").slideDown("fast");
					$("#Catolico4").slideDown("fast");
					$("#Catolico5").slideDown("fast");
					$("#Catolico6").slideDown("fast");
					$("#Catolico7").slideDown("fast");
					
					$("#Catolico1").children().prop('disabled',false);
					$("#Catolico2").children().prop('disabled',false);
					$("#Catolico3").children().prop('disabled',false);
					$("#Catolico4").children().prop('disabled',false);
					$("#Catolico5").children().prop('disabled',false);
					$("#Catolico6").children().prop('disabled',false);
					$("#Catolico7").children().prop('disabled',false);
					
					 //Habilitar o deshabilitar confesi�n.
					 if($('#Sacramento1').is(':checked')) 
					 {	
					 	$("#Sacramento2").slideDown("fast");
						$("#Sacramento2").prop('disabled',false);
					 } 
					 else
					 {
					  	$("#Sacramento2").slideUp("fast");
					  	$("#Sacramento2").prop('disabled',true);
					 }
					 
					 //Habilitar o deshabilitar primera comuni�n.
					 if($('#Sacramento2').is(':checked')) 
					 {	
					 	$("#Sacramento3").slideDown("fast");
						$("#Sacramento3").prop('disabled',false);
					 } 
					 else
					 {
					  	$("#Sacramento3").slideUp("fast");
					  	$("#Sacramento3").prop('disabled',true);
					 }
					 
					 //Habilitar o deshabilitar confirmaci�n.
					 if($('#Sacramento3').is(':checked')) 
					 {	
					 	$("#Sacramento4").slideDown("fast");
						$("#Sacramento4").prop('disabled',false);
					 } 
					 else
					 {
					  	$("#Sacramento4").slideUp("fast");
					  	$("#Sacramento4").prop('disabled',true);
					 }
					 
					 //Habilitar o deshabilitar matrimonio, unci�n de los enfermos y orden sacerdotal.
					 if($('#Sacramento4').is(':checked')) 
					 {	
					 	$("#Sacramento5").slideDown("fast");
						$("#Sacramento5").prop('disabled',false);
						$("#Sacramento6").slideDown("fast");
						$("#Sacramento6").prop('disabled',false);
						$("#Sacramento7").slideDown("fast");
						$("#Sacramento7").prop('disabled',false);
					 } 
					 else
					 {
					  	$("#Sacramento5").slideUp("fast");
					  	$("#Sacramento5").prop('disabled',true);
						$("#Sacramento6").slideUp("fast");
					  	$("#Sacramento6").prop('disabled',true);
						$("#Sacramento7").slideUp("fast");
					  	$("#Sacramento7").prop('disabled',true);
					 }
					 
					 
					 if($('#Sacramento5').is(':checked') || $('#Sacramento7').is(':checked'))
					{ 
						//Habilitar o deshabilitar matrimonio.
						 if($('#Sacramento7').is(':checked')) 
						 {	
							$("#Sacramento5").slideUp("fast");
							$("#Sacramento5").prop('disabled',true);
						 } 
						 else
						 {
							
							$("#Sacramento5").slideDown("fast");
							$("#Sacramento5").prop('disabled',false);
						 }
						 
						 //Habilitar o deshabilitar orden sacerdotal.
						 if($('#Sacramento5').is(':checked')) 
						 {	
							$("#Sacramento7").slideUp("fast");
							$("#Sacramento7").prop('disabled',true);
						 } 
						 else
						 {
							
							$("#Sacramento7").slideDown("fast");
							$("#Sacramento7").prop('disabled',false);
						 }
					}
						 
				}
				else
				{
					$("#Catolico1").slideUp("fast");
					$("#Catolico2").slideUp("fast");
					$("#Catolico3").slideUp("fast");
					$("#Catolico4").slideUp("fast");
					$("#Catolico5").slideUp("fast");
					$("#Catolico6").slideUp("fast");
					$("#Catolico7").slideUp("fast");
					
					$("#Catolico1").children().prop('disabled',true);
					$("#Catolico2").children().prop('disabled',true);
					$("#Catolico3").children().prop('disabled',true);
					$("#Catolico4").children().prop('disabled',true);
					$("#Catolico5").children().prop('disabled',true);
					$("#Catolico6").children().prop('disabled',true);
					$("#Catolico7").children().prop('disabled',true);
				}
			}).change();
		//Opci�n para deshabilitar y habilitar checks seg�n el sacramento cumplido.
			//Si cumple Bautismo se libera Confesi�n.
			$('#Sacramento1').click(function(){
			  if($(this).is(':checked')) 
			  {	$("#Sacramento2").slideDown("fast");
				$("#Sacramento2").prop('disabled',false);} 
			 	else{
				  $("#Sacramento2").prop('checked',false);
				  $("#Sacramento2").slideUp("fast");
				  $("#Sacramento2").prop('disabled',true);
				  $("#Sacramento3").prop('checked',false);
				  $("#Sacramento3").slideUp("fast");
				  $("#Sacramento3").prop('disabled',true);
				  $("#Sacramento4").prop('checked',false);
				  $("#Sacramento4").slideUp("fast");
				  $("#Sacramento4").prop('disabled',true);
				  $("#Sacramento5").prop('checked',false);
				  $("#Sacramento5").slideUp("fast");
				  $("#Sacramento5").prop('disabled',true);
				  $("#Sacramento6").prop('checked',false);
				  $("#Sacramento6").slideUp("fast");
				  $("#Sacramento6").prop('disabled',true);
				  $("#Sacramento7").prop('checked',false);
				  $("#Sacramento7").slideUp("fast");
				  $("#Sacramento7").prop('disabled',true);}
			});
			
			//Si cumple Confesi�n se libera Primera Comuni�n.
			$('#Sacramento2').click(function(){
			  if($(this).is(':checked')) 
			  {	$("#Sacramento3").slideDown("fast");
				$("#Sacramento3").prop('disabled',false);} 
			  else{
				  $("#Sacramento3").prop('checked',false);
				  $("#Sacramento3").slideUp("fast");
				  $("#Sacramento3").prop('disabled',true);
				  $("#Sacramento4").prop('checked',false);
				  $("#Sacramento4").slideUp("fast");
				  $("#Sacramento4").prop('disabled',true);
				  $("#Sacramento5").prop('checked',false);
				  $("#Sacramento5").slideUp("fast");
				  $("#Sacramento5").prop('disabled',true);
				  $("#Sacramento6").prop('checked',false);
				  $("#Sacramento6").slideUp("fast");
				  $("#Sacramento6").prop('disabled',true);
				  $("#Sacramento7").prop('checked',false);
				  $("#Sacramento7").slideUp("fast");
				  $("#Sacramento7").prop('disabled',true);}
			});
			
			//Si cumple Primera Comuni�n se libera Confirmaci�n.
			$('#Sacramento3').click(function(){
			  if($(this).is(':checked')) 
			  {	$("#Sacramento4").slideDown("fast");
				$("#Sacramento4").prop('disabled',false);} 
			  else{
				  $("#Sacramento4").prop('checked',false);
				  $("#Sacramento4").slideUp("fast");
				  $("#Sacramento4").prop('disabled',true);
				  $("#Sacramento5").prop('checked',false);
				  $("#Sacramento5").slideUp("fast");
				  $("#Sacramento5").prop('disabled',true);
				  $("#Sacramento6").prop('checked',false);
				  $("#Sacramento6").slideUp("fast");
				  $("#Sacramento6").prop('disabled',true);
				  $("#Sacramento7").prop('checked',false);
				  $("#Sacramento7").slideUp("fast");
				  $("#Sacramento7").prop('disabled',true);}
			});
			
			//Si cumple Confirmaci�n se libera Matrimonio, Unci�n de los enfermos y Orden Sacerdotal.
			$('#Sacramento4').click(function(){
			  if($(this).is(':checked')) 
			  {	$("#Sacramento5").slideDown("fast");
				$("#Sacramento5").prop('disabled',false);
				$("#Sacramento6").slideDown("fast");
				$("#Sacramento6").prop('disabled',false);
				$("#Sacramento7").slideDown("fast");
				$("#Sacramento7").prop('disabled',false);}
			  else{
				  $("#Sacramento5").prop('checked',false);
				  $("#Sacramento5").slideUp("fast");
				  $("#Sacramento5").prop('disabled',true);
				  $("#Sacramento6").prop('checked',false);
				  $("#Sacramento6").slideUp("fast");
				  $("#Sacramento6").prop('disabled',true);
				  $("#Sacramento7").prop('checked',false);
				  $("#Sacramento7").slideUp("fast");
				  $("#Sacramento7").prop('disabled',true);}
			});		
			
			//Si cumple Orden Sacerdotal se bloquea Matrimonio.
			$('#Sacramento7').click(function(){
			  if($('#Sacramento7').is(':checked')) 
					 {	
					 	$("#Sacramento5").slideUp("fast");
					  	$("#Sacramento5").prop('disabled',true);
					 } 
					 else
					 {
					  	
						$("#Sacramento5").slideDown("fast");
						$("#Sacramento5").prop('disabled',false);
					 }
			});
					
					 
			//Si cumple Matrimonio se bloquea Orden Sacerdotal.
			$('#Sacramento5').click(function(){
					 if($('#Sacramento5').is(':checked')) 
					 {	
					 	$("#Sacramento7").slideUp("fast");
					  	$("#Sacramento7").prop('disabled',true);
					 } 
					 else
					 {
					  	
						$("#Sacramento7").slideDown("fast");
						$("#Sacramento7").prop('disabled',false);
					 }		
			});
				
		
	});
</script>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">
		<form action="Empleados/DatosInformacion/DatosReligiososABM.php" method="post">
<?php
		$GetDatos=$bddr->prepare('SELECT * FROM DatosReligiosos WHERE IdEmpleado_Fk=\''. $IdEmp.'\'');//Obteniendo datos religiosos del empleado.
		$GetDatos->execute();
		if($GetDatos->rowCount()>0)
		{    
			$Datos=$GetDatos->fetch();        
			$ABM="Actualizar";
            
            echo "<input type='hidden' name='IdR' value='$Datos[0]' />";	
		}
		else
		{
			$ABM="Guardar";
		}
?>	
		<table  class='Datos' style="padding: 10px; width: 100%;">
            <tr>
                <th colspan="2" style="text-align: center;"><h2>Datos Religiosos<hr color='#0099FF'></h2></th>
            </tr>
			<tr>
				<td style='text-align: right'>Religi&oacute;n que practica:</td>
				<td>
                <select id="Religion" name="Religion">
					<option value='Cristiano Cat�lico' <?php if('Cristiano Cat�lico'==$Datos[1]) echo 'selected=\'selected\''; ?>>Cristiano Cat&oacute;lico</option>
					<option value='Cristiano Evang�lico' <?php if('Cristiano Evang�lico'==$Datos[1]) echo 'selected=\'selected\''; ?>>Cristiano Evang&eacute;lico</option>
					<option value='Otros' <?php if('Otros'==$Datos[1]) echo 'selected=\'selected\''; ?>>Otros</option>
                    <option value='No Pratico' <?php if('No Pratico'==$Datos[1]) echo 'selected=\'selected\''; ?>>No Pratico</option>
                    
                </select>
                </td>
			</tr>  
					
			<tr>
				<td style='text-align: right'><div id="Catolico7">Sacramentos:</div></td>
				<td style=' padding-left: 3%;' >    
				<div id="Catolico1">	
						<input type='checkbox' id='Sacramento1' name='Sacramento1' value='Bautismo' <?php if('Bautismo'==$Datos[4]) echo 'checked=\'checked\''; ?>/><label for="Sacramento1">Bautismo</label><br />
						<input type='checkbox' id='Sacramento2' name='Sacramento2' value='Confesi�n' <?php if('Confesi�n'==$Datos[5]) echo 'checked=\'checked\''; ?>/><label for="Sacramento2">Confesi&oacute;n</label><br />
						<input type='checkbox' id='Sacramento3' name='Sacramento3' value='Primera Comuni�n' <?php if('Primera Comuni�n'==$Datos[6]) echo 'checked=\'checked\''; ?>/><label for="Sacramento3">Primera Comuni&oacute;n</label><br />
						<input type='checkbox' id='Sacramento4' name='Sacramento4' value='Confirmaci�n' <?php if('Confirmaci�n'==$Datos[7]) echo 'checked=\'checked\''; ?>/><label for="Sacramento4">Confirmaci&oacute;n</label><br />
						<input type='checkbox' id='Sacramento5' name='Sacramento5' value='Matrimonio' <?php if('Matrimonio'==$Datos[8]) echo 'checked=\'checked\''; ?>/><label for="Sacramento5">Matrimonio</label><br />
						<input type='checkbox' id='Sacramento6' name='Sacramento6' value='Unci�n de los enfermos' <?php if('Unci�n de los enfermos'==$Datos[9]) echo 'checked=\'checked\''; ?>/><label for="Sacramento6">Unci&oacute;n de los enfermos</label><br />
						<input type='checkbox' id='Sacramento7' name='Sacramento7' value='Orden Sacerdotal' <?php if('Orden Sacerdotal'==$Datos[10]) echo 'checked=\'checked\''; ?>/><label for="Sacramento7">Orden Sacerdotal</label><br />  
				</div>
                </td>
			</tr>
			<tr>
			    <th colspan='2'><div id="Catolico2">Si pertenece a alg&uacute;n grupo religioso llene lo siguiente.</div></th>
			</tr>   
			        
			<tr>
			    <td style='text-align: right; width:250px'><div id="Catolico3">Grupo religioso:</div></td>
				<td style=' padding-left: 3%; color: red;'><div id="Catolico4"><input type="text" name="GrupoReligioso" value="<?php echo $Datos[2] ?>"/></div></td>
			</tr>
			<tr>
				<td style='text-align: right; width:250px'><div id="Catolico5">Direcci&oacute;n del grupo:</div></td>
				<td style=' padding-left: 3%; color: red;'><div id="Catolico6"><input type="text" name="Direccion" value="<?php echo $Datos[3] ?>"/></div></td>
			</tr>
			<tr>
				<th colspan="2"><input class="boton" type="submit" value="<?php echo $ABM?>" name="Enviar" title="Guardar datos." /></th>
			</tr>
            <tr>
				<th colspan="2">
					<font size="-1">Todos los campos en <font color="#FF0000">rojo</font> deben ser llenados.</font>
                </th>
			</tr>
			</table>
</form>
</div>