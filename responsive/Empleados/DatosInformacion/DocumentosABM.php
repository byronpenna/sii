<?php

/**
 * @author Manuel Calder�n
 * @copyright 2013
 */

  require '../../net.php';
    
    if(isset($_POST['Enviar']))
    {    
        if($_POST['Enviar'] == "Subir")
        {   
                    // obtenemos los datos del archivo
            $tamano = $_FILES["archivo"]['size'];
            $tipo = $_FILES["archivo"]['type'];
            $archivo = $_FILES["archivo"]['name'];
            $prefijo = substr(md5(uniqid(rand())),0,6);  
            $destino =  $_SERVER['DOCUMENT_ROOT']."/Empleados/DatosInformacion/Documentos/".$prefijo."_".$archivo;
            
            if(copy($_FILES['archivo']['tmp_name'],$destino))
            {                
                $AddFile = $bddr->prepare("Insert into Documentos values (Null, '" .$_POST['IdEmp']."', '".$_POST['nombre']."' ,'" . $prefijo."_".$archivo."')");
                $AddFile->execute();
                
                if(isset($_POST['Portal']))
                    Redireccion("http://oportunidades.fusalmo.org/Main.php?fusalmo=Files&n=1");
                    
                else
                    Redireccion("../../Empleado.php?l=DocumentsData&n=1");
            }            
            else
                Redireccion("../../Empleado.php?l=DocumentsData&n=5");
        }
        if($_POST['Enviar'] == "X" || $_POST['Enviar'] == "Eliminar Archivo")
        {  
            $File = $bddr->prepare("Select * from Documentos where IdDocumento = ". $_POST['IdD']);
            $File->execute();
            $borrar = $File->fetch(); 
            
            $destino =  $_SERVER['DOCUMENT_ROOT']."/Empleados/DatosInformacion/Documentos/";            
            
            if(!unlink($destino . $borrar[3]))
                Redireccion("../../Empleado.php?l=DocumentsData&n=5");
            
            $DeleteFile = $bddr->prepare("Delete from Documentos where IdDocumento = ". $_POST['IdD']);
            $DeleteFile->execute();
            
            if(isset($_POST['Portal']))
                    Redireccion("http://oportunidades.fusalmo.org/Main.php?fusalmo=Files&n=3");
                    
            else
                    Redireccion("../../Empleado.php?l=DocumentsData&n=3");            
            
        }        
    }
    Redireccion("../../Empleado.php?l=DocumentsData&n=4");
?>