<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px;">
<?php
		
	$GetDatos=$bddr->prepare("SELECT * FROM Empleado WHERE IdEmpleado = " . $IdEmp);//Obteniendo datos personales del empleado.
    $GetDatos->execute();
    
    if($Datos=$GetDatos->fetch())               
       	$ABM="Actualizar";       
    
    else    
        $ABM="Guardar";
    
?>

<form action="Empleados/DatosInformacion/DatosPersonalesABM.php" name="FormEmpleado" method="POST">
    <table class='MiForm'   style=" padding: 10px; width: 100%;">
        <tr>            
            <th colspan="2" ><h2>Informaci&oacute;n Personal.</h2>
            <hr color="#0099FF" />           
            </th>
        </tr>            
		<tr>
			<td class='TDDerecha'><label>Primer Nombre:</label></td>
			<td class='TDIzquierda'><input required="true" title="Primer Nombre" type="text" name="Nombre1" size="25" value="<?php echo "$Datos[1]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Segundo Nombre:</label></td>
			<td class='TDIzquierda'><input title="Segundo Nombre" type="text" name="Nombre2" size="25" value="<?php echo "$Datos[2]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Tercer Nombre:</label></td>
			<td class='TDIzquierda'><input  title="Tercer Nombre" type="text" name="Nombre3" size="25" value="<?php echo "$Datos[3]";?>" /></td>
		</tr>            
		<tr>
			<td class='TDDerecha'><label>Primer apellido:</label></td>
			<td class='TDIzquierda'><input required="true" title="Primer Apellido" type="text" name="Apellido1" size="25" value="<?php echo "$Datos[4]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Segundo apellido:</label></td>
			<td class='TDIzquierda'><input required="true" title="Segundo Apellido" type="text" name="Apellido2" size="25" value="<?php echo "$Datos[5]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Sexo:</label></td>
			<td class='TDIzquierda'>
                <select name="Sexo" style="width: 150px;">
                <?php if($ABM=="Actualizar"){//Seleccionar el g�nero correspondiente al de la BD.?>
                    <option <?php if($Datos[12]=="F")echo"selected='true'";?> value="F" >Femenino</option> 
                    <option <?php if($Datos[12]=="M")echo"selected='true'";?>value="M">Masculino</option>                        
                 <?php }else{?>
                    <option selected='true' value="F" >Femenino</option> 
                    <option value="M">Masculino</option>  
                 <?php }?>                        
                </select>
            </td>
		</tr>
        <tr>
            <td class='TDDerecha'><label>Fecha de nacimiento:</label></td>
			<td style='TDIzquierda'>
                <table>
                    <tr>
                   	    <td style='font-size:10pt;'>A&ntilde;o</td>
                       	<td style='font-size:10pt;'>Mes</td>
                       	<td style='font-size:10pt;'>D&iacute;a</td>
                    </tr>
                    <tr>
                    <td>
                        <select name='Anio'>
              <?php //Obteniendo valores de fechas desde la BD usando substr().
                for($i = 1900; $i <= (date('Y')-18); $i++)
                {
                    if(  $i == substr($Datos[11],0,4))
                        echo "          <option selected='true' value='$i'>$i</option>";
                    else
                        echo "          <option value='$i'>$i</option>";                                                
                }
              ?>
                        </select>
                    </td>
                    <td>
                        <select name='Mes' >   
                        <option <?php if(substr($Datos[11],5,2)=="01")echo "selected='true'";?> value='01'>Enero</option>                             
                        <option <?php if(substr($Datos[11],5,2)=="02")echo "selected='true'";?> value='02'>Febrero</option>
                        <option <?php if(substr($Datos[11],5,2)=="03")echo "selected='true'";?> value='03'>Marzo</option>
                        <option <?php if(substr($Datos[11],5,2)=="04")echo "selected='true'";?> value='04'>Abril</option>
                        <option <?php if(substr($Datos[11],5,2)=="05")echo "selected='true'";?> value='05'>Mayo</option>
                        <option <?php if(substr($Datos[11],5,2)=="06")echo "selected='true'";?> value='06'>Junio</option>
                        <option <?php if(substr($Datos[11],5,2)=="07")echo "selected='true'";?> value='07'>Julio</option>
                        <option <?php if(substr($Datos[11],5,2)=="08")echo "selected='true'";?> value='08'>Agosto</option>
                        <option <?php if(substr($Datos[11],5,2)=="09")echo "selected='true'";?> value='09'>Septiembre</option>
                        <option <?php if(substr($Datos[11],5,2)=="10")echo "selected='true'";?> value='10'>Octubre</option>
                        <option <?php if(substr($Datos[11],5,2)=="11")echo "selected='true'";?> value='11'>Noviembre</option>
                        <option <?php if(substr($Datos[11],5,2)=="12")echo "selected='true'";?> value='12'>Diciembre</option>
                        </select>
                    </td> 
                    <td>
                        <select name='Dia' > 
                        <?php
                            for($i = 1; $i <= 31; $i++)
                            {
                                if($i==substr($Datos[11],8,2))
                                    echo "<option selected='true' value='$i'>$i</option>"; 
                                else
                                    echo "<option value='$i'>$i</option>";                                               
                            }
                        ?>                               
                        </select>
                    </td>                                 
                    </tr>
                </table>                 
            </td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>DUI:</label></td>
			<td class='TDIzquierda'><input type="text" required="required" placeholder="99999999-9" name="DUI" size="25" value="<?php echo "$Datos[15]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Direcci�n de residencia:</label></td>
			<td class='TDIzquierda'><input type="text" required="required" name="Direccion" size="25" value="<?php echo "$Datos[6]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Municipio:</label></td>
			<td class='TDIzquierda'><input type="text" required="required" name="Municipio" size="25" value="<?php echo "$Datos[7]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Departamento:</label></td>
			<td class='TDIzquierda'><input type="text" required="required" name="Departamento" size="25" value="<?php echo "$Datos[8]";?>" />
                
            </td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Tel&eacute;fono Fijo:</label></td>
			<td class='TDIzquierda'><input title="9999-9999 Ext: 9999" type="text" placeholder="9999-9999 Ext: 9999" name="TelefonoFijo" size="25" value="<?php echo "$Datos[9]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Tel&eacute;fono Celular:</label></td>
			<td class='TDIzquierda'><input required="required" title="9999-9999 Ext: 9999" type="text" placeholder="9999-9999 Ext: 9999" name="TelefonoCelular" size="25" value="<?php echo "$Datos[10]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Estatura:</label></td>
			<td class='TDIzquierda'><input type="text" placeholder="En metros" name="Estatura" size="25" value="<?php echo "$Datos[22]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Peso:</label></td>
			<td class='TDIzquierda'><input type="text" placeholder="En libras" name="Peso" size="25" value="<?php echo "$Datos[23]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Estado Civil:</label></td>
			<td class='TDIzquierda'>
                <select name="EstadoCivil">
                    <option <?php if($Datos[13]=="Soltero")echo"selected='true'";?> value='Soltero'>Soltero</option>
                    <option <?php if($Datos[13]=="Acompa�ado")echo"selected='true'";?> value='Acompa�ado'>Acompa�ado</option>
                    <option <?php if($Datos[13]=="Casado")echo"selected='true'";?> value='Casado'>Casado</option>
                    <option <?php if($Datos[13]=="Divorciado")echo"selected='true'";?> value='Divorciado'>Divorciado</option>
                    <option <?php if($Datos[13]=="Viudo")echo"selected='true'";?> value='Viudo'>Viudo</option>
                </select>
            </td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Nacionalidad:</label></td>
			<td class='TDIzquierda'><input type="text" name="Nacionalidad" size="25" required value="<?php echo "$Datos[14]";?>"/></td>
		</tr>    
        <tr>
			<td class='TDDerecha'><label>AFP:</label></td>
			<td class='TDIzquierda'><input type="text" name="AFP" size="25" value="<?php echo "$Datos[18]";?>" /></td>
		</tr>     
        <tr>
			<td class='TDDerecha'><label>N�mero de AFP:</label></td>
			<td class='TDIzquierda'><input type="text" name="NumAFP" size="25" value="<?php echo "$Datos[19]";?>" />
            </td>
		</tr>   
        <tr>
			<td class='TDDerecha'><label>N�mero de Afiliaci&oacute;n del ISSS:</label></td>
			<td class='TDIzquierda'><input type="text" name="ISSS" size="25" value="<?php echo "$Datos[20]";?>" /></td>
		</tr>
        <tr>
			<td class='TDDerecha'><label>NIT:</label></td>
			<td class='TDIzquierda'><input type="text" name="NIT" size="25" value="<?php echo "$Datos[21]";?>" /></td>
		</tr>        
		<tr>
			<td class='TDDerecha'><label>Profesi&oacute;n:</label></td>
			<td class='TDIzquierda'><input type="text" required="required" name="Profesion" size="25" value="<?php echo "$Datos[17]";?>" /></td>
		</tr>
		<tr>
			<td class='TDDerecha'><label>Correo:</label></td>
			<td class='TDIzquierda'><input type="text" placeholder="micorreo@fusalmo.org" name="Correo" size="25" value="<?php echo "$Datos[16]";?>"/></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center;"><input class="boton" type="submit" name="Enviar" value="<?php echo $ABM ?>"/></td>
		</tr>
        <tr>
			<td colspan="2" style="text-align: center;"><font size="-1">Todos los campos en <font color="#FF0000">rojo</font> deben ser llenados.</font></td>
		</tr>
   </table>   
</form>
</div>