<?php

/**
 * @author Manuel Calder�n
 * @copyright 2013
 */

    require '../../net.php';
    
    if(isset($_POST['Enviar']))
    {    
        if($_POST['Enviar'] == "Guardar")
        {
            $Cadena= '\''.$_SESSION["IdUsuario"].'\',
                      \''.$_POST['Nombre1'].'\',
                      \''.$_POST['Nombre2'].'\',
                      \''.$_POST['Nombre3'].'\',
                      \''.$_POST['Apellido1'].'\',
                      \''.$_POST['Apellido2'].'\',
                      \''.$_POST['Direccion'].'\',
                      \''.$_POST['Municipio'].'\',
                      \''.$_POST['Departamento'].'\',
                      \''.$_POST['TelefonoFijo'].'\',
                      \''.$_POST['TelefonoCelular'].'\',
                      \''.$_POST['Anio'].'-'.$_POST['Mes'].'-'.$_POST['Dia'].'\',
                      \''.$_POST['Sexo'].'\',
                      \''.$_POST['EstadoCivil'].'\',
                      \''.$_POST['Nacionalidad'].'\',
                      \''.$_POST['DUI'].'\',
                      \''.$_POST['Correo'].'\',
                      \''.$_POST['Profesion'].'\',
                      \''.$_POST['AFP'].'\',
                      \''.$_POST['NumAFP'].'\',
                      \''.$_POST['ISSS'].'\',
                      \''.$_POST['NIT'].'\',
                      \''.$_POST['Estatura'].'\',
                      \''.$_POST['Peso'].'\''; 

            $ABM="Guardar";  
            $IdEmp = $_SESSION["IdUsuario"];                          
        }
        
        else if($_POST['Enviar'] == "Actualizar")
        {
            $Cadena =   'Nombre1=\''.$_POST['Nombre1'].'\',
                         Nombre2=\''.$_POST['Nombre2'].'\',
                         Nombre3=\''.$_POST['Nombre3'].'\',
						 Apellido1=\''.$_POST['Apellido1'].'\',
                         Apellido2=\''.$_POST['Apellido2'].'\',
                         Direccion=\''.$_POST['Direccion'].'\',
						 Municipio=\''.$_POST['Municipio'].'\',
                         Departamento=\''.$_POST['Departamento'].'\',
						 Telefonofijo=\''.$_POST['TelefonoFijo'].'\',
                         TelefonoCelular=\''.$_POST['TelefonoCelular'].'\',
 	  					 FechaNacimiento=\''.$_POST['Anio'].'-'.$_POST['Mes'].'-'.$_POST['Dia'].'\',
                         Genero=\''.$_POST['Sexo'].'\',
						 EstadoCivil=\''.$_POST['EstadoCivil'].'\',
                         Nacionalidad=\''.$_POST['Nacionalidad'].'\',
						 DUI=\''.$_POST['DUI'].'\',
                         Correo=\''.$_POST['Correo'].'\',
                         Profesion=\''.$_POST['Profesion'].'\',
                         NombreAFP=\''.$_POST['AFP'].'\',
						 NumeroAFP=\''.$_POST['NumAFP'].'\',
                         NumAfISSS=\''.$_POST['ISSS'].'\',
                         NIT=\''.$_POST['NIT'].'\',
                         Estatura=\''.$_POST['Estatura'].'\',
						 Peso=\''.$_POST['Peso'].'\'';
                         
            $ABM="Actualizar"; 
            $IdUpdate = $_SESSION["IdUsuario"];
        }
        
        /** Datos de Tabla **/
        $Id       = "IdEmpleado";
        $Tabla    = "Empleado";
        $Retornar = "?l=PersonalData";        

        include("ABM.php");
    }
    Redireccion("../../Empleado.php?l=PersonalData&n=4");
?>