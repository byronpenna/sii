<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">

    <form action="Empleados/DatosInformacion/DatosAcademicosABM.php" method="post">
<?php
        
        
		$GetDatos=$bddr->prepare('SELECT * FROM DatosAcademicos WHERE IdEmpleado_Fk=\''. $IdEmp.'\' AND IdDatoAcademico=\''.$_SESSION["IdA"].'\'');//Obteniendo datos académicos del empleado.
		$GetDatos->execute();
		if($GetDatos->rowCount()>0)
		{    
			$Datos=$GetDatos->fetch();        
			$ABM="Actualizar";
            
            echo "<input type='hidden' name='IdA' value='".$_SESSION["IdA"]."' />";
		}
		else
		{
			$ABM="Guardar";
		}
?>

        <table border="0" align="center" width="100%">
            <tr>
            	<th colspan="2" style="text-align: center;"><h2>Datos Acad&eacute;micos<hr color='#0099FF'/> </h2></th>
            </tr>
            <tr>
            	<td style='text-align: right;'>Nivel de estudio:</td>
            	<td>
                    <select name="Nivel">
                        <option value="Educación Básica" <?php if('Educación Básica'==$Datos[1])echo 'selected=\'selected\''; ?>>Educaci&oacute;n B&aacute;sica</option>                        
                        <option value="Educación Media" <?php if('Educación Media'==$Datos[1])echo 'selected=\'selected\''; ?>>Educaci&oacute;n Media</option>
                        <option value="Educación Superior" <?php if('Educación Superior'==$Datos[1])echo 'selected=\'selected\''; ?>>Educaci&oacute;n Superior</option> 
                        <option value="Diplomados" <?php if('Diplomados'==$Datos[1])echo 'selected=\'selected\''; ?>>Diplomados</option>
                        <option value="Cursos" <?php if('Cursos'==$Datos[1])echo 'selected=\'selected\''; ?>>Cursos</option>                                               
                    </select>
                </td>
            </tr>
            <tr>
            	<td style='text-align: right;'>Instituci&oacute;n:</td>
            	<td><input type="text" name="Institucion"  value="<?php echo $Datos[2]; ?>" required='true' /></td>
            </tr>
            <tr>
            	<td style='text-align: right;'>Título o Certificado:</td>
            	<td><input type="text" name="Titulo" value="<?php echo $Datos[3]; ?>" required="true" /></td>
            </tr>
            <tr>
            	<td style='text-align: right;'>Direcci&oacute;n:</td>
            	<td><input type="text" name="Direccion" value="<?php echo $Datos[4]; ?>" /></td>
            </tr>
            <tr>
            	<td style='text-align: right;'>Tel&eacute;fono:</td>
            	<td><input type="text" name="Telefono" value="<?php echo $Datos[5]; ?>" /></td>
            </tr>
            <tr>
            	<td style='text-align: right;'>SitioWeb:</td>
            	<td><input type="text" name="SitioWeb" value="<?php echo $Datos[6]; ?>" /></td>
            </tr>
            <tr>
            	<td style='text-align: right;'>A&ntilde;o de ingreso:</td>
            	<td>
                    <select name="AnioIngreso">
                    	<?php 
							for($i = date("Y"); $i >= 1900; $i--)
							{
								if(  $i == $Datos[7])
									echo '<option selected=\'selected\' value=\''.$i.'\'>'.$i.'</option>';
								else
									echo '<option value=\''.$i.'\'>'.$i.'</option>';                                                
							}
						?>
                    </select>
                </td>
            </tr>
			<tr>
            	<td style='text-align: right;'>A&ntilde;o de egreso:</td>
            	<td>
                    <select name="AnioEgreso">
                    	<?php 
							for($i = date("Y"); $i >= 1900; $i--)
							{
								if( $i == $Datos[8])
									echo '<option selected=\'selected\' value=\''.$i.'\'>'.$i.'</option>';
								else
									echo '<option value=\''.$i.'\'>'.$i.'</option>';                                                
							}
						?>
                    </select>
                </td>
            </tr>            
            <tr><td colspan="2" style='text-align: center;'><input type="submit" value="<?php echo $ABM; ?>" name="Enviar" class='boton'/></td></tr>
            <tr>
		          <td colspan="2" style="text-align: center; margin-top: 5px;">
                    <font size="-1">Todos los campos en <font color="#FF0000">rojo</font> deben ser llenados.</font>
                  </td>
			</tr>
        </table>   
    </form>
</div>