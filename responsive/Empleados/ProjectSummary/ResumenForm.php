<?php
	if(isset($_POST['Enviar']))
    {
        $ABM = "Actualizar";
        $Resumen = $bdd->prepare("Select * from Project_Summary where IdSummary = " . $_SESSION["irp"]);
        $Resumen->execute();
        $DataR = $Resumen->fetch();
    }
    else
    {
        $ABM = "Crear Resumen";
    }
?>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">   
<style>
.mitabla td
{
    padding: 5px;
}
</style>
<form action="Empleados/ProjectSummary/ResumenABM.php" method="post">
<table style="width: 100%; padding-left: 30px; padding: 5px;" class="mitabla">
<tr>
    <td colspan="2">
        <table style="width: 100%;">
        <tr>
            <td><h2>Resumen de Proyectos</h2> </td>
            <td style="width: 30%; text-align: right;"><a href="?l=ProjectSummary"><input type="button" class="boton" value="<- Atras" /></a></td>    
        </tr>
        </table>
        <hr color='skyblue' />
    </td>
</tr>
<tr>
    <td style="width: 30%;">Nombre del proyecto:</td>
    <td><input type="text" name="nombre" value="<?php echo $DataR[2];?>" style="width: 80%;" /></td>
</tr>
<tr>
    <td>Entidad financiadora:</td>
    <td><input type="text" name="financiador" value="<?php echo $DataR[3];?>" style="width: 80%;" /></td>
</tr>
<tr>
    <td>Monto total para el proyecto:</td>
    <td><input type="number" name="monto" value="<?php echo $DataR[4];?>" min="0" style="width: 150px;" /></td>
</tr>
<tr>
    <td>Monto total de contrapartida:</td>
    <td><input type="number" name="contrapartida" value="<?php echo $DataR[5];?>" min="0" style="width: 150px;" /></td>
</tr>
<tr>
    <td>Fecha de inicio<br />(A&ntilde;o-Mes-D&iacute;a):</td>
    <td><input style="text-align: left; padding-left: 20px; width: 150px;" type="date" id="FechaI" name="FechaI" value="<?php echo $DataR[7];?>" pattern="^\d{4}\-\d{2}\-\d{2}$" required="true" /></td>         
</tr>
<tr>
    <td>Fecha de hasta<br />(A&ntilde;o-Mes-D&iacute;a):</td>
    <td><input style="text-align: left; padding-left: 20px; width: 150px;" type="date" id="FechaH" name="FechaH" value="<?php echo $DataR[8];?>" pattern="^\d{4}\-\d{2}\-\d{2}$" required="true" /></td>         
</tr>
<tr>
    <td>Coordinador del proyecto:<br />
        <td><input type="text" name="coordinador" value="<?php echo $DataR[6];?>" style="width: 80%;" /></td>
    </td>
</tr>
<tr>
    <td>L�nea Estrat�gica:</td>
    <td>
        <table style="width: 100%;">            
            <?php
                   $Tematicas = $bdd->prepare("Select * from Project_Line");
                   $Tematicas->execute();
                   
                   $aux = "Apertura";
                   while($DataT = $Tematicas->fetch())
                   {
                                
                        if(strpos($DataR[13], $DataT[1]))
                            echo "<tr><td style='width: 50%;'><input type='checkbox' value='$DataT[1]' name='linea[]' checked='true' />$DataT[1]</td></tr>";
                        
                        else
                            echo "<tr><td style='width: 50%;'><input type='checkbox' value='$DataT[1]' name='linea[]' />$DataT[1]</td></tr>";                          
                   }
            ?>                     
        </table>
    </td>
</tr>

<tr>
    <td colspan="2">Objetivo general:<br />
        <textarea name="objetivo" style="width: 90%; height: 50px;"><?php echo $DataR[9];?></textarea>
    </td>
</tr>
<tr>
    <td colspan="2">Descripci�n general (500 caracteres):<br />
        <textarea name="descripcion" style="width: 90%; height: 50px;" maxlength="500"><?php echo $DataR[11];?></textarea>
    </td>
</tr>

<tr>
    <td>Tem�tica:</td>
    <td>
        <table style="width: 100%;">            
            <?php
                   $Tematicas = $bdd->prepare("Select * from Project_Tematica");
                   $Tematicas->execute();
                   
                   $aux = "Apertura";
                   while($DataT = $Tematicas->fetch())
                   {
                        if($aux == "Apertura")                        
                            echo "<tr>";
                                
                                
                        if(strpos($DataR[10], $DataT[1]))
                            echo "<td style='width: 50%;'><input type='checkbox' value='$DataT[1]' name='tematica[]' checked='true' />$DataT[1]</td>";
                        
                        else
                            echo "<td style='width: 50%;'><input type='checkbox' value='$DataT[1]' name='tematica[]' />$DataT[1]</td>";
                        
                        
                        if($aux == "Cierre")                        
                            echo "<tr>";  
                            
                        if($aux == "Apertura") 
                            $aux = "Cierre";
                            
                        else
                            $aux = "Apertura";
                                                     
                   }
            ?>      
            <tr><td colspan="2">Nueva tem�tica: <input type="text" value="" name="nuevatematica" style="margin-left: 20px; width: 60%;" /></td></tr>               
        </table>
    </td>
</tr>
<tr>
    <td colspan="2" ><input type="submit" style="margin-left: 35%;" name="Enviar" value="<?php echo $ABM;?>" class="boton" /></td>
</tr>
</table>
</form>
</div>