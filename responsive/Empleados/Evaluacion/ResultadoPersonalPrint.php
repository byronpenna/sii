<?php
	session_start();
    require '../../net.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Ficha del Empleado</title>
</head>
<script >
function cerrar() { setTimeout(window.close,1500); }
</script>
<body onload="window.print();cerrar();" style="font-family: Calibri; font-size: small;">
<div style="width: 94%; margin-left: 3%;">
<?php

/**
 * @author José Manuel Calderón
 * @copyright 2014
 */
   
   

    unset($_SESSION['Numero']);
    unset($_SESSION['IdC']);
    unset($_SESSION['IdE']);
    unset($_SESSION['IdForm']);    
            
    if(isset($_POST['IdE']))
    $IDE = $_POST['IdE'];
    
    else
    $IDE = $_SESSION["IdUsuario"];
    
    $MisDatos = $bddr->prepare("SELECT a.*  FROM  Cargos as a
                                inner join CargosAsignacion as ca on ca.IdCargo = a.IdCargos 
                                where ca.IdEmpleado = $IDE and FechaFin = '0000-00-00'" );
    $MisDatos->execute();
    $DataEmpleado = $MisDatos->fetch();
       
    
   

    
    if(isset($_POST['Evaluacion']))
    {
        $_SESSION['eva'] = $_POST['Evaluacion'];        
    }   
    else
    {
        $Evaluacion = $bddr->prepare("Select * from Evaluacion");
        $Evaluacion->execute();   
        $DataEva = $Evaluacion->fetch();
        $_SESSION['eva'] = $DataEva[1];
        $_SESSION['IdEva'] = $DataEva[0];
    }                                            

    
        
        $IDC = $DataEmpleado[0];              
                
        $_SESSION['IdE'] = $IDE;
        $_SESSION['IdC'] = $IDC;
        
             
                
        $DataEmpleados = $bddr->prepare("Select e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.* 
                                         from Empleado as e
                                         inner join CargosAsignacion as ca on ca.IdEmpleado = e.IdEmpleado
                                         inner join Cargos as c on ca.IdCargo = c.IdCargos  
                                         where e.IdEmpleado = $IDE and c.IdCargos = $IDC  and FechaFin = '0000-00-00'");
        $DataEmpleados->execute();                                
        $DatosEmpleados = $DataEmpleados->fetch();
        
        $Area = $bddr->prepare("Select * from AreasDeTrabajo where IdAreaDeTrabajo = $DatosEmpleados[8]");
        $Area->execute();
            
        $DataA = $Area->fetch();
                                     
        $ArrayAutoevaluacion = array();
        $ArrayClienteInterno = array();
        $ArrayColegaTrabajo = array();
        $ArrayColaborador = array();
        $ArrayJefeInmediato = array();
        
        $ArrayAutoevaluacionJ = array();
        $ArrayClienteInternoJ = array();
        $ArrayColegaTrabajoJ = array();
        $ArrayColaboradorJ = array();
        $ArrayJefeInmediatoJ = array();        

                           
               $Evaluadores = $bddr->prepare("Select * from Evaluacion_Estado WHERE IdEmpleado = $IDE and IdEvaluacion = " . $_POST['DataIdEva'] ." ORDER BY TipoEvaluacion ASC ");
               $Evaluadores->execute();
               
               while($DataEvaluador = $Evaluadores->fetch())  
               {
                                              
                   $TemasResultados = $bddr->prepare("SELECT t . * , f.Formulario FROM Evaluacion_Resultados AS r
                                                      INNER JOIN Evaluacion_Preguntas AS p ON r.IdPregunta = p.IdPregunta
                                                      INNER JOIN Evaluacion_Temas AS t ON p.IdTema = t.IdTema
                                                      INNER JOIN Evaluacion_Formulario AS f ON t.IdFormulario = f.IdFormulario
                                                      WHERE r.Evaluacion = '". $_SESSION['eva'] ."' and IdEmpleado = $IDE and IdAsignaciónCargo = $IDC 
                                                      AND f.IdFormulario = $DataEvaluador[3] and IdJefe = $DataEvaluador[6] 
                                                      GROUP BY t.IdTema");   
                   $IdForm = $DataEvaluador[3];                                                   
                                                                                          
                   $TemasResultados->execute();                       
                   
                   unset($arrayTemas);
                   unset($arrayPromedio);
                   
                   $arrayTemas = array();
                   $arrayPromedio = array();            
                                        
                   while($DataT = $TemasResultados->fetch())   
                        array_push($arrayTemas, $DataT[0]);
                    
                               
                        
                   $PromedioTotal = 0;
                   $contador = 0;
                    

                    
                    
                   if(count($arrayTemas) > 0)
                   {
                    
                       foreach($arrayTemas as $value)
                       {
                             $Resultados  = $bddr->prepare("SELECT r.* , t.Temas  FROM Evaluacion_Resultados AS r
                                                           INNER JOIN Evaluacion_Preguntas AS p ON r.IdPregunta = p.IdPregunta
                                                           INNER JOIN Evaluacion_Temas AS t ON p.IdTema = t.IdTema
                                                           where t.IdTema = $value and r.Evaluacion = '".$_SESSION['eva'] ."' and IdEmpleado = $IDE and IdAsignaciónCargo = $IDC and IdJefe = $DataEvaluador[6] ");
                             $Resultados->execute();
                             $Promedio = 0;              
                             $TemaColocado = false;           
                             $contador++;
                             $tema = "";
                             
                             while($DataR = $Resultados->fetch())
                             {                   
                                if(!$TemaColocado)                            
                                    $TemaColocado = true;   
    
                                $tema = $DataR[8];                            
                                $Promedio = $Promedio + $DataR[3];
                             }
                             
                             $Promedio = $Promedio / $Resultados->rowCount(); 
                             array_push($arrayPromedio, number_format($Promedio,2));
                             
                             $PromedioTotal = $PromedioTotal + $Promedio;           
                       }            
                    
                        $PromedioTotal = $PromedioTotal / count($arrayTemas);                
                   }
                    
                    if($DataEvaluador[2] == "Autoevaluación")
                    {
                        array_push($ArrayAutoevaluacion, number_format($PromedioTotal,2));
                        array_push($ArrayAutoevaluacionJ, $DataEvaluador[6]);                        
                    }    
                    if($DataEvaluador[2] == "Evaluación - Cliente interno")
                    {
                        array_push($ArrayClienteInterno, number_format($PromedioTotal,2));
                        array_push($ArrayClienteInternoJ, $DataEvaluador[6]);
                    }    
                    if($DataEvaluador[2] == "Evaluación - Colega de trabajo")
                    {
                        array_push($ArrayColegaTrabajo, number_format($PromedioTotal,2));
                        array_push($ArrayColegaTrabajoJ, $DataEvaluador[6]);
                    }    
                    if($DataEvaluador[2] == "Evaluación - Colaborador")
                    {
                        array_push($ArrayColaborador, number_format($PromedioTotal,2));
                        array_push($ArrayColaboradorJ,  $DataEvaluador[6]);
                    }    
                    if($DataEvaluador[2] == "Evaluación - Jefe inmediato")
                    {
                        array_push($ArrayJefeInmediato, number_format($PromedioTotal,2));
                        array_push($ArrayJefeInmediatoJ, $DataEvaluador[6]);
                    }
                                                                                                                           
                }
                
                $ArrayPromedioTotal = array();
                $ContadorTotal = 0;
                                
                
                
                $PromedioAux = 0;
                $ContadorA = 0;
                $i = 0;
                
                foreach($ArrayAutoevaluacion as $value)
                {
                    $i++;                                    
                    if($value > 0)
                    {
                        $PromedioAux += $value;
                        $ContadorA++;
                    }    
                }  
                                  
                if(count($ArrayAutoevaluacion) == 0)
                {

                    array_push($ArrayPromedioTotal, "-");
                }
                else
                {
                    if($ContadorA == 0)
                    $ContadorA = 1;                                        
                    $PromedioAux = $PromedioAux / $ContadorA;
                          
                    if($PromedioAux != 0)
                    {
                        //$ContadorTotal++;
                        array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                    }
                    
                    else
                        array_push($ArrayPromedioTotal, "-");
                }

                
                
                $PromedioAux = 0;
                $ContadorA = 0;
                $i = 0;

                foreach($ArrayClienteInterno as $value)
                {
                    
                    $i++;
                    if($value > 0)
                    {
                        $PromedioAux += $value;
                        $ContadorA++;
                    }                                           
                }
                                      
                if(count($ArrayClienteInterno) == 0)
                {
                    array_push($ArrayPromedioTotal, "-");
                }
                else
                {
                    if($ContadorA == 0)
                    $ContadorA = 1;
                                        
                    $PromedioAux = $PromedioAux / $ContadorA;
                          
                    if($PromedioAux != 0)
                    {
                        $ContadorTotal++;
                        array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                    }
                    
                    else
                        array_push($ArrayPromedioTotal, "-");
                }
                                                    

                
                
                $PromedioAux = 0;
                $ContadorA = 0;                
                $i = 0;

                foreach($ArrayColegaTrabajo as $value)
                {
                    
                    $i++;
               
                    if($value > 0)
                    {
                        $PromedioAux += $value;
                        $ContadorA++;
                    }                    
                }                      
                if(count($ArrayColegaTrabajo) == 0)
                {

                    array_push($ArrayPromedioTotal, "-");
                }
                else
                {
                    if($ContadorA == 0)
                    $ContadorA = 1;
                                        
                    $PromedioAux = $PromedioAux / $ContadorA;

                    if($PromedioAux != 0)
                    {
                        $ContadorTotal++;
                        array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                    }
                    
                    else
                        array_push($ArrayPromedioTotal, "-");
                        
                }
                                                    
                
                
                $PromedioAux = 0;
                $ContadorA = 0;                
                $i = 0;
                
                foreach($ArrayColaborador as $value)
                {
                    
                    $i++;
                
                    
                    
                    if($value > 0)
                    {
                        $PromedioAux += $value;
                        $ContadorA++;
                    }                                        
                }                    
                if(count($ArrayColaborador) == 0)
                {
                    
                
                    array_push($ArrayPromedioTotal, "-");
                }
                else
                {
                    if($ContadorA == 0)
                    $ContadorA = 1;
                    
                    $PromedioAux = $PromedioAux / $ContadorA;

                    if($PromedioAux != 0)
                    {
                        $ContadorTotal++;
                        array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                    }
                    
                    else
                        array_push($ArrayPromedioTotal, "-");
                }                    

                
                
                $PromedioAux = 0;
                $ContadorA = 0;                
                $i = 0;

                foreach($ArrayJefeInmediato as $value)
                {                    
                    $i++;
      
                    
                    if($value > 0)
                    {
                        $PromedioAux += $value;
                        $ContadorA++;
                    }                                                                
                } 
                                  
                if(count($ArrayJefeInmediato) == 0)
                {

                    array_push($ArrayPromedioTotal, "-");
                }   
                else
                {
                    if($ContadorA == 0)
                    $ContadorA = 1;
                                        
                    $PromedioAux = $PromedioAux / $ContadorA;
                    
                    if($PromedioAux != 0)
                    {
                        $ContadorTotal++;
                        array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                    }
                    else
                        array_push($ArrayPromedioTotal, "-");                    
                }
                                                               
            ?>

<div style="width: 100%;">
<table style="width: 100%;">
    <tr><td style='text-align: right;'>
        <div style="float: left;"><img src="../../images/logo.jpg" width="204" height="70" alt="" style="border-color: skyblue; border-radius: 10px;" /></div>
        <h2>Resultado de Evaluación 360</h2>
        </td>
    </tr>          
    <tr>
    <tr><td colspan='2' style='color: blue; '><strong><h3 style='padding: 0px;margin: 0px;'>Empleado evaluado</h3></strong></td><tr>    
        <td>   
        <?php
            echo "<table style='width: 94%; margin-left: 3%; '>
                  <tr><td class='tdleft' style='width: 30%'> Empleado: </td><td>$DatosEmpleados[1] $DatosEmpleados[2] $DatosEmpleados[3] $DatosEmpleados[4] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Area: </td><td>$DataA[1] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Cargo: </td><td> $DatosEmpleados[6] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Tipo de Evaluación:</td><td><strong>" . $_SESSION['eva'] . "</strong></td></tr>
                  </table>"; 
        ?>
        </td>
    </tr>
    <tr>
        <td>
            <hr color='skyblue' />
            <h3 style='padding: 0px; margin: 0px;' >Promedio del Empleado</h3>
            <br />
            <table style="width: 60%; margin-left: 20%;">
                <tr><th>Enfoque de Evaluación</th><th>Promedios</th></tr>
                <tr><td><em style="color: blue;">Autoevaluación (No aplica)</em></td>
                    <th><em style="color: blue;"><?php echo $ArrayPromedioTotal[0];?></em></th></tr>
                <tr><td>Cliente interno</td>        <th><?php echo $ArrayPromedioTotal[1];?></th></tr>
                <tr><td>Colega de trabajo</td>      <th><?php echo $ArrayPromedioTotal[2];?></th></tr>
                <tr><td>Colaborador</td>            <th><?php echo $ArrayPromedioTotal[3];?></th></tr>
                <tr><td>Jefe inmediato</td>         <th><?php echo $ArrayPromedioTotal[4];?></th></tr>
                <tr><td colspan="2"><hr color='skyblue' /></td></tr>
                <?php
                      
                      $SumaPromedio = 0;
                      $j = 0;
	                   foreach($ArrayPromedioTotal as $value)
                       {
                            $j++;
                            if($value != "-" && $j != 1)
                                $SumaPromedio += $value;
                       }    
                       
                       if($ContadorTotal == 0)
                        $SumaPromedio =  0;
                        
                       else
                        $SumaPromedio = $SumaPromedio/$ContadorTotal;
                       
                ?>
                <tr><td class="tdleft"><strong>Promedio</strong></td><th><?php echo number_format($SumaPromedio, 2)?></th></tr>            
            </table>
            <br />
            <table style="text-align: justify;">
            <tr><td><h3>Comentarios</h3></td></tr>
            <?php
                   $Comentarios = $bddr->prepare("SELECT ComentarioEmpleado FROM Evaluacion_Estado where IdEmpleado = $IDE and ComentarioEmpleado != '' and IdEvaluacion = " . $_POST['DataIdEva'] ."" );
                   $Comentarios->execute();                   
                   
                   while($DataComentarios = $Comentarios->fetch())
                   {
                        echo "<tr><td><strong>Evaluador: </strong><em><br /> $DataComentarios[0]</em><br /><br /></td></tr>";
                   }
            ?>
            </table>
        </td>
    </tr>
</table>
</div>
<div style="padding-top: 50px;">
    <table style="width: 60%; text-align: center; margin-left: 25%;">
        <tr>
            <td style="border-top-style: ridge;">Firma Empleado</td>
            <td style="width: 20%;"></td>
            <td style="border-top-style: ridge;">Firma Jefe Inmediato</td>
        </tr>
    </table>
</div>
</div>
</body>
</html>