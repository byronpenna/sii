<?php

/**
 * @author José Manuel Calderón
 * @copyright 2014
 */
  
require '../../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Asignar Evaluador")
    {
        
    
        $FormFill = $bddr->prepare("Insert into Evaluacion_Estado 
                                    Values (null, :idEva, :TipoE, :idFor, :IdEmpl, :IdCar, :IdEvalua, 'Pendiente', '')");
        $FormFill->bindParam(':idEva', $_SESSION['IdEva']);
        $FormFill->bindParam(':TipoE', $_POST['TipoEvaluacion']);
        $FormFill->bindParam(':idFor', $_POST['IdFormulario']);
        $FormFill->bindParam(':IdEmpl', $_POST['IdE']);
        $FormFill->bindParam(':IdCar', $_POST['IdPosition']);
        $FormFill->bindParam(':IdEvalua', $_POST['IEv']);                
        
        if($FormFill->execute())
        {                       
                  
        //Correo
            $Empleado = $bddr->prepare("SELECT * FROM DatosInstitucionales where IdEmpleado = :IEv ");
            $Empleado->bindParam(':IEv', $_POST['IEv']);
            $Empleado->execute();
            
            if($Empleado->rowCount() > 0)
            {
                $EmpleadoE = $bddr->prepare("Select e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2 FROM Empleado as e where IdEmpleado = '" . $_POST['IdE'] . "'");
                $EmpleadoE->execute();
            
                $DataE = $Empleado->fetch();
                
                $DataE2 = $EmpleadoE->fetch();
                $to = "$DataE[2]";
                
                $subject = "Evaluación de Desempeño";
                
                
                if($_POST['TipoEvaluacion'] == "Autoevaluación")
                {
                    $message = "
                    
                                Sistema de Información Institucional de Polideportivos Don Bosco
                                --------------------------------------------------------------------------
                                
                                Notificación de Evaluación
                                --------------------------------------------------------------------------
                                Se ha activado la 'Autoevaluación de Desempeño', si desconoce el procedimiento 
                                para poder realizar la autoevaluación le solicitamos que se acerque a Tecnología e 
                                Innovación para apoyarte en este proceso.
                                
                                Saludos y Bendiciones                       
                                --------------------------------------------------------------------------
                                Fecha de Registro:    ".date("Y-m-d H:i:s")."
                                https://siipdb.fusalmo.org
                                ";                    
                }
                else
                {
                $message = "
                
                            Sistema de Información Institucional de Polideportivos Don Bosco
                            --------------------------------------------------------------------------
                            
                            Notificación de Evaluación
                            --------------------------------------------------------------------------
                            Se te ha asignado para evaluar a '$DataE2[0] $DataE2[1] $DataE2[2] $DataE2[3]' con el 
                            enfoque de '". $_POST['TipoEvaluacion'] ."', si desconoce el procedimiento para poder realizar 
                            la evaluación le solicitamos que se acerque a Tecnología e Innovación para 
                            apoyarte en este proceso.
                            
                            Saludos y Bendiciones                       
                            --------------------------------------------------------------------------
                            Fecha de Registro:    ".date("Y-m-d H:i:s")."
                            https://siipdb.fusalmo.org
                            ";
                }
                
                // More headers
                $headers .= 'From: <noreply.siipdb@fusalmo.org>' . "\r\n";
                $headers .= 'Cc: m.calderon@fusalmo.org' . "\r\n";

                //mail($to,$subject,$message,$headers);                                   
            }
        }
        
        $_SESSION['IdPosition'] = $_POST['IdPosition'];
        $_SESSION['IdE'] = $_POST['IdE'];
        
        Redireccion("../../Empleado.php?l=ManagementAllocation"); 
    }
    
    if($_POST['Enviar'] == "Quitar")
    {
        $Empleado = $bddr->prepare("Delete from Evaluacion_Estado where IdEstado = :IEv ");
        $Empleado->bindParam(':IEv', $_POST['IdEstado']);
        $Empleado->execute();
        
        Redireccion("../../Empleado.php?l=ManagementAllocation&n=2");
    }    
}


?>