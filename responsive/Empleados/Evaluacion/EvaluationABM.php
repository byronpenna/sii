<?php

/**
 * @author José Manuel Calderón
 * @copyright 2014
 */
  
require '../../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Ver Evaluación")
    {
        $_SESSION['IdC'] = $_POST['IdC'];
        $_SESSION['IdE'] = $_POST['IdE'];
        $_SESSION['IdForm'] = $_POST['IdForm'];
        
        Redireccion("../../Empleado.php?l=EvaluationExecute");       
    }    
    
    elseif($_POST['Enviar'] == "Siguiente Tema" || $_POST['Enviar'] == "Finalizar")
    {
        
        $IdP = explode("-", $_POST['Idp']);
        
        foreach($IdP as $valueaux)
        {
            $Delete = $bddr->prepare("Delete from Evaluacion_Resultados 
                                      where IdPregunta = $valueaux and Evaluacion = '" . $_SESSION['eva'] ."' and IdEmpleado = ". $_POST['IdE'] ." and IdAsignaciónCargo = " .  $_SESSION['IdC'] . " and IdJefe = " . $_SESSION['IdUsuario']);
            $Delete->execute();
        }
                
        foreach($IdP as $value)
        {             
            $Insert = $bddr->prepare("Insert into Evaluacion_Resultados 
                                      values(Null, :IdP, :Fecha, :Resultado,:eva, :IdEmpleado, :IdAsignacionCargo, :IdJefe)");
                                      
            $Insert->bindParam(':IdP', $value);
            $Insert->bindParam(':Fecha', date("Y-m-d"));
            $Insert->bindParam(':Resultado', $_POST[$value]);
            $Insert->bindParam(':eva', $_SESSION['eva']);
            $Insert->bindParam(':IdEmpleado', $_POST['IdE']);
            $Insert->bindParam(':IdAsignacionCargo', $_SESSION['IdC']);
            $Insert->bindParam(':IdJefe', $_SESSION['IdUsuario']);                
            $Insert->execute();
        }
        
        if($_POST['Enviar'] == "Siguiente Tema" )
        {
            $_SESSION['Numero'] = $_POST['Numero'] + 1 ;
            
            $Update = $bddr->prepare("Update Evaluacion_Estado set Estado = 'Incompleta'
                                      where IdEvaluacion = :Eva and IdEmpleado = :IdE and IdCargo = :IdC and IdEvaluador =:IdJ");
            $Update->bindParam(':Eva', $_SESSION['ideva']);
            $Update->bindParam(':IdE', $_POST['IdE']);
            $Update->bindParam(':IdC', $_SESSION['IdC']);
            $Update->bindParam(':IdJ', $_SESSION['IdUsuario']);
            $Update->execute();
                        
            Redireccion("../../Empleado.php?l=EvaluationExecute");            
        }
        
        if($_POST['Enviar'] == "Finalizar" )
        {
            $Update = $bddr->prepare("Update Evaluacion_Estado set ComentarioEmpleado = :Obs, Estado = 'Finalizada'
                                      where IdEvaluacion = :Eva and IdEmpleado = :IdE and IdCargo = :IdC and IdEvaluador =:IdJ");
            $Update->bindParam(':Eva', $_SESSION['ideva']);
            $Update->bindParam(':IdE', $_POST['IdE']);
            $Update->bindParam(':IdC', $_SESSION['IdC']);
            $Update->bindParam(':IdJ', $_SESSION['IdUsuario']);
            $Update->bindParam(':Obs', $_POST['Observaciones']);
            $Update->execute();
             
            Redireccion("../../Empleado.php?l=Evaluation&n=1");                                    
        }                      
    }
    elseif($_POST['Enviar'] == "Regresar Tema")
    {
        $_SESSION['Numero'] = $_POST['Numero'] - 1 ;
        Redireccion("../../Empleado.php?l=EvaluationExecute");
    }
    
    if($_POST['Enviar'] == "Conforme" || $_POST['Enviar'] == "Inconforme" )
    {
        $Insert = $bddr->prepare("Insert into Evaluacion_Comentarios Values (null, ".$_POST['IdEstado'].", '". $_POST['Enviar'] ."','". $_POST['Comentario'] ."')" );                                          
        $Insert->execute();

        Redireccion("../../Empleado.php?l=MyEvaluations&n=1");
    }   
    
    elseif($_POST['Enviar'] == "Editar Evaluación")        
        Redireccion("../../Empleado.php?l=EvaluationExecute");
     
     
    elseif($_POST['Enviar'] == "Finalizar Evaluación")        
    {
        $Update = $bddr->prepare("Update Evaluacion_Estado set EstadoEvaluacion = 'Finalizado' where Evaluacion = :Eva and IdEmpleado = :IdE and IdCargo = :IdC and IdEvaluador =:IdJ");
        $Update->bindParam(':Eva', $_POST['Eva']);
        $Update->bindParam(':IdE', $_POST['IdE']);
        $Update->bindParam(':IdC', $_POST['IdC']);
        $Update->bindParam(':IdJ', $_POST['IdJ']);
        $Update->execute();
        
        Redireccion("../../Empleado.php?l=MyEvaluations&n=2");
    }
}
else
    Redireccion("../../Empleado.php?l=EvaluationExecute");

Redireccion("../../Empleado.php?l=EvaluationExecute");

?>