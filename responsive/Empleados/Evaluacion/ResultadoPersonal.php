<?php

/**
 * @author José Manuel Calderón
 * @copyright 2014
 */
   
   

    unset($_SESSION['Numero']);
    unset($_SESSION['IdC']);
    unset($_SESSION['IdE']);
    unset($_SESSION['IdForm']);    
            
    if(isset($_POST['IdE']))
    $IDE = $_POST['IdE'];
    
    else
    $IDE = $_SESSION["IdUsuario"];
    
    $MisDatos = $bddr->prepare("SELECT a.*  FROM  Cargos as a
                                inner join CargosAsignacion as ca on ca.IdCargo = a.IdCargos 
                                where ca.IdEmpleado = $IDE and FechaFin = '0000-00-00'" );
    $MisDatos->execute();
    $DataEmpleado = $MisDatos->fetch();
       
    
   

    
    if(isset($_POST['Evaluacion']))
    {
        $_SESSION['eva'] = $_POST['Evaluacion'];        
    }   
    else
    {
        $Evaluacion = $bddr->prepare("Select * from Evaluacion  ORDER BY IdEvaluacion DESC");
        $Evaluacion->execute();   
        
        $DataEva = $Evaluacion->fetch();
        $_SESSION['eva'] = $DataEva[1];
        $_SESSION['IdEva'] = $DataEva[0];
    }                                            

    
        
        $IDC = $DataEmpleado[0];              
                
        $_SESSION['IdE'] = $IDE;
        $_SESSION['IdC'] = $IDC;
                          
                
        $DataEmpleados = $bddr->prepare("Select e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.* 
                                         from Empleado as e
                                         inner join CargosAsignacion as ca on ca.IdEmpleado = e.IdEmpleado
                                         inner join Cargos as c on ca.IdCargo = c.IdCargos  
                                         where e.IdEmpleado = $IDE and c.IdCargos = $IDC  and FechaFin = '0000-00-00'");
        $DataEmpleados->execute();                                
        $DatosEmpleados = $DataEmpleados->fetch();
        
        $Area = $bddr->prepare("Select * from AreasDeTrabajo where IdAreaDeTrabajo = $DatosEmpleados[8]");
        $Area->execute();
            
        $DataA = $Area->fetch();
                                     
        $ArrayAutoevaluacion = array();
        $ArrayClienteInterno = array();
        $ArrayColegaTrabajo = array();
        $ArrayColaborador = array();
        $ArrayJefeInmediato = array();
        
        $ArrayAutoevaluacionJ = array();
        $ArrayClienteInternoJ = array();
        $ArrayColegaTrabajoJ = array();
        $ArrayColaboradorJ = array();
        $ArrayJefeInmediatoJ = array();        

        $IdEvaluacion = $bddr->prepare("SELECT IdEvaluacion FROM Evaluacion  WHERE NombreEvaluación ='" . $_SESSION['eva']."'");
        $IdEvaluacion->execute();
        $DataIdEva = $IdEvaluacion->fetch();

?>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">
<table style="width: 100%;">
    <tr><td style='text-align: right;'><h2>Resultado de Evaluación 360</h2></td></tr>
    <tr>
        <td>
            <table>
            <tr>
                <td>
                    <?php
        	               if(isset($_POST['Aux1']))
                           {
                                echo "<form action='?l=AllEvaluations' method='post'>
                                        <input type='hidden' name='ia' value='$DataA[0]' />
                                        <input type='submit' name='Enviar' value='<- Regresar' class='boton' />
                                      </form>  ";
                           }
                    ?>
                </td>
                <form method="post" action="">
                <td style="padding-left: 20px; width: 50%;">Seleccione la evaluación que desea observar:</td>
                <td style=" width: 40%;"><select name="Evaluacion" style="width: 80%;" >
                    <?php
                    
                           $Evaluacion = $bddr->prepare("Select * from Evaluacion  ORDER BY IdEvaluacion DESC ");
                           $Evaluacion->execute();
    	                   while($DataE = $Evaluacion->fetch())
                           {
                                if(isset($_POST['Evaluacion']))
                                {
                                    if($_POST['Evaluacion'] == $DataE[1])
                                        echo "<option value='$DataE[1]' selected>$DataE[1]</option>";
                                        
                                    else
                                        echo "<option value='$DataE[1]'>$DataE[1]</option>";
                                }
                                else
                                    echo "<option value='$DataE[1]'>$DataE[1]</option>";
                           }
                    ?>
                    </select>
                </td>
                <td><input type="submit" value="Ver" style="width: 50px;" class="boton" /></td>
                </form>
            </tr>
            <tr>
                <td colspan="4" style="text-align: right;">
                    <?php
	                       echo "<form action='Empleados/Evaluacion/ResultadoPersonalPrint.php' method='post' target='_blank'>
                                    <input type='hidden' name='IdE' value='$IDE' />
                                    <input type='hidden' name='DataIdEva' value='$DataIdEva[0]' />
                                    <input type='hidden' name='Evaluacion' value='".$_SESSION['eva']."' />
                                    
                                    <input type='submit' class='botonG' value='Imprimir' />                          
                                  </form>";
                    ?>
                </td>
            </tr>
            </table>
        </td>  
    </tr>  
    <tr>
        <td>   
        <?php
            echo "<table style='width: 94%; margin-left: 3%; '>
                    
                  <tr><td colspan='2' style='color: blue; '> Empleado evaluado</td><tr>
                  <tr><td class='tdleft' style='width: 30%'> Empleado: </td><td>$DatosEmpleados[1] $DatosEmpleados[2] $DatosEmpleados[3] $DatosEmpleados[4] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Area: </td><td>$DataA[1] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Cargo: </td><td> $DatosEmpleados[6] </td></tr>
                  <tr><td class='tdleft' style='width: 30%'> Tipo de Evaluación:</td><td><strong>" . $_SESSION['eva'] . "</strong></td></tr>
                  </table>"; 
        ?>
        </td>
    </tr>

    <tr>
        <td>
        <table style="width: 100%;">
        <tr><td colspan='6'><hr color='skyblue' /></td></tr>
        
            <?php
                

                                              
               
               $Evaluadores = $bddr->prepare("Select * from Evaluacion_Estado WHERE IdEmpleado = $IDE and IdEvaluacion = $DataIdEva[0] ORDER BY TipoEvaluacion ASC ");
               $Evaluadores->execute();
               
               while($DataEvaluador = $Evaluadores->fetch())  
               {
                                                             
                   $TemasResultados = $bddr->prepare("SELECT t . * , f.Formulario FROM Evaluacion_Resultados AS r
                                                      INNER JOIN Evaluacion_Preguntas AS p ON r.IdPregunta = p.IdPregunta
                                                      INNER JOIN Evaluacion_Temas AS t ON p.IdTema = t.IdTema
                                                      INNER JOIN Evaluacion_Formulario AS f ON t.IdFormulario = f.IdFormulario
                                                      WHERE r.Evaluacion = '". $_SESSION['eva'] ."' and IdEmpleado = $IDE and IdAsignaciónCargo = $IDC 
                                                      AND f.IdFormulario = $DataEvaluador[3] and IdJefe = $DataEvaluador[6] 
                                                      GROUP BY t.IdTema");   
                   $IdForm = $DataEvaluador[3];                                                                                                        
                   $TemasResultados->execute();                       
                   
                   unset($arrayTemas);
                   unset($arrayPromedio);
                   
                   $arrayTemas = array();
                   $arrayPromedio = array();            
                                        
                   while($DataT = $TemasResultados->fetch())   
                        array_push($arrayTemas, $DataT[0]);
                    
                               
                        
                   $PromedioTotal = 0;
                   $contador = 0;
                    

                    
                    
                   if(count($arrayTemas) > 0)
                   {
                    
                       foreach($arrayTemas as $value)
                       {
                             $Resultados  = $bddr->prepare("SELECT r.* , t.Temas  FROM Evaluacion_Resultados AS r
                                                           INNER JOIN Evaluacion_Preguntas AS p ON r.IdPregunta = p.IdPregunta
                                                           INNER JOIN Evaluacion_Temas AS t ON p.IdTema = t.IdTema
                                                           where t.IdTema = $value and r.Evaluacion = '".$_SESSION['eva'] ."' and IdEmpleado = $IDE and IdAsignaciónCargo = $IDC and IdJefe = $DataEvaluador[6] ");
                             $Resultados->execute();
                             $Promedio = 0;              
                             $TemaColocado = false;           
                             $contador++;
                             $tema = "";
                             
                             while($DataR = $Resultados->fetch())
                             {                   
                                if(!$TemaColocado)                            
                                    $TemaColocado = true;   
    
                                $tema = $DataR[8];                            
                                $Promedio = $Promedio + $DataR[3];
                             }
                             
                             $Promedio = $Promedio / $Resultados->rowCount(); 
                             array_push($arrayPromedio, number_format($Promedio,2));
                             
                             $PromedioTotal = $PromedioTotal + $Promedio;           
                       }            
                    
                        $PromedioTotal = $PromedioTotal / count($arrayTemas);                
                   }
                    
                    if($DataEvaluador[2] == "Autoevaluación")
                    {
                        array_push($ArrayAutoevaluacion, number_format($PromedioTotal,2));
                        array_push($ArrayAutoevaluacionJ, $DataEvaluador[6]);                        
                    }    
                    if($DataEvaluador[2] == "Evaluación - Cliente interno")
                    {
                        array_push($ArrayClienteInterno, number_format($PromedioTotal,2));
                        array_push($ArrayClienteInternoJ, $DataEvaluador[6]);
                    }    
                    if($DataEvaluador[2] == "Evaluación - Colega de trabajo")
                    {
                        array_push($ArrayColegaTrabajo, number_format($PromedioTotal,2));
                        array_push($ArrayColegaTrabajoJ, $DataEvaluador[6]);
                    }    
                    if($DataEvaluador[2] == "Evaluación - Colaborador")
                    {
                        array_push($ArrayColaborador, number_format($PromedioTotal,2));
                        array_push($ArrayColaboradorJ,  $DataEvaluador[6]);
                    }    
                    if($DataEvaluador[2] == "Evaluación - Jefe inmediato")
                    {
                        array_push($ArrayJefeInmediato, number_format($PromedioTotal,2));
                        array_push($ArrayJefeInmediatoJ, $DataEvaluador[6]);
                    }
                                                                                                                           
                }
                
                $ArrayPromedioTotal = array();
                $ContadorTotal = 0;
                                
                echo "<table style='width: 80%; margin-left: 10%'>";
                
                $PromedioAux = 0;
                $ContadorA = 0;
                $i = 0;
                echo "<tr><td colspan='3'><h3 style='padding: 0px;margin: 0px;' >AutoEvaluación</h3></td></tr>";
                foreach($ArrayAutoevaluacion as $value)
                {
                    
                    echo "<tr><form action='?l=Result' method='post'>
                            <input type='hidden' name='IdE' value='$IDE' />
                            <input type='hidden' name='IdC' value='$IDC' />
                            <input type='hidden' name='IdEvaluador' value='$ArrayAutoevaluacionJ[$i]' />
                            <input type='hidden' name='IdForm' value='$IdForm' />";
                    $i++;
                    
                    echo "<td style='width: 33%' class='tdleft'>Evaluador $i</td><th style='width: 33%'>$value</th>
                              <td style='width: 33%;'><input type='submit' class='botonG' value='Ver' style='width: 50px' /></td>                          
                          </form>
                          <tr>";
                    
                    if($value > 0)
                    {
                        $PromedioAux += $value;
                        $ContadorA++;
                    }    
                }  
                                  
                if(count($ArrayAutoevaluacion) == 0)
                {
                    echo "<tr><td style='text-align: center; color: red;' colspan='3'><h3>No se le asignaron Autoevaluaciones</h3></td><tr>";
                    array_push($ArrayPromedioTotal, "-");
                }
                else
                {
                    if($ContadorA == 0)
                    $ContadorA = 1;
                                        
                    $PromedioAux = $PromedioAux / $ContadorA;
                    echo "<tr><td style='width: 33%;' class='tdleft'> <strong>Promedio:</strong> </td>
                              <th style='width: 33%;' ><strong>".number_format($PromedioAux,2)."</strong></th>
                          <tr>";
                          
                    if($PromedioAux != 0)
                    {
                        //$ContadorTotal++;
                        array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                    }
                    
                    else
                        array_push($ArrayPromedioTotal, "-");
                }
                echo "<tr><td><br /></td></tr>";
                
                
                $PromedioAux = 0;
                $ContadorA = 0;
                $i = 0;
                echo "<tr><td colspan='3'><h3 style='padding: 0px;margin: 0px;' >Evaluación - Cliente interno</h3></td></tr>";
                foreach($ArrayClienteInterno as $value)
                {
                    
                    echo "<tr><form action='?l=Result' method='post'>
                            <input type='hidden' name='IdE' value='$IDE' />
                            <input type='hidden' name='IdC' value='$IDC' />
                            <input type='hidden' name='IdEvaluador' value='$ArrayClienteInternoJ[$i]' />
                            <input type='hidden' name='IdForm' value='$IdForm' />";                    
                    $i++;
                    echo "<td style='width: 33%' class='tdleft'>Evaluador $i</td><th style='width: 33%'>$value</th>
                              <td style='width: 33%'><input type='submit' class='botonG' value='Ver' style='width: 50px' /></td>                          
                          </form>
                          <tr>";
                    
                    if($value > 0)
                    {
                        $PromedioAux += $value;
                        $ContadorA++;
                    }                                           
                }
                                      
                if(count($ArrayClienteInterno) == 0)
                {
                    echo "<tr><td style='text-align: center; color: red;' colspan='3'>No se le asginaron Evaluaciones de Cliente Interno</td><tr>";
                    array_push($ArrayPromedioTotal, "-");
                }
                else
                {
                    if($ContadorA == 0)
                    $ContadorA = 1;
                                        
                    $PromedioAux = $PromedioAux / $ContadorA;
                    echo "<tr><td style='width: 33%;' class='tdleft'> <strong>Promedio:</strong> </td>
                              <th style='width: 33%;' ><strong>".number_format($PromedioAux,2)."</strong></th>
                          <tr>";
                          
                    if($PromedioAux != 0)
                    {
                        $ContadorTotal++;
                        array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                    }
                    
                    else
                        array_push($ArrayPromedioTotal, "-");
                }
                                                    
                echo "<tr><td><br /></td></tr>";
                
                
                $PromedioAux = 0;
                $ContadorA = 0;                
                $i = 0;
                echo "<tr><td colspan='3'><h3 style='padding: 0px;margin: 0px;' >Evaluación - Colega de trabajo</h3></td></tr>";
                foreach($ArrayColegaTrabajo as $value)
                {
                    
                    echo "<tr><form action='?l=Result' method='post'>
                                <input type='hidden' name='IdE' value='$IDE' />
                                <input type='hidden' name='IdC' value='$IDC' />
                                <input type='hidden' name='IdEvaluador'  value='$ArrayColegaTrabajoJ[$i]' />
                                <input type='hidden' name='IdForm' value='$IdForm' />";
                    $i++;                                                                         
                    echo "<td style='width: 33%' class='tdleft'>Evaluador $i</td><th style='width: 33%'>$value</th>
                              <td style='width: 33%'>                              
                                <input type='submit' class='botonG' value='Ver' style='width: 50px' />                                                        
                              </td>
                              </form>
                          <tr>";
                       
                    if($value > 0)
                    {
                        $PromedioAux += $value;
                        $ContadorA++;
                    }                    
                }                      
                if(count($ArrayColegaTrabajo) == 0)
                {
                    echo "<tr><td style='text-align: center;color: red;' colspan='3'>No se le asginaron Evaluaciones de Colega de Trabajo</td><tr>";
                    array_push($ArrayPromedioTotal, "-");
                }
                else
                {
                    if($ContadorA == 0)
                    $ContadorA = 1;
                                        
                    $PromedioAux = $PromedioAux / $ContadorA;
                    echo "<tr><td style='width: 33%;'  class='tdleft'> <strong>Promedio:</strong> </td>
                              <th style='width: 33%;' ><strong>".number_format($PromedioAux,2)."</strong></th>
                          <tr>";

                    if($PromedioAux != 0)
                    {
                        $ContadorTotal++;
                        array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                    }
                    
                    else
                        array_push($ArrayPromedioTotal, "-");
                        
                }
                                                    
                echo "<tr><td><br /></td></tr>";
                
                
                $PromedioAux = 0;
                $ContadorA = 0;                
                $i = 0;
                echo "<tr><td colspan='3'><h3 style='padding: 0px;margin: 0px;'>Evaluación - Colaborador</h3></td></tr>";
                foreach($ArrayColaborador as $value)
                {
                    
                    echo "<tr>
                            <form action='?l=Result' method='post'>
                            <input type='hidden' name='IdE' value='$IDE' />
                            <input type='hidden' name='IdC' value='$IDC' />
                            <input type='hidden' name='IdEvaluador' value='$ArrayColaboradorJ[$i]' />
                            <input type='hidden' name='IdForm' value='$IdForm' />";                    
                    $i++;
                    echo "<td style='width: 33%' class='tdleft'>Evaluador $i</td><th style='width: 33%'>$value</th>
                              <td style='width: 33%'><input type='submit' class='botonG' value='Ver' style='width: 50px'/></td>                          
                          </form>
                          <tr>";
                    
                    if($value > 0)
                    {
                        $PromedioAux += $value;
                        $ContadorA++;
                    }                                        
                }                    
                if(count($ArrayColaborador) == 0)
                {
                    
                    echo "<tr><td style='text-align: center; color: red;' colspan='3'>No se le asginaron Evaluaciones de Colaborador</td><tr>";
                    array_push($ArrayPromedioTotal, "-");
                }
                else
                {
                    if($ContadorA == 0)
                    $ContadorA = 1;
                    
                    $PromedioAux = $PromedioAux / $ContadorA;
                    echo "<tr><td style='width: 33%;' class='tdleft'> <strong>Promedio: </strong></td>
                              <th style='width: 33%;' ><strong>".number_format($PromedioAux,2)."</strong></th>
                          <tr>";
                          
                    if($PromedioAux != 0)
                    {
                        $ContadorTotal++;
                        array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                    }
                    
                    else
                        array_push($ArrayPromedioTotal, "-");
                }                    
                echo "<tr><td><br /></td></tr>";
                
                
                $PromedioAux = 0;
                $ContadorA = 0;                
                $i = 0;
                echo "<tr><td colspan='3'><h3 style='padding: 0px; margin: 0px;' >Evaluación - Jefe inmediato</h3></td></tr>";
                foreach($ArrayJefeInmediato as $value)
                {
                    
                    
                    echo "<tr><form action='?l=Result' method='post'>
                            <input type='hidden' name='IdE' value='$IDE' />
                            <input type='hidden' name='IdC' value='$IDC' />
                            <input type='hidden' name='IdEvaluador' value='$ArrayJefeInmediatoJ[$i]' />
                            <input type='hidden' name='IdForm' value='$IdForm' />";
                    $i++;                          
                    echo "<td style='width: 33%' class='tdleft'>Evaluador $i</td><th style='width: 33%'>$value</th>
                              <td style='width: 33%'><input type='submit' class='botonG' value='Ver' style='width: 50px' /></td>                          
                          </form>
                          <tr>";
                          
                    
                    if($value > 0)
                    {
                        $PromedioAux += $value;
                        $ContadorA++;
                    }                                                                
                } 
                                  
                if(count($ArrayJefeInmediato) == 0)
                {
                    echo "<tr><td style='text-align: center;color: red;' colspan='3'>No se le asginaron Evaluaciones de Jefe Inmediato</td><tr>";
                    array_push($ArrayPromedioTotal, "-");
                }   
                else
                {
                    if($ContadorA == 0)
                    $ContadorA = 1;
                                        
                    $PromedioAux = $PromedioAux / $ContadorA;
                    echo "<tr><td style='width: 33%;' class='tdleft'> <strong>Promedio:</strong> </td>
                              <th style='width: 33%;' ><strong>".number_format($PromedioAux,2)."</strong></th>
                          <tr>";
                    
                    if($PromedioAux != 0)
                    {
                        $ContadorTotal++;
                        array_push($ArrayPromedioTotal, number_format($PromedioAux,2));
                    }
                    else
                        array_push($ArrayPromedioTotal, "-");                    
                }
                                    
                echo "<tr><td><br /></td></tr>
                      </table>";                                                               
            ?>
        </table>    
        </td>        
    </tr>
    <tr>
        <td>
            <hr color='skyblue' />
            <h2 style='padding: 0px; margin: 0px;' >Promedio del Empleado</h2>
            <br />
            <table style="width: 60%; margin-left: 20%;">
                <tr><th>Enfoque de Evaluación</th><th>Promedios</th></tr>
                <tr><td><em style="color: blue;">Autoevaluación (No aplica)</em></td>
                    <th><em style="color: blue;"><?php echo $ArrayPromedioTotal[0];?></em></th></tr>
                <tr><td>Cliente interno</td>        <th><?php echo $ArrayPromedioTotal[1];?></th></tr>
                <tr><td>Colega de trabajo</td>      <th><?php echo $ArrayPromedioTotal[2];?></th></tr>
                <tr><td>Colaborador</td>            <th><?php echo $ArrayPromedioTotal[3];?></th></tr>
                <tr><td>Jefe inmediato</td>         <th><?php echo $ArrayPromedioTotal[4];?></th></tr>
                <tr><td colspan="2"><hr color='skyblue' /></td></tr>
                <?php
                      
                      $SumaPromedio = 0;
                      $j = 0;
	                   foreach($ArrayPromedioTotal as $value)
                       {
                            $j++;
                            if($value != "-" && $j != 1)
                                $SumaPromedio += $value;
                       }    
                       
                       if($ContadorTotal == 0)
                        $SumaPromedio =  0;
                        
                       else
                        $SumaPromedio = $SumaPromedio/$ContadorTotal;
                       
                ?>
                <tr><td class="tdleft"><strong>Promedio</strong></td><th><?php echo number_format($SumaPromedio, 2)?></th></tr>            
            </table>
            <br />
            <table>
            <tr><td><h3>Comentarios</h3></td></tr>
            <?php
                   $Comentarios = $bddr->prepare("SELECT ComentarioEmpleado FROM Evaluacion_Estado where IdEmpleado = $IDE and ComentarioEmpleado != '' and IdEvaluacion = $DataIdEva[0]" );
                   $Comentarios->execute();                   
                   
                   while($DataComentarios = $Comentarios->fetch())
                   {
                        echo "<tr><td><strong>Evaluador: </strong><em><br /> $DataComentarios[0]</em><br /><br /></td></tr>";
                   }
            ?>
            </table>
        </td>
    </tr>
</table>
</div>