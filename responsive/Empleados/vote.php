<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">
<table style="width: 100%;">
<tr>
    <td style="text-align: center;"><h2>Comit� de Seguridad e Higiene Ocupacional<br />Votaci�n 2015-2017</h2><hr color='skyblue' /><br /></td>
</tr>
<?php

    
    $ArrayV = array();
    $TotalVotos = 0;
    for($i = 1; $i <= 8; $i++)
    {
        $CantidadVotos = $bddr->prepare("SELECT * FROM Votacion where Voto = $i");
        $CantidadVotos->execute();
        array_push($ArrayV,$CantidadVotos->rowCount());
        $TotalVotos+=$CantidadVotos->rowCount();
    }
    
	
?>
<style>
.tdcenter
{
    text-align: center;    
}
</style>
<tr>
    <td>
        <table style="width: 80%; margin-left: 10%; ">
            <tr><th>N�</th><th>Cantidatos</th>                                    <td class="tdcenter"><strong>Conteo <br />de Votos</strong></td></tr>
            <tr><td>1</td><td>Lilia Ivett Padilla de Menj�var</td>                <td class="tdcenter"><?php echo $ArrayV[0]?></td></tr>
            <tr><td>2</td><td>Roberto Carlos Zelaya V�squez</td>                  <td class="tdcenter"><?php echo $ArrayV[1]?></td></tr>
            <tr><td>3</td><td>Rosa Mar�a Mart�nez de Mart�nez</td>                <td class="tdcenter"><?php echo $ArrayV[2]?></td></tr>
            <tr><td>4</td><td>El�as Alexander Marroqu�n Ard�n</td>                <td class="tdcenter"><?php echo $ArrayV[3]?></td></tr>
            <tr><td>5</td><td>Adriana Gabriela Salgado G�mez</td>                 <td class="tdcenter"><?php echo $ArrayV[4]?></td></tr>
            <tr><td>6</td><td>Jasm�n Guadalupe Geraldina Cu�llar de Zelada</td>   <td class="tdcenter"><?php echo $ArrayV[5]?></td></tr>
            <tr><td>7</td><td>Israel S�nchez Cruz</td>                            <td class="tdcenter"><?php echo $ArrayV[6]?></td></tr>
            <tr><td>8</td><td>Jaime C�sar Navarro Pineda</td>                     <td class="tdcenter"><?php echo $ArrayV[7]?></td></tr>
            <tr><td colspan="3"><hr color='skyblue' /></td></tr>
            <tr><td colspan="2" class="tdleft"> Total de Votos</td><td style="text-align: center;"><?php echo $TotalVotos; ?></td></tr>
        </table>
        <hr color='skyblue' />
    </td>    
</tr>
<tr>
    <td>
    <script src="js/highcharts.js"></script>
    <script src="js/exporting.js"></script>
    
    <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
    <script>
    $(function () {

    $(document).ready(function () {

        // Build the chart
        $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Votaciones - Comit� de Seguridad e Higiene Ocupacional'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Grafica de Datos',
                data: 
                [
                    ['Lilia Padilla',   <?php echo $ArrayV[0]?>],
                    ['Roberto Zelaya',   <?php echo $ArrayV[1]?>],
                    ['Rosa Mart�nez',   <?php echo $ArrayV[2]?>],
                    ['El�as Marroqu�n',   <?php echo $ArrayV[3]?>],
                    ['Adriana Salgado',   <?php echo $ArrayV[4]?>],
                    ['Jasm�n Cu�llar',   <?php echo $ArrayV[5]?>],
                    ['Israel Cruz',   <?php echo $ArrayV[6]?>],
                    ['Jaime Navarro',   <?php echo $ArrayV[7]?>],
                ]
            }]
        });
    });

});
    </script>
    </td>
</tr>
</table>
</div>