<?php

/**
 * @author Manuel
 * @copyright 2016
 */

if(isset($_POST['a�o']))
{
    $a�o = $_POST['a�o'];
    $_SESSION['a�o'] = $a�o;
}
else
{
    if($_SESSION['a�o'] == "")
    {
        $a�o = date("Y");
        $_SESSION['a�o'] = $a�o;        
    }                
    else
        $a�o = $_SESSION['a�o'];
}

$TotalPermisos = $bddr->prepare("SELECT Estado, COUNT(IdPermiso) FROM Permisos WHERE YEAR(Fecha) = $a�o GROUP BY Estado");
$TotalPermisos->execute();

$PermisosA = $PermisosP = $PermisosD = 0;

while($DataP = $TotalPermisos->fetch())
{
    if($DataP[0] == "Aceptada")
        $PermisosA = $DataP[1];
        
    if($DataP[0] == "Denegada")
        $PermisosD = $DataP[1];
        
    if($DataP[0] == "Pendiente")
        $PermisosP = $DataP[1];                    
}
?>

<script>

function CargarEmpleados()
{    
    var code = $("#Searching").val();    	
    document.getElementById('Fill').innerHTML = "<center><em style='color: green;'>Cargando datos...</em></center>";
    
    $.get("Empleados/ProcesosArea/_empleadosList.php", { code: code },
		function(resultado)
		{
            document.getElementById('Fill').innerHTML = "";
			if(resultado == false)   			
				alert("Error");
			
			else    				    
				$('#Fill').append(resultado);	
		}
	);    
}
</script>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px;">  
<table style="width: 100%;">
    <tr><td><h2 style="color: #1D7399;">Control de Permisos</h2></td>            
        <td style="text-align: right;">
        <form action="" method="post">
            Ver a�o:<select name="a�o" style="margin: 5px;">
                <?php
                       for($i = date("Y"); $i >= (date("Y") - 3) ; $i--)
                       {
                            if($i == $a�o)
                                echo "<option selected value='$i'>$i</option>";
                                
                            else
                                echo "<option value='$i'>$i</option>";
                       }   
                ?>
            </select>
            <input type="submit" name="Enviar" value="Ver" class="boton"  style="width: 50px;"/>
        </form>                  
        </td>
    </tr>
    <tr><td colspan="2"><hr color='#69ACD7'  /></td></tr>
    <tr>
        <td colspan="2">
            <table style="width: 100%;">
                <tr><td colspan="2"><h2>Resumen Institucional <?php echo "$a�o";?>    </h2></td></tr>
                <form action="?l=PermissionsControlView" method="post">
                <input type="hidden" name="tipoP" value="Pendientes" />
                <tr>
                    <td style="width: 40%;">Total de permisos pendientes:</td>
                    <td style="width: 30%;"><?php echo $PermisosP . " permisos";?></td>
                    <td style="width: 40%;"><input type="submit" class="boton" value="ver" style="width: 50px;" /></td>
                </tr>
                </form>       
                <form action="?l=PermissionsControlView" method="post">
                <input type="hidden" name="tipoP" value="Denegados" />
                <tr>
                    <td>Total de permisos denegados:</td>
                    <td><?php echo $PermisosD . " permisos";?></td>
                    <td><input type="submit" class="boton" value="ver" style="width: 50px;" /></td>
                </tr>
                </form>      
                <form action="?l=PermissionsControlView" method="post">
                <input type="hidden" name="tipoP" value="Aprobados" />                 
                <tr>
                    <td>Total de permisos aprobados:</td>
                    <td><?php echo $PermisosA . " permisos";?></td>
                    <td><input type="submit" class="boton" value="ver" style="width: 50px;" /></td>
                </tr>
                </form>
                <tr><td colspan="3"><hr color='#69ACD7' /></td></tr>
                <tr>
                    <td >Total de solicitados:</td>
                    <td><?php echo ($PermisosA + $PermisosD + $PermisosP ). " permisos";?></td>
                </tr>
                <tr><td colspan="3"><hr color='#69ACD7' /></td></tr>                                                
            </table> 
        </td>
    </tr>
    <tr>
        <td colspan="2">
        <form action="Empleados/ProcesosArea/PermisoArea.php" method="post" target="_blank">
            <table style="width: 100%;">
                <tr><td colspan="3"><h2>Reporte de �rea</h2></td></tr>
                <tr>
                    <td style="width: 40%;">Seleccione el �rea:</td>
                    <td colspan="2">
                        <select name="Area">
                            <?php
	                             $Areas = $bddr->prepare("SELECT * FROM AreasDeTrabajo");
                                 $Areas->execute();
                                 
                                 while($DataA = $Areas->fetch())
                                    echo "<option value='$DataA[0]'>$DataA[1]</option>";  
                            ?>
                        </select>
                    </td>                    
                </tr>
                <tr>
                    <td>Fecha de Inicio(A�o-Mes-D�a)</td>
                    <td><input style="text-align: center;" type="date" id="FechaInicio" name="FechaInicio" value="" pattern="^\d{4}\-\d{2}\-\d{2}$" /></td>
                    <td rowspan="2" style="text-align: left;"><input type="submit" name="Enviar" value="Imprimir" class="boton" /></td>
                </tr>
                <tr>
                    <td>Fecha de Fin(A�o-Mes-D�a)</td>
                    <td><input style="text-align: center;" type="date" id="FechaFin" name="FechaFin" value="" pattern="^\d{4}\-\d{2}\-\d{2}$" /></td>
                </tr>                
                <tr>
                    
                </tr>
            </table>
            <hr color='#69ACD7' />
            </form>
        </td>
    </tr>
<tr>
        <td colspan="2">
            <table style="width: 100%;">
                <tr><td colspan="3"><h2>Busqueda de Empleado</h2></td></tr>
                <tr>
                    <td style="width: 40%;">Escriba un nombre o un apellido:</td>
                    <td><input type="text" id="Searching" style="width: 100%;" onkeyup="CargarEmpleados()"  /></td>
                    <td></td>                    
                </tr>
                <tr>
                    <td colspan="3"><div id="Fill"></div></td>
                </tr>
            </table>
        </td>
    </tr>    
</table>
</div>