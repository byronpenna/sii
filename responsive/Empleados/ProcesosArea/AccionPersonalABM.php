<?php

/**
 * @author Jos� Manuel Calder�ns
 * @copyright 2014
 */

require '../../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Crear Accion de Personal")
    {
        $Accion = isset($_POST['Accion']) ? $_POST['Accion'] : array();  
        $Acciones = "";
        
        for ($index = 0 ; $index < count($Accion); $index ++)        
            $Acciones = $Acciones . $Accion[$index]. "-";
             
        $Insert = $bddr->prepare("Insert into AccionesPersonales values(null, :idae, :idaj, :cp, :ap, :sp, :action, 'Pendiente', :obs, :fecha, '')");
        $Insert->bindParam(':idae', $_POST['idae']);
        $Insert->bindParam(':idaj', $_POST['idaj']);
        $Insert->bindParam(':cp', $_POST['cargoP']);
        $Insert->bindParam(':ap', $_POST['cargoA']);
        $Insert->bindParam(':sp', $_POST['cargoS']);
        $Insert->bindParam(':action', $Acciones);
        $Insert->bindParam(':obs', $_POST['Observaciones']);
        $Insert->bindParam(':fecha', date("d-m-Y h:i A"));            
        $Insert->execute();   
        
        Redireccion("../../Empleado.php?l=PersonalAction&n=1");              
    }
    if($_POST['Enviar'] == "Cancelar")
    {
        $IdAction = $_POST['action'];
        $Insert = $bddr->prepare("Delete from AccionesPersonales where IdAccionPersonal = $IdAction");
        $Insert->execute();   
                
        Redireccion("../../Empleado.php?l=PersonalAction&n=3");               
    }
    
}
?>