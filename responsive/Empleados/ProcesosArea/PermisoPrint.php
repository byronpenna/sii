<?php
	session_start();
    require '../../net.php';
    
    $mes = $_POST['Mes'];
    $Cargos =  $_POST['cargo'];
    $IdEmp =  $_POST['idemp'];
                    
    $MisCargos = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, a.NombreAreaDeTrabajo
                                 FROM CargosAsignacion AS ca
                                 INNER JOIN Empleado AS e ON e.IdEmpleado = ca.IdEmpleado
                                 INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                                 INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
                                 WHERE ca.IdEmpleado = '$IdEmp' and ca.IdAsignacion = $Cargos");                                            
    $MisCargos->execute();
    
    $Datos = $MisCargos->fetch();
    
    
    $DatosJefe = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado, ca.IdAsignacion
                                 FROM CargosAsignacion AS ca
                                 INNER JOIN Jerarquia AS j ON ca.IdCargo = j.IdCargoSuperior
                                 INNER JOIN Cargos AS c ON c.IdCargos = j.IdCargoSuperior
                                 INNER JOIN Empleado AS e ON ca.IdEmpleado = e.IdEmpleado
                                 INNER JOIN AreasDeTrabajo AS at ON at.IdAreaDeTrabajo = c.IdArea_Fk
                                 WHERE j.IdCargos_Fk = $Datos[4]");                                            
    $DatosJefe->execute();
    $Jefe = $DatosJefe->fetch(); 
    
    if($mes=="01")$Mes = "Enero";
    if($mes=="02")$Mes = "Febrero";
    if($mes=="03")$Mes = "Marzo";
    if($mes=="04")$Mes = "Abril";
    if($mes=="05")$Mes = "Mayo";
    if($mes=="06")$Mes = "Junio";
    if($mes=="07")$Mes = "Julio";
    if($mes=="08")$Mes = "Agosto";
    if($mes=="09")$Mes = "Septiembre";
    if($mes=="10")$Mes = "Octubre";
    if($mes=="11")$Mes = "Noviembre";
    if($mes=="12")$Mes = "Diciembre";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Ficha del Empleado</title>
</head>
<script >
function cerrar() { setTimeout(window.close,1500); }
</script>
<body onload="window.print();cerrar();" style="font-family: Calibri; font-size: small;">
<div style="width: 94%; margin-left: 3%;">

<table style="width: 100%; margin-top: 40px;">
<tr>
    <td style='text-align: center;'><img src='../../images/logo.png' /></td>
    <td style='text-align: right;'><h2>Permisos Mensuales<br /> de <?php echo "$Mes";?></h2></td>
</tr>
<tr>
    <td colspan="2">
        <table style="width: 80%; margin-top: 20px; margin-bottom: 20px; margin-left: 10%;">
            <tr><td style="width: 30%;">Nombre del Empleado: </td><td><?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?></td></tr>
            <tr><td >Cargo Asignado:                 </td><td><?php echo "$Datos[5]"?></td></tr>
            <tr><td >Area de trabajo:                </td><td><?php echo "$Datos[6]"?></td></tr>
            <tr><td></td><td><?php echo $localidad;?></td></tr>
        </table>
    </td>
</tr>
<tr>
            <td colspan="4">
            <?php
                if(isset($_POST['Enviar']))
                {                            

                    $MisPermisos = $bddr->prepare("SELECT * FROM Permisos
                                                   WHERE MONTH(Fecha) = $mes AND YEAR(Fecha) = ".date("Y")." and IdAsignacionEmpleado = $Cargos
                                                   ORDER BY Fecha DESC");          
                    $MisPermisos->execute();

    
                  echo "<table style='width: 100%; text-align: center;'>
                        <tr><th>Tipo de Permiso</th><th>Fecha de Inicio</th><th>Fecha de Fin</th><th style='width: 30%'>Motivo</th><th>Estado</th></tr>";
        
                           while($Permiso = $MisPermisos->fetch())
                           {
                                if($Permiso[8] == "Pendiente")                                                      
                                    $Solicitud = "<em style='color: black'>Pendiente</em>";
                                    
                                if($Permiso[8] == "Aceptada")
                                   $Solicitud = "<em style='color: blue'>Aceptada</em>";

                                if($Permiso[8] == "Denegada")
                                   $Solicitud = "<em style='color: red'>Denegada</em>";
                                                         
                                echo "<tr>
                                          <td>$Permiso[9]</td>
                                          <td>$Permiso[3]<br />$Permiso[4]</td>
                                          <td>$Permiso[5]<br />$Permiso[6]</td>
                                          <td style='text-align: left;'>$Permiso[7]</td>
                                          <td>$Solicitud </td>
                                      </tr>";
                           }                 
                          echo "<tr>
                                    <td colspan='5'><br /><br /><br /><br />
                                        <table style='width: 80%;margin-left:10%; text-align: center;'>
                                            <tr>
                                                <td style='width:30%; border-top-style: solid;'>Empleado<br />$Datos[0] $Datos[1] $Datos[2] $Datos[3]</td> 
                                                <td style='width:40%;'></td>
                                                <td style='width:30%; border-top-style: solid;'>Jefe Inmediato<br />$Jefe[0] $Jefe[1] $Jefe[2] $Jefe[3]</td>
                                            </tr>                                            
                                        </table>
                                    </td>
                                </tr>";
                      }
                      else
                        echo "<tr><td colspan='5'><h2 style='color:red'>Error, no posees permisos en este mes</h2></td></tr>";                
                ?>
            </td>
        </tr>
    </table>
</div>
</body>
</html>