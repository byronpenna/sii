<?php

/**
 * @author Manuel
 * @copyright 2016
 */



if(isset($_POST['a�o']))
{
    $a�o = $_POST['a�o'];
    $_SESSION['a�o'] = $a�o;
}
else
{
    if($_SESSION['a�o'] == "")
        $a�o = date("Y");
        
    else
        $a�o = $_SESSION['a�o'];
}

$IdEmployee = $_POST['idp'];

if(isset($_SESSION['ide']))
{
    $IdEmployee = $_SESSION['ide'];
    unset($_SESSION['ide']);
}


$Empleados = $bddr->prepare("SELECT e.IdEmpleado, e.Nombre1, e.Nombre2, e.Nombre3, e.Apellido1, e.Apellido2, t.NombreAreaDeTrabajo, c.Cargo, ca.IdAsignacion
                             FROM Empleado as e 
                             INNER JOIN CargosAsignacion as ca on e.IdEmpleado = ca.IdEmpleado
                             INNER JOIN Cargos as c on ca.IdCargo = c.IdCargos 
                             INNER JOIN AreasDeTrabajo as t on c.IdArea_Fk = t.IdAreaDeTrabajo
                             where ca.IdEmpleado = $IdEmployee  and ca.FechaFin = '0000-00-00' 
                             GROUP BY e.IdEmpleado");

$Empleados->execute();                            
if($Empleados->rowCount() == 0)
Redireccion("../../Empleado.php?l=PermissionsControl&n=1");

$DataE = $Empleados->fetch();


$TotalPermisos = $bddr->prepare("SELECT p.Estado, COUNT(p.IdPermiso) FROM Permisos as p
                                 inner join CargosAsignacion as ca on p.IdAsignacionEmpleado = ca.IdAsignacion  
                                 WHERE YEAR(Fecha) = $a�o and ca.IdAsignacion = $DataE[8] GROUP BY Estado");
$TotalPermisos->execute();

$PermisosA = $PermisosP = $PermisosD = $PermisosPe = 0;

while($DataP = $TotalPermisos->fetch())
{
    if($DataP[0] == "Aceptada")
        $PermisosA = $DataP[1];
        
    if($DataP[0] == "Denegada")
        $PermisosD = $DataP[1];
        
    if($DataP[0] == "Pendiente")
        $PermisosP = $DataP[1];  
        
    if($DataP[0] == "Penalizaci�n")
        $PermisosPe = $DataP[1];                            
}

?>
<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px;">  
<table style="width: 100%;">
    <tr><td>
                <table style="width: 100%">
                <tr>
                    <td><h2>Permisos de Empleado</h2></td> 
                    <td style="text-align: right;"><a href="?l=PermissionsControl"><input type="button" value="<- Atras" class="boton" /></a></td>
                </tr>
            </table>
        <hr color='skyblue' />
        </td>
    </tr>
    <tr>
        <td>
        <?php
        echo "<table style='width: 60%;'>
                <tr><td>Empleado:</td><td>$DataE[1] $DataE[2] $DataE[3] $DataE[4]</td></tr>        
                <tr><td>Cargo:</td><td>$DataE[7]</td></tr>
                <tr><td>�rea:</td><td>$DataE[6]</td></tr>
             </table>";
        ?>
        </td>
    </tr>
    <tr>
        <td>
            <hr color='skyblue' />
            <table style="width: 100%;">
                <tr><td colspan="2"><strong>Resumen del a�o <?php echo "$a�o";?></strong></td></tr>
                <form action="?l=PermissionsEmployeeView" method="post">
                <input type="hidden" name="tipoP" value="Pendientes" />
                <input type="hidden" name="idp" value="<?php echo $IdEmployee;?>" />
                <tr>
                    <td style="width: 40%;">Total de permisos pendientes:</td>
                    <td style="width: 30%;"><?php echo $PermisosP . " permisos";?></td>
                    <td style="width: 40%;"><input type="submit" class="boton" value="ver" style="width: 50px;" /></td>
                </tr>
                </form>       
                <form action="?l=PermissionsEmployeeView" method="post">
                <input type="hidden" name="tipoP" value="Denegados" />
                <input type="hidden" name="idp" value="<?php echo $IdEmployee;?>" />
                <tr>
                    <td>Total de permisos denegados:</td>
                    <td><?php echo $PermisosD . " permisos";?></td>
                    <td><input type="submit" class="boton" value="ver" style="width: 50px;" /></td>
                </tr>
                </form>      
                <form action="?l=PermissionsEmployeeView" method="post">
                <input type="hidden" name="tipoP" value="Aprobados" />                
                <input type="hidden" name="idp" value="<?php echo $IdEmployee;?>" /> 
                <tr>
                    <td>Total de permisos aprobados:</td>
                    <td><?php echo $PermisosA . " permisos";?></td>
                    <td><input type="submit" class="boton" value="ver" style="width: 50px;" /></td>
                </tr>
                </form>
                <tr><td colspan="3"><hr color='#69ACD7' /></td></tr>
                <tr>
                    <td >Total de solicitados:</td>
                    <td><?php echo ($PermisosA + $PermisosD + $PermisosP ). " permisos";?></td>
                </tr>
                <tr><td colspan="3"><hr color='#69ACD7' /></td></tr>
                <form action="?l=PermissionsEmployeeView" method="post">
                <input type="hidden" name="tipoP" value="Penalizaci�n" />                
                <input type="hidden" name="idp" value="<?php echo $IdEmployee;?>" /> 
                <tr>
                    <td>Total de penalizaciones:</td>
                    <td><?php echo $PermisosPe . " penalizaciones";?></td>
                    <td><input type="submit" class="boton" value="ver" style="width: 50px;" /></td>
                </tr>
                </form>                                                                               
            </table>         
        </td>
    </tr>
</table>
</div>