<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */

    require '../../net.php';    
    
    if(isset($_POST['Enviar']))
    {
        if($_POST['Enviar'] == "Solicitar")
        {
            $FechaI = $_POST['AnioI']."-".$_POST['MesI']. "-" .$_POST['DiaI'];
            $FechaF = $_POST['AnioF']."-".$_POST['MesF']. "-" .$_POST['DiaF'];
            
            $horai = $_POST['HI'].":".$_POST['MI']. " " .$_POST['TI'] ;
            $horaf = $_POST['HF'].":".$_POST['MF']. " " .$_POST['TF'] ;
            
            $Insert = $bddr->prepare("Insert into Permisos values(null, :idae, :idaj, :fechaI, :hi, :fechaF, :hf, :motivo, 'Pendiente',:TipoP, '')");
            $Insert->bindParam(':idae',$_POST['ide']);
            $Insert->bindParam(':idaj',$_POST['idj']);
            $Insert->bindParam(':fechaI',$FechaI);
            $Insert->bindParam(':hi', $horai);
            $Insert->bindParam(':fechaF',$FechaF);
            $Insert->bindParam(':hf', $horaf);
            $Insert->bindParam(':motivo', $_POST['Motivo']);
            $Insert->bindParam(':TipoP', $_POST['TipoP']);
            $Insert->execute();
            
            $Id = $bddr->lastInsertId();
            
            $CorreoEmpleado = $bddr->prepare("SELECT Correo FROM DatosInstitucionales where IdEmpleado = " . $_POST['ide']);
            $CorreoEmpleado->execute();    
            $DataCE = $CorreoEmpleado->fetch();
                
            $CorreoEmpleado = $bddr->prepare("SELECT Correo FROM DatosInstitucionales where IdEmpleado = " . $_POST['idje']);
            $CorreoEmpleado->execute();    
            $DataCJ = $CorreoEmpleado->fetch();     
                 
            
            $to = "$DataCJ[0]";                        
            $subject = "Solicitud de Permiso";
            
            $message = "
                <h2>Solicitud de Permiso</h2><br />
            
                Nombre del Empleado: <br />
                ".$_POST['NombreEmpleado']." <br /><br />
                
                Fecha de Inicio: <br />
                $FechaI, $horai<br /><br />
                
                Fecha de Finalizaci�n: <br />
                $FechaF, $horaf<br /><br />
                  
                Motivo del Permiso:<br />
                ". $_POST['TipoP'] . ", ".$_POST['Motivo']."<br /><br />                
                
                Para mas detalles entrar pulsa 
                <a href='https://siipdb.fusalmo.org/PermisoView.php?p=$Id&u=".$_POST['idje']."'>Aqu�</a>
                
            ";

            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org, recursoshumanos@fusalmo.org" . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            
            //mail($to,$subject,$message,$headers);  
            Redireccion("../../Empleado.php?l=Permissions&n=1"); 
        }
        
        if($_POST['Enviar'] == "Denegar" || $_POST['Enviar'] == "Aceptar")
        {   
            if($_POST['Enviar'] == "Denegar")
                $estado = "Denegada"; 
                
            if($_POST['Enviar'] == "Aceptar")
                $estado = "Aceptada";

            $Update = $bddr->prepare("UPDATE  Permisos SET  Estado =  '$estado', ComentarioJefe = :Comentario WHERE  IdPermiso = :idp");
            $Update->bindParam(':idp', $_POST['idp']);
            $Update->bindParam(':Comentario', $_POST['Comentario']);            
            $Update->execute();

            $CorreoEmpleado = $bddr->prepare("SELECT i.Correo FROM DatosInstitucionales as i 
                                              INNER jOIN Permisos as e on i.IdEmpleado = e.IdAsignacionEmpleado 
                                              where e.IdPermiso = " . $_POST['idp']);
            $CorreoEmpleado->execute();    
            $DataCE = $CorreoEmpleado->fetch();
            
            $Permiso = $bddr->prepare("SELECT * FROM Permisos Where IdPermiso = " . $_POST['idp']);
            $Permiso->execute();
            
            $DataP = $Permiso->fetch();     
                
            $to = "$DataCE[0]";
            $subject = "Solicitud de Permiso";
            
            $message = "
                <h2>Solicitud de Permiso</h2><br />
                
                Solicitud de Permiso $estado  <br /><br />         
                
                Fecha de Inicio: <br />
                $DataP[3], $DataP[4]<br /><br />
                
                Fecha de Finalizaci�n: <br />
                $DataP[5], $DataP[6]<br /><br />
                  
                Motivo del Permiso:<br />
                $DataP[9]: $DataP[7]<br /><br />
                
                Comentario sobre el Permiso:<br />
                ".$_POST['Comentario']."<br /><br />
                
                Para para verificar la solicitud entra en:<br />    
                <a href='https://siipdb.fusalmo.org' >https://siipdb.fusalmo.org</a>
                
            ";
            
            // More headers
            $headers .= "From: <siipdb_noreply@fusalmo.org>" . "\r\n";
            $headers .= "Cc: desarrollo@fusalmo.org, recursoshumanos@fusalmo.org" . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

            //mail($to,$subject,$message,$headers);            
            
            if($_SESSION["u"] != "")
            {
                echo"<script language='javascript'>alert('Proceso exitoso');</script>";
                Redireccion("../../Empleado.php?l=Permissions&n=6");     
            }
                        
            Redireccion("../../Empleado.php?l=Permissions&n=2"); 
        }
        
        if($_POST['Enviar'] == "Penalizar")
        {
            $FechaI = $_POST['AnioI']."-".$_POST['MesI']. "-" .$_POST['DiaI'];
            $FechaF = $_POST['AnioF']."-".$_POST['MesF']. "-" .$_POST['DiaF'];
            
            $horai = $_POST['HI'].":".$_POST['MI']. " " .$_POST['TI'] ;
            $horaf = $_POST['HF'].":".$_POST['MF']. " " .$_POST['TF'] ;
            
            $Insert = $bddr->prepare("Insert into Permisos values(null, :idae, :idaj, :fechaI, :hi, :fechaF, :hf, :motivo, 'Penalizaci�n','Penalizaci�n', '')");
            $Insert->bindParam(':idae',$_POST['ide']);
            $Insert->bindParam(':idaj', $_SESSION["IdUsuario"]);
            $Insert->bindParam(':fechaI',$FechaI);
            $Insert->bindParam(':hi', $horai);
            $Insert->bindParam(':fechaF',$FechaF);
            $Insert->bindParam(':hf', $horaf);
            $Insert->bindParam(':motivo', $_POST['Motivo']);
            $Insert->execute();  
            
            $_SESSION['ide'] = $_POST['ide'];
            
            Redireccion("../../Empleado.php?l=PermissionsEmployee&n=1");       
                  
                  
        }
        
        if($_POST['Enviar'] == "Borrar")
        {            
            $Eliminar = $bddr->prepare("Delete from Permisos where IdPermiso = :idp");
            $Eliminar->bindParam(':idp',$_POST['idp']);
            $Eliminar->execute();  
            
            echo "<script>function cerrar() { setTimeout(window.close,500); }</script><body onload='cerrar();'></body>";
                   
        }        
        
        
    }
    else
        Redireccion("../../Empleado.php?l=Permissions&n=4"); 
?>
