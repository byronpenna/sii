<?php

/**
 * @author Manuel Calderón
 * @copyright 2014
 */

?>

<div style="float:right; width: 50%; text-align: left; background-color: white; border-radius: 10px; padding:10px">    
    <h2 style="color: blue;">Mis Compañeros/as de Area</h2>
    
    <?php

       $MisDatos =   $bddr->prepare("SELECT ca.IdEmpleado, ca.FechaInicio, ca.FechaFin, a.IdAreaDeTrabajo, a.NombreAreaDeTrabajo, c.IdCargos, c.Cargo
                                     FROM CargosAsignacion AS ca                                         
                                     INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                                     INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
                                     WHERE ca.IdEmpleado = $IdEmp");
                                   
       $MisDatos->execute();
       $Datos = $MisDatos->fetch();
       
       
       $misCompañeros = $bddr->prepare("SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, e.FechaNacimiento, c.IdCargos, c.Cargo, ca.FechaInicio, ca.FechaFin
                                        FROM Empleado AS e
                                        INNER JOIN CargosAsignacion AS ca ON e.IdEmpleado = ca.IdEmpleado
                                        INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                                        INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
                                        WHERE a.IdAreaDeTrabajo = $Datos[3] AND ca.FechaFin = '0000-00-00'
                                        ORDER BY  e.Apellido1 ASC ");
       $misCompañeros->fetch();
       $misCompañeros->execute();   
       
       echo "<br /><table style='width:110%; margin-left:-44%;'>
             <tr style='height: 35px;'><th colspan='2' class='tdleft'>Area de Trabajo: </th><th colspan='2'>$Datos[4]</th>
             <tr><td colspan='4'><hr color='skyblue' /></td></tr>
             <tr style='height: 35px;'><th colspan='2'>Nombre </th><th>Fecha de Nacimiento</th><th>Cargo</th></tr>";
       
       if($misCompañeros->rowCount() > 0)
       {
           $i = 0; 
           while($DatosCompas = $misCompañeros->fetch())
           {
                $i++;
                echo "<tr style='height: 30px;'><td>$i</td><td>$DatosCompas[0] $DatosCompas[1] $DatosCompas[2]</td>
                          <td style='color: blue; text-align: center '>$DatosCompas[4]</td><td style='color: blue'>$DatosCompas[6]</td></tr>";
           }
       }
       else
            echo "<tr><td style='color: red'><h2>Al parecer no hay otra persona designada en tu área</h2></td>";

       
        echo "</table>";
        
    ?>
    
</div>
<div class="clear"></div>