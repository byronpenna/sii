<?php

/**
 * @author Manuel
 * @copyright 2016
 */

require '../../net.php';

$Searching = $_GET['code'];

$Empleados = $bddr->prepare("SELECT e.IdEmpleado, e.Nombre1, e.Nombre2, e.Nombre3, e.Apellido1, e.Apellido2, t.NombreAreaDeTrabajo, c.Cargo
                             FROM Empleado as e 
                             INNER JOIN CargosAsignacion as ca on e.IdEmpleado = ca.IdEmpleado
                             INNER JOIN Cargos as c on ca.IdCargo = c.IdCargos 
                             INNER JOIN AreasDeTrabajo as t on c.IdArea_Fk = t.IdAreaDeTrabajo
                             where (e.Nombre1 like '%$Searching%' or 
                                   e.Nombre2 like '%$Searching%' or
                                   e.Nombre3 like '%$Searching%' or
                                   e.Apellido1 like '%$Searching%' or
                                   e.Apellido2 like '%$Searching%') and
                                   FechaFin = '0000-00-00' 
                             GROUP BY e.IdEmpleado");

$Empleados->execute();                            

$print = "<table style='width: 100%; '>
          <tr><th>�rea</th><th>Cargo</th><th>Empleado</th><th></th></tr>";

if($Empleados->rowCount() > 0)
{
    while($DataE = $Empleados->fetch())
    {
        $print .=  "<tr><td>$DataE[6]</td>
                        <td>$DataE[7]</td>
                        <td>$DataE[1] $DataE[2] <br /> $DataE[3] $DataE[4]</td>
                        <td>
                            <form action='?l=PermissionsEmployee' method='post'>
                                <input type='hidden' name='idp' value='$DataE[0]' />
                                <input type='submit' name='Enviar' value='Ver' class='boton' style='width: 40px;' />
                            </form>
                        </td>
                    </tr>";
    }    
}
else
    $print .=  "<tr><td colspan='4' style='color: red; text-align: center;'>No hay empleados activos en esta busqueda</td></tr>";


$print .=  "</table>";


echo utf8_encode($print);
?>