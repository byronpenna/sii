<?php
        $MisDatos =   $bddr->prepare("SELECT ca.IdEmpleado, ca.FechaInicio, ca.FechaFin, a.IdAreaDeTrabajo, a.NombreAreaDeTrabajo, c.IdCargos, c.Cargo, ca.IdAsignacion
                                      FROM CargosAsignacion AS ca
                                      INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                                      INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk                                        
                                      WHERE ca.IdEmpleado = '$IdEmp' AND ca.FechaFin =  '0000-00-00'");
                                   
       $MisDatos->execute();
       
       $aux = $MisDatos->fetch();
        
        if(isset($_POST['MiCargo']))
            $IdCharge = $_POST['MiCargo']; 
        
        else
            $IdCharge = $aux[5];
            
        
        $ida = $aux[7]
?>

<script>
    $(document).ready(function(){
	   $("#Empleado").change(function(){cargar_boleta(this.form);});       
    });
    
    function cargar_boleta(form)
    {        
        var code = $("#Empleado").val();
        var code2 = <?php echo "'$ida'";?>;        	
        document.getElementById("Data").innerHTML="<br /><br /><center><em style='color: green;'>Cargando datos... </em></center><br /><br />";
        
        if(code != "..."){
        	$.get("Empleados/ProcesosArea/AccionPersonalData.php", { code: code , code2: code2},
        		function(resultado)
        		{
        			if(resultado == false)        			
        				alert("Error");
        			
        			else
                    {
                        document.getElementById("Data").innerHTML = "";
                        $('#Data').append(resultado);	        	
                    }  			
        						
        		}        
        	);
        }
        else        
            document.getElementById("Data").innerHTML="<br /><br /><center><em style='color: blue;'>Esperando datos... </em></center><br /><br />";
        
    }
    
    function Calcular()
    {
       var sueldoNuevo = document.getElementById('cargoS').value;
       var sueldoViejo = document.getElementById('SA').value
            
       document.getElementById('IS').value = sueldoNuevo - sueldoViejo;
       document.getElementById('NS').value = sueldoNuevo;
       
    }

</script>

<?php

/**
 * @author Jos� Manuel Calder�n
 * @copyright 2014
 */
 
       $MisDatos =   $bddr->prepare("SELECT ca.IdEmpleado, ca.FechaInicio, ca.FechaFin, a.IdAreaDeTrabajo, a.NombreAreaDeTrabajo, c.IdCargos, c.Cargo
                                     FROM CargosAsignacion AS ca
                                     INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
                                     INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk                                        
                                     WHERE ca.IdEmpleado = '$IdEmp' AND ca.FechaFin =  '0000-00-00'");
                                   
       $MisDatos->execute();
       
                    
?>

<div style="float:right; width: 75%; text-align: left; background-color: white; border-radius: 10px; padding:10px">   
    
    <table style="width: 100%;">
        <form action="Empleado.php?l=PersonalActionForm" method="post" >
        <tr><td colspan="2" style="text-align: right;">Generar acci�n como:</td>
            <td colspan="2" style="text-align: left; padding-left: 20px;"><select name="MiCargo" id="MiCargo"> 
                <?php                                                    
	                   while($Datos = $MisDatos->fetch())
                       {
                            if($IdCharge == $Datos[5])
                                echo "<option value='$Datos[5]' selected='true' >$Datos[6]</option>";  
                            
                            else
                                echo "<option value='$Datos[5]' >$Datos[6]</option>";
                            
                            if($IdCharge == "")  
                                $IdCharge = $Datos[5];                                                                                        
                       }
                ?>
                </select>
            <input type="submit" value="Cambiar"  class="boton" /></td>
        </tr>
        </form>
        <tr><td colspan="4"><hr color='#69ACD7'  /></td></tr>
        
        
        <tr><td colspan="4"><h2>Formulario de Acci�n de Personal</h2></td></tr>

        <tr><td class="tdleft" >Empleado:</td>
            <td><select name="Empleado" id="Empleado"> 
                <option value="...">...</option>
                <?php 
                        $Empleados = $bddr->prepare("SELECT e.IdEmpleado, e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2 
                                                     FROM  Empleado AS e
                                                     INNER JOIN CargosAsignacion AS ca ON e.IdEmpleado = ca.IdEmpleado
                                                     INNER JOIN Jerarquia AS j ON ca.IdCargo = j.IdCargos_Fk
                                                     WHERE IdCargoSuperior = $IdCharge AND ca.FechaFin =  '0000-00-00'");
                                                     
                        $Empleados->execute();
                        
                        if($Empleados->rowCount() > 0)
                        {
                            while($ACargo = $Empleados->fetch())                        
                                echo "<option value='$ACargo[0]' >$ACargo[1] $ACargo[2] $ACargo[3] $ACargo[4]</option>";
                        }
                        else
                            echo "<option value='...' >No posees a cargo ningun empleado</option>";
                        
                ?>
                </select>
            </td>
            <td style="text-align: right;">Fecha del Creaci�n: </td>
            <td style="padding-left: 10px;"><?php echo date("d-m-Y");?></td>            
        </tr>
        <tr>
            <td colspan="4">
                <div id="Data" style="width: 100%;"><br /><br /><center><em style="color: blue;">Esperando datos... </em></center><br /><br /></div>
            </td>
        </tr>
    </table>
</div>