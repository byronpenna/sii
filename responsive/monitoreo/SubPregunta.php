<?php
    
    if($_SESSION["encuesta"] != "" )
    {
        $query = "Select * from Monitoreo_Encuesta as enc
                  inner join Monitoreo_Boleta as bol 
                  on bol.IdEncuesta = enc.IdEncuesta  
                  where bol.IdBoleta = :idb";
                  
        $datos = $bdd->prepare($query);
        $datos->bindParam('idb', $_SESSION["boleta"]);        
        $datos->execute();
        $datosEncuesta = $datos->fetch();
    }
    else
        Redireccion("../monitoreo.php");
?>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
    <a href="/monitoreo.php?m=bole" class='flecha' style='text-decoration:none'>Boleta</a> ->
    <a href="/monitoreo.php?m=Whq" class='flecha' style='text-decoration:none'>Preguntas</a>
</div>
<?php 
    if(isset($_GET['n']))
    {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Tema Agregados Exitosamente</div>";

        elseif($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Pregunta Agregado Exitosamente</div>";            
    }
    
        $query = "Select * from Monitoreo_Pregunta as p
                  inner join Monitoreo_Tema as t on p.IdTema = t.IdTema 
                  where IdPregunta = :idp";
                  
        $datos2 = $bdd->prepare($query);
        $datos2->bindParam('idp', $_GET['ip']);        
        $datos2->execute();
        $datosPregunta = $datos2->fetch();    
    
    
?>
<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; ">
            <table style="width: 90%; margin-left: 5%; ">
            <form action="monitoreo/PreguntasABM.php" method="post" >
                <input type="hidden" name="idp" value="<?php echo $datosPregunta[0];?>"  />
                <input type="hidden" name="idt" value="<?php echo $datosPregunta[1];?>"  />
                    <tr>
                    	<th colspan="2"><h2>Agregar una Sub-Pregunta </h2></th>
                    </tr>
                    <tr><td style="width: 40%; text-align: right; padding-right: 20px;">Tema:</td>
                        <td style="color: blue;"><?php echo $datosPregunta[6];?></td></tr>
                    <tr><td style="width: 40%; text-align: right; padding-right: 20px;">Pregunta:</td>
                        <td style="padding-top: 20px; color: blue;"><?php echo $datosPregunta[2];?></td></tr>
                    <tr><td style="width: 40%; text-align: right; padding-right: 20px;">Criterios a tomar en Cuenta:</td>
                        <td style="padding-top: 20px;">                                             
                                <?php
    	                           $temas = $bdd->prepare("SELECT * FROM Monitoreo_Criterios where IdEncuesta = :id");
                                   $temas->bindParam('id', $_SESSION["encuesta"]);
                               	   $temas->execute();
                                   
                                   if($temas->rowCount() > 0)
                                        while($Tdatos = $temas->fetch())
                                            echo "<input type='Checkbox' style='margin-left: 20%;' Name='criterio[]' value='$Tdatos[0]' />$Tdatos[1]<br />";
                                            
                                   else
                                        echo "Error Criterios no agregados";    
                                ?>                            
                        </td>
                    </tr>
                    <tr><td style="text-align: right; padding-right: 20px;">Sub-pregunta:</td>
                        <td style="color: red; padding-top: 20px;" ><textarea required="true" title="Pregunta requerida" name="pregunta" style="width: 300px; height: 50px;"></textarea>*</td></tr>
                         
                    <tr><td colspan="2" style="text-align: center;"><input type="submit" value="Insertar" name="enviar" class="boton" /></td></tr>
                 </form>
            </table>           
</div>
<div class="clr"></div>