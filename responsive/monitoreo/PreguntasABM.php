<?php

/**
 * @author Manuel
 * @copyright 2013
 */

require '../net.php';

if(isset($_POST['enviart']))
{    
    if($_POST['enviart'] == "Agregar")
    {
        $insert = $bdd->prepare("Insert into Monitoreo_Tema 
                                 values (null, :boleta, :tema) ");
                                 
        $insert->bindParam(':boleta', $_POST['idboleta']);
        $insert->bindParam(':tema', $_POST['tema']);
        $insert->execute();
        
        Redireccion("../monitoreo.php?m=Whq&n=1");          
        
    }
}elseif(isset($_POST['enviar']))
{
    if($_POST['enviar'] == "Agregar")
    {
        $insert = $bdd->prepare("Insert into Monitoreo_Pregunta 
                                 values (null, :tema, :pregunta, 'Pregunta')");
                                 
        $insert->bindParam(':tema', $_POST['temap']);
        $insert->bindParam(':pregunta', $_POST['pregunta']);
        $insert->execute();
        
        Redireccion("../monitoreo.php?m=Whq&n=2");          
    } 
    elseif($_POST['enviar'] == "Insertar")
    {
        
        $insert = $bdd->prepare("Insert into Monitoreo_Pregunta 
                                 values (null, :tema, :pregunta, 'Sub-Pregunta')");
                                 
        $insert->bindParam(':tema', $_POST['idt']);
        $insert->bindParam(':pregunta', $_POST['pregunta']);
        $insert->execute();
                
        $pregunta = $bdd->prepare("SELECT * FROM Monitoreo_Pregunta where IdTema = :tema and Pregunta = :pregunta");
        $pregunta->bindParam(':tema', $_POST['idt']);
        $pregunta->bindParam(':pregunta', $_POST['pregunta']);
   	    $pregunta->execute();              
        
        $idPb = $pregunta->fetch();
           
        $criterios = isset($_POST['criterio']) ? $_POST['criterio'] : array();    
           
        foreach($criterios as $idc) {
            
            $insert2 = $bdd->prepare("Insert into Monitoreo_SubPregunta 
                                     values (null, :idp , :idc , :ids)");
                                 
            
            $insert2->bindParam(':idp', $_POST['idp']);
            $insert2->bindParam(':idc', $idc);
            $insert2->bindParam(':ids', $idPb[0]);
            $insert2->execute();
            
        }
        
        Redireccion("../monitoreo.php?m=Whq&n=4");    
    }
    
}
elseif($_GET['ip'])
{
        $eliminar = $bdd->prepare("Delete from Monitoreo_Pregunta where IdPregunta = :id");
                                 
        $eliminar->bindParam(':id', $_GET['ip']);        
        $eliminar->execute();
        
        Redireccion("../monitoreo.php?m=Whq&n=3"); 
}

    

else 
Redireccion("../monitoreo.php");
    
    
?>