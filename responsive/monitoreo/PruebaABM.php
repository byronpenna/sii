<?php

/**
 * @author Manuel
 * @copyright 2013
 */

require '../net.php';

if(isset($_POST['enviar']))
{    
    if($_POST['enviar'] == "Insertar")
    {
        $Dato = $bdd->prepare("Insert into Monitoreo_Encuesta 
                               values(NULL, :nombre, :anio, :usuario, :Descripcion)");
        
        $Dato->bindParam('nombre', $_POST['Nombre']);
        $Dato->bindParam('anio', date("Y"));
        $Dato->bindParam('usuario', $_SESSION["Usuario"] );
        $Dato->bindParam('Descripcion', $_POST['Descripcion']);
        $Dato->execute();
        
        Redireccion("../monitoreo.php?n=1");
    }
    elseif($_POST['enviar'] == "Eliminar")
    {
        $Dato = $bdd->prepare("Delete from Monitoreo_Encuesta where IdEncuesta = :id");
        
        $Dato->bindParam('id', $_POST['id']);
        $Dato->execute();
        
        Redireccion("../monitoreo.php?n=2");
    }
    elseif($_POST['enviar'] == "Detalles")
    {
        $_SESSION["encuesta"] = $_POST['id'];
        Redireccion("../monitoreo.php?m=det");
        
    }

    elseif($_POST['enviar'] == "Modificar")
    {
        $Dato = $bdd->prepare("update Monitoreo_Encuesta set
                              Nombre = :nombre, 
                              Descripcion = :Descripcion
                              where IdEncuesta = :id");
        
        $Dato->bindParam('nombre', $_POST['Nombre']);
        $Dato->bindParam('Descripcion', $_POST['Descripcion']);
        $Dato->bindParam('id', $_SESSION["encuesta"]);
        $Dato->execute();

        Redireccion("../monitoreo.php?m=det&n=1");
    }    
    else
        Redireccion("../monitoreo.php");
    
}
else
    Redireccion("../monitoreo.php");
?>