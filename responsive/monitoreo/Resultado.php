<script src="js/RGraph.common.core.js" ></script>
<script src="js/RGraph.radar.js" ></script>
<?php

    if($_SESSION["boleta"] != "" || $_SESSION["IdDato"] != "" )
    {
        if($_SESSION["IdDato"] == "")
        {
            $query = "SELECT e.Nombre, b.NombreBoleta, df.Nombre 
                      FROM Monitoreo_Encuesta           AS e
                      INNER JOIN Monitoreo_Boleta       AS b ON e.IdEncuesta      = b.IdEncuesta
                      INNER JOIN Monitoreo_Resultado    AS r ON b.IdBoleta        = r.IdBoleta
                      INNER JOIN InscripcionDatoFusalmo AS i ON r.IdUsuario       = i.IdUsuario
                      INNER JOIN DatoFusalmo            AS df ON df.IdDatoFUSALMO = i.IdDatoFUSALMO
                      WHERE r.IdBoleta = :id
                      GROUP BY i.IdUsuario";
            
            $datos = $bdd->prepare($query);
            $datos->bindParam('id', $_SESSION["boleta"]);
        }
        
        else
        {
            $query = "SELECT e.Nombre, b.NombreBoleta, df.Nombre 
                      FROM Monitoreo_Encuesta           AS e
                      INNER JOIN Monitoreo_Boleta       AS b ON e.IdEncuesta      = b.IdEncuesta
                      INNER JOIN Monitoreo_Resultado    AS r ON b.IdBoleta        = r.IdBoleta
                      INNER JOIN InscripcionDatoFusalmo AS i ON r.IdUsuario       = i.IdUsuario
                      INNER JOIN DatoFusalmo            AS df ON df.IdDatoFUSALMO = i.IdDatoFUSALMO
                      WHERE r.IdBoleta = :id AND i.IdDatoFUSALMO = :idI
                      GROUP BY i.IdUsuario";
                      
            $datos = $bdd->prepare($query);
            
            $datos->bindParam('id', $_SESSION["boleta"]);
            $datos->bindParam('idI', $_SESSION["IdDato"]);
        }
        
                  

        $datos->execute();
        $datosEncuesta = $datos->fetch();
    }
    else
        Redireccion("../monitoreo.php");
        
if($_SESSION["TipoUsuario"] == "Consultor" || $_SESSION["TipoUsuario"] == "Administrador" ) { ?>
    <div class="Raiz">
        <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
        <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
        <a href="/monitoreo.php?m=bole" class='flecha' style='text-decoration:none'>Boleta</a> ->
        <a href="/monitoreo.php?m=res" class='flecha' style='text-decoration:none'>Respuestas</a>
    </div>
<?php } ?>

<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px;">
<table class='Datos' style="margin-left: 8%; width: 84%;">    
    <tr>
        <td colspan="4" style="text-align: center;"><h2>Resultado General por Pregunta</td>
        <td style="text-align: right;">
        <?php 	if($_SESSION["TipoUsuario"] == "Consultor" || $_SESSION["TipoUsuario"] == "Administrador" ) {?>
            <a href="monitoreo.php?m=Stad" style="text-decoration: none;">
                <input style="width: 150px;" type="button" class="boton" value="Datos Cuantitativos" />
            </a>
        <?php } ?>
        </td>
    </tr>
    <tr>
        <td style="text-align: right; padding-right: 20px; width: 25%;">Encuesta:</td>
        <td colspan="4" style=" width: 70%;"> <?php echo $datosEncuesta[0];?> </h2></td>
    </tr>
    <tr>
        <td style="text-align: right; padding-right: 20px;">Boleta:</td>
        <td colspan="4"> <?php echo $datosEncuesta[1];?> </h2></td>
    </tr>
    <?php if($_SESSION["IdDato"] == ""){?>
    <tr>
        <td style="text-align: right; padding-right: 20px;">Dato FUSALMO:</td>
        <td colspan="4">Todos los Involucrados</h2></td>
    </tr>           
    <?php }else{?>
    <tr>
        <td style="text-align: right; padding-right: 20px;">Dato FUSALMO:</td>
        <td colspan="2"> <?php echo $datosEncuesta[2];?> </h2></td>
        <td colspan="2">
        <?php if($_SESSION["IdDato"] != ""){?>
                        <form action="monitoreo.php?m=compare" method="post">
                              <input type="submit" class="boton" value="Comparar" />
                        </form>
        <?php }?>
        </td>
    </tr>   
    <?php }?>
    <tr>
        <td style="text-align: right; padding-right: 20px;">Encuestados:</td>
        <td colspan="2"> <?php echo $datos->rowCount();?> jovenes  </h2></td>
        <td colspan="2">
        <?php if($_SESSION["IdDato"] != ""){?>
                        <form action="monitoreo/ReporteEstudiante.php">
                              <input type="submit" class="boton" value="Ver Jovenes" />
                        </form>
        <?php }?>
        </td>
    </tr>         
<tr><td colspan="5"><hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" /></td></tr>
<tr><td colspan="5">
        <table rules='all' style="width: 90%; margin-left: 5%;">
            <tr>
                <td style="text-align: left; width: 70%;"><h2>Temas</h2></td>
                <td style="width: 10%;text-align: center">Media</td>
            </tr>        
            <?php
            
	        $query = "SELECT * 
                      FROM  Monitoreo_Tema
                      WHERE IdBoleta = :id";
                  
            $datosTema = $bdd->prepare($query);
            $datosTema->bindParam('id', $_SESSION["boleta"]);
            $datosTema->execute();               
            
            $j = 0;
            
            $ArrayPromedios = array();
            $ArrayTemas = array();
            
            while($temas = $datosTema->fetch())
            { 
                $j++;
                
                if($_SESSION["IdDato"] != "")
                {
           	            $query = "SELECT r.IdBoleta, r.IdPregunta, r.Fecha, p.Pregunta, SUM( c.ValorNumero ) , COUNT( r.IdPregunta )  
                                  FROM  Monitoreo_Resultado AS r
                                  INNER JOIN Monitoreo_Pregunta AS p ON r.IdPregunta = p.IdPregunta
                                  INNER JOIN Monitoreo_Criterios AS c ON r.Resultado = c.IdCriterio
                                  INNER JOIN InscripcionDatoFusalmo as i ON i.IdUsuario = r.IdUsuario 
                                  WHERE p.categoria =  'Pregunta' AND p.IdTema = :id AND i.IdDatoFUSALMO = :idi 
                                  GROUP BY r.IdPregunta";
                                  
                        $datosPromedio = $bdd->prepare($query);
                        $datosPromedio->bindParam(':id', $temas[0]);                
                        $datosPromedio->bindParam(':idi', $_SESSION["IdDato"]);                    
                }
                else
                {
   	                $query = "SELECT r.IdBoleta, r.IdPregunta, r.Fecha, p.Pregunta, SUM( c.ValorNumero ) , COUNT( r.IdPregunta )  
                              FROM  Monitoreo_Resultado AS r
                              INNER JOIN Monitoreo_Pregunta AS p ON r.IdPregunta = p.IdPregunta
                              INNER JOIN Monitoreo_Criterios AS c ON r.Resultado = c.IdCriterio
                              INNER JOIN InscripcionDatoFusalmo as i ON i.IdUsuario = r.IdUsuario 
                              WHERE p.categoria =  'Pregunta' AND p.IdTema = :id  
                              GROUP BY r.IdPregunta";
                                  
                    $datosPromedio = $bdd->prepare($query);
                    $datosPromedio->bindParam(':id', $temas[0]);                
                }
                $datosPromedio->execute();
                
                
                $PromedioPreguntas = $SUMPromedioPreguntas = $Promedio = 0;
                
                if($datosPromedio->rowCount() > 0)
                {                     
                    while($datosP = $datosPromedio->fetch())
                    {
                        $PromedioPreguntas = $datosP[4] / $datosP[5] ;  
                        $SUMPromedioPreguntas = $PromedioPreguntas + $SUMPromedioPreguntas;
                    }   
                    
                    $Promedio = $SUMPromedioPreguntas / $datosPromedio->rowCount();
                    
                    $texto = number_format($Promedio,2);
                    array_push($ArrayPromedios,$texto);
                    array_push($ArrayTemas,$temas[2]);
                }
                else
                    $texto = "-";
                
                echo "<tr>
                          <td style='color: darkblue; padding-top: 10px; padding-bottom: 10px;'>$j - 
                          <a style='text-decoration:none; color: darkblue;' href='monitoreo.php?m=resPre&t=$temas[0]'>$temas[2] </a></td>
                          <td style='text-align: center'>" . $texto ."</td>
                      </tr>";
            }
        ?>
        </table>
    </td>
</tr>
<?php
	        $query2 = "(SELECT * FROM Monitoreo_Criterios
                        ORDER BY Posicion ASC LIMIT 1)
               
                        UNION ALL 
               
                       (SELECT * FROM Monitoreo_Criterios
                        ORDER BY Posicion DESC LIMIT 1)";

            $datosCriterios = $bdd->prepare($query2);
            $datosCriterios->bindParam('id', $_SESSION["encuesta"]);
            $datosCriterios->execute();
            
            $Minimo = "";
            $Ideal = "";
            
            while($criterios = $datosCriterios->fetch())
            {
                if($Ideal == "")
                    $Ideal = $criterios[2];
                
                elseif($Minimo == "")
                    $Minimo  = $criterios[2];
            }
            echo "<tr>
                      <td colspan='4' style='text-align: right; padding-right: 20px;'>Valor Minimo: </td>
                      <td style='width: 10%; text-align: center;'>$Minimo</td>
                  </tr>
                  <tr>
                      <td colspan='4' style='text-align: right; padding-right: 20px;'>Valor Ideal:</td>
                      <td style='text-align: center;'>$Ideal</td>
                  </tr>";
                  
            
                    
?> 
<tr><td colspan="5"><hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" /></td></tr>

</table>

<script src="js/highcharts.js"></script>
<script src="js/highcharts-more.js"></script>
<script src="js/exporting.js"></script>

<div id="container" style="min-width: 400px; max-width: 600px; height: 400px; margin: 0 auto"></div>

<script>
$(function () {
	$('#container').highcharts({
	            
	    chart: {
	        polar: true,
	        type: 'line'
	    },
	    
	    title: {
	        text: 'Ideal vs Promedios vs Minimo',
	        x: -80
	    },
	    
	    pane: {
	    	size: '80%'
	    },
	    
	    xAxis: {
	        categories: [
                        <?php
                               $k = 0;
	                           foreach($ArrayTemas as $value)
                               {
                                    $k++;
                                    
                                    if($k < count($ArrayTemas))
                                        echo "'". substr($value,0 , 25) ."...',";  
                                    
                                    else
                                        echo "'". substr($value,0 , 25) ."...'";   
                               }
                        ?>
                        ],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	        
	    yAxis: {
	        gridLineInterpolation: 'polygon',
	        lineWidth: 0,
            min: -2,
            max: 2,
	    },
        credits: {
              enabled: false
        },    
	    
	    tooltip: {
	    	shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}:<b>{point.y}</b><br/>'
	    },
	    
	    legend: {
	        align: 'right',
	        verticalAlign: 'top',
	        y: 70,
	        layout: 'vertical'
	    },
	    
	    series: [{
	        name: 'Ideal',
	        data: [
                  <?php
                       $k = 0;
                       foreach($ArrayPromedios as $value)
                       {
                            $k++;
                            
                            if($k < count($ArrayPromedios))
                                echo $Ideal . ",";  
                            
                            else
                                echo $Ideal;   
                       }
                  ?>
                  ],
	        pointPlacement: 'on'
	    },
        {
	        name: 'Minimo',
	        data: [
                  <?php
                       $k = 0;
                       foreach($ArrayPromedios as $value)
                       {
                            $k++;
                            
                            if($k < count($ArrayPromedios))
                                echo $Minimo . ",";  
                            
                            else
                                echo $Minimo;   
                       }
                  ?>
                  ],
	        pointPlacement: 'on'
	    }, {
	        name: 'Promedio Actual',
	        data: [
                  <?php
                       $k = 0;
                       foreach($ArrayPromedios as $value)
                       {
                            $k++;
                            
                            if($k < count($ArrayPromedios))
                                echo $value . ",";  
                            
                            else
                                echo $value;   
                       }
                  ?>
                  ],
	        pointPlacement: 'on'
	    },
        ]
	});
});
</script>


<div class="clr"></div>