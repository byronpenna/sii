<?php

/**
 * @author Manuel
 * @copyright 2013
 */

    if($_SESSION["encuesta"] != "" )
    {
        $query = "Select * from Monitoreo_Encuesta as enc
                  where enc.IdEncuesta = :idb";
                  
        $datos = $bdd->prepare($query);
        $datos->bindParam('idb', $_SESSION["encuesta"]);        
        $datos->execute();
        $datosEncuesta = $datos->fetch();
    }
    else
        Redireccion("../monitoreo.php");
?>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
    <a href="/monitoreo.php?m=cri" class='flecha' style='text-decoration:none'>Criterios</a>
</div>


<?php 
    if(isset($_GET['n']))
    {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Datos Ingresados Exitosamente</div>";
            
        elseif($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Datos Eliminados Exitosamente</div>";

        elseif($_GET['n'] == 3)
            echo "<div id='Notificacion' name='Notificacion' style='color:Blue;'>Datos Actualizados Exitosamente</div>";  

        elseif($_GET['n'] == 4)
            echo "<div id='Notificacion' name='Notificacion' style='color:Blue;'>Posici�n Actualizados Exitosamente</div>";                         
    }
?>
<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; ">
<table class='Datos' style="margin-left: 5%; width: 90%">
    <tr>
    	<td colspan="11" style="text-align: center;"><h2>Boletas para <br /> <?php echo $datosEncuesta[1];?></h2>
        <hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" />
        </td>    
    </tr>

    <tr>
    	<th colspan="9"><h2> Lista de criterio de evaluaci�n</h2></th>
        <td ><a href="monitoreo.php?m=crifo" style="color: skyblue;text-decoration: none;">Agregar</a></td>
    </tr>
    <?php
    	
        $criterios = $bdd->prepare("Select * from Monitoreo_Criterios where IdEncuesta = :id 
                                    ORDER BY Posicion ASC");
        $criterios->bindParam('id',$_SESSION["encuesta"]);
        $criterios->execute();
        
        echo "<tr>
                    <td colspan='2' style='text-align:center'>Posicion<td>                    
                    <td style='text-align:center' >Valor Textual<td>  
                    <td style='text-align:center' >Valor Num�rico<td>                    
                    <td style='text-align:center' >Valor Simb�lico<td>
                    <td style='text-align:center' >Acci�n<td>  
              </tr>";
        
        if($criterios->rowCount() > 0)
        {
            while($datosCriterios = $criterios->fetch())
            {
                echo "<tr>
                            <form action='monitoreo/CriteriosABM.php' method='post'>
                            <td colspan='2'>$datosCriterios[4]
                            
                            <table style='float: right; padding-left:5px'>";
                echo "      <input type='hidden' name='idc' value='$datosCriterios[0]' />
                            <input type='hidden' name='p' value='$datosCriterios[4]' />
                            <tr>"; 
                
                if($datosCriterios[4] != 1)
                    echo "  <td><input type='submit' value='+' name='enviar' class='boton' title='Subir' style='width:30px' /></td>";                 
                else
                    echo "<td><input type='submit' value='+' name='enviar' class='botonE' style='width:30px; background-color:white; border-style: none' disabled='true'/></td>";
                
                if($datosCriterios[4] != $criterios->rowCount())                             
                    echo "  <td><input type='submit' value='-' name='enviar' class='boton' title='Bajar' style='width:30px' /></td>";
                    
                else
                    echo "<td><input type='submit' value='-' name='enviar' class='botonE'style='width:30px; background-color:white; border-style: none' disabled='true'/></td>";
                
                echo "      </tr>
                            </table>
                            <td>                    
                            <td style='text-align:center' >$datosCriterios[1]<td>  
                            <td style='text-align:center' >$datosCriterios[2]<td>                    
                            <td style='text-align:center' >$datosCriterios[3]<td>
                            <td style='text-align:center' >
                                <input type='submit' value='x' name='enviar' class='botonE' title='Eliminar' style='width:30px' onClick='return confirmar();' />                                                            
                                <input type='submit' value='e' name='enviar' class='boton' title='Editar' style='width:30px'  />
                                </form> 
                            <td> 
                              
                      </tr>";                
            }
        }
        else
            echo "<tr><td colspan='9' style='color:red; text-align: center;'>No hay criterios de evaluaci�n<td></tr>";        
    ?>
</table>
</div>
<div class="clr"></div>