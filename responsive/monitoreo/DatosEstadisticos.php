<?php

/**
 * @author Manuel
 * @copyright 2013
 */
	
    
    if($_SESSION["encuesta"] != "")
    {
        $query = "Select * from Monitoreo_Encuesta where IdEncuesta = :id";
        $datos = $bdd->prepare($query);
        $datos->bindParam('id', $_SESSION["encuesta"]);
        $datos->execute();
        $datosEncuesta = $datos->fetch();

    }
    else
        Redireccion("../monitoreo.php");
?>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
    <a href="/monitoreo.php?m=Stad" class='flecha' style='text-decoration:none'>Datos Estadisticos</a>
</div>

<?php 
    if(isset($_GET['n']))
    {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:blue;'>Datos Actualizado Exitosamente</div>";
            
        elseif($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Datos Eliminados Exitosamente</div>";
    }
?>
<div >
<table class='Datos' style="margin-left: 5%; width: 90%;">

<tr>
	<th colspan="2"><h2>Datos Estadisticos </h2>
    <hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" />
    </th>
</tr>

<tr>
    <td style="text-align: right; padding-right: 20px; padding-bottom: 20px;">Encuesta:</td>
    <td style="padding-bottom: 20px"> <?php echo $datosEncuesta[1];?></td>
</tr>     
<?php
                    $totalR = $bdd->prepare("SELECT r . * FROM Monitoreo_Resultado AS r
                                                 INNER JOIN Monitoreo_Boleta AS b ON b.IdBoleta = r.IdBoleta
                                                 INNER JOIN Monitoreo_Encuesta AS e ON e.IdEncuesta = b.IdEncuesta
                                                 WHERE e.IdEncuesta = :id
                                                 GROUP BY r.IdBoleta, r.IdUsuario");
                                                 
                    $totalR->bindParam(':id', $_SESSION["encuesta"]);
                    $totalR->execute();

?>
<tr>
    <td style="text-align: right; padding-right: 20px; padding-bottom: 20px;">Total de Aplicaciones:</td>
    <td style="padding-bottom: 20px"> <?php echo $totalR->rowCount();?> </td>
</tr> 
<?php
                    $totalR = $bdd->prepare("SELECT r . * FROM Monitoreo_Resultado AS r
                                                 INNER JOIN Monitoreo_Boleta AS b ON b.IdBoleta = r.IdBoleta
                                                 INNER JOIN Monitoreo_Encuesta AS e ON e.IdEncuesta = b.IdEncuesta
                                                 WHERE e.IdEncuesta = :id
                                                 GROUP BY r.IdUsuario");
                                                 
                    $totalR->bindParam(':id', $_SESSION["encuesta"]);
                    $totalR->execute();

?>
<tr>
    <td style="text-align: right; padding-right: 20px; padding-bottom: 20px;">Total de Jovenes:</td>
    <td style="padding-bottom: 20px"> <?php echo $totalR->rowCount();?> </td>
</tr> 
<tr>
    <td colspan="2">
        
        <table style="width: 100%;" rules=all>
                        <tr><td style="color: darkblue;">Datos FUSALMO</td>
            <?php   
                    $matriz = array();         
                       
                    $datoBoleta = $bdd->prepare("Select * from Monitoreo_Boleta where IdEncuesta = :id");
                    $datoBoleta->bindParam('id', $_SESSION["encuesta"]);
                    $datoBoleta->execute();
                    
                    while($boleta = $datoBoleta->fetch())
                    {
                        echo "<td style='width:10%; color: darkblue;text-align: center;'>$boleta[2]</td>";
                        array_push($matriz,$boleta[0]);  
                    }
            ?>
            </tr>
                        
            <?php            
                $DatosE = $bdd->prepare("SELECT e.IdEncuesta, b.IdBoleta, d . * 
                                        FROM Monitoreo_BoletaDatoFUSALMO AS b
                                        INNER JOIN Monitoreo_Boleta AS bo ON b.IdBoleta = bo.IdBoleta
                                        INNER JOIN Monitoreo_Encuesta AS e ON bo.IdEncuesta = e.IdEncuesta
                                        INNER JOIN DatoFusalmo AS d ON b.IdDatoFUSALMO = d.IdDatoFUSALMO
                                        where e.IdEncuesta = :idE
                                        GROUP BY d.Nombre   ");

                $DatosE->bindParam('idE', $_SESSION["encuesta"]);
                $DatosE->execute();
                
                $total = array();
                
                while($datoFUSALMO = $DatosE->fetch())
                {
                    $i = 0;
                    echo "<tr><td style='padding: 5px; color: darkblue;'>$datoFUSALMO[3]</td>";

                    foreach($matriz as $value)
                    {
                        echo "<td style='padding: 5px; text-align:center; color: green;'>";
                
                        $query = "SELECT * FROM  Monitoreo_Resultado as r
                                  INNER JOIN InscripcionDatoFusalmo AS df ON r.IdUsuario = df.IdUsuario
                                  WHERE r.IdBoleta = $value and df.IdDatoFUSALMO = $datoFUSALMO[2] GROUP BY r.IdUsuario";

                        $TotalB = $bdd->prepare($query);
                        $TotalB->execute();     
                        
                        
                        if($TotalB->rowCount() != 0)
                        {
                            echo "<form action='monitoreo/BoletaABM.php?r=true' method='post' >";
                            
                            $query2 = "SELECT * FROM  Monitoreo_BoletaDatoFUSALMO 
                                       where IdBoleta = $value and IdDatoFUSALMO = $datoFUSALMO[2]";
                                       
                            $verificar = $bdd->prepare($query2);
                            $verificar->execute();
                            
                            if($verificar->rowCount() == 1)
                                $class = "botonG";
                            
                            else
                                $class = "boton";
                                                       
                            echo "<input type='submit' style='width:30px;' class='$class' title='Ver Resultados' value='".$TotalB->rowCount()."' name='enviar' />
                                  <input type='hidden' name='idD' value='$datoFUSALMO[2]' />
                                  <input type='hidden' name='id' value='$value' />                                  
                                  </form>";
                                  
                            $total[$i] =  $total[$i] + $TotalB->rowCount();
                        }
                        
                        else
                        {
                            echo "-";  
                            $total[$i] =  $total[$i] + 0;  
                        }
                            $i++;
                        
                        echo "</td>";
                    }
                    echo "</tr>";
                }                        
            ?>  
        </table>
    
    </td>    
</tr>  

</table>
</div>
<div class="clr"></div>