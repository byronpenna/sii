<?php
	
    
    if($_SESSION["encuesta"] != "")
    {
        $query = "Select * from Monitoreo_Encuesta where IdEncuesta = :id";
        $datos = $bdd->prepare($query);
        $datos->bindParam('id', $_SESSION["encuesta"]);
        $datos->execute();
        $datosEncuesta = $datos->fetch();
    }
    else
        Redireccion("../monitoreo.php");
?>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
    <a href="/monitoreo.php?m=bole" class='flecha' style='text-decoration:none'>Boleta</a>
</div>
<?php 
    if(isset($_GET['n']))
    {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Datos Agregados Exitosamente</div>";
    }
?>
<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; ">
<table class='Datos' style="margin-left: 7%; width: 84%;">
<form action="monitoreo/BoletaABM.php" method="post" >
<tr>
	<th colspan="2"><h2>Formulario de Boletas <br /> <?php echo $datosEncuesta[1];?> </h2>
    <hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" />
    </th>
</tr>
<tr><td style="text-align: right; padding-right: 20px">Nombre de la Boleta</td><td style="color: red;" ><input type="text" name="nombre" value="" required="true" title="Campo Requerido" />*</td></tr>
<tr><td colspan="2" style="text-align: center;"><input type="submit" value="Agregar" name="enviar" class="boton" /></td></tr>
</form>
</table>
</div>
<div class="clr"></div>