<?php

    if($_SESSION["boleta"] != "" || $_SESSION["IdDato"] != ""  || isset($_GET["t"]))
    {
        if($_SESSION["IdDato"] == "")
        {
            $query = "SELECT e.Nombre, b.NombreBoleta, df.Nombre, t.Tema FROM Monitoreo_Encuesta  AS e
                      INNER JOIN Monitoreo_Boleta       AS b ON e.IdEncuesta = b.IdEncuesta
                      INNER JOIN Monitoreo_Tema         AS t ON t.IdBoleta = b.IdBoleta
                      INNER JOIN Monitoreo_Resultado    AS r ON b.IdBoleta        = r.IdBoleta
                      INNER JOIN InscripcionDatoFusalmo AS i ON r.IdUsuario       = i.IdUsuario
                      INNER JOIN DatoFusalmo            AS df ON df.IdDatoFUSALMO = i.IdDatoFUSALMO
                      WHERE t.IdTema = :id
                      GROUP BY i.IdUsuario";
                      
            $datos = $bdd->prepare($query);
            $datos->bindParam('id', $_GET["t"]);
        }
        else
        {
            $query = "SELECT e.Nombre, b.NombreBoleta, df.Nombre, t.Tema FROM Monitoreo_Encuesta  AS e
                      INNER JOIN Monitoreo_Boleta       AS b ON e.IdEncuesta = b.IdEncuesta
                      INNER JOIN Monitoreo_Tema         AS t ON t.IdBoleta = b.IdBoleta
                      INNER JOIN Monitoreo_Resultado    AS r ON b.IdBoleta        = r.IdBoleta
                      INNER JOIN InscripcionDatoFusalmo AS i ON r.IdUsuario       = i.IdUsuario
                      INNER JOIN DatoFusalmo            AS df ON df.IdDatoFUSALMO = i.IdDatoFUSALMO
                      WHERE t.IdTema = :id AND i.IdDatoFUSALMO = :idI
                      GROUP BY i.IdUsuario";
                      
            $datos = $bdd->prepare($query);
            $datos->bindParam('id', $_GET["t"]);
            $datos->bindParam('idI', $_SESSION["IdDato"]);
        }
        
        $datos->execute();
        $datosEncuesta = $datos->fetch();
    }
    else
        Redireccion("../monitoreo.php?m=res");
        
if($_SESSION["TipoUsuario"] == "Consultor" || $_SESSION["TipoUsuario"] == "Administrador" ) {?>
<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
    <a href="/monitoreo.php?m=bole" class='flecha' style='text-decoration:none'>Boleta</a> ->
    <a href="/monitoreo.php?m=res" class='flecha' style='text-decoration:none'>Respuestas</a>
</div>
<?php } 
        if($datos->rowCount() == 0)
            Redireccion("../monitoreo.php?m=res");
?>
<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; ">
<table class='Datos' style="margin-left: 7%; width: 84%;">
<form action="monitoreo/BoletaABM.php" method="post" >
<tr>
    <td><a href="monitoreo.php?m=res" style="text-decoration: none;"><input  style="width: 150px;" type="button" class="boton" value="<- Resultado General" /></a></td>
    <td colspan="4" style="text-align: center;"><h2>Resultado General por Tema </td>
</tr>
<tr>
    <td style="text-align: right; padding-right: 20px; width: 25%;">Encuesta:</td>
    <td colspan="4"> <?php echo $datosEncuesta[0];?> </h2></td>
</tr>
<tr>
    <td style="text-align: right; padding-right: 20px;">Boleta:</td>
    <td colspan="4"> <?php echo $datosEncuesta[1];?> </h2></td>
</tr>
<?php if($_SESSION["IdDato"] == ""){?>
<tr>
    <td style="text-align: right; padding-right: 20px;">Dato FUSALMO:</td>
    <td colspan="4">Todos los Involucrados</h2></td>
</tr>           
<?php }else{?>
<tr>
    <td style="text-align: right; padding-right: 20px;">Dato FUSALMO:</td>
    <td colspan="4"> <?php echo $datosEncuesta[2];?> </h2></td>
</tr>   
<?php }?>
<tr>
    <td style="text-align: right; padding-right: 20px;">Tema:</td>
    <td colspan="4"> <?php echo $datosEncuesta[3];?> </h2></td>
</tr>
<tr>
    <td style="text-align: right; padding-right: 20px;">Encuestados:</td>
    <td colspan="4"> <?php echo $datos->rowCount();?> Jovenes </h2></td>
</tr>
<tr><td colspan="5"><hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" /></td></tr>
<tr><td colspan="5">
        <table rules='all' style="width: 90%; margin-left: 5%;">
            <tr>
                <td style="text-align: left; width: 70%;"><h2>Preguntas</h2></td>
                <td style="width: 10%;text-align: center">Media</td>
            </tr>        
            <?php

            $ArrayPromedios = array();
            $ArrayPregunta = array();
            
                if($_SESSION["IdDato"] != "")
                {          
                    echo "SELECT r.IdBoleta, r.IdPregunta, r.Fecha, p.Pregunta, SUM( c.ValorNumero ) , COUNT( r.IdPregunta )  
                              FROM  Monitoreo_Resultado AS r
                              INNER JOIN Monitoreo_Pregunta AS p ON r.IdPregunta = p.IdPregunta
                              INNER JOIN Monitoreo_Criterios AS c ON r.Resultado = c.IdCriterio
                              INNER JOIN InscripcionDatoFusalmo as i ON i.IdUsuario = r.IdUsuario 
                              WHERE p.categoria =  'Pregunta' AND p.IdTema = ".$_GET["t"]." AND i.IdDatoFUSALMO = ".$_SESSION["IdDato"]." 
                              GROUP BY r.IdPregunta";
                              
       	            $query = "SELECT r.IdBoleta, r.IdPregunta, r.Fecha, p.Pregunta, SUM( c.ValorNumero ) , COUNT( r.IdPregunta )  
                              FROM  Monitoreo_Resultado AS r
                              INNER JOIN Monitoreo_Pregunta AS p ON r.IdPregunta = p.IdPregunta
                              INNER JOIN Monitoreo_Criterios AS c ON r.Resultado = c.IdCriterio
                              INNER JOIN InscripcionDatoFusalmo as i ON i.IdUsuario = r.IdUsuario 
                              WHERE p.categoria =  'Pregunta' AND p.IdTema = :id AND i.IdDatoFUSALMO = :idI 
                              GROUP BY r.IdPregunta";
      
                          
                    $datosPromedio = $bdd->prepare($query);
                    $datosPromedio->bindParam('id', $_GET["t"]);
                    $datosPromedio->bindParam('idI', $_SESSION["IdDato"]);
                }
                else
                {
   	                $query = "SELECT r.IdBoleta, r.IdPregunta, r.Fecha, p.Pregunta, SUM( c.ValorNumero ) , COUNT( r.IdPregunta )  
                              FROM  Monitoreo_Resultado AS r
                              INNER JOIN Monitoreo_Pregunta AS p ON r.IdPregunta = p.IdPregunta
                              INNER JOIN Monitoreo_Criterios AS c ON r.Resultado = c.IdCriterio
                              INNER JOIN InscripcionDatoFusalmo as i ON i.IdUsuario = r.IdUsuario 
                              WHERE p.categoria =  'Pregunta' AND p.IdTema = :id 
                              GROUP BY r.IdPregunta";
      
                    $datosPromedio = $bdd->prepare($query);
                    $datosPromedio->bindParam('id', $_GET["t"]);
                    
                }
                
                
                $datosPromedio->execute(); 
                $PromedioPreguntas = $Promedio = 0; 
                $i = 0;
                

                
                while($datosP = $datosPromedio->fetch())
                {
                    $PromedioPreguntas = $datosP[4] / $datosP[5] ;  
                    $Promedio = $PromedioPreguntas + $Promedio;
                
                $i++;
                
                echo "<tr>
                          <td style='color: darkblue; padding-top: 10px; padding-bottom: 10px;'>
                          <a style='text-decoration:none; color: darkblue;' href='monitoreo.php?m=resCri&c=$datosP[1]'>$i - $datosP[3]</a></td>
                          <td style='text-align: center'>" . number_format($PromedioPreguntas,2) ."</td>
                      </tr>";   
                      
                      array_push($ArrayPregunta , $datosP[3]);
                      array_push($ArrayPromedios , number_format($PromedioPreguntas,2));
                }
                
                $Promedio = $Promedio / $datosPromedio->rowCount();
                
                echo "<tr>
                          <td style='color: darkblue; padding-top: 10px; padding-bottom: 10px; text-align: right; padding-right: 20px'>Promedio del Tema -></td>
                          <td style='text-align: center'>" . number_format($Promedio,2) ."</td>
                      </tr>";                    

        ?>                    
        </table>
    </td>    
</tr>
<tr><td colspan="5"><hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" /></td></tr>
<?php
	        $query2 = "(SELECT * FROM Monitoreo_Criterios
                        ORDER BY Posicion ASC LIMIT 1)
               
                        UNION ALL 
               
                       (SELECT * FROM Monitoreo_Criterios
                        ORDER BY Posicion DESC LIMIT 1)";

            $datosCriterios = $bdd->prepare($query2);
            $datosCriterios->bindParam('id', $_SESSION["encuesta"]);
            $datosCriterios->execute();
            
            $Minimo = "";
            $Ideal = "";
            
            while($criterios = $datosCriterios->fetch())
            {
                if($Ideal == "")
                    $Ideal = $criterios[2];

                elseif($Minimo == "")
                    $Minimo  = $criterios[2];
            }
            echo "<tr>
                      <td colspan='4' style='text-align: right; padding-right: 20px;'>Valor Minimo: </td>
                      <td style='width: 10%; text-align: center;'>$Minimo</td>
                  </tr>
                  <tr>
                      <td colspan='4' style='text-align: right; padding-right: 20px;'>Valor Ideal:</td>
                      <td style='text-align: center;'>$Ideal</td>
                  </tr>";
?>        
</form>
</table>

<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script>
$(function () {
        $('#container').highcharts({
            chart: {
            },
            credits: {
                  enabled: false
            },
            title: {
                text: '<?php echo $datosEncuesta[3];?>'
            },
            xAxis: {
                categories: [
                            <?php
                               $k = 0;
                               foreach($ArrayPromedios as $value)
                               {
                                    $k++;                                    
                                    if($k < count($ArrayPromedios))
                                        echo "'Pregunta $k',";  
                                    
                                    else
                                        echo "'Pregunta $k'";   
                               }
                           ?>
                            ]
            },
            
            yAxis: {    
            min: -2,
            max: 2,
	        },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' fruits';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
            series: [{
                type: 'column',
                name: 'Minimo',
                data: [<?php
                       $k = 0;
                       foreach($ArrayPromedios as $value)
                       {
                            $k++;
                            
                            if($k < count($ArrayPromedios))
                                echo $Minimo . ",";  
                            
                            else
                                echo $Minimo;   
                       }
                       ?>
                       ]
            },{
                type: 'column',
                name: 'Promedios',
                data: [
                       <?php
                       $k = 0;
                       foreach($ArrayPromedios as $value)
                       {
                            $k++;
                            
                            if($k < count($ArrayPromedios))
                                echo $value . ",";  
                            
                            else
                                echo $value;   
                       }
                      ?>
                      ],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            }, {
                type: 'column',
                name: 'Ideal',
                data: [
                       <?php
                       $k = 0;
                       foreach($ArrayPromedios as $value)
                       {
                            $k++;
                            
                            if($k < count($ArrayPromedios))
                                echo $Ideal . ",";  
                            
                            else
                                echo $Ideal;   
                       }
                      ?>
                      ],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            }]
        });
    });
</script>
</div>
<div class="clr"></div>