<?php

    if($_SESSION["encuesta"] != "")
    {
        $query = "Select * from Monitoreo_Encuesta where IdEncuesta = :id";
        $datos = $bdd->prepare($query);
        $datos->bindParam('id', $_SESSION["encuesta"]);
        $datos->execute();
        $datosEncuesta = $datos->fetch();
        $ABM = "Modificar";
    }

    else
    {
        $datosEncuesta[1] = "";
        $datosEncuesta[4] = "";
        $ABM = "Insertar";
    }
?>
<div class="Raiz">
<a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
<a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a>
</div>

<div style="width: 90%; background-color: white; border-radius: 20px; margin-left: 5%; ">
<form action="monitoreo/PruebaABM.php" method="POST" name="monitoreo" id="monitoreo">        
<table style=" width: 80%; margin-left: 10%;">
<tr>
	<td colspan="2" style="text-align: center;"><h2>Formulario de Encuesta</h2>
        <hr style="color: skyblue; background-color: skyblue; height: 5px; width: 80%;" />  
    </td>
</tr>
<tr>
	<td style="text-align: right; padding-right: 20px">Nombre de prueba:</td>
	<td style="color: red;"><input type="text" name="Nombre" id="Nombre" value="<?php echo $datosEncuesta[1]?>" style="width: 200px;" required="true" title="Campo Requerido" />*</td>
</tr>
	<td style="text-align: right; padding-right: 20px">Descripción del Dato:</td>
	<td><textarea name="Descripcion" id="Descripcion" style="width: 250px;"  rows="3"><?php echo $datosEncuesta[4]?></textarea></td>
</tr>
<tr><td colspan="2" style="text-align: center;">
    <input class="boton" type="submit" value="<?php echo $ABM?>" name="enviar" onclick="return validar(this.form);" />
</td></tr>
</table>
</form>
</div>

<div class="clr"></div> 
