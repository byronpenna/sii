<?php
	
    
    if($_SESSION["encuesta"] != "")
    {
        $query = "Select * from Monitoreo_Encuesta where IdEncuesta = :id";
        $datos = $bdd->prepare($query);
        $datos->bindParam('id', $_SESSION["encuesta"]);
        $datos->execute();
        $datosEncuesta = $datos->fetch();
        
        $_SESSION["boleta"] = "";
    }
    else
        Redireccion("../monitoreo.php");
?>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
    <a href="/monitoreo.php?m=bole" class='flecha' style='text-decoration:none'>Boleta</a>
</div>

<?php 
    if(isset($_GET['n']))
    {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:blue;'>Datos Actualizado Exitosamente</div>";
            
        elseif($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Datos Eliminados Exitosamente</div>";
    }
?>
<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; ">
<table class='Datos' style="margin-left: 7%; width: 84%;">

<tr>
	<th colspan="5"><h2>Boletas para <br /> <?php echo $datosEncuesta[1];?> </h2>
    <hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" />
    </th>    
</tr>
<?php

        $query = "Select * from Monitoreo_Boleta where IdEncuesta = :id
                  ORDER BY  NombreBoleta ASC ";
        $datos = $bdd->prepare($query);
        $datos->bindParam('id', $_SESSION["encuesta"]);
        $datos->execute();

        if($datos->rowCount() > 0){
            
            echo "<tr><td>N�</td><td>Nombre</td><td colspan='3' >Acci�n</td></tr>";
            $i=0;
            while ($datosBoleta = $datos->fetch())
            {
                $i++;
                $query = "Select count(IdBoleta) from Monitoreo_Tema where IdBoleta = :id";
                $datost = $bdd->prepare($query);
                $datost->bindParam('id', $datosBoleta[0]);
                $datost->execute();
                $tema = $datost->fetch();
                
                $query = "SELECT COUNT(t.IdTema) FROM  Monitoreo_Tema AS t
                          INNER JOIN Monitoreo_Pregunta AS p ON t.IdTema = p.IdTema
                          where t.IdBoleta = :id
                          ";
                                                    
                $datosp = $bdd->prepare($query);
                $datosp->bindParam('id', $datosBoleta[0]);
                $datosp->execute();
                $pregunta = $datosp->fetch();     
                
                echo "<tr>
                            <td style='width:5%; padding-top: 10px;'>$i</td><td>$datosBoleta[2]</td>
                            <td colspan='2'><div style='float: left;'>Temas:    $tema[0]</div><div style='float: right; margin-right: 15%'>Preguntas:   $pregunta[0]</div></td>
                            <td style='text-align: center;width:15%;padding-top: 10px;'>
                                <form action='monitoreo/BoletaABM.php' method='post' >
                                    <input type='hidden' name='id' value='$datosBoleta[0]' />                            
                                    <input type='submit' style='width:30px;' class='boton' title='Ver Preguntas' value='?' name='enviar' alt='Preguntas' />
                                    <input type='submit' style='width:30px;' class='botonE' title='Eliminar' value='X' name='enviar' onClick='return confirmar();' /><br />
                                    <input type='submit' style='width:30px;' class='boton' title='Ver Datos FUSALMO' value='D' name='enviar' />
                                    <input type='submit' style='width:30px;' class='boton' title='Ver Resultados' value='R' name='enviar' />
                                </form> 
                            </td>
                      </tr>";
            }
        }else
        {            
            echo "<tr><td colspan='5' style='text-align: Center; width: 10%; color: red'> Agrega una Nueva Boleta </td></tr>";
        }
?>
<form action='monitoreo/BoletaABM.php' method='post' >
<tr>
	<th colspan="5">
    <br /><br /><br />
    <hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" />
    </th>
</tr>
<tr><td colspan="2" style="text-align: right; padding-right: 20px">Nombre de la Boleta: </td><td colspan="3" style="color: red;" ><input type="text" name="nombre" value="" required="true" title="Campo Requerido" />*</td></tr>
<tr><td colspan="5" style="text-align: center;"><input type="submit" value="Agregar" name="enviar" class="boton" /></td></tr>
</form>
</table>
</div>
<div class="clr"></div>