<script>
    $(document).ready(function()
    {
	   $("#encuesta").change(function(){Cargar_Boletas(this.form);});
       $("#Comparar").click(function(){Cargar_Comparacion(this.form);});
       $("#boleta").attr("disabled",true);
    });
    
    function Cargar_Boletas(form)
    {
        var Boletas = document.getElementById("boleta");        
        var code = $("#encuesta").val();    	        
        
    	$.get("monitoreo/BoletasOptions.php", { code: code },
    		function(resultado)
    		{
    			if(resultado == false)
    			{
    				alert("Error");
    			}
    			else
    			{    			    
    				$("#boleta").attr("disabled", false);
    				document.getElementById("boleta").options.length=0;
    				$('#boleta').append(resultado);	
    			}
    		}
    
    	);
        
    }

    function Cargar_Comparacion(form)
    { 
        var code = $("#boleta").val();    	        
        window.location='monitoreo.php?m=compare&code=' + code ;
        
    }    
</script>
<?php

/**
 * @author Manuel
 * @copyright 2013
 */

if($_SESSION["TipoUsuario"] == "Consultor" || $_SESSION["TipoUsuario"] == "Administrador" ) { ?>
    <div class="Raiz">
        <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
        <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
        <a href="/monitoreo.php?m=bole" class='flecha' style='text-decoration:none'>Boleta</a> ->
        <a href="/monitoreo.php?m=res" class='flecha' style='text-decoration:none'>Respuestas</a> ->
        <a href="/monitoreo.php?m=compare" class='flecha' style='text-decoration:none'>Comparar</a> ->
    </div>
<?php } 

    if($_SESSION["boleta"] != "" || $_SESSION["IdDato"] != "" )
    {
        if($_SESSION["IdDato"] == "")
        {
            $query = "SELECT e.Nombre, b.NombreBoleta, df.Nombre 
                      FROM Monitoreo_Encuesta           AS e
                      INNER JOIN Monitoreo_Boleta       AS b ON e.IdEncuesta      = b.IdEncuesta
                      INNER JOIN Monitoreo_Resultado    AS r ON b.IdBoleta        = r.IdBoleta
                      INNER JOIN InscripcionDatoFusalmo AS i ON r.IdUsuario       = i.IdUsuario
                      INNER JOIN DatoFusalmo            AS df ON df.IdDatoFUSALMO = i.IdDatoFUSALMO
                      WHERE r.IdBoleta = :id
                      GROUP BY i.IdUsuario";
            
            $datos = $bdd->prepare($query);
            $datos->bindParam('id', $_SESSION["boleta"]);
        }
        
        else
        {
            $query = "SELECT e.Nombre, b.NombreBoleta, df.Nombre 
                      FROM Monitoreo_Encuesta           AS e
                      INNER JOIN Monitoreo_Boleta       AS b ON e.IdEncuesta      = b.IdEncuesta
                      INNER JOIN Monitoreo_Resultado    AS r ON b.IdBoleta        = r.IdBoleta
                      INNER JOIN InscripcionDatoFusalmo AS i ON r.IdUsuario       = i.IdUsuario
                      INNER JOIN DatoFusalmo            AS df ON df.IdDatoFUSALMO = i.IdDatoFUSALMO
                      WHERE r.IdBoleta = :id AND i.IdDatoFUSALMO = :idI
                      GROUP BY i.IdUsuario";
                      
            $datos = $bdd->prepare($query);
            
            $datos->bindParam('id', $_SESSION["boleta"]);
            $datos->bindParam('idI', $_SESSION["IdDato"]);
        }
        
                  
        
        $datos->execute();
        $datosEncuesta = $datos->fetch();
        
        $encuestaO = $datosEncuesta[0];
    }
    else
        Redireccion("../monitoreo.php");
?>


<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px;">
<table class='Datos' style="margin-left: 8%; width: 84%;">    
    <tr>
        <td colspan="4" style="text-align: center;"><h2>Comparación de Boletas</td>
    </tr>
    
    <?php if($_SESSION["IdDato"] == ""){?>
    <tr>
        <td style="text-align: right; padding-right: 20px;">Dato FUSALMO:</td>
        <td colspan="4">Todos los Involucrados</td>
    </tr>           
    <?php }else{?>
    <tr>
        <td style="text-align: right; padding-right: 20px;">Dato FUSALMO:</td>
        <td colspan="4"> <?php echo $datosEncuesta[2];?> </td>
    </tr>   
    <?php }?>  
        
    <tr style="height: 30px; background-color: white;"><td colspan="5"></td></tr>
    <tr style="height: 10px; background-color: skyblue;"><td colspan="5" style="color: white; font-size: small;">Original</td></tr> 
    
    <tr>
        <td style="text-align: right; padding-right: 20px; width: 25%;">Encuesta:</td>
        <td colspan="2" style=" width: 70%;"> <?php echo $datosEncuesta[0];?> </td>
    </tr>
    <tr>
        <td style="text-align: right; padding-right: 20px;">Boleta:</td>
        <td colspan="2"> <?php echo $datosEncuesta[1];?> </td>
    </tr>
    <tr>
        <td style="text-align: right; padding-right: 20px;">Encuestados:</td>
        <td colspan="4"> <?php echo $datos->rowCount();?> jovenes  </td>
    </tr>  
    
    <tr style="height: 30px; background-color: white;"><td colspan="5"></td></tr>
    <tr style="height: 10px; background-color: skyblue;"><td colspan="5" style="color: white; font-size: small;">A comparar</td></tr>
    <tr>
        <td style="text-align: right; padding-right: 20px; width: 25%;">Encuesta:</td>
        <td colspan="2" style=" width: 70%;">
            <select name="encuesta" id="encuesta" >
            <option>...</option>
                <?php
	                   $Select = $bdd->prepare("SELECT e.* FROM Monitoreo_Encuesta AS e
                                                INNER JOIN Monitoreo_Boleta AS b ON e.IdEncuesta = b.IdEncuesta
                                                INNER JOIN Monitoreo_BoletaDatoFUSALMO AS m ON b.IdBoleta = m.IdBoleta
                                                WHERE m.IdDatoFUSALMO = :id
                                                GROUP BY e.IdEncuesta");

                       $Select->bindParam(':id', $_SESSION["IdDato"]);
                       $Select->execute();
                       
                       while($Options = $Select->fetch())
                            echo "<option value='$Options[0]'> $Options[2] - $Options[1]</option>";
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td style="text-align: right; padding-right: 20px;">Boleta:</td>
        <td colspan="2">
            <select name="boleta" id="boleta">
                <option>...</option>
            </select>
        </td>
    </tr>
    <tr>        
        <td colspan="4" style="text-align: center;">
            <input name="Comparar" id="Comparar" type="button" class="boton" value="Comparar" />
        </td>
    </tr>    
    
    <tr style="height: 30px; background-color: white;"><td colspan="5"></td></tr>
    <tr style="height: 10px; background-color: skyblue;"><td colspan="5" style="color: white; font-size: small; text-align: right;">Comparando</td></tr>
    <tr style="height: 30px; background-color: white;"><td colspan="5"></td></tr>
    <tr>
        <td colspan="4">
            <div id="Content" style="text-align: center;">
            <?php  if(!isset($_GET['code'])){ ?>
            <em style="color: red; font-size: small; ">Esperando Datos... </em>
            <?php  }else {
                
                    $ide = $_GET['code'];   
                    
                    $comp = $bdd->prepare("Select e.Nombre from Monitoreo_Boleta as b
                                           inner join Monitoreo_Encuesta as e on  b.IdEncuesta = e.IdEncuesta
                                           where b.IdBoleta = $ide");
                    $comp->execute();
                    
                    if(!$comp->rowCount() > 0)
                    Redireccion("monitoreo.php?m=compare");   
                    
                    $encuestaC = $comp->fetch();
                    
                    
                ?>
            <!--  Esto es para  cuando se efectua la comparación --!>
            <table>
            <tr><td>
                    <table rules='all' style="width: 100%; border-style: groove; border-color: skyblue; border-radius: 10px;">
                    <tr><td colspan="2" style="text-align: left;"><?php echo $encuestaO?></td></tr>
                        <tr>
                            <td style="text-align: center; width: 70%;"><h2>Temas</h2></td>
                            <td style="width: 10%;text-align: center">Media</td>
                        </tr>        
                        <?php
                        
                        
                        
            	        $query = "SELECT * 
                                  FROM  Monitoreo_Tema
                                  WHERE IdBoleta = :id";
                              
                        $datosTema = $bdd->prepare($query);
                        $datosTema->bindParam('id', $_SESSION["boleta"]);
                        $datosTema->execute();               
                        
                        $j = 0;
                        
                        $ArrayPromedios = array();
                        $ArrayTemas = array();
                        
                        while($temas = $datosTema->fetch())
                        { 
                            $j++;
                            
                            if($_SESSION["IdDato"] != "")
                            {
                       	            $query = "SELECT r.IdBoleta, r.IdPregunta, r.Fecha, p.Pregunta, SUM( c.ValorNumero ) , COUNT( r.IdPregunta )  
                                              FROM  Monitoreo_Resultado AS r
                                              INNER JOIN Monitoreo_Pregunta AS p ON r.IdPregunta = p.IdPregunta
                                              INNER JOIN Monitoreo_Criterios AS c ON r.Resultado = c.IdCriterio
                                              INNER JOIN InscripcionDatoFusalmo as i ON i.IdUsuario = r.IdUsuario 
                                              WHERE p.categoria =  'Pregunta' AND p.IdTema = :id AND i.IdDatoFUSALMO = :idi 
                                              GROUP BY r.IdPregunta";
                                              
                                    $datosPromedio = $bdd->prepare($query);
                                    $datosPromedio->bindParam(':id', $temas[0]);                
                                    $datosPromedio->bindParam(':idi', $_SESSION["IdDato"]);                    
                            }
                            else
                            {
               	                $query = "SELECT r.IdBoleta, r.IdPregunta, r.Fecha, p.Pregunta, SUM( c.ValorNumero ) , COUNT( r.IdPregunta )  
                                          FROM  Monitoreo_Resultado AS r
                                          INNER JOIN Monitoreo_Pregunta AS p ON r.IdPregunta = p.IdPregunta
                                          INNER JOIN Monitoreo_Criterios AS c ON r.Resultado = c.IdCriterio
                                          INNER JOIN InscripcionDatoFusalmo as i ON i.IdUsuario = r.IdUsuario 
                                          WHERE p.categoria =  'Pregunta' AND p.IdTema = :id  
                                          GROUP BY r.IdPregunta";
                                              
                                $datosPromedio = $bdd->prepare($query);
                                $datosPromedio->bindParam(':id', $temas[0]);                
                            }
                            $datosPromedio->execute();
                            
                            
                            $PromedioPreguntas = $SUMPromedioPreguntas = $Promedio = 0;
                            
                            if($datosPromedio->rowCount() > 0)
                            {                     
                                while($datosP = $datosPromedio->fetch())
                                {
                                    $PromedioPreguntas = $datosP[4] / $datosP[5] ;  
                                    $SUMPromedioPreguntas = $PromedioPreguntas + $SUMPromedioPreguntas;
                                }   
                                
                                $Promedio = $SUMPromedioPreguntas / $datosPromedio->rowCount();
                                
                                $texto = number_format($Promedio,2);
                                array_push($ArrayPromedios,$texto);
                                array_push($ArrayTemas,$temas[2]);
                            }
                            else
                                $texto = "-";
            
                            echo "<tr>
                                      <td style='color: darkblue; padding-top: 10px; padding-bottom: 10px;'>$j - " . $temas[2] . "</td>
                                      <td style='text-align: center'>" . $texto ."</td>
                                  </tr>";
                        }
                    ?>
                    </table>
                </td>
            <td>
                    <table rules='all' style="width: 100%; border-style: groove; border-color: skyblue; border-radius: 10px">
                    <tr><td colspan="2" style="text-align: right;"> <?php echo $encuestaC[0]?></td></tr>
                        <tr>
                            <td style="text-align: left; width: 70%;"><h2>Temas</h2></td>
                            <td style="width: 10%;text-align: center">Media</td>
                        </tr>        
                        <?php                                                
                        
            	        $query = "SELECT * 
                                  FROM  Monitoreo_Tema
                                  WHERE IdBoleta = :id";
                              
                        $datosTema = $bdd->prepare($query);
                        $datosTema->bindParam('id', $ide);
                        $datosTema->execute();               
                        
                        $j = 0;
                        
                        $ArrayPromediosC = array();
                        $ArrayTemasC = array();
                        
                        while($temas = $datosTema->fetch())
                        { 
                            $j++;
                            
                            if($_SESSION["IdDato"] != "")
                            {
                       	            $query = "SELECT r.IdBoleta, r.IdPregunta, r.Fecha, p.Pregunta, SUM( c.ValorNumero ) , COUNT( r.IdPregunta )  
                                              FROM  Monitoreo_Resultado AS r
                                              INNER JOIN Monitoreo_Pregunta AS p ON r.IdPregunta = p.IdPregunta
                                              INNER JOIN Monitoreo_Criterios AS c ON r.Resultado = c.IdCriterio
                                              INNER JOIN InscripcionDatoFusalmo as i ON i.IdUsuario = r.IdUsuario 
                                              WHERE p.categoria =  'Pregunta' AND p.IdTema = :id AND i.IdDatoFUSALMO = :idi 
                                              GROUP BY r.IdPregunta";
                                              
                                    $datosPromedio = $bdd->prepare($query);
                                    $datosPromedio->bindParam(':id', $temas[0]);                
                                    $datosPromedio->bindParam(':idi', $_SESSION["IdDato"]);                    
                            }
                            else
                            {
               	                $query = "SELECT r.IdBoleta, r.IdPregunta, r.Fecha, p.Pregunta, SUM( c.ValorNumero ) , COUNT( r.IdPregunta )  
                                          FROM  Monitoreo_Resultado AS r
                                          INNER JOIN Monitoreo_Pregunta AS p ON r.IdPregunta = p.IdPregunta
                                          INNER JOIN Monitoreo_Criterios AS c ON r.Resultado = c.IdCriterio
                                          INNER JOIN InscripcionDatoFusalmo as i ON i.IdUsuario = r.IdUsuario 
                                          WHERE p.categoria =  'Pregunta' AND p.IdTema = :id  
                                          GROUP BY r.IdPregunta";
                                              
                                $datosPromedio = $bdd->prepare($query);
                                $datosPromedio->bindParam(':id', $temas[0]);                
                            }
                            $datosPromedio->execute();
                            
                            
                            $PromedioPreguntas = $SUMPromedioPreguntas = $Promedio = 0;
                            
                            if($datosPromedio->rowCount() > 0)
                            {                     
                                while($datosP = $datosPromedio->fetch())
                                {
                                    $PromedioPreguntas = $datosP[4] / $datosP[5] ;  
                                    $SUMPromedioPreguntas = $PromedioPreguntas + $SUMPromedioPreguntas;
                                }   
                                
                                $Promedio = $SUMPromedioPreguntas / $datosPromedio->rowCount();
                                
                                $texto = number_format($Promedio,2);
                                array_push($ArrayPromediosC,$texto);
                                array_push($ArrayTemasC,$temas[2]);
                            }
                            else
                                $texto = "-";
                            
                            echo "<tr>
                                      <td style='color: darkblue; padding-top: 10px; padding-bottom: 10px;'>$j - ".  $temas[2] ."</td>
                                      <td style='text-align: center'>" . $texto ."</td>
                                  </tr>";
                        }
                    ?>
                    </table>
                </td>    
            </tr>
            
            </table>
            
            <script src="js/highcharts.js"></script>
            <script src="js/highcharts-more.js"></script>
            <script src="js/exporting.js"></script>
            
            <div id="container" style="min-width: 400px; max-width: 600px; height: 400px; margin: 0 auto"></div>
            
            
<script>
$(function () {
	$('#container').highcharts({
	            
	    chart: {
	        polar: true,
	        type: 'line'
	    },
	    
	    title: {
	        text: 'Comparación de Boletas',
	        x: -80
	    },
	    
	    pane: {
	    	size: '80%'
	    },
	    
	    xAxis: {
	        categories: [
                        <?php
                               $k = 0;
	                           foreach($ArrayTemas as $value)
                               {
                                    $k++;
                                    
                                    if($k < count($ArrayTemas))
                                        echo "'". substr($value,0 , 25) ."...',";  
                                    
                                    else
                                        echo "'". substr($value,0 , 25) ."...'";   
                               }
                        ?>
                        ],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	        
	    yAxis: {
	        gridLineInterpolation: 'polygon',
	        lineWidth: 0,
            min: -2,
            max: 2,
	    },
        credits: {
              enabled: false
        },    
	    
	    tooltip: {
	    	shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}:<b>{point.y}</b><br/>'
	    },
	    
	    legend: {
	        align: 'right',
	        verticalAlign: 'top',
	        y: 70,
	        layout: 'vertical'
	    },
	    
	    series: [{
	        name: 'Ideal',
	        data: [
                  <?php
                       $k = 0;
                       foreach($ArrayPromedios as $value)
                       {
                            $k++;
                            
                            if($k < count($ArrayPromedios))
                                echo 2 . ",";  
                            
                            else
                                echo 2;   
                       }
                  ?>
                  ],
	        pointPlacement: 'on'
	    },
        {
	        name: '<?php echo substr($encuestaC[0],0,20) . "...";?>',
	        data: [
                  <?php
                       $k = 0;
                       foreach($ArrayPromediosC as $value)
                       {
                            $k++;
                            
                            if($k < count($ArrayPromediosC))
                                echo $value . ",";  
                            
                            else
                                echo $value;   
                       }
                  ?>
                  ],
	        pointPlacement: 'on'
	    }, {
	        name: '<?php echo substr($encuestaO,0,20) . "...";?>',
	        data: [
                  <?php
                       $k = 0;
                       foreach($ArrayPromedios as $value)
                       {
                            $k++;
                            
                            if($k < count($ArrayPromedios))
                                echo $value . ",";  
                            
                            else
                                echo $value;   
                       }
                  ?>
                  ],
	        pointPlacement: 'on'
	    },
        ]
	});
});
</script>

            <?php } ?>
            <!--  Esto es para  cuando se efectua la comparación --!>
            </div>
        </td>
    </tr> 
</table>
