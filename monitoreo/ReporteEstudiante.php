<?php

/**
 * @author Manuel
 * @copyright 2013
 */
    
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=Listado Participantes ". $_SESSION["IdDato"] .".xls");
    header('Content-Transfer-Encoding: binary');
    require '../net.php';

    $ListadoJovenes = $bdd->prepare("SELECT usu.Id, usu.NombreUsuario, usu.Contrase�a
                                     FROM Monitoreo_Encuesta AS e
                                     INNER JOIN Monitoreo_Boleta AS b ON e.IdEncuesta = b.IdEncuesta
                                     INNER JOIN Monitoreo_Resultado AS r ON b.IdBoleta = r.IdBoleta
                                     INNER JOIN InscripcionDatoFusalmo AS i ON r.IdUsuario = i.IdUsuario                                     
                                     INNER JOIN DatoFusalmo AS df ON df.IdDatoFUSALMO = i.IdDatoFUSALMO
                                     INNER JOIN usuario  AS usu ON i.IdUsuario = usu.Id
                                     WHERE r.IdBoleta = :IdB
                                     AND i.IdDatoFUSALMO = :IdD
                                     GROUP BY usu.Id");
                                     
    $ListadoJovenes->bindParam(':IdB', $_SESSION["boleta"]);
    $ListadoJovenes->bindParam(':IdD', $_SESSION["IdDato"]);
    $ListadoJovenes->execute();
    
    echo "<table>
          <tr><td colspan='3' style='height: 20px; background-color: white;'></td></tr>  
          <tr><td style='width: 30px;'><td><td>Id</td><td>Usuario</td><td>Contrase�a</td><td>Nombres</td><td>Apellidos</td><td>Centro Escolar</td><td>Secci�n</td></tr>
          <tr><td colspan='8' style=' background-color: skyblue; height: 3px;'></td></tr>";
            
    while($Jovenes = $ListadoJovenes->fetch())
    {
        
        
        echo "<tr><td><td>
                <td style='text-align: center;'>$Jovenes[0]</td>
                <td style='text-align: center;'>$Jovenes[1]</td>
                <td style='text-align: center;'>$Jovenes[2]</td>";
                
        $Joven = $bdd->prepare("Select Nombre, Apellido1, Apellido2 from joven where IdUsuario = $Jovenes[0]");
        $Joven->execute();
        
        $DataJoven = $Joven->fetch();        
        echo "  <td style='text-align: center;'>$DataJoven[0]</td>
                <td style='text-align: center;'>$DataJoven[1] $DataJoven[2]</td>";
        
        $query = "Select ins.NombreInstitucion, sec.SeccionEducativa from Educacion_Ciclos as c
                       inner join Educacion_Inscripcion as i on c.IdCiclo = i.IdCiclo 
                       inner join Educacion_Estudiantes as est on i.IdInscripcion = est.IdInscripcion
                       inner join Institucion as ins on i.IdInstitucion = ins.IdInstitucion
                       inner join Institucion_Seccion as sec on i.IdSeccion = sec.IdSeccion
                       inner join Educacion_CicloPeriodo as cp on c.IdCiclo = cp.IdCiclo
                       inner join Educacion_Periodos as p on cp.IdPeriodo = p.IdPeriodo
                       where p.Status = 'Seleccionado' and p.IdDatoFUSALMO = ".$_SESSION["IdDato"]." and est.IdUsuario = $Jovenes[0]";
                       
        $DatosAca = $bdd->prepare($query);
        
        $DatosAca->execute();        
        $DataA = $DatosAca->fetch();
        echo "<td>$DataA[0]</td><td>$DataA[1]</td>";
        echo "</tr>";
    }
    echo "<tr><td>".$_SESSION["boleta"]."</td><td>".$_SESSION["IdDato"]."</td></tr>
          </table>";
?>