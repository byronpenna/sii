<div style="width: 100%; border-radius: 10px; border-style: groove; border-color: skyblue; background-color: white;">
<?php	
    $Resumenes = $bdd->prepare("SELECT * FROM Project_Summary");
    $Resumenes->execute();
?>
<table style="margin: 10px; width: 100%;">
<tr>
    <td>
        <table style="width: 90%;">
            <tr>
                <td><h2>Resumen de Proyectos</h2></td>
                <td style="text-align: right;">
                    <form action='monitoreo/Resumen/ReporteResumen.php' method='post'>
                        <input type='submit' name='Enviar' value='Reporte' class='boton' style='width: 60px' />
                    </form>
                </td>
            </tr>
        </table>    
    </td>
</tr>
<tr>
<td>
    <table style="width: 90%; margin-left: 5%;">
    <tr>
        <th>N�</th>
        <th>Proyecto</th>
        <th>Financiador</th>
        <th>Monto Financiado</th>
        <th>Monto Contrapartida</th>
        <th>Monto Global</th>
        <tr></tr>
    </tr>
    <?php
    $i = 0;
    $Total = 0;
    $TotalC = 0;
    $TotalG = 0;
    while($DataR = $Resumenes->fetch())
    {
        $i++;
        
        $totalGlobal = $DataR[4] + $DataR[5];
        
        $Total += $DataR[4]; 
        $TotalC += $DataR[5];
        $TotalG += $totalGlobal;

        echo "<tr>
                  <td>$i</td><td>$DataR[2]</td><td>$DataR[3]</td><td>$". number_format($DataR[4], 2) . "</td><td>$". number_format($DataR[5], 2) . "</td><td>$". number_format($totalGlobal, 2) . "</td>
                  <th>
                    <form action='?m=SummaryProfile' method='post' target='_blank'>
                        <input type='hidden' name='irp' value='$DataR[0]' />
                        <input type='submit' name='Enviar' value='Ver' class='boton' style='width: 40px' />
                    </form>
                  </th>
              </tr>";
    }
    echo "<tr><th colspan='7'><hr color='skyblue' /></th></tr>
          <tr><th colspan='2'>Total de Proyectos: ".$Resumenes->rowCount(). "</th><th>Total</th>
              <th>$". number_format($Total, 2) . "</th>
              <th>$". number_format($TotalC, 2) . "</th>
              <th>$". number_format($TotalG, 2) . "</th></tr>";
    ?>
    </table>
</td>
</tr>
<tr><td></td></tr>
<tr>
    <td>
        <table style="width: 100%;">
            <tr>
                <td><h2>Beneficiarios</h2></td>
                <td style="text-align: right;">
<!--
                    <form action='monitoreo/Resumen/ReporteBeneficiarios.php' method='post'>
                          <input type='submit' name='Enviar' value='Reporte' class='boton' style='width: 60px' />
                    </form>
 --!>
                </td>
            </tr>
        </table>    
    </td>
</tr>
<tr>
<td>
    <table style="width: 80%; margin-left: 10%;">
    <tr>
        <td style="width: 50%; vertical-align: top;">
        <table style="width: 100%;">
        <tr>
            <th style="width: 10%;">N�</th>
            <th style="width: 30%;">Beneficiarios</th>
            <th style="width: 30%;">Sexo</th>
            <th style="width: 30%;">Cantidad de <br />Beneficiarios</th>            
        </tr>
        <?php
        	
        $Beneficiarios = $bdd->prepare("SELECT Participantes,  Sexo, SUM(Cantidad) FROM Project_Participants GROUP BY Participantes, Sexo");
        $Beneficiarios->execute();
        
        $i = 0;
        $Total = 0;

        $TotalM = 0;
        $TotalF = 0;
        while($DataB = $Beneficiarios->fetch())
        {
            $i++;
            $Total += $DataB[2]; 
            echo "<tr><th>$i</th><td>$DataB[0]</td><td style='text-align: center;'>$DataB[1]</td><td style='text-align: center;'>$DataB[2]</td></tr>";
            
            if($DataB[1] == "Masculino")
                $TotalM += $DataB[2];
            else
                $TotalF += $DataB[2];
        }
        echo "<tr><th colspan='4'><hr color='skyblue' /></th></tr>
              <tr><th colspan='3'>Total de Beneficiados</th><th>$Total</th></tr>";
        ?>
        </table>
        </td>        
    </tr>
    </table>
</td>
</tr>
<tr></tr>
<tr>
<td>
<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>

<div id="Grafica" style="min-width: 250px; height: 400px; margin: 0 auto"></div>
<script>
$(function () {
    $('#Grafica').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Gr�fica por Sexo'
        },
        subtitle: {
            text: 'Fuente: SIIPDB'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        credits: {
            enabled: false
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Beneficiarios'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '<b>{point.y} de Beneficiarios</b>'
        },
        series: [{
            name: 'Sexo',
            data: [
                ['Masculino', <?php echo $TotalM?>],
                ['Femenino', <?php echo $TotalF?>]
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
</td>
</tr>
</table>
</div>
