<?php

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename= Reporte de Proyectos.xls");
header('Content-Transfer-Encoding: binary');
require '../../net.php';

$Resumenes = $bdd->prepare("SELECT * FROM Project_Summary");
$Resumenes->execute();

echo "<table>
        <tr>
            <th>Creado Por</th>
            <th>Nombre de Proyecto</th>
            <th>Financiador</th>
            <th>Monton</th>
            <th>Contrapartida</th>
            <th>Coordinador</th>
            <th>Fecha Desde</th>
            <th>Fecha Hasta</th>
            <th>Objetivo</th>
            <th>Tematica</th>
            <th>Resumen Proyecto</th>
        </tr>
        ";
while($DataR = $Resumenes->fetch())
{
    $Usuario = $bdd->prepare("Select NombreUsuario from usuario where Id = $DataR[1]");
    $Usuario->execute();
    
    $DataU = $Usuario->fetch();
    echo "<tr>
            <th>$DataU[0]</th>
            <th>$DataR[2]</th>
            <th>$DataR[3]</th>
            <th>$DataR[4]</th>
            <th>$DataR[5]</th>
            <th>$DataR[6]</th>
            <th>$DataR[7]</th>
            <th>$DataR[8]</th>
            <th>$DataR[9]</th>
            <th>";

    $ArrayTematica = explode ("." ,$DataR[10]);
    
    foreach($ArrayTematica as $value)
            echo "$value<br />";   
                     
    echo "  </th>
            
            <th>$DataR[11]</th>
        </tr>";
}
        
echo "";
?>