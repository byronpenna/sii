<?php require '../../net.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Resumen</title>
    <script src="../../js/jquery-1.7.2.min.js" type="text/javascript"></script>
</head>
<script >
function cerrar() 
{ 
    setTimeout(window.print,1500);
    setTimeout(window.close,1500); 
}
</script>
<body onload="cerrar();" style="font-family: Calibri; font-size: small;">
<?php
    
    if(isset($_POST['irp']))
	   $irp = $_POST['irp'];
    
    else
    {
        echo "<script>setTimeout(window.close,1500)</script>";   
    }
?>
<style>
.mytable td
{
    vertical-align: top;
} 
</style>
<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding:10px">    
<table style="width: 100%;">
    <tr>
        <td><h2 style="color: blue;">Resumen de Proyecto</h2></td>
        <td style="text-align: right;">
        <img src="../../images/logo.jpg" style="width: 200px;" />
        </td>
    </tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="2">
            <table style="width: 100%;" class="mytable">
            <?php
                   $Proyectos = $bdd->prepare("Select * from Project_Summary where IdSummary = $irp");
                   $Proyectos->execute();
                   
                   if($Proyectos->rowCount() > 0)
                   {
                        $DataP = $Proyectos->fetch();
                        
                        echo "<tr><th class='tdleft'style='width: 30%'>Nombre del proyecto:</th><td>$DataP[2]</td></tr>
                              <tr><th class='tdleft'>Entidad financiadora:</th><td>$DataP[3]</td></tr>
                              <tr><th class='tdleft'>Monto financiado:</th><td>$". number_format($DataP[4],2)."</td></tr>
                              <tr><th class='tdleft'>Monto contrapartioda:</th><td>$". number_format($DataP[5],2)."</td></tr>
                              <tr><th colspan='2'><br /></th></tr>
                              <tr><th class='tdleft'>Coordinador del proyecto:</th><td>$DataP[6]</td></tr>
                              <tr><th class='tdleft' style='vertical-align: top;'>Lineas estrat�gicas:</th><td>";
                              
                              $ArrayLinea = explode ("." ,$DataP[13]);
                        
                              foreach($ArrayLinea as $value)
                                  echo "$value<br />";
                            
                              echo "</td></tr>
                              <tr><th class='tdleft'>Fecha de inicio:</th><td>$DataP[7]</td></tr>
                              <tr><th class='tdleft'>Fecha de finalizaci�n:</th><td>$DataP[8]</td></tr>
                              <tr><th colspan='2'><br /></th></tr>
                              <tr><th class='tdleft' style='vertical-align: top;'>Objetivo general:</th><td>$DataP[9]</td></tr>
                              <tr><th class='tdleft' style='vertical-align: top;'>Descripci�n general:</th><td>$DataP[11]</td></tr>
                              <tr><th class='tdleft' style='vertical-align: top;'>Tem�tica del proyecto:</th><td>";
                        
                        $ArrayTematica = explode ("." ,$DataP[10]);
                        
                        foreach($ArrayTematica as $value)
                            echo "$value<br />";
                        
                        echo "    </td>
                              </tr>";
                   }
                   else
                        Redireccion("?l=ProjectSummary"); 
            ?>
            </table>
        </td>
    </tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="2">
            <table style="width: 100%;">
                <tr>
                    <td><h2 style="color: blue;">Participantes</h2></td>
                    <td style="text-align: right;"></a></td>
                </tr>
            </table>
        <?php
           $Participantes = $bdd->prepare("Select * from Project_Participants where IdSummary = $irp");
           $Participantes->execute();
           
           if($Participantes->rowCount() > 0)
           {
               $i = 0;
               echo "<table style='width: 100%; text-align: center;'>
                     <tr>
                         <th>N�</th>
                         <th>Participantes</th>
                         <th>Sexo</th>
                         <th>Rango de Edad</th>
                         <th>Cantidad</th>
                     </tr>"; 
               while($DataPa = $Participantes->fetch())
               {    
                    $i++;
                    echo "<tr>
                              <td>$i</td>
                              <td>$DataPa[1]</td>
                              <td>$DataPa[2]</td>
                              <td>$DataPa[3] A�os - $DataPa[4] A�os</td>
                              <td>$DataPa[5]</td>   
                          </tr>";
               }
               echo "</table>";
           }
           else
           {
                echo "<center>
                        <h2 style='color: red'>No hay comunidades registradas</h2>
                      </center>";
           }
        ?>
        </td>
    </tr>
    <tr><td colspan="2"><hr color='skyblue' /></td></tr>
    <tr>
        <td colspan="2">
            <table style="width: 100%;">
                <tr>
                    <td><h2 style="color: blue;">Comunidades de Impacto</h2></td>
                    <td style="text-align: right;"></td>
                </tr>
            </table>
        <?php
           $Participantes = $bdd->prepare("Select * from Project_Comunity where IdSummary = $irp");
           $Participantes->execute();
           
           if($Participantes->rowCount() > 0)
           {
               $i = 0;
               echo "<table style='width: 100%; text-align: center;'>
                     <tr>
                         <th>N�</th>
                         <th>Pais</th>
                         <th>Departamento</th>
                         <th>Municipio</th>
                         <th>Lugar</th>
                     </tr>"; 
               while($DataC = $Participantes->fetch())
               {    
                    $i++;
                    echo "<tr>
                              <td>$i</td>
                              <td>$DataC[1]</td>
                              <td>$DataC[2]</td>
                              <td>$DataC[3]</td>
                              <td>$DataC[4]</td>  
                          </tr>";
               }
               echo "</table>";
           }
           else
           {
                echo "<center>
                        <h2 style='color: red'>No hay comunidades registradas</h2>                
                      </center>";
           }
        ?>
        </td>
    </tr>    
</table>
</div>
</body>
</html>