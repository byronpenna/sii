<?php

/**
 * @author Manuel
 * @copyright 2013
 */

require '../net.php';

if(isset($_POST['enviar']))
{    
    if($_POST['enviar'] == "Agregar")
    {
        $insert = $bdd->prepare("Insert into Monitoreo_Boleta 
                                 values (null, :idEncuesta, :nombre) ");
                                 
        $insert->bindParam(':idEncuesta', $_SESSION["encuesta"]);
        $insert->bindParam(':nombre', $_POST["nombre"]);
        $insert->execute();
        
        Redireccion("../monitoreo.php?m=bole&n=1");          
    }
    elseif($_POST['enviar'] == "X")
    {
        $eliminar = $bdd->prepare("Delete from Monitoreo_Boleta where IdBoleta = :id");
        $eliminar->bindParam(':id', $_POST["id"]);
        $eliminar->execute();
        
        Redireccion("../monitoreo.php?m=bole&n=2");  
    }

    elseif($_POST['enviar'] == "?")
    {
        $_SESSION["boleta"]= $_POST["id"];
        Redireccion("../monitoreo.php?m=Whq");  
    }
    
    elseif($_POST['enviar'] == "D")
    {
        $_SESSION["boleta"]= $_POST["id"];
        Redireccion("../monitoreo.php?m=dato");  
    } 
    elseif($_POST['enviar'] == "R" || $_POST['enviar'] == "Resultados" || $_GET['r'] == "true")
    {
        $_SESSION["boleta"]= $_POST["id"];
        $_SESSION["IdDato"]= $_POST["idD"];
        Redireccion("../monitoreo.php?m=res");  
    }    
    elseif($_POST['enviar'] == "Guardar")
    {
        $eliminar = $bdd->prepare("Delete from Monitoreo_BoletaDatoFUSALMO where IdBoleta = :id");
        $eliminar->bindParam(':id', $_SESSION["boleta"]);
        $eliminar->execute();
        
        $IdDatoFusalmo = isset($_POST['DatoFusalmo']) ? $_POST['DatoFusalmo'] : array();    
           
        foreach($IdDatoFusalmo as $idf) 
        {
            $insert = $bdd->prepare("Insert into Monitoreo_BoletaDatoFUSALMO 
                                     values (null, :idb, :idf)");
                                     
            $insert->bindParam(':idb', $_SESSION["boleta"]);  
            $insert->bindParam(':idf', $idf);          
            $insert->execute();                        
        }
        
        Redireccion("../monitoreo.php?m=dato&n=1");  
    }
}
else
    Redireccion("../monitoreo.php");
?>