<?php
	
    
    if($_SESSION["encuesta"] != "")
    {
        $query = "Select * from Monitoreo_Encuesta as e
                  inner join Monitoreo_Boleta as b on e.IdEncuesta = b.IdEncuesta 
                  where IdBoleta = :id";
                    
        $datos = $bdd->prepare($query);
        $datos->bindParam('id', $_SESSION["boleta"]);
        $datos->execute();
        $datosEncuesta = $datos->fetch();
    }
    else
        Redireccion("../monitoreo.php");
?>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
    <a href="/monitoreo.php?m=bole" class='flecha' style='text-decoration:none'>Boleta</a>
</div>

<table style='width: 80%; background-color: white; border-radius: 10px; margin-left: 10%; border-radius: 10px; border-color: skyblue; border-style: groove; padding: 10px; margin-top: 20px;'>
<form action="monitoreo/BoletaABM.php" method="post" >

<tr><td colspan="2" style="text-align: center;"><h2>Relaci�n Boleta con Datos FUSALMO</h2></td></tr>
<tr style="height: 5px;"><td colspan="3" style="background-color: skyblue;"></td></tr>
<tr>
    <th colspan="2" >
        <table style="width: 80%; margin-left: 10%; margin-top: 20px; margin-bottom: 20px;">
        <tr>
        	<td style="text-align: right;">Nombre de Encuesta:</td><td style="padding-left: 20px;"><?php echo $datosEncuesta[1]?></td>
        </tr>
        <tr>
        	<td style="text-align: right;">Boleta de Encuesta:</td><td style="padding-left: 20px;"><?php echo $datosEncuesta[7]?></td>
        </tr>
        </table>
    <?php
	
    if(isset($_GET['n']))
    {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Datos guardados Exitosamente</div>";
    }
    
    ?>    
    </th>

</tr>
<tr style="height: 5px;"><td colspan="3" style="background-color: skyblue;"></td></tr>
<tr>
<th style='width: 70%;'><h2>Dato Fusalmo</h2></th><td style="text-align: center;" ><h2>Acci�n</h2></td>
</tr>
<tr style="height: 5px;"><td colspan="3"  style="background-color: skyblue;"></td></tr>
<?php

$DatosFusalmo2 = $bdd->prepare("SELECT * FROM  DatoFusalmo");
$DatosFusalmo2->execute();

if($DatosFusalmo2->rowCount() > 0)
{
    while($datos = $DatosFusalmo2->fetch())
    {
         echo "<tr>
                 <td style='text-align: center' >$datos[1]</td> 
                 <td style='text-align: center' >
                    <input type='hidden' name='IdDatoFusalmo' value='$datos[0]' />";
             
             $DatosFusalmo3 = $bdd->prepare("SELECT * FROM  Monitoreo_BoletaDatoFUSALMO 
                                             where IdDatoFUSALMO = $datos[0] and IdBoleta =" . $_SESSION["boleta"]);
             $DatosFusalmo3->execute();
             $chequeado = $DatosFusalmo3->fetch();
             
             if($chequeado[0] != "")
                echo      "<input type='Checkbox' Name='DatoFusalmo[]' value='$datos[0]' checked='true' />";
                       
                                
             else
                echo      "<input type='Checkbox' Name='DatoFusalmo[]' value='$datos[0]' />";
                
         echo   "</td>                             
               </tr>";
    }
        
     echo "<tr style='height: 5px;'><td colspan='3' style='background-color: skyblue;'></td></tr>
        <tr>
            <td colspan='2' style='text-align: right;'> 
                <input type='submit' style='margin-right:7%;' class='boton' value='Guardar' name='enviar' />
            <td>
        </tr>";
}
else
{
    echo "<tr style='height: 5px;'>
            <td colspan='2' style='text-align: center' ><h2 style='color:red'>No hay datos FUSALMO registrados</h2></td>
          </tr>";
}
?>
</form>
</table>