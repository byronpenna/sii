<?php

/**
 * @author Manuel
 * @copyright 2013
 */

require '../net.php';

if(isset($_POST['enviar']))
{    
    if($_POST['enviar'] == "Agregar")
    {
        
        $query = "Select * from Monitoreo_Criterios
                  where IdEncuesta = :idb";
                  
        $datos = $bdd->prepare($query);
        $datos->bindParam('idb', $_SESSION["encuesta"]);        
        $datos->execute();                
        $p = $datos->rowCount() + 1;

        $insert = $bdd->prepare("Insert into Monitoreo_Criterios 
                                 values (null, :vt, :vn, :vs, :p, :idb) ");

        $insert->bindParam(':vt', $_POST['vt']);
        $insert->bindParam(':vn', $_POST['vn']);
        $insert->bindParam(':vs', $_POST['vs']);
        $insert->bindParam(':p', $p );
        $insert->bindParam(':idb', $_SESSION["encuesta"]);
        
                        
        $insert->execute();
        
        Redireccion("../monitoreo.php?m=cri&n=1");           
    }
    if($_POST['enviar'] == "Modificar")
    {
        $update = $bdd->prepare("Update Monitoreo_Criterios set
                                 ValorTexto = :vt, 
                                 ValorNumero = :vn,
                                 ValorSimbolico = :vs
                                 where IdCriterio = :idc");

        $update->bindParam(':vt', $_POST['vt']);
        $update->bindParam(':vn', $_POST['vn']);
        $update->bindParam(':vs', $_POST['vs']);   
        $update->bindParam(':idc', $_POST['idc'] );                                     
        $update->execute();
        
        Redireccion("../monitoreo.php?m=cri&n=3");           
    }    
    elseif($_POST['enviar'] == "x")
    {

        $query = "Select * from Monitoreo_Criterios
                  where Posicion > :p and IdEncuesta = :idc
                  ORDER BY Posicion ASC ";
                  
        $datos = $bdd->prepare($query);
        $datos->bindParam('p', $_POST['p']);
        $datos->bindParam('idc', $_SESSION["encuesta"]);
        $datos->execute(); 

        while($actualizacionp = $datos->fetch())
        {   
            $nuevaPosicion = $actualizacionp[4] - 1;
            $update = $bdd->prepare("Update Monitoreo_Criterios set
                                     Posicion  = :np
                                     where IdCriterio = :id");
                                     
            $update->bindParam('np', $nuevaPosicion);
            $update->bindParam('id', $actualizacionp[0]);                                  
            $update->execute();  
        }
        
        $eliminar = $bdd->prepare("Delete from Monitoreo_Criterios  where IdCriterio = :idc");
        $eliminar->bindParam(':idc', $_POST['idc'] );
        $eliminar->execute();
        
        Redireccion("../monitoreo.php?m=cri&n=2");
                   
    }    
    elseif($_POST['enviar'] == "e")
        Redireccion("../monitoreo.php?m=crifo&ic=". $_POST['idc']);

    elseif($_POST['enviar'] == "+" || $_POST['enviar'] == "-")
    {
                
       if($_POST['enviar'] == "+")
       {
            $nuevaPosicion = $_POST['p']  ;     
            $viejaPosicion =  $_POST['p'] - 1 ;

            $update = $bdd->prepare("Update Monitoreo_Criterios set
                                     Posicion  = :np
                                     where IdEncuesta = :idc and Posicion = :ap");
                                     
            $update->bindParam('np', $nuevaPosicion); 
            $update->bindParam('ap', $viejaPosicion);               
            $update->bindParam('idc', $_SESSION["encuesta"]);                                     
            $update->execute();     
            
            $update2 = $bdd->prepare("Update Monitoreo_Criterios set
                                     Posicion  = :np
                                     where IdCriterio = :idc");
                                     
            $update2->bindParam('np', $viejaPosicion); 
            $update2->bindParam('idc', $_POST['idc']);                                                 
            $update2->execute();                       
            
            Redireccion("../monitoreo.php?m=cri&n=4");  
        }
       
       elseif($_POST['enviar'] == "-")
       {            
        
            $nuevaPosicion = $_POST['p']  ;     
            $viejaPosicion =  $_POST['p'] + 1 ;

            $update = $bdd->prepare("Update Monitoreo_Criterios set
                                     Posicion  = :np
                                     where IdEncuesta = :idc and Posicion = :ap");
                                     
            $update->bindParam('np', $nuevaPosicion); 
            $update->bindParam('ap', $viejaPosicion);               
            $update->bindParam('idc', $_SESSION["encuesta"]);                                     
            $update->execute();     

            $update2 = $bdd->prepare("Update Monitoreo_Criterios set
                                     Posicion  = :np
                                     where IdCriterio = :idc");
                                     
            $update2->bindParam('np', $viejaPosicion); 
            $update2->bindParam('idc', $_POST['idc']);                                                 
            $update2->execute();                       
            
            Redireccion("../monitoreo.php?m=cri&n=4");  
        }                
                  
    }       
    else
        Redireccion("../monitoreo.php?m=cri");          
        
}
else 
Redireccion("../monitoreo.php");
    
    
?>