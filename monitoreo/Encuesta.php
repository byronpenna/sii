<?php

/**
 * @author Manuel
 * @copyright 2013
 */
 
 if(isset($_GET['t']))
 $t = $_GET['t'];
 
 else
 $t = 0;

    if(isset($_POST['idb']) || $_SESSION["boleta"] != "")
    {
            if(isset($_POST['idb']))
            $IdBoleta = $_POST['idb'];
            
            else
            $IdBoleta = $_SESSION["boleta"];

            $IdUsurio = $_SESSION["IdUsuario"];

            $query = "SELECT * FROM Monitoreo_Encuesta  as e
                      inner join Monitoreo_Boleta  as b on e.IdEncuesta = b.IdEncuesta
                      WHERE b.IdBoleta = :id";
            
            $datos = $bdd->prepare($query);
            $datos->bindParam('id', $IdBoleta);
            $datos->execute();
            $DatosEB = $datos->fetch();         

            $IdEncuesta = $DatosEB[0];

            $TotalT = $bdd->prepare("SELECT * FROM Monitoreo_Tema WHERE IdBoleta = $IdBoleta");
            $TotalT->execute();
                                    
            if($t >= $TotalT->rowCount())
                   $t = 1;         
    }
    else
    Redireccion("/monitoreo.php?");              
?>

<script language="javascript">
function Validar()
{
    var name = 'Criterio[]';	
	var keywords = document.getElementsByName(name);
	
	for (var i=0; i<keywords.length; i++)
	{
		if(keywords[i].value == '-- ')
        {
            alert('Falta seleccionar alguna respuesta de una o mas preguntas;')
            return false;
        }       
	}
    
    if(confirm("�Esta Seguro de Pasar el Tema?"))   
    return true; 
}

function BuscarSubPregunta(Id)
{    
     var Criterio = document.getElementById(Id)
     var SubPregunta = document.getElementsByName(Id);
     
     for (x=0;x<SubPregunta.length;x++)
         SubPregunta[x].style.display = "none";
     
     
     if(Criterio.value != "-- ")
     {        
         for (x=0;x<SubPregunta.length;x++){
            
            <?php

                $datosPs = $bdd->prepare("SELECT s.*
                                         FROM Monitoreo_SubPregunta AS s
                                         INNER JOIN Monitoreo_Pregunta AS p ON s.IdPregunta = p.IdPregunta
                                         INNER JOIN Monitoreo_Tema AS t ON p.IdTema = t.IdTema
                                         INNER JOIN Monitoreo_Boleta AS b ON t.IdBoleta = b.IdBoleta
                                         WHERE b.IdBoleta = :id");
                $datosPs->bindParam('id', $IdBoleta);
                $datosPs->execute();
                
                while($sub = $datosPs->fetch())
                {	
                ?>            
                 if(Criterio.value == <?php echo $sub[2]?> &&  SubPregunta[x].id == <?php echo $sub[3]?>)
                 {                                                   
                             SubPregunta[x].style.display = "inline";
                             return;
                 }
                 else
                        SubPregunta[x].style.display = "none";
                <?php
        	    }
                ?>
         }
     }
     else
     {
         for (x=0;x<SubPregunta.length;x++)
         SubPregunta[x].style.display = "none";
     }
}
</script>
        

<table style="width: 90%; margin-left:5%; background-color: white; border-color: skyblue; border-radius: 10px; padding: 10px;">
    <form action="monitoreo/EncuestaABM.php" method="post">
    <tr>
        <td ><h2>Encuesta:</h2></td><td colspan='2'><h2><?php echo $DatosEB[1]?></h2></td>
        <input type="hidden" name="idb" value="<?php echo $IdBoleta?>" />
    </tr>
    <tr>
        <td ><h2>Boleta:</h2></td><td colspan='2'><h2><?php echo $DatosEB[7]?></h2></td>         
    </tr> 
    <tr>
        <td colspan="3" style="text-align: right;" ><h2 style="padding-right: 30px;">Tema <?php echo $t + 1 . " de " .$TotalT->rowCount();?></h2></td>         
    </tr>    
    <tr><td colspan="4"><hr style='color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;' /></td></tr>
    <?php
            
            $datosT = $bdd->prepare("SELECT * FROM Monitoreo_Tema WHERE IdBoleta = $IdBoleta LIMIT  $t , 1");
            $datosT->execute();
            
            $i = 0;
            
            while($Tema = $datosT->fetch())
            {   
                $i++;                
                echo "<tr><td colspan='4' style='color:Green; padding-top: 20px'>$i - $Tema[2]</td><td><td></tr>";
                
                $datosP = $bdd->prepare("SELECT * FROM Monitoreo_Pregunta WHERE IdTema = $Tema[0] and Categoria='Pregunta'");
                $datosP->execute();

                $j = 0;
                while($Preguntas = $datosP->fetch())
                {
                    $j++;
                    echo "<tr>
                              <td style='color:blue; padding-left:10%; padding-top: 20px'>$i.$j</td>
                              <td colspan='2' style='color:blue; padding-top: 20px'>$Preguntas[2]</td>
                              <input type='hidden' value='$Preguntas[0]' name='Pregunta[]' />
                              <input type='hidden' value='$t' name='t' />
                              <input type='hidden' value='$IdBoleta' name='idboleta' />
                              <input type='hidden' value='".$TotalT->rowCount()."' name='totalTemas' />
                              <td style='padding-left:5%;padding-top: 20px'>";                              
                    echo "        <select name='Criterio[]' id='$Preguntas[0]' onChange='BuscarSubPregunta($Preguntas[0])' >
                                        <option value='-- ' >Seleccionar </option>";              
    	                           $DatosC = $bdd->prepare("SELECT * FROM Monitoreo_Criterios where IdEncuesta = $IdEncuesta ORDER BY  Posicion ASC ");
                               	   $DatosC->execute();
                                   
                                   if($DatosC->rowCount() > 0)
                                        while($Criterios= $DatosC->fetch())
                                        {
                                            $VerificarP = $bdd->prepare("SELECT * FROM Monitoreo_Resultado where IdUsuario = $IdUsurio and IdPregunta = $Preguntas[0]");
                               	            $VerificarP->execute();
                                            $VerificarCriterio = $VerificarP->fetch();
                                            
                                            if($VerificarCriterio[4] == $Criterios[0])                                            
                                                echo "<option value='$Criterios[0]' selected='true' >$Criterios[3]  $Criterios[1]</option>";
                                            
                                            else
                                                echo "<option value='$Criterios[0]' >$Criterios[3]  $Criterios[1]</option>";
                                        }
                                        
                                   else
                                        echo "<option value='----' selected='true' style='color:red'>Error, Criterios no ingresados</option>";
                                        
                    echo "         </select>
                              <td>
                          </tr>";
                    
                    echo "<tr><td colspan='1'></td><td colspan='3' style='text-align: left'>";
                                                  
                       $DatosS = $bdd->prepare("SELECT * 
                                                FROM Monitoreo_SubPregunta AS sub
                                                INNER JOIN Monitoreo_Pregunta AS pre ON sub.IdPreguntaBuscar = pre.IdPregunta
                                                where sub.IdPregunta = $Preguntas[0]                                                            
                                                GROUP BY sub.IdPreguntaBuscar");
                   	   $DatosS->execute();
                       
                       if($DatosS->rowCount() > 0)
                       {                                                              
                            while($SubPregunta= $DatosS->fetch())
                            {   
                                $VerificarP = $bdd->prepare("SELECT * FROM Monitoreo_Resultado where IdUsuario = $IdUsurio and IdPregunta = $SubPregunta[3]");
                               	$VerificarP->execute();
                                $VerificarCriterio = $VerificarP->fetch();                                                            
                                
                                if($VerificarP->rowCount() > 0)
                                $display = "inline";
                                
                                else 
                                $display = "none";
                                
                                echo "<div style='display:$display' name='$Preguntas[0]'  id='$SubPregunta[4]' ><br />$SubPregunta[6]<br />
                                          <input type='hidden' name='subpregunta[]' value='$SubPregunta[3]'  />
                                          <textarea name='respsubpregunta[]' style='width:500px; height:50px'>$VerificarCriterio[4]</textarea><br /><br /> 
                                      </div>";
                            }
                       }
                    echo "</td><tr>";
                }
            }
    ?>
    <tr><td colspan="4"><br /><br /><hr style='color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;' /></td></tr>
    <tr>    
        <td style="text-align: center;">
            <?php if($t > 0){?><input type="submit" style="width: 150px;" name="enviar" value="Pregunta Anterior" class="boton" /><?php }?>
        </td>
        <td colspan="2" style="text-align: center;"></td>
        <td style="text-align: center;">
            <?php if(($t+1) < $TotalT->rowCount()) {?><input type="submit" style="width: 150px;" name="enviar" value="Siguiente Pregunta" class="boton" onclick="return Validar();" /><?php }?>
            <?php if(($t+1) == $TotalT->rowCount()) {?><input type="submit" style="width: 150px;" name="enviar" value="Finalizar" class="boton" onclick="return Validar();" /><?php }?>
        </td>
    </tr>
    </form>
</table>