<?php

/**
 * @author Manuel
 * @copyright 2013
 */

    if($_SESSION["encuesta"] != "")
    {
        $query = "Select * from Monitoreo_Encuesta as enc
                  where enc.IdEncuesta = :idb";
                  
        $datos = $bdd->prepare($query);
        $datos->bindParam('idb', $_SESSION["encuesta"]);        
        $datos->execute();
        $datosEncuesta = $datos->fetch();
    }
    else
        Redireccion("../monitoreo.php");
?>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
    <a href="/monitoreo.php?m=cri" class='flecha' style='text-decoration:none'>Criterios</a>
</div>
<form action="monitoreo/CriteriosABM.php" method="post">
<?php
	if(isset($_GET['ic']))
    {
        $datos = $bdd->prepare("Select * from Monitoreo_Criterios where IdCriterio = :id");
        $datos->bindParam(':id',$_GET['ic']);
        $datos->execute();
        
        $criterios = $datos->fetch();
        $abm = "Modificar";
        echo "<input type='hidden' name='idc' value='".$_GET['ic']."' />";
    }
    else{
        $criterios[1] = $criterios[2] = $criterios[3] = "";
        $abm = "Agregar";
    }
?>
<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; ">
<table class='Datos' style="margin-left: 7%; width: 84%;">

<tr>
	<th colspan="2"><h2>Boletas para <br /> <?php echo $datosEncuesta[1];?></h2>
    <hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" />
    </th>    
</tr>

<tr>
	<th colspan="2"><h2> Agregar nuevo criterio de evaluaci�n</h2></th>    
</tr>

    <tr><td style="text-align: right; padding-right: 20px; width: 50%;">Valor Textual:</td>
        <td style="color: red;"><input type="text" name="vt" value="<?php echo $criterios[1]?>" style="width: 100px;"  /></td></tr>
        
    <tr><td style="text-align: right; padding-right: 20px">Valor Num�rico:</td>
        <td style="color: red;"><input type="text" name="vn" value="<?php echo $criterios[2]?>" style="width: 100px;" /></td></tr>
        
    <tr><td style="text-align: right; padding-right: 20px">Valor Simbolico:</td><td style="color: red;">
        <input type="text" name="vs" value="<?php echo $criterios[3]?>" style="width: 100px;"  /></td></tr>
        
    <tr><td colspan="2" style="text-align: center;">  <input type="submit" class="boton" value="<?php echo $abm?>" name="enviar" /></td></tr>
    
</table>
</form>
</div>
<div class="clr"></div>