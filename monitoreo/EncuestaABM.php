<?php

/**
 * @author Manuel
 * @copyright 2013
 */
require '../net.php';
if(isset($_POST['enviar']))
{    
    if($_POST['enviar'] == "Siguiente Pregunta" || $_POST['enviar'] == "Finalizar"  )
    {
        
        
        $preguntas = isset($_POST['Pregunta']) ? $_POST['Pregunta'] : array();    
        $subpregunta = isset($_POST['subpregunta']) ? $_POST['subpregunta'] : array();
        $Criterio = isset($_POST['Criterio']) ? $_POST['Criterio'] : array();
        $respsubpregunta = isset($_POST['respsubpregunta']) ? $_POST['respsubpregunta'] : array();
           
           
        foreach($preguntas as $index => $value) 
        {
            $delete = $bdd->prepare("Delete from Monitoreo_Resultado where IdUsuario = :idu and IdPregunta = :idp");
            $delete->bindParam(':idu', $_SESSION["IdUsuario"]);
            $delete->bindParam(':idp', $preguntas[$index]);
            $delete->execute();
            
            $insert = $bdd->prepare("Insert into Monitoreo_Resultado 
                                     values (null, :IdBoleta, :IdUsuario, :IdPregunta, :Resultado, :fecha)");
                                     
            $insert->bindParam(':IdBoleta', $_POST['idb']);
            $insert->bindParam(':IdUsuario', $_SESSION["IdUsuario"]);
            $insert->bindParam(':IdPregunta', $preguntas[$index]);
            $insert->bindParam(':Resultado', $Criterio[$index]);
            $insert->bindParam(':fecha', date("Y-m-d"));        
            $insert->execute();
        }
        
        foreach($subpregunta as $index => $value) 
        {   
            $delete = $bdd->prepare("Delete from Monitoreo_Resultado where IdUsuario = :idu and IdPregunta = :idp");
            $delete->bindParam(':idu', $_SESSION["IdUsuario"]);
            $delete->bindParam(':idp', $subpregunta[$index]);
            $delete->execute();
                        
            
            if($respsubpregunta[$index] != ""){
                
            $insert = $bdd->prepare("Insert into Monitoreo_Resultado 
                                     values (null, :IdBoleta, :IdUsuario, :IdPregunta, :Resultado, :fecha)");
                                     
            $insert->bindParam(':IdBoleta', $_POST['idb']);
            $insert->bindParam(':IdUsuario', $_SESSION["IdUsuario"]);
            $insert->bindParam(':IdPregunta', $subpregunta[$index]);
            $insert->bindParam(':Resultado', $respsubpregunta[$index]); 
            $insert->bindParam(':fecha', date("Y-m-d"));                    
            $insert->execute();
            }
        }        
        
        $_SESSION["boleta"] = $_POST['idboleta'];
        $t = $_POST['t'] + 1;      
        
        if($_POST['enviar'] == "Finalizar")
            Redireccion("../monitoreo.php?n=1");
        
        else
            Redireccion("../monitoreo.php?m=enc&t=$t");          
    }
    if($_POST['enviar'] == "Pregunta Anterior")
    {
        $_SESSION["boleta"] = $_POST['idboleta'];
        $t = $_POST['t'] - 1;      
    
        Redireccion("../monitoreo.php?m=enc&t=$t");
    }
    
}
else
    Redireccion("../monitoreo.php?m=enc");
        

        
?>