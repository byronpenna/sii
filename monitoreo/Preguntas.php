<?php
    
    if($_SESSION["encuesta"] != "" && $_SESSION["boleta"] != "")
    {
        $query = "Select * from Monitoreo_Encuesta as enc
                  inner join Monitoreo_Boleta as bol 
                  on bol.IdEncuesta = enc.IdEncuesta  
                  where bol.IdBoleta = :idb";
                  
        $datos = $bdd->prepare($query);
        $datos->bindParam('idb', $_SESSION["boleta"]);        
        $datos->execute();
        $datosEncuesta = $datos->fetch();
    }
    else
        Redireccion("../monitoreo.php");
?>

<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
    <a href="/monitoreo.php?m=bole" class='flecha' style='text-decoration:none'>Boleta</a> ->
    <a href="/monitoreo.php?m=Whq" class='flecha' style='text-decoration:none'>Preguntas</a>
</div>
<?php 
    if(isset($_GET['n']))
    {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Tema Agregados Exitosamente</div>";

        elseif($_GET['n'] == 2)
            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>Pregunta Agregada Exitosamente</div>";   

        elseif($_GET['n'] == 3)
            echo "<div id='Notificacion' name='Notificacion' style='color:red;'>Pregunta Eliminada Exitosamente</div>";   

        elseif($_GET['n'] == 4)
            echo "<div id='Notificacion' name='Notificacion' style='color:green;'>SubPregunta Agregada Exitosamente</div>";                                    
    }
?>
<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; ">
    <table class='Datos' style="width: 100%;">

    <tr>
    	<th colspan="2"><h2>Encuestas <?php echo $datosEncuesta[1];?> </h2>     
    </tr>
    <tr>
        <th colspan="2" style="border-color: skyblue; border-bottom-style: groove;">
            <h2>Boletas <?php echo $datosEncuesta[7];?> </h2>        
        </th>
    </tr>
    <tr>
        <td style=" border-color: skyblue; border-bottom-style: groove; width: 50%;">
            <form action="monitoreo/PreguntasABM.php" method="post" >
            <input type="hidden" name="idboleta" value="<?php echo $datosEncuesta[5];?>"  />
            <table style="width: 90%; margin-left: 5%;">
                <tr>
                	<th colspan="2"><h2>Agregar nuevo tema </h2></th>
                </tr>
                <tr><td style="text-align: right; padding-right: 20px;">Nombre de la tema</td><td style="color: red;" >
                     <input type="text" name="tema" value="" required="true" title="Campo Requerido" />*</td></tr>
                     
                <tr><td colspan="2" style="text-align: center;"><input type="submit" value="Agregar" name="enviart" class="boton" /></td></tr>
            </table>
            </form>
        </td>
        
        <td style="text-align: right; padding-right: 20px; border-color: skyblue; border-bottom-style: groove; width: 50%;">

            <table style="width: 90%; margin-left: 5%; ">
            <form action="monitoreo/PreguntasABM.php" method="post" >
                <tr>
                    	<th colspan="2"><h2>Agregar nueva Pregunta </h2>
                        <input type="hidden" name="idboleta" value="<?php echo $datosEncuesta[5];?>"  />
                        </th>
                </tr>
                <tr><td style="text-align: right; padding-right: 20px;">Tema:</td>
                    <td>
                            <select id="temap" name="temap" style="width: 200px;">                   
                                <?php
    	                           $temas = $bdd->prepare("SELECT * FROM Monitoreo_Tema where IdBoleta = :idBoleta");
                                   $temas->bindParam('idBoleta', $datosEncuesta[5] );
                               	   $temas->execute();
                                   
                                   if($temas->rowCount() > 0)
                                        while($Tdatos = $temas->fetch())
                                            echo "<option value='$Tdatos[0]' selected='true'>$Tdatos[2]</option>";
                                            
                                   else
                                        echo "<option value='----' selected='true' style='color:red'>Error, Ingresar un tema</option>";
    
                                ?>
                            </select>
                    </td>
                </tr>
                    <tr><td style="text-align: right; padding-right: 20px;">Pregunta:</td><td style="color: red;" >
                         <textarea name="pregunta" style="width: 300px; height: 50px;"></textarea>*</td></tr>
                         
                    <tr><td colspan="2" style="text-align: center;"><input type="submit" value="Agregar" name="enviar" class="boton" /></td></tr>                                
            </form>
            </table>    


       
        </td>
    </tr>




  
    <tr>
        <td colspan="2" style="border-color: skyblue;" >
            <table>
                <form action="monitoreo/PreguntasABM.php" method="post" >
                <input type="hidden" name="idboleta" value="<?php echo $datosEncuesta[5];?>"  />
                    <tr>
                    	<th colspan="4"><h2>Temas y Preguntas</h2></th>
                    </tr>
                    <?php
                       $temas = $bdd->prepare("SELECT * FROM Monitoreo_Tema where IdBoleta = :idBoleta");
                       $temas->bindParam('idBoleta', $datosEncuesta[5] );
                   	   $temas->execute();
                       
                       if($temas->rowCount() > 0)
                       {
                            while($Tdatos = $temas->fetch())
                            {   
                                echo "<tr>
                                          <th colspan='4' style='border-color: skyblue; border-top-style: groove; text-align: left; padding-left: 50px;' ><h2 style='color:Green;'>Tema: $Tdatos[2]</h2><th> 
                                      </tr>";
                                
                                $preguntas = $bdd->prepare("SELECT * FROM Monitoreo_Pregunta where IdTema = :IdTema and Categoria = 'Pregunta'");
                                $preguntas->bindParam('IdTema', $Tdatos[0] );
                           	    $preguntas->execute();
                                
                                if($preguntas->rowCount() > 0){  
                                    
                                    $i = 0;
                                    while($datosP = $preguntas->fetch())
                                    {
                                        $i++;
                                        echo "<tr><td style='width:5%; text-align: center;'><strong>$i</strong></td>
                                                  <td style='padding-top: 10px; padding-bottom: 10px;'>$datosP[2]</td>                                                      
                                                  <td style='text-align: center;width:10%;'><a href='monitoreo.php?m=Subq&ip=$datosP[0]' style='text-decoration: none; color: blue; width: 40px;'  >Agregar subPregunta</a></td>
                                                  <td style='text-align: center;width:10%;'><a href='monitoreo/PreguntasABM.php?ip=$datosP[0]' style='text-decoration: none; color: red; width: 40px;' onClick='return confirmar();'  >Eliminar Pregunta</a></td>
                                              </tr>";
                                            
                                        $subpreguntas = $bdd->prepare("SELECT * FROM Monitoreo_SubPregunta                                        
                                                                       where IdPregunta = :idp
                                                                       GROUP BY  IdPreguntaBuscar");
                                                                       
                                        $subpreguntas->bindParam('idp', $datosP[0] );
                                   	    $subpreguntas->execute();
                                        
                                        $j = 0;
                                        
                                        while($datosSubPregunta = $subpreguntas->fetch())
                                        {
                                            $j++;
                                            echo "<tr>
                                                      <td></td>
                                                      <td colspan='3' style='color: blue; padding-top: 5px; padding-bottom:5px'>$i.$j -"; 
                                            
                                            $pregunta = $bdd->prepare("SELECT * FROM Monitoreo_Pregunta                                        
                                                                           where IdPregunta = :idp");
                                                                       
                                            $pregunta->bindParam('idp', $datosSubPregunta[3] );
                                       	    $pregunta->execute();
                                            $textosub = $pregunta->fetch();
                                            echo $textosub[2];
                                        
                                            
                                            
                                            echo "    </td>
                                                  </tr>"; 
                                           
                                        }
                                         
//
                                    }    
                                }
                                else
                                        echo "<tr><td colspan='2' style='text-align: center;'> <h2 style='color: red'>Agrega preguntas al Tema</h2></td></tr>";
                            }
                       }
                       else                       
                             echo "<tr><td colspan='4' style='color:red; text-align: center'>Error, Datos no encontrados</td></tr>";
                       
                    ?>
                </form>
            </table>
        </td>
    </tr>    
    </table>
</div>
<div class="clr"></div>