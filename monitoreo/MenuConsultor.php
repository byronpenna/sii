<style type="text/css">
#menujoven {
	width: 12em;
	padding: 0 0 1em 0;
	margin-bottom: 1em;
	font:normal 12px/1.8em Comic Sans MS;
	color: #333;
    border-radius: 10px;
}

#menujoven ul {
	list-style: none;
	margin: 0;
	padding: 0;
	border: none;
    border-radius: 10px;
}
	
#menujoven li {
	border-bottom: 1px solid #90bade;
	margin: 0;
	list-style: none;
	list-style-image: none;
    border-radius: 10px;
}
	
#menujoven li a {
	display: block;
	padding: 5px 5px 5px 0.5em;
	border-left: 10px solid #1958b7;
	border-right: 10px solid #508fc4;
	background-color: #2175bc;
	color: #fff;
	text-decoration: none;
	width: 100%;
    border-radius: 10px;
}

body #menujoven li a {
	width: auto;
    border-radius: 10px;
}

#menujoven li a:hover {
	border-left: 10px solid #1c64d1;
	border-right: 10px solid #5ba3e0;
	background-color: #2586d7;
	color: #fff;
    border-radius: 10px;
}
</style>

<div id="menujoven" style="border-radius: 10px;">
  <ul>
    <li><a href="monitoreo.php?m=pruf">Actualizar</a></li>       
    <li><a href="monitoreo.php?m=bole">Boletas</a></li>
    <li><a href="monitoreo.php?m=cri">Criterios</a></li>
    <li><a href="monitoreo.php?m=Stad">Datos Cuantitativos</a></li>       
  </ul>
</div>