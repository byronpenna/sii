<?php

    if($_SESSION["boleta"] != "" || $_SESSION["IdDato"] != "" || isset($_GET["c"]))
    {
        if($_SESSION["IdDato"] == "")
        {
        $query = "SELECT e.IdEncuesta, e.Nombre, b.NombreBoleta, df.Nombre, t.IdTema, t.Tema, p.Pregunta
                  FROM Monitoreo_Encuesta  AS e
                  INNER JOIN Monitoreo_Boleta       AS b ON e.IdEncuesta = b.IdEncuesta
                  INNER JOIN Monitoreo_Tema         AS t ON t.IdBoleta = b.IdBoleta
                  INNER JOIN Monitoreo_Pregunta     as p on p.IdTema = t.IdTema 
                  INNER JOIN Monitoreo_Resultado    AS r ON b.IdBoleta        = r.IdBoleta
                  INNER JOIN InscripcionDatoFusalmo AS i ON r.IdUsuario       = i.IdUsuario
                  INNER JOIN DatoFusalmo            AS df ON df.IdDatoFUSALMO = i.IdDatoFUSALMO
                  WHERE p.IdPregunta = :id 
                  GROUP BY i.IdUsuario ";
                  
        $datos = $bdd->prepare($query);
        $datos->bindParam('id', $_GET["c"]);
        
        }
        else
        {
            $query = "SELECT e.IdEncuesta, e.Nombre, b.NombreBoleta, df.Nombre, t.IdTema, t.Tema, p.Pregunta
                      FROM Monitoreo_Encuesta  AS e
                      INNER JOIN Monitoreo_Boleta       AS b ON e.IdEncuesta = b.IdEncuesta
                      INNER JOIN Monitoreo_Tema         AS t ON t.IdBoleta = b.IdBoleta
                      INNER JOIN Monitoreo_Pregunta     as p on p.IdTema = t.IdTema 
                      INNER JOIN Monitoreo_Resultado    AS r ON b.IdBoleta        = r.IdBoleta
                      INNER JOIN InscripcionDatoFusalmo AS i ON r.IdUsuario       = i.IdUsuario
                      INNER JOIN DatoFusalmo            AS df ON df.IdDatoFUSALMO = i.IdDatoFUSALMO
                      WHERE p.IdPregunta = :id AND i.IdDatoFUSALMO = :idI
                      GROUP BY i.IdUsuario";
                      
            $datos = $bdd->prepare($query);
            $datos->bindParam('id', $_GET["c"]);  
            $datos->bindParam('idI', $_SESSION["IdDato"]);          
        }
        
        $datos->execute();
        $datosEncuesta = $datos->fetch();
        
        $idtema = $datosEncuesta[4];
        $idEncuesta = $datosEncuesta[0];
        
    }
    else
       Redireccion("../monitoreo.php?m=res");
       
       
if($_SESSION["TipoUsuario"] == "Consultor" || $_SESSION["TipoUsuario"] == "Administrador" )
{                     
?>
<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/monitoreo.php?m=det" class='flecha' style='text-decoration:none'>Encuesta</a> ->
    <a href="/monitoreo.php?m=bole" class='flecha' style='text-decoration:none'>Boleta</a> ->
    <a href="/monitoreo.php?m=res" class='flecha' style='text-decoration:none'>Respuestas</a>
</div>
<?php } 
        if($datos->rowCount() == 0)
            Redireccion("../monitoreo.php?m=res");
?>

<div style="width: 100%; text-align: left; background-color: white; border-radius: 10px; padding-bottom: 20px; ">
<table class='Datos' style="margin-left: 7%; width: 84%;">
<form action="monitoreo/BoletaABM.php" method="post" >
<tr>
    <td><a href="monitoreo.php?m=resPre&t=<?php echo $idtema;?>" style="text-decoration: none;">
         <input style="width: 150px;" type="button" class="boton" value="<- Regresar Temas" /></a></td>
    <td colspan="4" style="text-align: center;"><h2>Resultado General de Boleta </td></tr>    
<tr>
    <td style="text-align: right; padding-right: 20px; width: 25%;">Encuesta:</td>
    <td colspan="4"> <?php echo $datosEncuesta[1];?> </h2></td>
</tr>
<tr>
    <td style="text-align: right; padding-right: 20px;">Boleta:</td>
    <td colspan="4"> <?php echo $datosEncuesta[2];?> </h2></td>
</tr>
<?php if($_SESSION["IdDato"] == ""){?>
<tr>
    <td style="text-align: right; padding-right: 20px;">Dato FUSALMO:</td>
    <td colspan="4">Todos los Involucrados</h2></td>
</tr>           
<?php }else{?>
<tr>
    <td style="text-align: right; padding-right: 20px;">Dato FUSALMO:</td>
    <td colspan="4"> <?php echo $datosEncuesta[3];?> </h2></td>
</tr>   
<?php }?>
<tr>
    <td style="text-align: right; padding-right: 20px;">Tema:</td>
    <td colspan="4"> <?php echo $datosEncuesta[5];?> </h2></td>
</tr>

<tr>
    <td style="text-align: right; padding-right: 20px;">Pregunta:</td>
    <td colspan="4"> <?php echo $datosEncuesta[6];?> </h2></td>
</tr>

<tr><td colspan="5"><hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" /></td></tr>
<tr>
    <td style="text-align: right; padding-right: 20px;">Encuestados:</td>
    <td colspan="4"> <?php echo $datos->rowCount();?> Jovenes </h2></td>
</tr>
<tr><td colspan="5">
        <table rules='all' style="width: 90%; margin-left: 5%;">
            <tr>
                <td style="text-align: center; width: 10%;">Posicion</td>
                <td style="width: 10%;text-align: center">Criterio Textual</td>
                <td style="width: 10%;text-align: center">Criterio Numérico</td>
                <td style="width: 10%;text-align: center">Criterio Simbólico</td>
                <td style="width: 10%;text-align: center;"><strong>Cantidad Respuestas</strong></td>
            </tr>         
<?php

        $query = "SELECT * FROM Monitoreo_Criterios where IdEncuesta = :id
                  ORDER BY Posicion ASC ";                  
                  
        $datosR = $bdd->prepare($query);
        $datosR->bindParam('id', $idEncuesta);  
        $datosR->execute();
                                 
        $total = 0;
        
        while($datosResultado = $datosR->fetch())
        {
            echo "<tr>"; 
            echo "<td style='text-align:center;'>$datosResultado[4]</td>";
            echo "<td style='text-align:center;'>$datosResultado[1]</td>";
            echo "<td style='text-align:center;'>$datosResultado[2]</td>";
            echo "<td style='text-align:center;'>$datosResultado[3]</td>";
                    
                    if($_SESSION["IdDato"] != "")
                    {          
                        $queryC = "SELECT * FROM Monitoreo_Resultado as r
                                   inner join InscripcionDatoFusalmo as df on r.IdUsuario = df.IdUsuario
                                   where IdPregunta = :idp and Resultado = :idc and df.IdDatoFUSALMO = " . $_SESSION["IdDato"].
                                   " GROUP BY r.IdUsuario";
                    }
                    else
                    {
                        $queryC = "SELECT * FROM Monitoreo_Resultado as r
                                   inner join InscripcionDatoFusalmo as df on r.IdUsuario = df.IdUsuario
                                   where IdPregunta = :idp and Resultado = :idc
                                   GROUP BY r.IdUsuario";
                    }
                    
                    $datosC = $bdd->prepare($queryC);                                             
                    $datosC->bindParam('idp', $_GET["c"]);
                    $datosC->bindParam('idc', $datosResultado[0]);    
                    $datosC->execute();
                    $total = $datosC->rowCount() + $total;
                    
            echo "<td style='text-align:center; color: darkblue'><strong>".$datosC->rowCount()."</strong></td>";
            echo "</tr>";
        }
        

            echo "<tr>"; 
            echo "<td colspan='4' style='text-align:right; padding-right: 20px; padding-top: 10px;'><strong>Total de Respuestas:</strong> </td>";
            echo "<td style='text-align:center; color: darkblue; padding-top: 10px;'><strong>$total</strong></td>";
            echo "</tr>";          	
?>
        </table>
    </td>    
</tr>
<tr><td colspan="5"><hr style="color: skyblue; background-color: skyblue; height: 5px; width: 100%; margin-top: 5px;" /></td></tr>

<tr><td colspan="5" style="padding-left: 20px ;"><h2>Sub Preguntas</h2></td></tr>
<?php

    $datosS = $bdd->prepare("SELECT * 
                             FROM Monitoreo_SubPregunta AS s
                             INNER JOIN Monitoreo_Pregunta AS p ON s.IdPreguntaBuscar = p.IdPregunta
                             WHERE s.IdPregunta = :id
                             GROUP BY IdPreguntaBuscar");
         
    $datosS->bindParam('id',$_GET["c"] );         
    $datosS->execute();
        
    $s = 0;
    while ($Subpregunta = $datosS->fetch()) 
    {        
        $s++;
        echo "<tr><td colspan='5' style='color: darkblue; padding-top: 20px'><h2>$s - $Subpregunta[6]</h2></td>";
        
        $datosSc = $bdd->prepare("SELECT * FROM Monitoreo_SubPregunta AS s
                                  INNER JOIN Monitoreo_Criterios AS c ON s.IdCriterios = c.IdCriterio 
                                  WHERE s.IdPreguntaBuscar = :id GROUP BY c.ValorTexto");

        $datosSc->bindParam('id',$Subpregunta[4]);         
        $datosSc->execute();
        echo "<tr><td colspan='5' style='color: darkblue; padding-bottom: 20px'>";
        while($criterio = $datosSc->fetch())
        {
            $idSubP = $criterio[3];
            echo " $criterio[5] ";
        }
        echo "</td></tr>";   
        
        
        if($_SESSION["IdDato"] == "")
        {          
        $QueryDato = "SELECT * FROM Monitoreo_Resultado 
                      WHERE IdPregunta = :id";
        }
        
        else
        {
        $QueryDato = "SELECT * FROM Monitoreo_Resultado AS r
                      INNER JOIN InscripcionDatoFusalmo AS df ON r.IdUsuario = df.IdUsuario
                      WHERE r.IdPregunta = :id AND df.IdDatoFUSALMO = " . $_SESSION["IdDato"] . " GROUP BY r.IdUsuario";
        }
        
        $datosSr = $bdd->prepare($QueryDato);
 
        $datosSr->bindParam('id', $idSubP);
        $datosSr->execute();
        
        $j = 0;
        while($respuesta = $datosSr->fetch())
        {
            $j++;
            echo "<tr><td style='text-align: right; padding-right: 10px; colors: green;'>$s.$j</td><td colspan='4' style='padding-top: 10px'>$respuesta[4]</td></tr>";
        }
    }

	echo "";
?>
</table>
</div>
<div class="clr"></div>