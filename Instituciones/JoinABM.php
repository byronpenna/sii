<?php

/**
 * @author Manuel
 * @copyright 2013
 */
require '../net.php';

if(isset($_POST['Enviar']))
{
    if($_POST['Enviar'] == "Guardar Grupos")
    {
        $eliminar = $bdd->prepare("Delete from InscripcionSeccion 
                                   where IdDatoFusalmo = :idD and  IdInstitucion = :idI");
        $eliminar->bindParam(':idD', $_POST['idD']);
        $eliminar->bindParam(':idI', $_SESSION["Ins"]);
        $eliminar->execute();
        
        $IdSecciones = isset($_POST['Grupo']) ? $_POST['Grupo'] : array();    
           
        foreach($IdSecciones as $ids) 
        {
            $insert = $bdd->prepare("Insert into InscripcionSeccion 
                                     values (null, :idD, :idI, :ids, :year)");
                                     
            $insert->bindParam(':idD', $_POST['idD']);
            $insert->bindParam(':idI', $_SESSION["Ins"]); 
            $insert->bindParam(':ids', $ids);
            $insert->bindParam(':year', date("Y"));             
            $insert->execute();                                 
        }
        
        Redireccion("../Instituciones.php?in=Join&n=1");
    }
}


?>