<?php

/**
 * @author Manuel
 * @copyright 2013
 */
    function datonulo($string)
    {
        if($string == "")
            echo "-";
            
        else
            echo $string;
    }

    if($_SESSION["TipoUsuario"] == "Administrador")
    {
	    if(isset($_GET['n']))
        {
            if($_GET['n'] == 1)
                echo "<div id='Notificacion' name='Notificacion' style='color:green; float: right; width: 300px; height: 50px; padding-bottom:20px;'>Datos Ingresados Exitosamente</div>";                                     
        }
?>
        <div class="Raiz">
            <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
            <a href="/Instituciones.php" class='flecha' style='text-decoration:none'>Instituciones</a> ->
            <a href="/Instituciones.php?in=InsList" class='flecha' style='text-decoration:none'>Lista de Instituciones</a> ->
            <a href="/Instituciones.php?in=Profile" class='flecha' style='text-decoration:none'>Perfil de Instituci�n</a> ->
        </div>
        
        <div style='width: 90%; background-color: white; border-radius: 20px; border-color: skyblue; border-style: groove; margin-left: 5%; margin-top: 20px; padding:20px;'>       	
            <div style="padding: 10px; width: 32%; float: left; margin-left: 40px;">
                <h2>Perfil de Instituciones</h2>
                <form method="post" action="Instituciones/InstitucionesABM.php">
                    <input type='hidden' name='IdIns' value='<?php echo $_SESSION["Ins"];?>' />
                    <input class="boton" type="submit" name="Enviar" value="Actualizar Datos" style="width: 160px;" /><br />
                    <input class="boton" type="submit" name="Enviar" value="Contactos" style="width: 160px;"  /><br />
                    <input class="boton" type="submit" name="Enviar" value="Grupos" style="width: 160px;"  /><br />
                    <input class="boton" type="submit" name="Enviar" value="Datos FUSALMO" style="width: 160px;"  /><br />
                </form>
            </div> 
<?php
        $datosinstitucion = $bdd->prepare("SELECT * FROM  Institucion where IdInstitucion = :idIns ");
        $datosinstitucion->bindParam(':idIns', $_SESSION["Ins"]);
        $datosinstitucion->execute();
        
        if($datosinstitucion->rowCount() == 0)
        Redireccion("../Instituciones.php");
        
        $institucion = $datosinstitucion->fetch();
        	 
?>
            <div style="padding: 10px; width: 55%; float: right;">
                  <table>
                        <tr><td colspan='2' style='text-align: center;'><h2>Informaci�n de Instituci�n</h2></td></tr>
                        
                        <tr><td style='width: 50%; text-align: right; padding-right: 20px;'>Nombre de la Instituci�n: </td>
                            <td><?php echo $institucion[1];?></td></tr>
                            
                        <tr><td style='width: 50%; text-align: right; padding-right: 20px;'>Direcci�n:</td>
                            <td><?php datonulo($institucion[2]);?></td></tr>
                            
                        <tr><td style='width: 50%; text-align: right; padding-right: 20px;'>Tel�fono:</td>
                            <td><?php datonulo($institucion[3]);?></td></tr>
                            
                        <tr><td style='width: 50%; text-align: right; padding-right: 20px;'>Descripci�n de la instituci�n: </td>
                            <td><?php datonulo($institucion[4]);?></td></tr>
                            
                        <tr><td style='width: 50%; text-align: right; padding-right: 20px;'>Estado: </td>
                            <td><?php datonulo($institucion[5]);?></td></tr>                                                    
                  </table>
            </div>             
            
            <div class='clr'></div>            
        </div>
        
<?php  } ?>