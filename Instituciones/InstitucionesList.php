<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/Instituciones.php" class='flecha' style='text-decoration:none'>Instituciones</a> ->
    <a href="/Instituciones.php?in=InsList" class='flecha' style='text-decoration:none'>Lista de Instituciones</a> ->
</div>
<?php

/**
 * @author Manuel
 * @copyright 2013
 */
 
    $_SESSION["Ins"] = "";
  
    if(!isset($_GET['pag'])) 
	   $pag = 1;	
	
	else 
	   $pag = $_GET['pag'];
        
   	$hasta = 10;
	$desde = ($hasta * $pag) - $hasta;
    
    
    if(isset($_POST['busqueda']))
        $Busqueda = $_POST['busqueda'];

    else
    {
       if(isset($_GET['b'])) 
            $Busqueda = $_GET['b'];
       
       else
            $Busqueda = "";
    }
    
    $Query = "Select * from Institucion 
              where NombreInstitucion like '%$Busqueda%'";
    
    $resultado = $Query . " LIMIT $desde, $hasta";
    
    $lista = $bdd->prepare($resultado);
	$lista->execute();
    
    
    $paginado = $bdd->prepare($Query);
	$paginado->execute();

    ?>  
    <div style="background-color: white; border-radius: 10px; border-color: #78bbe6; border-style: groove; margin-bottom: 20px; width: 80%; margin-left: 10%;" >
    <form method="POST" action="?in=InsList">
        <table style="margin-bottom: 20px; float:left; margin-left: 10%;">
                <tr>
                    <td colspan="3"> <h2>Buscador r�pido de Instituciones</h2></td>
                </tr>
                <tr>
                    <td>Digite el Nombre de la Instituci�n</td>
                    <td><input type="text" name="busqueda" id="busqueda" value="<?php echo $Busqueda;?>" /></td>
                    <td><input type="button" value="?" title="Si desea buscar todos deje el campo de texto en blanco y pulse enter" /></td>
                </tr>
                <tr>
                <td colspan="4">Resultado de B�squeda: <?php echo $paginado->rowCount();?>                
                </td>
                </tr>
        </table>
    </form>
    <div class="clr"></div> 
    </div>
    <?php
    
    if(isset($_GET['n']))
    {
        if($_GET['n'] == 1)
            echo "<div id='Notificacion' name='Notificacion' style='color:red; float: right; width: 300px; height: 50px; padding-bottom:20px;'>Datos Ingresados Exitosamente</div>";                                     
    }

    echo "<div style='width: 90%; background-color: white; border-radius: 10px; border-color: skyblue; border-style: groove; margin-left: 5%; margin-top: 20px; padding:20px;'>";
	if ($lista->rowCount()>0)
    {
        echo "<table style='width: 90%; margin-left: 5%;'>
              <tr><th>Id</th><th>Nombre de Instituci�n</th><th colspan='2'>Acciones</th></tr>";
        while($datos=$lista->fetch())
        {
            echo "<tr>
                    <td>". $datos[0] ."</td><td>". $datos[1] ."</td>    
                    <form action='Instituciones/InstitucionesABM.php' method='post' name='Instituci�n' id='Instituci�n'>
                    <input type='hidden' name='IdIns' value='$datos[0]' />
                    <td><input id='Accesar' name='Enviar' class='boton' type='submit' value='Accesar' style='width: 100px;' /></td>                     
                    <td><input id='Eliminar' name='Enviar' class='boton' type='submit' value='Eliminar' style='width: 100px;' onClick='return confirmar();' /></td>
                    </form>
                  </tr>";
        }        
    }          
    
    $paginas = ceil($paginado->rowCount() / $hasta);
    
    $direccion = "Instituciones.php?in=InsList";
    echo     "<tr><td colspan='4' style='text-align:center;'>
                        <table style='width: 50%; margin-left:25%; text-align: center;'>
                        <tr><th colspan='3'>P�gina [$pag] de [$paginas]</th></tr>";
                  
    if ($pag > 1)
        echo "<tr><td style='width: 33%;'><a style='text-decoration:none' href=\"$direccion&pag=1&b=$Busqueda\"> <<- </a> | <a style='text-decoration:none' href=\"$direccion&pag=" . ($pag -1) . "&b=$Busqueda\"> <- </a> | </td>"; 
    else
        echo "<tr><td style='width: 33%;'></td>";
                
        echo "<td style='width: 34%;'><a style='text-decoration:none' href=\"Instituciones.php?in=InsList\">Principal</a></td>";

    if ($pag < $paginas)
        echo "<td style='width: 33%;'> |<a style='text-decoration:none' href=\"$direccion&pag=" . ($pag + 1) . "&b=$Busqueda\"> -> </a> | <a style='text-decoration:none' href=\"$direccion&pag=" . ($paginas) ."&b=$Busqueda\"> ->> </a> </td>";    
    else
        echo "<td style='width: 33%;'></td></tr>";               

    echo "</table>
          </table>";
    echo "</div>";  
?>