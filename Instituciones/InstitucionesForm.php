<?php

/**
 * @author Manuel
 * @copyright 2013
 */

if(isset($_GET['m']))
{
    if($_GET['m'] == "true")
    {
        $abm = "Modificar";
        $saludo = "Actualizar Instituci�n";
        
        $datosinstitucion = $bdd->prepare("SELECT * FROM  Institucion where IdInstitucion = :idIns ");
        $datosinstitucion->bindParam(':idIns', $_SESSION["Ins"]);
        $datosinstitucion->execute();
        
        if($datosinstitucion->rowCount() == 0)
        Redireccion("../Instituciones.php");
        
        $institucion = $datosinstitucion->fetch();
    }
}
else
{
    $abm = "Agregar";  
    $saludo = "Agregar nueva Instituci�n";
    $institucion[1] = "";  
    $institucion[2] = "";
    $institucion[3] = "";
    $institucion[4] = "";
}      

?>


<div class="Raiz">
    <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
    <a href="/Instituciones.php" class='flecha' style='text-decoration:none'>Instituciones</a> ->
    <?php
	       if($abm == "Modificar")
           echo "<a href='/Instituciones.php?in=InsList' class='flecha' style='text-decoration:none'>Lista de Instituciones</a> -> 
                 <a href='/Instituciones.php?in=Profile' class='flecha' style='text-decoration:none'>Perfil de Instituci�n</a> ->";
    ?>
</div>

<?php
echo "<div style='width: 90%; background-color: white; border-radius: 20px; border-color: skyblue; border-style: groove; margin-left: 5%; margin-top: 20px; padding:20px;'>
      <form action='Instituciones/InstitucionesABM.php' method='post'>
          <table style='width: 80%; margin-left: 10%;'>
                <tr><td colspan='2' style='text-align: center;'><h2>$saludo</h2></td></tr>
                
                <tr><td style='width: 45%; text-align: right; padding-right: 20px;'>Nombre de la Instituci�n: </td>
                <td style='color: red;'><input  required='true' title='Escribe el nombre de la Instituci�n' type='text' name='Nombre' value='$institucion[1]' />*</td></tr>
                    
                <tr><td style='width: 45%; text-align: right; padding-right: 20px;'>Direcci�n:</td>
                    <td><input type='text' name='Direccion' value='$institucion[2]' /></td></tr>
                    
                <tr><td style='width: 45%; text-align: right; padding-right: 20px;'>Tel�fono:</td>
                    <td><input type='text' name='Telefono' value='$institucion[3]' /></td></tr>
                    
                <tr><td style='width: 45%; text-align: right; padding-right: 20px;'>Descripci�n de la instituci�n: </td>
                    <td><textarea name='Descripcion' cols='25' rows='4'>$institucion[4]</textarea></td></tr>
                
                <tr><td colspan='2' style='text-align: center;'><input type='submit' class='boton' name='Enviar' id='Enviar' value='$abm' /></td></tr>
          </table>
      </form>
      </div> ";
?>