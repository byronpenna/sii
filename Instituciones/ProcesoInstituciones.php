<?php

if(isset($_GET['in']))
{
    /** Listado de Instituciones **/
    if($_GET['in'] == 'InsList')
        include("Instituciones/InstitucionesList.php");
    
    /** Formulario para nueva Institucion **/
    elseif($_GET['in'] == 'InsForm')
        include("Instituciones/InstitucionesForm.php");        
  
    /** Perfil de Institucion **/
    elseif($_GET['in'] == 'Profile')
        include("Instituciones/InstitucionPerfil.php"); 

    /** Contactos de Institucion **/
    elseif($_GET['in'] == 'Contact')
        include("Instituciones/ContactosList.php");  
    
    /** Formulario de Contactos **/    
    elseif($_GET['in'] == 'ContactForm')
        include("Instituciones/ContactosForm.php");   

    /** Grupos de Institucion **/
    elseif($_GET['in'] == 'Group')
        include("Instituciones/GruposList.php");

    /** Relacionar Dato Fusalmo con Instituci�n **/
    elseif($_GET['in'] == 'Join')
        include("Instituciones/JoinDatoFUSALMOInstitucion.php");
    
    /** Relacionar Dato Fusalmo con Secci�n **/
    elseif($_GET['in'] == 'JoinGroup')
        include("Instituciones/JoinDatoFUSALMOGrupo.php"); 

    /**  **/
    elseif($_GET['in'] == 'Fusalmo')
        include("Instituciones/DatosFusalmo.php");         
                                                                                
    else
        Redireccion("Instituciones.php");
        
        
}
else{
    
    $_SESSION["Ins"] = "";
     
    if($_SESSION["TipoUsuario"] == "Administrador")
    {
	    if(isset($_GET['n']))
        {
            if($_GET['n'] == 1)
                echo "<div id='Notificacion' name='Notificacion' style='color:green; float: right; width: 300px; height: 50px; padding-bottom:20px;'>Datos Ingresados Exitosamente</div>";                                     
        }
        ?>
        <div class="Raiz">
            <a href="/Main.php" class='flecha' style='text-decoration:none'>Principal  </a> ->
            <a href="/Instituciones.php" class='flecha' style='text-decoration:none'>Instituciones</a> ->
        </div>
        <?php
        
        echo "<div style='width: 90%; background-color: white; border-radius: 20px; border-color: skyblue; border-style: groove; margin-left: 5%; margin-top: 20px; padding:20px;'> ";
        
        include("MenuInstituciones.php");
        
        echo "    <div  style='float: right; width: 40%; margin-right: 60px' ><img src='../images/IconoInstituciones.png' /></div>
                  <div class='clr'></div>     
              </div>";    
    }
}
?>