<?php

/**
 * @author Manuel
 * @copyright 2013
 */

/** Esta Parte del Sistema ayudara a limpiar las variables de Sesi�n
 *  suponiendo que todos por alguna razon tienen que presionar cualquier 
 *  bot�n del Menu **/
 
    $IdUsuario     =  $_SESSION["IdUsuario"];
    $NombreUsuario =  $_SESSION["Usuario"];         
    $TipoUsuario   =  $_SESSION["TipoUsuario"];    	    	
    $Constrase�a   =  $_SESSION["Contrase�a"];              
    $Localidad     =  $_SESSION["Localidad"];

    session_unset();
    
    $_SESSION["IdUsuario"]     = $IdUsuario;            
	$_SESSION["Usuario"]       = $NombreUsuario;        	    	
    $_SESSION["TipoUsuario"]   = $TipoUsuario;
    $_SESSION["Contrase�a"]    = $Constrase�a;
    $_SESSION["Localidad"]     = $Localidad;  
    
    $_SESSION["autenticado"]   = true;
 

?>