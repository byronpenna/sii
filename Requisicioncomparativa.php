<?php
/**
 * @author Manuel y Lili
 * @copyright 2015 - 2016
 */

    require 'net.php'; 


    if (isset($_POST["ide"])) 
        $id = $_POST["ide"];
    else{
        if(isset($_GET["idr"])){
            $IdEmpleado = $bddr->prepare("SELECT IdEmpleado FROM Request_Requisiciones where Idrequisicion = " . $_GET["idr"]);
            $IdEmpleado->execute();        
            $DataE = $IdEmpleado->fetch();
            
            echo "
            <body onload='javascript:document.Request.submit();' />
                    <form method='post' action='RequisicionView.php' name='Request'>
                        <input type='hidden' name='ide' value='$DataE[0]'>
                        <input type='hidden' name='idp' value='".$_GET["idr"]."'>
                    </form>
                </body>
            ";    
        }
    }     

    $query = "SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo
            FROM CargosAsignacion AS ca
            INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
            INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
            WHERE ca.IdEmpleado = '$id'  and ca.FechaFin = '0000-00-00'";          

    $MisCargos = $bddr->prepare($query);                                                                        
    $MisCargos->execute();
    $DataC = $MisCargos->fetch();
?>

<html>
<head>
    <title>SIIF</title>

      <style type="text/css">
      .alertify-notifier .ajs-message.ajs-error, .alertify-notifier  .ajs-message.ajs-success {
          color: #fff;
          background: rgba(217, 92, 92, 0.95);
          text-shadow: -1px -1px 0 rgba(0, 0, 0, 0.5);
          text-align: center;
        }

        .alertify-notifier .ajs-message.ajs-warning {
          background: rgba(252, 248, 215, 0.95);
          border-color: #999;
        }
       
        button a {
          color: white;
          text-decoration: none; /* no underline */
        }

        button a:hover{
            color: white;
            text-decoration:none; 
        }

        .ancho {width: 200px;}

        .ancho_cantidad {width: 70px;} 
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <noscript><meta http-equiv="refresh" content="0;Error.php"/>Error!!! Javascript Desactivado</noscript>
    <?php include("lib.php");?>

    <script type="text/javascript" src="Empleados/Requisiciones/addrow.js"></script>
    <!-- <script type="text/javascript" src="Empleados/Requisiciones/addrow2.js"></script> -->
    <script type="text/javascript" src="Empleados/Requisiciones/addrow3.js"></script>
    <script type="text/javascript" src="Empleados/Requisiciones/js/validate.js"></script>
    <script type="text/javascript" src="Empleados/Requisiciones/js/init.js"></script>
    <script type="text/javascript" src="Empleados/Requisiciones/js/functions.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


    <link rel="stylesheet" href="Empleados/Requisiciones/DataTables/tablacomparativa.css"></script>
    <script type="text/javascript" src="Empleados/Requisiciones/DataTables/tablacomparativa.js"></script>
    <link rel="stylesheet" href="Empleados/Requisiciones/styles/main.css">

    <script type="text/javascript" src="Empleados/Requisiciones/DataTables/alertify/alertify.js"></script>
    <link rel="stylesheet" href="Empleados/Requisiciones/DataTables/alertify/css/alertify.css"></script>

    <script type="text/javascript" src="Empleados/Requisiciones/DataTables/tablacomparativa2.js"></script>
    <script type="text/javascript">


    </script>

</head>
<body>
<div class="main">
    <div class="fbg"></div>
    <div class="main_resize"><div class="header"><?php include("header.php");?></div></div>    
    <div class="fbg"></div>    
    <div class="main_resize">    
    <div class="content">            
        <div style="float: left; margin-top: 25px;">
        <?php if($_SESSION["autenticado"] == true)
                  include("menu/menu.php")
        ?>
        </div>
        <div style="float: right; margin-right: 20%; "></div>        
        <div class="clr"></div>     
        <div class="main" style="margin-top: 30px; margin-bottom: 30px;">
            <table style="width: 80%; background-color: white; border-radius: 10px; margin-left: 10%;">
                <tr><td><h2 style="color: blue; padding-left: 10px;">Solicitud de Requisici&oacuten</h2><hr color='skyblue' /></td></tr>
                <tr>
                    <td>
                        <table style="width: 100%;  padding-left: 20px;">
<?php
    $query2 = "SELECT e.Nombre1, e.Nombre2, e.Apellido1, e.Apellido2, c.IdCargos, c.Cargo, at.NombreAreaDeTrabajo, e.IdEmpleado
    FROM Empleado AS e
    INNER JOIN CargosAsignacion AS ac ON ac.IdEmpleado = e.IdEmpleado
    INNER JOIN Cargos AS c ON c.IdCargos = ac.IdCargo
    INNER JOIN AreasDeTrabajo as at on at.IdAreaDeTrabajo = c.IdArea_Fk
    where ac.IdAsignacion = $DataC[1]";

    $Accion = $bddr->prepare($query2);                                            
    $Accion->execute();
    $DatosEmple = $Accion->fetch(); 

    $MisRequi = $bddr->prepare("SELECT r.IdEmpleado, r.Idproyecto, r.Actividad, r.Fecha_Solicitud, r.Fecha_entrega, r.Comentario, r.Linea_Presupuestaria, r.SubLinea_Presupuestaria, r.Tipo_Requisicion, r.Estado, r.Comentario_Jefe, r.Comentario_finanzas , p.IdProyecto, p.Proyecto, p.Institucion_Financiadora, p.Descripcion, l.IdLinea, l.IdProyecto, l.LineaPresupuestaria, s.IdSubLineaPresupuestaria, s.IdLineaPresupuestaria, s.SubLinea, s.Monto,r.IdRequisicion, r.procesado
    FROM Request_Requisiciones as r 
    inner join Request_Proyecto as p on r.IdProyecto = p.IdProyecto 
    inner join Request_LineaPresupuestaria as l on r.Linea_Presupuestaria = l.IdLinea
    inner join Request_SubLineaPresupuestaria as s on r.SubLinea_Presupuestaria = s.IdSubLineaPresupuestaria
    WHERE r.Idrequisicion =" . $_POST["idp"]) ;

    $MisRequi->execute();
    $Requi = $MisRequi->fetch();
    $canti = $Requi[21];
    $materia = $Requi[23];
    $comentaro = $Requi[22];

   $idSubLineaQuery = $bddr->prepare("SELECT SubLinea_Presupuestaria FROM Request_Requisiciones WHERE Idrequisicion =  " . $_POST['idp'] . ";");
   $idSubLineaQuery->execute();
   $idSubLinea = $idSubLineaQuery->fetch(PDO::FETCH_ASSOC)['SubLinea_Presupuestaria'];

    $sqlMaterial= "SELECT * FROM Request_Material as k 
    inner join Request_Requisiciones as o on k.Idrequisicion = o.Idrequisicion
    right join Request_Seguimiento as w on k.IdMaterial = w.Id_Material
    where k.IdRequisicion = " . $_POST["idp"];

    $sqlAlimentacion= "SELECT * FROM Request_Alimentacion as a 
    inner join Request_Requisiciones as o on a.Idrequisicion = o.Idrequisicion
    inner join Request_SeguimientoAli as w on a.IdAlimentacion = w.Id_Alimentacion
    where a.IdRequisicion = " . $_POST["idp"]; 

    $mate= "SELECT IdSeguimiento, IdRequisicion, Costo, Cantidad_aprobada, Proveedor, Comentario, IdSubLineaPresupuestaria FROM Request_Seguimiento 
    where IdRequisicion = " . $_POST["idp"];

    $ali= "SELECT * FROM Request_Alimentacion as a 
    inner join Request_Requisiciones as o on a.Idrequisicion = o.Idrequisicion
    inner join Request_Seguimiento as s on a.Idrequisicion = s.IdRequisicion
    where a.IdRequisicion = " . $_POST["idp"]; 

    $sqlseguimiento = $bddr->prepare("SELECT w.IdSeguimiento, w.IdRequisicion, w.Costo, w.Cantidad_aprobada, w.Proveedor, w.Comentario, w.IdSubLineaPresupuestaria, r.IdEmpleado, r.Idproyecto, r.Actividad, r.Fecha_Solicitud, r.Fecha_entrega, r.Comentario, r.Linea_Presupuestaria, r.SubLinea_Presupuestaria, r.Tipo_Requisicion, r.Estado, r.Comentario_Jefe, r.Comentario_finanzas FROM Request_Seguimiento as w 
    inner join Request_Requisiciones as r on w.Idrequisicion = r.Idrequisicion
    where w.IdSeguimiento = $seg[0] "); 

    $sqlseguimiento->execute();
    $seg = $sqlseguimiento->fetch();                               
            
    $stmt = $bddr->prepare($sqlMaterial);
    $stmt->execute();
    $result = $stmt->fetchAll();

    $stmt2 = $bddr->prepare($sqlAlimentacion);
    $stmt2->execute();
    $result2 = $stmt2->fetchAll();

    $mate = $bddr->prepare($mate);
    $mate->execute();
    $materiales = $mate->fetchAll();

    $ali = $bddr->prepare($ali);
    $ali->execute();
    $alimentacion = $ali->fetchAll();


    $lines = $bddr->prepare("SELECT * FROM Request_Material as k 
    inner join Request_Requisiciones as o on k.Idrequisicion = o.Idrequisicion
    right join Request_Seguimiento as w on k.IdMaterial = w.Id_Material
    where k.IdRequisicion = " . $_POST["idp"]) ;
    $lines->execute();
    $lineo = $lines->fetch();
?>        
                        <input type="hidden" name="NombreEmpleado" value="<?php echo "$Datos[0] $Datos[1] $Datos[2] $Datos[3]"?>" />
                        <tr><td  style="width: 30%;">Empleado solicitante: </td><td><?php echo "$DatosEmple[0] $DatosEmple[1] $DatosEmple[2] $DatosEmple[3]"?></td></tr>
                        <tr><td>Cargo Asignado:</td><td><?php echo "$DatosEmple[5]"?></td></tr>
                        <tr><td>Area de trabajo:</td><td><?php echo "$DatosEmple[6]"?></td></tr>
                </table>
                <hr color='skyblue' />
                </td>               
            </tr>
            <tr>
                <td>
                    <table style="width: 60%; margin-left: 5%;">
                        <tr><td  style="padding-bottom: 10px;"><strong>Nombre de proyecto:</strong></td><td><?php echo  "$Requi[13]" ?></td></tr>
                        <tr><td  style="padding-bottom: 10px;"><strong>Linea Presupuestaria:</strong></td><td><?php echo  "$Requi[18]"?></td></tr>
                        <tr><td  style="padding-bottom: 10px;"><strong>Sub-Linea Presupuestaria:</strong></td><td><?php echo  "$Requi[21]"?></td></tr>

                        <tr><td  style="padding-bottom: 10px;"><strong>Actividad:</strong></td><td><?php echo  "$Requi[2]"?></td></tr>
                        <tr><td  style="padding-bottom: 10px;"><strong>Fecha de la solicitud:</strong></td><td><?php echo  "$Requi[3]"?></td></tr>
                        <?php if($Requi[8] != "Alimentacion") { ?>
                        <tr><td  style="padding-bottom: 10px;"><strong>Fecha de entrega:</strong></td><td><?php echo  "$Requi[4]"?></td></tr>
                        <?php } ?>
                        <tr><td  style="padding-bottom: 10px;"><strong>Tipo de requisici&oacuten:</strong></td><td><?php echo  "$Requi[8]"?></td></tr>
                        <tr><td  style="padding-bottom: 10px; width: 50%;"><strong>Comentarios:</strong></td><td><?php echo "$Requi[5]"?></td></tr>
                        <tr><td colspan="2" style="padding-bottom: 10px;"><strong>Detalles de la requisici&oacuten:</strong><br />  
                        <input type="hidden" name="idp" value="<?php echo $_POST["idp"];?>" />
                        <button action-button=true class="modal-trigger button buttonQuotation" modal='mdlQuotation'>Solicitud de Cotizaci&oacute;n</button>
                        <button class='modal-trigger button buttonProvider <?php echo $Requi[24] == 0 ? "visible" : ""?>' modal='mdlProvider'>Agregar Proveedor</button>

                       
                        <input type="hidden" name="estado2" id="estado2" value='<?php echo $Requi[9]; ?>'>
                        <button id="boton2" class="modal-trigger button buttonQuotation" >
                        <a class=""  type="button"> Mostrar Comparativas</a>
                        </button>

                        <button id="boton" class='modal-trigger button buttonQuotation'  
                        style='display: <?php echo $Requi[9]!="Entregado"? "inline" : "none"?>'>
                        <a class="enlace" id="btnModalc">Crear Tabla Comparativa</a>
                        </button>


                        <?php  if(intval($Requi[24]) == 1){ ?>
                        <form method="" action="" style="display: inline-block;">
                            <input type='hidden' name='idp' value='<?php echo $Requi[23];?>' />
                            <input type="button" class="modal-trigger button buttonQuotation" onclick="cancelarProcesar()" style="background: red;" name="Cancelar" value="Cancelar Proceso" />
                        </form>
                        <?php }?>
                        <?php if(intval($Requi[24]) != 1){ ?>
                        <form method="" action="" style="display: inline-block;">
                            <input type='hidden' name='idp' value='<?php echo $Requi[23];?>' />
                            <input type="button" class="modal-trigger button buttonQuotation" onclick="procesarRequi()" style="background: <?php echo $Requi[9]=="Entregado" ? 'green': 'gray'; ?>;"  name="Procesar" value="Procesar Requisici&oacute;n" <?php echo $Requi[9]=="Entregado" ? '': 'disabled'; ?>/>
                        </form>
                        <?php } ?>  


                        <div id="alertas" style="color: red;"></div>

                        

                        <input type="hidden" name="requiType" value="<?php echo $Requi[8] ?>">
                        <input type="hidden" name="idSubLinea" value="<?php echo $idSubLinea; ?>">
                        <input type="hidden" name="idProject" value="<?php echo $Requi[1] ?>">

                        <form method='post' name='formIdRequi' action='Requisicioncomparativa.php' target='_blank'>
                            <input type='hidden' name='idp' value='<?php echo $Requi[23];?>' />
                        </form>
                    </table >
                            <!--para probar AJAX -->

                            <input type="hidden" name="" id="tipoRequi" value="<?php echo $Requi[8] ;?>">
                            <input type="hidden" name="" id="idRequi" value='<?php echo $Requi[23];?>'>

                        <div class="modalDialog" id="mdlcontent">
                            <input type="hidden" name="idSubLinea" id="idSublinea" value="<?php echo $idSubLinea; ?>">
                            <div class="content">
                                <div id="cargaexterna" style="width: 100%;">
                                    
                                </div>
                            </div>
                            <div class="footer fixed-foo">
                                <button id="btnAtras" style="display:none;width: 100px;background:rgb(255, 77, 77);" class="modal-trigger btn" >
                                    Atras
                                </button>
                                <a class="modal-btn close" mdl-action="close" id='cls' style="display: none;" onclick="modalClose()">Cerrar</a>
                            </div>
                        </div>

                        
<?php 
        if($Requi[8] == "Materiales"){
        echo "
                            <table colspan='8' rules='rows' style='width: 60%; margin-left: 5%; text-align: center;'>
                                <form action='Empleados/Requisiciones/seguimientoABM.php' method='POST'>
                                    <input type='hidden' name='ider' value='$lineo[1]' />
                                    <input type='hidden' name='iddd' value='$lineo[1]' style='width: 72px;'  />

                                    <div class='table-material' style='overflow: auto; width: 100%;'>
                                    </div>
                                    <hr color='skyblue' /> 
                                </form>                        
                            </table>
        ";
        }else if($Requi[8] == "Alimentacion") {
        echo "
                            <table colspan='8' rules='rows' style='width: 60%; margin-left: 0%; text-align: center;'>
                                <form action='Empleados/Requisiciones/seguimientoAABM.php' method='POST'>
                                    <div class='table-material' style='overflow: auto; width: 100%;'></div>
                                    <hr color='skyblue' /> 
                                    <table>  
                                        <input type='hidden' name='idp' value='" . $_POST['idp'] . "'/>                  
                                    </table>
                                </form>
                            </table>
        ";
    } 
?>
                <?php //&& $Requi[9]!="Entregado" 
                if(intval($Requi[24]) != 1){ ?>
                <br>
                <center>
                <form method="" action="">
                    <input type='hidden' name='idp' value='<?php echo $Requi[23];?>' />
                    <input type="radio" name="Decision" value="Entregado" required='true' />Entregado 
                    <input type="radio" name="Decision" value="EnProceso" required='true' />En Proceso
                    <!--<input type="radio" name="Decision" value="Aprobado" required='true' />Aprobado-->
                    <input type="button" onclick="estadoRequi()" class="modal-trigger button buttonProvider visible" name="Estado" value="Guardar" />
                </form>
                </center>
                <?php } ?>  
                    <!-- <hr color='skyblue' />     -->
                </td>               
                </tr>
                <tr>
                    <td>
                    <input type="hidden" name="FlagProcesado" value="<?php echo $Requi[24]; ?>">
                        <?php if(intval($Requi[24]) == 1){ ?>
                        <!--<br>
                        <center>
                            <div class='buttons-pdf row'>
                                <button action-button=true class="button modal-trigger" modal="mdlOrder">Generar orden de compra</button>
                                <button action-button=true class="button modal-trigger" modal="mdlComparative">Generar Cuadro Comparativo</button>
                            </div>
                        </center>-->
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                <td>
                    
                    <?php
                           if(!isset($_POST['aux']) && ($Requi[9] == "Pendiente"))
                           {
                    ?>
                    <form action="Empleados/Requisiciones/requestABM.php" method="post">
                        <table style="width: 90%; margin-left: 5%;">                    
                            <input type="hidden" name="idp" value="<?php echo $_POST["idp"];?>" />
                            <tr>
                                <td colspan="2"><strong>Decisi&oacuten del Requerimiento</strong></td>
                            </tr>                        
                            <tr>
                                <td style="padding-bottom: 10px; width: 40%;"><input type="radio" name="Decision" value="Aceptar" required='true' />Aceptar</td>
                                <td style="padding-bottom: 10px; width: 50%;"><input type="radio" name="Decision" value="Denegar" required='true' />Denegar</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-bottom: 10px;">
                                    <strong>Comentario sobre la decisi&oacuten:</strong>    
                                    <br /><textarea name="Comentario" style="width: 70%; height: 50px;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-bottom: 10px;"><input type="submit" class="boton" name="Enviar" value="Guardar" /></td>
                            </tr>                                               
                        </table>
                    </form> 
                    <?php
                        }
                        else
                        {
                            if($DatosEmple[10] == "")
                                $ComentarioJefe = "<em style='color: blue'>No hay comentarios</em>";
                                
                            else
                                $ComentarioJefe = $Requi[10];
                    ?>      
                     <?php
                    
                            if($DatosEmple[11] == "")
                                $ComentarioJefe = "<em style='color: blue'>No hay comentarios</em>";
                                
                            else
                                $ComentarioJefe = $Requi[11];
                    ?>      
                        <table style="width: 90%; margin-left: 5%;">                    
                            <input type="hidden" name="idp" value="<?php echo $IdRequisicion;?>" />
                            <tr><td colspan="2"><strong>Decisi&oacuten de la Requisici&oacuten:</strong> <br /><?php echo  "$Requi[9]"?><br /><br /></td></tr>
                            <tr>
                                <td colspan="2" style="padding-bottom: 10px;">
                                    <strong>Comentario sobre la decisi&oacuten:</strong><br /><?php echo  "$Requi[10]"?>   
                                </td>
                            </tr>  
                             <tr>
                                <td colspan="2" style="padding-bottom: 10px;">
                                    <strong>Comentario sobre la decisi&oacuten: Lic Iglesias</strong><br /><?php echo  "$Requi[11]"?>   
                                </td>
                            </tr>                                              
                        </table>                        
                    <?php
                        }   
                    ?>                
                    <hr color='skyblue' />
                </td>             
            </tr>  
            <?php           
            if($_SESSION["TipoUsuario"] == "Administrador")
            {
            ?>   
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="width: 85%;">
                            <script>function cerrar() { setTimeout(window.close,1500); }</script>
                                <form action="Empleados/Requisiciones/requestABM.php" method="post">
                                    <input type="hidden" name="idp" value="<?php echo $IdRequisicion;?>" />
                                    <table style="width: 100%;">                                            
                                        <tr>
                                            <td><strong>Administraci&oacute;n Compras</strong></td>
                                            <td><input type="submit" class="botonE" name="Enviar" value="Eliminar"  /></td>
                                            <td><input type="button" class="botonG" name="Enviar" value="Cerrar" onclick="cerrar();" /></td>
                                        </tr>                                                
                                    </table>
                                </form>
                            </td>
                            <td>
                                <form method='post' action='Requisicioncomparativa.php' target='_blank'>
                                    <input type='hidden' name='idp' value='<?php echo $Requi[23];?>' />
                                    <input type='hidden' name='ide' value='<?php echo $Requi[0];?>' />
                                    <input type='hidden' name='' value='View' />
                                    <input type='submit' name='Enviar' style='width: 30px' class='botonG' value='Ver' />
                                </form>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php
               }
            ?>       
        </table>
        </div>
    </div>
    </div>
  <div class="fbg"></div>
</div>
    <div class="footer"><?php include("footer.php");?></div>

    <div class="modal" id="mdlOrder">
        <div class="content">
            
        </div>
        <div class="footer fixed-foo">
            <a class="modal-btn close" mdl-action="close">Cerrar</a>
        </div>
    </div>
                
    <div class="modal" id="mdlProvider">
        <div class="content">
            <form name="frmProvider" id="frmProvider" class='row'>
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtName">Nombre</label>                
                    <input type="text" name="txtName" id="txtName">
                </div>
                
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtAddress">Direcci&oacute;n</label>
                    <input type="text" name="txtAddress" id="txtAddress">
                </div>
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>                
                    <label for="txtCity">Ciudad</label>
                    <input type="text" name="txtCity" id="txtCity">
                </div>     
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtState">Estado</label>
                    <input type="text" name="txtState" id="txtState">
                </div>
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>                
                    <label for="txtPhone">Tel&eacute;fono</label>
                    <input type="text" name="txtPhone" id="txtPhone">
                </div>
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>                
                    <label for="txtCP">C&oacute;digo Postal</label>
                    <input type="text" name="txtCP" id="txtCP">
                </div>
                <div class='content-field radio col l4 m4 s4 offset-m2 offset-l2 offset-s2'>
                    <label>Proveedor Material</label>            
                    <input type="radio" name="rdbType" value="M">              
                </div>
                <div class="content-field radio col l3 m4 s4 offset-m1">
                    <label>Proveedor Alimentos</label>
                    <input type="radio" name="rdbType" value="A">
                </div>
                <div class="content-field c-button col l2 m4 s6 offset-l5 offset-m4 offset-s3">
                    <button class="button" id="btnAddProvider">Registrar</button>
                </div>
            </form>
        </div>
        <div class="footer fixed-foo">
            <a class="modal-btn close" mdl-action="close" id='cerrarProve'>Cerrar</a>
        </div>
    </div>

    <div class="modal" id="mdlQuotation">
        <div class="content">
            <form name="frmQuotation" id="frmQuotation" class="row" action="Empleados/Requisiciones/php/Print.php" method="GET" target="_blank">
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtinicio">Comentario al inicio</label>
                    <input type="text" name="inicio" id="txtinicio">
                </div>

                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtPlaceDate">Lugar y Fecha</label>                
                    <input type="text" name="placeDate" id="txtPlaceDate">
                </div>
                
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtPlace">Lugar de Entrega</label>
                    <input type="text" name="place" id="txtPlace">
                </div>
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>                
                    <label for="txtValidity">Vigencia de la Cotizaci&oacute;n</label>
                    <input type="text" name="validity" id="txtValidity">
                </div>     
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtWarranty">Tiempo de Garant&iacute;a de los Bienes</label>
                    <input type="text" name="warranty" id="txtWarranty">
                </div>
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>                
                    <label for="txtResponsable">Encargado de Compras</label>
                    <input type="text" name="nameResponsable" id="txtResponsable">
                </div>
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>                
                    <label for="txtNote">Notas</label>
                    <textarea name="note" id="txtNote" cols="80" rows="20"></textarea>
                </div>
                <div class="content-field c-button col l2 m4 s6 offset-l5 offset-m4 offset-s3">
                    <button class="button" id="btnSenQuotation">Registrar</button>
                </div>
                <input type="hidden" name="Quotation" value="1">
                <input type="hidden" name="idRequi" value="<?php echo $Requi[23]; ?>">
                <input type="hidden" name="type" value="<?php echo $Requi[8]; ?>">
            </form>
        </div>
        <div class="footer fixed-foo">
            <a class="modal-btn close" mdl-action="close">Cerrar</a>
        </div>
    </div>

    <div class="modal" id="mdlComparative2">
        <div class="content" id="contenido">
            <form method='POST' action='Empleados/Requisiciones/php/pdfComparativa.php' target="_blank">
                    
                    <input type='hidden' id='pdfcompa' name='pdfComparativa'></input>
                    <input type='hidden' id='nombrecompa' name='nombre' ></input>
                    <input type='hidden' id='idreque' name='idreq' ></input>
                    <input type='hidden' id='tiporequisi'name='tipo' ></input>
                   
                <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                    <h4 style="text-align: center">T&eacute;rminos y condiciones:</h4>
                    <input type="checkbox" name="payment2[]" id="payment_1" value="Crédito de 30 días">
                    <label for="payment_1">Cr&eacute;dito de 30 d&iacute;as</label><br>
                    <input type="checkbox" name="payment2[]" id="payment_2" value="Crédito de 15 días">
                    <label for="payment_2">Cr&eacute;dito de 15 d&iacute;as</label><br>
                    <input type="checkbox" name="payment2[]" id="payment_3" value="Crédito de 8 días">
                    <label for="payment_3">Cr&eacute;dito de 8 d&iacute;as</label><br>
                    <input type="checkbox" name="payment2[]" id="payment_4" value="Pago con Cheque">
                    <label for="payment_4">Pago con Cheque</label><br>
                    <input type="checkbox" name="payment2[]" id="payment_5" value="Pago a Cuenta">
                    <label for="payment_5">Abono a Cuenta</label><br>
                </div>
                
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtMaker" id='autor'>Elaborado por: </label>
                    <input type="text" name="usuario" id="txtMaker">
                </div>

                <div class="content-field">
                    <center><button class="button" id="btnSenQuotation"  onclick="modalCloseComparativo()">Generar Cuadro Comparativo</button></center>
                </div>
            </form>
        </div>
        <div class="footer fixed-foo">
            <a class="modal-btn " onclick="modalCloseComparativo()">Cerrar</a>
        </div>
    </div>

    <div class="modal" id="mdlComparative">
        <div class="content">
            <form name="frmComparative" id="frmComparative" class="row" action="Empleados/Requisiciones/php/Print.php" method="POST" target="_blank">
                <input type="hidden" name="idRequision" value="<?php echo $Requi[23]; ?>"> <!-- input[name='idp'] = $Requi[23] -->
                <input type="hidden" name="comparation" value="true">
                <input type="hidden" name="type" value="<?php echo $Requi[8]; ?>">

                <div class="content-field content-field col l8 m10 s12 offset-l2 offset-m1">
                    <h4 style="text-align: center">T&eacute;rminos y condiciones:</h4>
                    <input type="checkbox" name="payment[]" id="payment_1" value="Crédito de 30 días">
                    <label for="payment_1">Cr&eacute;dito de 30 d&iacute;as</label><br>
                    <input type="checkbox" name="payment[]" id="payment_2" value="Crédito de 15 días">
                    <label for="payment_2">Cr&eacute;dito de 15 d&iacute;as</label><br>
                    <input type="checkbox" name="payment[]" id="payment_3" value="Crédito de 8 días">
                    <label for="payment_3">Cr&eacute;dito de 8 d&iacute;as</label><br>
                    <input type="checkbox" name="payment[]" id="payment_4" value="Pago con Cheque">
                    <label for="payment_4">Pago con Cheque</label><br>
                    <input type="checkbox" name="payment[]" id="payment_5" value="Pago a Cuenta">
                    <label for="payment_5">Abono a Cuenta</label><br>
                </div>
                
                <div class='content-field content-field col l8 m10 s12 offset-l2 offset-m1'>
                    <label for="txtMaker">Elaborado por: </label>
                    <input type="text" name="maker" id="txtMaker">
                </div>

                <div class="content-field">
                    <center><button class="button" id="btnSenQuotation">Generar Cuadro Comparativo</button></center>
                </div>
            </form>
        </div>
        <div class="footer fixed-foo">
            <a class="modal-btn close" mdl-action="close">Cerrar</a>
        </div>
    </div>
</body>
</html>