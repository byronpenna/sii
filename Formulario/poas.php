<?php
//conexion a base de datos
  $db_host="localhost";
  $db_name="siif_EmpleadoFusalmo";
  $db_user="siif_padre";
  $db_pass="Fusalmo2016";

   
   
       try {
       $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
       $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    //Captura de error si no se puede conectar
    catch(PDOException $e)
    {
         echo "<script>alert('Error conectando a la base de datos');</script>";
        header('Location: index.php.php?r=0');
        
    }
   

?>

<?php

$query = "SELECT ca.IdEmpleado, ca.IdAsignacion, c.IdCargos, c.Cargo, a.IdAreaDeTrabajo
          FROM CargosAsignacion AS ca
          INNER JOIN Cargos AS c ON c.IdCargos = ca.IdCargo
          INNER JOIN AreasDeTrabajo AS a ON a.IdAreaDeTrabajo = c.IdArea_Fk
          WHERE ca.IdEmpleado = '".$_SESSION["IdUsuario"]."'  and ca.FechaFin = '0000-00-00'";          
          
$MisCargos = $bddr->prepare($query);                                                                        
$MisCargos->execute();
$DataC = $MisCargos->fetch();


?>

<!DOCTYPE html>
<html lang="es">

<head>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Información Institucional FUSALMO</title>
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="Empleados/POA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">


</head>

<body class="fixed-nav sticky-footer bg-light" id="page-top">
 <style type="text/css">

   

    #botoncete {
    background-color: #6aadd8;
}

  </style>
  <div class="content-wrapper">
 
    
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Lista General de Procesos</a>
        </li>
        <li class="breadcrumb-item active">Procesos</li>
      </ol>
      <!-- Example DataTables Card-->

       <?php
echo '<a class="btn btn-primary btn-primary" id="botoncete" href="index.php">Volver</a>';
?>
 <?php
echo '<a class="btn btn-primary btn-primary" id="botoncete" href="?l=AgregarPOA">Agregar POA</a>';
echo "<br>";
?>
<br>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Procesos Creados</div>
        <div class="card-body">
          <div class="table-responsive">

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th style="display: none;">IdPOA</th>
                  <th>Nombre del POA</th>
                  <th>Fecha Inicio</th>
                  <th>Fecha Final</th>                  
                  <th>Avance</th>
                  <th>Detalles</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th style="display: none;">IdPOA</th>
                  <th>Nombre del POA</th>
                  <th>Fecha Inicio</th>
                  <th>Fecha Final</th>                  
                  <th>Avance</th>
                  <th>Detalles</th>
                </tr>
              </tfoot>
              <tbody>
              
                
                <?php
                $st = $conn->prepare("SELECT * FROM poa_poa");
                $st->execute();
                $rows = $st->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($rows as $row) {
                        
                        echo "<tr>";
                        echo '<td style="display: none;">'.$row['Id'].'</td>';
                        echo '<td>'.utf8_decode($row['Nombre']).'</td>';
                        echo '<td>'.$row['fecha_inicio'].'</td>';
                        echo '<td>'.$row['fecha_final'].'</td>';
                          if($row['Estado'] == 1){
                        echo '<td style="color: green;">100% Completado</td>';
                       }else{
                          if($row['Estado'] == NULL ){
                             echo '<td>Sin Avances</td>';

                          }else{

                          $ab = $conn->prepare("SELECT COUNT(*) from poa_procesos Where IdPoa = :id");
                           $ab->bindParam(':id',$row['Id'], PDO::PARAM_STR);
                           $ab->execute();
                           $registros2 = $ab->fetchColumn();
                          

                           $ac = $conn->prepare("SELECT * from poa_procesos Where IdPoa = :id");
                           $ac->bindParam(':id',$row['Id'], PDO::PARAM_STR);
                           $ac->execute();
                           $columnas = $ac->fetchAll(PDO::FETCH_ASSOC);

                                  foreach ($columnas as $listos) {

                                       if ($listos["Estado"] == 0 ){
                                        $nocompletado2 ++;
                                         
                                  }else{
                                      $completado2 ++;

                                  }
                                }




          
                        if ($registros2 != 0){
                              $porcentaje2 = 100/$registros2;
                              $avanceTotal2 = round($porcentaje2 * $completado2,0,PHP_ROUND_HALF_UP);
                              if($avanceTotal2 <= 33){
                                 echo '<td style="color: #FF0000;">'.$avanceTotal2.'% Completados</td>';
                               }elseif ($avanceTotal2 >= 34 && $avanceTotal2 <= 66) {
                                 echo '<td style="color: #CFF70A;">'.$avanceTotal2.'% Completados</td>';
                               }elseif ($avanceTotal2 >= 67) {
                                echo '<td style="color: green;">'.$avanceTotal2.'% Completados</td>';
                               }
                        }else {
                          $avanceTotal2 = 0;
                          echo '<td style="color: #FF0000;">0% Completado</td>';
                        }
                        $completado2 = 0;
                        $nocompletado2 = 0;
                       }


                        
                       }
                        echo '<td><a href="?l=tables&id='.$row['Id'].'">Ver Procesos</a></td>';
              


                          
                                 echo '</tr>';

                      
                          }

                       
                      

                ?>



                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
 
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="Empleados/POA/vendor/jquery/jquery.min.js"></script>
    <script src="Empleados/POA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="Empleados/POA/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="Empleados/POA/vendor/datatables/jquery.dataTables.js"></script>
    <script src="Empleados/POA/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="Empleados/POA/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="Empleados/POA/js/sb-admin-datatables.min.js"></script>
  </div>
  </div>

</body>

</html>
