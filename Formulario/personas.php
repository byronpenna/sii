<?php
session_start();

if ($_SESSION["autenticado"] != "SI") {
//si no existe, va a la página de autenticacion
echo '<script>
                alert("No has iniciado sesion!");
                window.location= "index.html"
    </script>';

}

  $db_host="localhost";
  $db_name="siif_form";
  $db_user="siif_juanito";
  $db_pass="Juanito0708+";

   
   
       try {
       $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
       $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    //Captura de error si no se puede conectar
    catch(PDOException $e)
    {
         echo "<script>alert('Error conectando a la base de datos');</script>";
        header('Location: index.php.php?r=0');
        
    }

?>

<?php



?>

<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Personas Registradas</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">


</head>

<body class="fixed-nav sticky-footer bg-light" id="page-top">
 <style type="text/css">

   

    #botoncete {
    background-color: #6aadd8;
}

  </style>
  <div class="content-wrapper" style="margin-left: 0px;">
 
    
 
<br>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Personas Registradas</div>
        <div class="card-body">
          <div class="table-responsive">

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Edad</th>
                  <th>Sexo</th>                  
                  <th>Profesión</th>
                  <th>País</th>
                  <th>Sede</th>
                  <th>E-mail</th>
                  <th>Telefono Fijo</th>
                  <th>Telefono Celular</th>
                  <th>Fecha Llegada</th>
                  <th>Hora Llegada</th>
                  <th>Empresa y Vuelo (Llegada)</th>
                  <th>Fecha Partida</th>
                  <th>Hora Partida</th>
                  <th>Empresa y Vuelo (Partida)</th>
                  <th>Necesidad Especil</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Nombre</th>
                  <th>Edad</th>
                  <th>Sexo</th>                  
                  <th>Profesión</th>
                  <th>País</th>
                  <th>Sede</th>
                  <th>E-mail</th>
                  <th>Telefono Fijo</th>
                  <th>Telefono Celular</th>
                  <th>Fecha Llegada</th>
                  <th>Hora Llegada</th>
                  <th>Empresa y Vuelo (Llegada)</th>
                  <th>Fecha Partida</th>
                  <th>Hora Partida</th>
                  <th>Empresa y Vuelo (Partida)</th>
                  <th>Necesidad Especil</th>
                </tr>
              </tfoot>
              <tbody>
              
                
                <?php
                $st = $conn->prepare("SELECT * FROM form_datos");
                $st->execute();
                $rows = $st->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($rows as $row) {
                        
                        echo "<tr>";
                        echo '<td>'.$row['nombre'].'</td>';
                        echo '<td>'.$row['edad'].'</td>';
                        echo '<td>'.$row['sexo'].'</td>';
                        echo '<td>'.$row['profesion'].'</td>';
                        echo '<td>'.$row['pais'].'</td>';
                        echo '<td>'.$row['sede'].'</td>';
                        echo '<td>'.$row['mail'].'</td>';
                        echo '<td>'.$row['tel_fijo'].'</td>';
                        echo '<td>'.$row['tel_celular'].'</td>';
                        echo '<td>'.$row['f_llegada'].'</td>';
                        echo '<td>'.$row['h_llegada'].'</td>';
                        echo '<td>'.$row['empresa_vuelo1'].'</td>';
                        echo '<td>'.$row['f_partida'].'</td>';
                        echo '<td>'.$row['h_partida'].'</td>';
                        echo '<td>'.$row['empresa_vuelo1'].'</td>';
                        echo '<td>'.$row['necesidad'].'</td>';
                        echo '</tr>';
                  }
                       
                      

                ?>



                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
 
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
  </div>
  </div>

</body>

</html>
