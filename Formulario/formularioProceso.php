<?php

$db_host="localhost";
  $db_name="siif_EmpleadoFusalmo";
  $db_user="siif_padre";
  $db_pass="Fusalmo2016";

   
   
       try {
       $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
       $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    //Captura de error si no se puede conectar
    catch(PDOException $e)
    {
         echo "<script>alert('Error conectando a la base de datos');</script>";
        header('Location: index.php.php?r=0');
        
    }


  $idPoa = $_GET['id'];


?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Fusalmo</title>
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">

      <script src="Empleados/POA/vendor/jquery/jquery.min.js"></script>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
$('#selectEmpleados').select2();
}); 


</script>
</head>

<body class="fixed-nav sticky-footer bg-light" id="page-top">
  <!-- Navigation-->

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Proceso</a>
        </li>
        <li class="breadcrumb-item active">Registro de Proceso</li>
      </ol>
      
        
  
<div class="container">

   <?php
echo '<a class="btn btn-primary btn-primary" href="javascript:history.back(-1);">Volver</a>';
echo "<br>";
?>
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Registro de Proceso</div>
      <div class="card-body">
        <form enctype="multipart/form-data" action="?l=registroProceso" method="post">
        <?php
           echo '<input style="display: none;" name="idPoa" type="number" value="'.$idPoa.'"/>';
        ?>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">Nombre de Proceso</label>
                <input class="form-control" id="nombreProceso" name="nombreProceso" type="text" aria-describedby="nameHelp" placeholder="Ingrese nombre de Proceso">
              </div>
              <div class="col-md-6">
                <label for="exampleInputLastName" style="margin-right: 3%;">Indicador: </label>
                
                <input id="C9"  type="checkbox" name="indicadorCuantitativo" onchange="javascript:checkHidden91()" style="margin-right: 1%;" value="10" checked ><label style="margin-right: 3%;">Cuantitativo</label>
                
                <input id="C8" type="checkbox" name="indicadorCualitativo" onchange="javascript:checkHidden81()" style="margin-right: 1%;" value="2"><label style="margin-right: 3%;">Cualitativo</label>
                
                <input id="hidden9" name="inputCuantitativo" class="form-control" id="exampleInputLastName" type="number" min="1" aria-describedby="nameHelp" placeholder="Indicador Cuantitativo" >

                <input id="hidden8" name="inputCualitativo" class="form-control" id="exampleInputLastName" type="text" aria-describedby="nameHelp" placeholder="Indicador Cualitativo" style="display: none;">

              </div>
            </div>
          </div>

           <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPassword1">Fecha Inicio</label>
                <input class="form-control" type="month" name="fechaInicio">
              </div>
              <div class="col-md-6">
                <label for="exampleConfirmPassword">Fecha Final</label>
                <input class="form-control" type="month" name="fechaFinal">
              </div>
            </div>
          </div>


             <!--  <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <label for="exampleConfirmPassword">Resultado</label>
                <input class="form-control" type="text">
              </div>
              <div class="col-md-12">
                <label for="exampleConfirmPassword">Producto</label>
                <input class="form-control" type="text">
              </div>
             
            </div>
          </div>
 -->

          

        
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">


              <label for="exampleInputEmail1">Presupuesto</label>
              <div class="input-group"> 
        <span class="input-group-addon">$</span>
        <input type="number" value="1000" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="c2" name="inputPresupuesto" />
    </div>
              </div>
              <div class="col-md-6">
                <label for="exampleConfirmPassword">Documentos</label>
                <input type="file" class="form-control" id="archivo[]" name="archivo[]" multiple="">
              </div>
            </div>
          </div> <!-- aqui -->



           <div class="form-group">
           <div class="form-row">
              <div class="col-md-12">
                       <label for="exampleInputPassword1">Asignacion de Empleados</label>
                <select class="form-control" id="selectEmpleados" name="empleados[]" multiple="multiple">
               
<?php 
                $stm = $conn->prepare("SELECT  IdEmpleado,Nombre1,Apellido1 FROM poa_empleado");
                $stm->execute();
                $rows = $stm->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($rows as $row) {
                      ?>
                       <option value="<?php echo $row['IdEmpleado']; ?>"><?php echo utf8_decode($row['Nombre1']). ' ' . utf8_decode($row['Apellido1']); ?></option>  
                       <?php
                        }

                      ?>

             </select> 
             </div>
             </div>
          </div>


          <!-- hasta aqui -->
          <input type="submit" class="btn btn-primary btn-block" />
                  </form>
       
      </div>
    </div>
  </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
  
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="Empleados/POA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="Empleados/POA/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="Empleados/POA/js/sb-admin.min.js"></script>
    <script src="Empleados/POA/js/pestanas.js"></script>
  </div>
</body>

</html>
