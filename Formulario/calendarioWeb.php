<?php


  $db_host="localhost";
  $db_name="siif_EmpleadoFusalmo";
  $db_user="siif_padre";
  $db_pass="Fusalmo2016";

   
$idProceso = $_GET['id'];
 

?>
<!DOCTYPE html>


<head>
 
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Sistema de Plan Operativo Anual</title>
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">

<link rel='stylesheet' href='Empleados/POA/css/fullcalendar/fullcalendar.css' />
<script src='Empleados/POA/lib/jquery.min.js'></script>
<script src='Empleados/POA/lib/moment.min.js'></script>
<script src='Empleados/POA/js/fullcalendar/fullcalendar.js'></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
</head>

<body>
          <div class="container">
                <div class="row">
                       <div class="col"></div>
                       <div class="col-12">  <div id="CalendarioWeb"></div></div>
                        <div class="col"></div>
                </div>
            
          </div>
<?php
      try {
       $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
       $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    //Captura de error si no se puede conectar
    catch(PDOException $e)
    {
         echo "<script>alert('Error conectando a la base de datos');</script>";
        header('Location: index.php.php?r=0');
        
    }

  

$st = $conn->prepare("SELECT * FROM poa_actividades Where IdProceso = :id");
                 $st->bindParam(':id',$idProceso, PDO::PARAM_STR);

                $st->execute();
                $rows = $st->fetchAll(PDO::FETCH_ASSOC);


?>
  <script type="text/javascript">
    $(document).ready(function(){

        $('#CalendarioWeb').fullCalendar({
          header:{
              left:'today,prev,next',
              center:'title',
              right:'month, basicWeek, basicDay, agendaWeek, agendaDay,listWeek'
          },
          events:[
            
              <?php
              
                  

                        foreach ($rows as $row) {
                           echo '{';
                          echo 'title: "'.utf8_decode($row['nombre_actividad']).'",';
                          echo 'descripcion: "'.$row['fecha_inicio'].'",';
                           echo 'descripcion2: "'.$row['fecha_final'].'",';
                          echo 'start: "'.$row['fecha_inicio'].'",';
                          echo 'end: "'.$row['fecha_final'].'",';
                            echo '},';
                        }

              ?>

          ],
              eventClick:function(calEvent,jsEvent,view){
                $('#tituloEvento').html(calEvent.title);
                $('#finicio').html(calEvent.descripcion);
                $('#ffinal').html(calEvent.descripcion2);
              
                $("#exampleModal").modal();
              }
        });

    });
  </script>




  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="tituloEvento"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="descripcionEvento">
        <label><strong>Fecha Inicio: </strong> </label><label id="finicio"> </label><br>
        <label><strong>Fecha Final: </strong>  </label><label id="ffinal"> </label>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>
</body>




</html>
