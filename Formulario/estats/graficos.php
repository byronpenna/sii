<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script src="https://code.highcharts.com/modules/exporting.src.js"></script>
<script type="text/javascript">
Highcharts.chart('pastel', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Avance'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        colorByPoint: true,
        data: [{
            name: 'completado',
            y: <?php echo $avanceTotal ?>
        }, {
            name: 'incompleto',
            y: <?php echo $avance_restante?>
        }]
    }]
});
</script>

<script>
Highcharts.chart('barra', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Avance'
    },
    xAxis: {
        categories: ['completado', 'incompleto'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'progreso',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' millions'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        colorByPoint: true,
        data: [{
            name: 'completado',
            y: <?php echo $avanceTotal?>
        }, {
            name: 'incompleto',
            y: <?php echo $avance_restante?>
        }]
    }]
});
</script>
