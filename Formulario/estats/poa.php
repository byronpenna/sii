<?php
//conexion a base de datos
  $db_host="localhost";
  $db_name="siif_EmpleadoFusalmo";
  $db_user="siif_padre";
  $db_pass="Fusalmo2016";
       try {
       $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
       $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    //Captura de error si no se puede conectar
    catch(PDOException $e)
    {
         echo "<script>alert('Error conectando a la base de datos');</script>";
        header('Location: index.php.php?r=0');
        
    }


if(!isset($_POST["idpoa"]) & !isset($_POST["grafico"]) & !isset($_POST["vercom"]) ){
     $idPoa = $_GET['id'];
     $tipografico="singrafico";
     $vercompletados=3;
}else{
  $idPoa = $_POST["idpoa"];
  $tipografico= $_POST["grafico"];
  $vercompletados=$_POST["vercom"];
}
?>
 <?php

 $completado = 0;
 $nocompletado = 0;
 $completado2 = 0;
 $nocompletado2 = 0;
 $completado3 = 0;
 $nocompletado3 = 0;

                 $cn = $conn->prepare("SELECT COUNT(*) from poa_procesos Where IdPoa = :id");
                 $cn->bindParam(':id',$idPoa, PDO::PARAM_STR);
                 $cn->execute();
                 $registros = $cn->fetchColumn();
                 
                


                 $br = $conn->prepare("SELECT * from poa_procesos Where IdPoa = :id");
                 $br->bindParam(':id',$idPoa, PDO::PARAM_STR);
                 $br->execute();
                 $rows = $br->fetchAll(PDO::FETCH_ASSOC);

                         if (!empty($rows)) {
                    foreach ($rows as $row) {

                             if ($row["Estado"] == 0 ){
                              $nocompletado +=1;
                             
                        }else{
                            $completado +=1;
                               
                            
                        }
                      
                      }
                  }else{
                    $nocompletado = 1;
                  }


                      if ($nocompletado == 0) {
                       $ac = $conn->prepare("UPDATE poa_poa SET Estado= 1 Where Id = :id ");
                       $ac->bindParam(':id',$idPoa, PDO::PARAM_STR);
                       $ac->execute();

                      }else{
                          
                        $ac = $conn->prepare("UPDATE poa_poa SET Estado= 0 Where Id = :id ");
                        $ac->bindParam(':id',$idPoa, PDO::PARAM_STR);
                        $ac->execute();
                      }

                     
                    
                        if ($registros != 0){
                              $porcentaje = 100/$registros;
                              $avanceTotal = round($porcentaje * $completado,0,PHP_ROUND_HALF_UP);
                        }else {
                          $avanceTotal = 0;
                        }
          $avance_restante=100-$avanceTotal;
?>

<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
       <tr><td colspan="7"><h2 style="color: #197198;">Plan Operativo Anual - Estadisticas - poa</h2></td></tr>
        <tr><td colspan="7"><hr color='skyblue' /></td></tr>
        <tr style='display:  inline-block;'>
            <div class="content-wrapper">
    <div class="container-fluid">
      <div id="cabezera">
        <?php
          
          $st = $conn->prepare("SELECT GROUP_CONCAT(AreasDeTrabajo.NombreAreaDeTrabajo SEPARATOR '/') AS NombreAreaDeTrabajo,
                                 poa_poa.Id,
                                 poa_poa.Nombre,
                                 poa_poa.fecha_inicio,
                                 poa_poa.fecha_final                                 
                                 FROM (siif_EmpleadoFusalmo.poa_DetallePOA poa_DetallePOA 
                                 INNER JOIN siif_EmpleadoFusalmo.poa_poa poa_poa 
                                 ON (poa_DetallePOA.IdPoa = poa_poa.Id)) 
                                 INNER JOIN siif_EmpleadoFusalmo.AreasDeTrabajo AreasDeTrabajo 
                                 ON (poa_DetallePOA.IdArea = AreasDeTrabajo.IdAreaDeTrabajo) 
                                 WHERE poa_poa.Id = :id");
          $st->bindParam(':id',$idPoa, PDO::PARAM_STR);
          $st->execute();

          ?>

          <ul>
          <?php

          $result = $st -> fetch();
              
              echo "<li><b>Nombre de POA: </b>".$result['Nombre']."</li>";
              echo "<li><b>Areas: </b>".$result['NombreAreaDeTrabajo']."</li>";
              echo "<li><b>Fecha Inicio: </b>".$result['fecha_inicio']."</li>";
              echo "<li><b>Fecha de Finzalizacion: </b>".$result['fecha_final']."</li>";
        ?>
       </ul>
      </div>
      

<form action="?l=stats/proceso" method="post">
<fieldset>
        <legend>elija tipo de grafico</legend>
        <label>
            <input type="radio" name="grafico" value="pastel" checked="checked">pastel
        </label>
        <label>
            <input type="radio" name="grafico" value="barra">barra
        </label>
        <label>
            <input type="radio" name="grafico" value="otros">otros
        </label>
        <input type="hidden" name="vercom" value="<?php echo $vercompletados ?>">
        <input type="hidden" name="idpoa" value="<?php echo $idPoa ?>">
        <input type="submit" name="enviar">
</fieldset>
</form>
<br>
<?php 
if ( $tipografico=="singrafico" || $tipografico=="pastel" ) {
  echo '<div id="pastel" style="min-width: 310px; height: 400px; margin: 0 auto"></div>';
}else if($tipografico=="barra"){
  echo '<div id="barra" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>';
}else{
  echo 'no';

}

include 'graficos.php';

?>
<br>
<form action="?l=stats/proceso" method="post">
<fieldset>
        <legend>datos que ver </legend>
        <label>
            <input type="radio" name="vercom" value="1" >completados
        </label>
        <label>
            <input type="radio" name="vercom" value="2">no completados
        </label>
        <label>
            <input type="radio" name="vercom" value="3" checked="checked">ambos
        </label>
        <input type="hidden" name="idpoa" value="<?php echo $idPoa ?>">
        <input type="hidden" name="grafico" value="<?php echo $tipografico ?>">
        <input type="submit" name="enviar">
</fieldset>
</form>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Procesos Creados</div>
        <div class="card-body">
          <div class="table-responsive">

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" border="1">
              <thead>
                <tr>
                  <th style="display: none;">IdProceso</th>
                  <th>Nombre del Proceso</th>
                  <th>Responsable</th>
                  <th>Fecha Inicio</th>
                  <th>Fecha Final</th>
                  
                  <th>Indicador</th>
                  <th>Avance</th>
                  <th>Actividades</th>
                </tr>
              </thead>
              <tbody>
              
                
                <?php
                $st = $conn->prepare("SELECT GROUP_CONCAT(poa_cargos.Cargo SEPARATOR '/') AS Cargo, 
                                             poa_procesos.IdProceso,
                                             poa_procesos.nombre_proceso,
                                             poa_cargos.Cargo,
                                             poa_procesos.fecha_inicio,
                                             poa_procesos.fecha_final,
                                             poa_procesos.presupuesto,
                                             poa_procesos.indicador_cualitativo,
                                             poa_procesos.indicador_cuantitativo,
                                             poa_procesos.Estado
                                      FROM (((siif_EmpleadoFusalmo.poa_cargosasignacion poa_cargosasignacion
                                              INNER JOIN siif_EmpleadoFusalmo.poa_empleado poa_empleado
                                                 ON (poa_cargosasignacion.IdEmpleado = poa_empleado.IdEmpleado))
                                             INNER JOIN siif_EmpleadoFusalmo.poa_cargos poa_cargos
                                                ON (poa_cargosasignacion.IdCargo = poa_cargos.IdCargos))
                                            INNER JOIN
                                            siif_EmpleadoFusalmo.poa_asignacionprocesos poa_asignacionprocesos
                                               ON (poa_asignacionprocesos.IdEmpleado = poa_empleado.IdEmpleado))
                                           INNER JOIN siif_EmpleadoFusalmo.poa_procesos poa_procesos
                                              ON (poa_asignacionprocesos.IdProceso = poa_procesos.IdProceso)
                                              WHERE poa_procesos.IdPoa = :poa  GROUP BY  poa_procesos.IdProceso");                
                $st->bindParam(':poa',$idPoa, PDO::PARAM_STR);
                $st->execute();
                $rows = $st->fetchAll(PDO::FETCH_ASSOC);

                		$impresionfinal='';
                		$procompletados='';
                		$pronocompletados='';

                        foreach ($rows as $row) {
                        $por='';
                        $impresion='';
                        $impresion.="<tr>";
                        $impresion.='<td style="display: none;">'.$row['IdProceso'].'</td>';
                        $impresion.='<td>'.$row['nombre_proceso'].'</td>';
                        $impresion.='<td>'.$row['Cargo'].'</td>';
                        $impresion.='<td>'.$row['fecha_inicio'].'</td>';
                        $impresion.='<td>'.$row['fecha_final'].'</td>';
                         if (!empty($row["indicador_cuantitativo"])){
                           $impresion.= '<td>'.$row['indicador_cuantitativo'].'</td>';
                        }else{
                           $impresion.= '<td>'.$row['indicador_cualitativo'].'</td>';
                        }
                        

                       
                            if($row['Estado'] == 1){
                        $impresion.= '<td style="color: green;">100% Completado</td>';
                        $por=100;
                       }else{
                          if($row['Estado'] == NULL ){
                             $impresion.= '<td>Sin Avances</td>';

                          }else{
                          $ab = $conn->prepare("SELECT COUNT(*) from poa_actividades Where IdProceso = :id");
                           $ab->bindParam(':id',$row['IdProceso'], PDO::PARAM_STR);
                           $ab->execute();
                           $registros2 = $ab->fetchColumn();
                          

                           $ac = $conn->prepare("SELECT * from poa_actividades Where IdProceso = :id");
                           $ac->bindParam(':id',$row['IdProceso'], PDO::PARAM_STR);
                           $ac->execute();
                           $columnas = $ac->fetchAll(PDO::FETCH_ASSOC);

                                  foreach ($columnas as $listos) {

                                       if ($listos["Estado"] == 0 ){
                                        $nocompletado2 ++;
                                         
                                  }else{
                                      $completado2 ++;

                                  }
                                }

                                
                                 
                               
                        if ($registros2 != 0){
                              $porcentaje2 = 100/$registros2;
                              $avanceTotal2 = round($porcentaje2 * $completado2,0,PHP_ROUND_HALF_UP);
                              $por=$avanceTotal2;

                              if($avanceTotal2 <= 33){
                                 $impresion.= '<td style="color: #FF0000;">'.$avanceTotal2.'% Completados</td>';
                               }elseif ($avanceTotal2 >= 34 && $avanceTotal2 <= 66) {
                                 $impresion.= '<td style="color: #e49216;">'.$avanceTotal2.'% Completados</td>';
                               }elseif ($avanceTotal2 >= 67) {
                                $impresion.= '<td style="color: green;">'.$avanceTotal2.'% Completados</td>';
                               }
                        }else {
                          $avanceTotal2 = 0;
                          $impresion.= '<td style="color: #FF0000;">0% Completado</td>';
                        }
                        $completado2 = 0;
                        $nocompletado2 = 0;
                       }
                      
                        }
                        $impresion.= '<td><a href="actividades.php?id='.$row['IdProceso'].'&poa='.$idPoa.'">Ver Actividades</a></td>';
                        $impresion.= '</tr>';


                        if($por==100){
                        	$procompletados.=$impresion;
                        }else{
                        	$pronocompletados.=$impresion;
                        }

                      }

                      switch ($vercompletados) {
                      	case 1:
                      		echo $procompletados;
                      		break;
                      	case 2:
                      		echo $pronocompletados;
                      		break;
                      	case 3:
                      	    echo $procompletados;
                      	    echo $pronocompletados;
                      		break;
                      }
                      if($vercompletados==1){
						
                      }else{
						
                      }                      
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
        </tr>
    </table>
</div>