<?php
//conexion a base de datos
 $db_host="localhost";
  $db_name="siif_EmpleadoFusalmo";
  $db_user="siif_padre";
  $db_pass="Fusalmo2016";
       try {
       $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
       $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    //Captura de error si no se puede conectar
    catch(PDOException $e)
    {
         echo "<script>alert('Error conectando a la base de datos');</script>";
        header('Location: index.php.php?r=0');
        
    }

?>
<div style="float:right; width: 80%; text-align: left; background-color: white; padding:10px"> 
    <table style="width: 100%;">
       <tr><td colspan="7"><h2 style="color: #197198;">Plan Operativo Anual - Estadisticas </h2></td></tr>
        <tr><td colspan="7"><hr color='skyblue' /></td></tr>
        <tr style='display:  inline-block;'>
        	<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Procesos Creados</div>
        <div class="card-body">
          <div class="table-responsive">

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th style="display: none;">IdPOA</th>
                  <th>Nombre del POA</th>
                  <th>Fecha Inicio</th>
                  <th>Fecha Final</th>                  
                  <th>Avance</th>
                  <th>Detalles</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th style="display: none;">IdPOA</th>
                  <th>Nombre del POA</th>
                  <th>Fecha Inicio</th>
                  <th>Fecha Final</th>                  
                  <th>Avance</th>
                  <th>Detalles</th>
                </tr>
              </tfoot>
              <tbody>
              
                
                <?php
                $st = $conn->prepare("SELECT * FROM poa_poa");
                $st->execute();
                $rows = $st->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($rows as $row) {
                        
                        echo "<tr>";
                        echo '<td style="display: none;">'.$row['Id'].'</td>';
                        echo '<td>'.$row['Nombre'].'</td>';
                        echo '<td>'.$row['fecha_inicio'].'</td>';
                        echo '<td>'.$row['fecha_final'].'</td>';
                         if($row['Estado'] == 1){
                        echo '<td style="color: green;">100% Completado</td>';
                       }else{
                          if($row['Estado'] == NULL ){
                             echo '<td>Sin Avances</td>';

                          }else{

                          $ab = $conn->prepare("SELECT COUNT(*) from poa_procesos Where IdPoa = :id");
                           $ab->bindParam(':id',$row['Id'], PDO::PARAM_STR);
                           $ab->execute();
                           $registros2 = $ab->fetchColumn();
                          

                           $ac = $conn->prepare("SELECT * from poa_procesos Where IdPoa = :id");
                           $ac->bindParam(':id',$row['Id'], PDO::PARAM_STR);
                           $ac->execute();
                           $columnas = $ac->fetchAll(PDO::FETCH_ASSOC);
                           $nocompletado2=0;
                           $completado2=0;
                                  foreach ($columnas as $listos) {

                                       if ($listos["Estado"] == 0 ){
                                        $nocompletado2 ++;
                                         
                                  }else{
                                      $completado2 ++;

                                  }
                                }

                        if ($registros2 != 0){
                              $porcentaje2 = 100/$registros2;
                              $avanceTotal2 = round($porcentaje2 * $completado2,0,PHP_ROUND_HALF_UP);
                              if($avanceTotal2 <= 33){
                                 echo '<td style="color: #FF0000;">'.$avanceTotal2.'% Completados</td>';
                               }elseif ($avanceTotal2 >= 34 && $avanceTotal2 <= 66) {
                                 echo '<td style="color: #CFF70A;">'.$avanceTotal2.'% Completados</td>';
                               }elseif ($avanceTotal2 >= 67) {
                                echo '<td style="color: green;">'.$avanceTotal2.'% Completados</td>';
                               }
                        }else {
                          $avanceTotal2 = 0;
                          echo '<td style="color: #FF0000;">0% Completado</td>';
                        }
                        $completado2 = 0;
                        $nocompletado2 = 0;
                       }


                        
                       }
						echo '<td><a href="?l=stats/proceso&amp;id='.$row['Id'].'">Ver Procesos </td>';
                        echo '</tr>';
                      }
                ?>    
              </tbody>
            </table>
          </div>
        </div>
      </div>
        </tr>
    </table>
</div>