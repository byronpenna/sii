<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
 
  <!-- Bootstrap core CSS-->
  <link href="Empleados/POA/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="Empleados/POA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="Empleados/POA/css/sb-admin.css" rel="stylesheet">
   <script src="Empleados/POA/vendor/jquery/jquery.min.js"></script>
    <script src="Empleados/POA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="Empleados/POA/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="Empleados/POA/js/sb-admin.min.js"></script>
    <script src="Empleados/POA/js/pestañas.js"></script>


<script type="text/javascript">
  $(document).ready(function() {
    $("input[name$='tipo']").click(function() {
        var test = $(this).val();

        $("div.form-group").hide();
        $("#hidden" + test).show();
    });
})

</script>


</head>

<body>
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="?l=ProcesosPOA">POA</a>
        </li>
        <li class="breadcrumb-item active">Registro de POA</li>
      </ol>
      <div class="row">

<div class="container"">
 <?php
echo '<a class="btn btn-primary btn-primary" href="javascript:history.back(-1);">Volver</a>';
echo "<br>";
?>
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Seleccion Areas: </div>
      <div class="card-body"  style="padding-left: 3%;">
        <form enctype="multipart/form-data" action="?l=registroPOA" method="post">
          <div class="form-grop">
            <div class="form-row">
              <div class="col-md-6">
                 <label class="form-check-label">
                    <input id="C1" class="form-check-input" name="tei" type="checkbox"  value="4">
                  Gerencia de Tecnologia e Innovacion
                 </label>
                 <br>
                 <label class="form-check-label">
                    <input id="C2" class="form-check-input" name="gaf" type="checkbox"  value="12">
                     Gerencia Administrativa Financiera
                 </label>
                 <br>
                 <label class="form-check-label">
                    <input id="C3" class="form-check-input" name="gid" type="checkbox"  value="13">
                  Gerencia de Investigacion y Desarrollo
                 </label>
                 <br>
                
                 <label class="form-check-label">
                    <input id="C3" class="form-check-input" name="caif" type="checkbox"  value="14">
                  Centro Integral para la Familia
                 </label>
                 <br>

                   <label class="form-check-label">
                    <input id="C3" class="form-check-input" name="psa" type="checkbox"  value="16">
                  Polideportivo Don Bosco Santa Ana
                 </label>
                 <br>

                   <label class="form-check-label">
                    <input id="C3" class="form-check-input" name="psa" type="checkbox"  value="18">
                  Programa Integral Juvenil Don Bosco
                 </label>
                 <br>

                 <label class="form-check-label">
                    <input id="C3" class="form-check-input" name="ccf" type="checkbox"  value="20">
                 Centro Cultural FUSALMO
                 </label>
                 <br>

                  <label class="form-check-label">
                    <input id="C3" class="form-check-input" name="ai" type="checkbox"  value="22">
                 Auditoria Interna
                 </label>
                 <br>

                   <label class="form-check-label">
                    <input id="C3" class="form-check-input" name="do" type="checkbox"  value="26">
                 Direccion de Oratorio
                 </label>
                 <br>

                   <label class="form-check-label">
                    <input id="C3" class="form-check-input" name="cm" type="checkbox"  value="28">
                 Comunicaciones
                 </label>
                 <br>

                  <label class="form-check-label">
                    <input id="C3" class="form-check-input" name="cnext" type="checkbox"  value="33">
                 Consultoria Externa
                 </label>
                 <br>
                
                

              </div>
              <div class="col-md-6">
                <label class="form-check-label">
                    <input id="C6" class="form-check-input" name="SGPYPJ" type="checkbox" value="25">
                  Gerencia Gestion Sociolaboral
                 </label>
                  <br>
                 <label class="form-check-label">
                    <input id="C7" class="form-check-input" name="ggf" type="checkbox"  value="29">
                   Gerencia Gestion y Recaudacion de Fondos
                 </label>
                 <br>
                  <label class="form-check-label">
                    <input id="C6" class="form-check-input" name="ggrf" type="checkbox" value="30">
                   Sub-Gerencia de Participacion y Protagonismo
                 </label>
                <br>
                 <label class="form-check-label">
                    <input id="C6" class="form-check-input" name="rrhh" type="checkbox" value="11">
                   Coordinacion de Recursos Humanos
                 </label>
                <br>
                <label class="form-check-label">
                    <input id="C6" class="form-check-input" name="de" type="checkbox" value="15">
                   Direccion Ejecutiva
                 </label>
                <br>
                 <label class="form-check-label">
                    <input id="C6" class="form-check-input" name="psm" type="checkbox" value="17">
                  Polideportivo Don Bosco San Miguel
                 </label>
                <br>
                <label class="form-check-label">
                    <input id="C6" class="form-check-input" name="usaid" type="checkbox" value="19">
                  Proyecto USAID
                 </label>
                <br>
                <label class="form-check-label">
                    <input id="C6" class="form-check-input" name="sg" type="checkbox" value="21">
                  Servicios Generales
                 </label>
                <br>
                 <label class="form-check-label">
                    <input id="C6" class="form-check-input" name="po" type="checkbox" value="27">
                  Proyecto Coalicion
                 </label>
                <br>
                 <label class="form-check-label">
                    <input id="C6" class="form-check-input" name="yset" type="checkbox" value="32">
                  Modelo Familiar Basado en YSET
                 </label>
                <br>
                 <label class="form-check-label">
                    <input id="C6" class="form-check-input" name="cf" type="checkbox" value="34">
                  Consultoria FOMILENIO II
                 </label>
                <br>
                 
              </div>
            </div>
          </div>
                <br>
                <br>
           <div id="hidden1" class="form-group" >
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPassword1"><b>Inscripccion POA</b></label><br>
                <label for="exampleInputPassword1">Nombre:</label>
                <input class="form-control" type="text" name="nombre1"><br>
                <label for="exampleInputPassword1">Fecha de Inicio:</label>
                <input class="form-control" type="month" name="fechaInicio">
                 <label for="exampleInputPassword1">Fecha de Final:</label>
                <input class="form-control" type="month" name="fechaFinal">
                <br>
                
              </div>
             
            </div>
          </div>



        
         <!--  <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
              
              </div>
              <div class="col-md-6">
                
              </div>
            </div>
          </div> <!-- aqui --> 
          
          <!-- hasta aqui -->
          
          <input type="submit" class="btn btn-primary btn-block" />
         
        </form>
       
      </div>
    </div>
  </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   
    <!-- Scroll to Top Button-->
   </div></tr></table>
    <!-- Bootstrap core JavaScript-->
    <script src="Empleados/POA/vendor/jquery/jquery.min.js"></script>
    <script src="Empleados/POA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="Empleados/POA/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="Empleados/POA/js/sb-admin.min.js"></script>
    <script src="Empleados/POA/js/pestañas.js"></script>
  </div>
</body>

</html>
